﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="RoutineServiceLevel.aspx.cs" Inherits="MIS.RoutineServiceLevel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="CustomStyles/onm.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>Routine Service Level</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server">
                            <fieldset>                             
                                <div>                                   
                                        <asp:GridView ID="GridView1" runat="server" Width="870px">
                                            <%--<Columns>
                                                <asp:BoundField HeaderText="No." />
                                                <asp:BoundField HeaderText="Type" />
                                                <asp:BoundField HeaderText="Item" />
                                                <asp:BoundField HeaderText="Item Performance Description" />
                                                <asp:TemplateField HeaderText="Item Performance Status"></asp:TemplateField>
                                            </Columns>--%>
                                        </asp:GridView>
                                   
                                </div>
                               <asp:Button ID="btnPrint" runat="server" Text="Print" />
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
</asp:Content>
