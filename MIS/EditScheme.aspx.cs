﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class EditScheme : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayCircleList();
        }

        private void DisplayCircleList()
        {
            ContentPlaceHolder content = (ContentPlaceHolder)this.Master.FindControl("ContentSection");

            List<CircleManager> circleList = new List<CircleManager>();
            circleList = new CircleDB().GetCircles();

            StringBuilder htmlCircleSelectDropdown = new StringBuilder();

            htmlCircleSelectDropdown.Append("<div class=\"container\" style=\"margin-top:5%;\">");
            htmlCircleSelectDropdown.Append("<div class=\"row\">");
            htmlCircleSelectDropdown.Append("<div class=\"col-sm-12 col-md-10 col-md-offset-1\">");
            htmlCircleSelectDropdown.Append("<div class=\"panel panel-default\">");
            htmlCircleSelectDropdown.Append("<div class=\"panel-heading\">");
            htmlCircleSelectDropdown.Append("<strong>Edit Schemes</strong>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("<div class=\"panel-body\">");
            htmlCircleSelectDropdown.Append("<form id=\"editSchemes\" role=\"form\" class=\"form form-horizontal\" action=\"#\" method=\"post\" runat=\"server\">");
            htmlCircleSelectDropdown.Append("<fieldset>");

            htmlCircleSelectDropdown.Append("<div class=\"row\">");
            htmlCircleSelectDropdown.Append("<div class=\"col-sm-12 col-md-12\">");
            htmlCircleSelectDropdown.Append("<div class=\"box box-centerside\">");
            //htmlSelectDropdown.Append("<br />");
            htmlCircleSelectDropdown.Append("<div class=\"form-group\">");
            htmlCircleSelectDropdown.Append("<label for=\"circleList\" class=\"col-xs-4 control-label\">Select Circle</label>");
            htmlCircleSelectDropdown.Append("<div class=\"col-xs-8\">");
            htmlCircleSelectDropdown.Append("<select class=\"form-control\" name=\"circleList\" id=\"circleList\">");
            htmlCircleSelectDropdown.Append("<option value=\"\">--Select Circle--</option>");
            foreach (var item in circleList)
            {
                htmlCircleSelectDropdown.Append("<option value=\"" + item.CircleGuid + "\">");
                htmlCircleSelectDropdown.Append(item.CircleName);
                htmlCircleSelectDropdown.Append("</option>");
            }
            htmlCircleSelectDropdown.Append("</select>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("<div class=\"form-group\">");
            htmlCircleSelectDropdown.Append("<label for=\"divisionList\" class=\"col-xs-4 control-label\">Select Division</label>");
            htmlCircleSelectDropdown.Append("<div class=\"col-xs-8\">");
            htmlCircleSelectDropdown.Append("<input type=\"text\" name=\"divisionList\" class=\"form-control\" id=\"divisionList\" />");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("<div id=\"schemeDetails\" class=\"form-group\"></div>");         
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("<div class=\"row\">");
            htmlCircleSelectDropdown.Append("<br />");
            htmlCircleSelectDropdown.Append("<div id=\"courseDetails\"></div>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("</fieldset>");
            htmlCircleSelectDropdown.Append("</form>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("<div class=\"panel-footer\">");
            htmlCircleSelectDropdown.Append("<div class=\"row\">");
            htmlCircleSelectDropdown.Append("<div class=\"col-xs-12\">");
            htmlCircleSelectDropdown.Append("<div class=\"pull-left\">");
            htmlCircleSelectDropdown.Append("<a class=\"btn btn-primary\" href=\"AIS.aspx\" onclick=\"\">Back to Home</a>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("</div>");
            htmlCircleSelectDropdown.Append("</div>");

            content.Controls.Add(new Literal { Text = htmlCircleSelectDropdown.ToString() });
                     

        }

        private void GetSchemeList()
        {
            //List<SchemeManager> schemeList = new List<SchemeManager>();
            //schemeList = new SchemeDB().GetSchemes(string circleGuid);

            //StringBuilder htmlSelectDropdown = new StringBuilder();

            //htmlSelectDropdown.Append("<div id=\"schemeList\" class=\"form-group\">");
            //htmlSelectDropdown.Append("<label for=\"schemeList\" class=\"col-xs-4 control-label\">Select Scheme</label>");
            //htmlSelectDropdown.Append("<div class=\"col-xs-8\">");
            //htmlSelectDropdown.Append("<select class=\"form-control\" name=\"schemeList\" id=\"schemeList\">");
            //htmlSelectDropdown.Append("<option value=\"\">--Select Course--</option>");
            //foreach (var item in schemeData)
            //{
            //    htmlSelectDropdown.Append("<option value=\"" + item.SchemeGuid + "\">");
            //    htmlSelectDropdown.Append(item.SchemeName);
            //    htmlSelectDropdown.Append("</option>");
            //}
            //htmlSelectDropdown.Append("</select>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("<div class=\"row\">");
            //htmlSelectDropdown.Append("<br />");
            //htmlSelectDropdown.Append("<div id=\"courseDetails\"></div>");
            //htmlSelectDropdown.Append("</div>");

            //htmlSelectDropdown.Append("</fieldset>");
            //htmlSelectDropdown.Append("</form>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("<div class=\"panel-footer\">");
            //htmlSelectDropdown.Append("<div class=\"row\">");
            //htmlSelectDropdown.Append("<div class=\"col-xs-12\">");
            //htmlSelectDropdown.Append("<div class=\"pull-left\">");
            //htmlSelectDropdown.Append("<a class=\"btn btn-primary\" href=\"AIS.aspx\" onclick=\"\">Back to Home</a>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("</div>");
            //htmlSelectDropdown.Append("</div>");


        }

        [WebMethod]
        public static Array DisplaySchemeName(string circleGuid)
        {        

            List<SchemeManager> schemeList = new List<SchemeManager>();
            schemeList = new SchemeDB().GetSchemes(circleGuid);

            List<String> schemeNameList = new List<string>();

            if (schemeList.Count > 0)
            {
                foreach (SchemeManager schemeObj in schemeList)
                {
                    schemeNameList.Add(schemeObj.SchemeGuid.ToString());                    
                    schemeNameList.Add(schemeObj.SchemeName.ToString());
                }
            }
            return schemeNameList.ToArray();

        }
       
        [WebMethod]
        public static Array DisplaySchemeData(string schemeGuid)
        {
            SchemeManager schemeObj = new SchemeDB().GetSchemeDataBySchemeGuid(new Guid(schemeGuid));

            string[] schemeDetails = new string[11];

            schemeDetails[0] = schemeObj.SchemeName;
            schemeDetails[1] = schemeObj.SchemeCode;
            schemeDetails[2] = schemeObj.SchemeGuid.ToString();
            schemeDetails[3] = schemeObj.SchemeType;
            schemeDetails[4] = schemeObj.UtmEast1.ToString();
            schemeDetails[5] = schemeObj.UtmEast2.ToString();
            schemeDetails[6] = schemeObj.UtmNorth1.ToString();
            schemeDetails[7] = schemeObj.UtmNorth2.ToString();
            schemeDetails[8] = schemeObj.Area.ToString();
            //schemeDetails[9] = schemeObj.DivisionGuid.ToString();
            schemeDetails[9] = schemeObj.DrawingID.ToString();

            return schemeDetails;
        }

        [WebMethod]
        public static bool UpdateSchemeData(string schemeGuid,string schemeCode, string schemeType, string schemeName, string utmEast1, string utmEast2, string utmNorth1, string utmNorth2, string area, string circleGuid, string divisionGuid, string drawingID)
        {
            return new SchemeDB().UpdateSchemeData(new Guid(schemeGuid), schemeCode, schemeType, schemeName, (string.IsNullOrEmpty(utmEast1) ? 0 : int.Parse(utmEast1)), (string.IsNullOrEmpty(utmEast2) ? 0 : int.Parse(utmEast2)), (string.IsNullOrEmpty(utmNorth1) ? 0 : int.Parse(utmNorth1)), (string.IsNullOrEmpty(utmNorth2) ? 0 : int.Parse(utmNorth2)), (string.IsNullOrEmpty(area) ? 0 : int.Parse(area)), new Guid(circleGuid), divisionGuid, drawingID);
        }

    }
}