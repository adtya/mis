﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class CreateWorkType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Units> unitsList = new List<Units>();

            unitsList = new UnitsDB().GetUnits();

            StringBuilder htmlUnitList = new StringBuilder();

            if (unitsList.Count > 0)
            {
                htmlUnitList.Append("<select class=\"form-control\" name=\"workTypeUnitList\" id=\"workTypeUnitList\">");

                foreach (Units unitObj in unitsList)
                {
                    htmlUnitList.Append("<option value=\"" + unitObj.UnitGuid.ToString() + "\">" + unitObj.UnitSymbol.ToString() + "</option>");
                }

                htmlUnitList.Append("</select>");

            }

            ltWorkTypeUnit.Text = htmlUnitList.ToString();
        }

        [WebMethod]
        public static void CreateNewWorkType(string workTypeName, string workTypeUnit, string[] arrWorkTypeSubHeadTitle)
        {
            new WorkTypeDB().AddNewWorkType(workTypeName,workTypeUnit,arrWorkTypeSubHeadTitle);            
        }
    }
}