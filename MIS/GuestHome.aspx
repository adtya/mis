﻿<%@ Page Title="Guest User" Language="C#" MasterPageFile="~/Guest.Master" AutoEventWireup="true" CodeBehind="GuestHome.aspx.cs" Inherits="MIS.GuestHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    <link href="CustomStyles/Guest/guest-home.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <p>The Flood and River Erosion Management Agency of Assam (FREMAA) is set up in 2010-11 as an Executing Agency (EA) under Society Registration Act 1860 to act as a special purpose vehicle for implementation of “The Assam Integrated Flood and Riverbank Erosion Risk Management Investment Program (AIFRERIP) funded by Asian Devlopment Bank(ADB).
The primary aim of FREMAA is to manage the implementation of the Asian Development Bank (ADB) funded project with provision of comprehensive, cost-effective and sustainable structural and non-structural measures in the selected strategic locations.</p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">

</asp:Content>