﻿<%@ Page Title ="Create Units" Language="C#" MasterPageFile="~/DBA.Master" AutoEventWireup="true" CodeBehind="CreateUnits.aspx.cs" Inherits="MIS.CreateUnits" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div class="container" style="margin-top:5%;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>Create Units</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" action="#" method="post" runat="server">
                            <fieldset>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="unitName" id="unitNameLabel" class="control-label">Unit Name</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="unitName" class="form-control" id="unitName" placeholder="Unit Name" /> 
                                            </div>
                                        </div>
                                       

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="unitSymbol" id="unitSymbolLabel" class="control-label">Unit Symbol</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="unitSymbol" class="form-control" id="unitSymbol" placeholder="Unit Symbol" />
                                            </div>
                                        </div>
                                        </div>                                
                                       </div>

                               <div class="row">
                                    <div class="col-xs-4 col-xs-offset-4 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
                                        <button type="button" name="createUnitBtn" class="btn btn-block btn-primary" id="createUnitBtn">Create Units</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="pull-left">
                                    <a class="btn btn-primary" href="DBAHome.aspx" onclick="">Back to Home</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
    <script src="CustomScripts/create_unit.js" type="text/javascript"></script>
</asp:Content>
