﻿<%@ Page Title="Countries List" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Countries.aspx.cs" Inherits="MIS.Countries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container-fluid" style="margin-top:5%;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                
            </div>
        </div>
    </div>
    <asp:Literal ID="countriesListLiteral" runat="server"></asp:Literal>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
</asp:Content>