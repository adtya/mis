﻿<%@ Page Title="Add Asset" Language="C#" MasterPageFile="~/DBA.Master" AutoEventWireup="true" CodeBehind="CreateAsset.aspx.cs" Inherits="MIS.CreateAsset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    <link href="Content/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
    <script src="Scripts/jquery.glob.js" type="text/javascript"></script>
    <script src="Scripts/globinfo/jQuery.glob.en-IN.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.GlobalMoneyInput2.js" type="text/javascript"></script>

    <script src="Scripts/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="Scripts/Datejs/build/date.js" type="text/javascript"></script>
    <script src="Scripts/date.format.js" type="text/javascript"></script>
    <script src="Scripts/jquery.Guid.js" type="text/javascript"></script>
    <script src="Scripts/dropzone/dropzone.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>New Asset</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server" id="addAssetForm" name="addAssetForm">
                            <fieldset>                               

                                <div class="row">
                                    <div class="col-sm-12 col-md-12">                                        
                                            <div class="col-sm-6 col-md-6">
                                                <div class="form-group">
                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                       <label for="divisionList" id="divisionListLabel" class="control-label">Division</label>
                                                     </div>
                                                     <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                        <%--<asp:Literal ID="ltDivisionList" runat="server"></asp:Literal>--%>
                                                         <select class="form-control multiselect" name="divisionList" id="divisionList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">

                                                         </select>
                                                     </div>
                                                 </div>
                                             </div>
                                            <div class="col-sm-6 col-md-6">
                                                <div class="form-group">
                                                   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                       <label for="schemeList" id="schemeListLabel" class="control-label">Scheme</label>
                                                   </div>
                                                   <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                        <select class="form-control multiselect" id="schemeList" name="schemeList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">

                                                        </select>
                                                   </div>
                                                </div>
                                           </div>
                                     </div>
                                </div>                               
                                
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                      
                                            <div class="col-sm-6 col-md-6">
                                                 <div class="form-group">
                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                         <label for="assetTypeList" id="assetTypeLabel" class="control-label">Asset Type</label>
                                                    </div>
                                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                        <asp:Literal ID="ltAssetTypeList" runat="server"></asp:Literal>
                                                    </div>
                                             </div>
                                           </div>

                                            <div class="col-sm-6 col-md-6">
                                                  <div class="form-group">
                                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="assetCodeNumber" id="assetCodeNumberLabel" class="control-label">Asset Number</label>
                                                       </div>
                                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <select class="form-control multiselect" id="assetCodeNumberSelect" name="assetCodeNumberSelect" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">

                                                            </select>
                                                       </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>       
                                                                   
                                                      
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">                                         
                                                <div class="col-sm-12 col-md-12" >
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="assetCode" id="assetCodeLabel" class="control-label">Asset Code</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input id="assetCode" name="assetCode" placeholder="Asset Code" class="form-control" readonly="readonly" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6" style="display:none" id="showKeyAssetSelect">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="keyAsset" id="keyAssetLabel" class="control-label text-left">Select Key Asset</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control multiselect" id="keyAssetSelect" name="keyAssetSelect" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                 </div>
                                            </div>
                                      </div>
                                </div>                                               
                                            

                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                                
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="drawingCode" id="drawingCodeLabel" class="control-label">Drawing Code</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div id="uploadDrawing" class="input-group">
                                                                    <input id="drawingCode" name="drawingCode" placeholder="Drawing Code" class="form-control upload" readonly="readonly"/>
                                                                    <span class="input-group-addon form-control-static upload">
                                                                        <i class="fa fa-upload" title="Upload Drawing"></i>
                                                                    </span>
                                                                </div>
                                                                <input type="file" id="drawingCode1" name="drawingCode1" class="form-control hidden" />
                                                                <%--<div id="dZUpload" class="hidden dropzone">
                                                                    <%--<div id="dropzonePreview" class="dz-default dz-message">
                                                                                                                                        
                                                                    </div>--%

                                                                </div>--%>
                                                               

                                                            </div>
                                                         </div>
                                                     </div>

                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                 <label for="amtd" id="amtdLabel"class="control-label">AMTD</label>
                                                           </div>
                                                           <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input id="amtd" name="amtd" placeholder="AMTD" class="form-control" />                                              
                                                           </div>
                                                        </div>
                                                   </div>
                                            </div>                               
                                    
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                                
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="assetName" id="assetNameLabel" class="control-label">Asset Name</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input id="assetName" name="assetName" placeholder="Asset Name" class="form-control" />                                                   
                                                            </div>
                                                        </div>
                                                    </div>                                   
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="assetCost" id="assetCostLabel"class="control-label">Asset Cost (Rs.)</label>
                                                           </div>
                                                           <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input id="assetCost" name="assetCost" placeholder="Asset Cost (Rs.)" class="form-control" />                                                    
                                                           </div>                                            
                                                       </div>
                                                   </div>
                                            </div>
                                        </div>                                

                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                   
                                                
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                             <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="chainageStart" id="chainageStartLabel"class="control-label">Chainage Start</label>
                                                             </div>
                                                             <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input id="chainageStart" name="chainageStart" placeholder="Chainage Start" class="form-control" />                                                   
                                                             </div>
                                                         </div>
                                                    </div>                                                                               
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="chainageEnd" id="chainageEndLabel" class="control-label">Chainage End</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input id="chainageEnd" name="chainageEnd" placeholder="Chainage End" class="form-control" />                                                    
                                                            </div>
                                                         </div>
                                                </div>
                                            </div>
                                        </div>                              

                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                   
                                                
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                                 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                        <label for="eastingUS" id="eastingUSLabel"class="control-label">Easting US</label>
                                                                 </div>
                                                                 <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <input id="eastingUS" name="eastingUS" placeholder="Easting US" class="form-control" />                                                    
                                                                </div>
                                                            </div>
                                                         </div>                                                                            
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="eastingDS" id="eastingDSLabel"class="control-label">Easting DS</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <input id="eastingDS" name="eastingDS" placeholder="Easting DS" class="form-control" />                                                   
                                                                </div>
                                                           </div>
                                                    </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                             <div class="col-sm-12 col-md-12">                                   
                                                
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                             <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">                                            
                                                                    <label for="northingUS" id="northingUSLabel"class="control-label">Northing US</label>
                                                             </div>
                                                             <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <input id="northingUS" name="northingUS" placeholder="Northing US" class="form-control" />                                                        
                                                             </div>
                                                         </div>
                                                     </div>
                                                                                                                               
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="northingDS" id="northingDSLabel"class="control-label">Northing DS</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input id="northingDS" name="northingDS" placeholder="Northing DS" class="form-control" />                                                   
                                                            </div>
                                                         </div>
                                                      </div>
                                            </div>
                                        </div>                   

                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                   
                                                
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                             <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="initiationDate" id="initiationDateLabel"class="control-label">Initiation Date</label>
                                                             </div>
                                                             <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                 <div id="dpDateOfInitiationOfAsset" class="input-group date">
                                                                     <input type="text" name="dateOfInitiationOfAsset" class="form-control" id="dateOfInitiationOfAsset" placeholder="Date Of Initiation (dd/mm/yyyy)" />
                                                                     <span class="input-group-addon form-control-static">
                                                                         <i class="fa fa-calendar fa-fw"></i>
                                                                     </span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                                                                                                    
                                                     <div class="col-sm-6 col-md-6">
                                                          <div class="form-group">
                                                             <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="completionDate" id="completionDateLabel"class="control-label">Completion Date</label>
                                                             </div>
                                                             <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                 <div id="dpDateOfCompletionOfAsset" class="input-group date">
                                                                     <input type="text" name="dateOfCompletionOfAsset" class="form-control" id="dateOfCompletionOfAsset" placeholder="Date Of Completion (dd/mm/yyyy)" />
                                                                     <span class="input-group-addon form-control-static">
                                                                         <i class="fa fa-calendar fa-fw"></i>
                                                                     </span>
                                                                </div>
                                                            </div>

                                                        </div>                                                     
                                                </div>
                                            </div>                                    
                                        </div>                                       
                                     </div>
                                </div>
                                
                                <div class="col-xs-12 col-sm-12 col-md-12">                                   
                                                 <div class="form-group" id="assetTypePanel">
                                                 </div>
                                </div>                                       
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary" type="submit" aria-hidden="true" id="saveAssetBtn">Save</button>
                                            <button class="btn btn-primary" id="cancelAssetBtn" aria-hidden="true">Cancel</button>

                                        </div>                                                               
                                    </div>
                                </div>
                                <%--<asp:Button ID="Button1" runat="server" Text="Print" Width="62px" OnClick="Button1_Click"/>--%>                                           
                                        
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>  
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
    <script src="CustomScripts/DBA/Asset/add-asset.js" type="text/javascript"></script> 
</asp:Content>
