﻿<%@ Page Title="Fill Progress" Language="C#" MasterPageFile="~/SIO.Master" AutoEventWireup ="true" CodeBehind="FillProgress.aspx.cs" Inherits="MIS.FillProgress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    <link href="Content/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="Content/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

    <link href="CustomStyles/create-new-project.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
    <script src="Scripts/bootbox.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.validate.min.js" type="text/javascript"></script>
    <script src="Scripts/additional-methods.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.glob.js" type="text/javascript"></script>
    <script src="Scripts/globinfo/jQuery.glob.en-IN.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.GlobalMoneyInput2.js" type="text/javascript"></script>
    <script src="Scripts/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="Scripts/Datejs/build/date.js" type="text/javascript"></script>
    <script src="Scripts/date.format.js" type="text/javascript"></script>   

    <script src="CustomScripts/my-validation-methods.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div class="container" style="margin-top:5%;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>Fill Project Progress</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" name="fillProgressFormName" id="fillProgressForm" class="form form-horizontal" action="#" method="post" runat="server">
                            <fieldset>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="box">
                                            <%--<div class="step">--%>
                                                <%--<div class="panel panel-default">--%>

                                                    <%--<div class="panel-heading">
                                                        <h3 align="center">
                                                            <strong>Fill Project Progress Information</strong>
                                                        </h3>
                                                    </div>--%>

                                                    <%--<div class="panel-body">  --%>                                    
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="ProjectName" id="ProjectNameLabel" class="control-label">Name of Project</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="ProjectName" class="form-control" id="ProjectName" placeholder="Name of Project" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="subChainageName" id="subChainageNameLabel" class="control-label">Name of Sub-Chainage</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="subChainageName" class="form-control" id="subChainageName" placeholder="Name of Sub-Chainage" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="pmcEngineers" id="pmcEngineersLabel" class="control-label">PMC Engineers</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">                                                                
                                                                    <input type="text" name="pmcEngineers" class="form-control" id="pmcEngineers" placeholder="PMC Engineers" />                                                             
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="siteEngineers" id="siteEngineersLabel" class="control-label">Site Engineers</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="siteEngineers" class="form-control" id="siteEngineers" placeholder="Site Engineers" /> 
                                                            </div>                                                            
                                                        </div>

                                                  <%--  </div>--%>

                                               <%-- </div>--%>
                                           <%-- </div>--%>
                                         

                                            <!--Buttons-->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="pull-right">                                                     
                                                        <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnSave">Save</button>
                                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Buttons ended-->

                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>                        
                    </div>
                    <div id="step-display" class="panel-footer"></div>
                </div>
            </div>
        </div>      
    </div>       
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">   
</asp:Content>
