﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class RepairSchemeRisk : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dtRepairSchemeRisk = new DataTable();
            DataRow drRepairSchemeRisk = null;

            dtRepairSchemeRisk.Columns.Add(new DataColumn("Scheme Code", typeof(string)));
            dtRepairSchemeRisk.Columns.Add(new DataColumn("Name", typeof(string)));
            dtRepairSchemeRisk.Columns.Add(new DataColumn("Risk Rating", typeof(string)));


            drRepairSchemeRisk = dtRepairSchemeRisk.NewRow();

            drRepairSchemeRisk["Scheme Code"] = "";
            drRepairSchemeRisk["Name"] = "";
            drRepairSchemeRisk["Risk Rating"] = "";


            dtRepairSchemeRisk.Rows.Add(drRepairSchemeRisk);

            GridView1.DataSource = dtRepairSchemeRisk;
            GridView1.DataBind();
        }
    }
}