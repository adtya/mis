﻿<%@ Page Title="MIS Home" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MIS.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="CustomStyles/default.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <!-- Main component for a primary marketing message or call to action -->
    <p>The Flood and River Erosion Management Agency of Assam (FREMAA) is set up in 2010-11 as an Executing Agency (EA) under Society Registration Act 1860 to act as a special purpose vehicle for implementation of “The Assam Integrated Flood and Riverbank Erosion Risk Management Investment Program (AIFRERIP) funded by Asian Devlopment Bank(ADB).
The primary aim of FREMAA is to manage the implementation of the Asian Development Bank (ADB) funded project with provision of comprehensive, cost-effective and sustainable structural and non-structural measures in the selected strategic locations.</p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
</asp:Content>
