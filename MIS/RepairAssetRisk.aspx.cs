﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class RepairAssetRisk : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dtRepairAssetRisk = new DataTable();
            DataRow drRepairAssetRisk = null;

            dtRepairAssetRisk.Columns.Add(new DataColumn("Scheme Code", typeof(string)));
            dtRepairAssetRisk.Columns.Add(new DataColumn("Name", typeof(string)));
            dtRepairAssetRisk.Columns.Add(new DataColumn("Risk Rating", typeof(string)));


            drRepairAssetRisk = dtRepairAssetRisk.NewRow();

            drRepairAssetRisk["Scheme Code"] = "";
            drRepairAssetRisk["Name"] = "";
            drRepairAssetRisk["Risk Rating"] = "";


            dtRepairAssetRisk.Rows.Add(drRepairAssetRisk);

            GridView1.DataSource = dtRepairAssetRisk;
            GridView1.DataBind();
        }
    }
}