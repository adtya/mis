﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class CreateWorkItem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayUnit();
        }

        private void DisplayUnit()
        {
            List<UnitManager> unitList = new List<UnitManager>();

            unitList = new UnitDB().GetUnits();

            StringBuilder htmlUnitList = new StringBuilder();

            if (unitList.Count > 0)
            {
                htmlUnitList.Append("<select class=\"form-control\" name=\"workItemUnitList\" id=\"workItemUnitList\">");

                foreach (UnitManager unitObj in unitList)
                {
                    htmlUnitList.Append("<option value=\"" + unitObj.UnitGuid.ToString() + "\">" + unitObj.UnitSymbol + "</option>");
                }

                htmlUnitList.Append("</select>");

            }

            ltWorkTypeUnit.Text = htmlUnitList.ToString();
        }

        [WebMethod]
        public static void CreateNewWorkItem(string workItemName, string workItemUnitGuid, string[] arrWorkItemSubHeadTitles)
        {
            new WorkItemDB().AddWorkItem(workItemName, new Guid(workItemUnitGuid), arrWorkItemSubHeadTitles);            
        }
    }
}