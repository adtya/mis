﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineAnalysisDetailsDB
    {
        protected internal RoutineAnalysisDetailsManager GetRoutineAnalysisGuid(Guid routineWorkItemGuid, Guid assetGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_analysis_details_uuid ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_analysis_details obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_uuid= :routineWorkItemGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            arParams[1] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetGuid;

            RoutineAnalysisDetailsManager routineAnalysisDetailsManagerObj = new RoutineAnalysisDetailsManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    routineAnalysisDetailsManagerObj.RoutineAnalysisDetailsGuid = string.IsNullOrEmpty(reader["routine_analysis_details_uuid"].ToString()) ? new Guid() : new Guid(reader["routine_analysis_details_uuid"].ToString());
                }
            }
            return routineAnalysisDetailsManagerObj;
        }

        protected internal int SaveRoutineAnalysisDetailsData(RoutineAnalysisDetailsManager routineAnalysisDetailsManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_routine_analysis_details ");
            sqlCommand.Append("( ");
            sqlCommand.Append("routine_analysis_details_uuid, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("routine_work_item_uuid, ");
            sqlCommand.Append("asset_quantity, ");
            sqlCommand.Append("routine_cost ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":routineAnalysisDetailsGuid, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":routineWorkItemGuid, ");
            sqlCommand.Append(":assetQuantity, ");
            sqlCommand.Append(":routineWorkItemCost ");            
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[5];

            arParams[0] = new NpgsqlParameter("routineAnalysisDetailsGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineAnalysisDetailsManager.RoutineAnalysisDetailsGuid;

            arParams[1] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = routineAnalysisDetailsManager.RoutineWorkItemManager.RoutineWorkItemGuid;

            arParams[2] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = routineAnalysisDetailsManager.AssetManager.AssetGuid;

            arParams[3] = new NpgsqlParameter("routineWorkItemCost", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = routineAnalysisDetailsManager.RoutineWorkItemCost.Amount;

            arParams[4] = new NpgsqlParameter("assetQuantity", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = routineAnalysisDetailsManager.AssetQuantity;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateRoutineAnalysisDetailsData(Guid routineAnalysisDetailsGuid, RoutineAnalysisDetailsManager routineAnalysisDetailsManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_om_routine_analysis_details ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("asset_quantity= :assetQuantity, ");
            sqlCommand.Append("routine_cost= :routineWorkItemCost ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("routine_analysis_details_uuid= :routineAnalysisDetailsGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("routineAnalysisDetailsGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineAnalysisDetailsGuid;

            arParams[1] = new NpgsqlParameter("assetQuantity", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = routineAnalysisDetailsManager.AssetQuantity;

            arParams[2] = new NpgsqlParameter("routineWorkItemCost", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = routineAnalysisDetailsManager.RoutineWorkItemCost.Amount;  

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;
        }

        protected internal int DeleteRoutineAnalysisDetailsData(Guid routineWorkItemGuid, Guid assetGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_analysis_details ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("routine_work_item_uuid = :routineWorkItemGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("asset_uuid = :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            arParams[1] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}