﻿using NodaMoney;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RepairFundingManager
    {
        #region Private Members

        private Guid repairFundingGuid;          
        private string fiscalYear;
        private Money availableFunds;
        private decimal availableFundsInput;
        
        #endregion


        #region Public Constructors

        public RepairFundingManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid RepairFundingGuid
        {
            get
            {
                return repairFundingGuid;
            }
            set
            {
                repairFundingGuid = value;
            }
        }
      
        public string FiscalYear
        {
            get
            {
                return fiscalYear;
            }
            set
            {
                fiscalYear = value;
            }
        }

        public Money AvailableFunds
        {
            get
            {
                if (availableFunds.Amount.Equals(0))
                {
                    availableFunds = AvailableFundsInput;
                }
                return availableFunds;
            }
            set
            {
                availableFunds = value;
            }
        }

        public decimal AvailableFundsInput
        {
            private get
            {
                return availableFundsInput;
            }
            set
            {
                availableFundsInput = value;
            }
        }

        #endregion
    }
}