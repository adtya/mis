﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class BridgeDB
    {
        protected internal int AddBridge(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_bridge ");
            sqlCommand.Append("( ");
            sqlCommand.Append("bridge_uuid, ");
            sqlCommand.Append("length, ");
            sqlCommand.Append("pier_no, ");
            sqlCommand.Append("road_width, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("bridge_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":bridgeGuid, ");
            sqlCommand.Append(":length, ");
            sqlCommand.Append(":pierNumber, ");
            sqlCommand.Append(":roadWidth, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":bridgeTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[6];

            arParams[0] = new NpgsqlParameter("bridgeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("length", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.BridgeManager.Length;

            arParams[2] = new NpgsqlParameter("pierNumber", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.BridgeManager.PierNumber;

            arParams[3] = new NpgsqlParameter("roadWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.BridgeManager.RoadWidth;

            arParams[4] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.AssetGuid;

            arParams[5] = new NpgsqlParameter("bridgeTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.BridgeManager.BridgeTypeManager.BridgeTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateBridge(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_bridge ");
            sqlCommand.Append("SET ");         
            sqlCommand.Append("length= :length, ");
            sqlCommand.Append("pier_no= :pierNumber, ");
            sqlCommand.Append("road_width= :roadWidth, ");
            sqlCommand.Append("bridge_type_uuid= :bridgeTypeGuid "); 
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[6];

             arParams[0] = new NpgsqlParameter("bridgeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
             arParams[0].Direction = ParameterDirection.Input;
             arParams[0].Value = assetManager.BridgeManager.BridgeGuid;

             arParams[1] = new NpgsqlParameter("length", NpgsqlTypes.NpgsqlDbType.Double);
             arParams[1].Direction = ParameterDirection.Input;
             arParams[1].Value = assetManager.BridgeManager.Length;

             arParams[2] = new NpgsqlParameter("pierNumber", NpgsqlTypes.NpgsqlDbType.Integer);
             arParams[2].Direction = ParameterDirection.Input;
             arParams[2].Value = assetManager.BridgeManager.PierNumber;

             arParams[3] = new NpgsqlParameter("roadWidth", NpgsqlTypes.NpgsqlDbType.Double);
             arParams[3].Direction = ParameterDirection.Input;
             arParams[3].Value = assetManager.BridgeManager.RoadWidth;

             arParams[4] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
             arParams[4].Direction = ParameterDirection.Input;
             arParams[4].Value = assetManager.AssetGuid;

             arParams[5] = new NpgsqlParameter("bridgeTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
             arParams[5].Direction = ParameterDirection.Input;
             arParams[5].Value = assetManager.BridgeManager.BridgeTypeManager.BridgeTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}