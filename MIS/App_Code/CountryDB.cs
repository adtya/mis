﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class CountryDB
    {
        protected internal void CreateCountry(string countryCode2, string countryCode3, string countryCodeNumeric, string countryName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_countries ");
            sqlCommand.Append("( ");
            sqlCommand.Append("country_uuid, ");
            sqlCommand.Append("country_code_iso_2, ");
            sqlCommand.Append("country_code_iso_3, ");
            sqlCommand.Append("country_code_iso_numeric, ");
            sqlCommand.Append("country_name ");
            //sqlCommand.Append("division_deleted ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":countryGuid, ");
            sqlCommand.Append(":countryCode2, ");
            sqlCommand.Append(":countryCode3, ");
            sqlCommand.Append(":countryCodeNumeric, ");
            sqlCommand.Append(":countryName ");
            //sqlCommand.Append(":divisionDeleted ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[5];

            arParams[0] = new NpgsqlParameter("countryGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("countryCode2", NpgsqlTypes.NpgsqlDbType.Varchar, 2);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = countryCode2;

            arParams[2] = new NpgsqlParameter("countryCode3", NpgsqlTypes.NpgsqlDbType.Varchar, 3);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = countryCode3;

            arParams[3] = new NpgsqlParameter("countryCodeNumeric", NpgsqlTypes.NpgsqlDbType.Varchar, 3);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = countryCodeNumeric;

            arParams[4] = new NpgsqlParameter("countryName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = countryName;

            //arParams[2] = new NpgsqlParameter("divisionDeleted", NpgsqlTypes.NpgsqlDbType.Composite);
            //arParams[2].Direction = ParameterDirection.Input;
            //arParams[2].Value = divisionDeleted;

            int result = Convert.ToInt32(NpgsqlHelper.ExecuteScalar(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams));

            //using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            //{
            //    while (reader.Read())
            //    {
            //        DivisionManager divisionObj = new DivisionManager();

            //        divisionObj.DivisionGuid = new Guid(reader["division_id"].ToString());
            //        divisionObj.DivisionName = reader["division_name"].ToString();
            //        divisionObj.DivisionDeleted = Convert.ToBoolean(reader["division_deleted"].ToString());

            //        divisionList.Add(divisionObj);
            //    }
            //}
        }

        protected internal List<Country> GetCountries()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_countries ");
            sqlCommand.Append(";");

            List<Country> countriesList = new List<Country>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    Country countryObj = new Country();

                    countryObj.CountryGuid = new Guid(reader["country_uuid"].ToString());
                    countryObj.CountryName = reader["country_name"].ToString();
                    countryObj.CountryISOCode2 = reader["country_code_iso_2"].ToString();
                    countryObj.CountryISOCode3 = reader["country_code_iso_3"].ToString();
                    countryObj.CountryISOCodeNumeric = reader["country_code_iso_numeric"].ToString();

                    countriesList.Add(countryObj);
                }
            }
            return countriesList;
        }
    }
}