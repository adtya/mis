﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SchemeCodeAndSchemeRiskRatingRelationDB
    {
        protected internal List<SchemeCodeAndSchemeRiskRatingRelationManager> GetSchemesWithSchemeRiskRatingData()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.scheme_uuid, ");
            sqlCommand.Append("obj1.scheme_code, ");
            sqlCommand.Append("obj1.scheme_name, ");
            sqlCommand.Append("obj2.scheme_code_scheme_risk_rating_relationship_uuid, ");
            sqlCommand.Append("obj2.scheme_risk_rating_uuid, ");
            sqlCommand.Append("obj3.scheme_risk_rating ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_schemes obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_scheme_code_scheme_risk_rating_relationship obj2 ON obj2.scheme_uuid = obj1.scheme_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_scheme_risk_rating obj3 ON obj2.scheme_risk_rating_uuid = obj3.scheme_risk_rating_uuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("scheme_name ");
            sqlCommand.Append(";");
     
            List<SchemeCodeAndSchemeRiskRatingRelationManager> schemeCodeAndSchemeRiskRatingRelationManagerList = new List<SchemeCodeAndSchemeRiskRatingRelationManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    SchemeCodeAndSchemeRiskRatingRelationManager schemeCodeAndSchemeRiskRatingRelationManagerObj = new SchemeCodeAndSchemeRiskRatingRelationManager();

                    schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeManager.SchemeGuid = new Guid(reader["scheme_uuid"].ToString());
                    schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeManager.SchemeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["scheme_code"].ToString());
                    schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeManager.SchemeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["scheme_name"].ToString());

                    schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeCodeSchemeRiskRatingRelationGuid = string.IsNullOrEmpty(reader["scheme_code_scheme_risk_rating_relationship_uuid"].ToString()) ? new Guid() : new Guid(reader["scheme_code_scheme_risk_rating_relationship_uuid"].ToString());
                    schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeRiskRatingManager.SchemeRiskRatingGuid = string.IsNullOrEmpty(reader["scheme_risk_rating_uuid"].ToString()) ? new Guid() : new Guid(reader["scheme_risk_rating_uuid"].ToString());
                    schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeRiskRatingManager.SchemeRiskRating = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["scheme_risk_rating"].ToString());

                    schemeCodeAndSchemeRiskRatingRelationManagerList.Add(schemeCodeAndSchemeRiskRatingRelationManagerObj);
                }
            }
            return schemeCodeAndSchemeRiskRatingRelationManagerList;
        }

        protected internal SchemeCodeAndSchemeRiskRatingRelationManager GetSchemeRiskRatingNumber(Guid assetGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.scheme_risk_rating_number ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_scheme_risk_rating obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_scheme_code_scheme_risk_rating_relationship obj2 ON obj2.scheme_risk_rating_uuid = obj1.scheme_risk_rating_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_assets obj3 ON obj2.scheme_uuid = obj3.scheme_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj3.asset_uuid=:assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetGuid;

            SchemeCodeAndSchemeRiskRatingRelationManager schemeCodeAndSchemeRiskRatingRelationManagerObj = new SchemeCodeAndSchemeRiskRatingRelationManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(),arParams))
            {
                while (reader.Read())
                {
                    schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeRiskRatingManager.SchemeRiskRatingNumber = Convert.ToInt16(reader["scheme_risk_rating_number"].ToString());
                }
            }
            return schemeCodeAndSchemeRiskRatingRelationManagerObj;
        }

        protected internal int AddSchemeCodeAndSchemeRiskRatingRelation(SchemeCodeAndSchemeRiskRatingRelationManager schemeCodeAndSchemeRiskRatingRelationManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_scheme_code_scheme_risk_rating_relationship ");
            sqlCommand.Append("( ");
            sqlCommand.Append("scheme_code_scheme_risk_rating_relationship_uuid, ");
            sqlCommand.Append("scheme_risk_rating_uuid, ");
            sqlCommand.Append("scheme_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":schemeCodeSchemeRiskRatingRelationGuid, ");
            sqlCommand.Append(":schemeRiskRatingGuid, ");
            sqlCommand.Append(":schemeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("schemeCodeSchemeRiskRatingRelationGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = schemeCodeAndSchemeRiskRatingRelationManager.SchemeCodeSchemeRiskRatingRelationGuid;

            arParams[1] = new NpgsqlParameter("schemeRiskRatingGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = schemeCodeAndSchemeRiskRatingRelationManager.SchemeRiskRatingManager.SchemeRiskRatingGuid;

            arParams[2] = new NpgsqlParameter("schemeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = schemeCodeAndSchemeRiskRatingRelationManager.SchemeManager.SchemeGuid;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;
        }

        protected internal int UpdateSchemeCodeAndSchemeRiskRatingRelationData(Guid schemeCodeSchemeRiskRatingRelationGuid, Guid schemeRiskRatingGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_om_scheme_code_scheme_risk_rating_relationship ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("scheme_risk_rating_uuid= :schemeRiskRatingGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("scheme_code_scheme_risk_rating_relationship_uuid= :schemeCodeSchemeRiskRatingRelationGuid");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("schemeCodeSchemeRiskRatingRelationGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = schemeCodeSchemeRiskRatingRelationGuid;

            arParams[1] = new NpgsqlParameter("schemeRiskRatingGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = schemeRiskRatingGuid;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;
        }
    }
}