﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class WorkItemAndWorkItemSubHeadsRelationDB
    {
        protected internal int AddWorkItemAndWorkItemSubHeadRelation(WorkItemManager workItemManager)
        {
            int result = 0;

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_work_item_and_work_item_sub_heads_relation ");
            sqlCommand.Append("( ");
            sqlCommand.Append("work_item_and_work_item_sub_heads_relation_uuid, ");
            sqlCommand.Append("work_item_uuid, ");
            sqlCommand.Append("work_item_sub_head_uuid, ");
            sqlCommand.Append("work_item_sub_head_sequence ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            if (workItemManager.WorkItemSubHeadManager.Count > 0)
            {
                foreach (WorkItemSubHeadManager workItemSubHeadManagerObj in workItemManager.WorkItemSubHeadManager)
                {
                    int index = workItemManager.WorkItemSubHeadManager.IndexOf(workItemSubHeadManagerObj);

                    //int index = Array.IndexOf(workItemSubHeadsGuidList, workItemSubHeadsGuidObj);

                    sqlCommand.Append("( ");

                    sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                    sqlCommand.Append("'" + workItemManager.WorkItemGuid.ToString() + "', ");
                    sqlCommand.Append("'" + workItemSubHeadManagerObj.WorkItemSubHeadGuid.ToString() + "', ");
                    //sqlCommand.Append("'" + workItemSubHeadManagerObj.WorkItemSubHeadSequence + "' ");
                    sqlCommand.Append(workItemSubHeadManagerObj.WorkItemSubHeadSequence + " ");

                    if (index == workItemManager.WorkItemSubHeadManager.Count - 1)
                    {
                        sqlCommand.Append(") ");
                    }
                    else
                    {
                        sqlCommand.Append("), ");
                    }
                }

                sqlCommand.Append(";");

                result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());
            }

            return result;

        }

        protected internal int UpdateWorkItemAndWorkItemSubHeadRelation(WorkItemManager workItemManager)
        {
            DeleteWorkItemAndWorkItemSubHeadRelation(workItemManager);

            return AddWorkItemAndWorkItemSubHeadRelation(workItemManager);
        }

        protected internal int DeleteWorkItemAndWorkItemSubHeadRelation(WorkItemManager workItemManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_item_and_work_item_sub_heads_relation ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("work_item_uuid = :workItemGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("workItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = workItemManager.WorkItemGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}