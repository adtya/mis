﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SubChainageAndWorkItemsRelationManager
    {
        private Guid subChainageAndWorkItemRelationGuid;
        private SubChainageManager subChainageManager;
        private WorkItemManager workItemManager;




        public SubChainageManager SubChainageManager
        {
            get
            {
                if (subChainageManager == null)
                {
                    subChainageManager = new SubChainageManager();
                }
                return subChainageManager;
            }
            set
            {
                subChainageManager = value;
            }
        }

        public WorkItemManager WorkItemManager
        {
            get
            {
                if (workItemManager == null)
                {
                    workItemManager = new WorkItemManager();
                }
                return workItemManager;
            }
            set
            {
                workItemManager = value;
            }
        }

        public Guid SubChainageAndWorkItemRelationGuid
        {
            get
            {
                return subChainageAndWorkItemRelationGuid;
            }
            set
            {
                subChainageAndWorkItemRelationGuid = value;
            }
        }
    }
}