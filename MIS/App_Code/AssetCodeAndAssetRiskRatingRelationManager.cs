﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class AssetCodeAndAssetRiskRatingRelationManager
    {
        #region Private Members

        private Guid assetCodeAssetRiskRatingRelationGuid;
        private AssetManager assetManager;
        private AssetRiskRatingManager assetRiskRatingManager;

        #endregion


        #region Public Getter/Setter Properties

        public Guid AssetCodeAssetRiskRatingRelationGuid
        {
            get
            {
                return assetCodeAssetRiskRatingRelationGuid;
            }
            set
            {
                assetCodeAssetRiskRatingRelationGuid = value;
            }
        }

        public AssetManager AssetManager
        {
            get
            {
                if (assetManager == null)
                {
                    assetManager = new AssetManager();
                }
                return assetManager;
            }
            set
            {
                assetManager = value;
            }
        }

        public AssetRiskRatingManager AssetRiskRatingManager
        {
            get
            {
                if (assetRiskRatingManager == null)
                {
                    assetRiskRatingManager = new AssetRiskRatingManager();
                }
                return assetRiskRatingManager;
            }
            set
            {
                assetRiskRatingManager = value;
            }
        }

        #endregion

    }
}