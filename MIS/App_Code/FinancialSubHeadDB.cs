﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class FinancialSubHeadDB
    {
        protected internal FinancialSubHeadManager GetFinancialSubHead(Guid financialSubHeadGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.financial_sub_head_uuid, ");
            sqlCommand.Append("obj1.financial_sub_head_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_financial_sub_heads obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.financial_sub_head_uuid = :financialSubHeadGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("financialSubHeadGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = financialSubHeadGuid;

            FinancialSubHeadManager financialSubHeadManagerObj = new FinancialSubHeadManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    financialSubHeadManagerObj.FinancialSubHeadGuid = new Guid(reader["financial_sub_head_uuid"].ToString());
                    financialSubHeadManagerObj.FinancialSubHeadName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["financial_sub_head_name"].ToString());
                }
            }

            return financialSubHeadManagerObj;
        }


        protected internal List<FinancialSubHeadManager> GetFinancialSubHeads()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.financial_sub_head_uuid, ");
            sqlCommand.Append("obj1.financial_sub_head_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_financial_sub_heads obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.financial_sub_head_name ");
            sqlCommand.Append(";");

            List<FinancialSubHeadManager> financialSubHeadManagerList = new List<FinancialSubHeadManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    FinancialSubHeadManager financialSubHeadManagerObj = new FinancialSubHeadManager();

                    financialSubHeadManagerObj.FinancialSubHeadGuid = new Guid(reader["financial_sub_head_uuid"].ToString());
                    financialSubHeadManagerObj.FinancialSubHeadName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["financial_sub_head_name"].ToString());

                    financialSubHeadManagerList.Add(financialSubHeadManagerObj);
                }
            }

            return financialSubHeadManagerList;
        }


        protected internal bool CheckFinancialSubHeadExistence(string subHeadName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_financial_sub_heads obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.financial_sub_head_name = :subHeadName ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("subHeadName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(subHeadName);

            bool result = false;

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    result = true;
                }
            }

            return result;
        }


        protected internal int AddFinancialSubHead(Guid financialSubHeadGuid, string financialSubHeadName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_financial_sub_heads ");
            sqlCommand.Append("( ");
            sqlCommand.Append("financial_sub_head_uuid, ");
            sqlCommand.Append("financial_sub_head_name ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":financialSubHeadGuid, ");
            sqlCommand.Append(":financialSubHeadName ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("financialSubHeadGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = financialSubHeadGuid;

            arParams[1] = new NpgsqlParameter("financialSubHeadName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(financialSubHeadName);

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }


        protected internal int UpdateFinancialSubHead(Guid financialSubHeadGuid, string financialSubHeadName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_project_financial_sub_heads ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("financial_sub_head_name = :financialSubHeadName ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("financial_sub_head_uuid = :financialSubHeadGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("financialSubHeadGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = financialSubHeadGuid;

            arParams[1] = new NpgsqlParameter("financialSubHeadName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(financialSubHeadName);

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }


        protected internal int DeleteFinancialSubHead(Guid financialSubHeadGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_financial_sub_heads ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("financial_sub_head_uuid = :financialSubHeadGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("financialSubHeadGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = financialSubHeadGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

    }
}