﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SluiceTypeManager
    {
         #region Private Members

        private Guid sluiceTypeGuid;
        private string sluiceTypeName;
       
       #endregion


        #region Public Constructors

        public SluiceTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid SluiceTypeGuid
        {
            get
            {
                return sluiceTypeGuid;
            }
            set
            {
                sluiceTypeGuid = value;
            }
        }

        public string SluiceTypeName
        {
            get
            {
                return sluiceTypeName;
            }
            set
            {
                sluiceTypeName = value;
            }
        }        

        #endregion
    }
}