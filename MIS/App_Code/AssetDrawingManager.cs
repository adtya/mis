﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class AssetDrawingManager
    {
        #region Private Members
        private Guid assetDrawingGuid;
        private string drawingFileName;
        private DateTime drawingFileUploadDate;
        private string drawingFileUrl;
     

        #endregion

        #region Public Getter/Setter Properties

        public Guid AssetDrawingGuid
        {
            get
            {
                return assetDrawingGuid;
            }
            set
            {
                assetDrawingGuid = value;
            }
        }

        public string DrawingFileName
        {
            get
            {
                return drawingFileName;
            }
            set
            {
                drawingFileName = value;
            }
        }

        public DateTime DrawingFileUploadDate
        {
            get
            {
                return drawingFileUploadDate;
            }
            set
            {
                drawingFileUploadDate = value;
            }
        }

        public string DrawingFileUrl
        {
            get
            {
                return drawingFileUrl;
            }
            set
            {
                drawingFileUrl = value;
            }
        }

        #endregion
    }
}