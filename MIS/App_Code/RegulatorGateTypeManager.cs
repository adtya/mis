﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RegulatorGateTypeManager
    {
        #region Private Members

        private Guid regulatorGateTypeGuid;
        private string regulatorGateTypeName;
       
       #endregion


        #region Public Constructors

        public RegulatorGateTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid RegulatorGateTypeGuid
        {
            get
            {
                return regulatorGateTypeGuid;
            }
            set
            {
                regulatorGateTypeGuid = value;
            }
        }

        public string RegulatorGateTypeName
        {
            get
            {
                return regulatorGateTypeName;
            }
            set
            {
                regulatorGateTypeName = value;
            }
        }        

        #endregion
    }
}