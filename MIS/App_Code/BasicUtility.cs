﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public static class BasicUtility
    {
        public static string IndexToColumn(int index)
        {
            const int ColumnBase = 26;
            const int DigitMax = 7; // ceil(log26(Int32.Max))
            const string Digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            if (index <= 0)
            {
                throw new IndexOutOfRangeException("index must be a positive number");
            }

            if (index <= ColumnBase)
            {
                return Digits[index - 1].ToString();
            }

            var sb = new StringBuilder().Append(' ', DigitMax);
            var current = index;
            var offset = DigitMax;
            while (current > 0)
            {
                sb[--offset] = Digits[--current % ColumnBase];
                current /= ColumnBase;
            }
            return sb.ToString(offset, DigitMax - offset);
        }

        public static string ToRomanUpper(int number)
        {
            if ((number < 0) || (number > 3999))
                throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");

            if (number < 1)
                return string.Empty;

            if (number >= 1000)
                return "M" + ToRomanUpper(number - 1000);

            if (number >= 900)
                return "CM" + ToRomanUpper(number - 900); //EDIT: i've typed 400 instead 900

            if (number >= 500)
                return "D" + ToRomanUpper(number - 500);

            if (number >= 400)
                return "CD" + ToRomanUpper(number - 400);

            if (number >= 100)
                return "C" + ToRomanUpper(number - 100);

            if (number >= 90)
                return "XC" + ToRomanUpper(number - 90);

            if (number >= 50)
                return "L" + ToRomanUpper(number - 50);

            if (number >= 40)
                return "XL" + ToRomanUpper(number - 40);

            if (number >= 10)
                return "X" + ToRomanUpper(number - 10);

            if (number >= 9)
                return "IX" + ToRomanUpper(number - 9);

            if (number >= 5)
                return "V" + ToRomanUpper(number - 5);

            if (number >= 4)
                return "IV" + ToRomanUpper(number - 4);

            if (number >= 1)
                return "I" + ToRomanUpper(number - 1);

            throw new ArgumentOutOfRangeException("something bad happened");
        }

        public static string ToRomanLower(int number)
        {
            return ToRomanUpper(number).ToLower();
        }
    }
}