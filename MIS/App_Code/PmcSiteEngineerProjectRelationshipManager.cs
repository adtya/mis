﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class PmcSiteEngineerProjectRelationshipManager
    {
        private Guid pmcSiteEngineerProjectRelationshipGuid;
        private ProjectManager projectManager;
        private string siteEngineerName;

        public Guid PmcSiteEngineerProjectRelationshipGuid
        {
            get
            {
                return pmcSiteEngineerProjectRelationshipGuid;
            }
            set
            {
                pmcSiteEngineerProjectRelationshipGuid = value;
            }
        }

        public ProjectManager ProjectManager
        {
            get
            {
                if (projectManager == null)
                {
                    projectManager = new ProjectManager();
                }
                return projectManager;
            }
            set
            {
                projectManager = value;
            }
        }

        public string SiteEngineerName
        {
            get
            {
                return siteEngineerName;
            }
            set
            {
                siteEngineerName = value;
            }
        }
    }
}