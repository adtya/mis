﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SpurShapeTypeManager
    {
         #region Private Members

        private Guid shapeTypeGuid;
        private string shapeTypeName;
       
       #endregion


        #region Public Constructors

        public SpurShapeTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid ShapeTypeGuid
        {
            get
            {
                return shapeTypeGuid;
            }
            set
            {
                shapeTypeGuid = value;
            }
        }

        public string ShapeTypeName
        {
            get
            {
                return shapeTypeName;
            }
            set
            {
                shapeTypeName = value;
            }
        }        

        #endregion
    }
}