﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class AssetTypeAndAssetQuantityParameterRelationManager
    {
        #region Private Members

        private Guid assetTypeAssetQuantityParameterRelationGuid;
        private AssetManager assetManager;
        //private AssetTypeManager assetTypeManager;
        private AssetQuantityParameterManager assetQuantityParameterManager;

        #endregion


        #region Public Getter/Setter Properties

        public Guid AssetTypeAssetQuantityParameterRelationGuid
        {
            get
            {
                return assetTypeAssetQuantityParameterRelationGuid;
            }
            set
            {
                assetTypeAssetQuantityParameterRelationGuid = value;
            }
        }

        public AssetManager AssetManager
        {
            get
            {
                if (assetManager == null)
                {
                    assetManager = new AssetManager();
                }
                return assetManager;
            }
            set
            {
                assetManager = value;
            }
        }

        //public AssetTypeManager AssetTypeManager
        //{
        //    get
        //    {
        //        if (assetTypeManager == null)
        //        {
        //            assetTypeManager = new AssetTypeManager();
        //        }
        //        return assetTypeManager;
        //    }
        //    set
        //    {
        //        assetTypeManager = value;
        //    }
        //}

        public AssetQuantityParameterManager AssetQuantityParameterManager
        {
            get
            {
                if (assetQuantityParameterManager == null)
                {
                    assetQuantityParameterManager = new AssetQuantityParameterManager();
                }
                return assetQuantityParameterManager;
            }
            set
            {
                assetQuantityParameterManager = value;
            }
        }

        #endregion
    }
}