﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SluiceManager
    {
        #region Private Members

        private Guid sluiceGuid;               
        private int ventNumberSluice;
        private double ventHeightSluice;
        private double ventWidthSluice;
        private double ventDiameterSluice;   
       // private AssetManager assetManager;
        private SluiceTypeManager sluiceTypeManager;
        private List<GateManager> gateManagerList;
 
        #endregion


        #region Public Constructors

        public SluiceManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid SluiceGuid
        {
            get
            {
                return sluiceGuid;
            }
            set
            {
                sluiceGuid = value;
            }
        }       

        public int VentNumberSluice
        {
            get
            {
                return ventNumberSluice;
            }
            set
            {
                ventNumberSluice = value;
            }
        }

        public double VentHeightSluice
        {
            get
            {
                return ventHeightSluice;
            }
            set
            {
                ventHeightSluice = value;
            }
        }

        public double VentWidthSluice
        {
            get
            {
                return ventWidthSluice;
            }
            set
            {
                ventWidthSluice = value;
            }
        }


        public double VentDiameterSluice
        {
            get
            {
                return ventDiameterSluice;
            }
            set
            {
                ventDiameterSluice = value;
            }
        }


        public SluiceTypeManager SluiceTypeManager
        {
            get
            {
                if (sluiceTypeManager == null)
                {
                    sluiceTypeManager = new SluiceTypeManager();
                }
                return sluiceTypeManager;
            }
            set
            {
                sluiceTypeManager = value;
            }
        }      

        public List<GateManager> GateManagerList
        {
            get
            {                
                return gateManagerList;
            }
            set
            {
                gateManagerList = value;
            }
        }

        #endregion
    }
}