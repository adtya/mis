﻿using NodaMoney;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class FieldSurveyDB
    {
        protected internal List<FieldSurveyManager> GetFieldSurveyData(Guid assetGuid, Guid circleGuid, string fiscalYear)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT DISTINCT ");
            sqlCommand.Append("obj1.field_survey_uuid, ");            
            sqlCommand.Append("obj1.earth_volume_quantity, ");
            sqlCommand.Append("obj1.field_survey_work_item_cost, ");
            sqlCommand.Append("obj2.routine_work_item_uuid, ");
            sqlCommand.Append("obj2.routine_work_item_type_code, ");
            sqlCommand.Append("obj2.routine_work_item_short_desc, ");
            sqlCommand.Append("obj3.routine_work_item_rate ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_field_survey obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_routine_work_items obj2 ON obj2.routine_work_item_uuid = obj1.routine_work_item_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_routine_work_item_rate obj3 ON obj1.routine_work_item_uuid = obj3.routine_work_item_uuid ");
            sqlCommand.Append("AND obj3.circle_uuid = :circleGuid ");
            sqlCommand.Append("AND obj3.routine_work_item_rate_fiscal_year = :fiscalYear ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.asset_uuid  = :assetGuid ");          
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetGuid;

            arParams[1] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = circleGuid;

            arParams[2] = new NpgsqlParameter("fiscalYear", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = fiscalYear;

            List<FieldSurveyManager> fieldSurveyManagerList = new List<FieldSurveyManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    FieldSurveyManager fieldSurveyManagerObj = new FieldSurveyManager();

                    RoutineWorkItemRateManager routineWorkItemRateManagerObj = new RoutineWorkItemRateManager();
                    routineWorkItemRateManagerObj.RoutineWorkItemRate = string.IsNullOrEmpty(reader["routine_work_item_rate"].ToString()) ? Money.Parse("0") : Money.Parse(reader["routine_work_item_rate"].ToString());

                    fieldSurveyManagerObj.FieldSurveyGuid = new Guid(reader["field_survey_uuid"].ToString());
                    fieldSurveyManagerObj.EarthVolumeQuantity = Convert.ToDouble(reader["earth_volume_quantity"].ToString());
                    fieldSurveyManagerObj.FieldSurveyWorkItemCost = string.IsNullOrEmpty(reader["field_survey_work_item_cost"].ToString()) ? Money.Parse("0") : Money.Parse(reader["field_survey_work_item_cost"].ToString());
                    fieldSurveyManagerObj.RoutineWorkItemManager.RoutineWorkItemGuid = new Guid(reader["routine_work_item_uuid"].ToString());
                    fieldSurveyManagerObj.RoutineWorkItemManager.RoutineWorkItemTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_type_code"].ToString());
                    fieldSurveyManagerObj.RoutineWorkItemManager.ShortDescription = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_short_desc"].ToString());
                    fieldSurveyManagerObj.RoutineWorkItemManager.RoutineWorkItemRateManagerList = new List<RoutineWorkItemRateManager>();

                    fieldSurveyManagerObj.RoutineWorkItemManager.RoutineWorkItemRateManagerList.Add(routineWorkItemRateManagerObj);

                    fieldSurveyManagerList.Add(fieldSurveyManagerObj);
                }
            }
            return fieldSurveyManagerList;
        }

        protected internal int SaveFieldSurveyData(FieldSurveyManager fieldSurveyManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_field_survey ");
            sqlCommand.Append("( ");
            sqlCommand.Append("field_survey_uuid, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("routine_work_item_uuid, ");
            sqlCommand.Append("earth_volume_quantity, ");
            sqlCommand.Append("field_survey_work_item_cost, ");
            sqlCommand.Append("fiscal_year, ");
            sqlCommand.Append("circle_uuid ");      
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":fieldSurveyGuid, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":routineWorkItemGuid, ");
            sqlCommand.Append(":earthVolumeQuantity, ");
            sqlCommand.Append(":fieldSurveyWorkItemCost, ");
            sqlCommand.Append(":fiscalYear, ");
            sqlCommand.Append(":circleGuid ");  
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[7];

            arParams[0] = new NpgsqlParameter("fieldSurveyGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = fieldSurveyManager.FieldSurveyGuid;

            arParams[1] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = fieldSurveyManager.AssetManager.AssetGuid;

            arParams[2] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = fieldSurveyManager.RoutineWorkItemManager.RoutineWorkItemGuid;

            arParams[3] = new NpgsqlParameter("earthVolumeQuantity", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = fieldSurveyManager.EarthVolumeQuantity;

            arParams[4] = new NpgsqlParameter("fieldSurveyWorkItemCost", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = fieldSurveyManager.FieldSurveyWorkItemCost.Amount;

            arParams[5] = new NpgsqlParameter("fiscalYear", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = fieldSurveyManager.SystemVariablesManager.FiscalYear;

            arParams[6] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = fieldSurveyManager.SystemVariablesManager.CircleManager.CircleGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateFieldSurveyData(Guid fieldSurveyGuid, Double earthVolumeQuantity, Money fieldSurveyWorkItemCost)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_om_field_survey ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("earth_volume_quantity= :earthVolumeQuantity, ");
            sqlCommand.Append("field_survey_work_item_cost= :fieldSurveyWorkItemCost ");            
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("field_survey_uuid= :fieldSurveyGuid");
            sqlCommand.Append(";"); 

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("fieldSurveyGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = fieldSurveyGuid;

            arParams[1] = new NpgsqlParameter("earthVolumeQuantity", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = earthVolumeQuantity;

            arParams[2] = new NpgsqlParameter("fieldSurveyWorkItemCost", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = fieldSurveyWorkItemCost.Amount;            

            return NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);
        }

        protected internal int DeleteFieldSurveyData(Guid fieldSurveyGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_field_survey ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("field_survey_uuid = :fieldSurveyGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("fieldSurveyGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = fieldSurveyGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}