﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RepairAnalysisStatusManger
    {
        #region Private Members

        private Guid repairAnalysisStatusGuid;
        private string repairAnalysisStatus;
   
        #endregion

        #region Public Getter/Setter Properties

        public Guid RepairAnalysisStatusGuid
        {
            get
            {
                return repairAnalysisStatusGuid;
            }
            set
            {
                repairAnalysisStatusGuid = value;
            }
        }

        public string RepairAnalysisStatus
        {
            get
            {
                return repairAnalysisStatus;
            }
            set
            {
                repairAnalysisStatus = value;
            }
        }       

        #endregion
    }
}