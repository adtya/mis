﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class WeirDB
    {
        protected internal int AddWeirData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_weir ");
            sqlCommand.Append("( ");
            sqlCommand.Append("weir_uuid, ");
            sqlCommand.Append("invert_level, ");
            sqlCommand.Append("crest_top_level, ");
            sqlCommand.Append("crest_top_width, ");            
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("weir_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":weirGuid, ");
            sqlCommand.Append(":invertLevel, ");
            sqlCommand.Append(":crestTopLevel, ");
            sqlCommand.Append(":crestTopWidth, ");            
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":weirTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[7];

            arParams[0] = new NpgsqlParameter("weirGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("invertLevel", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.WeirManager.InvertLevel;

            arParams[2] = new NpgsqlParameter("dsElevation", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.CanalManager.DSElevation;

            arParams[3] = new NpgsqlParameter("crestTopLevel", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.WeirManager.CrestTopLevel;

            arParams[4] = new NpgsqlParameter("crestTopWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.WeirManager.CrestTopWidth;

            arParams[5] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.AssetGuid;

            arParams[6] = new NpgsqlParameter("weirTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.WeirManager.WeirTypeManager.WeirTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateWeirData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_weir ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("invert_level= :invertLevel, ");
            sqlCommand.Append("crest_top_level= :crestTopLevel, ");
            sqlCommand.Append("crest_top_width= :crestTopWidth, ");
            sqlCommand.Append("weir_type_uuid= :weirTypeGuid ");            
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[7];

            arParams[0] = new NpgsqlParameter("weirGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.WeirManager.WeirGuid;

            arParams[1] = new NpgsqlParameter("invertLevel", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.WeirManager.InvertLevel;

            arParams[2] = new NpgsqlParameter("dsElevation", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.CanalManager.DSElevation;

            arParams[3] = new NpgsqlParameter("crestTopLevel", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.WeirManager.CrestTopLevel;

            arParams[4] = new NpgsqlParameter("crestTopWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.WeirManager.CrestTopWidth;

            arParams[5] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.AssetGuid;

            arParams[6] = new NpgsqlParameter("weirTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.WeirManager.WeirTypeManager.WeirTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
        
    }
}