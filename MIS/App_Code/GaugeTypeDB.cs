﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class GaugeTypeDB
    {
        protected internal List<GaugeTypeManager> GetGaugeTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("gauge_type_uuid, ");
            sqlCommand.Append("gauge_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_gauge_type ");
            sqlCommand.Append("ORDER BY gauge_type_name ");
            sqlCommand.Append(";");

            List<GaugeTypeManager> gaugeTypeManagerList = new List<GaugeTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    GaugeTypeManager gaugeTypeManagerObj = new GaugeTypeManager();

                    gaugeTypeManagerObj.GaugeTypeGuid = new Guid(reader["gauge_type_uuid"].ToString());
                    gaugeTypeManagerObj.GaugeTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["gauge_type_name"].ToString());

                    gaugeTypeManagerList.Add(gaugeTypeManagerObj);
                }
            }

            return gaugeTypeManagerList;
        }
    }
}