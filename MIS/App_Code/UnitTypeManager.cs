﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class UnitTypeManager
    {
        private Guid unitTypeGuid;
        private string unitTypeName;
        private List<UnitManager> unitManagerList;

        public Guid UnitTypeGuid
        {
            get
            {
                return unitTypeGuid;
            }
            set
            {
                unitTypeGuid = value;
            }
        }

        public string UnitTypeName
        {
            get
            {
                return unitTypeName;
            }
            set
            {
                unitTypeName = value;
            }
        }

        public List<UnitManager> UnitManagerList
        {
            get
            {
                return unitManagerList;
            }
            set
            {
                unitManagerList = value;
            }
        }
    }
}