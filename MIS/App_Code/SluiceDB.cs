﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SluiceDB
    {
        protected internal int AddSluiceData(AssetManager assetManager)//,int numberOfGates,double height, double width,double diameter, DateTime installDate,Guid gateTypeGuid,Guid gateConstructionMaterialGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_sluice ");
            sqlCommand.Append("( ");
            sqlCommand.Append("sluice_uuid, ");
            sqlCommand.Append("vent_number, ");
            sqlCommand.Append("vent_diameter, ");
            sqlCommand.Append("vent_height, ");
            sqlCommand.Append("vent_width, ");            
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("sluice_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":sluiceGuid, ");
            sqlCommand.Append(":ventNumberSluice, ");
            sqlCommand.Append(":ventDiameterSluice, ");
            sqlCommand.Append(":ventHeightSluice, ");
            sqlCommand.Append(":ventWidthSluice, ");            
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":sluiceTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[7];

            arParams[0] = new NpgsqlParameter("sluiceGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("ventNumberSluice", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.SluiceManager.VentNumberSluice;

            arParams[2] = new NpgsqlParameter("ventDiameterSluice", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.SluiceManager.VentDiameterSluice;

            arParams[3] = new NpgsqlParameter("ventHeightSluice", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.SluiceManager.VentHeightSluice;

            arParams[4] = new NpgsqlParameter("ventWidthSluice", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.SluiceManager.VentWidthSluice;         

            arParams[5] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.AssetGuid;

            arParams[6] = new NpgsqlParameter("sluiceTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.SluiceManager.SluiceTypeManager.SluiceTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            if (result == 1)
            {
                new GateDB().AddGateForSluice(assetManager);
            }          

            return result;
        }

        protected internal int UpdateSluiceData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_sluice ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("vent_number= :ventNumberSluice, ");
            sqlCommand.Append("vent_diameter= :ventDiameterSluice, ");
            sqlCommand.Append("vent_height= :ventHeightSluice, ");
            sqlCommand.Append("vent_width= :ventWidthSluice, ");
            sqlCommand.Append("sluice_type_uuid= :sluiceTypeGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[7];

            arParams[0] = new NpgsqlParameter("sluiceGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.SluiceManager.SluiceGuid;

            arParams[1] = new NpgsqlParameter("ventNumberSluice", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.SluiceManager.VentNumberSluice;

            arParams[2] = new NpgsqlParameter("ventDiameterSluice", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.SluiceManager.VentDiameterSluice;

            arParams[3] = new NpgsqlParameter("ventHeightSluice", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.SluiceManager.VentHeightSluice;

            arParams[4] = new NpgsqlParameter("ventWidthSluice", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.SluiceManager.VentWidthSluice;

            arParams[5] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.AssetGuid;

            arParams[6] = new NpgsqlParameter("sluiceTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.SluiceManager.SluiceTypeManager.SluiceTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            if (result == 1)
            {
                new GateDB().UpdateGateDataForSluice(assetManager);
            }

            return result;
           
        }
        
    }
}