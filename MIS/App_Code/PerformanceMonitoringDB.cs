﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class PerformanceMonitoringDB
    {
        protected internal int AddPerformanceMontoringData(PerformanceMonitoringManager performanceMonitoringManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_performance_monitoring ");
            sqlCommand.Append("( ");
            sqlCommand.Append("performance_monitoring_uuid, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("current_monitoring_date, ");
            sqlCommand.Append("overall_status_uuid, ");
            sqlCommand.Append("inspector_name, ");
            sqlCommand.Append("inspector_position_uuid, ");
            sqlCommand.Append("inspection_date, ");
            sqlCommand.Append("general_comments ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":performanceMonitoringGuid, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":currentMonitoringDate, ");
            sqlCommand.Append(":overallStatusGuid, ");
            sqlCommand.Append(":inspectorName, ");
            sqlCommand.Append(":inspectorPositionGuid, ");
            sqlCommand.Append(":inspectionDate, ");
            sqlCommand.Append(":generalComments ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[8];

            arParams[0] = new NpgsqlParameter("performanceMonitoringGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = performanceMonitoringManager.PerformanceMonitoringGuid;

            arParams[1] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = performanceMonitoringManager.AssetManager.AssetGuid;

            arParams[2] = new NpgsqlParameter("currentMonitoringDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = performanceMonitoringManager.SystemVariablesManager.CurrentMonitoringDate;

            arParams[3] = new NpgsqlParameter("overallStatusGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = performanceMonitoringManager.OverallStatusManager.OverallStatusGuid;

            arParams[4] = new NpgsqlParameter("inspectorName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(performanceMonitoringManager.InspectorName);

            arParams[5] = new NpgsqlParameter("inspectorPositionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = performanceMonitoringManager.DesignationManager.DesignationGuid;

            arParams[6] = new NpgsqlParameter("inspectionDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = performanceMonitoringManager.InspectionDate;

            arParams[7] = new NpgsqlParameter("generalComments", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(performanceMonitoringManager.GeneralComments);

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal PerformanceMonitoringManager GetPerformanceDataByAssetUuid(Guid assetGuid, DateTime currentMonitoringDate)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.performance_monitoring_uuid, ");
            sqlCommand.Append("obj1.inspector_name, ");
            sqlCommand.Append("obj1.inspection_date, ");
            sqlCommand.Append("obj1.general_comments, ");
            sqlCommand.Append("obj2.asset_uuid, ");
            sqlCommand.Append("obj2.asset_code,  ");
            sqlCommand.Append("obj2.asset_name, ");
            sqlCommand.Append("obj3.asset_type_uuid, ");
            sqlCommand.Append("obj3.asset_type_code, ");
            sqlCommand.Append("obj4.overall_status_uuid, ");
            sqlCommand.Append("obj4.overall_status, ");
            sqlCommand.Append("obj5.designation_uuid, ");
            sqlCommand.Append("obj5.designation_name ");
            sqlCommand.Append("FROM mis_om_performance_monitoring obj1 ");
            sqlCommand.Append("LEFT JOIN mis_assets obj2 ON obj1.asset_uuid = obj2.asset_uuid ");
            sqlCommand.Append("LEFT JOIN mis_asset_type obj3 ON obj3.asset_type_uuid = obj2.asset_type_uuid ");
            sqlCommand.Append("LEFT JOIN mis_om_overall_status obj4 ON obj4.overall_status_uuid = obj1.overall_status_uuid ");
            sqlCommand.Append("LEFT JOIN mis_designations obj5 ON obj5.designation_uuid = obj1.inspector_position_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.asset_uuid = :assetGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.current_monitoring_date = :currentMonitoringDate ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetGuid;

            arParams[1] = new NpgsqlParameter("currentMonitoringDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = currentMonitoringDate;

            PerformanceMonitoringManager performanceMonitoringDataObj = new PerformanceMonitoringManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    performanceMonitoringDataObj.PerformanceMonitoringGuid = new Guid(reader["performance_monitoring_uuid"].ToString());
                    performanceMonitoringDataObj.InspectorName = reader["inspector_name"].ToString();
                    performanceMonitoringDataObj.InspectionDate = DateTime.Parse(reader["inspection_date"].ToString());
                    performanceMonitoringDataObj.GeneralComments = reader["general_comments"].ToString();

                    performanceMonitoringDataObj.AssetManager.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    performanceMonitoringDataObj.AssetManager.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_code"].ToString());
                    performanceMonitoringDataObj.AssetManager.AssetName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_name"].ToString());

                    performanceMonitoringDataObj.AssetManager.AssetTypeManager.AssetTypeGuid = new Guid(reader["asset_type_uuid"].ToString());
                    performanceMonitoringDataObj.AssetManager.AssetTypeManager.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_type_code"].ToString());

                    performanceMonitoringDataObj.OverallStatusManager.OverallStatusGuid = new Guid(reader["overall_status_uuid"].ToString());
                    performanceMonitoringDataObj.OverallStatusManager.OverallStatusName = reader["overall_status"].ToString();

                    performanceMonitoringDataObj.DesignationManager.DesignationGuid = new Guid(reader["designation_uuid"].ToString());
                    performanceMonitoringDataObj.DesignationManager.DesignationName = reader["designation_name"].ToString();
                                        
                }
            }

            return performanceMonitoringDataObj;

        }

        protected internal int UpdatePerformanceMontoringData(Guid performanceMonitoringGuid, PerformanceMonitoringManager performanceMonitoringManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_om_performance_monitoring ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("overall_status_uuid= :overallStatusGuid, ");
            sqlCommand.Append("inspector_name= :inspectorName, ");
            sqlCommand.Append("inspector_position_uuid= :designationGuid, ");
            sqlCommand.Append("inspection_date= :inspectionDate, ");
            sqlCommand.Append("general_comments= :generalComments ");           
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("performance_monitoring_uuid= :performanceMonitoringGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[6];

            arParams[0] = new NpgsqlParameter("performanceMonitoringGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = performanceMonitoringGuid;

            arParams[1] = new NpgsqlParameter("overallStatusGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = performanceMonitoringManager.OverallStatusManager.OverallStatusGuid;

            arParams[2] = new NpgsqlParameter("inspectorName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = performanceMonitoringManager.InspectorName;

            arParams[3] = new NpgsqlParameter("designationGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = performanceMonitoringManager.DesignationManager.DesignationGuid;

            arParams[4] = new NpgsqlParameter("inspectionDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = performanceMonitoringManager.InspectionDate;

            arParams[5] = new NpgsqlParameter("generalComments", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = performanceMonitoringManager.GeneralComments;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);
            if (rowsAffected == 1)
            {
                new MonitoringItemPerformanceDB().UpdateItemPerformance(performanceMonitoringManager);
            }
            return rowsAffected;
        }
    }
}