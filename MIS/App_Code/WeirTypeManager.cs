﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class WeirTypeManager
    {
        #region Private Members

        private Guid weirTypeGuid;
        private string weirTypeName;
       
       #endregion


        #region Public Constructors

        public WeirTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid WeirTypeGuid
        {
            get
            {
                return weirTypeGuid;
            }
            set
            {
                weirTypeGuid = value;
            }
        }

        public string WeirTypeName
        {
            get
            {
                return weirTypeName;
            }
            set
            {
                weirTypeName = value;
            }
        }        

        #endregion
    }
}