﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class DistrictManager
    {
        #region Data Members

        private Guid districtGuid;
        private string districtName;
        private string districtCode;

        #endregion




        #region Getters/Setters

        public Guid DistrictGuid
        {
            get
            {
                return districtGuid;
            }
            set
            {
                districtGuid = value;
            }
        }

        public string DistrictName
        {
            get
            {
                return districtName;
            }
            set
            {
                districtName = value;
            }
        }

        public string DistrictCode
        {
            get
            {
                return districtCode;
            }
            set
            {
                districtCode = value;
            }
        }

        #endregion
    }
}