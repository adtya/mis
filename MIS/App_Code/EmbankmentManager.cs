﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class EmbankmentManager
    {
        #region Private Members

        private Guid embankmentGuid;
        private double usElevation;
        private double dsElevation;
        private double crestWidth;
        private double length;
        private double csSlope1;
        private double rsSlope1;
        private double bermSlope1;
        private double platformWidth;
        private double averageHeight;
        private double csSlope2;
        private double rsSlope2;
        private double bermSlope2;                 
        //private AssetManager assetManager;
        private EmbankmentCrestTypeManager embankmentCrestTypeManager;
        private EmbankmentFillTypeManager embankmentFillTypeManager;

        #endregion


        #region Public Constructors

        public EmbankmentManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid EmbankmentGuid
        {
            get
            {
                return embankmentGuid;
            }
            set
            {
                embankmentGuid = value;
            }
        }

        public double UsElevation
        {
            get
            {
                return usElevation;
            }
            set
            {
                usElevation = value;
            }
        }

        public double DsElevation
        {
            get
            {
                return dsElevation;
            }
            set
            {
                dsElevation = value;
            }
        }

        public double CrestWidth
        {
            get
            {
                return crestWidth;
            }
            set
            {
                crestWidth = value;
            }
        }

        public double Length
        {
            get
            {
                return length;
            }
            set
            {
                length = value;
            }
        }

        public double CsSlope1
        {
            get
            {
                return csSlope1;
            }
            set
            {
                csSlope1 = value;
            }
        }

        public double RsSlope1
        {
            get
            {
                return rsSlope1;
            }
            set
            {
                rsSlope1 = value;
            }
        }

        public double BermSlope1
        {
            get
            {
                return bermSlope1;
            }
            set
            {
                bermSlope1 = value;
            }
        }

        public double PlatformWidth
        {
            get
            {
                return platformWidth;
            }
            set
            {
                platformWidth = value;
            }
        }

        public double AverageHeight
        {
            get
            {
                return averageHeight;
            }
            set
            {
                averageHeight = value;
            }
        }

        public double CsSlope2
        {
            get
            {
                return csSlope2;
            }
            set
            {
                csSlope2 = value;
            }
        }

        public double RsSlope2
        {
            get
            {
                return rsSlope2;
            }
            set
            {
                rsSlope2 = value;
            }
        }

        public double BermSlope2
        {
            get
            {
                return bermSlope2;
            }
            set
            {
                bermSlope2 = value;
            }
        }

        public EmbankmentCrestTypeManager EmbankmentCrestTypeManager
        {
            get
            {
                if (embankmentCrestTypeManager == null)
                {
                    embankmentCrestTypeManager = new EmbankmentCrestTypeManager();
                }
                return embankmentCrestTypeManager;
            }
            set
            {
                embankmentCrestTypeManager = value;
            }
        }

        public EmbankmentFillTypeManager EmbankmentFillTypeManager
        {
            get
            {
                if (embankmentFillTypeManager == null)
                {
                    embankmentFillTypeManager = new EmbankmentFillTypeManager();
                }
                return embankmentFillTypeManager;
            }
            set
            {
                embankmentFillTypeManager = value;
            }
        }

        //public AssetManager AssetManager
        //{
        //    get
        //    {
        //        if (assetManager == null)
        //        {
        //            assetManager = new AssetManager();
        //        }
        //        return assetManager;
        //    }
        //    set
        //    {
        //        assetManager = value;
        //    }
        //}

        #endregion
    }
}