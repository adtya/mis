﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RegulatorTypeManager
    {
        #region Private Members

        private Guid regulatorTypeGuid;
        private string regulatorTypeName;
       
       #endregion


        #region Public Constructors

        public RegulatorTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid RegulatorTypeGuid
        {
            get
            {
                return regulatorTypeGuid;
            }
            set
            {
                regulatorTypeGuid = value;
            }
        }

        public string RegulatorTypeName
        {
            get
            {
                return regulatorTypeName;
            }
            set
            {
                regulatorTypeName = value;
            }
        }        

        #endregion
    }
}