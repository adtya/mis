﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class AssetCodeAndAssetRiskRatingRelationDB
    {
        protected internal AssetCodeAndAssetRiskRatingRelationManager GetAssetRiskRatingForAssetCode(Guid assetGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_code_asset_risk_rating_relationship_uuid, ");
            sqlCommand.Append("obj1.asset_risk_rating_uuid, ");
            sqlCommand.Append("obj2.asset_risk_rating, ");
            sqlCommand.Append("obj2.asset_risk_rating_number ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_asset_code_asset_risk_rating_relationship obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_asset_risk_rating obj2 ON obj1.asset_risk_rating_uuid = obj2.asset_risk_rating_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.asset_uuid=:assetGuid");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetGuid;
            
            AssetCodeAndAssetRiskRatingRelationManager assetRiskRatingRelationForAssetCodeManagerObj = new AssetCodeAndAssetRiskRatingRelationManager();
            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {                   
                    assetRiskRatingRelationForAssetCodeManagerObj.AssetCodeAssetRiskRatingRelationGuid = string.IsNullOrEmpty(reader["asset_code_asset_risk_rating_relationship_uuid"].ToString()) ? new Guid() : new Guid(reader["asset_code_asset_risk_rating_relationship_uuid"].ToString());
                    assetRiskRatingRelationForAssetCodeManagerObj.AssetRiskRatingManager.AssetRiskRatingGuid = string.IsNullOrEmpty(reader["asset_risk_rating_uuid"].ToString()) ? new Guid() : new Guid(reader["asset_risk_rating_uuid"].ToString());
                    assetRiskRatingRelationForAssetCodeManagerObj.AssetRiskRatingManager.AssetRiskRating = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_risk_rating"].ToString());
                    assetRiskRatingRelationForAssetCodeManagerObj.AssetRiskRatingManager.AssetRiskRatingNumber = Convert.ToInt16(reader["asset_risk_rating_number"].ToString());
                    
                }
            }
            return assetRiskRatingRelationForAssetCodeManagerObj;
        }

        protected internal List<AssetCodeAndAssetRiskRatingRelationManager> GetAssetsWithAssetRiskRatingData()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_uuid, ");
            sqlCommand.Append("obj1.asset_code, ");
            sqlCommand.Append("obj1.asset_name, ");
            sqlCommand.Append("obj2.asset_code_asset_risk_rating_relationship_uuid, ");
            sqlCommand.Append("obj2.asset_risk_rating_uuid, ");
            sqlCommand.Append("obj3.asset_risk_rating ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_assets obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_asset_code_asset_risk_rating_relationship obj2 ON obj2.asset_uuid = obj1.asset_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_asset_risk_rating obj3 ON obj2.asset_risk_rating_uuid = obj3.asset_risk_rating_uuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("asset_name ");
            sqlCommand.Append(";");

            List<AssetCodeAndAssetRiskRatingRelationManager> assetCodeAndAssetRiskRatingRelationManagerList = new List<AssetCodeAndAssetRiskRatingRelationManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    AssetCodeAndAssetRiskRatingRelationManager assetCodeAndAssetRiskRatingRelationManagerObj = new AssetCodeAndAssetRiskRatingRelationManager();

                    assetCodeAndAssetRiskRatingRelationManagerObj.AssetManager.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    assetCodeAndAssetRiskRatingRelationManagerObj.AssetManager.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_code"].ToString());
                    assetCodeAndAssetRiskRatingRelationManagerObj.AssetManager.AssetName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_name"].ToString());

                    assetCodeAndAssetRiskRatingRelationManagerObj.AssetCodeAssetRiskRatingRelationGuid = string.IsNullOrEmpty(reader["asset_code_asset_risk_rating_relationship_uuid"].ToString()) ? new Guid() : new Guid(reader["asset_code_asset_risk_rating_relationship_uuid"].ToString());
                    assetCodeAndAssetRiskRatingRelationManagerObj.AssetRiskRatingManager.AssetRiskRatingGuid = string.IsNullOrEmpty(reader["asset_risk_rating_uuid"].ToString()) ? new Guid() : new Guid(reader["asset_risk_rating_uuid"].ToString());
                    assetCodeAndAssetRiskRatingRelationManagerObj.AssetRiskRatingManager.AssetRiskRating = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_risk_rating"].ToString());

                    assetCodeAndAssetRiskRatingRelationManagerList.Add(assetCodeAndAssetRiskRatingRelationManagerObj);
                }
            }
            return assetCodeAndAssetRiskRatingRelationManagerList;
        }

        protected internal int AddAssetCodeAndAssetRiskRatingRelation(AssetCodeAndAssetRiskRatingRelationManager assetCodeAndAssetRiskRatingRelationManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_asset_code_asset_risk_rating_relationship ");
            sqlCommand.Append("( ");
            sqlCommand.Append("asset_code_asset_risk_rating_relationship_uuid, ");
            sqlCommand.Append("asset_risk_rating_uuid, ");
            sqlCommand.Append("asset_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":assetCodeAssetRiskRatingRelationGuid, ");
            sqlCommand.Append(":assetRiskRatingGuid, ");
            sqlCommand.Append(":assetGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("assetCodeAssetRiskRatingRelationGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetCodeAndAssetRiskRatingRelationManager.AssetCodeAssetRiskRatingRelationGuid;

            arParams[1] = new NpgsqlParameter("assetRiskRatingGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetCodeAndAssetRiskRatingRelationManager.AssetRiskRatingManager.AssetRiskRatingGuid;

            arParams[2] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetCodeAndAssetRiskRatingRelationManager.AssetManager.AssetGuid;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;
        }

        protected internal int UpdateAssetCodeAndAssetRiskRatingRelationData(Guid assetCodeAssetRiskRatingRelationGuid, Guid assetRiskRatingGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_om_asset_code_asset_risk_rating_relationship ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("asset_risk_rating_uuid= :assetRiskRatingGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_code_asset_risk_rating_relationship_uuid= :assetCodeAssetRiskRatingRelationGuid");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("assetCodeAssetRiskRatingRelationGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetCodeAssetRiskRatingRelationGuid;

            arParams[1] = new NpgsqlParameter("assetRiskRatingGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetRiskRatingGuid;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;
        }
        
    }
}