﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class FremaaOfficerProgressDB
    {
        protected internal int AddFremaaOfficerProgress(SubChainagePhysicalProgressManager subChainagePhysicalProgressManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_sub_chainage_fremaa_officer_progress ");
            sqlCommand.Append("( ");
            sqlCommand.Append("fremaa_officer_progress_uuid, ");
            sqlCommand.Append("physical_progress_uuid, ");
            sqlCommand.Append("fremaa_officer_project_relationship_uuid, ");
            sqlCommand.Append("fremaa_officer_arose ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            foreach (FremaaOfficerProgressManager fremaaOfficerProgressManagerObj in subChainagePhysicalProgressManager.FremaaOfficerProgressManager)
            {
                int index = subChainagePhysicalProgressManager.FremaaOfficerProgressManager.IndexOf(fremaaOfficerProgressManagerObj);

                sqlCommand.Append("( ");

                sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                sqlCommand.Append("'" + subChainagePhysicalProgressManager.PhysicalProgressGuid + "', ");
                sqlCommand.Append("'" + fremaaOfficerProgressManagerObj.ProjectAndFremaaOfficersRelationManager.FremaaOfficerProjectRelationshipGuid + "', ");
                sqlCommand.Append("'" + fremaaOfficerProgressManagerObj.FremaaOfficerArose + "' ");

                if (index == subChainagePhysicalProgressManager.FremaaOfficerProgressManager.Count - 1)
                {
                    sqlCommand.Append(") ");
                }
                else
                {
                    sqlCommand.Append("), ");
                }
            }

            sqlCommand.Append(";");

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());

            return rowsAffected;
        }

        protected internal List<FremaaOfficerProgressManager> GetFremaaOfficerProgressWithFremaaOfficerName(Guid subChainagePhysicalProgressGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.*, ");
            sqlCommand.Append("obj2.fremaa_officer_engaged_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_sub_chainage_fremaa_officer_progress obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_project_project_and_fremaa_officers_relation obj2 ON obj2.project_and_fremaa_officers_relation_uuid = obj1.fremaa_officer_project_relationship_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.physical_progress_uuid = :subChainagePhysicalProgressGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("subChainagePhysicalProgressGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = subChainagePhysicalProgressGuid;

            List<FremaaOfficerProgressManager> fremaaOfficerProgressManagerList = new List<FremaaOfficerProgressManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    FremaaOfficerProgressManager fremaaOfficerProgressManagerObj = new FremaaOfficerProgressManager();

                    fremaaOfficerProgressManagerObj.FremaaOfficerProgressGuid = new Guid(reader["fremaa_officer_progress_uuid"].ToString());
                    fremaaOfficerProgressManagerObj.ProjectAndFremaaOfficersRelationManager.FremaaOfficerProjectRelationshipGuid = new Guid(reader["fremaa_officer_project_relationship_uuid"].ToString());
                    fremaaOfficerProgressManagerObj.ProjectAndFremaaOfficersRelationManager.FremaaOfficerName = reader["fremaa_officer_engaged_name"].ToString();
                    fremaaOfficerProgressManagerObj.FremaaOfficerArose = Boolean.Parse(reader["fremaa_officer_arose"].ToString());

                    fremaaOfficerProgressManagerList.Add(fremaaOfficerProgressManagerObj);
                }
            }

            return fremaaOfficerProgressManagerList;
        }

    }
}