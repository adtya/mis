﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineWorkItemManager
    {

        #region Private Members

        private Guid routineWorkItemGuid;       
        private string shortDescription;
        private string longDescription;
        private string routineWorkItemTypeCode;       
        private RoutineWorkItemClassManager routineWorkItemClassManager;
        private RoutineWorkItemTypeManager routineWorkItemTypeManager;
        private AssetQuantityParameterManager assetQuantityParameterManager;
        private List<RoutineWorkItemRateManager> routineWorkItemRateManagerList;

        #endregion



        #region Public Getter/Setter Properties

        public Guid RoutineWorkItemGuid
        {
            get
            {
                return routineWorkItemGuid;
            }
            set
            {
                routineWorkItemGuid = value;
            }
        }

        public string ShortDescription
        {
            get
            {
                return shortDescription;
            }
            set
            {
                shortDescription = value;
            }
        }

        public string LongDescription
        {
            get
            {
                return longDescription;
            }
            set
            {
                longDescription = value;
            }
        }

        public string RoutineWorkItemTypeCode
        {
            get
            {
                return routineWorkItemTypeCode;
            }
            set
            {
                routineWorkItemTypeCode = value;
            }
        }

        public RoutineWorkItemClassManager RoutineWorkItemClassManager
        {
            get
            {
                if (routineWorkItemClassManager == null)
                {
                    routineWorkItemClassManager = new RoutineWorkItemClassManager();
                }
                return routineWorkItemClassManager;
            }
            set
            {
                routineWorkItemClassManager = value;
            }
        }

        public RoutineWorkItemTypeManager RoutineWorkItemTypeManager
        {
            get
            {
                if (routineWorkItemTypeManager == null)
                {
                    routineWorkItemTypeManager = new RoutineWorkItemTypeManager();
                }
                return routineWorkItemTypeManager;
            }
            set
            {
                routineWorkItemTypeManager = value;
            }
        }

        public AssetQuantityParameterManager AssetQuantityParameterManager
        {
            get
            {
                if (assetQuantityParameterManager == null)
                {
                    assetQuantityParameterManager = new AssetQuantityParameterManager();
                }
                return assetQuantityParameterManager;
            }
            set
            {
                assetQuantityParameterManager = value;
            }
        }

        public List<RoutineWorkItemRateManager> RoutineWorkItemRateManagerList
        {
            get
            {
                return routineWorkItemRateManagerList;
            }
            set
            {
                routineWorkItemRateManagerList = value;
            }
        }
       
        #endregion
    }
}