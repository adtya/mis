﻿using NodaMoney;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SubChainageDB
    {
        //protected internal List<ProjectManager> GetSchemes()
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("SELECT ");
        //    sqlCommand.Append("scheme_name ");
        //    sqlCommand.Append("FROM ");
        //    sqlCommand.Append("mis_projects ");
        //    sqlCommand.Append(";");

        //    //NpgsqlParameter[] arParams = new NpgsqlParameter[10];

        //    //arParams[0] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    //arParams[0].Direction = ParameterDirection.Input;
        //    //arParams[0].Value = Guid.NewGuid();

        //    //arParams[1] = new NpgsqlParameter("districtGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    //arParams[1].Direction = ParameterDirection.Input;
        //    //arParams[1].Value = districtGuid;

        //    //arParams[2] = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    //arParams[2].Direction = ParameterDirection.Input;
        //    //arParams[2].Value = divisionGuid;

        //    //arParams[3] = new NpgsqlParameter("schemeName", NpgsqlTypes.NpgsqlDbType.Text);
        //    //arParams[3].Direction = ParameterDirection.Input;
        //    //arParams[3].Value = schemeName;

        //    //arParams[4] = new NpgsqlParameter("schemeChainage", NpgsqlTypes.NpgsqlDbType.Text);
        //    //arParams[4].Direction = ParameterDirection.Input;
        //    //arParams[4].Value = schemeChainage;

        //    //arParams[5] = new NpgsqlParameter("dateOfStart", NpgsqlTypes.NpgsqlDbType.Date);
        //    //arParams[5].Direction = ParameterDirection.Input;
        //    //arParams[5].Value = dateOfStart;

        //    //arParams[6] = new NpgsqlParameter("expectedDateOfCompletion", NpgsqlTypes.NpgsqlDbType.Date);
        //    //arParams[6].Direction = ParameterDirection.Input;
        //    //arParams[6].Value = expectedDateOfCompletion;

        //    //arParams[7] = new NpgsqlParameter("actualDateOfCompletion", NpgsqlTypes.NpgsqlDbType.Date);
        //    //arParams[7].Direction = ParameterDirection.Input;
        //    //arParams[7].Value = actualDateOfCompletion;

        //    //arParams[8] = new NpgsqlParameter("projectValue", NpgsqlTypes.NpgsqlDbType.Money);
        //    //arParams[8].Direction = ParameterDirection.Input;
        //    //arParams[8].Value = projectValue;

        //    //arParams[9] = new NpgsqlParameter("subProjectName", NpgsqlTypes.NpgsqlDbType.Text);
        //    //arParams[9].Direction = ParameterDirection.Input;
        //    //arParams[9].Value = subProjectName;

        //    List<ProjectManager> projectsList = new List<ProjectManager>();

        //    using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
        //    {
        //        while (reader.Read())
        //        {
        //            ProjectManager projectObj = new ProjectManager();

        //            projectObj.ProjectName = reader["scheme_name"].ToString();

        //            projectsList.Add(projectObj);
        //        }
        //    }

        //    return projectsList;
        //}

        protected internal List<SubChainageManager> GetSubChainagesListForSio(Guid userGuid)
        {         
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.sub_chainage_name, ");
            sqlCommand.Append("obj1.sub_chainage_uuid, ");
            sqlCommand.Append("obj1.starting_date, ");
            sqlCommand.Append("obj1.expected_completion_date, ");
            sqlCommand.Append("obj1.actual_completion_date, ");
            sqlCommand.Append("obj2.project_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_sub_chainages obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_projects obj2 ON obj1.project_uuid = obj2.project_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.sub_chainage_sio_uuid = :userGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("userGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = userGuid;

            List<SubChainageManager> subChainageManagerList = new List<SubChainageManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    SubChainageManager subChainageManagerObj = new SubChainageManager();

                    subChainageManagerObj.SubChainageName = reader["sub_chainage_name"].ToString();
                    subChainageManagerObj.SubChainageGuid = new Guid(reader["sub_chainage_uuid"].ToString());
                    subChainageManagerObj.SubChainageStartingDate = DateTime.Parse(reader["starting_date"].ToString());
                    subChainageManagerObj.SubChainageExpectedCompletionDate = DateTime.Parse(reader["expected_completion_date"].ToString());
                    subChainageManagerObj.SubChainageActualCompletionDate = DateTime.Parse(reader["actual_completion_date"].ToString());

                    subChainageManagerObj.ProjectManager.ProjectName = reader["project_name"].ToString();

                    subChainageManagerObj.SubChainagePhysicalProgressManager = new SubChainagePhysicalProgressDB().GetSubChainagePhysicalProgressAll(subChainageManagerObj.SubChainageGuid);

                    subChainageManagerList.Add(subChainageManagerObj);
                }
            }

            return subChainageManagerList;
        }

        //protected internal SubChainageManager GetSubChainageDetails(Guid subChainageGuid)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("SELECT ");
        //    sqlCommand.Append("obj1.*, ");
        //    sqlCommand.Append("obj2.*, ");
        //    sqlCommand.Append("obj3.district_name, ");
        //    sqlCommand.Append("obj4.division_name, ");
        //    sqlCommand.Append("obj5.*, ");
        //    sqlCommand.Append("obj6.*, ");
        //    sqlCommand.Append("obj7.unit_symbol, ");
        //    sqlCommand.Append("obj8.*, ");
        //    sqlCommand.Append("obj9.*, ");
        //    sqlCommand.Append("obj10.* ");
        //    sqlCommand.Append("FROM ");
        //    sqlCommand.Append("mis_project_sub_chainages obj1 ");
        //    sqlCommand.Append("LEFT OUTER JOIN mis_projects obj2 ON obj2.project_uuid = obj1.project_uuid ");
        //    sqlCommand.Append("LEFT OUTER JOIN mis_districts obj3 ON obj3.district_uuid = obj2.district_uuid ");
        //    sqlCommand.Append("LEFT OUTER JOIN mis_divisions obj4 ON obj4.division_uuid = obj2.division_uuid ");
        //    sqlCommand.Append("LEFT OUTER JOIN mis_users obj5 ON obj5.user_uuid = obj1.sub_chainage_sio_uuid ");
        //    sqlCommand.Append("LEFT OUTER JOIN mis_project_work_items obj6 ON obj6.work_item_uuid = ANY(SELECT work_item_uuid FROM mis_project_sub_chainage_work_item_relation WHERE sub_chainage_uuid = :subChainageGuid) ");
        //    sqlCommand.Append("LEFT OUTER JOIN mis_units obj7 ON obj7.unit_uuid = obj6.unit_uuid ");
        //    sqlCommand.Append("LEFT OUTER JOIN mis_project_work_item_sub_heads obj8 ON obj8.work_item_sub_head_uuid = ANY(SELECT a.work_item_sub_head_uuid FROM mis_project_work_item_project_work_item_sub_heads_relation a WHERE a.work_item_uuid IN (obj6.work_item_uuid)) ");
        //    sqlCommand.Append("LEFT OUTER JOIN mis_fremaa_officer_project_relationship obj9 ON obj9.project_uuid = obj2.project_uuid ");
        //    sqlCommand.Append("LEFT OUTER JOIN mis_pmc_site_engineer_project_relationship obj10 ON obj10.project_uuid = obj2.project_uuid ");
        //    sqlCommand.Append("WHERE ");
        //    sqlCommand.Append("obj1.sub_chainage_uuid = :subChainageGuid ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter[] arParams = new NpgsqlParameter[1];

        //    arParams[0] = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[0].Direction = ParameterDirection.Input;
        //    arParams[0].Value = subChainageGuid;

        //    SubChainageManager subChainageObj = new SubChainageManager();

        //    WorkItemManager workItemObj = null;
        //    bool workItemExist = false;
            
        //    using (NpgsqlDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
        //    {
        //        while (reader.Read())
        //        {
        //            //int totalRows = reader.Cast<object>().Count(); //reader.GetInt32(reader.GetOrdinal("sub_chainage_uuid"));

        //            subChainageObj.SubChainageGuid = new Guid(reader["sub_chainage_uuid"].ToString());
        //            subChainageObj.SubChainageName = reader["sub_chainage_name"].ToString();
        //            subChainageObj.UserManager.FirstName = reader["first_name"].ToString();
        //            subChainageObj.UserManager.MiddleName = reader["middle_name"].ToString();
        //            subChainageObj.UserManager.LastName = reader["last_name"].ToString();
        //            subChainageObj.SubChainageValue = Money.Parse(reader["sub_chainage_value"].ToString());
        //            subChainageObj.AdministrativeApprovalReference = reader["administrative_approval_reference"].ToString();
        //            subChainageObj.TechnicalSanctionReference = reader["technical_sanction_reference"].ToString();
        //            subChainageObj.ContractorName = reader["contractor_name"].ToString();
        //            subChainageObj.WorkOrderReference = reader["work_order_reference"].ToString();
        //            subChainageObj.SubChainageStartingDate = DateTime.Parse(reader["starting_date"].ToString());
        //            subChainageObj.SubChainageExpectedCompletionDate = DateTime.Parse(reader["expected_completion_date"].ToString());
        //            subChainageObj.SubChainageActualCompletionDate = DateTime.Parse(reader["actual_completion_date"].ToString());
        //            subChainageObj.SubChainageStatus = Convert.ToInt16(reader["sub_chainage_status"].ToString());
        //            subChainageObj.TypeOfWorks = reader["type_of_works"].ToString();

        //            //subChainageObj.ProjectManager.DistrictManager.DistrictGuid = new Guid(reader["district_uuid"].ToString());
        //            //subChainageObj.ProjectManager.DistrictManager.DistrictCode = reader["district_code"].ToString();
        //            subChainageObj.ProjectManager.DistrictManager.DistrictName = reader["district_name"].ToString();

        //            subChainageObj.ProjectManager.DivisionManager.DivisionName = reader["division_name"].ToString();

        //            subChainageObj.ProjectManager.ProjectName = reader["project_name"].ToString();
        //            subChainageObj.ProjectManager.SubProjectName = reader["sub_project_name"].ToString();
        //            subChainageObj.ProjectManager.ProjectChainage = reader["chainage_of_project"].ToString();
        //            subChainageObj.ProjectManager.DateOfStart = DateTime.Parse(reader["project_starting_date"].ToString());
        //            subChainageObj.ProjectManager.ExpectedDateOfCompletion = DateTime.Parse(reader["expected_completion_date_of_project"].ToString());
        //            subChainageObj.ProjectManager.ActualDateOfCompletion = DateTime.Parse(reader["actual_completion_date_of_project"].ToString());
        //            subChainageObj.ProjectManager.ProjectValue = Money.Parse(reader["project_value"].ToString());
        //            //subChainageObj.ProjectManager.OfficersEngaged = new List<string>(reader["officers"] as string[]);
        //            //subChainageObj.ProjectManager.PmcSiteEngineers = (reader["pmc"] as string[]).ToList();

        //            if (!workItemExist)
        //            {
        //               workItemObj = new WorkItemManager();
        //            }

                   
        //            Guid workItemGuid = new Guid(reader["work_item_uuid"].ToString());
        //            WorkItemSubHeadManager workItemSubHeadObj = new WorkItemSubHeadManager();

        //            if (subChainageObj.WorkItemManager.Exists(x => x.WorkItemGuid == workItemGuid))
        //            {
        //                workItemSubHeadObj.WorkItemSubHeadGuid = new Guid(reader["work_item_sub_head_uuid"].ToString());

        //                if (workItemObj.WorkItemSubHeadManager.Exists(x => x.WorkItemSubHeadGuid == workItemSubHeadObj.WorkItemSubHeadGuid))
        //                {
        //                    workItemSubHeadObj.WorkItemSubHeadName = reader["work_item_sub_head_name"].ToString();

        //                    workItemObj.WorkItemSubHeadManager.Add(workItemSubHeadObj);
        //                }
        //                else
        //                {
        //                    workItemSubHeadObj.WorkItemSubHeadName = reader["work_item_sub_head_name"].ToString();

        //                    workItemObj.WorkItemSubHeadManager.Add(workItemSubHeadObj);
        //                }

        //                //subChainageObj.WorkItemManager.Add(workItemObj);
        //            }
        //            else
        //            {
        //                workItemExist = true;

        //                workItemObj = new WorkItemManager();

        //                workItemObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                        
        //                workItemObj.WorkItemName = reader["work_item_name"].ToString();
        //                workItemObj.UnitManager.UnitGuid = new Guid(reader["unit_uuid"].ToString());
        //                workItemObj.UnitManager.UnitSymbol = reader["unit_symbol"].ToString();

        //                workItemSubHeadObj.WorkItemSubHeadGuid = new Guid(reader["work_item_sub_head_uuid"].ToString());

        //                if (workItemObj.WorkItemSubHeadManager.Exists(x => x.WorkItemSubHeadGuid == workItemSubHeadObj.WorkItemSubHeadGuid))
        //                {
        //                    workItemSubHeadObj.WorkItemSubHeadName = reader["work_item_sub_head_name"].ToString();
        //                }
        //                else
        //                {
        //                    workItemSubHeadObj.WorkItemSubHeadName = reader["work_item_sub_head_name"].ToString();

        //                    workItemObj.WorkItemSubHeadManager.Add(workItemSubHeadObj);
        //                }

        //                subChainageObj.WorkItemManager.Add(workItemObj);
        //            }
        //        }
        //    }
            
        //    return subChainageObj;
        //}

        protected internal SubChainageManager GetSubChainageDetails(Guid subChainageGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.*, ");
            sqlCommand.Append("obj2.*, ");
            sqlCommand.Append("obj3.district_name, ");
            sqlCommand.Append("obj4.division_name, ");
            sqlCommand.Append("obj5.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_sub_chainages obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_projects obj2 ON obj2.project_uuid = obj1.project_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_districts obj3 ON obj3.district_uuid = obj2.district_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_divisions obj4 ON obj4.division_uuid = obj2.division_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_users obj5 ON obj5.user_uuid = obj1.sub_chainage_sio_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.sub_chainage_uuid = :subChainageGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = subChainageGuid;

            SubChainageManager subChainageObj = new SubChainageManager();

            //WorkItemManager workItemObj = null;
            //bool workItemExist = false;

            using (NpgsqlDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    subChainageObj.SubChainageGuid = new Guid(reader["sub_chainage_uuid"].ToString());
                    subChainageObj.SubChainageName = reader["sub_chainage_name"].ToString();
                    subChainageObj.UserManager.FirstName = reader["first_name"].ToString();
                    subChainageObj.UserManager.MiddleName = reader["middle_name"].ToString();
                    subChainageObj.UserManager.LastName = reader["last_name"].ToString();
                    subChainageObj.SubChainageValue = Money.Parse(reader["sub_chainage_value"].ToString());
                    subChainageObj.AdministrativeApprovalReference = reader["administrative_approval_reference"].ToString();
                    subChainageObj.TechnicalSanctionReference = reader["technical_sanction_reference"].ToString();
                    subChainageObj.ContractorName = reader["contractor_name"].ToString();
                    subChainageObj.WorkOrderReference = reader["work_order_reference"].ToString();
                    subChainageObj.SubChainageStartingDate = DateTime.Parse(reader["starting_date"].ToString());
                    subChainageObj.SubChainageExpectedCompletionDate = DateTime.Parse(reader["expected_completion_date"].ToString());
                    subChainageObj.SubChainageActualCompletionDate = DateTime.Parse(reader["actual_completion_date"].ToString());
                    subChainageObj.SubChainageStatus = Convert.ToInt16(reader["sub_chainage_status"].ToString());
                    subChainageObj.TypeOfWorks = reader["type_of_works"].ToString();

                    //subChainageObj.ProjectManager.DistrictManager.DistrictGuid = new Guid(reader["district_uuid"].ToString());
                    //subChainageObj.ProjectManager.DistrictManager.DistrictCode = reader["district_code"].ToString();
                    subChainageObj.ProjectManager.DistrictManager.DistrictName = reader["district_name"].ToString();

                    subChainageObj.ProjectManager.DivisionManager.DivisionName = reader["division_name"].ToString();

                    subChainageObj.ProjectManager.ProjectGuid = new Guid(reader["project_uuid"].ToString());
                    subChainageObj.ProjectManager.ProjectName = reader["project_name"].ToString();
                    subChainageObj.ProjectManager.SubProjectName = reader["sub_project_name"].ToString();
                    subChainageObj.ProjectManager.ProjectChainage = reader["chainage_of_project"].ToString();
                    subChainageObj.ProjectManager.ProjectStartDate = DateTime.Parse(reader["project_starting_date"].ToString());
                    subChainageObj.ProjectManager.ProjectExpectedCompletionDate = DateTime.Parse(reader["expected_completion_date_of_project"].ToString());
                    subChainageObj.ProjectManager.ProjectActualCompletionDate = DateTime.Parse(reader["actual_completion_date_of_project"].ToString());
                    subChainageObj.ProjectManager.ProjectValue = Money.Parse(reader["project_value"].ToString());
                    //subChainageObj.ProjectManager.OfficersEngaged = new List<string>(reader["officers"] as string[]);
                    //subChainageObj.ProjectManager.PmcSiteEngineers = (reader["pmc"] as string[]).ToList();

                    subChainageObj.ProjectManager.OfficersEngaged = new ProjectAndFremaaOfficersRelationDB().GetFremaaOfficersListRelatedToProject(subChainageObj.ProjectManager.ProjectGuid);
                    subChainageObj.ProjectManager.PmcSiteEngineers = new ProjectAndPmcSiteEngineersRelationDB().GetPmcSiteEngineersListRelatedToProject(subChainageObj.ProjectManager.ProjectGuid);

                    subChainageObj.WorkItemManager = new WorkItemDB().GetWorkItemsWithUnitBasedOnSubChainage(subChainageObj.SubChainageGuid);

                    //if (!workItemExist)
                    //{
                    //    workItemObj = new WorkItemManager();
                    //}


                    //Guid workItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    //WorkItemSubHeadManager workItemSubHeadObj = new WorkItemSubHeadManager();

                    //if (subChainageObj.WorkItemManager.Exists(x => x.WorkItemGuid == workItemGuid))
                    //{
                    //    workItemSubHeadObj.WorkItemSubHeadGuid = new Guid(reader["work_item_sub_head_uuid"].ToString());

                    //    if (workItemObj.WorkItemSubHeadManager.Exists(x => x.WorkItemSubHeadGuid == workItemSubHeadObj.WorkItemSubHeadGuid))
                    //    {
                    //        workItemSubHeadObj.WorkItemSubHeadName = reader["work_item_sub_head_name"].ToString();

                    //        workItemObj.WorkItemSubHeadManager.Add(workItemSubHeadObj);
                    //    }
                    //    else
                    //    {
                    //        workItemSubHeadObj.WorkItemSubHeadName = reader["work_item_sub_head_name"].ToString();

                    //        workItemObj.WorkItemSubHeadManager.Add(workItemSubHeadObj);
                    //    }

                    //    //subChainageObj.WorkItemManager.Add(workItemObj);
                    //}
                    //else
                    //{
                    //    workItemExist = true;

                    //    workItemObj = new WorkItemManager();

                    //    workItemObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());

                    //    workItemObj.WorkItemName = reader["work_item_name"].ToString();
                    //    workItemObj.UnitManager.UnitGuid = new Guid(reader["unit_uuid"].ToString());
                    //    workItemObj.UnitManager.UnitSymbol = reader["unit_symbol"].ToString();

                    //    workItemSubHeadObj.WorkItemSubHeadGuid = new Guid(reader["work_item_sub_head_uuid"].ToString());

                    //    if (workItemObj.WorkItemSubHeadManager.Exists(x => x.WorkItemSubHeadGuid == workItemSubHeadObj.WorkItemSubHeadGuid))
                    //    {
                    //        workItemSubHeadObj.WorkItemSubHeadName = reader["work_item_sub_head_name"].ToString();
                    //    }
                    //    else
                    //    {
                    //        workItemSubHeadObj.WorkItemSubHeadName = reader["work_item_sub_head_name"].ToString();

                    //        workItemObj.WorkItemSubHeadManager.Add(workItemSubHeadObj);
                    //    }

                    //    subChainageObj.WorkItemManager.Add(workItemObj);
                    //}
                }
            }

            return subChainageObj;
        }

        protected internal List<SubChainageManager> GetAllSubChainageDetails()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_sub_chainages ");
            sqlCommand.Append(";");

            List<SubChainageManager> subChainageList = new List<SubChainageManager>();

            using (NpgsqlDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    SubChainageManager subChainageObj = new SubChainageManager();

                    subChainageObj.SubChainageGuid = new Guid(reader["sub_chainage_uuid"].ToString());
                    subChainageObj.SubChainageName = reader["sub_chainage_name"].ToString();
                    subChainageObj.SubChainageStartingDate = DateTime.Parse(reader["starting_date"].ToString());
                    subChainageObj.SubChainageExpectedCompletionDate = DateTime.Parse(reader["expected_completion_date"].ToString());
                    subChainageObj.SubChainageActualCompletionDate = DateTime.Parse(reader["actual_completion_date"].ToString());
                    subChainageObj.SubChainageStatus = Convert.ToInt16(reader["sub_chainage_status"].ToString());
                    
                    subChainageList.Add(subChainageObj);
                }
            }

            return subChainageList;
        }

        protected internal void UpdateSubChainageActualDate(SubChainageManager subChainageObj)
        {
            StringBuilder sqlCommand = new StringBuilder();

            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_project_sub_chainages ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("actual_completion_date = :actualCompletionDate ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("sub_chainage_uuid = :subChainageGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("actualCompletionDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = subChainageObj.SubChainageActualCompletionDate;

            arParams[1] = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = subChainageObj.SubChainageGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);
        }






        protected internal List<SubChainageManager> GetSubChainagesBasedOnProject(Guid projectGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.sub_chainage_uuid, ");
            sqlCommand.Append("obj1.sub_chainage_name, ");
            sqlCommand.Append("obj1.sub_chainage_sio_uuid, ");
            sqlCommand.Append("obj1.sub_chainage_value, ");
            sqlCommand.Append("obj1.administrative_approval_reference, ");
            sqlCommand.Append("obj1.technical_sanction_reference, ");
            sqlCommand.Append("obj1.contractor_name, ");
            sqlCommand.Append("obj1.work_order_reference, ");
            sqlCommand.Append("obj1.starting_date, ");
            sqlCommand.Append("obj1.expected_completion_date, ");
            sqlCommand.Append("obj1.actual_completion_date, ");
            sqlCommand.Append("obj1.type_of_works, ");
            sqlCommand.Append("obj1.sub_chainage_status, ");
            sqlCommand.Append("obj2.first_name, ");
            sqlCommand.Append("obj2.middle_name, ");
            sqlCommand.Append("obj2.last_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_sub_chainages obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_users obj2 ON obj2.user_uuid = obj1.sub_chainage_sio_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = projectGuid;

            List<SubChainageManager> subChainageManagerList = new List<SubChainageManager>();

            using (NpgsqlDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    SubChainageManager subChainageManagerObj = new SubChainageManager();

                    subChainageManagerObj.SubChainageGuid = new Guid(reader["sub_chainage_uuid"].ToString());
                    subChainageManagerObj.SubChainageName = reader["sub_chainage_name"].ToString();
                    subChainageManagerObj.SubChainageValue = Money.Parse(reader["sub_chainage_value"].ToString(), Currency.FromCode("INR", "ISO-4217"));
                    subChainageManagerObj.AdministrativeApprovalReference = reader["administrative_approval_reference"].ToString();
                    subChainageManagerObj.TechnicalSanctionReference = reader["technical_sanction_reference"].ToString();
                    subChainageManagerObj.ContractorName = reader["contractor_name"].ToString();
                    subChainageManagerObj.WorkOrderReference = reader["work_order_reference"].ToString();
                    subChainageManagerObj.SubChainageStartingDate = DateTime.Parse(reader["starting_date"].ToString());
                    subChainageManagerObj.SubChainageExpectedCompletionDate = DateTime.Parse(reader["expected_completion_date"].ToString());
                    subChainageManagerObj.SubChainageActualCompletionDate = DateTime.Parse(reader["actual_completion_date"].ToString());
                    subChainageManagerObj.TypeOfWorks = reader["type_of_works"].ToString();
                    subChainageManagerObj.SubChainageStatus = Convert.ToInt16(reader["sub_chainage_status"].ToString());

                    subChainageManagerObj.UserManager.FirstName = reader["first_name"].ToString();
                    subChainageManagerObj.UserManager.MiddleName = reader["middle_name"].ToString();
                    subChainageManagerObj.UserManager.LastName = reader["last_name"].ToString();

                    subChainageManagerObj.WorkItemManager = new WorkItemDB().GetWorkItemsWithUnitBasedOnSubChainage(subChainageManagerObj.SubChainageGuid);

                    subChainageManagerList.Add(subChainageManagerObj);
                }
            }

            return subChainageManagerList;
        }

        protected internal int AddSubChainage(ProjectManager projectManager)
        {
            int rowsAffected = 0;

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_sub_chainages ");
            sqlCommand.Append("( ");
            sqlCommand.Append("sub_chainage_uuid, ");
            sqlCommand.Append("sub_chainage_name, ");
            sqlCommand.Append("sub_chainage_sio_uuid, ");
            sqlCommand.Append("project_uuid, ");
            sqlCommand.Append("sub_chainage_value, ");
            sqlCommand.Append("administrative_approval_reference, ");
            sqlCommand.Append("technical_sanction_reference, ");
            sqlCommand.Append("contractor_name, ");
            sqlCommand.Append("work_order_reference, ");
            sqlCommand.Append("starting_date, ");
            sqlCommand.Append("expected_completion_date, ");
            sqlCommand.Append("actual_completion_date, ");
            sqlCommand.Append("type_of_works, ");
            sqlCommand.Append("sub_chainage_status ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            if (projectManager.SubChainageManagerList.Count > 0)
            {
                foreach (SubChainageManager subChainageManagerObj in projectManager.SubChainageManagerList)
                {
                    int index = projectManager.SubChainageManagerList.IndexOf(subChainageManagerObj);

                    sqlCommand.Append("( ");

                    subChainageManagerObj.SubChainageGuid = Guid.NewGuid();

                    sqlCommand.Append("'" + subChainageManagerObj.SubChainageGuid + "', ");
                    sqlCommand.Append("'" + subChainageManagerObj.SubChainageName + "', ");
                    sqlCommand.Append("'" + subChainageManagerObj.SubChainageSioGuid + "', ");
                    sqlCommand.Append("'" + projectManager.ProjectGuid + "', ");
                    sqlCommand.Append("'" + subChainageManagerObj.SubChainageValue.Amount + "', ");
                    sqlCommand.Append("'" + subChainageManagerObj.AdministrativeApprovalReference + "', ");
                    sqlCommand.Append("'" + subChainageManagerObj.TechnicalSanctionReference + "', ");
                    sqlCommand.Append("'" + subChainageManagerObj.ContractorName + "', ");
                    sqlCommand.Append("'" + subChainageManagerObj.WorkOrderReference + "', ");
                    sqlCommand.Append("to_date('" + subChainageManagerObj.SubChainageStartingDate.ToString("dd/MM/yyyy") + "', 'DD/MM/YYYY'), ");
                    sqlCommand.Append("to_date('" + subChainageManagerObj.SubChainageExpectedCompletionDate.ToString("dd/MM/yyyy") + "', 'DD/MM/YYYY'), ");
                    sqlCommand.Append("to_date('" + subChainageManagerObj.SubChainageExpectedCompletionDate.ToString("dd/MM/yyyy") + "', 'DD/MM/YYYY'), ");
                    sqlCommand.Append("'" + subChainageManagerObj.TypeOfWorks + "', ");
                    sqlCommand.Append("'" + (int)SubChainageStatusEnum.Created + "' ");

                    if (index == projectManager.SubChainageManagerList.Count - 1)
                    {
                        sqlCommand.Append(") ");
                    }
                    else
                    {
                        sqlCommand.Append("), ");
                    }
                }

                sqlCommand.Append(";");

                rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());

                if (rowsAffected > 0)
                {
                    new SubChainageAndWorkItemsRelationDB().AddSubChainageAndWorkItemsRelation(projectManager.SubChainageManagerList);
                }
            }

            return rowsAffected;

        }

    }
}