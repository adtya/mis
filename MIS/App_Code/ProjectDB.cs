﻿using Npgsql;
using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using NodaMoney;
using System.Globalization;

namespace MIS.App_Code
{
    public class ProjectDB
    {
        protected internal List<ProjectManager> GetProjectNameWithStatus()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("project_uuid, ");
            sqlCommand.Append("project_name, ");
            sqlCommand.Append("project_status ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_projects ");
            sqlCommand.Append(";");

            List<ProjectManager> projectList = new List<ProjectManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    ProjectManager projectObj = new ProjectManager();

                    projectObj.ProjectGuid = new Guid(reader["project_uuid"].ToString());
                    projectObj.ProjectName = reader["project_name"].ToString();
                    projectObj.ProjectStatus = Convert.ToInt16(reader["project_status"].ToString());

                    projectList.Add(projectObj);
                }
            }

            return projectList;
        }


        protected internal int AddProject(ProjectManager projectManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_projects ");
            sqlCommand.Append("( ");
            sqlCommand.Append("project_uuid, ");
            sqlCommand.Append("district_uuid, ");
            sqlCommand.Append("division_uuid, ");
            sqlCommand.Append("project_name, ");
            sqlCommand.Append("sub_project_name, ");
            sqlCommand.Append("chainage_of_project, ");
            sqlCommand.Append("project_starting_date, ");
            sqlCommand.Append("expected_completion_date_of_project, ");
            sqlCommand.Append("actual_completion_date_of_project, ");
            sqlCommand.Append("project_value, ");
            sqlCommand.Append("project_status, ");
            sqlCommand.Append("include_land_acquisition, ");
            sqlCommand.Append("project_creation_date ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":projectGuid, ");
            sqlCommand.Append(":districtGuid, ");
            sqlCommand.Append(":divisionGuid, ");
            sqlCommand.Append(":projectName, ");
            sqlCommand.Append(":subProjectName, ");
            sqlCommand.Append(":chainageOfProject, ");
            sqlCommand.Append(":dateOfStartOfProject, ");
            sqlCommand.Append(":expectedDateOfCompletionOfProject, ");
            sqlCommand.Append(":actualDateOfCompletionOfProject, ");
            sqlCommand.Append(":projectValue, ");
            sqlCommand.Append(":projectStatus, ");
            sqlCommand.Append(":landAcquisitionIncluded, ");
            sqlCommand.Append(":projectCreationDate ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[13];

            projectManager.ProjectGuid = Guid.NewGuid();

            arParams[0] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = projectManager.ProjectGuid;

            arParams[1] = new NpgsqlParameter("districtGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = projectManager.DistrictManager.DistrictGuid;

            arParams[2] = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = projectManager.DivisionManager.DivisionGuid;

            arParams[3] = new NpgsqlParameter("projectName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(projectManager.ProjectName);

            arParams[4] = new NpgsqlParameter("subProjectName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(projectManager.SubProjectName);;

            arParams[5] = new NpgsqlParameter("chainageOfProject", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = projectManager.ProjectChainage;

            arParams[6] = new NpgsqlParameter("dateOfStartOfProject", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = projectManager.ProjectStartDate;

            arParams[7] = new NpgsqlParameter("expectedDateOfCompletionOfProject", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = projectManager.ProjectExpectedCompletionDate;

            arParams[8] = new NpgsqlParameter("actualDateOfCompletionOfProject", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = projectManager.ProjectActualCompletionDate;

            arParams[9] = new NpgsqlParameter("projectValue", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = projectManager.ProjectValue.Amount;

            arParams[10] = new NpgsqlParameter("projectStatus", NpgsqlTypes.NpgsqlDbType.Smallint);
            arParams[10].Direction = ParameterDirection.Input;
            arParams[10].Value = (int)ProjectStatusEnum.Created;

            arParams[11] = new NpgsqlParameter("landAcquisitionIncluded", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[11].Direction = ParameterDirection.Input;
            arParams[11].Value = projectManager.LandAcquisitionIncluded;

            arParams[12] = new NpgsqlParameter("projectCreationDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[12].Direction = ParameterDirection.Input;
            arParams[12].Value = DateTime.Today;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            if (rowsAffected == 1)
            {
                new ProjectAndFremaaOfficersRelationDB().AddProjectAndFremaaOfficersRelation(projectManager);

                new ProjectAndPmcSiteEngineersRelationDB().AddProjectAndPmcSiteEngineersRelation(projectManager);

                new SubChainageDB().AddSubChainage(projectManager);

                new ProjectValueDistributionDB().AddProjectValueDistribution(projectManager);
            }

            return rowsAffected;
        }




        protected internal ProjectManager GetProject(Guid projectGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.project_uuid, ");
            sqlCommand.Append("obj1.district_uuid, ");
            sqlCommand.Append("obj1.division_uuid, ");
            sqlCommand.Append("obj1.project_name, ");
            sqlCommand.Append("obj1.sub_project_name, ");
            sqlCommand.Append("obj1.chainage_of_project, ");
            sqlCommand.Append("obj1.project_starting_date, ");
            sqlCommand.Append("obj1.expected_completion_date_of_project, ");
            sqlCommand.Append("obj1.actual_completion_date_of_project, ");
            sqlCommand.Append("obj1.project_value, ");
            sqlCommand.Append("obj1.project_status, ");
            sqlCommand.Append("obj1.include_land_acquisition, ");
            sqlCommand.Append("obj1.project_creation_date, ");
            sqlCommand.Append("obj2.district_code, ");
            sqlCommand.Append("obj2.district_name, ");
            sqlCommand.Append("obj3.division_code, ");
            sqlCommand.Append("obj3.division_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_projects obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_districts obj2 ON obj2.district_uuid = obj1.district_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_divisions obj3 ON obj3.division_uuid = obj1.division_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = projectGuid;

            ProjectManager projectManagerObj = new ProjectManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    projectManagerObj.ProjectGuid = new Guid(reader["project_uuid"].ToString());
                    projectManagerObj.ProjectName = reader["project_name"].ToString();
                    projectManagerObj.SubProjectName = reader["sub_project_name"].ToString();
                    projectManagerObj.ProjectChainage = reader["chainage_of_project"].ToString();
                    projectManagerObj.ProjectStartDate = DateTime.Parse(reader["project_starting_date"].ToString());
                    projectManagerObj.ProjectExpectedCompletionDate = DateTime.Parse(reader["expected_completion_date_of_project"].ToString());
                    projectManagerObj.ProjectActualCompletionDate = DateTime.Parse(reader["actual_completion_date_of_project"].ToString());
                    projectManagerObj.ProjectValue = Money.Parse(reader["project_value"].ToString(), Currency.FromCode("INR", "ISO-4217"));
                    projectManagerObj.ProjectStatus = Convert.ToInt16(reader["project_status"].ToString());
                    projectManagerObj.ProjectCreationDate = DateTime.Parse(reader["project_creation_date"].ToString());

                    projectManagerObj.DistrictManager.DistrictGuid = new Guid(reader["district_uuid"].ToString());
                    projectManagerObj.DistrictManager.DistrictCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["district_code"].ToString());
                    projectManagerObj.DistrictManager.DistrictName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["district_name"].ToString());

                    projectManagerObj.DivisionManager.DivisionGuid = new Guid(reader["division_uuid"].ToString());
                    projectManagerObj.DivisionManager.DivisionCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["division_code"].ToString());
                    projectManagerObj.DivisionManager.DivisionName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["division_name"].ToString());

                    projectManagerObj.OfficersEngaged = new ProjectAndFremaaOfficersRelationDB().GetFremaaOfficersListRelatedToProject(projectGuid);

                    projectManagerObj.PmcSiteEngineers = new ProjectAndPmcSiteEngineersRelationDB().GetPmcSiteEngineersListRelatedToProject(projectGuid);

                    projectManagerObj.SubChainageManagerList = new SubChainageDB().GetSubChainagesBasedOnProject(projectGuid);

                    projectManagerObj.ProjectValueDistributionManagerList = new ProjectValueDistributionDB().GetProjectValueDistribution(projectGuid);
                }
            }

            return projectManagerObj;
        }


        protected internal List<ProjectManager> GetProjects()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.project_uuid, ");
            sqlCommand.Append("obj1.project_name, ");
            sqlCommand.Append("obj1.sub_project_name, ");
            sqlCommand.Append("obj1.chainage_of_project, ");
            sqlCommand.Append("obj1.project_starting_date, ");
            sqlCommand.Append("obj1.expected_completion_date_of_project, ");
            sqlCommand.Append("obj1.actual_completion_date_of_project, ");
            sqlCommand.Append("obj1.project_value, ");
            sqlCommand.Append("obj1.project_status, ");
            sqlCommand.Append("obj1.include_land_acquisition, ");
            sqlCommand.Append("obj1.project_creation_date ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_projects obj1 ");
            sqlCommand.Append(";");

            List<ProjectManager> projectManagerList = new List<ProjectManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    ProjectManager projectManagerObj = new ProjectManager();

                    projectManagerObj.ProjectGuid = new Guid(reader["project_uuid"].ToString());
                    projectManagerObj.ProjectName = reader["project_name"].ToString();
                    projectManagerObj.SubProjectName = reader["sub_project_name"].ToString();
                    projectManagerObj.ProjectChainage = reader["chainage_of_project"].ToString();
                    projectManagerObj.ProjectStartDate = DateTime.Parse(reader["project_starting_date"].ToString());
                    projectManagerObj.ProjectExpectedCompletionDate = DateTime.Parse(reader["expected_completion_date_of_project"].ToString());
                    projectManagerObj.ProjectActualCompletionDate = DateTime.Parse(reader["actual_completion_date_of_project"].ToString());
                    projectManagerObj.ProjectValue = Decimal.Parse(reader["project_value"].ToString());
                    projectManagerObj.ProjectStatus = Convert.ToInt16(reader["project_status"].ToString());
                    projectManagerObj.ProjectCreationDate = DateTime.Parse(reader["project_creation_date"].ToString());

                    projectManagerList.Add(projectManagerObj);
                }
            }

            return projectManagerList;
        }

        protected internal List<ProjectManager> GetProjectsWithDistrictAndDivision()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.project_uuid, ");
            sqlCommand.Append("obj1.district_uuid, ");
            sqlCommand.Append("obj1.division_uuid, ");
            sqlCommand.Append("obj1.project_name, ");
            sqlCommand.Append("obj1.sub_project_name, ");
            sqlCommand.Append("obj1.chainage_of_project, ");
            sqlCommand.Append("obj1.project_starting_date, ");
            sqlCommand.Append("obj1.expected_completion_date_of_project, ");
            sqlCommand.Append("obj1.actual_completion_date_of_project, ");
            sqlCommand.Append("obj1.project_value, ");
            sqlCommand.Append("obj1.project_status, ");
            sqlCommand.Append("obj1.include_land_acquisition ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_projects obj1 ");
            sqlCommand.Append(";");

            List<ProjectManager> projectManagerList = new List<ProjectManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    ProjectManager projectManagerObj = new ProjectManager();

                    projectManagerObj.ProjectGuid = new Guid(reader["project_uuid"].ToString());

                    projectManagerObj.DistrictManager = new DistrictDB().GetDistrict(new Guid(reader["district_uuid"].ToString()));

                    projectManagerObj.DivisionManager = new DivisionDB().GetDivision(new Guid(reader["division_uuid"].ToString()));

                    projectManagerObj.ProjectName = reader["project_name"].ToString();
                    projectManagerObj.SubProjectName = reader["sub_project_name"].ToString();
                    projectManagerObj.ProjectChainage = reader["chainage_of_project"].ToString();
                    projectManagerObj.ProjectStartDate = DateTime.Parse(reader["project_starting_date"].ToString());
                    projectManagerObj.ProjectExpectedCompletionDate = DateTime.Parse(reader["expected_completion_date_of_project"].ToString());
                    projectManagerObj.ProjectActualCompletionDate = DateTime.Parse(reader["actual_completion_date_of_project"].ToString());
                    projectManagerObj.ProjectValue = Decimal.Parse(reader["project_value"].ToString());
                    projectManagerObj.ProjectStatus = Convert.ToInt16(reader["project_status"].ToString());

                    projectManagerList.Add(projectManagerObj);
                }
            }

            return projectManagerList;
        }



        protected internal int SaveProjectBasicDetails(ProjectManager projectManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_projects ");
            sqlCommand.Append("( ");
            sqlCommand.Append("project_uuid, ");
            sqlCommand.Append("district_uuid, ");
            sqlCommand.Append("division_uuid, ");
            sqlCommand.Append("project_name, ");
            sqlCommand.Append("sub_project_name, ");
            sqlCommand.Append("project_chainage, ");
            sqlCommand.Append("project_start_date, ");
            sqlCommand.Append("project_expected_completion_date, ");
            sqlCommand.Append("project_actual_completion_date, ");
            sqlCommand.Append("project_value, ");
            sqlCommand.Append("project_status, ");
            sqlCommand.Append("project_creation_date, ");
            sqlCommand.Append("project_saved_partially_step ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":projectGuid, ");
            sqlCommand.Append(":districtGuid, ");
            sqlCommand.Append(":divisionGuid, ");
            sqlCommand.Append(":projectName, ");
            sqlCommand.Append(":subProjectName, ");
            sqlCommand.Append(":projectChainage, ");
            sqlCommand.Append(":projectStartDate, ");
            sqlCommand.Append(":projectExpectedCompletionDate, ");
            sqlCommand.Append(":projectActualCompletionDate, ");
            sqlCommand.Append(":projectValue, ");
            sqlCommand.Append(":projectStatus, ");
            sqlCommand.Append(":projectCreationDate, ");
            sqlCommand.Append(":projectSavedPartiallyStep ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[13];

            arParams[0] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = projectManager.ProjectGuid;

            arParams[1] = new NpgsqlParameter("districtGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = projectManager.DistrictManager.DistrictGuid;

            arParams[2] = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = projectManager.DivisionManager.DivisionGuid;

            arParams[3] = new NpgsqlParameter("projectName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = projectManager.ProjectName;

            arParams[4] = new NpgsqlParameter("subProjectName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = projectManager.SubProjectName;

            arParams[5] = new NpgsqlParameter("projectChainage", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = projectManager.ProjectChainage;

            arParams[6] = new NpgsqlParameter("projectStartDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = projectManager.ProjectStartDate;

            arParams[7] = new NpgsqlParameter("projectExpectedCompletionDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = projectManager.ProjectExpectedCompletionDate;

            arParams[8] = new NpgsqlParameter("projectActualCompletionDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = projectManager.ProjectExpectedCompletionDate;

            arParams[9] = new NpgsqlParameter("projectValue", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = projectManager.ProjectValue.Amount;

            arParams[10] = new NpgsqlParameter("projectStatus", NpgsqlTypes.NpgsqlDbType.Smallint);
            arParams[10].Direction = ParameterDirection.Input;
            arParams[10].Value = (int)ProjectStatusEnum.Saved_Partially;

            arParams[11] = new NpgsqlParameter("projectCreationDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[11].Direction = ParameterDirection.Input;
            arParams[11].Value = DateTime.Today;

            arParams[12] = new NpgsqlParameter("projectSavedPartiallyStep", NpgsqlTypes.NpgsqlDbType.Smallint);
            arParams[12].Direction = ParameterDirection.Input;
            arParams[12].Value = 1;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;

        }

        //protected internal int SaveProjectOtherOfficersEngagedDetails(Guid projectGuid, string[] arrOfficersEngaged)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("UPDATE ");
        //    sqlCommand.Append("mis_projects ");
        //    sqlCommand.Append("SET ");
        //    sqlCommand.Append("officers = :arrOfficersEngaged ");
        //    sqlCommand.Append("WHERE ");
        //    sqlCommand.Append("project_uuid = :projectGuid ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter[] arParams = new NpgsqlParameter[2];

        //    arParams[0] = new NpgsqlParameter("arrOfficersEngaged", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text);
        //    arParams[0].Direction = ParameterDirection.Input;
        //    arParams[0].Value = arrOfficersEngaged;

        //    arParams[1] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[1].Direction = ParameterDirection.Input;
        //    arParams[1].Value = projectGuid;

        //    int result = 0;

        //    try
        //    {
        //        result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);
        //    }
        //    catch (Exception ex)
        //    {
        //        string a = ex.StackTrace;
        //        Console.WriteLine(a);
        //        Console.WriteLine(ex.ToString());
        //    }

        //    return result;
        //}

        //protected internal int SaveProjectPmcSiteEngineersDetails(Guid projectGuid, string[] arrPmcSiteEngineers)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("UPDATE ");
        //    sqlCommand.Append("mis_projects ");
        //    sqlCommand.Append("SET ");
        //    sqlCommand.Append("pmc = :arrPmcSiteEngineers ");
        //    sqlCommand.Append("WHERE ");
        //    sqlCommand.Append("project_uuid = :projectGuid ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter[] arParams = new NpgsqlParameter[2];

        //    arParams[0] = new NpgsqlParameter("arrPmcSiteEngineers", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text);
        //    arParams[0].Direction = ParameterDirection.Input;
        //    arParams[0].Value = arrPmcSiteEngineers;

        //    arParams[1] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[1].Direction = ParameterDirection.Input;
        //    arParams[1].Value = projectGuid;

        //    int result = 0;

        //    try
        //    {
        //        result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);
        //    }
        //    catch (Exception ex)
        //    {
        //        string a = ex.StackTrace;
        //        Console.WriteLine(a);
        //        Console.WriteLine(ex.ToString());
        //    }

        //    return result;
        //}

        //protected internal int SaveLandAcquisitionDetails(Guid projectGuid, bool includeLandAcquisition)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("UPDATE ");
        //    sqlCommand.Append("mis_projects ");
        //    sqlCommand.Append("SET ");
        //    sqlCommand.Append("include_land_acquisition = :includeLandAcquisition ");
        //    sqlCommand.Append("WHERE ");
        //    sqlCommand.Append("project_uuid = :projectGuid ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter[] arParams = new NpgsqlParameter[2];

        //    arParams[0] = new NpgsqlParameter("includeLandAcquisition", NpgsqlTypes.NpgsqlDbType.Boolean);
        //    arParams[0].Direction = ParameterDirection.Input;
        //    arParams[0].Value = includeLandAcquisition;

        //    arParams[1] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[1].Direction = ParameterDirection.Input;
        //    arParams[1].Value = projectGuid;

        //    int result = 0;

        //    result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

        //    return result;
        //}

        //protected internal int SubmitProject(Guid projectGuid, short projectStatus)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("UPDATE ");
        //    sqlCommand.Append("mis_projects ");
        //    sqlCommand.Append("SET ");
        //    sqlCommand.Append("project_status = :projectStatus ");
        //    sqlCommand.Append("WHERE ");
        //    sqlCommand.Append("project_uuid = :projectGuid ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter[] arParams = new NpgsqlParameter[2];

        //    arParams[0] = new NpgsqlParameter("projectStatus", NpgsqlTypes.NpgsqlDbType.Smallint);
        //    arParams[0].Direction = ParameterDirection.Input;
        //    arParams[0].Value = projectStatus;

        //    arParams[1] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[1].Direction = ParameterDirection.Input;
        //    arParams[1].Value = projectGuid;

        //    int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

        //    return result;
        //}

        //protected internal bool CreateProject(Guid projectGuid, Guid districtGuid, Guid divisonGuid, string projectName, string subProjectName, string chainageOfProject, DateTime dateOfStartOfProject, DateTime expectedDateOfCompletionOfProject, Money projectValue, short projectStatus, string[] arrOfficersEngaged, string[] arrPmcSiteEngineers, List<SubChainageManager> arrSubChainages, bool includeLandAcquisition, List<ProjectValueDistributionHeadManager> arrHeadBudgets)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("INSERT INTO ");
        //    sqlCommand.Append("mis_projects ");
        //    sqlCommand.Append("( ");
        //    sqlCommand.Append("project_uuid, ");
        //    sqlCommand.Append("district_uuid, ");
        //    sqlCommand.Append("divison_uuid, ");
        //    sqlCommand.Append("project_name, ");
        //    sqlCommand.Append("sub_project_name, ");
        //    sqlCommand.Append("chainage_of_project, ");
        //    sqlCommand.Append("project_starting_date, ");
        //    sqlCommand.Append("expected_completion_date_of_project, ");
        //    sqlCommand.Append("actual_completion_date_of_project, ");
        //    sqlCommand.Append("project_value, ");
        //    sqlCommand.Append("project_status, ");
        //    sqlCommand.Append("officers, ");
        //    sqlCommand.Append("pmc, ");
        //    sqlCommand.Append("include_land_acquisition ");
        //    sqlCommand.Append(") ");
        //    sqlCommand.Append("VALUES ");
        //    sqlCommand.Append("( ");
        //    sqlCommand.Append(":projectGuid, ");
        //    sqlCommand.Append(":districtGuid, ");
        //    sqlCommand.Append(":divisonGuid, ");
        //    sqlCommand.Append(":projectName, ");
        //    sqlCommand.Append(":subProjectName, ");
        //    sqlCommand.Append(":chainageOfProject, ");
        //    sqlCommand.Append(":dateOfStartOfProject, ");
        //    sqlCommand.Append(":expectedDateOfCompletionOfProject, ");
        //    sqlCommand.Append(":actualDateOfCompletionOfProject, ");
        //    sqlCommand.Append(":projectValue, ");
        //    sqlCommand.Append(":projectStatus, ");
        //    sqlCommand.Append(":arrOfficersEngaged, ");
        //    sqlCommand.Append(":arrPmcSiteEngineers, ");
        //    sqlCommand.Append(":includeLandAcquisition ");
        //    sqlCommand.Append(") ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter[] arParams = new NpgsqlParameter[14];

        //    arParams[0] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[0].Direction = ParameterDirection.Input;
        //    arParams[0].Value = projectGuid;

        //    arParams[1] = new NpgsqlParameter("districtGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[1].Direction = ParameterDirection.Input;
        //    arParams[1].Value = districtGuid;

        //    arParams[2] = new NpgsqlParameter("divisonGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[2].Direction = ParameterDirection.Input;
        //    arParams[2].Value = divisonGuid;

        //    arParams[3] = new NpgsqlParameter("projectName", NpgsqlTypes.NpgsqlDbType.Text);
        //    arParams[3].Direction = ParameterDirection.Input;
        //    arParams[3].Value = projectName;

        //    arParams[4] = new NpgsqlParameter("subProjectName", NpgsqlTypes.NpgsqlDbType.Text);
        //    arParams[4].Direction = ParameterDirection.Input;
        //    arParams[4].Value = subProjectName;

        //    arParams[5] = new NpgsqlParameter("chainageOfProject", NpgsqlTypes.NpgsqlDbType.Text);
        //    arParams[5].Direction = ParameterDirection.Input;
        //    arParams[5].Value = chainageOfProject;

        //    arParams[6] = new NpgsqlParameter("dateOfStartOfProject", NpgsqlTypes.NpgsqlDbType.Date);
        //    arParams[6].Direction = ParameterDirection.Input;
        //    arParams[6].Value = dateOfStartOfProject;

        //    arParams[7] = new NpgsqlParameter("expectedDateOfCompletionOfProject", NpgsqlTypes.NpgsqlDbType.Date);
        //    arParams[7].Direction = ParameterDirection.Input;
        //    arParams[7].Value = expectedDateOfCompletionOfProject;

        //    arParams[8] = new NpgsqlParameter("actualDateOfCompletionOfProject", NpgsqlTypes.NpgsqlDbType.Date);
        //    arParams[8].Direction = ParameterDirection.Input;
        //    arParams[8].Value = new DateTime();

        //    arParams[9] = new NpgsqlParameter("projectValue", NpgsqlTypes.NpgsqlDbType.Money);
        //    arParams[9].Direction = ParameterDirection.Input;
        //    arParams[9].Value = projectValue.Amount;

        //    arParams[10] = new NpgsqlParameter("projectStatus", NpgsqlTypes.NpgsqlDbType.Smallint);
        //    arParams[10].Direction = ParameterDirection.Input;
        //    arParams[10].Value = projectStatus;

        //    arParams[11] = new NpgsqlParameter("arrOfficersEngaged", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text);
        //    arParams[11].Direction = ParameterDirection.Input;
        //    arParams[11].Value = arrOfficersEngaged;

        //    arParams[12] = new NpgsqlParameter("arrPmcSiteEngineers", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text);
        //    arParams[12].Direction = ParameterDirection.Input;
        //    arParams[12].Value = arrPmcSiteEngineers;

        //    arParams[13] = new NpgsqlParameter("includeLandAcquisition", NpgsqlTypes.NpgsqlDbType.Boolean);
        //    arParams[13].Direction = ParameterDirection.Input;
        //    arParams[13].Value = includeLandAcquisition;

        //    int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

        //    if (result == 1)
        //    {
        //        bool subChainageResult = false;
        //        bool headBudgetResult = false;

        //        if (arrSubChainages.Count > 0)
        //        {
        //            foreach (SubChainageManager subChainageObj in arrSubChainages)
        //            {
        //                subChainageObj.SubChainageGuid = Guid.NewGuid();
        //                subChainageObj.ProjectManager.ProjectGuid = projectGuid;
        //            }

        //            subChainageResult = new SubChainageDB().CreateNewSubChainage(arrSubChainages);
        //        }

        //        if (arrHeadBudgets.Count > 0)
        //        {
        //            foreach (ProjectValueDistributionHeadManager headObj in arrHeadBudgets)
        //            {
        //                headObj.HeadGuid = Guid.NewGuid();
        //                headObj.ProjectManager.ProjectGuid = projectGuid;
        //            }

        //            headBudgetResult = new ProjectValueDistributionHeadDB().SaveHeadBudget(arrHeadBudgets);
        //        }

        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //protected internal List<ProjectManager> GetProjectNameWithStatus()
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("SELECT ");
        //    sqlCommand.Append("project_uuid, ");
        //    sqlCommand.Append("project_name, ");
        //    sqlCommand.Append("project_status ");
        //    sqlCommand.Append("FROM ");
        //    sqlCommand.Append("mis_projects ");
        //    sqlCommand.Append(";");

        //    List<ProjectManager> projectList = new List<ProjectManager>();

        //    using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
        //    {
        //        while (reader.Read())
        //        {
        //            ProjectManager projectObj = new ProjectManager();

        //            projectObj.ProjectGuid = new Guid(reader["project_uuid"].ToString());
        //            projectObj.ProjectName = reader["project_name"].ToString();
        //            projectObj.ProjectStatus = Convert.ToInt16(reader["project_status"].ToString());

        //            projectList.Add(projectObj);
        //        }
        //    }

        //    return projectList;
        //}




        protected internal int UpdateProjectBasicDetails(ProjectManager projectManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_projects ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("district_uuid = :districtGuid, ");
            sqlCommand.Append("division_uuid = :divisionGuid, ");
            sqlCommand.Append("project_name = :projectName, ");
            sqlCommand.Append("sub_project_name = :subProjectName, ");
            sqlCommand.Append("project_chainage = :projectChainage, ");
            sqlCommand.Append("project_start_date = :projectStartDate, ");
            sqlCommand.Append("project_expected_completion_date = :projectExpectedCompletionDate, ");
            sqlCommand.Append("project_actual_completion_date = :projectActualCompletionDate, ");
            sqlCommand.Append("project_value = :projectValue ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[10];

            arParams[0] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = projectManager.ProjectGuid;

            arParams[1] = new NpgsqlParameter("districtGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = projectManager.DistrictManager.DistrictGuid;

            arParams[2] = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = projectManager.DivisionManager.DivisionGuid;

            arParams[3] = new NpgsqlParameter("projectName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = projectManager.ProjectName;

            arParams[4] = new NpgsqlParameter("subProjectName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = projectManager.SubProjectName;

            arParams[5] = new NpgsqlParameter("projectChainage", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = projectManager.ProjectChainage;

            arParams[6] = new NpgsqlParameter("projectStartDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = projectManager.ProjectStartDate;

            arParams[7] = new NpgsqlParameter("projectExpectedCompletionDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = projectManager.ProjectExpectedCompletionDate;

            arParams[8] = new NpgsqlParameter("projectActualCompletionDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = projectManager.ProjectActualCompletionDate;

            arParams[9] = new NpgsqlParameter("projectValue", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = projectManager.ProjectValue.Amount;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;

        }





    }


}