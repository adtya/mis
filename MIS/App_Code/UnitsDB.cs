﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class UnitsDB
    {
        protected internal List<Units> GetUnits()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("unit_list ");
            sqlCommand.Append(";");

            //NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            //arParams[0] = new NpgsqlParameter("unitGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            //arParams[0].Direction = ParameterDirection.Input;
            //arParams[0].Value = Guid.NewGuid();

            //arParams[1] = new NpgsqlParameter("unitName", NpgsqlTypes.NpgsqlDbType.Text);
            //arParams[1].Direction = ParameterDirection.Input;
            //arParams[1].Value = unitName;

            //arParams[2] = new NpgsqlParameter("unitSymbol", NpgsqlTypes.NpgsqlDbType.Text);
            //arParams[2].Direction = ParameterDirection.Input;
            //arParams[2].Value = unitSymbol;

            List<Units> unitList = new List<Units>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    Units unitObj = new Units();

                    unitObj.UnitGuid = new Guid(reader["unit_uuid"].ToString());
                    unitObj.UnitName = reader["unit_name"].ToString();
                    unitObj.UnitSymbol = reader["unit_symbol"].ToString();

                    unitList.Add(unitObj);
                }
            }

            return unitList;

        }

        protected internal void AddNewUnit(string unitName, string unitSymbol)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("unit_list ");
            sqlCommand.Append("( ");
            sqlCommand.Append("unit_uuid, ");
            sqlCommand.Append("unit_name, ");
            sqlCommand.Append("unit_symbol ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":unitGuid, ");
            sqlCommand.Append(":unitName, ");
            sqlCommand.Append(":unitSymbol ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("unitGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("unitName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = unitName;

            arParams[2] = new NpgsqlParameter("unitSymbol", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = unitSymbol;

            int result = Convert.ToInt32(NpgsqlHelper.ExecuteScalar(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams));

        }
    }
}