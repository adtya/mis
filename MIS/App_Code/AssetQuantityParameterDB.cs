﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class AssetQuantityParameterDB
    {
        protected internal List<AssetQuantityParameterManager> GetAssetQuantityParameter()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_quantity_parameter_uuid, ");
            sqlCommand.Append("obj1.asset_quantity_parameter_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_quantity_parameters obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("asset_quantity_parameter_name ");
            sqlCommand.Append(";");

            List<AssetQuantityParameterManager> assetQuantityParameterManagerList = new List<AssetQuantityParameterManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    AssetQuantityParameterManager assetQuantityParameterManagerObj = new AssetQuantityParameterManager();

                    assetQuantityParameterManagerObj.AssetQuantityParameterGuid = new Guid(reader["asset_quantity_parameter_uuid"].ToString());
                    assetQuantityParameterManagerObj.AssetQuantityParameterName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_quantity_parameter_name"].ToString());

                    assetQuantityParameterManagerList.Add(assetQuantityParameterManagerObj);
                }
            }

            return assetQuantityParameterManagerList;
        }

        protected internal AssetQuantityParameterManager GetUnitForAssetQuantityParameter(Guid assetQuantityParameterGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.unit_uuid, ");
            sqlCommand.Append("obj1.unit_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_units obj1 ");
            sqlCommand.Append("LEFT JOIN mis_asset_quantity_parameters ");
            sqlCommand.Append("ON mis_asset_quantity_parameters.unit_uuid = obj1.unit_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("mis_asset_quantity_parameters.asset_quantity_parameter_uuid =:assetQuantityParameterGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("assetQuantityParameterGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = assetQuantityParameterGuid;

            AssetQuantityParameterManager assetQuantityParameterManagerObj = new AssetQuantityParameterManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    assetQuantityParameterManagerObj.UnitManager.UnitGuid = new Guid(reader["unit_uuid"].ToString());
                    assetQuantityParameterManagerObj.UnitManager.UnitName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["unit_name"].ToString());
                }
            }

            return assetQuantityParameterManagerObj;
        }
    }
}