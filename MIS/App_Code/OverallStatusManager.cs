﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class OverallStatusManager
    {
        #region Private Members

        private Guid overallStatusGuid;
        private string overallStatusName;
        private int overallStatusFormat;
       
        #endregion      


        #region Public Getter/Setter Properties

        public Guid OverallStatusGuid
        {
            get
            {
                return overallStatusGuid;
            }
            set
            {
                overallStatusGuid = value;
            }
        }

        public string OverallStatusName
        {
            get
            {
                return overallStatusName;
            }
            set
            {
                overallStatusName = value;
            }
        }

        public int OverallStatusFormat
        {
            get
            {
                return overallStatusFormat;
            }
            set
            {
                overallStatusFormat = value;
            }
        }
       
        #endregion
    }
}