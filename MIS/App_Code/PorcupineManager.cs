﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class PorcupineManager
    {
        #region Private Members

        private Guid porcupineGuid;
        private double screenLength;
        private double spacingAlongScreen;
        private int screenRows;
        private int numberOfLayers;
        private double memberLength;        
        private double lengthPorcupine;   
        //private AssetManager assetManager;
        private PorcupineTypeManager porcupineTypeManager;
        private PorcupineMaterialTypeManager porcupineMaterialTypeManager;

        #endregion


        #region Public Constructors

        public PorcupineManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid PorcupineGuid
        {
            get
            {
                return porcupineGuid;
            }
            set
            {
                porcupineGuid = value;
            }
        }

        public double ScreenLength
        {
            get
            {
                return screenLength;
            }
            set
            {
                screenLength = value;
            }
        }

        public double SpacingAlongScreen
        {
            get
            {
                return spacingAlongScreen;
            }
            set
            {
                spacingAlongScreen = value;
            }
        }

        public int ScreenRows
        {
            get
            {
                return screenRows;
            }
            set
            {
                screenRows = value;
            }
        }

        public int NumberOfLayers
        {
            get
            {
                return numberOfLayers;
            }
            set
            {
                numberOfLayers = value;
            }


        }

        public double MemberLength
        {
            get
            {
                return memberLength;
            }
            set
            {
                memberLength = value;
            }
        }
       
        public double LengthPorcupine
        {
            get
            {
                return lengthPorcupine;
            }
            set
            {
                lengthPorcupine = value;
            }
        }                    
                
        //public AssetManager AssetManager
        //{
        //    get
        //    {
        //        if (assetManager == null)
        //        {
        //            assetManager = new AssetManager();
        //        }
        //        return assetManager;
        //    }
        //    set
        //    {
        //        assetManager = value;
        //    }
        //}

        public PorcupineTypeManager PorcupineTypeManager
        {
            get
            {
                if (porcupineTypeManager == null)
                {
                    porcupineTypeManager = new PorcupineTypeManager();
                }
                return porcupineTypeManager;
            }
            set
            {
                porcupineTypeManager = value;
            }
        }

        public PorcupineMaterialTypeManager PorcupineMaterialTypeManager
        {
            get
            {
                if (porcupineMaterialTypeManager == null)
                {
                    porcupineMaterialTypeManager = new PorcupineMaterialTypeManager();
                }
                return porcupineMaterialTypeManager;
            }
            set
            {
                porcupineMaterialTypeManager = value;
            }
        }
        #endregion
    }
}