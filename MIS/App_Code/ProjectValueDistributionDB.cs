﻿using NodaMoney;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class ProjectValueDistributionDB
    {
        protected internal List<ProjectValueDistributionManager> GetHeadBudgets(Guid projectGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("head_budget ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_value_distribution ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = projectGuid;

            List<ProjectValueDistributionManager> headList = new List<ProjectValueDistributionManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    ProjectValueDistributionManager headObj = new ProjectValueDistributionManager();

                    //headObj.HeadBudget = Convert.ToInt64(reader["head_budget"].ToString());

                    headList.Add(headObj);
                }
            }

            return headList;
        }

        protected internal List<ProjectValueDistributionManager> GetProjectValueDistribution(Guid projectGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.financial_sub_head_budget, ");
            sqlCommand.Append("obj1.financial_sub_head_uuid, ");
            sqlCommand.Append("obj2.financial_sub_head_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_value_distribution obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_project_financial_sub_heads obj2 ON obj2.financial_sub_head_uuid = obj1.financial_sub_head_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.project_uuid = :projectGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj2.financial_sub_head_name ");
            sqlCommand.Append("; ");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = projectGuid;

            List<ProjectValueDistributionManager> projectValueDistributionManagerList = new List<ProjectValueDistributionManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    ProjectValueDistributionManager projectValueDistributionManagerObj = new ProjectValueDistributionManager();

                    projectValueDistributionManagerObj.FinancialSubHeadBudget = Money.Parse(reader["financial_sub_head_budget"].ToString(), Currency.FromCode("INR", "ISO-4217"));
                    projectValueDistributionManagerObj.FinancialSubHeadManager.FinancialSubHeadGuid = new Guid(reader["financial_sub_head_uuid"].ToString());
                    projectValueDistributionManagerObj.FinancialSubHeadManager.FinancialSubHeadName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["financial_sub_head_name"].ToString());

                    projectValueDistributionManagerList.Add(projectValueDistributionManagerObj);
                }
            }

            return projectValueDistributionManagerList;

        }

        protected internal int AddProjectValueDistribution(ProjectManager projectManager)
        {
            int rowsAffected = 0;

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_value_distribution ");
            sqlCommand.Append("( ");
            sqlCommand.Append("project_value_distribution_uuid, ");
            sqlCommand.Append("financial_sub_head_uuid, ");
            sqlCommand.Append("financial_sub_head_budget, ");
            sqlCommand.Append("project_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            if (projectManager.ProjectValueDistributionManagerList.Count > 0)
            {
                foreach (ProjectValueDistributionManager projectValueDistributionManagerObj in projectManager.ProjectValueDistributionManagerList)
                {
                    int index = projectManager.ProjectValueDistributionManagerList.IndexOf(projectValueDistributionManagerObj);

                    sqlCommand.Append("( ");

                    sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                    sqlCommand.Append("'" + projectValueDistributionManagerObj.FinancialSubHeadManager.FinancialSubHeadGuid + "', ");
                    sqlCommand.Append("'" + projectValueDistributionManagerObj.FinancialSubHeadBudget.Amount + "', ");
                    sqlCommand.Append("'" + projectManager.ProjectGuid + "' ");

                    if (index == projectManager.ProjectValueDistributionManagerList.Count - 1)
                    {
                        sqlCommand.Append(") ");
                    }
                    else
                    {
                        sqlCommand.Append("), ");
                    }
                }

                sqlCommand.Append(";");

                rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());
            }

            return rowsAffected;

        }
    }
}