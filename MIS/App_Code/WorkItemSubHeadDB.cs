﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class WorkItemSubHeadDB
    {
        protected internal WorkItemSubHeadManager GetWorkItemSubHead(Guid workItemSubHeadGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_sub_head_uuid, ");
            sqlCommand.Append("obj1.work_item_sub_head_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_item_sub_heads obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.work_item_sub_head_uuid = :workItemSubHeadGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("workItemSubHeadGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = workItemSubHeadGuid;

            WorkItemSubHeadManager workItemSubHeadManagerObj = new WorkItemSubHeadManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    workItemSubHeadManagerObj.WorkItemSubHeadGuid = new Guid(reader["work_item_sub_head_uuid"].ToString());
                    workItemSubHeadManagerObj.WorkItemSubHeadName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["work_item_sub_head_name"].ToString());
                }
            }

            return workItemSubHeadManagerObj;
        }


        protected internal List<WorkItemSubHeadManager> GetWorkItemSubHeads()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_sub_head_uuid, ");
            sqlCommand.Append("obj1.work_item_sub_head_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_item_sub_heads obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_sub_head_name ");
            sqlCommand.Append(";");

            List<WorkItemSubHeadManager> workItemSubHeadManagerList = new List<WorkItemSubHeadManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    WorkItemSubHeadManager workItemSubHeadManagerObj = new WorkItemSubHeadManager();

                    workItemSubHeadManagerObj.WorkItemSubHeadGuid = new Guid(reader["work_item_sub_head_uuid"].ToString());
                    workItemSubHeadManagerObj.WorkItemSubHeadName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["work_item_sub_head_name"].ToString());

                    workItemSubHeadManagerList.Add(workItemSubHeadManagerObj);
                }
            }

            return workItemSubHeadManagerList;
        }

        protected internal List<WorkItemSubHeadManager> GetWorkItemSubHeadsBasedOnWorkItem(Guid workItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            //sqlCommand.Append("SELECT ");
            //sqlCommand.Append("obj1.work_item_sub_head_uuid, ");
            //sqlCommand.Append("obj1.work_item_sub_head_name, ");
            //sqlCommand.Append("obj2.work_item_sub_head_sequence ");
            //sqlCommand.Append("FROM ");
            //sqlCommand.Append("mis_project_work_item_sub_heads obj1 ");
            //sqlCommand.Append("LEFT OUTER JOIN mis_project_work_item_and_work_item_sub_heads_relation obj2 ON obj2.work_item_sub_head_uuid = obj1.work_item_sub_head_uuid ");
            //sqlCommand.Append("WHERE ");
            //sqlCommand.Append("obj1.work_item_sub_head_uuid = ANY(SELECT obj3.work_item_sub_head_uuid FROM mis_project_work_item_and_work_item_sub_heads_relation obj3 WHERE obj3.work_item_uuid = :workItemGuid) ");
            //sqlCommand.Append("AND ");
            //sqlCommand.Append("obj2.work_item_uuid = :workItemGuid ");
            //sqlCommand.Append("ORDER BY ");
            //sqlCommand.Append("obj2.work_item_sub_head_sequence ");
            //sqlCommand.Append(";");

            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_sub_head_uuid, ");
            sqlCommand.Append("obj1.work_item_sub_head_name, ");
            sqlCommand.Append("obj2.work_item_sub_head_sequence ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_item_sub_heads obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_project_work_item_and_work_item_sub_heads_relation obj2 ON obj2.work_item_sub_head_uuid = obj1.work_item_sub_head_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj2.work_item_uuid = :workItemGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj2.work_item_sub_head_sequence ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("workItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = workItemGuid;

            List<WorkItemSubHeadManager> workItemSubHeadManagerList = new List<WorkItemSubHeadManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    WorkItemSubHeadManager workItemSubHeadManagerObj = new WorkItemSubHeadManager();

                    workItemSubHeadManagerObj.WorkItemSubHeadGuid = new Guid(reader["work_item_sub_head_uuid"].ToString());
                    workItemSubHeadManagerObj.WorkItemSubHeadName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["work_item_sub_head_name"].ToString());

                    workItemSubHeadManagerList.Add(workItemSubHeadManagerObj);
                }
            }

            return workItemSubHeadManagerList;
        }


        protected internal bool CheckWorkItemSubHeadExistence(string subHeadName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_item_sub_heads obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.work_item_sub_head_name = :subHeadName ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("subHeadName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(subHeadName);

            bool result = false;

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    result = true;
                }
            }

            return result;
        }


        protected internal int AddWorkItemSubHead(Guid workItemSubHeadGuid, string workItemSubHeadName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_work_item_sub_heads ");
            sqlCommand.Append("( ");
            sqlCommand.Append("work_item_sub_head_uuid, ");
            sqlCommand.Append("work_item_sub_head_name ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":workItemSubHeadGuid, ");
            sqlCommand.Append(":workItemSubHeadName ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("workItemSubHeadGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = workItemSubHeadGuid;

            arParams[1] = new NpgsqlParameter("workItemSubHeadName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(workItemSubHeadName);

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }


        protected internal int UpdateWorkItemSubHead(Guid workItemSubHeadGuid, string workItemSubHeadName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_project_work_item_sub_heads ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("work_item_sub_head_name = :workItemSubHeadName ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("work_item_sub_head_uuid = :workItemSubHeadGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("workItemSubHeadGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = workItemSubHeadGuid;

            arParams[1] = new NpgsqlParameter("workItemSubHeadName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(workItemSubHeadName);

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }


        protected internal int DeleteWorkItemSubHead(Guid workItemSubHeadGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_item_sub_heads ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("work_item_sub_head_uuid = :workItemSubHeadGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("workItemSubHeadGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = workItemSubHeadGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

    }
}