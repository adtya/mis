﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SubChainagePhysicalProgressManager
    {
        private Guid physicalProgressGuid;
        private DateTime physicalProgressDate;//Only Month and Year are useful
        private DateTime physicalProgressAddedOn;
        private bool canBeEdited;
        private SubChainageManager subChainageManager;
        private List<FremaaOfficerProgressManager> fremaaOfficerProgressManager;
        private List<PmcSiteEngineerProgressManager> pmcSiteEngineerProgressManager;
        private List<WorkItemProgressManager> workItemProgressManager;
        
        public Guid PhysicalProgressGuid
        {
            get
            {
                return physicalProgressGuid;
            }
            set
            {
                physicalProgressGuid = value;
            }
        }

        public DateTime PhysicalProgressDate
        {
            get
            {
                return physicalProgressDate;
            }
            set
            {
                physicalProgressDate = value;
            }
        }

        public DateTime PhysicalProgressAddedOn
        {
            get
            {
                return physicalProgressAddedOn;
            }
            set
            {
                physicalProgressAddedOn = value;
            }
        }

        public bool CanBeEdited
        {
            get
            {
                return canBeEdited;
            }
            set
            {
                canBeEdited = value;
            }
        }

        public SubChainageManager SubChainageManager
        {
            get
            {
                if (subChainageManager == null)
                {
                    subChainageManager = new SubChainageManager();
                }
                return subChainageManager;
            }
            set
            {
                subChainageManager = value;
            }
        }

        public List<FremaaOfficerProgressManager> FremaaOfficerProgressManager
        {
            get
            {
                if (fremaaOfficerProgressManager == null)
                {
                    fremaaOfficerProgressManager = new List<FremaaOfficerProgressManager>();
                }
                return fremaaOfficerProgressManager;
            }
            set
            {
                fremaaOfficerProgressManager = value;
            }
        }

        public List<PmcSiteEngineerProgressManager> PmcSiteEngineerProgressManager
        {
            get
            {
                if (pmcSiteEngineerProgressManager == null)
                {
                    pmcSiteEngineerProgressManager = new List<PmcSiteEngineerProgressManager>();
                }
                return pmcSiteEngineerProgressManager;
            }
            set
            {
                pmcSiteEngineerProgressManager = value;
            }
        }

        public List<WorkItemProgressManager> WorkItemProgressManager
        {
            get
            {
                if (workItemProgressManager == null)
                {
                    workItemProgressManager = new List<WorkItemProgressManager>();
                }
                return workItemProgressManager;
            }
            set
            {
                workItemProgressManager = value;
            }
        }
                
    }
}