﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class UserManager
    {
        #region Data Members

        private Guid userGuid;
        private RoleManager roleManager;
        private DesignationManager designationManager;
        private string email;
        private short title;
        private string firstName;
        private string middleName;
        private string lastName;
        private short gender;
        private string password;
        private bool isUserDeleted;
        private bool actAsFremaaOfficer;
        private bool actAsPmcSiteEngineer;

        #endregion


        #region Getter/Setter

        public Guid UserGuid
        {
            get
            {
                return userGuid;
            }
            set
            {
                userGuid = value;
            }
        }

        public RoleManager RoleManager
        {
            get
            {
                if (roleManager == null)
                {
                    roleManager = new RoleManager();
                }
                return roleManager;
            }
            set
            {
                roleManager = value;
            }
        }

        public DesignationManager DesignationManager
        {
            get
            {
                if (designationManager == null)
                {
                    designationManager = new DesignationManager();
                }
                return designationManager;
            }
            set
            {
                designationManager = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        public short Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        public string MiddleName
        {
            get
            {
                return middleName;
            }
            set
            {
                middleName = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        public short Gender
        {
            get
            {
                return gender;
            }
            set
            {
                gender = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        public bool IsUserDeleted
        {
            get
            {
                return isUserDeleted;
            }
            set
            {
                isUserDeleted = value;
            }
        }

        public bool ActAsFremaaOfficer
        {
            get
            {
                return actAsFremaaOfficer;
            }
            set
            {
                actAsFremaaOfficer = value;
            }
        }

        public bool ActAsPmcSiteEngineer
        {
            get
            {
                return actAsPmcSiteEngineer;
            }
            set
            {
                actAsPmcSiteEngineer = value;
            }
        }

        #endregion

    }
}