﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SpurConstructionTypeManager
    {
         #region Private Members

        private Guid constructionTypeGuid;
        private string constructionTypeName;
       
       #endregion


        #region Public Constructors

        public SpurConstructionTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid ConstructionTypeGuid
        {
            get
            {
                return constructionTypeGuid;
            }
            set
            {
                constructionTypeGuid = value;
            }
        }

        public string ConstructionTypeName
        {
            get
            {
                return constructionTypeName;
            }
            set
            {
                constructionTypeName = value;
            }
        }        

        #endregion
    }
}