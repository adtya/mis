﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RelationOfAssetMonitoringItemTypeManager
    {
        #region Private Members

        private Guid relationGuid;
        private AssetTypeManager assetTypeManager;
        private MonitoringItemTypeManager monitoringItemTypeManager;

        #endregion


        #region Public Constructors

        public RelationOfAssetMonitoringItemTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid RelationGuid
        {
            get
            {
                return relationGuid;
            }
            set
            {
                relationGuid = value;
            }
        }

        public AssetTypeManager AssetTypeManager
        {
            get
            {
                if (assetTypeManager == null)
                {
                    assetTypeManager = new AssetTypeManager();
                }
                return assetTypeManager;
            }
            set
            {
                assetTypeManager = value;
            }
        }

        public MonitoringItemTypeManager MonitoringItemTypeManager
        {
            get
            {
                if (monitoringItemTypeManager == null)
                {
                    monitoringItemTypeManager = new MonitoringItemTypeManager();
                }
                return monitoringItemTypeManager;
            }
            set
            {
                monitoringItemTypeManager = value;
            }
        }

        #endregion
    }
}