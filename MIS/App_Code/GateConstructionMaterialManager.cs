﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class GateConstructionMaterialManager
    {
        #region Private Members

        private Guid gateConstructionMaterialGuid;
        private string gateConstructionMaterialName;

        #endregion

        #region Public Getter/Setter Properties

        public Guid GateConstructionMaterialGuid
        {
            get
            {
                return gateConstructionMaterialGuid;
            }
            set
            {
                gateConstructionMaterialGuid = value;
            }
        }

        public string GateConstructionMaterialName
        {
            get
            {
                return gateConstructionMaterialName;
            }
            set
            {
                gateConstructionMaterialName = value;
            }
        }

        #endregion
    }
}