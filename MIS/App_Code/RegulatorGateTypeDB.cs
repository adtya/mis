﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RegulatorGateTypeDB
    {
        protected internal List<RegulatorGateTypeManager> GetRegGateTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("regulator_gate_type_uuid, ");
            sqlCommand.Append("regulator_gate_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_regulator_gate_type ");
            sqlCommand.Append("ORDER BY regulator_gate_type_name ");
            sqlCommand.Append(";");

            List<RegulatorGateTypeManager> regGateTypeManagerList = new List<RegulatorGateTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RegulatorGateTypeManager regGateTypeManagerObj = new RegulatorGateTypeManager();

                    regGateTypeManagerObj.RegulatorGateTypeGuid = new Guid(reader["regulator_gate_type_uuid"].ToString());
                    regGateTypeManagerObj.RegulatorGateTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["regulator_gate_type_name"].ToString());

                    regGateTypeManagerList.Add(regGateTypeManagerObj);
                }
            }

            return regGateTypeManagerList;
        }
    }
}