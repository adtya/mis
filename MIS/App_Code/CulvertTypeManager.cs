﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class CulvertTypeManager
    {
        #region Private Members

        private Guid culvertTypeGuid;
        private string culvertTypeName;
       
       #endregion


        #region Public Constructors

        public CulvertTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid CulvertTypeGuid
        {
            get
            {
                return culvertTypeGuid;
            }
            set
            {
                culvertTypeGuid = value;
            }
        }

        public string CulvertTypeName
        {
            get
            {
                return culvertTypeName;
            }
            set
            {
                culvertTypeName = value;
            }
        }        

        #endregion
    }
}