﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SpurManager
    {
        #region Private Members

        private Guid spurGuid;
        private double orientation;
        private double lengthSpur;
        private double widthSpur;                 
       // private AssetManager assetManager;
        private SpurConstructionTypeManager spurConstructionTypeManager;
        private SpurRevetTypeManager spurRevetTypeManager;
        private SpurShapeTypeManager spurShapeTypeManager;   

        #endregion


        #region Public Constructors

        public SpurManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid SpurGuid
        {
            get
            {
                return spurGuid;
            }
            set
            {
                spurGuid = value;
            }
        }

        public double Orientation
        {
            get
            {
                return orientation;
            }
            set
            {
                orientation = value;
            }
        }

        public double LengthSpur
        {
            get
            {
                return lengthSpur;
            }
            set
            {
                lengthSpur = value;
            }
        }

        public double WidthSpur
        {
            get
            {
                return widthSpur;
            }
            set
            {
                widthSpur = value;
            }
        }
        
        public SpurConstructionTypeManager SpurConstructionTypeManager
        {
            get
            {
                if (spurConstructionTypeManager == null)
                {
                    spurConstructionTypeManager = new SpurConstructionTypeManager();
                }
                return spurConstructionTypeManager;
            }
            set
            {
                spurConstructionTypeManager = value;
            }
        }

        public SpurRevetTypeManager SpurRevetTypeManager
        {
            get
            {
                if (spurRevetTypeManager == null)
                {
                    spurRevetTypeManager = new SpurRevetTypeManager();
                }
                return spurRevetTypeManager;
            }
            set
            {
                spurRevetTypeManager = value;
            }
        }

        public SpurShapeTypeManager SpurShapeTypeManager
        {
            get
            {
                if (spurShapeTypeManager == null)
                {
                    spurShapeTypeManager = new SpurShapeTypeManager();
                }
                return spurShapeTypeManager;
            }
            set
            {
                spurShapeTypeManager = value;
            }
        }

        //public AssetManager AssetManager
        //{
        //    get
        //    {
        //        if (assetManager == null)
        //        {
        //            assetManager = new AssetManager();
        //        }
        //        return assetManager;
        //    }
        //    set
        //    {
        //        assetManager = value;
        //    }
        //}

        #endregion
    }
}