﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SpurRevetTypeDB
    {
        protected internal List<SpurRevetTypeManager> GetSpurRevetTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("spur_revet_type_uuid, ");
            sqlCommand.Append("spur_revet_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_spur_revet_type ");
            sqlCommand.Append("ORDER BY spur_revet_type_name ");
            sqlCommand.Append(";");

            List<SpurRevetTypeManager> spurRevetTypeManagerList = new List<SpurRevetTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    SpurRevetTypeManager spurRevetTypeManagerObj = new SpurRevetTypeManager();

                    spurRevetTypeManagerObj.SpurRevetTypeGuid = new Guid(reader["spur_revet_type_uuid"].ToString());
                    spurRevetTypeManagerObj.SpurRevetTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["spur_revet_type_name"].ToString());

                    spurRevetTypeManagerList.Add(spurRevetTypeManagerObj);
                }
            }

            return spurRevetTypeManagerList;
        }
    }
}