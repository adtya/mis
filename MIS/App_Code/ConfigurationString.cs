﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public static class ConnectionString
    {
        public static String GetReadConnectionString()
        {
            return ConfigurationManager.AppSettings["PostgreSQLConnectionString"];
        }

        public static String GetWriteConnectionString()
        {
            return ConfigurationManager.AppSettings["PostgreSQLConnectionString"];
        }
    }
}