﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SubChainageAndWorkItemsRelationDB
    {
        protected internal int AddSubChainageAndWorkItemsRelation(List<SubChainageManager> subChainageManagerList)
        {
            int rowsAffected = 0;

            List<SubChainageAndWorkItemsRelationManager> subChainageAndWorkItemsRelationManagerList = new List<SubChainageAndWorkItemsRelationManager>();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_sub_chainage_and_work_items_relation ");
            sqlCommand.Append("( ");
            sqlCommand.Append("sub_chainage_and_work_items_relation_uuid, ");
            sqlCommand.Append("sub_chainage_uuid, ");
            sqlCommand.Append("work_item_uuid, ");
            sqlCommand.Append("work_item_total_value, ");
            sqlCommand.Append("work_item_cumulative_value, ");
            sqlCommand.Append("work_item_balance_value ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            if (subChainageManagerList.Count > 0)
            {
                foreach (SubChainageManager subChainageManagerObj in subChainageManagerList)
                {
                    if (subChainageManagerObj.WorkItemManager.Count > 0)
                    {
                        foreach (WorkItemManager workItemManagerObj in subChainageManagerObj.WorkItemManager)
                        {
                            SubChainageAndWorkItemsRelationManager subChainageAndWorkItemsRelationManagerObj = new SubChainageAndWorkItemsRelationManager();

                            subChainageAndWorkItemsRelationManagerObj.SubChainageAndWorkItemRelationGuid = Guid.NewGuid();
                            subChainageAndWorkItemsRelationManagerObj.SubChainageManager.SubChainageGuid = subChainageManagerObj.SubChainageGuid;
                            subChainageAndWorkItemsRelationManagerObj.WorkItemManager.WorkItemGuid = workItemManagerObj.WorkItemGuid;
                            subChainageAndWorkItemsRelationManagerObj.WorkItemManager.WorkItemTotalValue = workItemManagerObj.WorkItemTotalValue;
                            subChainageAndWorkItemsRelationManagerObj.WorkItemManager.WorkItemCumulativeValue = "0";
                            subChainageAndWorkItemsRelationManagerObj.WorkItemManager.WorkItemBalanceValue = workItemManagerObj.WorkItemTotalValue;

                            subChainageAndWorkItemsRelationManagerList.Add(subChainageAndWorkItemsRelationManagerObj);
                        }
                    }
                }
            }


            if (subChainageAndWorkItemsRelationManagerList.Count > 0)
            {
                foreach (SubChainageAndWorkItemsRelationManager subChainageAndWorkItemsRelationManagerObj in subChainageAndWorkItemsRelationManagerList)
                {
                    int index = subChainageAndWorkItemsRelationManagerList.IndexOf(subChainageAndWorkItemsRelationManagerObj);

                    sqlCommand.Append("( ");

                    sqlCommand.Append("'" + subChainageAndWorkItemsRelationManagerObj.SubChainageAndWorkItemRelationGuid + "', ");
                    sqlCommand.Append("'" + subChainageAndWorkItemsRelationManagerObj.SubChainageManager.SubChainageGuid + "', ");
                    sqlCommand.Append("'" + subChainageAndWorkItemsRelationManagerObj.WorkItemManager.WorkItemGuid + "', ");
                    sqlCommand.Append("'" + subChainageAndWorkItemsRelationManagerObj.WorkItemManager.WorkItemTotalValue + "', ");
                    sqlCommand.Append("'" + subChainageAndWorkItemsRelationManagerObj.WorkItemManager.WorkItemCumulativeValue + "', ");
                    sqlCommand.Append("'" + subChainageAndWorkItemsRelationManagerObj.WorkItemManager.WorkItemBalanceValue + "' ");

                    if (index == subChainageAndWorkItemsRelationManagerList.Count - 1)
                    {
                        sqlCommand.Append(") ");
                    }
                    else
                    {
                        sqlCommand.Append("), ");
                    }
                }

                sqlCommand.Append(";");

                rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());
            }

            return rowsAffected;

        }

        protected internal int UpdateSubChainageAndWorkItemsRelationValues(SubChainagePhysicalProgressManager subChainagePhysicalProgressManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_project_sub_chainage_and_work_items_relation AS obj1 ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("work_item_cumulative_value = obj2.work_item_cumulative_value, ");
            sqlCommand.Append("work_item_balance_value = obj2.work_item_balance_value ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("( ");
            sqlCommand.Append("VALUES ");

            

            if (subChainagePhysicalProgressManager.WorkItemProgressManager.Count > 0)
            {
                foreach (WorkItemProgressManager workItemProgressManagerObj in subChainagePhysicalProgressManager.WorkItemProgressManager)
                {
                    int index = subChainagePhysicalProgressManager.WorkItemProgressManager.IndexOf(workItemProgressManagerObj);

                    sqlCommand.Append("( ");

                    //sqlCommand.Append("'" + workItemProgressManagerObj.WorkItemManager.SubChainageAndWorkItemsRelationManager.SubChainageAndWorkItemRelationGuid + "', ");
                    sqlCommand.Append("'" + subChainagePhysicalProgressManager.SubChainageManager.SubChainageGuid + "', ");
                    sqlCommand.Append("'" + workItemProgressManagerObj.WorkItemManager.WorkItemGuid + "', ");
                    sqlCommand.Append("'" + workItemProgressManagerObj.WorkItemManager.WorkItemCumulativeValue + "', ");
                    sqlCommand.Append("'" + workItemProgressManagerObj.WorkItemManager.WorkItemBalanceValue + "' ");

                    sqlCommand.Append(") ");

                    if (index < subChainagePhysicalProgressManager.WorkItemProgressManager.Count - 1)
                    {
                        sqlCommand.Append(", ");
                    }
                }
            }

            //sqlCommand.Append(") AS obj2(sub_chainage_and_work_items_relation_uuid, work_item_cumulative_value, work_item_balance_value) ");
            sqlCommand.Append(") AS obj2(sub_chainage_uuid, work_item_uuid, work_item_cumulative_value, work_item_balance_value) ");

            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj2.sub_chainage_uuid::uuid = obj1.sub_chainage_uuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj2.work_item_uuid::uuid = obj1.work_item_uuid ");
            sqlCommand.Append("; ");

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());

            return rowsAffected;

        }
    }
}