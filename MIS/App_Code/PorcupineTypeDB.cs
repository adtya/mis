﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class PorcupineTypeDB
    {
        protected internal List<PorcupineTypeManager> GetPorcupineTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("porcupine_type_uuid, ");
            sqlCommand.Append("porcupine_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_porcupine_type ");
            sqlCommand.Append("ORDER BY porcupine_type_name ");
            sqlCommand.Append(";");

            List<PorcupineTypeManager> porcupineTypeManagerList = new List<PorcupineTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    PorcupineTypeManager porcupineTypeManagerObj = new PorcupineTypeManager();

                    porcupineTypeManagerObj.PorcupineTypeGuid = new Guid(reader["porcupine_type_uuid"].ToString());
                    porcupineTypeManagerObj.PorcupineTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["porcupine_type_name"].ToString());

                    porcupineTypeManagerList.Add(porcupineTypeManagerObj);
                }
            }

            return porcupineTypeManagerList;
        }
    }
}