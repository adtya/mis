﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class CanalManager
    {
        #region Private Members

        private Guid canalGuid;  
        private double usElevation;             
        private double dsElevation;
        private double length;
        private double bedWidth;   
        private double slope1;
        private double slope2;
        private double averageDepth;
       // private AssetManager assetManager;
        private CanalLiningTypeManager canalLiningTypeManager;       

        #endregion


        #region Public Constructors

        public CanalManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid CanalGuid
        {
            get
            {
                return canalGuid;
            }
            set
            {
                canalGuid = value;
            }
        }

        public double UsElevation
        {
            get
            {
                return usElevation;
            }
            set
            {
                usElevation = value;
            }
        }

        public double DSElevation
        {
            get
            {
                return dsElevation;
            }
            set
            {
                dsElevation = value;
            }
        }

        public double Length
        {
            get
            {
                return length;
            }
            set
            {
                length = value;
            }
        }

        public double BedWidth
        {
            get
            {
                return bedWidth;
            }
            set
            {
                bedWidth = value;
            }
        } 
        
        public double Slope1
        {
            get
            {
                return slope1;
            }
            set
            {
                slope1 = value;
            }
        }
       
        public double Slope2
        {
            get
            {
                return slope2;
            }
            set
            {
                slope2 = value;
            }
        }

        public double AverageDepth
        {
            get
            {
                return averageDepth;
            }
            set
            {
                averageDepth = value;
            }
        }

        public CanalLiningTypeManager CanalLiningTypeManager
        {
            get
            {
                if (canalLiningTypeManager == null)
                {
                    canalLiningTypeManager = new CanalLiningTypeManager();
                }
                return canalLiningTypeManager;
            }
            set
            {
                canalLiningTypeManager = value;
            }
        }   
     
        // public AssetManager AssetManager
        //{
        //    get
        //    {
        //        if (assetManager == null)
        //        {
        //            assetManager = new AssetManager();
        //        }
        //        return assetManager;
        //    }
        //    set
        //    {
        //        assetManager = value;
        //    }
        //}

        #endregion
    }
}