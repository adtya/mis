﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class GaugeSeasonTypeManager
    {
         #region Private Members

        private Guid seasonTypeGuid;
        private string seasonTypeName;
       
       #endregion


        #region Public Constructors

        public GaugeSeasonTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid SeasonTypeGuid
        {
            get
            {
                return seasonTypeGuid;
            }
            set
            {
                seasonTypeGuid = value;
            }
        }

        public string SeasonTypeName
        {
            get
            {
                return seasonTypeName;
            }
            set
            {
                seasonTypeName = value;
            }
        }        

        #endregion
    }
}