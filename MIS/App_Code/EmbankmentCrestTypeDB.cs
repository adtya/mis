﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class EmbankmentCrestTypeDB
    {
        protected internal List<EmbankmentCrestTypeManager> GetEmbankmentCrestTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("crest_type_uuid, ");
            sqlCommand.Append("crest_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_embankment_crest_type ");
            sqlCommand.Append("ORDER BY crest_type_name ");
            sqlCommand.Append(";");

            List<EmbankmentCrestTypeManager> embankmentCrestTypeManagerList = new List<EmbankmentCrestTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    EmbankmentCrestTypeManager embankmentCrestTypeManagerObj = new EmbankmentCrestTypeManager();

                    embankmentCrestTypeManagerObj.CrestTypeGuid = new Guid(reader["crest_type_uuid"].ToString());
                    embankmentCrestTypeManagerObj.CrestTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["crest_type_name"].ToString());

                    embankmentCrestTypeManagerList.Add(embankmentCrestTypeManagerObj);
                }
            }

            return embankmentCrestTypeManagerList;
        }
    }
}