﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class DepartmentManager
    {
        private Guid departmentGuid;
        private string departmentName;

        public Guid DepartmentGuid
        {
            get
            {
                return departmentGuid;
            }
            set
            {
                departmentGuid = value;
            }
        }

        public string DepartmentName
        {
            get
            {
                return departmentName;
            }
            set
            {
                departmentName = value;
            }
        }
    }
}