﻿using NodaMoney;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    [Serializable]
    public class ProjectValueDistributionHeadManager
    {
        public ProjectValueDistributionHeadManager()
        {

        }
        
        private Guid headGuid;
        private string headName;
        private Money headBudget;
        private decimal headBudgetInput;
        private ProjectManager projectManager;

        public Guid HeadGuid
        {
            get
            {
                return headGuid;
            }
            set
            {
                headGuid = value;
            }
        }

        public string HeadName
        {
            get
            {
                return headName;
            }
            set
            {
                headName = value;
            }
        }

        public Money HeadBudget
        {
            get
            {
                if (headBudget.Amount.Equals(0))
                {
                    headBudget = headBudgetInput;
                }
                return headBudget;
            }
            set
            {
                headBudget = value;
            }
        }

        public decimal HeadBudgetInput
        {
            private get
            {
                return headBudgetInput;
            }
            set
            {
                headBudgetInput = value;
            }
        }

        public ProjectManager ProjectManager
        {
            get
            {
                if (projectManager == null)
                {
                    projectManager = new ProjectManager();
                }
                return projectManager;
            }
            set
            {
                projectManager = value;
            }
        }
        
    }
}