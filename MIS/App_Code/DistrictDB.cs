﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class DistrictDB
    {
        protected internal DistrictManager GetDistrict(Guid districtGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.district_uuid, ");
            sqlCommand.Append("obj1.district_code, ");
            sqlCommand.Append("obj1.district_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_districts obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.district_uuid = :districtGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("districtGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = districtGuid;

            DistrictManager districtManagerObj = new DistrictManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    districtManagerObj.DistrictGuid = new Guid(reader["district_uuid"].ToString());
                    districtManagerObj.DistrictCode = reader["district_code"].ToString();
                    districtManagerObj.DistrictName = reader["district_name"].ToString();
                }
            }

            return districtManagerObj;
        }

        protected internal List<DistrictManager> GetDistricts()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.district_uuid, ");
            sqlCommand.Append("obj1.district_code, ");
            sqlCommand.Append("obj1.district_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_districts obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.district_name ");
            sqlCommand.Append(";");

            List<DistrictManager> districtManagerList = new List<DistrictManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    DistrictManager districtManagerObj = new DistrictManager();

                    districtManagerObj.DistrictGuid = new Guid(reader["district_uuid"].ToString());
                    districtManagerObj.DistrictCode = reader["district_code"].ToString();
                    districtManagerObj.DistrictName = reader["district_name"].ToString();

                    districtManagerList.Add(districtManagerObj);
                }
            }
            return districtManagerList;
        }

        protected internal void AddDistrict(string districtName, string districtCode)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_districts ");
            sqlCommand.Append("( ");
            sqlCommand.Append("district_uuid, ");
            sqlCommand.Append("district_name, ");
            sqlCommand.Append("district_code, ");
            sqlCommand.Append("state_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":districtGuid, ");
            sqlCommand.Append(":districtName, ");
            sqlCommand.Append(":districtCode, ");
            sqlCommand.Append(":stateGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[4];

            arParams[0] = new NpgsqlParameter("districtGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("districtName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = districtName;

            arParams[2] = new NpgsqlParameter("districtCode", NpgsqlTypes.NpgsqlDbType.Varchar, 2);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = districtCode;

            arParams[3] = new NpgsqlParameter("stateGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = new Guid();

            int result = Convert.ToInt32(NpgsqlHelper.ExecuteScalar(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams));

        }

        
    }
}