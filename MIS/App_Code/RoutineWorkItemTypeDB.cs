﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineWorkItemTypeDB
    {
        protected internal List<RoutineWorkItemTypeManager> GetWorkItemType(Guid routineWorkItemClassGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_work_item_type_uuid, ");
            sqlCommand.Append("obj1.routine_work_item_type_name, ");
            sqlCommand.Append("obj1.routine_work_item_type_full_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_work_item_type obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_class_uuid = :routineWorkItemClassGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.routine_work_item_type_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("routineWorkItemClassGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = routineWorkItemClassGuid;

            List<RoutineWorkItemTypeManager> routineWorkItemTypeManagerList = new List<RoutineWorkItemTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    RoutineWorkItemTypeManager routineWorkItemTypeManagerObj = new RoutineWorkItemTypeManager();

                    routineWorkItemTypeManagerObj.RoutineWorkItemTypeGuid = new Guid(reader["routine_work_item_type_uuid"].ToString());
                    routineWorkItemTypeManagerObj.RoutineWorkItemTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["routine_work_item_type_name"].ToString());
                    routineWorkItemTypeManagerObj.RoutineWorkItemTypeFullName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["routine_work_item_type_full_name"].ToString());

                    routineWorkItemTypeManagerList.Add(routineWorkItemTypeManagerObj);
                }
            }
            return routineWorkItemTypeManagerList;
        }

        protected internal RoutineWorkItemTypeManager GetWorkItemNumber(Guid routineWorkItemTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_work_item_type_number ");        
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_work_item_type obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_type_uuid = :routineWorkItemTypeGuid ");            
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("routineWorkItemTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = routineWorkItemTypeGuid;

            RoutineWorkItemTypeManager routineWorkItemTypeManagerObj = new RoutineWorkItemTypeManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    routineWorkItemTypeManagerObj.RoutineWorkItemTypeNumber = Convert.ToInt16(reader["routine_work_item_type_number"]);
                }
            }
            return routineWorkItemTypeManagerObj;
        }

        protected internal int UpdateWorkItemTypeNumber(RoutineWorkItemManager routineWorkItemManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_om_routine_work_item_type ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("routine_work_item_type_number = :routineWorkItemTypeNumber ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("routine_work_item_type_uuid = :routineWorkItemTypeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("routineWorkItemTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemManager.RoutineWorkItemTypeManager.RoutineWorkItemTypeGuid;

            arParams[1] = new NpgsqlParameter("routineWorkItemTypeNumber", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = Convert.ToInt16(routineWorkItemManager.RoutineWorkItemTypeManager.RoutineWorkItemTypeNumber + 1);

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}