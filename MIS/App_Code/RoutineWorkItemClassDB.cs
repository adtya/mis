﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineWorkItemClassDB
    {
        protected internal List<RoutineWorkItemClassManager> GetWorkItemClass()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_work_item_class_uuid, ");
            sqlCommand.Append("obj1.routine_work_item_class_name ");       
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_work_item_class obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("routine_work_item_class_name ");
            sqlCommand.Append(";");

            List<RoutineWorkItemClassManager> routineWorkItemClassManagerList = new List<RoutineWorkItemClassManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RoutineWorkItemClassManager routineWorkItemClassManagerObj = new RoutineWorkItemClassManager();

                    routineWorkItemClassManagerObj.RoutineWorkItemClassGuid = new Guid(reader["routine_work_item_class_uuid"].ToString());
                    routineWorkItemClassManagerObj.RoutineWorkItemClassName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_class_name"].ToString());                   

                    routineWorkItemClassManagerList.Add(routineWorkItemClassManagerObj);
                }
            }

            return routineWorkItemClassManagerList;
        }
    }
}