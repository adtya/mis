﻿using NodaMoney;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RepairAnalysisManager
    {
        #region Private Members

        private Guid repairAnalysisGuid;
        private ProbabilityFailureManager probabilityFailureManager;
        private CurrentAssetFunctionalityManager currentAssetFunctionalityManager;
        private RepairAnalysisStatusManger repairAnalysisStatusManger;
        private AssetManager assetManager;
        private int repairIntensityFactor;
        private int total;
        private Money repairCost;
        private decimal repairCostInput;
        private DateTime submissionDate;
        private string intensityFactorJustification;
        private string repairDescription;
        private string failureModeDescription;
        private string damageDescription;
        private SystemVariablesManager systemVariablesManager;
        private AssetCodeAndAssetRiskRatingRelationManager assetCodeAndAssetRiskRatingRelationManager;
        private SchemeCodeAndSchemeRiskRatingRelationManager schemeCodeAndSchemeRiskRatingRelationManager;
              
        
        #endregion


        #region Public Constructors

        public RepairAnalysisManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid RepairAnalysisGuid
        {
            get
            {
                return repairAnalysisGuid;
            }
            set
            {
                repairAnalysisGuid = value;
            }
        }

        public ProbabilityFailureManager ProbabilityFailureManager
        {
            get
            {
                if (probabilityFailureManager == null)
                {
                    probabilityFailureManager = new ProbabilityFailureManager();
                }
                return probabilityFailureManager;
            }
            set
            {
                probabilityFailureManager = value;
            }
        }

        public CurrentAssetFunctionalityManager CurrentAssetFunctionalityManager
        {
            get
            {
                if (currentAssetFunctionalityManager == null)
                {
                    currentAssetFunctionalityManager = new CurrentAssetFunctionalityManager();
                }
                return currentAssetFunctionalityManager;
            }
            set
            {
                currentAssetFunctionalityManager = value;
            }
        }

        public RepairAnalysisStatusManger RepairAnalysisStatusManger
        {
            get
            {
                if (repairAnalysisStatusManger == null)
                {
                    repairAnalysisStatusManger = new RepairAnalysisStatusManger();
                }
                return repairAnalysisStatusManger;
            }
            set
            {
                repairAnalysisStatusManger = value;
            }
        }

        public AssetManager AssetManager
        {
            get
            {
                if (assetManager == null)
                {
                    assetManager = new AssetManager();
                }
                return assetManager;
            }
            set
            {
                assetManager = value;
            }
        }

        public int RepairIntensityFactor
        {
            get
            {
                return repairIntensityFactor;
            }
            set
            {
                repairIntensityFactor = value;
            }
        }

        public int Total
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
            }
        }

        public Money RepairCost
        {
            get
            {
                if (repairCost.Amount.Equals(0))
                {
                    repairCost = RepairCostInput;
                }
                return repairCost;
            }
            set
            {
                repairCost = value;
            }
        }

        public decimal RepairCostInput
        {
            private get
            {
                return repairCostInput;
            }
            set
            {
                repairCostInput = value;
            }
        }

        public DateTime SubmissionDate
        {
            get
            {
                return submissionDate;
            }
            set
            {
                submissionDate = value;
            }
        }

        public string IntensityFactorJustification
        {
            get
            {
                return intensityFactorJustification;
            }
            set
            {
                intensityFactorJustification = value;
            }
        }

        public string RepairDescription
        {
            get
            {
                return repairDescription;
            }
            set
            {
                repairDescription = value;
            }
        }

        public string FailureModeDescription
        {
            get
            {
                return failureModeDescription;
            }
            set
            {
                failureModeDescription = value;
            }
        }

        public string DamageDescription
        {
            get
            {
                return damageDescription;
            }
            set
            {
                damageDescription = value;
            }
        }

        public SystemVariablesManager SystemVariablesManager
        {
            get
            {
                if (systemVariablesManager == null)
                {
                    systemVariablesManager = new SystemVariablesManager();
                }
                return systemVariablesManager;
            }
            set
            {
                systemVariablesManager = value;
            }
        }

        public AssetCodeAndAssetRiskRatingRelationManager AssetCodeAndAssetRiskRatingRelationManager
        {
            get
            {
                if (assetCodeAndAssetRiskRatingRelationManager == null)
                {
                    assetCodeAndAssetRiskRatingRelationManager = new AssetCodeAndAssetRiskRatingRelationManager();
                }
                return assetCodeAndAssetRiskRatingRelationManager;
            }
            set
            {
                assetCodeAndAssetRiskRatingRelationManager = value;
            }
        }

        public SchemeCodeAndSchemeRiskRatingRelationManager SchemeCodeAndSchemeRiskRatingRelationManager
        {
            get
            {
                if (schemeCodeAndSchemeRiskRatingRelationManager == null)
                {
                    schemeCodeAndSchemeRiskRatingRelationManager = new SchemeCodeAndSchemeRiskRatingRelationManager();
                }
                return schemeCodeAndSchemeRiskRatingRelationManager;
            }
            set
            {
                schemeCodeAndSchemeRiskRatingRelationManager = value;
            }
        }        

        #endregion
    }
}