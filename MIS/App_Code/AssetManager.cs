﻿using NodaMoney;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class AssetManager
    {
        #region Private Members

        private Guid assetGuid;
        private DivisionManager divisionManager;
        private SchemeManager schemeManager;
        private AssetTypeManager assetTypeManager;
        private Guid keyAssetGuid;
        private List<AssetManager> appurtenantAssetManager;
        private string assetCode;
        private string drawingCode;
        private string amtd;
        private string assetName;
        private int startChainage;
        private int endChainage;
        private int utmEast1;
        private int utmEast2;
        private int utmNorth1;
        private int utmNorth2;
        private DateTime initiationDate;
        private DateTime completionDate;
        private Money assetCost;
        private decimal assetCostInput;
        private int assetNumber;        
        private BridgeManager bridgeManager;
        private CanalManager canalManager;
        private CulvertManager culvertManager;
        private DrainageManager drainageManager;
        private DropStructureManager dropStructureManager;
        private EmbankmentManager embankmentManager;
        private GaugeManager gaugeManager;
        private PorcupineManager porcupineManager;
        private RegulatorManager regulatorManager;
        private RevetmentManager revetmentManager;
        private SluiceManager sluiceManager;
        private SpurManager spurManager;
        private TurnoutManager turnoutManager;
        private WeirManager weirManager;
        private AssetDrawingManager assetDrawingManager;
        
        
        #endregion


        #region Public Constructors

        public AssetManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid AssetGuid
        {
            get
            {
                return assetGuid;
            }
            set
            {
                assetGuid = value;
            }
        }

        public DivisionManager DivisionManager
        {
            get
            {
                if (divisionManager == null)
                {
                    divisionManager = new DivisionManager();
                }
                return divisionManager;
            }
            set
            {
                divisionManager = value;
            }
        }

        public SchemeManager SchemeManager
        {
            get
            {
                if (schemeManager == null)
                {
                    schemeManager = new SchemeManager();
                }
                return schemeManager;
            }
            set
            {
                schemeManager = value;
            }
        }

        public AssetTypeManager AssetTypeManager
        {
            get
            {
                if (assetTypeManager == null)
                {
                    assetTypeManager = new AssetTypeManager();
                }
                return assetTypeManager;
            }
            set
            {
                assetTypeManager = value;
            }
        }


        public Guid KeyAssetGuid
        {
            get
            {
                return keyAssetGuid;
            }
            set
            {
                keyAssetGuid = value;
            }
        }

        public string AssetCode
        {
            get
            {
                return assetCode;
            }
            set
            {
                assetCode = value;
            }
        }

        public string DrawingCode
        {
            get
            {
                return drawingCode;
            }
            set
            {
                drawingCode = value;
            }
        }

        public string Amtd
        {
            get
            {
                return amtd;
            }
            set
            {
                amtd = value;
            }
        }

        public string AssetName
        {
            get
            {
                return assetName;
            }
            set
            {
                assetName = value;
            }
        }

        public int StartChainage
        {
            get
            {
                return startChainage;
            }
            set
            {
                startChainage = value;
            }
        }

        public int EndChainage
        {
            get
            {
                return endChainage;
            }
            set
            {
                endChainage = value;
            }
        }

        public int UtmEast1
        {
            get
            {
                return utmEast1;
            }
            set
            {
                utmEast1 = value;
            }
        }

        public int UtmEast2
        {
            get
            {
                return utmEast2;
            }
            set
            {
                utmEast2 = value;
            }
        }

        public int UtmNorth1
        {
            get
            {
                return utmNorth1;
            }
            set
            {
                utmNorth1 = value;
            }
        }

        public int UtmNorth2
        {
            get
            {
                return utmNorth2;
            }
            set
            {
                utmNorth2 = value;
            }
        }

        public DateTime InitiationDate
        {
            get
            {
                return initiationDate;
            }
            set
            {
                initiationDate = value;
            }
        }

        public DateTime CompletionDate
        {
            get
            {
                return completionDate;
            }
            set
            {
                completionDate = value;
            }
        }

        public Money AssetCost
        {
            get
            {
                if (assetCost.Amount.Equals(0))
                {
                    assetCost = AssetCostInput;
                }
                return assetCost;
            }
            set
            {
                assetCost = value;
            }
        }

        public decimal AssetCostInput
        {
            private get
            {
                return assetCostInput;
            }
            set
            {
                assetCostInput = value;
            }
        }

        public int AssetNumber
        {
            get
            {
                return assetNumber;
            }
            set
            {
                assetNumber = value;
            }
        }

        public BridgeManager BridgeManager
        {
            get
            {
                if (bridgeManager == null)
                {
                    bridgeManager = new BridgeManager();
                }
                return bridgeManager;
            }
            set
            {
                bridgeManager = value;
            }
        }

        public CanalManager CanalManager
        {
            get
            {
                if (canalManager == null)
                {
                    canalManager = new CanalManager();
                }
                return canalManager;
            }
            set
            {
                canalManager = value;
            }
        }

        public CulvertManager CulvertManager
        {
            get
            {
                if (culvertManager == null)
                {
                    culvertManager = new CulvertManager();
                }
                return culvertManager;
            }
            set
            {
                culvertManager = value;
            }
        }

        public DrainageManager DrainageManager
        {
            get
            {
                if (drainageManager == null)
                {
                    drainageManager = new DrainageManager();
                }
                return drainageManager;
            }
            set
            {
                drainageManager = value;
            }
        }

        public DropStructureManager DropStructureManager
        {
            get
            {
                if (dropStructureManager == null)
                {
                    dropStructureManager = new DropStructureManager();
                }
                return dropStructureManager;
            }
            set
            {
                dropStructureManager = value;
            }
        }

        public EmbankmentManager EmbankmentManager
        {
            get
            {
                if (embankmentManager == null)
                {
                    embankmentManager = new EmbankmentManager();
                }
                return embankmentManager;
            }
            set
            {
                embankmentManager = value;
            }
        }

        public GaugeManager GaugeManager
        {
            get
            {
                if (gaugeManager == null)
                {
                    gaugeManager = new GaugeManager();
                }
                return gaugeManager;
            }
            set
            {
                gaugeManager = value;
            }
         }
                       
        public PorcupineManager PorcupineManager
        {
            get
            {
                if (porcupineManager == null)
                {
                    porcupineManager = new PorcupineManager();
                }
                return porcupineManager;
            }
            set
            {
                porcupineManager = value;
            }
        }

        public RegulatorManager RegulatorManager
        {
            get
            {
                if (regulatorManager == null)
                {
                    regulatorManager = new RegulatorManager();
                }
                return regulatorManager;
            }
            set
            {
                regulatorManager = value;
            }
        }

        public RevetmentManager RevetmentManager
        {
            get
            {
                if (revetmentManager == null)
                {
                    revetmentManager = new RevetmentManager();
                }
                return revetmentManager;
            }
            set
            {
                revetmentManager = value;
            }
        }

        public SluiceManager SluiceManager
        {
            get
            {
                if (sluiceManager == null)
                {
                    sluiceManager = new SluiceManager();
                }
                return sluiceManager;
            }
            set
            {
                sluiceManager = value;
            }
        }

        public SpurManager SpurManager
        {
            get
            {
                if (spurManager == null)
                {
                    spurManager = new SpurManager();
                }
                return spurManager;
            }
            set
            {
                spurManager = value;
            }
        }

        public TurnoutManager TurnoutManager
        {
            get
            {
                if (turnoutManager == null)
                {
                    turnoutManager = new TurnoutManager();
                }
                return turnoutManager;
            }
            set
            {
                turnoutManager = value;
            }
        }

        public WeirManager WeirManager
        {
            get
            {
                if (weirManager == null)
                {
                    weirManager = new WeirManager();
                }
                return weirManager;
            }
            set
            {
                weirManager = value;
            }
        }

        public AssetDrawingManager AssetDrawingManager
        {
            get
            {
                if (assetDrawingManager == null)
                {
                    assetDrawingManager = new AssetDrawingManager();
                }
                return assetDrawingManager;
            }
            set
            {
                assetDrawingManager = value;
            }
        }

        public List<AssetManager> AppurtenantAssetManager
        {
            get
            {
                return appurtenantAssetManager;
            }
            set
            {
                appurtenantAssetManager = value;
            }
        }

        #endregion

        
    }
}