﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class MonitoringItemTypeDB
    {
        protected internal int CreateMonitoringItemType(Guid monitoringItemTypeGuid, string monitoringItemTypeName,Guid assetTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_monitoring_item_type ");
            sqlCommand.Append("( ");
            sqlCommand.Append("monitoring_item_type_uuid, ");
            sqlCommand.Append("monitoring_item_type_name ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":monitoringItemTypeGuid, ");
            sqlCommand.Append(":monitoringItemTypeName ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("monitoringItemTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = monitoringItemTypeGuid;

            arParams[1] = new NpgsqlParameter("monitoringItemTypeName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = monitoringItemTypeName;
            
            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            if (rowsAffected == 1)
            {
                new RelationOfAssetMonitoringItemTypeDB().AddRelationOfAssetTypeWithMonitoringType(Guid.NewGuid(), assetTypeGuid, monitoringItemTypeGuid);
            }
            return rowsAffected;
        }

        protected internal List<MonitoringItemTypeManager> GetMonitoringItemType(Guid assetTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("mis_om_monitoring_item_type.monitoring_item_type_uuid, ");
            sqlCommand.Append("mis_om_monitoring_item_type.monitoring_item_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_monitoring_item_type ");
            sqlCommand.Append("INNER JOIN mis_om_assettype_monitoring_itemtype_relation ");
            sqlCommand.Append("ON ");
            sqlCommand.Append("mis_om_assettype_monitoring_itemtype_relation.monitoring_item_type_uuid = mis_om_monitoring_item_type.monitoring_item_type_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("mis_om_assettype_monitoring_itemtype_relation.asset_type_uuid= :assetTypeGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("monitoring_item_type_name ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetTypeGuid;

            List<MonitoringItemTypeManager> monitoringItemTypeManagerList = new List<MonitoringItemTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(),arParams))
            {
                while (reader.Read())
                {
                    MonitoringItemTypeManager monitoringItemTypeManagerObj = new MonitoringItemTypeManager();

                    monitoringItemTypeManagerObj.MonitoringItemTypeGuid = new Guid(reader["monitoring_item_type_uuid"].ToString());
                    monitoringItemTypeManagerObj.MonitoringItemTypeName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["monitoring_item_type_name"].ToString());
                    
                    monitoringItemTypeManagerList.Add(monitoringItemTypeManagerObj);
                }
            }
            return monitoringItemTypeManagerList;
        }
    }
}