﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SchemeManager
    {
        #region Private Members

        private Guid schemeGuid;
        private string schemeName;
        private string schemeCode;
        private string schemeType;
        private int utmEast1;
        private int utmEast2;
        private int utmNorth1;
        private int utmNorth2;
        private int area;
        private string drawingId;
        private CircleManager circleManager;
        private DivisionManager divisionManager;     

        #endregion


        #region Public Getter/Setter Properties

        public Guid SchemeGuid
        {
            get
            {
                return schemeGuid;
            }
            set
            {
                schemeGuid = value;
            }
        }

        public string SchemeName
        {
            get
            {
                return schemeName;
            }
            set
            {
                schemeName = value;
            }
        }

        public string SchemeCode
        {
            get
            {
                return schemeCode;
            }
            set
            {
                schemeCode = value;
            }
        }

        public string SchemeType
        {
            get
            {
                return schemeType;
            }
            set
            {
                schemeType = value;
            }
        }
        
        public int UtmEast1
        {
            get
            {
                return utmEast1;
            }
            set
            {
                utmEast1 = value;
            }
        }

        public int UtmEast2
        {
            get
            {
                return utmEast2;
            }
            set
            {
                utmEast2 = value;
            }
        }

        public int UtmNorth1
        {
            get
            {
                return utmNorth1;
            }
            set
            {
                utmNorth1 = value;
            }
        }

        public int UtmNorth2
        {
            get
            {
                return utmNorth2;
            }
            set
            {
                utmNorth2 = value;
            }
        }

        public int Area
        {
            get
            {
                return area;
            }
            set
            {
                area = value;
            }
        }

        public string DrawingId
        {
            get
            {
                return drawingId;
            }
            set
            {
                drawingId = value;
            }
        }

        public CircleManager CircleManager
        {
            get
            {
                if (circleManager == null)
                {
                    circleManager = new CircleManager();
                }
                return circleManager;
            }
            set
            {
                circleManager = value;
            }
        }

        public DivisionManager DivisionManager
        {
            get
            {
                if (divisionManager == null)
                {
                    divisionManager = new DivisionManager();
                }
                return divisionManager;
            }
            set
            {
                divisionManager = value;
            }
        }      

        #endregion

    }
}