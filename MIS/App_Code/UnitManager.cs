﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class UnitManager
    {
   
        #region Private Members

        private Guid unitGuid;
        private string unitName;
        private string unitAbbreviation;
        private string unitSymbol;
        private UnitTypeManager unitTypeManager;     

        #endregion


        #region Public Getter/Setter Properties

        public Guid UnitGuid
        {
            get
            {
                return unitGuid;
            }
            set
            {
                unitGuid = value;
            }
        }

        public string UnitName
        {
            get
            {
                return unitName;
            }
            set
            {
                unitName = value;
            }
        }

        public string UnitAbbreviation
        {
            get
            {
                return unitAbbreviation;
            }
            set
            {
                unitAbbreviation = value;
            }
        }

        public string UnitSymbol
        {
            get
            {
                return unitSymbol;
            }
            set
            {
                unitSymbol = value;
            }
        }

        public UnitTypeManager UnitTypeManager
        {
            get
            {
                if (unitTypeManager == null)
                {
                    unitTypeManager = new UnitTypeManager();
                }
                return unitTypeManager;
            }
            set
            {
                unitTypeManager = value;
            }
        }

        #endregion

    }
}