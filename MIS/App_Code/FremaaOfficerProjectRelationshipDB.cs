﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class FremaaOfficerProjectRelationshipDB
    {
        protected internal int SaveFremaaOfficerProjectRelationship(List<FremaaOfficerProjectRelationshipManager> fremaaOfficersEngagedList)
        {
            List<FremaaOfficerProjectRelationshipManager> subChainageWorkItemRelationManagerList = new List<FremaaOfficerProjectRelationshipManager>();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_fremaa_officer_project_relationship ");
            sqlCommand.Append("( ");
            sqlCommand.Append("fremaa_officer_project_relationship_uuid, ");
            sqlCommand.Append("project_uuid, ");
            sqlCommand.Append("fremaa_officer_engaged_name ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            foreach (FremaaOfficerProjectRelationshipManager fremaaOfficerProjectRelationshipObj in fremaaOfficersEngagedList)
            {
                int index = fremaaOfficersEngagedList.IndexOf(fremaaOfficerProjectRelationshipObj);

                sqlCommand.Append("( ");

                sqlCommand.Append("'" + fremaaOfficerProjectRelationshipObj.FremaaOfficerProjectRelationshipGuid + "', ");
                sqlCommand.Append("'" + fremaaOfficerProjectRelationshipObj.ProjectManager.ProjectGuid + "', ");
                sqlCommand.Append("'" + fremaaOfficerProjectRelationshipObj.FremaaOfficerName + "' ");

                if (index == fremaaOfficersEngagedList.Count - 1)
                {
                    sqlCommand.Append(") ");
                }
                else
                {
                    sqlCommand.Append("), ");
                }
            }

            sqlCommand.Append(";");

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());

            return rowsAffected;
        }

        protected internal List<FremaaOfficerProjectRelationshipManager> GetFremaaOfficersListRelatedToProject(Guid projectGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_fremaa_officer_project_relationship ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = projectGuid;

            List<FremaaOfficerProjectRelationshipManager> fremaaOfficerProjectRelationshipManagerList = new List<FremaaOfficerProjectRelationshipManager>();

            using (NpgsqlDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    FremaaOfficerProjectRelationshipManager fremaaOfficerProjectRelationshipManagerObj = new FremaaOfficerProjectRelationshipManager();

                    fremaaOfficerProjectRelationshipManagerObj.FremaaOfficerProjectRelationshipGuid = new Guid(reader["fremaa_officer_project_relationship_uuid"].ToString());
                    fremaaOfficerProjectRelationshipManagerObj.FremaaOfficerName = reader["fremaa_officer_engaged_name"].ToString();

                    fremaaOfficerProjectRelationshipManagerList.Add(fremaaOfficerProjectRelationshipManagerObj);
                }
            }
            return fremaaOfficerProjectRelationshipManagerList;
        }

    }
}