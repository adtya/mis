﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class AssetTypeManager
    {
        #region Private Members

        private Guid assetTypeGuid;
        private string assetTypeCode;
        private string assetTypeName;
        private Guid keyAssetTypeGuid;
        private UnitManager unitManager;
        private bool hasGates;

        #endregion


        #region Public Getter/Setter Properties

        public Guid AssetTypeGuid
        {
            get
            {
                return assetTypeGuid;
            }
            set
            {
                assetTypeGuid = value;
            }
        }

        public string AssetTypeCode
        {
            get
            {
                return assetTypeCode;
            }
            set
            {
                assetTypeCode = value;
            }
        }

        public string AssetTypeName
        {
            get
            {
                return assetTypeName;
            }
            set
            {
                assetTypeName = value;
            }
        }

        public UnitManager UnitManager
        {
            get
            {
                if (unitManager == null)
                {
                    unitManager = new UnitManager();
                }
                return unitManager;
            }
            set
            {
                unitManager = value;
            }
        }

        public bool HasGates
        {
            get
            {
                return hasGates;
            }
            set
            {
                hasGates = value;
            }
        }

        public Guid KeyAssetTypeGuid
        {
            get
            {
                return keyAssetTypeGuid;
            }
            set
            {
                keyAssetTypeGuid = value;
            }
        }

        #endregion
    }
}