﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class AssetRiskRatingDB
    {
        protected internal List<AssetRiskRatingManager> GetAssetRiskRating()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_asset_risk_rating ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("asset_risk_rating ");
            sqlCommand.Append(";");

            List<AssetRiskRatingManager> assetRiskRatingManagerList = new List<AssetRiskRatingManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    AssetRiskRatingManager assetRiskRatingManagerObj = new AssetRiskRatingManager();

                    assetRiskRatingManagerObj.AssetRiskRatingGuid = new Guid(reader["asset_risk_rating_uuid"].ToString());
                    assetRiskRatingManagerObj.AssetRiskRating = reader["asset_risk_rating"].ToString();

                    assetRiskRatingManagerList.Add(assetRiskRatingManagerObj);
                }
            }

            return assetRiskRatingManagerList;
        }
        
    }
}