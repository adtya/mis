﻿using NodaMoney;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class FieldSurveyManager
    {
        #region Private Members

        private Guid fieldSurveyGuid;
        private AssetManager assetManager;
        private RoutineWorkItemManager routineWorkItemManager;
        private SystemVariablesManager systemVariablesManager;
        private double earthVolumeQuantity;
        private Money fieldSurveyWorkItemCost;
        private decimal fieldSurveyWorkItemCostInput;
        

        #endregion


        #region Public Getter/Setter Properties

        public Guid FieldSurveyGuid
        {
            get
            {
                return fieldSurveyGuid;
            }
            set
            {
                fieldSurveyGuid = value;
            }
        }

        public AssetManager AssetManager
        {
            get
            {
                if (assetManager == null)
                {
                    assetManager = new AssetManager();
                }
                return assetManager;
            }
            set
            {
                assetManager = value;
            }
        }

        public RoutineWorkItemManager RoutineWorkItemManager
        {
            get
            {
                if (routineWorkItemManager == null)
                {
                    routineWorkItemManager = new RoutineWorkItemManager();
                }
                return routineWorkItemManager;
            }
            set
            {
                routineWorkItemManager = value;
            }
        }

        public double EarthVolumeQuantity
        {
            get
            {
                return earthVolumeQuantity;
            }
            set
            {
                earthVolumeQuantity = value;
            }
        }

        public Money FieldSurveyWorkItemCost
        {
            get
            {
                if (fieldSurveyWorkItemCost.Amount.Equals(0))
                {
                    fieldSurveyWorkItemCost = FieldSurveyWorkItemCostInput;
                }
                return fieldSurveyWorkItemCost;
            }
            set
            {
                fieldSurveyWorkItemCost = value;
            }
        }

        public decimal FieldSurveyWorkItemCostInput
        {
            private get
            {
                return fieldSurveyWorkItemCostInput;
            }
            set
            {
                fieldSurveyWorkItemCostInput = value;
            }
        }

        public SystemVariablesManager SystemVariablesManager
        {
            get
            {
                if (systemVariablesManager == null)
                {
                    systemVariablesManager = new SystemVariablesManager();
                }
                return systemVariablesManager;
            }
            set
            {
                systemVariablesManager = value;
            }
        }



        #endregion
    }
}