﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class PmcSiteEngineerProgressManager
    {
        private Guid pmcSiteEngineerProgressGuid;
        private bool pmcSiteEngineerArose;
        private ProjectAndPmcSiteEngineersRelationManager projectAndPmcSiteEngineersRelationManager;


        public Guid PmcSiteEngineerProgressGuid
        {
            get
            {
                return pmcSiteEngineerProgressGuid;
            }
            set
            {
                pmcSiteEngineerProgressGuid = value;
            }
        }

        public bool PmcSiteEngineerArose
        {
            get
            {
                return pmcSiteEngineerArose;
            }
            set
            {
                pmcSiteEngineerArose = value;
            }
        }

        public ProjectAndPmcSiteEngineersRelationManager ProjectAndPmcSiteEngineersRelationManager
        {
            get
            {
                if (projectAndPmcSiteEngineersRelationManager == null)
                {
                    projectAndPmcSiteEngineersRelationManager = new ProjectAndPmcSiteEngineersRelationManager();
                }
                return projectAndPmcSiteEngineersRelationManager;
            }
            set
            {
                projectAndPmcSiteEngineersRelationManager = value;
            }
        }
    }
}