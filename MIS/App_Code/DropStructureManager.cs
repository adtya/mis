﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class DropStructureManager
    {
        #region Private Members

        private Guid dropStructureGuid;               
        private double usInvertLevel;
        private double dsInvertLevel;
        private double stillingBasinWidth;
        private double stillingBasinLength;  
       // private AssetManager assetManager;   

        #endregion


        #region Public Constructors

        public DropStructureManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid DropStructureGuid
        {
            get
            {
                return dropStructureGuid;
            }
            set
            {
                dropStructureGuid = value;
            }
        }

        public double UsInvertLevel
        {
            get
            {
                return usInvertLevel;
            }
            set
            {
                usInvertLevel = value;
            }
        }

        public double DsInvertLevel
        {
            get
            {
                return dsInvertLevel;
            }
            set
            {
               dsInvertLevel = value;
            }
        }

        public double StillingBasinWidth
        {
            get
            {
                return stillingBasinWidth;
            }
            set
            {
                stillingBasinWidth = value;
            }
        }

        public double StillingBasinLength
        {
            get
            {
                return stillingBasinLength;
            }
            set
            {
                stillingBasinLength = value;
            }
        }
      
        // public AssetManager AssetManager
        //{
        //    get
        //    {
        //        if (assetManager == null)
        //        {
        //            assetManager = new AssetManager();
        //        }
        //        return assetManager;
        //    }
        //    set
        //    {
        //        assetManager = value;
        //    }
        //}

        #endregion
    }
}