﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class UnitDB
    {
        protected internal UnitManager GetUnit(Guid unitGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.unit_uuid, ");
            sqlCommand.Append("obj1.unit_name, ");
            sqlCommand.Append("obj1.unit_abbr, ");
            sqlCommand.Append("obj1.unit_symbol ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_units obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.unit_uuid = :unitGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("unitGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = unitGuid;

            UnitManager unitManagerObj = new UnitManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    unitManagerObj.UnitGuid = new Guid(reader["unit_uuid"].ToString());
                    unitManagerObj.UnitName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_name"].ToString());
                    unitManagerObj.UnitAbbreviation = reader["unit_abbr"].ToString();
                    unitManagerObj.UnitSymbol = reader["unit_symbol"].ToString();
                }
            }

            return unitManagerObj;

        }

        protected internal UnitManager GetUnitWithUnitType(Guid unitGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.unit_uuid, ");
            sqlCommand.Append("obj1.unit_name, ");
            sqlCommand.Append("obj1.unit_abbr, ");
            sqlCommand.Append("obj1.unit_symbol, ");
            sqlCommand.Append("obj2.unit_type_uuid, ");
            sqlCommand.Append("obj2.unit_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_units obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_unit_types obj2 ON obj2.unit_type_uuid = obj1.unit_type_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.unit_uuid = :unitGuid ");            
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("unitGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = unitGuid;

            UnitManager unitManagerObj = new UnitManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    unitManagerObj.UnitGuid = new Guid(reader["unit_uuid"].ToString());
                    unitManagerObj.UnitName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_name"].ToString());
                    unitManagerObj.UnitAbbreviation = reader["unit_abbr"].ToString();
                    unitManagerObj.UnitSymbol = reader["unit_symbol"].ToString();

                    unitManagerObj.UnitTypeManager.UnitTypeGuid = new Guid(reader["unit_type_uuid"].ToString());
                    unitManagerObj.UnitTypeManager.UnitTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_type_name"].ToString());
                }
            }

            return unitManagerObj;

        }


        protected internal List<UnitManager> GetUnits()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.unit_uuid, ");
            sqlCommand.Append("obj1.unit_name, ");
            sqlCommand.Append("obj1.unit_abbr, ");
            sqlCommand.Append("obj1.unit_symbol ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_units obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.unit_name ");
            sqlCommand.Append(";");

            List<UnitManager> unitManagerList = new List<UnitManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    UnitManager unitManagerObj = new UnitManager();

                    unitManagerObj.UnitGuid = new Guid(reader["unit_uuid"].ToString());
                    unitManagerObj.UnitName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_name"].ToString());
                    unitManagerObj.UnitAbbreviation = reader["unit_abbr"].ToString();
                    unitManagerObj.UnitSymbol = reader["unit_symbol"].ToString();

                    unitManagerList.Add(unitManagerObj);
                }
            }

            return unitManagerList;

        }

        protected internal List<UnitManager> GetUnitsBasedOnUnitType(Guid unitTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.unit_uuid, ");
            sqlCommand.Append("obj1.unit_name, ");
            sqlCommand.Append("obj1.unit_abbr, ");
            sqlCommand.Append("obj1.unit_symbol ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_units obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.unit_type_uuid = :unitTypeGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.unit_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("unitTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = unitTypeGuid;

            List<UnitManager> unitManagerList = new List<UnitManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    UnitManager unitManagerObj = new UnitManager();

                    unitManagerObj.UnitGuid = new Guid(reader["unit_uuid"].ToString());
                    unitManagerObj.UnitName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_name"].ToString());
                    unitManagerObj.UnitAbbreviation = reader["unit_abbr"].ToString();
                    unitManagerObj.UnitSymbol = reader["unit_symbol"].ToString();

                    unitManagerList.Add(unitManagerObj);
                }
            }

            return unitManagerList;

        }

        protected internal List<UnitManager> GetUnitsWithUnitType()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.unit_uuid, ");
            sqlCommand.Append("obj1.unit_name, ");
            sqlCommand.Append("obj1.unit_abbr, ");
            sqlCommand.Append("obj1.unit_symbol, ");
            sqlCommand.Append("obj2.unit_type_uuid, ");
            sqlCommand.Append("obj2.unit_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_units obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_unit_types obj2 ON obj2.unit_type_uuid = obj1.unit_type_uuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.unit_name ");
            sqlCommand.Append(";");

            List<UnitManager> unitManagerList = new List<UnitManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    UnitManager unitManagerObj = new UnitManager();

                    unitManagerObj.UnitGuid = new Guid(reader["unit_uuid"].ToString());
                    unitManagerObj.UnitName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_name"].ToString());
                    unitManagerObj.UnitAbbreviation = reader["unit_abbr"].ToString();
                    unitManagerObj.UnitSymbol = reader["unit_symbol"].ToString();

                    unitManagerObj.UnitTypeManager.UnitTypeGuid = new Guid(reader["unit_type_uuid"].ToString());
                    unitManagerObj.UnitTypeManager.UnitTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_type_name"].ToString());

                    unitManagerList.Add(unitManagerObj);
                }
            }

            return unitManagerList;

        }

        protected internal List<UnitManager> GetUnitsWithUnitTypeBasedOnUnitType(Guid unitTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.unit_uuid, ");
            sqlCommand.Append("obj1.unit_name, ");
            sqlCommand.Append("obj1.unit_abbr, ");
            sqlCommand.Append("obj1.unit_symbol, ");
            sqlCommand.Append("obj2.unit_type_uuid, ");
            sqlCommand.Append("obj2.unit_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_units obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_unit_types obj2 ON obj2.unit_type_uuid = obj1.unit_type_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.unit_type_uuid = :unitTypeGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.unit_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("unitTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = unitTypeGuid;

            List<UnitManager> unitManagerList = new List<UnitManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    UnitManager unitManagerObj = new UnitManager();

                    unitManagerObj.UnitGuid = new Guid(reader["unit_uuid"].ToString());
                    unitManagerObj.UnitName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_name"].ToString());
                    unitManagerObj.UnitAbbreviation = reader["unit_abbr"].ToString();
                    unitManagerObj.UnitSymbol = reader["unit_symbol"].ToString();

                    unitManagerObj.UnitTypeManager.UnitTypeGuid = new Guid(reader["unit_type_uuid"].ToString());
                    unitManagerObj.UnitTypeManager.UnitTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_type_name"].ToString());

                    unitManagerList.Add(unitManagerObj);
                }
            }

            return unitManagerList;

        }


        protected internal bool CheckUnitExistence(string unitName, Guid unitTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_units obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.unit_name = :unitName ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.unit_type_uuid = :unitTypeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("unitName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(unitName);

            arParams[1] = new NpgsqlParameter("unitTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = unitTypeGuid;

            bool result = false;

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    result = true;
                }
            }

            return result;
        }


        protected internal int AddUnit(UnitManager unitManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT ");
            sqlCommand.Append("INTO ");
            sqlCommand.Append("mis_units ");
            sqlCommand.Append("( ");
            sqlCommand.Append("unit_uuid, ");
            sqlCommand.Append("unit_name, ");
            sqlCommand.Append("unit_abbr, ");
            sqlCommand.Append("unit_symbol, ");
            sqlCommand.Append("unit_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":unitGuid, ");
            sqlCommand.Append(":unitName, ");
            sqlCommand.Append(":unitAbbr, ");
            sqlCommand.Append(":unitSymbol, ");
            sqlCommand.Append(":unitTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[5];

            arParams[0] = new NpgsqlParameter("unitGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = unitManager.UnitGuid;

            arParams[1] = new NpgsqlParameter("unitName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(unitManager.UnitName);

            arParams[2] = new NpgsqlParameter("unitAbbr", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = unitManager.UnitAbbreviation;

            arParams[3] = new NpgsqlParameter("unitSymbol", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = unitManager.UnitSymbol;

            arParams[4] = new NpgsqlParameter("unitTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = unitManager.UnitTypeManager.UnitTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }


        protected internal int UpdateUnit(UnitManager unitManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_units ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("unit_name = :unitName, ");
            sqlCommand.Append("unit_abbr = :unitAbbr, ");
            sqlCommand.Append("unit_symbol = :unitSymbol ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("unit_type_uuid = :unitTypeGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("unit_uuid = :unitGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[5];

            arParams[0] = new NpgsqlParameter("unitGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = unitManager.UnitGuid;

            arParams[1] = new NpgsqlParameter("unitName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(unitManager.UnitName);

            arParams[2] = new NpgsqlParameter("unitAbbr", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = unitManager.UnitAbbreviation;

            arParams[3] = new NpgsqlParameter("unitSymbol", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = unitManager.UnitSymbol;

            arParams[4] = new NpgsqlParameter("unitTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = unitManager.UnitTypeManager.UnitTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }


        protected internal int DeleteUnit(Guid unitTypeGuid, Guid unitGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_units ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("unit_type_uuid = :unitTypeGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("unit_uuid = :unitGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("unitTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = unitTypeGuid;

            arParams[1] = new NpgsqlParameter("unitGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = unitGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }     


    }
}