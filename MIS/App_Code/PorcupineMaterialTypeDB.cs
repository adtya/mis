﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class PorcupineMaterialTypeDB
    {
        protected internal List<PorcupineMaterialTypeManager> GetPorMaterialTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("material_type_uuid, ");
            sqlCommand.Append("material_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_porcupine_material_type ");
            sqlCommand.Append("ORDER BY material_type_name ");
            sqlCommand.Append(";");

            List<PorcupineMaterialTypeManager> porcupineMaterialTypeManagerList = new List<PorcupineMaterialTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    PorcupineMaterialTypeManager porcupineMaterialTypeManagerObj = new PorcupineMaterialTypeManager();

                    porcupineMaterialTypeManagerObj.PorcupineMaterialTypeGuid = new Guid(reader["material_type_uuid"].ToString());
                    porcupineMaterialTypeManagerObj.PorcupineMaterialTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["material_type_name"].ToString());

                    porcupineMaterialTypeManagerList.Add(porcupineMaterialTypeManagerObj);
                }
            }

            return porcupineMaterialTypeManagerList;
        }
    }
}