﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class DivisionManager
    {

        #region Private Members

        private Guid divisionGuid;
        private string divisionName;
        private string divisionCode;
        private CircleManager circleManager;
        #endregion




        #region Getters/Setters

        public Guid DivisionGuid
        {
            get
            {
                return divisionGuid;
            }
            set
            {
                divisionGuid = value;
            }
        }

        public string DivisionName
        {
            get
            {
                return divisionName;
            }
            set
            {
                divisionName = value;
            }
        }

        public string DivisionCode
        {
            get
            {
                return divisionCode;
            }
            set
            {
                divisionCode = value;
            }
        }

        public CircleManager CircleManager
        {
            get
            {
                if (circleManager == null)
                {
                    circleManager = new CircleManager();
                }
                return circleManager;
            }
            set
            {
                circleManager = value;
            }
        }

        #endregion

    }
}