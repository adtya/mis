﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class GaugeFrequencyTypeManager
    {
         #region Private Members

        private Guid frequencyTypeGuid;
        private string frequencyTypeName;
       
       #endregion


        #region Public Constructors

        public GaugeFrequencyTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid FrequencyTypeGuid
        {
            get
            {
                return frequencyTypeGuid;
            }
            set
            {
                frequencyTypeGuid = value;
            }
        }

        public string FrequencyTypeName
        {
            get
            {
                return frequencyTypeName;
            }
            set
            {
                frequencyTypeName = value;
            }
        }        

        #endregion
    }
}