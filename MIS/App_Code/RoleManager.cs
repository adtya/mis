﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RoleManager
    {

        #region Data Members

        private Guid roleGuid;
        private string roleName;
        private bool roleCanBeDeleted;

        #endregion




        #region Getter/Setter

        public Guid RoleGuid
        {
            get
            {
                return roleGuid;
            }
            set
            {
                roleGuid = value;
            }
        }

        public string RoleName
        {
            get
            {
                return roleName;
            }
            set
            {
                roleName = value;
            }
        }

        public bool RoleCanBeDeleted
        {
            get
            {
                return roleCanBeDeleted;
            }
            set
            {
                roleCanBeDeleted = value;
            }
        }

        #endregion

    }
}