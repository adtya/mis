﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace MIS.App_Code
{
    public class PasswordGenerator
    {
        protected internal string CreateRandomPassword()//UserManager userObj, string newUrl)
        {
            int length = 7;
            char[] allowedChars = "abcdefgijkmnopqrstwxyzABCDEFGHJKLMNPQRSTWXYZ23456789*$".ToCharArray();

            char[] passwordChars = new char[length];
            byte[] seedBytes = new byte[4];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetBytes(seedBytes);

            int seed = (seedBytes[0] & 0x7f) << 24 |
                seedBytes[1] << 16 |
                seedBytes[2] << 8 |
                seedBytes[3];

            Random random = new Random(seed);

            for (int i = 0; i < length; i++)
            {
                passwordChars[i] = allowedChars[random.Next(0, allowedChars.Length)];
            }
            string password = new string(passwordChars);
            //bool status = new UserDB().UpdateUserDetailsWhenApproved(userObj.UserGuid, password, userObj.UserType, userObj.SuperUserManager.SuperUserGuid, userObj.AdminManager.AdminGuid, userObj.SuperAdminManager.SuperAdminGuid);
            //if (status)
            //{
            //    string subject = "Account Details for Online Training Portal, WRD, Assam";
            //    string body = "Dear " + userObj.FirstName + " " + userObj.MiddleName + " " + userObj.LastName + "\n";
            //    body += "Your account has been approved by the DBA." + "\n\n";
            //    body += "The credentials for your account are:" + "\n";
            //    body += "User Id: " + userObj.EmailId + "\n";
            //    body += "Password: " + password + "\n";
            //    body += "To login click on the following link:\n";
            //    body += newUrl + "\n\n";
            //    body += "Thanks for your Patience\n\n\n\n";
            //    body += "Thanks n Regards\n";
            //    body += "Online Training Portal\n";
            //    body += "This is a system generated mail. Please do not reply to this.";
            //    new MailManager().SendMailToUser(userObj.EmailId, subject, body);
            //}
            return password;
        }
    }
}