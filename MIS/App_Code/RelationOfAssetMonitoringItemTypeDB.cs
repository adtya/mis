﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RelationOfAssetMonitoringItemTypeDB
    {
        protected internal int AddRelationOfAssetTypeWithMonitoringType(Guid relationGuid, Guid assetTypeGuid, Guid monitoringItemTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_assettype_monitoring_itemtype_relation ");
            sqlCommand.Append("( ");
            sqlCommand.Append("relation_uuid, ");
            sqlCommand.Append("asset_type_uuid, ");
            sqlCommand.Append("monitoring_item_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":relationGuid, ");
            sqlCommand.Append(":assetTypeGuid, ");
            sqlCommand.Append(":monitoringItemTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("relationGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = relationGuid; ;

            arParams[1] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetTypeGuid;

            arParams[2] = new NpgsqlParameter("monitoringItemTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = monitoringItemTypeGuid;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;
        }
    }
}