﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using System.Text;
using System.Data;
namespace MIS.App_Code
{
    public class WorkTypeDB
    {
        protected internal void AddNewWorkType(string workTypeName, string workTypeUnit, string[] workTypeSubHeadTitle)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("work_types ");
            sqlCommand.Append("( ");
            sqlCommand.Append("work_type_uuid, ");
            sqlCommand.Append("work_type_name, ");
            sqlCommand.Append("work_type_unit, ");
            sqlCommand.Append("work_type_sub_head_title ");           
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":workTypeGuid, ");
            sqlCommand.Append(":workTypeName, ");
            sqlCommand.Append(":workTypeUnit, ");
            sqlCommand.Append(":workTypeSubHeadTitle ");              
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[4];

            arParams[0] = new NpgsqlParameter("workTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("workTypeName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = workTypeName;

            arParams[2] = new NpgsqlParameter("workTypeUnit", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = workTypeUnit;

            arParams[3] = new NpgsqlParameter("workTypeSubHeadTitle", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = workTypeSubHeadTitle;            

            int result = Convert.ToInt32(NpgsqlHelper.ExecuteScalar(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams));

        }

        protected internal List<WorkType> GetWorkTypes()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("work_types ");
            sqlCommand.Append(";");

            //NpgsqlParameter[] arParams = new NpgsqlParameter[4];

            //arParams[0] = new NpgsqlParameter("workTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            //arParams[0].Direction = ParameterDirection.Input;
            //arParams[0].Value = Guid.NewGuid();

            //arParams[1] = new NpgsqlParameter("workTypeName", NpgsqlTypes.NpgsqlDbType.Text);
            //arParams[1].Direction = ParameterDirection.Input;
            //arParams[1].Value = workTypeName;

            //arParams[2] = new NpgsqlParameter("workTypeUnit", NpgsqlTypes.NpgsqlDbType.Uuid);
            //arParams[2].Direction = ParameterDirection.Input;
            //arParams[2].Value = workTypeUnit;

            //arParams[3] = new NpgsqlParameter("workTypeSubHeadTitle", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text);
            //arParams[3].Direction = ParameterDirection.Input;
            //arParams[3].Value = workTypeSubHeadTitle;

            List<WorkType> workTypeItemList = new List<WorkType>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    WorkType workTypeItemObj = new WorkType();

                    workTypeItemObj.WorkTypeGuid = new Guid(reader["work_type_uuid"].ToString());
                    workTypeItemObj.WorkTypeName = reader["work_type_name"].ToString();

                    workTypeItemList.Add(workTypeItemObj);
                }
            }

            return workTypeItemList;

        }       
    }
}