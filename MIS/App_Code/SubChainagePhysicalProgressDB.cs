﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SubChainagePhysicalProgressDB
    {
        protected internal bool CheckSubChainageCurrentMonthProgressExistence(Guid subChainageGuid, DateTime selectedDate)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_sub_chainage_physical_progress ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("sub_chainage_uuid = :subChainageGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("progress_date = :selectedDate ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = subChainageGuid;

            arParams[1] = new NpgsqlParameter("selectedDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = selectedDate;

            bool returnValue = false;

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    returnValue = true;
                }
            }

            return returnValue;
        }

        protected internal bool CheckSubChainageLastMonthProgressExistence(Guid subChainageGuid, DateTime selectedDate)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_sub_chainage_physical_progress ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("sub_chainage_uuid = :subChainageGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("progress_date = :selectedDate ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = subChainageGuid;

            arParams[1] = new NpgsqlParameter("selectedDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = selectedDate;

            bool returnValue = false;

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    returnValue = true;
                }
            }

            return returnValue;
        }

        protected internal SubChainagePhysicalProgressManager GetSubChainagePhysicalProgress(Guid subChainageGuid, DateTime selectedDate)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_sub_chainage_physical_progress obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.sub_chainage_uuid = :subChainageGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.progress_date = :selectedDate ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = subChainageGuid;

            arParams[1] = new NpgsqlParameter("selectedDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = selectedDate;

            SubChainagePhysicalProgressManager subChainagePhysicalProgressManagerObj = new SubChainagePhysicalProgressManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    subChainagePhysicalProgressManagerObj.PhysicalProgressGuid = new Guid(reader["physical_progress_uuid"].ToString());
                    subChainagePhysicalProgressManagerObj.PhysicalProgressDate = DateTime.Parse(reader["progress_date"].ToString());
                    subChainagePhysicalProgressManagerObj.SubChainageManager.SubChainageGuid = new Guid(reader["sub_chainage_uuid"].ToString());
                    subChainagePhysicalProgressManagerObj.PhysicalProgressAddedOn = DateTime.Parse(reader["progress_added_on"].ToString());

                    subChainagePhysicalProgressManagerObj.FremaaOfficerProgressManager = new FremaaOfficerProgressDB().GetFremaaOfficerProgressWithFremaaOfficerName(subChainagePhysicalProgressManagerObj.PhysicalProgressGuid);

                    subChainagePhysicalProgressManagerObj.PmcSiteEngineerProgressManager = new PmcSiteEngineerProgressDB().GetPmcSiteEngineerProgressWithPmcSiteEngineerName(subChainagePhysicalProgressManagerObj.PhysicalProgressGuid);
                    
                    subChainagePhysicalProgressManagerObj.WorkItemProgressManager = new WorkItemProgressDB().GetWorkItemProgressBasedOnSubChainagePhysicalProgress(subChainagePhysicalProgressManagerObj.PhysicalProgressGuid);
                }
            }

            return subChainagePhysicalProgressManagerObj;
        }

        protected internal int AddSubChainagePhysicalProgress(SubChainagePhysicalProgressManager subChainagePhysicalProgressManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_sub_chainage_physical_progress ");
            sqlCommand.Append("( ");
            sqlCommand.Append("physical_progress_uuid, ");
            sqlCommand.Append("progress_date, ");
            sqlCommand.Append("sub_chainage_uuid, ");
            sqlCommand.Append("progress_added_on ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":subChainagePhysicalProgressGuid, ");
            sqlCommand.Append(":physicalProgressDate, ");
            sqlCommand.Append(":subChainageGuid, ");
            sqlCommand.Append(":subChainagePhysicalProgressAddedOn ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[4];

            arParams[0] = new NpgsqlParameter("subChainagePhysicalProgressGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = subChainagePhysicalProgressManager.PhysicalProgressGuid;

            arParams[1] = new NpgsqlParameter("physicalProgressDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = subChainagePhysicalProgressManager.PhysicalProgressDate;

            arParams[2] = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = subChainagePhysicalProgressManager.SubChainageManager.SubChainageGuid;

            arParams[3] = new NpgsqlParameter("subChainagePhysicalProgressAddedOn", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = DateTime.Today;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            if (rowsAffected == 1)
            {
                new FremaaOfficerProgressDB().AddFremaaOfficerProgress(subChainagePhysicalProgressManager);

                new PmcSiteEngineerProgressDB().AddPmcSiteEngineerProgress(subChainagePhysicalProgressManager);

                new WorkItemProgressDB().AddProjectWorkItemProgress(subChainagePhysicalProgressManager);
            }

            return rowsAffected;
        }

        protected internal List<SubChainagePhysicalProgressManager> GetSubChainagePhysicalProgressAll(Guid subChainageGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_sub_chainage_physical_progress obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.sub_chainage_uuid= :subChainageGuid ");            
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = subChainageGuid;

            List<SubChainagePhysicalProgressManager> subChainagePhysicalProgressManagerList = new List<SubChainagePhysicalProgressManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    SubChainagePhysicalProgressManager subChainagePhysicalProgressManagerObj = new SubChainagePhysicalProgressManager();

                    subChainagePhysicalProgressManagerObj.PhysicalProgressGuid = new Guid(reader["physical_progress_uuid"].ToString());
                    subChainagePhysicalProgressManagerObj.PhysicalProgressDate = DateTime.Parse(reader["progress_date"].ToString());
                    subChainagePhysicalProgressManagerObj.SubChainageManager.SubChainageGuid = new Guid(reader["sub_chainage_uuid"].ToString());
                    subChainagePhysicalProgressManagerObj.PhysicalProgressAddedOn = DateTime.Parse(reader["progress_added_on"].ToString());
                    subChainagePhysicalProgressManagerObj.CanBeEdited = Boolean.Parse(reader["can_be_edited"].ToString());

                    //subChainagePhysicalProgressManagerObj.FremaaOfficerProgressManager = new FremaaOfficerProgressDB().GetFremaaOfficerProgressWithFremaaOfficerName(subChainagePhysicalProgressManagerObj.PhysicalProgressGuid);

                    //subChainagePhysicalProgressManagerObj.PmcSiteEngineerProgressManager = new PmcSiteEngineerProgressDB().GetPmcSiteEngineerProgressWithPmcSiteEngineerName(subChainagePhysicalProgressManagerObj.PhysicalProgressGuid);

                    subChainagePhysicalProgressManagerObj.WorkItemProgressManager = new WorkItemProgressDB().GetWorkItemProgressBasedOnSubChainagePhysicalProgress(subChainagePhysicalProgressManagerObj.PhysicalProgressGuid);

                    subChainagePhysicalProgressManagerList.Add(subChainagePhysicalProgressManagerObj);
                }
            }

            return subChainagePhysicalProgressManagerList;
        }
    }
}