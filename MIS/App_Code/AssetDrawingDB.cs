﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class AssetDrawingDB
    {
        protected internal int AddDrawing(AssetManager assetManager)
        {            
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_drawing ");
            sqlCommand.Append("( ");
            sqlCommand.Append("asset_drawing_uuid, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("drawing_file_name, ");
            sqlCommand.Append("drawing_file_upload_date, ");
            sqlCommand.Append("drawing_file_url ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":assetDrawingGuid, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":drawingFileName, ");
            sqlCommand.Append(":drawingFileUploadDate, ");
            sqlCommand.Append(":drawingFileUrl ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[5];

            arParams[0] = new NpgsqlParameter("assetDrawingGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.AssetGuid;

            arParams[2] = new NpgsqlParameter("drawingFileName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.AssetDrawingManager.DrawingFileName;

            arParams[3] = new NpgsqlParameter("drawingFileUploadDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.AssetDrawingManager.DrawingFileUploadDate;

            arParams[4] = new NpgsqlParameter("drawingFileUrl", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.AssetDrawingManager.DrawingFileUrl;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

    }
}