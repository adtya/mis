﻿using NodaMoney;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RepairFundingDB
    {
        protected internal List<RepairFundingManager> GetRepairFundingData()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_repair_funding obj1 ");
            sqlCommand.Append(";");

            List<RepairFundingManager> repairFundingManagerList = new List<RepairFundingManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RepairFundingManager repairFundingManagerObj = new RepairFundingManager();

                    repairFundingManagerObj.RepairFundingGuid = new Guid(reader["repair_funding_uuid"].ToString());
                    repairFundingManagerObj.FiscalYear = reader["fiscal_year"].ToString();
                    repairFundingManagerObj.AvailableFunds = Money.Parse(reader["available_funds"].ToString());

                    repairFundingManagerList.Add(repairFundingManagerObj);
                }
            }

            return repairFundingManagerList;
        }

        protected internal bool CheckFiscalYearExistence(string fiscalYear)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_repair_funding obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.fiscal_year = :fiscalYear ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("fiscalYear", NpgsqlTypes.NpgsqlDbType.Text);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = fiscalYear;

            bool result = false;

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    result = true;
                }
            }

            return result;
        }

        protected internal int SaveRepairFundingData(RepairFundingManager repairFundingManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_repair_funding ");
            sqlCommand.Append("( ");
            sqlCommand.Append("repair_funding_uuid, ");
            sqlCommand.Append("fiscal_year, ");
            sqlCommand.Append("available_funds ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":repairFundingGuid, ");
            sqlCommand.Append(":fiscalYear, ");
            sqlCommand.Append(":availableFunds ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("repairFundingGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = repairFundingManager.RepairFundingGuid;

            arParams[1] = new NpgsqlParameter("fiscalYear", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = repairFundingManager.FiscalYear;

            arParams[2] = new NpgsqlParameter("availableFunds", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = repairFundingManager.AvailableFunds.Amount;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int DeleteRepairFundingData(Guid repairFundingGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_repair_funding ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("repair_funding_uuid = :repairFundingGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("repairFundingGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = repairFundingGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateRepairFundingData(Guid repairFundingGuid, RepairFundingManager repairFundingManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_om_repair_funding ");
            sqlCommand.Append("SET ");          
            sqlCommand.Append("available_funds = :availableFunds ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("repair_funding_uuid = :repairFundingGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("repairFundingGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = repairFundingGuid;

            arParams[1] = new NpgsqlParameter("availableFunds", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = repairFundingManager.AvailableFunds.Amount;            

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal RepairFundingManager GetRepairFundingDataForRepairAnalyis()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_repair_funding obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.fiscal_year= ANY(SELECT fiscal_year FROM system_variables) ");
            sqlCommand.Append(";");

            RepairFundingManager repairFundingManagerObj = new RepairFundingManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {            
                    repairFundingManagerObj.RepairFundingGuid = new Guid(reader["repair_funding_uuid"].ToString());
                    repairFundingManagerObj.FiscalYear = reader["fiscal_year"].ToString();
                    repairFundingManagerObj.AvailableFunds = Money.Parse(reader["available_funds"].ToString(),Currency.FromCode("INR"));                    
                }
            }

            return repairFundingManagerObj;
        }
    }
}