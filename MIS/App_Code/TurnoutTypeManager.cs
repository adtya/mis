﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class TurnoutTypeManager
    {
        #region Private Members

        private Guid turnoutTypeGuid;
        private string turnoutTypeName;
       
       #endregion


        #region Public Constructors

        public TurnoutTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid TurnoutTypeGuid
        {
            get
            {
                return turnoutTypeGuid;
            }
            set
            {
                turnoutTypeGuid = value;
            }
        }

        public string TurnoutTypeName
        {
            get
            {
                return turnoutTypeName;
            }
            set
            {
                turnoutTypeName = value;
            }
        }        

        #endregion
    }
}