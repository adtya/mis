﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class PerformanceMonitoringManager
    {
        #region Private Members

        private Guid performanceMonitoringGuid;
        private DateTime currentMonitoringDate;
        private string inspectorName;
        private Guid inspectorPositionGuid;
        private DateTime inspectionDate;
        private string generalComments;       
        private AssetManager assetManager;
        private DesignationManager designationManager;
        private OverallStatusManager overallStatusManager;
        private SystemVariablesManager systemVariablesManager;
        private List<MonitoringItemPerformanceManager> monitoringItemPerformanceManager;

        #endregion


        #region Public Constructors

        public PerformanceMonitoringManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid PerformanceMonitoringGuid
        {
            get
            {
                return performanceMonitoringGuid;
            }
            set
            {
                performanceMonitoringGuid = value;
            }
        }

        public DateTime CurrentMonitoringDate
        {
            get
            {
                return currentMonitoringDate;
            }
            set
            {
                currentMonitoringDate = value;
            }
        }       

        public string InspectorName
        {
            get
            {
                return inspectorName;
            }
            set
            {
                inspectorName = value;
            }
        }

        public Guid InspectorPositionGuid
        {
            get
            {
                return inspectorPositionGuid;
            }
            set
            {
                inspectorPositionGuid = value;
            }
        }

        public DateTime InspectionDate
        {
            get
            {
                return inspectionDate;
            }
            set
            {
                inspectionDate = value;
            }
        }

        public string GeneralComments
        {
            get
            {
                return generalComments;
            }
            set
            {
                generalComments = value;
            }
        }

        public AssetManager AssetManager
        {
            get
            {
                if (assetManager == null)
                {
                    assetManager = new AssetManager();
                }
                return assetManager;
            }
            set
            {
                assetManager = value;
            }
        }

        public DesignationManager DesignationManager
        {
            get
            {
                if (designationManager == null)
                {
                    designationManager = new DesignationManager();
                }
                return designationManager;
            }
            set
            {
                designationManager = value;
            }
        }

        public OverallStatusManager OverallStatusManager
        {
            get
            {
                if (overallStatusManager == null)
                {
                    overallStatusManager = new OverallStatusManager();
                }
                return overallStatusManager;
            }
            set
            {
                overallStatusManager = value;
            }
        }

        public SystemVariablesManager SystemVariablesManager
        {
            get
            {
                if (systemVariablesManager == null)
                {
                    systemVariablesManager = new SystemVariablesManager();
                }
                return systemVariablesManager;
            }
            set
            {
                systemVariablesManager = value;
            }
        }

        public List<MonitoringItemPerformanceManager> MonitoringItemPerformanceManager
        {
            get
            {
                
                return monitoringItemPerformanceManager;
            }
            set
            {
                monitoringItemPerformanceManager = value;
            }
        }
        
        #endregion       
    }
}