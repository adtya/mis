﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class WeirTypeDB
    {
        protected internal List<WeirTypeManager> GetWeirTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("weir_type_uuid, ");
            sqlCommand.Append("weir_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_weir_type ");
            sqlCommand.Append("ORDER BY weir_type_name ");
            sqlCommand.Append(";");

            List<WeirTypeManager> weirTypeManagerList = new List<WeirTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    WeirTypeManager weirTypeManagerObj = new WeirTypeManager();

                    weirTypeManagerObj.WeirTypeGuid = new Guid(reader["weir_type_uuid"].ToString());
                    weirTypeManagerObj.WeirTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["weir_type_name"].ToString());

                    weirTypeManagerList.Add(weirTypeManagerObj);
                }
            }

            return weirTypeManagerList;
        }
    }
}