﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class DesignationManager
    {
        private Guid designationGuid;
        private string designationName;

        public Guid DesignationGuid
        {
            get
            {
                return designationGuid;
            }
            set
            {
                designationGuid = value;
            }
        }

        public string DesignationName
        {
            get
            {
                return designationName;
            }
            set
            {
                designationName = value;
            }
        }

    }
}