﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class GateManager
    {
        #region Private Members

        private Guid gateGuid;
        private int numberOfGates;
        private double height;
        private double width;  
        private double diameter;
        private DateTime installDate; 
        private GateTypeManager gateTypeManager;
        private GateConstructionMaterialManager gateConstructionMaterialManager;        

        #endregion

        #region Public Getter/Setter Properties

        public Guid GateGuid
        {
            get
            {
                return gateGuid;
            }
            set
            {
                gateGuid = value;
            }
        }

        public int NumberOfGates
        {
            get
            {
                return numberOfGates;
            }
            set
            {
                numberOfGates = value;
            }
        }

        public double Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }

        public double Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }
        
        public double Diameter
        {
            get
            {
                return diameter;
            }
            set
            {
                diameter = value;
            }
        }

        public DateTime InstallDate
        {
            get
            {
                return installDate;
            }
            set
            {
                installDate = value;
            }
        }

        public GateTypeManager GateTypeManager
        {
            get
            {
                if (gateTypeManager == null)
                {
                    gateTypeManager = new GateTypeManager();
                }
                return gateTypeManager;
            }
            set
            {
                gateTypeManager = value;
            }
        }

        public GateConstructionMaterialManager GateConstructionMaterialManager
        {
            get
            {
                if (gateConstructionMaterialManager == null)
                {
                    gateConstructionMaterialManager = new GateConstructionMaterialManager();
                }
                return gateConstructionMaterialManager;
            }
            set
            {
                gateConstructionMaterialManager = value;
            }
        }        

        #endregion
    }
}