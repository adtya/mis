﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RevetmentRiverprotTypeManager
    {
        #region Private Members

        private Guid riverprotTypeGuid;
        private string riverprotTypeName;
       
       #endregion


        #region Public Constructors

        public RevetmentRiverprotTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid RiverprotTypeGuid
        {
            get
            {
                return riverprotTypeGuid;
            }
            set
            {
                riverprotTypeGuid = value;
            }
        }

        public string RiverprotTypeName
        {
            get
            {
                return riverprotTypeName;
            }
            set
            {
                riverprotTypeName = value;
            }
        }        

        #endregion
    }
}