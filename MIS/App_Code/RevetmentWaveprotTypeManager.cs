﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RevetmentWaveprotTypeManager
    {
        #region Private Members

        private Guid waveprotTypeGuid;
        private string waveprotTypeName;
       
       #endregion


        #region Public Constructors

        public RevetmentWaveprotTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid WaveprotTypeGuid
        {
            get
            {
                return waveprotTypeGuid;
            }
            set
            {
                waveprotTypeGuid = value;
            }
        }

        public string WaveprotTypeName
        {
            get
            {
                return waveprotTypeName;
            }
            set
            {
                waveprotTypeName = value;
            }
        }        

        #endregion
    }
}