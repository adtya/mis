﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class GaugeDB
    {
        protected internal int AddGaugeData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_gauge ");
            sqlCommand.Append("( ");
            sqlCommand.Append("gauge_uuid, ");
            sqlCommand.Append("start_date, ");
            sqlCommand.Append("end_date, ");
            sqlCommand.Append("active_yn, ");
            sqlCommand.Append("lwl_yn, ");
            sqlCommand.Append("hwl_yn, ");
            sqlCommand.Append("level_geo_yn, ");
            sqlCommand.Append("zero_datum, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("gauge_type_uuid, ");
            sqlCommand.Append("frequency_type_uuid, ");
            sqlCommand.Append("season_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":gaugeGuid, ");
            sqlCommand.Append(":startDate, ");
            sqlCommand.Append(":endDate, ");
            sqlCommand.Append(":active, ");
            sqlCommand.Append(":lwl, ");
            sqlCommand.Append(":hwl, ");
            sqlCommand.Append(":levelGeo, ");
            sqlCommand.Append(":zeroDatum, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":gaugeTypeGuid, ");
            sqlCommand.Append(":frequencyTypeGuid, ");
            sqlCommand.Append(":seasonTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[12];

            arParams[0] = new NpgsqlParameter("gaugeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("startDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.GaugeManager.StartDate;

            arParams[2] = new NpgsqlParameter("endDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.GaugeManager.EndDate;

            arParams[3] = new NpgsqlParameter("active", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.GaugeManager.Active;

            arParams[4] = new NpgsqlParameter("lwl", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.GaugeManager.Lwl;

            arParams[5] = new NpgsqlParameter("hwl", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.GaugeManager.Hwl;

            arParams[6] = new NpgsqlParameter("levelGeo", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.GaugeManager.LevelGeo;

            arParams[7] = new NpgsqlParameter("zeroDatum", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.GaugeManager.ZeroDatum;

            arParams[8] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.AssetGuid;

            arParams[9] = new NpgsqlParameter("gaugeTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = assetManager.GaugeManager.GaugeTypeManager.GaugeTypeGuid;

            arParams[10] = new NpgsqlParameter("frequencyTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[10].Direction = ParameterDirection.Input;
            arParams[10].Value = assetManager.GaugeManager.GaugeFrequencyTypeManager.FrequencyTypeGuid;

            arParams[11] = new NpgsqlParameter("seasonTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[11].Direction = ParameterDirection.Input;
            arParams[11].Value = assetManager.GaugeManager.GaugeSeasonTypeManager.SeasonTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateGaugeData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_gauge ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("start_date= :startDate, ");
            sqlCommand.Append("end_date= :endDate, ");
            sqlCommand.Append("active_yn= :active, ");
            sqlCommand.Append("lwl_yn= :lwl, ");
            sqlCommand.Append("hwl_yn= :hwl, ");
            sqlCommand.Append("level_geo_yn= :levelGeo, ");
            sqlCommand.Append("zero_datum= :zeroDatum, ");           
            sqlCommand.Append("gauge_type_uuid= :gaugeTypeGuid, ");
            sqlCommand.Append("frequency_type_uuid= :frequencyTypeGuid, ");
            sqlCommand.Append("season_type_uuid= :seasonTypeGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[12];

            arParams[0] = new NpgsqlParameter("gaugeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.GaugeManager.GaugeGuid;

            arParams[1] = new NpgsqlParameter("startDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.GaugeManager.StartDate;

            arParams[2] = new NpgsqlParameter("endDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.GaugeManager.EndDate;

            arParams[3] = new NpgsqlParameter("active", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.GaugeManager.Active;

            arParams[4] = new NpgsqlParameter("lwl", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.GaugeManager.Lwl;

            arParams[5] = new NpgsqlParameter("hwl", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.GaugeManager.Hwl;

            arParams[6] = new NpgsqlParameter("levelGeo", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.GaugeManager.LevelGeo;

            arParams[7] = new NpgsqlParameter("zeroDatum", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.GaugeManager.ZeroDatum;

            arParams[8] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.AssetGuid;

            arParams[9] = new NpgsqlParameter("gaugeTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = assetManager.GaugeManager.GaugeTypeManager.GaugeTypeGuid;

            arParams[10] = new NpgsqlParameter("frequencyTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[10].Direction = ParameterDirection.Input;
            arParams[10].Value = assetManager.GaugeManager.GaugeFrequencyTypeManager.FrequencyTypeGuid;

            arParams[11] = new NpgsqlParameter("seasonTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[11].Direction = ParameterDirection.Input;
            arParams[11].Value = assetManager.GaugeManager.GaugeSeasonTypeManager.SeasonTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}