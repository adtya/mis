﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public enum SubChainageStatusEnum
    {
        Created = 1,

        Inactive = 2,

        Active = 3,

        Dropped = 4,

        Halted = 5,

        Completed = 6
    }
}