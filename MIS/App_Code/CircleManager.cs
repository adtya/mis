﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class CircleManager
    {
        #region Public Constructors

        public CircleManager()
        {

        }

        #endregion


        #region Private Members

        private Guid circleGuid;
        private string circleName;
        private string circleCode;
        private List<DivisionManager> divisionManagerList;

        #endregion


        #region Public Getter/Setter Properties

        public Guid CircleGuid
        {
            get
            {
                return circleGuid;
            }
            set
            {
                circleGuid = value;
            }
        }

        public string CircleCode
        {
            get
            {
                return circleCode;
            }
            set
            {
                circleCode = value;
            }
        }

        public string CircleName
        {
            get
            {
                return circleName;
            }
            set
            {
                circleName = value;
            }
        }

        public List<DivisionManager> DivisionManagerList
        {
            get
            {
                return divisionManagerList;
            }
            set
            {
                divisionManagerList = value;
            }
        }   

        #endregion
    }
}