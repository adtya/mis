﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RevetmentTypeDB
    {
        protected internal List<RevetmentTypeManager> GetRevetmentTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("revet_type_uuid, ");
            sqlCommand.Append("revet_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_revetment_type ");
            sqlCommand.Append("ORDER BY revet_type_name ");
            sqlCommand.Append(";");

            List<RevetmentTypeManager> revetmentTypeManagerList = new List<RevetmentTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RevetmentTypeManager revetmentManagerObj = new RevetmentTypeManager();

                    revetmentManagerObj.RevetTypeGuid = new Guid(reader["revet_type_uuid"].ToString());
                    revetmentManagerObj.RevetTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["revet_type_name"].ToString());

                    revetmentTypeManagerList.Add(revetmentManagerObj);
                }
            }

            return revetmentTypeManagerList;
        }
    }
}