﻿using NodaMoney;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class AssetDB
    {
        protected internal List<AssetManager> GetAssets()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_uuid, ");
            sqlCommand.Append("obj1.asset_code, ");
            sqlCommand.Append("obj1.asset_name, ");
            sqlCommand.Append("obj1.initiation_date, ");
            sqlCommand.Append("obj1.completion_date, ");
            sqlCommand.Append("obj2.division_uuid, ");
            sqlCommand.Append("obj2.division_code, ");
            sqlCommand.Append("obj2.division_name, ");
            sqlCommand.Append("obj3.scheme_uuid, ");
            sqlCommand.Append("obj3.scheme_code, ");
            sqlCommand.Append("obj3.scheme_name, ");
            sqlCommand.Append("obj4.asset_type_uuid, ");
            sqlCommand.Append("obj4.asset_type_code, ");
            sqlCommand.Append("obj4.asset_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_assets obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_divisions obj2 ON obj2.division_uuid = obj1.division_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_schemes obj3 ON obj3.scheme_uuid = obj1.scheme_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_type obj4 ON obj4.asset_type_uuid = obj1.asset_type_uuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("asset_name ");
            sqlCommand.Append(";");

            List<AssetManager> assetManagerList = new List<AssetManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    AssetManager assetManagerObj = new AssetManager();

                    assetManagerObj.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    assetManagerObj.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_code"].ToString());
                    assetManagerObj.AssetName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_name"].ToString());
                    assetManagerObj.InitiationDate = DateTime.Parse(reader["initiation_date"].ToString());
                    assetManagerObj.CompletionDate = DateTime.Parse(reader["completion_date"].ToString());

                    assetManagerObj.DivisionManager.DivisionGuid = new Guid(reader["division_uuid"].ToString());
                    assetManagerObj.DivisionManager.DivisionCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["division_code"].ToString());
                    assetManagerObj.DivisionManager.DivisionName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["division_name"].ToString());

                    assetManagerObj.SchemeManager.SchemeGuid = new Guid(reader["scheme_uuid"].ToString());
                    assetManagerObj.SchemeManager.SchemeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["scheme_code"].ToString());
                    assetManagerObj.SchemeManager.SchemeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["scheme_name"].ToString());

                    assetManagerObj.AssetTypeManager.AssetTypeGuid = new Guid(reader["asset_type_uuid"].ToString());
                    assetManagerObj.AssetTypeManager.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_type_code"].ToString());
                    assetManagerObj.AssetTypeManager.AssetTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_type_name"].ToString());

                    assetManagerList.Add(assetManagerObj);
                }
            }
            return assetManagerList;
        }

        protected internal List<AssetManager> GetKeyAssets(Guid assetTypeGuid, Guid divisionGuid, Guid schemeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");           
            sqlCommand.Append("obj1.* ");            
            sqlCommand.Append("FROM mis_assets obj1 ");
            sqlCommand.Append("WHERE obj1.asset_type_uuid = ANY(SELECT key_asset_type_uuid FROM mis_key_asset_lookup WHERE asset_type_uuid = :assetTypeGuid) ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.division_uuid = :divisionGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.scheme_uuid = :schemeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetTypeGuid;

            arParams[1] = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = divisionGuid;

            arParams[2] = new NpgsqlParameter("schemeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = schemeGuid;

            List<AssetManager> assetManagerList = new List<AssetManager>();            

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    AssetManager assetDataObj = new AssetManager();

                    assetDataObj.DivisionManager.DivisionGuid = new Guid(reader["division_uuid"].ToString());

                    assetDataObj.SchemeManager.SchemeGuid = new Guid(reader["scheme_uuid"].ToString());

                    assetDataObj.AssetTypeManager.AssetTypeGuid = new Guid(reader["asset_type_uuid"].ToString());

                    assetDataObj.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    assetDataObj.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_code"].ToString());
                    assetDataObj.DrawingCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["drawing_code"].ToString());
                    assetDataObj.Amtd = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["amtd"].ToString());
                    assetDataObj.AssetName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_name"].ToString());
                    assetDataObj.StartChainage = Convert.ToInt32(reader["chainage_start"].ToString());
                    assetDataObj.EndChainage = Convert.ToInt32(reader["chainage_end"].ToString());
                    assetDataObj.UtmEast1 = Convert.ToInt32(reader["utm_e1"].ToString());
                    assetDataObj.UtmEast2 = Convert.ToInt32(reader["utm_e2"].ToString());
                    assetDataObj.UtmNorth1 = Convert.ToInt32(reader["utm_n1"].ToString());
                    assetDataObj.UtmNorth2 = Convert.ToInt32(reader["utm_n2"].ToString());
                    assetDataObj.InitiationDate = DateTime.Parse(reader["initiation_date"].ToString());
                    assetDataObj.CompletionDate = DateTime.Parse(reader["completion_date"].ToString());
                    assetDataObj.AssetCost = Money.Parse(reader["asset_cost"].ToString(), Currency.FromCode("INR", "ISO-4217"));

                    assetManagerList.Add(assetDataObj);
                }                    
            }
            return assetManagerList;

        }

        protected internal int AddAsset(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_assets ");
            sqlCommand.Append("( ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("asset_code, ");
            sqlCommand.Append("drawing_code, ");
            sqlCommand.Append("amtd, ");
            sqlCommand.Append("asset_name, ");
            sqlCommand.Append("chainage_start, ");
            sqlCommand.Append("chainage_end, ");
            sqlCommand.Append("utm_e1, ");
            sqlCommand.Append("utm_e2, ");
            sqlCommand.Append("utm_n1, ");
            sqlCommand.Append("utm_n2, ");
            sqlCommand.Append("initiation_date, ");
            sqlCommand.Append("completion_date, ");
            sqlCommand.Append("asset_cost, ");
            sqlCommand.Append("division_uuid, ");
            sqlCommand.Append("scheme_uuid, ");
            sqlCommand.Append("asset_type_uuid, ");
            sqlCommand.Append("key_asset_uuid, ");
            sqlCommand.Append("asset_number ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":assetCode, ");
            sqlCommand.Append(":drawingCode, ");
            sqlCommand.Append(":amtd, ");
            sqlCommand.Append(":assetName, ");
            sqlCommand.Append(":startChainage, ");
            sqlCommand.Append(":endChainage, ");
            sqlCommand.Append(":utmEast1, ");
            sqlCommand.Append(":utmEast2, ");
            sqlCommand.Append(":utmNorth1, ");
            sqlCommand.Append(":utmNorth2, ");
            sqlCommand.Append(":initiationDate, ");
            sqlCommand.Append(":completionDate, ");
            sqlCommand.Append(":assetCost, ");
            sqlCommand.Append(":divisionGuid, ");
            sqlCommand.Append(":schemeGuid, ");
            sqlCommand.Append(":assetTypeGuid, ");
            sqlCommand.Append(":keyAssetGuid, ");
            sqlCommand.Append(":assetNumber ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[19];

            arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.AssetGuid; //assetGuid;

            arParams[1] = new NpgsqlParameter("assetCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToUpper(assetManager.AssetCode);//CultureInfo.CurrentCulture.TextInfo.ToUpper(assetCode);

            arParams[2] = new NpgsqlParameter("drawingCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(assetManager.DrawingCode);

            arParams[3] = new NpgsqlParameter("amtd", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(assetManager.Amtd);

            arParams[4] = new NpgsqlParameter("assetName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(assetManager.AssetName);

            arParams[5] = new NpgsqlParameter("startChainage", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.StartChainage;

            arParams[6] = new NpgsqlParameter("endChainage", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.EndChainage;

            arParams[7] = new NpgsqlParameter("utmEast1", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.UtmEast1;

            arParams[8] = new NpgsqlParameter("utmEast2", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.UtmEast2;

            arParams[9] = new NpgsqlParameter("utmNorth1", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = assetManager.UtmNorth1;

            arParams[10] = new NpgsqlParameter("utmNorth2", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[10].Direction = ParameterDirection.Input;
            arParams[10].Value = assetManager.UtmNorth2;

            arParams[11] = new NpgsqlParameter("completionDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[11].Direction = ParameterDirection.Input;
            arParams[11].Value = assetManager.CompletionDate;

            arParams[12] = new NpgsqlParameter("assetCost", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[12].Direction = ParameterDirection.Input;
            arParams[12].Value = assetManager.AssetCost.Amount;

            arParams[13] = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[13].Direction = ParameterDirection.Input;
            arParams[13].Value = assetManager.DivisionManager.DivisionGuid;

            arParams[14] = new NpgsqlParameter("schemeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[14].Direction = ParameterDirection.Input;
            arParams[14].Value = assetManager.SchemeManager.SchemeGuid;

            arParams[15] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[15].Direction = ParameterDirection.Input;
            arParams[15].Value = assetManager.AssetTypeManager.AssetTypeGuid;

            arParams[16] = new NpgsqlParameter("assetNumber", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[16].Direction = ParameterDirection.Input;
            arParams[16].Value = assetManager.AssetNumber;

            arParams[17] = new NpgsqlParameter("keyAssetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[17].Direction = ParameterDirection.Input;
            arParams[17].Value = assetManager.KeyAssetGuid;

            arParams[18] = new NpgsqlParameter("initiationDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[18].Direction = ParameterDirection.Input;            
            arParams[18].Value = assetManager.InitiationDate;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int DeleteAsset(Guid assetGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE FROM ");
            sqlCommand.Append("mis_assets ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid =:assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = assetGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal List<AssetManager> GetAssetCode()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT DISTINCT ");
            sqlCommand.Append("mis_divisions.division_code, ");
            sqlCommand.Append("mis_schemes.scheme_code, ");
            sqlCommand.Append("mis_assets.asset_uuid, ");
            sqlCommand.Append("mis_assets.asset_code, ");
            sqlCommand.Append("mis_assets.asset_name, ");
            sqlCommand.Append("mis_asset_type.asset_type_code ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_assets ");
            sqlCommand.Append("INNER JOIN mis_divisions ON mis_assets.division_uuid = mis_divisions.division_uuid ");
            sqlCommand.Append("INNER JOIN mis_schemes ON mis_schemes.scheme_uuid = mis_assets.scheme_uuid ");
            sqlCommand.Append("INNER JOIN mis_asset_type ON mis_asset_type.asset_type_uuid = mis_assets.asset_type_uuid ");
            sqlCommand.Append("ORDER BY asset_code ");
            sqlCommand.Append(";");

            List<AssetManager> assetCodeList = new List<AssetManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    AssetManager assetCodeObj = new AssetManager();
                    assetCodeObj.DivisionManager.DivisionCode = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["division_code"].ToString());
                    assetCodeObj.SchemeManager.SchemeCode = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["scheme_code"].ToString());
                    assetCodeObj.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    assetCodeObj.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_code"].ToString());
                    assetCodeObj.AssetName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_name"].ToString());

                    assetCodeObj.AssetTypeManager.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_type_code"].ToString());

                    assetCodeList.Add(assetCodeObj);
                }
            }
            return assetCodeList;
        }

        protected internal AssetManager GetAssetDataByAssetUuid(Guid assetGuid, string assetTypeCode)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");            
            sqlCommand.Append("obj2.division_code, ");
            sqlCommand.Append("obj2.division_name, ");
            sqlCommand.Append("obj3.scheme_code, ");
            sqlCommand.Append("obj3.scheme_name, ");
            sqlCommand.Append("obj4.asset_type_code, ");
            sqlCommand.Append("obj4.asset_type_name, ");
            sqlCommand.Append("obj1.*, ");

            if(assetTypeCode == "BRG")
            {
                sqlCommand.Append("obj5.bridge_uuid, ");
                sqlCommand.Append("obj5.length, ");
                sqlCommand.Append("obj5.pier_no, ");
                sqlCommand.Append("obj5.road_width, ");
                sqlCommand.Append("obj6.bridge_type_name ");
            }
            else if(assetTypeCode=="CAN")
            {
                sqlCommand.Append("obj7.canal_uuid, ");
                sqlCommand.Append("obj7.bed_elevation1, ");
                sqlCommand.Append("obj7.bed_elevation2, ");
                sqlCommand.Append("obj7.length, ");
                sqlCommand.Append("obj7.bed_width, ");
                sqlCommand.Append("obj7.slope, ");
                sqlCommand.Append("obj7.slope2, ");
                sqlCommand.Append("obj7.average_depth, ");
                sqlCommand.Append("obj8.lining_type_name ");
            }
            else if (assetTypeCode == "CUL")
            {
                sqlCommand.Append("obj9.culvert_uuid, ");
                sqlCommand.Append("obj9.vent_no, ");
                sqlCommand.Append("obj9.vent_height, ");                
                sqlCommand.Append("obj9.vent_width, ");
                sqlCommand.Append("obj9.length, ");
                sqlCommand.Append("obj10.culvert_type_name ");
            }
            else if (assetTypeCode == "DRN")
            {
                sqlCommand.Append("obj11.drainage_uuid, ");
                sqlCommand.Append("obj11.us_elevation, ");
                sqlCommand.Append("obj11.ds_elevation, ");
                sqlCommand.Append("obj11.length, ");
                sqlCommand.Append("obj11.bed_width, ");
                sqlCommand.Append("obj11.slope1, ");
                sqlCommand.Append("obj11.slope2, ");
                sqlCommand.Append("obj11.average_depth ");
                
            }
            else if (assetTypeCode == "DRP")
            {
                sqlCommand.Append("obj12.drop_structure_uuid, ");
                sqlCommand.Append("obj12.us_invert_level, ");
                sqlCommand.Append("obj12.ds_invert_level, ");
                sqlCommand.Append("obj12.stilling_basin_width, ");
                sqlCommand.Append("obj12.stilling_basin_length ");               
            }
            else if (assetTypeCode == "EMB")
            {
                sqlCommand.Append("obj13.embankment_uuid, ");
                sqlCommand.Append("obj13.us_elevation, ");
                sqlCommand.Append("obj13.ds_elevation, ");
                sqlCommand.Append("obj13.crest_width, ");
                sqlCommand.Append("obj13.length, ");
                sqlCommand.Append("obj13.cs_slope1, ");
                sqlCommand.Append("obj13.cs_slope2, ");
                sqlCommand.Append("obj13.rs_slope1, ");
                sqlCommand.Append("obj13.rs_slope2, ");
                sqlCommand.Append("obj13.berm_slope1, ");
                sqlCommand.Append("obj13.berm_slope2, ");
                sqlCommand.Append("obj13.platform_width, ");
                sqlCommand.Append("obj13.average_height, ");
                sqlCommand.Append("obj14.crest_type_name, ");
                sqlCommand.Append("obj15.fill_type_name ");           

            }
            else if (assetTypeCode == "GAU")
            {
                sqlCommand.Append("obj16.gauge_uuid, ");
                sqlCommand.Append("obj16.start_date, ");
                sqlCommand.Append("obj16.end_date, ");
                sqlCommand.Append("obj16.active_yn, ");
                sqlCommand.Append("obj16.lwl_yn, ");
                sqlCommand.Append("obj16.hwl_yn, ");
                sqlCommand.Append("obj16.level_geo_yn, ");
                sqlCommand.Append("obj16.zero_datum, ");
                sqlCommand.Append("obj17.frequency_type_name, ");
                sqlCommand.Append("obj18.season_type_name, "); 
                sqlCommand.Append("obj19.gauge_type_name ");                
            }
            else if (assetTypeCode == "POR")
            {
                sqlCommand.Append("obj20.porcupine_uuid, ");
                sqlCommand.Append("obj20.screen_length, ");
                sqlCommand.Append("obj20.spacing_along_screen, ");
                sqlCommand.Append("obj20.screen_rows, ");
                sqlCommand.Append("obj20.number_of_layers, ");
                sqlCommand.Append("obj20.member_length, ");
                sqlCommand.Append("obj20.length, ");
                sqlCommand.Append("obj21.porcupine_type_name, ");
                sqlCommand.Append("obj22.material_type_name ");                
            }
            else if (assetTypeCode == "REG")
            {
                sqlCommand.Append("obj23.regulator_uuid, ");
                sqlCommand.Append("obj23.vent_number, ");
                sqlCommand.Append("obj23.vent_height, ");
                sqlCommand.Append("obj23.vent_width, ");
                sqlCommand.Append("obj24.regulator_type_name, ");
                sqlCommand.Append("obj25.regulator_gate_type_name ");               
            }
            else if (assetTypeCode == "REV")
            {
                sqlCommand.Append("obj26.revetment_uuid, ");
                sqlCommand.Append("obj26.length, ");
                sqlCommand.Append("obj26.plain_width, ");
                sqlCommand.Append("obj26.slope, ");
                sqlCommand.Append("obj27.revet_type_name, ");
                sqlCommand.Append("obj28.riverprot_type_name, ");
                sqlCommand.Append("obj29.waveprot_type_name ");
            }
            else if (assetTypeCode == "SLU")
            {
                sqlCommand.Append("obj30.sluice_uuid, ");
                sqlCommand.Append("obj30.vent_number, ");
                sqlCommand.Append("obj30.vent_height, ");
                sqlCommand.Append("obj30.vent_width, ");
                sqlCommand.Append("obj30.vent_diameter, ");
                sqlCommand.Append("obj31.sluice_type_name ");  
            }
            else if (assetTypeCode == "SPU")
            {
                sqlCommand.Append("obj32.spur_uuid, ");
                sqlCommand.Append("obj32.orientation, ");
                sqlCommand.Append("obj32.length, ");
                sqlCommand.Append("obj32.width, ");
                sqlCommand.Append("obj33.shape_type_name, ");
                sqlCommand.Append("obj34.construction_type_name, ");
                sqlCommand.Append("obj35.spur_revet_type_name ");
            }
            else if (assetTypeCode == "TRN")
            {
                sqlCommand.Append("obj36.turnout_uuid, ");
                sqlCommand.Append("obj36.inletbox_width, ");
                sqlCommand.Append("obj36.inletbox_length, ");
                sqlCommand.Append("obj36.outlet_no, ");
                sqlCommand.Append("obj36.outlet_height, ");
                sqlCommand.Append("obj36.outlet_width, ");
                sqlCommand.Append("obj37.turnout_type_name, ");
                sqlCommand.Append("obj38.turnout_gate_type_name ");
            }
            else if (assetTypeCode == "WEI")
            {
                sqlCommand.Append("obj39.weir_uuid, ");
                sqlCommand.Append("obj39.invert_level, ");
                sqlCommand.Append("obj39.crest_top_level, ");
                sqlCommand.Append("obj39.crest_top_width, ");
                sqlCommand.Append("obj40.weir_type_name ");
            }
           sqlCommand.Append("FROM mis_assets obj1 ");
           sqlCommand.Append("LEFT OUTER JOIN mis_divisions obj2 ON obj1.division_uuid = obj2.division_uuid ");
           sqlCommand.Append("LEFT OUTER JOIN mis_schemes obj3 ON obj1.scheme_uuid = obj3.scheme_uuid ");
           sqlCommand.Append("LEFT OUTER JOIN mis_asset_type obj4 ON obj1.asset_type_uuid = obj4.asset_type_uuid ");
           if (assetTypeCode == "BRG")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_bridge obj5 ON obj1.asset_uuid = obj5.asset_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_bridge_type obj6 ON obj6.bridge_type_uuid = obj5.bridge_type_uuid ");
           }
           else if (assetTypeCode == "CAN")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_canal obj7 ON obj1.asset_uuid = obj7.asset_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_canal_lining_type obj8 ON obj8.lining_type_uuid = obj7.lining_type_uuid ");
           }
           else if (assetTypeCode == "CUL")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_culvert obj9 ON obj1.asset_uuid = obj9.asset_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_culvert_type obj10 ON obj10.culvert_type_uuid = obj9.culvert_type_uuid ");
           }
           else if (assetTypeCode == "DRN")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_drainage obj11 ON obj1.asset_uuid = obj11.asset_uuid ");
           }
           else if (assetTypeCode == "DRP")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_drop_structure obj12 ON obj1.asset_uuid = obj12.asset_uuid ");
           }
           else if (assetTypeCode == "EMB")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_embankment obj13 ON obj1.asset_uuid = obj13.asset_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_embankment_crest_type obj14 ON obj14.crest_type_uuid = obj13.crest_type_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_embankment_fill_type obj15 ON obj15.fill_type_uuid = obj13.fill_type_uuid ");
           }
           else if (assetTypeCode == "GAU")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_gauge obj16 ON obj1.asset_uuid = obj16.asset_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_gauge_frequency_type obj17 ON obj17.frequency_type_uuid = obj16.frequency_type_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_gauge_season_type obj18 ON obj18.season_type_uuid = obj16.season_type_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_gauge_type obj19 ON obj19.gauge_type_uuid = obj16.gauge_type_uuid ");
           }
           else if (assetTypeCode == "POR")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_porcupine obj20 ON obj1.asset_uuid = obj20.asset_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_porcupine_type obj21 ON obj21.porcupine_type_uuid = obj20.porcupine_type_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_porcupine_material_type obj22 ON obj22.material_type_uuid = obj20.material_type_uuid ");
           }
           else if (assetTypeCode == "REG")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_regulator obj23 ON obj1.asset_uuid = obj23.asset_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_regulator_type obj24 ON obj24.regulator_type_uuid = obj23.regulator_type_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_regulator_gate_type obj25 ON obj25.regulator_gate_type_uuid = obj23.regulator_gate_type_uuid ");
           }
           else if (assetTypeCode == "REV")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_revetment obj26 ON obj1.asset_uuid = obj26.asset_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_revetment_type obj27 ON obj27.revet_type_uuid = obj26.revet_type_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_revetment_riverprot_type obj28 ON obj28.riverprot_type_uuid = obj26.riverprot_type_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_revetment_waveprot_type obj29 ON obj29.waveprot_type_uuid = obj26.waveprot_type_uuid ");
           }
           else if (assetTypeCode == "SLU")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_sluice obj30 ON obj1.asset_uuid = obj30.asset_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_sluice_type obj31 ON obj31.sluice_type_uuid = obj30.sluice_type_uuid ");

           }
           else if (assetTypeCode == "SPU")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_spur obj32 ON obj1.asset_uuid = obj32.asset_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_spur_shape_type obj33 ON obj33.shape_type_uuid = obj32.shape_type_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_spur_construction_type obj34 ON obj34.construction_type_uuid = obj32.construction_type_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_spur_revet_type obj35 ON obj35.spur_revet_type_uuid = obj32.spur_revet_type_uuid ");
           }
           else if (assetTypeCode == "TRN")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_turnout obj36 ON obj1.asset_uuid = obj36.asset_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_turnout_type obj37 ON obj37.turnout_type_uuid = obj36.turnout_type_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_turnout_gate_type obj38 ON obj38.turnout_gate_type_uuid = obj36.turnout_gate_type_uuid ");
           }
           else if (assetTypeCode == "WEI")
           {
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_weir obj39 ON obj1.asset_uuid = obj39.asset_uuid ");
               sqlCommand.Append("LEFT OUTER JOIN mis_asset_weir_type obj40 ON obj40.weir_type_uuid = obj39.weir_type_uuid ");
           }      

           sqlCommand.Append("WHERE ");
           sqlCommand.Append("obj1.asset_uuid = :assetGuid ");
           sqlCommand.Append(";");

           NpgsqlParameter[] arParams = new NpgsqlParameter[1];

           arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
           arParams[0].Direction = ParameterDirection.Input;
           arParams[0].Value = assetGuid;

           AssetManager assetDataObj = new AssetManager();           

           using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
           {
               while (reader.Read())
               {                
                   assetDataObj.DivisionManager.DivisionCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["division_code"].ToString());
                   assetDataObj.DivisionManager.DivisionName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["division_name"].ToString());

                   assetDataObj.SchemeManager.SchemeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["scheme_code"].ToString());
                   assetDataObj.SchemeManager.SchemeName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["scheme_name"].ToString());

                   assetDataObj.AssetTypeManager.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_type_code"].ToString());
                   assetDataObj.AssetTypeManager.AssetTypeName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_type_name"].ToString());

                   assetDataObj.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_code"].ToString());
                   assetDataObj.DrawingCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["drawing_code"].ToString());
                   assetDataObj.Amtd = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["amtd"].ToString());
                   assetDataObj.AssetName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_name"].ToString());
                   assetDataObj.StartChainage =  Convert.ToInt32(reader["chainage_start"].ToString());
                   assetDataObj.EndChainage = Convert.ToInt32(reader["chainage_end"].ToString());
                   assetDataObj.UtmEast1 = Convert.ToInt32(reader["utm_e1"].ToString());
                   assetDataObj.UtmEast2 = Convert.ToInt32(reader["utm_e2"].ToString());
                   assetDataObj.UtmNorth1 = Convert.ToInt32(reader["utm_n1"].ToString());
                   assetDataObj.UtmNorth2 = Convert.ToInt32(reader["utm_n2"].ToString());
                   assetDataObj.InitiationDate = DateTime.Parse(reader["initiation_date"].ToString());
                   assetDataObj.CompletionDate = DateTime.Parse(reader["completion_date"].ToString());
                   //if (reader["asset_cost"].ToString() == "0" || reader["asset_cost"].ToString() == "0.00")
                   //{
                   //    decimal a = 0m;
                   //    Money assetCost = new Money(a, "INR");
                   //    assetDataObj.AssetCost = Money.TryParse(, Currency.FromCode());

                   //     Money assetCost;
                   //    Money.TryParse(reader["asset_cost"].ToString(), Currency.FromCode("INR", "ISO-4217"), out assetCost);
                   //}
                   //else
                   //{
                   //    assetDataObj.AssetCost = Money.Parse(reader["asset_cost"].ToString(), Currency.FromCode("INR", "ISO-4217"));
                   //}

                   Money assetCost;
                   Money.TryParse(reader["asset_cost"].ToString(), Currency.FromCode("INR", "ISO-4217"), out assetCost);
                   assetDataObj.AssetCost = assetCost;

                   
                   assetDataObj.KeyAssetGuid = new Guid(reader["key_asset_uuid"].ToString());
                                
                   if(assetTypeCode=="BRG")
                   {
                       assetDataObj.BridgeManager.BridgeGuid = new Guid(reader["bridge_uuid"].ToString());
                       assetDataObj.BridgeManager.Length = Convert.ToDouble(reader["length"].ToString());
                       assetDataObj.BridgeManager.PierNumber = Convert.ToInt32(reader["pier_no"].ToString());
                       assetDataObj.BridgeManager.RoadWidth = Convert.ToDouble(reader["road_width"].ToString());
                       assetDataObj.BridgeManager.BridgeTypeManager.BridgeTypeName = reader["bridge_type_name"].ToString();                      
                   }
                   else if (assetTypeCode == "CAN")
                   {
                       assetDataObj.CanalManager.CanalGuid = new Guid(reader["canal_uuid"].ToString());
                       assetDataObj.CanalManager.UsElevation = Convert.ToDouble(reader["bed_elevation1"].ToString());
                       assetDataObj.CanalManager.DSElevation = Convert.ToDouble(reader["bed_elevation2"].ToString());
                       assetDataObj.CanalManager.Length = Convert.ToDouble(reader["length"].ToString());
                       assetDataObj.CanalManager.BedWidth = Convert.ToDouble(reader["bed_width"].ToString());
                       assetDataObj.CanalManager.Slope1 = Convert.ToDouble(reader["slope"].ToString());
                       assetDataObj.CanalManager.Slope2 = Convert.ToDouble(reader["slope2"].ToString());
                       assetDataObj.CanalManager.AverageDepth = Convert.ToDouble(reader["average_depth"].ToString());
                       assetDataObj.CanalManager.CanalLiningTypeManager.LiningTypeName = reader["lining_type_name"].ToString();
                   }
                   else if (assetTypeCode == "CUL")
                   {
                       assetDataObj.CulvertManager.CulvertGuid = new Guid(reader["culvert_uuid"].ToString());
                       assetDataObj.CulvertManager.VentNumber = Convert.ToInt32(reader["vent_no"].ToString());
                       assetDataObj.CulvertManager.VentHeight = Convert.ToDouble(reader["vent_height"].ToString());
                       assetDataObj.CulvertManager.VentWidth = Convert.ToDouble(reader["vent_width"].ToString());
                       assetDataObj.CulvertManager.LengthCulvert = Convert.ToDouble(reader["length"].ToString());
                       assetDataObj.CulvertManager.CulvertTypeManager.CulvertTypeName = reader["culvert_type_name"].ToString();
                   }
                   else if (assetTypeCode == "DRN")
                   {
                       assetDataObj.DrainageManager.DrainageGuid = new Guid(reader["drainage_uuid"].ToString());
                       assetDataObj.DrainageManager.UsElevationDrainage = Convert.ToDouble(reader["us_elevation"].ToString());
                       assetDataObj.DrainageManager.DsElevationDrainage = Convert.ToDouble(reader["ds_elevation"].ToString());
                       assetDataObj.DrainageManager.LengthDrainage = Convert.ToDouble(reader["length"].ToString());
                       assetDataObj.DrainageManager.BedWidthDrainage = Convert.ToDouble(reader["bed_width"].ToString());
                       assetDataObj.DrainageManager.Slope1Drainage = Convert.ToDouble(reader["slope1"].ToString());
                       assetDataObj.DrainageManager.Slope2Drainage = Convert.ToDouble(reader["slope2"].ToString());
                       assetDataObj.DrainageManager.AverageDepthDrainage = Convert.ToDouble(reader["average_depth"].ToString());
                   }
                   else if (assetTypeCode == "DRP")
                   {
                       assetDataObj.DropStructureManager.DropStructureGuid = new Guid(reader["drop_structure_uuid"].ToString());
                       assetDataObj.DropStructureManager.UsInvertLevel = Convert.ToDouble(reader["us_invert_level"].ToString());
                       assetDataObj.DropStructureManager.DsInvertLevel = Convert.ToDouble(reader["ds_invert_level"].ToString());
                       assetDataObj.DropStructureManager.StillingBasinWidth = Convert.ToDouble(reader["stilling_basin_width"].ToString());
                       assetDataObj.DropStructureManager.StillingBasinLength = Convert.ToDouble(reader["stilling_basin_length"].ToString());
                   }
                   else if (assetTypeCode == "EMB")
                   {
                       assetDataObj.EmbankmentManager.EmbankmentGuid = new Guid(reader["embankment_uuid"].ToString());
                       assetDataObj.EmbankmentManager.UsElevation = Convert.ToDouble(reader["us_elevation"].ToString());
                       assetDataObj.EmbankmentManager.DsElevation = Convert.ToDouble(reader["ds_elevation"].ToString());
                       assetDataObj.EmbankmentManager.CrestWidth = Convert.ToDouble(reader["crest_width"].ToString());
                       assetDataObj.EmbankmentManager.Length = Convert.ToDouble(reader["length"].ToString());
                       assetDataObj.EmbankmentManager.CsSlope1 = Convert.ToDouble(reader["cs_slope1"].ToString());
                       assetDataObj.EmbankmentManager.CsSlope2 = Convert.ToDouble(reader["cs_slope2"].ToString());
                       assetDataObj.EmbankmentManager.RsSlope1 = Convert.ToDouble(reader["rs_slope1"].ToString());
                       assetDataObj.EmbankmentManager.RsSlope2 = Convert.ToDouble(reader["rs_slope2"].ToString());
                       assetDataObj.EmbankmentManager.BermSlope1 = Convert.ToDouble(reader["berm_slope1"].ToString());
                       assetDataObj.EmbankmentManager.BermSlope2 = Convert.ToDouble(reader["berm_slope2"].ToString());
                       assetDataObj.EmbankmentManager.PlatformWidth = Convert.ToDouble(reader["platform_width"].ToString());
                       assetDataObj.EmbankmentManager.AverageHeight = Convert.ToDouble(reader["average_height"].ToString());
                       assetDataObj.EmbankmentManager.EmbankmentCrestTypeManager.CrestTypeName = reader["crest_type_name"].ToString();
                       assetDataObj.EmbankmentManager.EmbankmentFillTypeManager.FillTypeName = reader["fill_type_name"].ToString();                       
                   }
                   else if (assetTypeCode == "GAU")
                   {
                       assetDataObj.GaugeManager.GaugeGuid = new Guid(reader["gauge_uuid"].ToString());
                       assetDataObj.GaugeManager.StartDate = DateTime.Parse(reader["start_date"].ToString());
                       assetDataObj.GaugeManager.EndDate = DateTime.Parse(reader["end_date"].ToString());
                       assetDataObj.GaugeManager.Active = Convert.ToBoolean(reader["active_yn"].ToString());
                       assetDataObj.GaugeManager.Lwl = Convert.ToBoolean(reader["lwl_yn"].ToString());
                       assetDataObj.GaugeManager.Hwl = Convert.ToBoolean(reader["hwl_yn"].ToString());
                       assetDataObj.GaugeManager.LevelGeo = Convert.ToBoolean(reader["level_geo_yn"].ToString());
                       assetDataObj.GaugeManager.ZeroDatum = Convert.ToDouble(reader["zero_datum"].ToString());
                       assetDataObj.GaugeManager.GaugeFrequencyTypeManager.FrequencyTypeName = reader["frequency_type_name"].ToString();
                       assetDataObj.GaugeManager.GaugeSeasonTypeManager.SeasonTypeName = reader["season_type_name"].ToString();
                       assetDataObj.GaugeManager.GaugeTypeManager.GaugeTypeName = reader["gauge_type_name"].ToString();                      
                   }
                   else if (assetTypeCode == "POR")
                   {
                       assetDataObj.PorcupineManager.PorcupineGuid = new Guid(reader["porcupine_uuid"].ToString());
                       assetDataObj.PorcupineManager.ScreenLength = Convert.ToDouble(reader["screen_length"].ToString());
                       assetDataObj.PorcupineManager.SpacingAlongScreen = Convert.ToDouble(reader["spacing_along_screen"].ToString());
                       assetDataObj.PorcupineManager.ScreenRows = Convert.ToInt32(reader["screen_rows"].ToString());
                       assetDataObj.PorcupineManager.NumberOfLayers = Convert.ToInt32(reader["number_of_layers"].ToString());
                       assetDataObj.PorcupineManager.MemberLength = Convert.ToDouble(reader["member_length"].ToString());
                       assetDataObj.PorcupineManager.LengthPorcupine = Convert.ToDouble(reader["length"].ToString());
                       assetDataObj.PorcupineManager.PorcupineTypeManager.PorcupineTypeName = reader["porcupine_type_name"].ToString();
                       assetDataObj.PorcupineManager.PorcupineMaterialTypeManager.PorcupineMaterialTypeName = reader["material_type_name"].ToString();
                   }
                   else if (assetTypeCode == "REG")
                   {
                       assetDataObj.RegulatorManager.RegulatorGuid = new Guid(reader["regulator_uuid"].ToString());
                       assetDataObj.RegulatorManager.VentNumberRegulator = Convert.ToInt32(reader["vent_number"].ToString());
                       assetDataObj.RegulatorManager.VentHeightRegulator = Convert.ToDouble(reader["vent_height"].ToString());
                       assetDataObj.RegulatorManager.VentWidthRegulator = Convert.ToDouble(reader["vent_width"].ToString());
                       assetDataObj.RegulatorManager.RegulatorTypeManager.RegulatorTypeName = reader["regulator_type_name"].ToString();
                       assetDataObj.RegulatorManager.RegulatorGateTypeManager.RegulatorGateTypeName = reader["regulator_gate_type_name"].ToString();
                   }
                   else if (assetTypeCode == "REV")
                   {
                       assetDataObj.RevetmentManager.RevetmentGuid = new Guid(reader["revetment_uuid"].ToString());
                       assetDataObj.RevetmentManager.LengthRevetment = Convert.ToDouble(reader["length"].ToString());
                       assetDataObj.RevetmentManager.PlainWidth = Convert.ToDouble(reader["plain_width"].ToString());
                       assetDataObj.RevetmentManager.SlopeRevetment = Convert.ToDouble(reader["slope"].ToString());
                       assetDataObj.RevetmentManager.RevetmentTypeManager.RevetTypeName = reader["revet_type_name"].ToString();
                       assetDataObj.RevetmentManager.RevetmentRiverprotTypeManager.RiverprotTypeName = reader["riverprot_type_name"].ToString();
                       assetDataObj.RevetmentManager.RevetmentWaveprotTypeManager.WaveprotTypeName = reader["waveprot_type_name"].ToString();
                   }
                   else if (assetTypeCode == "SLU")
                   {
                       assetDataObj.SluiceManager.SluiceGuid = new Guid(reader["sluice_uuid"].ToString());
                       assetDataObj.SluiceManager.VentNumberSluice = Convert.ToInt32(reader["vent_number"].ToString());
                       assetDataObj.SluiceManager.VentHeightSluice = Convert.ToDouble(reader["vent_height"].ToString());
                       assetDataObj.SluiceManager.VentWidthSluice = Convert.ToDouble(reader["vent_width"].ToString());
                       assetDataObj.SluiceManager.VentDiameterSluice = Convert.ToDouble(reader["vent_diameter"].ToString());
                       assetDataObj.SluiceManager.SluiceTypeManager.SluiceTypeName = reader["sluice_type_name"].ToString();
                   }
                   else if (assetTypeCode == "SPU")
                   {
                       assetDataObj.SpurManager.SpurGuid = new Guid(reader["spur_uuid"].ToString());
                       assetDataObj.SpurManager.Orientation = Convert.ToDouble(reader["orientation"].ToString());
                       assetDataObj.SpurManager.LengthSpur = Convert.ToDouble(reader["length"].ToString());
                       assetDataObj.SpurManager.WidthSpur = Convert.ToDouble(reader["width"].ToString());
                       assetDataObj.SpurManager.SpurShapeTypeManager.ShapeTypeName = reader["shape_type_name"].ToString();
                       assetDataObj.SpurManager.SpurConstructionTypeManager.ConstructionTypeName = reader["construction_type_name"].ToString();
                       assetDataObj.SpurManager.SpurRevetTypeManager.SpurRevetTypeName = reader["spur_revet_type_name"].ToString();
                   }
                   else if (assetTypeCode == "TRN")
                   {
                       assetDataObj.TurnoutManager.TurnoutGuid = new Guid(reader["turnout_uuid"].ToString());
                       assetDataObj.TurnoutManager.InletboxWidth = Convert.ToDouble(reader["inletbox_width"].ToString());
                       assetDataObj.TurnoutManager.InletboxLength = Convert.ToDouble(reader["inletbox_length"].ToString());
                       assetDataObj.TurnoutManager.OutletNumber = Convert.ToInt32(reader["outlet_no"].ToString());
                       assetDataObj.TurnoutManager.OutletHeight = Convert.ToDouble(reader["outlet_height"].ToString());
                       assetDataObj.TurnoutManager.OutletWidth = Convert.ToDouble(reader["outlet_width"].ToString());
                       assetDataObj.TurnoutManager.TurnoutTypeManager.TurnoutTypeName = reader["turnout_type_name"].ToString();
                       assetDataObj.TurnoutManager.TurnoutGateTypeManager.TurnoutGateTypeName = reader["turnout_gate_type_name"].ToString();
                   }
                   else if (assetTypeCode == "WEI")
                   {
                       assetDataObj.WeirManager.WeirGuid = new Guid(reader["weir_uuid"].ToString());
                       assetDataObj.WeirManager.InvertLevel = Convert.ToDouble(reader["invert_level"].ToString());
                       assetDataObj.WeirManager.CrestTopLevel = Convert.ToDouble(reader["crest_top_level"].ToString());
                       assetDataObj.WeirManager.CrestTopWidth = Convert.ToDouble(reader["crest_top_width"].ToString());
                       assetDataObj.WeirManager.WeirTypeManager.WeirTypeName = reader["weir_type_name"].ToString();
                   }                  
               }
           }

           return assetDataObj;

        }

        protected internal List<AssetManager> GetKeyAssetData(Guid assetGuid)
        {          
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_uuid AS key_asset_uuid, ");
            sqlCommand.Append("obj1.asset_code AS key_asset_code, ");
            sqlCommand.Append("obj1.asset_name AS key_asset_name, ");
            sqlCommand.Append("obj1.initiation_date AS key_asset_initiation_date, ");
            sqlCommand.Append("obj1.completion_date AS key_asset_completion_date, ");
            sqlCommand.Append("obj1.asset_cost AS key_asset_cost ");
            sqlCommand.Append("FROM mis_assets obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_assets obj2 ON obj1.key_asset_uuid = obj2.asset_uuid ");            
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj2.asset_uuid = :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetGuid;

            List<AssetManager> appurtenantAssetManagerList = new List<AssetManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    AssetManager appurtenantAssetManagerObj = new AssetManager();

                    appurtenantAssetManagerObj.AssetGuid = new Guid(reader["key_asset_uuid"].ToString());
                    appurtenantAssetManagerObj.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["key_asset_code"].ToString());
                    appurtenantAssetManagerObj.AssetName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["key_asset_name"].ToString());
                    appurtenantAssetManagerObj.InitiationDate = DateTime.Parse(reader["key_asset_initiation_date"].ToString());
                    appurtenantAssetManagerObj.CompletionDate = DateTime.Parse(reader["key_asset_completion_date"].ToString());
                    appurtenantAssetManagerObj.AssetCost = Money.Parse(reader["key_asset_cost"].ToString(), Currency.FromCode("INR", "ISO-4217"));

                    appurtenantAssetManagerList.Add(appurtenantAssetManagerObj);

                }
            }
            return appurtenantAssetManagerList;
        }

        protected internal int UpdateAssetData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_assets ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("drawing_code= :drawingCode, ");
            sqlCommand.Append("amtd= :amtd, ");
            sqlCommand.Append("asset_name= :assetName, ");
            sqlCommand.Append("chainage_start= :startChainage, ");
            sqlCommand.Append("chainage_end= :endChainage, ");
            sqlCommand.Append("utm_e1= :utmEast1, ");
            sqlCommand.Append("utm_e2= :utmEast2, ");
            sqlCommand.Append("utm_n1= :utmNorth1, ");
            sqlCommand.Append("utm_n2= :utmNorth2, ");
            sqlCommand.Append("initiation_date= :initiationDate, ");
            sqlCommand.Append("completion_date= :completionDate, ");
            sqlCommand.Append("key_asset_uuid= :keyAssetGuid, ");
            sqlCommand.Append("asset_cost= :assetCost ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[14];

            arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.AssetGuid;

            arParams[1] = new NpgsqlParameter("drawingCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.DrawingCode;

            arParams[2] = new NpgsqlParameter("amtd", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.Amtd;

            arParams[3] = new NpgsqlParameter("assetName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.AssetName;

            arParams[4] = new NpgsqlParameter("startChainage", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.StartChainage;

            arParams[5] = new NpgsqlParameter("endChainage", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.EndChainage;

            arParams[6] = new NpgsqlParameter("utmEast1", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.UtmEast1;

            arParams[7] = new NpgsqlParameter("utmEast2", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.UtmEast2;

            arParams[8] = new NpgsqlParameter("utmNorth1", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.UtmNorth1;

            arParams[9] = new NpgsqlParameter("utmNorth2", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = assetManager.UtmNorth2;

            arParams[10] = new NpgsqlParameter("completionDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[10].Direction = ParameterDirection.Input;
            arParams[10].Value = assetManager.CompletionDate;

            arParams[11] = new NpgsqlParameter("assetCost", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[11].Direction = ParameterDirection.Input;
            arParams[11].Value = assetManager.AssetCost.Amount;

            arParams[12] = new NpgsqlParameter("initiationDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[12].Direction = ParameterDirection.Input;
            arParams[12].Value = assetManager.InitiationDate;

            arParams[13] = new NpgsqlParameter("keyAssetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[13].Direction = ParameterDirection.Input;
            arParams[13].Value = assetManager.KeyAssetGuid;
            
            return NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);
        }

        protected internal List<AssetManager> GetAssetCodeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("asset_code ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_assets ");
            sqlCommand.Append("ORDER BY asset_code ");
            sqlCommand.Append(";");

            List<AssetManager> assetCodeList = new List<AssetManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    AssetManager assetCodeObj = new AssetManager();

                    assetCodeObj.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    assetCodeObj.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_code"].ToString());

                    assetCodeList.Add(assetCodeObj);
                }
            }
            return assetCodeList;
        }

        protected internal List<AssetManager> GetAssetCodeListForPerformance()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("asset_code ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_assets ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid NOT IN (SELECT asset_uuid FROM mis_om_performance_monitoring WHERE current_monitoring_date =(Select current_monitoring_date FROM system_variables) ) ");
            sqlCommand.Append("ORDER BY asset_code ");
            sqlCommand.Append(";");

            List<AssetManager> assetCodeList = new List<AssetManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    AssetManager assetCodeObj = new AssetManager();

                    assetCodeObj.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    assetCodeObj.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_code"].ToString());

                    assetCodeList.Add(assetCodeObj);
                }
            }
            return assetCodeList;
        }

        protected internal List<AssetManager> GetAssetCodeDataForPerformance(Guid assetCodeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT DISTINCT ");
            sqlCommand.Append("mis_assets.asset_uuid, ");
            sqlCommand.Append("mis_assets.asset_code, ");
            sqlCommand.Append("mis_assets.asset_name, ");
            sqlCommand.Append("mis_asset_type.asset_type_uuid, ");
            sqlCommand.Append("mis_asset_type.asset_type_code ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_assets ");
            sqlCommand.Append("LEFT JOIN mis_asset_type ON mis_asset_type.asset_type_uuid = mis_assets.asset_type_uuid ");
            sqlCommand.Append("WHERE mis_assets.asset_uuid=:assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetCodeGuid;

            List<AssetManager> assetDataList = new List<AssetManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    AssetManager assetCodeObj = new AssetManager();
                    assetCodeObj.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    assetCodeObj.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_code"].ToString());
                    assetCodeObj.AssetName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_name"].ToString());
                    assetCodeObj.AssetTypeManager.AssetTypeGuid = new Guid(reader["asset_type_uuid"].ToString());
                    assetCodeObj.AssetTypeManager.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_type_code"].ToString());
                    assetDataList.Add(assetCodeObj);
                }
            }
            return assetDataList;
        }

        protected internal Array GetAssetNumbers(Guid assetTypeGuid, Guid schemeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_number ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_assets obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_types obj2 ON obj2.asset_type_uuid = obj1.asset_type_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_schemes obj3 ON obj3.scheme_uuid = obj1.scheme_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.asset_type_uuid = :assetTypeGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.scheme_uuid = :schemeGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.asset_number ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetTypeGuid;

            arParams[1] = new NpgsqlParameter("schemeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = schemeGuid;

            List<int> assetNumberList = new List<int>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    assetNumberList.Add(Int32.Parse(reader["asset_number"].ToString()));
                }
            }

            return assetNumberList.ToArray();
        }

        protected internal List<AssetManager> GetAssetCodeBasedOnAssetTypeForRoutineAnalysis(Guid assetTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_uuid, ");
            sqlCommand.Append("obj1.asset_code,");
            sqlCommand.Append("obj2.asset_type_uuid,");
            sqlCommand.Append("obj2.asset_type_code ");
            sqlCommand.Append("FROM mis_assets obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_type obj2 ON obj1.asset_type_uuid=obj2.asset_type_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.asset_type_uuid= :assetTypeGuid ");
            sqlCommand.Append("; ");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetTypeGuid;

            List<AssetManager> assetCodeManagerList = new List<AssetManager>();
            

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    AssetManager assetCodeManagerObj = new AssetManager();

                    assetCodeManagerObj.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    assetCodeManagerObj.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_code"].ToString());
                    assetCodeManagerObj.AssetTypeManager.AssetTypeGuid = new Guid(reader["asset_type_uuid"].ToString());
                    assetCodeManagerObj.AssetTypeManager.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_type_code"].ToString());

                    assetCodeManagerList.Add(assetCodeManagerObj);

                }
            }

            return assetCodeManagerList;
        }

        protected internal AssetManager GetAssetCodeDataForRoutineAnalysis(Guid assetGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_uuid, ");
            sqlCommand.Append("obj1.asset_code, ");
            sqlCommand.Append("obj1.asset_name, ");
            sqlCommand.Append("obj2.asset_type_uuid, ");
            sqlCommand.Append("obj2.asset_type_code, ");
            sqlCommand.Append("obj2.asset_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_assets obj1 ");
            sqlCommand.Append("LEFT JOIN mis_asset_type obj2 ON obj1.asset_type_uuid = obj2.asset_type_uuid ");
            sqlCommand.Append("WHERE obj1.asset_uuid=:assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetGuid;

            AssetManager assetCodeObj = new AssetManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {                    
                    assetCodeObj.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    assetCodeObj.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_code"].ToString());
                    assetCodeObj.AssetName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_name"].ToString());
                    assetCodeObj.AssetTypeManager.AssetTypeGuid = new Guid(reader["asset_type_uuid"].ToString());
                    assetCodeObj.AssetTypeManager.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_type_code"].ToString());
                    assetCodeObj.AssetTypeManager.AssetTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_type_name"].ToString());
                    
                }
            }
            return assetCodeObj;
        }

        protected internal AssetManager GetAssetTypeDataForRoutineAnalysis(Guid assetGuid, string assetTypeCode)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");           
            if (assetTypeCode == "BRG")
            {
                sqlCommand.Append("obj1.*, ");               
                sqlCommand.Append("obj2.bridge_type_name ");
                sqlCommand.Append("FROM mis_asset_bridge obj1 ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_bridge_type obj2 ON obj2.bridge_type_uuid = obj1.bridge_type_uuid ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj1.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");
            }
            else if (assetTypeCode == "CAN")
            {
                sqlCommand.Append("obj3.*, ");                
                sqlCommand.Append("obj4.lining_type_name ");
                sqlCommand.Append("FROM mis_asset_canal obj3 ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_canal_lining_type obj4 ON obj4.lining_type_uuid = obj3.lining_type_uuid ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj3.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");
            }
            else if (assetTypeCode == "CUL")
            {
                sqlCommand.Append("obj5.*, ");                
                sqlCommand.Append("obj6.culvert_type_name ");
                sqlCommand.Append("FROM mis_asset_culvert obj5 ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_culvert_type obj6 ON obj6.culvert_type_uuid = obj5.culvert_type_uuid ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj5.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");
            }
            else if (assetTypeCode == "DRN")
            {
                sqlCommand.Append("obj7.* ");
                sqlCommand.Append("FROM mis_asset_drainage obj7 ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj7.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");

            }
            else if (assetTypeCode == "DRP")
            {
                sqlCommand.Append("obj8.* ");
                sqlCommand.Append("FROM mis_asset_drop_structure obj8 ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj8.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");
            }
            else if (assetTypeCode == "EMB")
            {
                sqlCommand.Append("obj9.*, ");
                sqlCommand.Append("obj10.crest_type_name, ");
                sqlCommand.Append("obj11.fill_type_name ");
                sqlCommand.Append("FROM mis_asset_embankment obj9 ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_embankment_crest_type obj10 ON obj10.crest_type_uuid = obj9.crest_type_uuid ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_embankment_fill_type obj11 ON obj11.fill_type_uuid = obj9.fill_type_uuid ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj9.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");

            }
            else if (assetTypeCode == "GAU")
            {
                sqlCommand.Append("obj12.*, ");                
                sqlCommand.Append("obj13.frequency_type_name, ");
                sqlCommand.Append("obj14.season_type_name, ");
                sqlCommand.Append("obj15.gauge_type_name ");
                sqlCommand.Append("FROM mis_asset_gauge obj12 ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_gauge_frequency_type obj13 ON obj13.frequency_type_uuid = obj12.frequency_type_uuid ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_gauge_season_type obj14 ON obj14.season_type_uuid = obj12.season_type_uuid ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_gauge_type obj15 ON obj15.gauge_type_uuid = obj12.gauge_type_uuid ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj12.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");
            }
            else if (assetTypeCode == "POR")
            {
                sqlCommand.Append("obj16.*, ");
                sqlCommand.Append("obj17.porcupine_type_name, ");
                sqlCommand.Append("obj18.material_type_name ");
                sqlCommand.Append("FROM mis_asset_porcupine obj16 ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_porcupine_type obj17 ON obj17.porcupine_type_uuid = obj16.porcupine_type_uuid ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_porcupine_material_type obj18 ON obj18.material_type_uuid = obj16.material_type_uuid ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj16.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");
            }
            else if (assetTypeCode == "REG")
            {
                sqlCommand.Append("obj19.*, ");
                sqlCommand.Append("obj20.regulator_type_name, ");
                sqlCommand.Append("obj21.regulator_gate_type_name ");
                sqlCommand.Append("FROM mis_asset_regulator obj19 ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_regulator_type obj20 ON obj20.regulator_type_uuid = obj19.regulator_type_uuid ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_regulator_gate_type obj21 ON obj21.regulator_gate_type_uuid = obj19.regulator_gate_type_uuid ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj19.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");
            }
            else if (assetTypeCode == "REV")
            {
                sqlCommand.Append("obj22.*, ");
                sqlCommand.Append("obj23.revet_type_name, ");
                sqlCommand.Append("obj24.riverprot_type_name, ");
                sqlCommand.Append("obj25.waveprot_type_name ");
                sqlCommand.Append("FROM mis_asset_revetment obj22 ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_revetment_type obj23 ON obj23.revet_type_uuid = obj22.revet_type_uuid ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_revetment_riverprot_type obj24 ON obj24.riverprot_type_uuid = obj22.riverprot_type_uuid ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_revetment_waveprot_type obj25 ON obj25.waveprot_type_uuid = obj22.waveprot_type_uuid ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj22.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");
            }
            else if (assetTypeCode == "SLU")
            {
                sqlCommand.Append("obj26.*, ");
                sqlCommand.Append("obj27.sluice_type_name ");
                sqlCommand.Append("FROM mis_asset_sluice obj26 ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_sluice_type obj27 ON obj27.sluice_type_uuid = obj26.sluice_type_uuid ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj26.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");
            }
            else if (assetTypeCode == "SPU")
            {
                sqlCommand.Append("obj28.*, ");
                sqlCommand.Append("obj29.shape_type_name, ");
                sqlCommand.Append("obj30.construction_type_name, ");
                sqlCommand.Append("obj31.spur_revet_type_name ");
                sqlCommand.Append("FROM mis_asset_spur obj28 ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_spur_shape_type obj29 ON obj29.shape_type_uuid = obj28.shape_type_uuid ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_spur_construction_type obj30 ON obj30.construction_type_uuid = obj28.construction_type_uuid ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_spur_revet_type obj31 ON obj31.spur_revet_type_uuid = obj28.spur_revet_type_uuid ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj28.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");
            }
            else if (assetTypeCode == "TRN")
            {
                sqlCommand.Append("obj32.*, ");
                sqlCommand.Append("obj33.turnout_type_name, ");
                sqlCommand.Append("obj34.turnout_gate_type_name ");
                sqlCommand.Append("FROM mis_asset_turnout obj32 ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_turnout_type obj33 ON obj33.turnout_type_uuid = obj32.turnout_type_uuid ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_turnout_gate_type obj34 ON obj34.turnout_gate_type_uuid = obj32.turnout_gate_type_uuid ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj32.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");
            }
            else if (assetTypeCode == "WEI")
            {
                sqlCommand.Append("obj35.*, ");
                sqlCommand.Append("obj36.weir_type_name ");
                sqlCommand.Append("FROM mis_asset_weir obj35 ");
                sqlCommand.Append("LEFT OUTER JOIN mis_asset_weir_type obj36 ON obj36.weir_type_uuid = obj35.weir_type_uuid ");
                sqlCommand.Append("WHERE ");
                sqlCommand.Append("obj35.asset_uuid = :assetGuid ");
                sqlCommand.Append(";");

            }          
            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetGuid;

            AssetManager assetTypeDataObj = new AssetManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {                   

                    if (assetTypeCode == "BRG")
                    {
                        assetTypeDataObj.BridgeManager.BridgeGuid = new Guid(reader["bridge_uuid"].ToString());
                        assetTypeDataObj.BridgeManager.Length = Convert.ToDouble(reader["length"].ToString());
                        assetTypeDataObj.BridgeManager.PierNumber = Convert.ToInt32(reader["pier_no"].ToString());
                        assetTypeDataObj.BridgeManager.RoadWidth = Convert.ToDouble(reader["road_width"].ToString());
                        assetTypeDataObj.BridgeManager.BridgeTypeManager.BridgeTypeName = reader["bridge_type_name"].ToString();
                    }
                    else if (assetTypeCode == "CAN")
                    {
                        assetTypeDataObj.CanalManager.CanalGuid = new Guid(reader["canal_uuid"].ToString());
                        assetTypeDataObj.CanalManager.UsElevation = Convert.ToDouble(reader["bed_elevation1"].ToString());
                        assetTypeDataObj.CanalManager.DSElevation = Convert.ToDouble(reader["bed_elevation2"].ToString());
                        assetTypeDataObj.CanalManager.Length = Convert.ToDouble(reader["length"].ToString());
                        assetTypeDataObj.CanalManager.BedWidth = Convert.ToDouble(reader["bed_width"].ToString());
                        assetTypeDataObj.CanalManager.Slope1 = Convert.ToDouble(reader["slope"].ToString());
                        assetTypeDataObj.CanalManager.Slope2 = Convert.ToDouble(reader["slope2"].ToString());
                        assetTypeDataObj.CanalManager.AverageDepth = Convert.ToDouble(reader["average_depth"].ToString());
                        assetTypeDataObj.CanalManager.CanalLiningTypeManager.LiningTypeName = reader["lining_type_name"].ToString();
                    }
                    else if (assetTypeCode == "CUL")
                    {
                        assetTypeDataObj.CulvertManager.CulvertGuid = new Guid(reader["culvert_uuid"].ToString());
                        assetTypeDataObj.CulvertManager.VentNumber = Convert.ToInt32(reader["vent_no"].ToString());
                        assetTypeDataObj.CulvertManager.VentHeight = Convert.ToDouble(reader["vent_height"].ToString());
                        assetTypeDataObj.CulvertManager.VentWidth = Convert.ToDouble(reader["vent_width"].ToString());
                        assetTypeDataObj.CulvertManager.LengthCulvert = Convert.ToDouble(reader["length"].ToString());
                        assetTypeDataObj.CulvertManager.CulvertTypeManager.CulvertTypeName = reader["culvert_type_name"].ToString();
                    }
                    else if (assetTypeCode == "DRN")
                    {
                        assetTypeDataObj.DrainageManager.DrainageGuid = new Guid(reader["drainage_uuid"].ToString());
                        assetTypeDataObj.DrainageManager.UsElevationDrainage = Convert.ToDouble(reader["us_elevation"].ToString());
                        assetTypeDataObj.DrainageManager.DsElevationDrainage = Convert.ToDouble(reader["ds_elevation"].ToString());
                        assetTypeDataObj.DrainageManager.LengthDrainage = Convert.ToDouble(reader["length"].ToString());
                        assetTypeDataObj.DrainageManager.BedWidthDrainage = Convert.ToDouble(reader["bed_width"].ToString());
                        assetTypeDataObj.DrainageManager.Slope1Drainage = Convert.ToDouble(reader["slope1"].ToString());
                        assetTypeDataObj.DrainageManager.Slope2Drainage = Convert.ToDouble(reader["slope2"].ToString());
                        assetTypeDataObj.DrainageManager.AverageDepthDrainage = Convert.ToDouble(reader["average_depth"].ToString());
                    }
                    else if (assetTypeCode == "DRP")
                    {
                        assetTypeDataObj.DropStructureManager.DropStructureGuid = new Guid(reader["drop_structure_uuid"].ToString());
                        assetTypeDataObj.DropStructureManager.UsInvertLevel = Convert.ToDouble(reader["us_invert_level"].ToString());
                        assetTypeDataObj.DropStructureManager.DsInvertLevel = Convert.ToDouble(reader["ds_invert_level"].ToString());
                        assetTypeDataObj.DropStructureManager.StillingBasinWidth = Convert.ToDouble(reader["stilling_basin_width"].ToString());
                        assetTypeDataObj.DropStructureManager.StillingBasinLength = Convert.ToDouble(reader["stilling_basin_length"].ToString());
                    }
                    else if (assetTypeCode == "EMB")
                    {
                        assetTypeDataObj.EmbankmentManager.EmbankmentGuid = new Guid(reader["embankment_uuid"].ToString());
                        assetTypeDataObj.EmbankmentManager.UsElevation = Convert.ToDouble(reader["us_elevation"].ToString());
                        assetTypeDataObj.EmbankmentManager.DsElevation = Convert.ToDouble(reader["ds_elevation"].ToString());
                        assetTypeDataObj.EmbankmentManager.CrestWidth = Convert.ToDouble(reader["crest_width"].ToString());
                        assetTypeDataObj.EmbankmentManager.Length = Convert.ToDouble(reader["length"].ToString());
                        assetTypeDataObj.EmbankmentManager.CsSlope1 = Convert.ToDouble(reader["cs_slope1"].ToString());
                        assetTypeDataObj.EmbankmentManager.CsSlope2 = Convert.ToDouble(reader["cs_slope2"].ToString());
                        assetTypeDataObj.EmbankmentManager.RsSlope1 = Convert.ToDouble(reader["rs_slope1"].ToString());
                        assetTypeDataObj.EmbankmentManager.RsSlope2 = Convert.ToDouble(reader["rs_slope2"].ToString());
                        assetTypeDataObj.EmbankmentManager.BermSlope1 = Convert.ToDouble(reader["berm_slope1"].ToString());
                        assetTypeDataObj.EmbankmentManager.BermSlope2 = Convert.ToDouble(reader["berm_slope2"].ToString());
                        assetTypeDataObj.EmbankmentManager.PlatformWidth = Convert.ToDouble(reader["platform_width"].ToString());
                        assetTypeDataObj.EmbankmentManager.AverageHeight = Convert.ToDouble(reader["average_height"].ToString());
                        assetTypeDataObj.EmbankmentManager.EmbankmentCrestTypeManager.CrestTypeName = reader["crest_type_name"].ToString();
                        assetTypeDataObj.EmbankmentManager.EmbankmentFillTypeManager.FillTypeName = reader["fill_type_name"].ToString();
                    }
                    else if (assetTypeCode == "GAU")
                    {
                        assetTypeDataObj.GaugeManager.GaugeGuid = new Guid(reader["gauge_uuid"].ToString());
                        assetTypeDataObj.GaugeManager.StartDate = DateTime.Parse(reader["start_date"].ToString());
                        assetTypeDataObj.GaugeManager.EndDate = DateTime.Parse(reader["end_date"].ToString());
                        assetTypeDataObj.GaugeManager.Active = Convert.ToBoolean(reader["active_yn"].ToString());
                        assetTypeDataObj.GaugeManager.Lwl = Convert.ToBoolean(reader["lwl_yn"].ToString());
                        assetTypeDataObj.GaugeManager.Hwl = Convert.ToBoolean(reader["hwl_yn"].ToString());
                        assetTypeDataObj.GaugeManager.LevelGeo = Convert.ToBoolean(reader["level_geo_yn"].ToString());
                        assetTypeDataObj.GaugeManager.ZeroDatum = Convert.ToDouble(reader["zero_datum"].ToString());
                        assetTypeDataObj.GaugeManager.GaugeFrequencyTypeManager.FrequencyTypeName = reader["frequency_type_name"].ToString();
                        assetTypeDataObj.GaugeManager.GaugeSeasonTypeManager.SeasonTypeName = reader["season_type_name"].ToString();
                        assetTypeDataObj.GaugeManager.GaugeTypeManager.GaugeTypeName = reader["gauge_type_name"].ToString();
                    }
                    else if (assetTypeCode == "POR")
                    {
                        assetTypeDataObj.PorcupineManager.PorcupineGuid = new Guid(reader["porcupine_uuid"].ToString());
                        assetTypeDataObj.PorcupineManager.ScreenLength = Convert.ToDouble(reader["screen_length"].ToString());
                        assetTypeDataObj.PorcupineManager.SpacingAlongScreen = Convert.ToDouble(reader["spacing_along_screen"].ToString());
                        assetTypeDataObj.PorcupineManager.ScreenRows = Convert.ToInt32(reader["screen_rows"].ToString());
                        assetTypeDataObj.PorcupineManager.NumberOfLayers = Convert.ToInt32(reader["number_of_layers"].ToString());
                        assetTypeDataObj.PorcupineManager.MemberLength = Convert.ToDouble(reader["member_length"].ToString());
                        assetTypeDataObj.PorcupineManager.LengthPorcupine = Convert.ToDouble(reader["length"].ToString());
                        assetTypeDataObj.PorcupineManager.PorcupineTypeManager.PorcupineTypeName = reader["porcupine_type_name"].ToString();
                        assetTypeDataObj.PorcupineManager.PorcupineMaterialTypeManager.PorcupineMaterialTypeName = reader["material_type_name"].ToString();
                    }
                    else if (assetTypeCode == "REG")
                    {
                        assetTypeDataObj.RegulatorManager.RegulatorGuid = new Guid(reader["regulator_uuid"].ToString());
                        assetTypeDataObj.RegulatorManager.VentNumberRegulator = Convert.ToInt32(reader["vent_number"].ToString());
                        assetTypeDataObj.RegulatorManager.VentHeightRegulator = Convert.ToDouble(reader["vent_height"].ToString());
                        assetTypeDataObj.RegulatorManager.VentWidthRegulator = Convert.ToDouble(reader["vent_width"].ToString());
                        assetTypeDataObj.RegulatorManager.RegulatorTypeManager.RegulatorTypeName = reader["regulator_type_name"].ToString();
                        assetTypeDataObj.RegulatorManager.RegulatorGateTypeManager.RegulatorGateTypeName = reader["regulator_gate_type_name"].ToString();
                    }
                    else if (assetTypeCode == "REV")
                    {
                        assetTypeDataObj.RevetmentManager.RevetmentGuid = new Guid(reader["revetment_uuid"].ToString());
                        assetTypeDataObj.RevetmentManager.LengthRevetment = Convert.ToDouble(reader["length"].ToString());
                        assetTypeDataObj.RevetmentManager.PlainWidth = Convert.ToDouble(reader["plain_width"].ToString());
                        assetTypeDataObj.RevetmentManager.SlopeRevetment = Convert.ToDouble(reader["slope"].ToString());
                        assetTypeDataObj.RevetmentManager.RevetmentTypeManager.RevetTypeName = reader["revet_type_name"].ToString();
                        assetTypeDataObj.RevetmentManager.RevetmentRiverprotTypeManager.RiverprotTypeName = reader["riverprot_type_name"].ToString();
                        assetTypeDataObj.RevetmentManager.RevetmentWaveprotTypeManager.WaveprotTypeName = reader["waveprot_type_name"].ToString();
                    }
                    else if (assetTypeCode == "SLU")
                    {
                        assetTypeDataObj.SluiceManager.SluiceGuid = new Guid(reader["sluice_uuid"].ToString());
                        assetTypeDataObj.SluiceManager.VentNumberSluice = Convert.ToInt32(reader["vent_number"].ToString());
                        assetTypeDataObj.SluiceManager.VentHeightSluice = Convert.ToDouble(reader["vent_height"].ToString());
                        assetTypeDataObj.SluiceManager.VentWidthSluice = Convert.ToDouble(reader["vent_width"].ToString());
                        assetTypeDataObj.SluiceManager.VentDiameterSluice = Convert.ToDouble(reader["vent_diameter"].ToString());
                        assetTypeDataObj.SluiceManager.SluiceTypeManager.SluiceTypeName = reader["sluice_type_name"].ToString();
                    }
                    else if (assetTypeCode == "SPU")
                    {
                        assetTypeDataObj.SpurManager.SpurGuid = new Guid(reader["spur_uuid"].ToString());
                        assetTypeDataObj.SpurManager.Orientation = Convert.ToDouble(reader["orientation"].ToString());
                        assetTypeDataObj.SpurManager.LengthSpur = Convert.ToDouble(reader["length"].ToString());
                        assetTypeDataObj.SpurManager.WidthSpur = Convert.ToDouble(reader["width"].ToString());
                        assetTypeDataObj.SpurManager.SpurShapeTypeManager.ShapeTypeName = reader["shape_type_name"].ToString();
                        assetTypeDataObj.SpurManager.SpurConstructionTypeManager.ConstructionTypeName = reader["construction_type_name"].ToString();
                        assetTypeDataObj.SpurManager.SpurRevetTypeManager.SpurRevetTypeName = reader["spur_revet_type_name"].ToString();
                    }
                    else if (assetTypeCode == "TRN")
                    {
                        assetTypeDataObj.TurnoutManager.TurnoutGuid = new Guid(reader["turnout_uuid"].ToString());
                        assetTypeDataObj.TurnoutManager.InletboxWidth = Convert.ToDouble(reader["inletbox_width"].ToString());
                        assetTypeDataObj.TurnoutManager.InletboxLength = Convert.ToDouble(reader["inletbox_length"].ToString());
                        assetTypeDataObj.TurnoutManager.OutletNumber = Convert.ToInt32(reader["outlet_no"].ToString());
                        assetTypeDataObj.TurnoutManager.OutletHeight = Convert.ToDouble(reader["outlet_height"].ToString());
                        assetTypeDataObj.TurnoutManager.OutletWidth = Convert.ToDouble(reader["outlet_width"].ToString());
                        assetTypeDataObj.TurnoutManager.TurnoutTypeManager.TurnoutTypeName = reader["turnout_type_name"].ToString();
                        assetTypeDataObj.TurnoutManager.TurnoutGateTypeManager.TurnoutGateTypeName = reader["turnout_gate_type_name"].ToString();
                    }
                    else if (assetTypeCode == "WEI")
                    {
                        assetTypeDataObj.WeirManager.WeirGuid = new Guid(reader["weir_uuid"].ToString());
                        assetTypeDataObj.WeirManager.InvertLevel = Convert.ToDouble(reader["invert_level"].ToString());
                        assetTypeDataObj.WeirManager.CrestTopLevel = Convert.ToDouble(reader["crest_top_level"].ToString());
                        assetTypeDataObj.WeirManager.CrestTopWidth = Convert.ToDouble(reader["crest_top_width"].ToString());
                        assetTypeDataObj.WeirManager.WeirTypeManager.WeirTypeName = reader["weir_type_name"].ToString();
                    }
                }
            }

            return assetTypeDataObj;
        }

        protected internal List<AssetManager> GetAssetCodeForFieldSurvey()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_uuid, ");
            sqlCommand.Append("obj1.asset_code ");       
            sqlCommand.Append("FROM mis_assets obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_divisions obj2 ON obj1.division_uuid=obj2.division_uuid ");       
            sqlCommand.Append("LEFT OUTER JOIN mis_circles obj3 ON obj3.circle_uuid=obj2.circle_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.asset_type_uuid = ANY(SELECT asset_type_uuid FROM mis_asset_types WHERE asset_type_code = 'EMB') ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj3.circle_uuid = ANY(SELECT circle_uuid FROM system_variables) ");
            sqlCommand.Append("; ");            
   
            List<AssetManager> assetCodeManagerList = new List<AssetManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    AssetManager assetCodeManagerObj = new AssetManager();

                    assetCodeManagerObj.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    assetCodeManagerObj.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_code"].ToString());
                    
                    assetCodeManagerList.Add(assetCodeManagerObj);
                }
            }
            return assetCodeManagerList;
        }
        
    }
}