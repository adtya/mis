﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RoleDB
    {

        protected internal List<RoleManager> GetRoles()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_user_roles ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("role_can_be_deleted = :roleCanBeDeleted ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("role_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("roleCanBeDeleted", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = true;
           
            List<RoleManager> roleList = new List<RoleManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    RoleManager roleObj = new RoleManager();

                    roleObj.RoleGuid = new Guid(reader["role_uuid"].ToString());
                    roleObj.RoleName = reader["role_name"].ToString();
                    roleObj.RoleCanBeDeleted = Convert.ToBoolean(reader["role_can_be_deleted"].ToString());

                    roleList.Add(roleObj);
                }
            }

            return roleList;
        }

    }
}