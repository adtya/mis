﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public static class ActualCompletionDateUpdater
    {
        public static void UpdateSubChainageActualDate()
        {
            List<SubChainageManager> subChainageList = new List<SubChainageManager>();
            subChainageList = new SubChainageDB().GetAllSubChainageDetails();

            //foreach (DateTime d in TimerManager.DateList) // Get the timer results
            //{
                foreach (SubChainageManager subChainageObj in subChainageList)
                {
                    int result = DateTime.Compare(subChainageObj.SubChainageActualCompletionDate, DateTime.Today);

                    if (result < 0)
                    {
                        subChainageObj.SubChainageActualCompletionDate = DateTime.Today;

                        new SubChainageDB().UpdateSubChainageActualDate(subChainageObj);
                    }
                }
           // }
            
        }
    }
}