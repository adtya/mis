﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class AssetTypeDB
    {
        protected internal AssetTypeManager GetAssetType(Guid assetTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_type_uuid, ");
            sqlCommand.Append("obj1.asset_type_code, ");
            sqlCommand.Append("obj1.asset_type_name, ");
            sqlCommand.Append("obj1.has_gates ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_types obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.asset_type_uuid = :assetTypeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = assetTypeGuid;

            AssetTypeManager assetTypeManagerObj = new AssetTypeManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    assetTypeManagerObj.AssetTypeGuid = new Guid(reader["asset_type_uuid"].ToString());
                    assetTypeManagerObj.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_type_code"].ToString());
                    assetTypeManagerObj.AssetTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_type_name"].ToString());
                    assetTypeManagerObj.HasGates = Boolean.Parse(reader["has_gates"].ToString());
                }
            }

            return assetTypeManagerObj;

        }

        protected internal AssetTypeManager GetAssetTypeWithUnit(Guid assetTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_type_uuid, ");
            sqlCommand.Append("obj1.asset_type_code, ");
            sqlCommand.Append("obj1.asset_type_name, ");
            sqlCommand.Append("obj1.has_gates ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_types obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.asset_type_uuid = :assetTypeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = assetTypeGuid;

            AssetTypeManager assetTypeManagerObj = new AssetTypeManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    assetTypeManagerObj.AssetTypeGuid = new Guid(reader["asset_type_uuid"].ToString());
                    assetTypeManagerObj.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_type_code"].ToString());
                    assetTypeManagerObj.AssetTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_type_name"].ToString());
                    assetTypeManagerObj.HasGates = Boolean.Parse(reader["has_gates"].ToString());

                    assetTypeManagerObj.UnitManager = new UnitDB().GetUnit(new Guid(reader["unit_uuid"].ToString()));
                }
            }

            return assetTypeManagerObj;

        }


        protected internal List<AssetTypeManager> GetAssetTypes()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_type_uuid, ");
            sqlCommand.Append("obj1.asset_type_code, ");
            sqlCommand.Append("obj1.asset_type_name, ");
            sqlCommand.Append("obj1.has_gates ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_types obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.asset_type_name ");
            sqlCommand.Append(";");

            List<AssetTypeManager> assetTypeManagerList = new List<AssetTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    AssetTypeManager assetTypeManagerObj = new AssetTypeManager();

                    assetTypeManagerObj.AssetTypeGuid = new Guid(reader["asset_type_uuid"].ToString());
                    assetTypeManagerObj.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_type_code"].ToString());
                    assetTypeManagerObj.AssetTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_type_name"].ToString());
                    assetTypeManagerObj.HasGates = Boolean.Parse(reader["has_gates"].ToString());

                    assetTypeManagerList.Add(assetTypeManagerObj);
                }
            }

            return assetTypeManagerList;

        }

        protected internal List<AssetTypeManager> GetAssetTypesWithUnit()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_type_uuid, ");
            sqlCommand.Append("obj1.asset_type_code, ");
            sqlCommand.Append("obj1.asset_type_name, ");
            sqlCommand.Append("obj1.has_gates ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_types obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.asset_type_name ");
            sqlCommand.Append(";");

            List<AssetTypeManager> assetTypeManagerList = new List<AssetTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    AssetTypeManager assetTypeManagerObj = new AssetTypeManager();

                    assetTypeManagerObj.AssetTypeGuid = new Guid(reader["asset_type_uuid"].ToString());
                    assetTypeManagerObj.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_type_code"].ToString());
                    assetTypeManagerObj.AssetTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_type_name"].ToString());
                    assetTypeManagerObj.HasGates = Boolean.Parse(reader["has_gates"].ToString());

                    assetTypeManagerObj.UnitManager = new UnitDB().GetUnit(new Guid(reader["unit_uuid"].ToString()));

                    assetTypeManagerList.Add(assetTypeManagerObj);
                }
            }

            return assetTypeManagerList;

        }

        protected internal bool CheckAssetTypeExistence(string assetTypeName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_types obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.asset_type_name = :assetTypeName ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("assetTypeName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(assetTypeName);

            bool result = false;

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    result = true;
                }
            }

            return result;
        }


        protected internal int AddAssetType(AssetTypeManager assetTypeManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_types ");
            sqlCommand.Append("( ");
            sqlCommand.Append("asset_type_uuid, ");
            sqlCommand.Append("asset_type_code, ");
            sqlCommand.Append("asset_type_name, ");
            sqlCommand.Append("has_gates, ");
            sqlCommand.Append("unit_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":assetTypeGuid, ");
            sqlCommand.Append(":assetTypeCode, ");
            sqlCommand.Append(":assetTypeName, ");
            sqlCommand.Append(":hasGates, ");
            sqlCommand.Append(":unitGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[5];

            arParams[0] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("assetTypeCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToUpper(assetTypeManager.AssetTypeCode);

            arParams[2] = new NpgsqlParameter("assetTypeName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(assetTypeManager.AssetTypeName);

            arParams[3] = new NpgsqlParameter("hasGates", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetTypeManager.HasGates;

            arParams[4] = new NpgsqlParameter("unitGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetTypeManager.UnitManager.UnitGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }


        protected internal int UpdateAssetType(AssetTypeManager assetTypeManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_asset_types ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("asset_type_code = :assetTypeCode, ");
            sqlCommand.Append("asset_type_name = :assetTypeName, ");
            sqlCommand.Append("has_gates = :hasGates, ");
            sqlCommand.Append("unit_uuid = :unitGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_type_uuid = :assetTypeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[5];

            arParams[0] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("assetTypeCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToUpper(assetTypeManager.AssetTypeCode);

            arParams[2] = new NpgsqlParameter("assetTypeName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(assetTypeManager.AssetTypeName);

            arParams[3] = new NpgsqlParameter("hasGates", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetTypeManager.HasGates;

            arParams[4] = new NpgsqlParameter("unitGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetTypeManager.UnitManager.UnitGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }


        protected internal int DeleteUnitType(Guid unitTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_types ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_type_uuid = :assetTypeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = unitTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}