﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SpurShapeTypeDB
    {
        protected internal List<SpurShapeTypeManager> GetSpurShapeTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("shape_type_uuid, ");
            sqlCommand.Append("shape_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_spur_shape_type ");
            sqlCommand.Append("ORDER BY shape_type_name ");
            sqlCommand.Append(";");

            List<SpurShapeTypeManager> spurShapeTypeManagerList = new List<SpurShapeTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    SpurShapeTypeManager spurShapeTypeManagerObj = new SpurShapeTypeManager();

                    spurShapeTypeManagerObj.ShapeTypeGuid = new Guid(reader["shape_type_uuid"].ToString());
                    spurShapeTypeManagerObj.ShapeTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["shape_type_name"].ToString());

                    spurShapeTypeManagerList.Add(spurShapeTypeManagerObj);
                }
            }

            return spurShapeTypeManagerList;
        }
    }
}