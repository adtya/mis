﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class EmbankmentDB
    {
        protected internal int AddEmbankmentData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_embankment ");
            sqlCommand.Append("( ");
            sqlCommand.Append("embankment_uuid, ");
            sqlCommand.Append("us_elevation, ");
            sqlCommand.Append("ds_elevation, ");
            sqlCommand.Append("length, ");
            sqlCommand.Append("crest_width, ");
            sqlCommand.Append("cs_slope1, ");
            sqlCommand.Append("cs_slope2, ");
            sqlCommand.Append("rs_slope1, ");
            sqlCommand.Append("rs_slope2, ");
            sqlCommand.Append("berm_slope1, ");
            sqlCommand.Append("berm_slope2, ");
            sqlCommand.Append("platform_width, ");
            sqlCommand.Append("average_height, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("crest_type_uuid, ");
            sqlCommand.Append("fill_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":embankmentGuid, ");
            sqlCommand.Append(":usElevation, ");
            sqlCommand.Append(":dsElevation, ");
            sqlCommand.Append(":length, ");
            sqlCommand.Append(":crestWidth, ");
            sqlCommand.Append(":csSlope1, ");
            sqlCommand.Append(":csSlope2, ");
            sqlCommand.Append(":rsSlope1, ");
            sqlCommand.Append(":rsSlope2, ");
            sqlCommand.Append(":bermSlope1, ");
            sqlCommand.Append(":bermSlope2, ");
            sqlCommand.Append(":platformWidth, ");
            sqlCommand.Append(":averageHeight, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":crestTypeGuid, ");
            sqlCommand.Append(":fillTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[16];

            arParams[0] = new NpgsqlParameter("embankmentGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("usElevation", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.EmbankmentManager.UsElevation;

            arParams[2] = new NpgsqlParameter("dsElevation", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.EmbankmentManager.DsElevation;

            arParams[3] = new NpgsqlParameter("length", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.EmbankmentManager.Length;

            arParams[4] = new NpgsqlParameter("crestWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.EmbankmentManager.CrestWidth;

            arParams[5] = new NpgsqlParameter("csSlope1", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.EmbankmentManager.CsSlope1;

            arParams[6] = new NpgsqlParameter("csSlope2", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.EmbankmentManager.CsSlope2;

            arParams[7] = new NpgsqlParameter("rsSlope1", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.EmbankmentManager.RsSlope1;

            arParams[8] = new NpgsqlParameter("rsSlope2", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.EmbankmentManager.RsSlope2;

            arParams[9] = new NpgsqlParameter("bermSlope1", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = assetManager.EmbankmentManager.BermSlope1;

            arParams[10] = new NpgsqlParameter("bermSlope2", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[10].Direction = ParameterDirection.Input;
            arParams[10].Value = assetManager.EmbankmentManager.BermSlope2;

            arParams[11] = new NpgsqlParameter("platformWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[11].Direction = ParameterDirection.Input;
            arParams[11].Value = assetManager.EmbankmentManager.PlatformWidth;

            arParams[12] = new NpgsqlParameter("averageHeight", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[12].Direction = ParameterDirection.Input;
            arParams[12].Value = assetManager.EmbankmentManager.AverageHeight;     
        
            arParams[13] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[13].Direction = ParameterDirection.Input;
            arParams[13].Value = assetManager.AssetGuid;

            arParams[14] = new NpgsqlParameter("crestTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[14].Direction = ParameterDirection.Input;
            arParams[14].Value = assetManager.EmbankmentManager.EmbankmentCrestTypeManager.CrestTypeGuid;

            arParams[15] = new NpgsqlParameter("fillTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[15].Direction = ParameterDirection.Input;
            arParams[15].Value = assetManager.EmbankmentManager.EmbankmentFillTypeManager.FillTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateEmbankmentData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_embankment ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("us_elevation= :usElevation, ");
            sqlCommand.Append("ds_elevation= :dsElevation, ");
            sqlCommand.Append("length= :length, ");
            sqlCommand.Append("crest_width= :crestWidth, ");
            sqlCommand.Append("cs_slope1= :csSlope1, ");
            sqlCommand.Append("cs_slope2= :csSlope2, ");
            sqlCommand.Append("rs_slope1= :rsSlope1, ");
            sqlCommand.Append("rs_slope2= :rsSlope2, ");
            sqlCommand.Append("berm_slope1= :bermSlope1, ");
            sqlCommand.Append("berm_slope2= :bermSlope2, ");
            sqlCommand.Append("platform_width= :platformWidth, ");
            sqlCommand.Append("average_height= :averageHeight, ");
            sqlCommand.Append("crest_type_uuid= :crestTypeGuid, ");
            sqlCommand.Append("fill_type_uuid= :fillTypeGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[16];

            arParams[0] = new NpgsqlParameter("embankmentGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.EmbankmentManager.EmbankmentGuid;

            arParams[1] = new NpgsqlParameter("usElevation", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.EmbankmentManager.UsElevation;

            arParams[2] = new NpgsqlParameter("dsElevation", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.EmbankmentManager.DsElevation;

            arParams[3] = new NpgsqlParameter("length", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.EmbankmentManager.Length;

            arParams[4] = new NpgsqlParameter("crestWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.EmbankmentManager.CrestWidth;

            arParams[5] = new NpgsqlParameter("csSlope1", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.EmbankmentManager.CsSlope1;

            arParams[6] = new NpgsqlParameter("csSlope2", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.EmbankmentManager.CsSlope2;

            arParams[7] = new NpgsqlParameter("rsSlope1", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.EmbankmentManager.RsSlope1;

            arParams[8] = new NpgsqlParameter("rsSlope2", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.EmbankmentManager.RsSlope2;

            arParams[9] = new NpgsqlParameter("bermSlope1", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = assetManager.EmbankmentManager.BermSlope1;

            arParams[10] = new NpgsqlParameter("bermSlope2", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[10].Direction = ParameterDirection.Input;
            arParams[10].Value = assetManager.EmbankmentManager.BermSlope2;

            arParams[11] = new NpgsqlParameter("platformWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[11].Direction = ParameterDirection.Input;
            arParams[11].Value = assetManager.EmbankmentManager.PlatformWidth;

            arParams[12] = new NpgsqlParameter("averageHeight", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[12].Direction = ParameterDirection.Input;
            arParams[12].Value = assetManager.EmbankmentManager.AverageHeight;

            arParams[13] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[13].Direction = ParameterDirection.Input;
            arParams[13].Value = assetManager.AssetGuid;

            arParams[14] = new NpgsqlParameter("crestTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[14].Direction = ParameterDirection.Input;
            arParams[14].Value = assetManager.EmbankmentManager.EmbankmentCrestTypeManager.CrestTypeGuid;

            arParams[15] = new NpgsqlParameter("fillTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[15].Direction = ParameterDirection.Input;
            arParams[15].Value = assetManager.EmbankmentManager.EmbankmentFillTypeManager.FillTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}