﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class GaugeManager
    {
        #region Private Members

        private Guid gaugeGuid;      
        private DateTime startDate;
        private DateTime endDate;
        private bool active;
        private bool lwl;
        private bool hwl;
        private bool levelGeo;
        private double zeroDatum;   
        //private AssetManager assetManager;
        private GaugeTypeManager gaugeTypeManager;
        private GaugeFrequencyTypeManager gaugeFrequencyTypeManager;
        private GaugeSeasonTypeManager gaugeSeasonTypeManager;
        
        #endregion


        #region Public Constructors

        public GaugeManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid GaugeGuid
        {
            get
            {
                return gaugeGuid;
            }
            set
            {
                gaugeGuid = value;
            }
        }       

        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
            }
        }
       
        public bool Lwl
        {
            get
            {
                return lwl;
            }
            set
            {
                lwl = value;
            }
        }

        public bool Hwl
        {
            get
            {
                return hwl;
            }
            set
            {
                hwl = value;
            }
        }

        public bool LevelGeo
        {
            get
            {
                return levelGeo;
            }
            set
            {
                levelGeo = value;
            }
        }

        public double ZeroDatum
        {
            get
            {
                return zeroDatum;
            }
            set
            {
                zeroDatum = value;
            }
        }                    
         
        public GaugeTypeManager GaugeTypeManager
        {
            get
            {
                if (gaugeTypeManager == null)
                {
                    gaugeTypeManager = new GaugeTypeManager();
                }
                return gaugeTypeManager;
            }
            set
            {
                gaugeTypeManager = value;
            }
        }

        public GaugeFrequencyTypeManager GaugeFrequencyTypeManager
        {
            get
            {
                if (gaugeFrequencyTypeManager == null)
                {
                    gaugeFrequencyTypeManager = new GaugeFrequencyTypeManager();
                }
                return gaugeFrequencyTypeManager;
            }
            set
            {
                gaugeFrequencyTypeManager = value;
            }
        }

        public GaugeSeasonTypeManager GaugeSeasonTypeManager
        {
            get
            {
                if (gaugeSeasonTypeManager == null)
                {
                    gaugeSeasonTypeManager = new GaugeSeasonTypeManager();
                }
                return gaugeSeasonTypeManager;
            }
            set
            {
                gaugeSeasonTypeManager = value;
            }
        }
        
        //public AssetManager AssetManager
        //{
        //    get
        //    {
        //        if (assetManager == null)
        //        {
        //            assetManager = new AssetManager();
        //        }
        //        return assetManager;
        //    }
        //    set
        //    {
        //        assetManager = value;
        //    }
        //}

        #endregion
    }
}