﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class PmcSiteEngineerProgressDB
    {
        protected internal int AddPmcSiteEngineerProgress(SubChainagePhysicalProgressManager subChainagePhysicalProgressManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_sub_chainage_pmc_site_engineer_progress ");
            sqlCommand.Append("( ");
            sqlCommand.Append("pmc_site_engineer_progress_uuid, ");
            sqlCommand.Append("physical_progress_uuid, ");
            sqlCommand.Append("pmc_site_engineer_project_relationship_uuid, ");
            sqlCommand.Append("pmc_site_engineer_arose ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            foreach (PmcSiteEngineerProgressManager pmcSiteEngineerProgressManagerObj in subChainagePhysicalProgressManager.PmcSiteEngineerProgressManager)
            {
                int index = subChainagePhysicalProgressManager.PmcSiteEngineerProgressManager.IndexOf(pmcSiteEngineerProgressManagerObj);

                sqlCommand.Append("( ");

                sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                sqlCommand.Append("'" + subChainagePhysicalProgressManager.PhysicalProgressGuid + "', ");
                sqlCommand.Append("'" + pmcSiteEngineerProgressManagerObj.ProjectAndPmcSiteEngineersRelationManager.PmcSiteEngineerProjectRelationshipGuid + "', ");
                sqlCommand.Append("'" + pmcSiteEngineerProgressManagerObj.PmcSiteEngineerArose + "' ");

                if (index == subChainagePhysicalProgressManager.PmcSiteEngineerProgressManager.Count - 1)
                {
                    sqlCommand.Append(") ");
                }
                else
                {
                    sqlCommand.Append("), ");
                }
            }

            sqlCommand.Append(";");

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());

            return rowsAffected;
        }

        protected internal List<PmcSiteEngineerProgressManager> GetPmcSiteEngineerProgressWithPmcSiteEngineerName(Guid subChainagePhysicalProgressGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.*, ");
            sqlCommand.Append("obj2.pmc_site_engineer_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_sub_chainage_pmc_site_engineer_progress obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_project_project_and_pmc_site_engineers_relation obj2 ON obj2.project_and_pmc_site_engineers_relation_uuid = obj1.pmc_site_engineer_project_relationship_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.physical_progress_uuid = :subChainagePhysicalProgressGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("subChainagePhysicalProgressGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = subChainagePhysicalProgressGuid;

            List<PmcSiteEngineerProgressManager> pmcSiteEngineerProgressManagerList = new List<PmcSiteEngineerProgressManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    PmcSiteEngineerProgressManager pmcSiteEngineerProgressManagerObj = new PmcSiteEngineerProgressManager();

                    pmcSiteEngineerProgressManagerObj.PmcSiteEngineerProgressGuid = new Guid(reader["pmc_site_engineer_progress_uuid"].ToString());
                    pmcSiteEngineerProgressManagerObj.ProjectAndPmcSiteEngineersRelationManager.PmcSiteEngineerProjectRelationshipGuid = new Guid(reader["pmc_site_engineer_project_relationship_uuid"].ToString());
                    pmcSiteEngineerProgressManagerObj.ProjectAndPmcSiteEngineersRelationManager.SiteEngineerName = reader["pmc_site_engineer_name"].ToString();
                    pmcSiteEngineerProgressManagerObj.PmcSiteEngineerArose = Boolean.Parse(reader["pmc_site_engineer_arose"].ToString());

                    pmcSiteEngineerProgressManagerList.Add(pmcSiteEngineerProgressManagerObj);
                }
            }

            return pmcSiteEngineerProgressManagerList;
        }

    }
}