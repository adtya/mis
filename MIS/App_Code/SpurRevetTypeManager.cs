﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SpurRevetTypeManager
    {
        #region Private Members

        private Guid spurRevetTypeGuid;
        private string spurRevetTypeName;
       
       #endregion


        #region Public Constructors

        public SpurRevetTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid SpurRevetTypeGuid
        {
            get
            {
                return spurRevetTypeGuid;
            }
            set
            {
                spurRevetTypeGuid = value;
            }
        }

        public string SpurRevetTypeName
        {
            get
            {
                return spurRevetTypeName;
            }
            set
            {
                spurRevetTypeName = value;
            }
        }        

        #endregion
    }
}