﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class DrainageDB
    {
        protected internal int AddDrainageData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_drainage ");
            sqlCommand.Append("( ");
            sqlCommand.Append("drainage_uuid, ");
            sqlCommand.Append("us_elevation, ");
            sqlCommand.Append("ds_elevation, ");
            sqlCommand.Append("length, ");
            sqlCommand.Append("bed_width, ");
            sqlCommand.Append("slope1, ");
            sqlCommand.Append("slope2, ");
            sqlCommand.Append("average_depth, ");
            sqlCommand.Append("asset_uuid ");            
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":drainageGuid, ");
            sqlCommand.Append(":usElevationDrainage, ");
            sqlCommand.Append(":dsElevationDrainage, ");
            sqlCommand.Append(":lengthDrainage, ");
            sqlCommand.Append(":bedWidthDrainage, ");
            sqlCommand.Append(":slope1Drainage, ");
            sqlCommand.Append(":slope2Drainage, ");
            sqlCommand.Append(":averageDepthDrainage, ");
            sqlCommand.Append(":assetGuid ");            
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[9];

            arParams[0] = new NpgsqlParameter("drainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("usElevationDrainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.DrainageManager.UsElevationDrainage;

            arParams[2] = new NpgsqlParameter("dsElevationDrainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.DrainageManager.DsElevationDrainage;

            arParams[3] = new NpgsqlParameter("lengthDrainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.DrainageManager.LengthDrainage;

            arParams[4] = new NpgsqlParameter("bedWidthDrainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.DrainageManager.BedWidthDrainage;

            arParams[5] = new NpgsqlParameter("slope1Drainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.DrainageManager.Slope1Drainage;

            arParams[6] = new NpgsqlParameter("slope2Drainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.DrainageManager.Slope2Drainage;

            arParams[7] = new NpgsqlParameter("averageDepthDrainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.DrainageManager.AverageDepthDrainage;

            arParams[8] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.AssetGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateDrainageData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_drainage ");
            sqlCommand.Append("SET ");            
            sqlCommand.Append("us_elevation= :usElevationDrainage, ");
            sqlCommand.Append("ds_elevation= :dsElevationDrainage, ");
            sqlCommand.Append("length= :lengthDrainage, ");
            sqlCommand.Append("bed_width= :bedWidthDrainage, ");
            sqlCommand.Append("slope1= :slope1Drainage, ");
            sqlCommand.Append("slope2= :slope2Drainage, ");
            sqlCommand.Append("average_depth= :averageDepthDrainage ");                   
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[9];

            arParams[0] = new NpgsqlParameter("drainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.DrainageManager.DrainageGuid;

            arParams[1] = new NpgsqlParameter("usElevationDrainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.DrainageManager.UsElevationDrainage;

            arParams[2] = new NpgsqlParameter("dsElevationDrainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.DrainageManager.DsElevationDrainage;

            arParams[3] = new NpgsqlParameter("lengthDrainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.DrainageManager.LengthDrainage;

            arParams[4] = new NpgsqlParameter("bedWidthDrainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.DrainageManager.BedWidthDrainage;

            arParams[5] = new NpgsqlParameter("slope1Drainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.DrainageManager.Slope1Drainage;

            arParams[6] = new NpgsqlParameter("slope2Drainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.DrainageManager.Slope2Drainage;

            arParams[7] = new NpgsqlParameter("averageDepthDrainage", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.DrainageManager.AverageDepthDrainage;

            arParams[8] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.AssetGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}