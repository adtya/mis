﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class PorcupineDB
    {
        protected internal int AddPorcupineData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_porcupine ");
            sqlCommand.Append("( ");
            sqlCommand.Append("porcupine_uuid, ");
            sqlCommand.Append("screen_length, ");
            sqlCommand.Append("spacing_along_screen, ");
            sqlCommand.Append("screen_rows, ");
            sqlCommand.Append("number_of_layers, ");
            sqlCommand.Append("member_length, ");
            sqlCommand.Append("length, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("porcupine_type_uuid, ");
            sqlCommand.Append("material_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":porcupineGuid, ");
            sqlCommand.Append(":screenLength, ");
            sqlCommand.Append(":spacingAlongScreen, ");
            sqlCommand.Append(":screenRows, ");
            sqlCommand.Append(":numberOfLayers, ");
            sqlCommand.Append(":memberLength, ");
            sqlCommand.Append(":lengthPorcupine, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":porcupineTypeGuid, ");
            sqlCommand.Append(":porcupineMaterialTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[10];

            arParams[0] = new NpgsqlParameter("porcupineGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("screenLength", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.PorcupineManager.ScreenLength;

            arParams[2] = new NpgsqlParameter("spacingAlongScreen", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.PorcupineManager.SpacingAlongScreen;

            arParams[3] = new NpgsqlParameter("screenRows", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.PorcupineManager.ScreenRows;

            arParams[4] = new NpgsqlParameter("numberOfLayers", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.PorcupineManager.NumberOfLayers;

            arParams[5] = new NpgsqlParameter("memberLength", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.PorcupineManager.MemberLength;

            arParams[6] = new NpgsqlParameter("lengthPorcupine", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.PorcupineManager.LengthPorcupine;

            arParams[7] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.AssetGuid;

            arParams[8] = new NpgsqlParameter("porcupineTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.PorcupineManager.PorcupineTypeManager.PorcupineTypeGuid;

            arParams[9] = new NpgsqlParameter("porcupineMaterialTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = assetManager.PorcupineManager.PorcupineMaterialTypeManager.PorcupineMaterialTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdatePorcupineData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_porcupine ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("screen_length= :screenLength, ");
            sqlCommand.Append("spacing_along_screen= :spacingAlongScreen, ");
            sqlCommand.Append("screen_rows= :screenRows, ");
            sqlCommand.Append("number_of_layers= :numberOfLayers, ");
            sqlCommand.Append("member_length= :memberLength, ");
            sqlCommand.Append("length= :lengthPorcupine, ");
            sqlCommand.Append("porcupine_type_uuid= :porcupineTypeGuid, ");
            sqlCommand.Append("material_type_uuid= :porcupineMaterialTypeGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[10];

            arParams[0] = new NpgsqlParameter("porcupineGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.PorcupineManager.PorcupineGuid;

            arParams[1] = new NpgsqlParameter("screenLength", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.PorcupineManager.ScreenLength;

            arParams[2] = new NpgsqlParameter("spacingAlongScreen", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.PorcupineManager.SpacingAlongScreen;

            arParams[3] = new NpgsqlParameter("screenRows", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.PorcupineManager.ScreenRows;

            arParams[4] = new NpgsqlParameter("numberOfLayers", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.PorcupineManager.NumberOfLayers;

            arParams[5] = new NpgsqlParameter("memberLength", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.PorcupineManager.MemberLength;

            arParams[6] = new NpgsqlParameter("lengthPorcupine", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.PorcupineManager.LengthPorcupine;

            arParams[7] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.AssetGuid;

            arParams[8] = new NpgsqlParameter("porcupineTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.PorcupineManager.PorcupineTypeManager.PorcupineTypeGuid;

            arParams[9] = new NpgsqlParameter("porcupineMaterialTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = assetManager.PorcupineManager.PorcupineMaterialTypeManager.PorcupineMaterialTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}