﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class ProjectAndPmcSiteEngineersRelationManager
    {
        private Guid pmcSiteEngineerProjectRelationshipGuid;
        private ProjectManager projectManager;
        private string siteEngineerName;
        private bool siteEngineerArose;

        public Guid PmcSiteEngineerProjectRelationshipGuid
        {
            get
            {
                return pmcSiteEngineerProjectRelationshipGuid;
            }
            set
            {
                pmcSiteEngineerProjectRelationshipGuid = value;
            }
        }

        public ProjectManager ProjectManager
        {
            get
            {
                if (projectManager == null)
                {
                    projectManager = new ProjectManager();
                }
                return projectManager;
            }
            set
            {
                projectManager = value;
            }
        }

        public string SiteEngineerName
        {
            get
            {
                return siteEngineerName;
            }
            set
            {
                siteEngineerName = value;
            }
        }

        public bool SiteEngineerArose
        {
            get
            {
                return siteEngineerArose;
            }
            set
            {
                siteEngineerArose = value;
            }
        }
    }
}