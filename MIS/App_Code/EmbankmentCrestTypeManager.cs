﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class EmbankmentCrestTypeManager
    {
        #region Private Members

        private Guid crestTypeGuid;
        private string crestTypeName;
       
        #endregion       

        #region Public Getter/Setter Properties

        public Guid CrestTypeGuid
        {
            get
            {
                return crestTypeGuid;
            }
            set
            {
                crestTypeGuid = value;
            }
        }

        public string CrestTypeName
        {
            get
            {
                return crestTypeName;
            }
            set
            {
                crestTypeName = value;
            }
        }        

        #endregion
    }
}