﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class TurnoutDB
    {
        protected internal int AddTurnOutData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_turnout ");
            sqlCommand.Append("( ");
            sqlCommand.Append("turnout_uuid, ");
            sqlCommand.Append("inletbox_width, ");
            sqlCommand.Append("inletbox_length, ");
            sqlCommand.Append("outlet_no, ");
            sqlCommand.Append("outlet_height, ");
            sqlCommand.Append("outlet_width, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("turnout_type_uuid, ");
            sqlCommand.Append("turnout_gate_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":turnoutGuid, ");
            sqlCommand.Append(":inletboxWidth, ");
            sqlCommand.Append(":inletboxLength, ");
            sqlCommand.Append(":outletNumber, ");
            sqlCommand.Append(":outletHeight, ");
            sqlCommand.Append(":outletWidth, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":turnoutTypeGuid, ");
            sqlCommand.Append(":turnoutGateTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[9];

            arParams[0] = new NpgsqlParameter("turnoutGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("inletboxWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.TurnoutManager.InletboxWidth;

            arParams[2] = new NpgsqlParameter("inletboxLength", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.TurnoutManager.InletboxLength;

            arParams[3] = new NpgsqlParameter("outletNumber", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.TurnoutManager.OutletNumber;

            arParams[4] = new NpgsqlParameter("outletHeight", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.TurnoutManager.OutletHeight;

            arParams[5] = new NpgsqlParameter("outletWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.TurnoutManager.OutletWidth;

            arParams[6] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.AssetGuid;

            arParams[7] = new NpgsqlParameter("turnoutTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.TurnoutManager.TurnoutTypeManager.TurnoutTypeGuid;

            arParams[8] = new NpgsqlParameter("turnoutGateTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.TurnoutManager.TurnoutGateTypeManager.TurnoutGateTypeGuid;


            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            if (result == 1)
            {
                new GateDB().AddGateForTurnout(assetManager);
            }

            return result;
        }

        protected internal int UpdateTurnoutData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_turnout ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("inletbox_width= :inletboxWidth, ");
            sqlCommand.Append("inletbox_length= :inletboxLength, ");
            sqlCommand.Append("outlet_no= :outletNumber, ");
            sqlCommand.Append("outlet_height= :outletHeight, ");
            sqlCommand.Append("outlet_width= :outletWidth, ");
            sqlCommand.Append("turnout_type_uuid= :turnoutTypeGuid, ");
            sqlCommand.Append("turnout_gate_type_uuid= :turnoutGateTypeGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[9];

            arParams[0] = new NpgsqlParameter("turnoutGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.TurnoutManager.TurnoutGuid;

            arParams[1] = new NpgsqlParameter("inletboxWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.TurnoutManager.InletboxWidth;

            arParams[2] = new NpgsqlParameter("inletboxLength", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.TurnoutManager.InletboxLength;

            arParams[3] = new NpgsqlParameter("outletNumber", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.TurnoutManager.OutletNumber;

            arParams[4] = new NpgsqlParameter("outletHeight", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.TurnoutManager.OutletHeight;

            arParams[5] = new NpgsqlParameter("outletWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.TurnoutManager.OutletWidth;

            arParams[6] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.AssetGuid;

            arParams[7] = new NpgsqlParameter("turnoutTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.TurnoutManager.TurnoutTypeManager.TurnoutTypeGuid;

            arParams[8] = new NpgsqlParameter("turnoutGateTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.TurnoutManager.TurnoutGateTypeManager.TurnoutGateTypeGuid;


            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            if (result == 1)
            {               
                new GateDB().UpdateGateDataForTurnout(assetManager);                               
            }

            return result;
        }
    }
}
