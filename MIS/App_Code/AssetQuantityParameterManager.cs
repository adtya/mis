﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class AssetQuantityParameterManager
    {
        #region Private Members

        private Guid assetQuantityParameterGuid;
        private string assetQuantityParameterName;
        private UnitManager unitManager;

        #endregion


        #region Public Getter/Setter Properties

        public Guid AssetQuantityParameterGuid
        {
            get
            {
                return assetQuantityParameterGuid;
            }
            set
            {
                assetQuantityParameterGuid = value;
            }
        }

        public string AssetQuantityParameterName
        {
            get
            {
                return assetQuantityParameterName;
            }
            set
            {
                assetQuantityParameterName = value;
            }
        }

        public UnitManager UnitManager
        {
            get
            {
                if (unitManager == null)
                {
                    unitManager = new UnitManager();
                }
                return unitManager;
            }
            set
            {
                unitManager = value;
            }
        }

        #endregion
    }
}