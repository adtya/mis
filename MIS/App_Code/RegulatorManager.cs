﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RegulatorManager
    {
        #region Private Members

        private Guid regulatorGuid;
        private int ventNumberRegulator;
        private double ventHeightRegulator;
        private double ventWidthRegulator;        
        private RegulatorTypeManager regulatorTypeManager;
        private RegulatorGateTypeManager regulatorGateTypeManager;
        private List<GateManager> gateManagerList;

        #endregion


        #region Public Constructors

        public RegulatorManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid RegulatorGuid
        {
            get
            {
                return regulatorGuid;
            }
            set
            {
                regulatorGuid = value;
            }
        }       
               
        public int VentNumberRegulator
        {
            get
            {
                return ventNumberRegulator;
            }
            set
            {
                ventNumberRegulator = value;
            }
        }

        public double VentHeightRegulator
        {
            get
            {
                return ventHeightRegulator;
            }
            set
            {
                ventHeightRegulator = value;
            }
        }

        public double VentWidthRegulator
        {
            get
            {
                return ventWidthRegulator;
            }
            set
            {
                ventWidthRegulator = value;
            }
        }

        public RegulatorTypeManager RegulatorTypeManager
        {
            get
            {
                if (regulatorTypeManager == null)
                {
                    regulatorTypeManager = new RegulatorTypeManager();
                }
                return regulatorTypeManager;
            }
            set
            {
                regulatorTypeManager = value;
            }
        }

        public RegulatorGateTypeManager RegulatorGateTypeManager
        {
            get
            {
                if (regulatorGateTypeManager == null)
                {
                    regulatorGateTypeManager = new RegulatorGateTypeManager();
                }
                return regulatorGateTypeManager;
            }
            set
            {
                regulatorGateTypeManager = value;
            }
        }

        public List<GateManager> GateManagerList
        {
            get
            {
                return gateManagerList;
            }
            set
            {
                gateManagerList = value;
            }
        }

        #endregion
    }
}