﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class CulvertDB
    {
        protected internal int AddCulvertData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_culvert ");
            sqlCommand.Append("( ");
            sqlCommand.Append("culvert_uuid, ");
            sqlCommand.Append("vent_no, ");
            sqlCommand.Append("vent_height, ");
            sqlCommand.Append("vent_width, ");
            sqlCommand.Append("length, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("culvert_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":culvertGuid, ");
            sqlCommand.Append(":ventNumber, ");
            sqlCommand.Append(":ventHeight, ");
            sqlCommand.Append(":ventWidth, ");
            sqlCommand.Append(":lengthCulvert, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":culvertTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[7];

            arParams[0] = new NpgsqlParameter("culvertGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("ventNumber", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.CulvertManager.VentNumber;

            arParams[2] = new NpgsqlParameter("ventHeight", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.CulvertManager.VentHeight;

            arParams[3] = new NpgsqlParameter("ventWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.CulvertManager.VentWidth;

            arParams[4] = new NpgsqlParameter("lengthCulvert", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.CulvertManager.LengthCulvert;

            arParams[5] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.AssetGuid;

            arParams[6] = new NpgsqlParameter("culvertTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.CulvertManager.CulvertTypeManager.CulvertTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateCulvertData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_culvert ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("vent_no= :ventNumber, ");
            sqlCommand.Append("vent_height= :ventHeight, ");
            sqlCommand.Append("vent_width= :ventWidth, ");
            sqlCommand.Append("length= :lengthCulvert, ");
            sqlCommand.Append("culvert_type_uuid= :culvertTypeGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[7];

            arParams[0] = new NpgsqlParameter("culvertGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.CulvertManager.CulvertGuid;

            arParams[1] = new NpgsqlParameter("ventNumber", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.CulvertManager.VentNumber;

            arParams[2] = new NpgsqlParameter("ventHeight", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.CulvertManager.VentHeight;

            arParams[3] = new NpgsqlParameter("ventWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.CulvertManager.VentWidth;

            arParams[4] = new NpgsqlParameter("lengthCulvert", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.CulvertManager.LengthCulvert;

            arParams[5] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.AssetGuid;

            arParams[6] = new NpgsqlParameter("culvertTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.CulvertManager.CulvertTypeManager.CulvertTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}