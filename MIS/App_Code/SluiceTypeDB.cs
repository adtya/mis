﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SluiceTypeDB
    {
        protected internal List<SluiceTypeManager> GetSluiceTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("sluice_type_uuid, ");
            sqlCommand.Append("sluice_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_sluice_type ");
            sqlCommand.Append("ORDER BY sluice_type_name ");
            sqlCommand.Append(";");

            List<SluiceTypeManager> sluiceTypeManagerList = new List<SluiceTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    SluiceTypeManager sluiceTypeManagerObj = new SluiceTypeManager();

                    sluiceTypeManagerObj.SluiceTypeGuid = new Guid(reader["sluice_type_uuid"].ToString());
                    sluiceTypeManagerObj.SluiceTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["sluice_type_name"].ToString());

                    sluiceTypeManagerList.Add(sluiceTypeManagerObj);
                }
            }

            return sluiceTypeManagerList;
        }
    }
}