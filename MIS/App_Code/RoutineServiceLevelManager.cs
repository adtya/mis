﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineServiceLevelManager
    {
        #region Private Members

        private Guid routineServiceLevelGuid;
        private int routineServiceLevel;
        private string routineServiceLevelCode;
        private AssetTypeManager assetTypeManager;
        private RoutineWorkItemManager routineWorkItemManager;

        #endregion


        #region Public Getter/Setter Properties

        public Guid RoutineServiceLevelGuid
        {
            get
            {
                return routineServiceLevelGuid;
            }
            set
            {
                routineServiceLevelGuid = value;
            }
        }

        public int RoutineServiceLevel
        {
            get
            {
                return routineServiceLevel;
            }
            set
            {
                routineServiceLevel = value;
            }
        }

        public string RoutineServiceLevelCode
        {
            get
            {
                return routineServiceLevelCode;
            }
            set
            {
                routineServiceLevelCode = value;
            }
        }

        public AssetTypeManager AssetTypeManager
        {
            get
            {
                if (assetTypeManager == null)
                {
                    assetTypeManager = new AssetTypeManager();
                }
                return assetTypeManager;
            }
            set
            {
                assetTypeManager = value;
            }
        }

        public RoutineWorkItemManager RoutineWorkItemManager
        {
            get
            {
                if (routineWorkItemManager == null)
                {
                    routineWorkItemManager = new RoutineWorkItemManager();
                }
                return routineWorkItemManager;
            }
            set
            {
                routineWorkItemManager = value;
            }
        }

        #endregion
    }
}