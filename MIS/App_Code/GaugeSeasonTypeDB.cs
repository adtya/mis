﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class GaugeSeasonTypeDB
    {
        protected internal List<GaugeSeasonTypeManager> GetGaugeSeasonTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("season_type_uuid, ");
            sqlCommand.Append("season_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_gauge_season_type ");
            sqlCommand.Append("ORDER BY season_type_name ");
            sqlCommand.Append(";");

            List<GaugeSeasonTypeManager> gaugeSeasonTypeManagerList = new List<GaugeSeasonTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    GaugeSeasonTypeManager gaugeSeasonTypeManagerObj = new GaugeSeasonTypeManager();

                    gaugeSeasonTypeManagerObj.SeasonTypeGuid = new Guid(reader["season_type_uuid"].ToString());
                    gaugeSeasonTypeManagerObj.SeasonTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["season_type_name"].ToString());

                    gaugeSeasonTypeManagerList.Add(gaugeSeasonTypeManagerObj);
                }
            }

            return gaugeSeasonTypeManagerList;
        }
    }
}