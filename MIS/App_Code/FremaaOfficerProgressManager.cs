﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class FremaaOfficerProgressManager
    {
        private Guid fremaaOfficerProgressGuid;
        private bool fremaaOfficerArose;
        private ProjectAndFremaaOfficersRelationManager projectAndFremaaOfficersRelationManager;


        public Guid FremaaOfficerProgressGuid
        {
            get 
            {
                return fremaaOfficerProgressGuid;
            }
            set 
            {
                fremaaOfficerProgressGuid = value;
            }
        }

        public bool FremaaOfficerArose
        {
            get 
            { 
                return fremaaOfficerArose;
            }
            set
            {
                fremaaOfficerArose = value;
            }
        }

        public ProjectAndFremaaOfficersRelationManager ProjectAndFremaaOfficersRelationManager
        {
            get 
            {
                if (projectAndFremaaOfficersRelationManager == null)
                {
                    projectAndFremaaOfficersRelationManager = new ProjectAndFremaaOfficersRelationManager();
                }
                return projectAndFremaaOfficersRelationManager; 
            }
            set 
            {
                projectAndFremaaOfficersRelationManager = value; 
            }
        }

    }
}