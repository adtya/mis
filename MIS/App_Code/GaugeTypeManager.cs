﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class GaugeTypeManager
    {
         #region Private Members

        private Guid gaugeTypeGuid;
        private string gaugeTypeName;
       
       #endregion


        #region Public Constructors

        public GaugeTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid GaugeTypeGuid
        {
            get
            {
                return gaugeTypeGuid;
            }
            set
            {
                gaugeTypeGuid = value;
            }
        }

        public string GaugeTypeName
        {
            get
            {
                return gaugeTypeName;
            }
            set
            {
                gaugeTypeName = value;
            }
        }        

        #endregion
    }
}