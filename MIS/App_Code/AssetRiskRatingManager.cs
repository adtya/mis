﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class AssetRiskRatingManager
    {
        #region Private Members

        private Guid assetRiskRatingGuid;
        private string assetRiskRating;
        private int assetRiskRatingNumber;
                
        #endregion

        #region Public Getter/Setter Properties

        public Guid AssetRiskRatingGuid
        {
            get
            {
                return assetRiskRatingGuid;
            }
            set
            {
                assetRiskRatingGuid = value;
            }
        }

        public string AssetRiskRating
        {
            get
            {
                return assetRiskRating;
            }
            set
            {
                assetRiskRating = value;
            }
        }

        public int AssetRiskRatingNumber
        {
            get
            {
                return assetRiskRatingNumber;
            }
            set
            {
                assetRiskRatingNumber = value;
            }
        }
     
        #endregion
    }
}