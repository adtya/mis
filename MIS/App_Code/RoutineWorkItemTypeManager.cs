﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineWorkItemTypeManager
    {
        #region Private Members

        private Guid routineWorkItemTypeGuid;
        private string routineWorkItemTypeName;
        private string routineWorkItemTypeFullName;
        private int routineWorkItemTypeNumber;
        private RoutineWorkItemClassManager routineWorkItemClassManager;
        //private RoutineWorkItemManager routineWorkItemManager;

        #endregion


        #region Public Getter/Setter Properties

        public Guid RoutineWorkItemTypeGuid
        {
            get
            {
                return routineWorkItemTypeGuid;
            }
            set
            {
                routineWorkItemTypeGuid = value;
            }
        }

        public string RoutineWorkItemTypeName
        {
            get
            {
                return routineWorkItemTypeName;
            }
            set
            {
                routineWorkItemTypeName = value;
            }
        }

        public int RoutineWorkItemTypeNumber
        {
            get
            {
                return routineWorkItemTypeNumber;
            }
            set
            {
                routineWorkItemTypeNumber = value;
            }
        }

        public string RoutineWorkItemTypeFullName
        {
            get
            {
                return routineWorkItemTypeFullName;
            }
            set
            {
                routineWorkItemTypeFullName = value;
            }
        }

        public RoutineWorkItemClassManager RoutineWorkItemClassManager
        {
            get
            {
                if (routineWorkItemClassManager == null)
                {
                    routineWorkItemClassManager = new RoutineWorkItemClassManager();
                }
                return routineWorkItemClassManager;
            }
            set
            {
                routineWorkItemClassManager = value;
            }
        }

        //public RoutineWorkItemManager RoutineWorkItemManager
        //{
        //    get
        //    {
        //        if (routineWorkItemManager == null)
        //        {
        //            routineWorkItemManager = new RoutineWorkItemManager();
        //        }
        //        return routineWorkItemManager;
        //    }
        //    set
        //    {
        //        routineWorkItemManager = value;
        //    }
        //}

        #endregion
    
    }
}