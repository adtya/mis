﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class MonitoringItemPerformanceManager
    {
        #region Private Members

        private Guid monitoringItemPerformanceGuid;       
        private string monitoringItemPerformanceDescription;    
        private MonitoringItemManager monitoringItemManager;
        private OverallStatusManager overallStatusManager;
        private SystemVariablesManager systemVariablesManager;
        private PerformanceMonitoringManager performanceMonitoringManager;    
              
       #endregion


        #region Public Constructors

        public MonitoringItemPerformanceManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid MonitoringItemPerformanceGuid
        {
            get
            {
                return monitoringItemPerformanceGuid;
            }
            set
            {
                monitoringItemPerformanceGuid = value;
            }
        }      

        public string MonitoringItemPerformanceDescription
        {
            get
            {
                return monitoringItemPerformanceDescription;
            }
            set
            {
                monitoringItemPerformanceDescription = value;
            }
        }

        public MonitoringItemManager MonitoringItemManager
        {
            get
            {
                if (monitoringItemManager == null)
                {
                    monitoringItemManager = new MonitoringItemManager();
                }
                return monitoringItemManager;
            }
            set
            {
                monitoringItemManager = value;
            }
        }

        public OverallStatusManager OverallStatusManager
        {
            get
            {
                if (overallStatusManager == null)
                {
                    overallStatusManager = new OverallStatusManager();
                }
                return overallStatusManager;
            }
            set
            {
                overallStatusManager = value;
            }
        }

        public SystemVariablesManager SystemVariablesManager
        {
            get
            {
                if (systemVariablesManager == null)
                {
                    systemVariablesManager = new SystemVariablesManager();
                }
                return systemVariablesManager;
            }
            set
            {
                systemVariablesManager = value;
            }
        }

        public PerformanceMonitoringManager PerformanceMonitoringManager
        {
            get
            {
                if (performanceMonitoringManager == null)
                {
                    performanceMonitoringManager = new PerformanceMonitoringManager();
                }
                return performanceMonitoringManager;
            }
            set
            {
                performanceMonitoringManager = value;
            }
        }
        
        #endregion
    }
    
}