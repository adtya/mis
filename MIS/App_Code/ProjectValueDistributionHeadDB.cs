﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class ProjectValueDistributionHeadDB
    {
        protected internal bool SaveHeadBudget(List<ProjectValueDistributionHeadManager> headBudgetList)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_financial_heads ");
            sqlCommand.Append("( ");
            sqlCommand.Append("head_uuid, ");
            sqlCommand.Append("head_name, ");
            sqlCommand.Append("head_budget, ");
            sqlCommand.Append("project_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            foreach (ProjectValueDistributionHeadManager headObj in headBudgetList)
            {
                int index = headBudgetList.IndexOf(headObj);

                sqlCommand.Append("( ");

                sqlCommand.Append("'" + headObj.HeadGuid + "', ");
                sqlCommand.Append("'" + headObj.HeadName + "', ");
                sqlCommand.Append("'" + headObj.HeadBudget.Amount + "', ");
                sqlCommand.Append("'" + headObj.ProjectManager.ProjectGuid + "' ");

                if (index == headBudgetList.Count - 1)
                {
                    sqlCommand.Append(") ");
                }
                else
                {
                    sqlCommand.Append("), ");
                }
            }

            sqlCommand.Append(";");

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());

            return (result > 0);
        }

        protected internal List<ProjectValueDistributionHeadManager> GetHeadBudgets(Guid projectGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("head_budget ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_value_distribution ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = projectGuid;

            List<ProjectValueDistributionHeadManager> headList = new List<ProjectValueDistributionHeadManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    ProjectValueDistributionHeadManager headObj = new ProjectValueDistributionHeadManager();

                    //headObj.HeadBudget = Convert.ToInt64(reader["head_budget"].ToString());

                    headList.Add(headObj);
                }
            }

            return headList;
        }
    }
}