﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class FinancialSubHeadManager
    {
        private Guid financialSubHeadGuid;
        private string financialSubHeadName;

        public Guid FinancialSubHeadGuid
        {
            get
            {
                return financialSubHeadGuid;
            }
            set
            {
                financialSubHeadGuid = value;
            }
        }

        public string FinancialSubHeadName
        {
            get
            {
                return financialSubHeadName;
            }
            set
            {
                financialSubHeadName = value;
            }
        }
    }
}