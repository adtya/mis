﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class TurnoutManager
    {
        #region Private Members

        private Guid turnoutGuid;                 
        private double inletboxLength; 
        private double inletboxWidth;
        private int outletNumber;
        private double outletHeight;
        private double outletWidth; 
        private TurnoutTypeManager turnoutTypeManager;
        private TurnoutGateTypeManager turnoutGateTypeManager;
        private List<GateManager> gateManagerList;
        #endregion


        #region Public Constructors

        public TurnoutManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid TurnoutGuid
        {
            get
            {
                return turnoutGuid;
            }
            set
            {
                turnoutGuid = value;
            }
        }        

        public double InletboxLength
        {
            get
            {
                return inletboxLength;
            }
            set
            {
                inletboxLength = value;
            }
        }

        public double InletboxWidth
        {
            get
            {
                return inletboxWidth;
            }
            set
            {
                inletboxWidth = value;
            }
        }

        public int OutletNumber
        {
            get
            {
                return outletNumber;
            }
            set
            {
                outletNumber = value;
            }
        }

        public double OutletHeight
        {
            get
            {
                return outletHeight;
            }
            set
            {
                outletHeight = value;
            }
        }

        public double OutletWidth
        {
            get
            {
                return outletWidth;
            }
            set
            {
                outletWidth = value;
            }
        }

        public TurnoutTypeManager TurnoutTypeManager
        {
            get
            {
                if (turnoutTypeManager == null)
                {
                    turnoutTypeManager = new TurnoutTypeManager();
                }
                return turnoutTypeManager;
            }
            set
            {
                turnoutTypeManager = value;
            }
        }

        public TurnoutGateTypeManager TurnoutGateTypeManager
        {
            get
            {
                if (turnoutGateTypeManager == null)
                {
                    turnoutGateTypeManager = new TurnoutGateTypeManager();
                }
                return turnoutGateTypeManager;
            }
            set
            {
                turnoutGateTypeManager = value;
            }
        }

        public List<GateManager> GateManagerList
        {
            get
            {
                return gateManagerList;
            }
            set
            {
                gateManagerList = value;
            }
        }

        #endregion
    }
}