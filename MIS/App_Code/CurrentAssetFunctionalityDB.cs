﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class CurrentAssetFunctionalityDB
    {
        protected internal List<CurrentAssetFunctionalityManager> GetCurrentAssetFunctionality()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_repair_analysis_current_asset_functionality ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("current_asset_functionality_number ");
            sqlCommand.Append(";");

            List<CurrentAssetFunctionalityManager> currentAssetFunctionalityManagerList = new List<CurrentAssetFunctionalityManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    CurrentAssetFunctionalityManager currentAssetFunctionalityManagerObj = new CurrentAssetFunctionalityManager();

                    currentAssetFunctionalityManagerObj.CurrentAssetFunctionalityGuid = new Guid(reader["current_asset_functionality_uuid"].ToString());
                    currentAssetFunctionalityManagerObj.CurrentAssetFunctionality = reader["current_asset_functionality"].ToString();
                    currentAssetFunctionalityManagerObj.CurrentAssetFunctionalityNumber = Convert.ToInt16(reader["current_asset_functionality_number"].ToString());

                    currentAssetFunctionalityManagerList.Add(currentAssetFunctionalityManagerObj);
                }
            }

            return currentAssetFunctionalityManagerList;
        }
    }
}