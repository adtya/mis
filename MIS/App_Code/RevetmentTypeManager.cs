﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RevetmentTypeManager
    {
         #region Private Members

        private Guid revetTypeGuid;
        private string revetTypeName;
       
       #endregion


        #region Public Constructors

        public RevetmentTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid RevetTypeGuid
        {
            get
            {
                return revetTypeGuid;
            }
            set
            {
                revetTypeGuid = value;
            }
        }

        public string RevetTypeName
        {
            get
            {
                return revetTypeName;
            }
            set
            {
                revetTypeName = value;
            }
        }        

        #endregion
    }
}