﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RegulatoreDB
    {
        protected internal int AddRegulatorData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_regulator ");
            sqlCommand.Append("( ");
            sqlCommand.Append("regulator_uuid, ");
            sqlCommand.Append("vent_number, ");       
            sqlCommand.Append("vent_height, ");
            sqlCommand.Append("vent_width, ");            
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("regulator_type_uuid, ");
            sqlCommand.Append("regulator_gate_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":regulatorGuid, ");
            sqlCommand.Append(":ventNumberRegulator, ");
            sqlCommand.Append(":ventHeightRegulator, ");            
            sqlCommand.Append(":ventWidthRegulator, ");            
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":regulatorTypeGuid, ");
            sqlCommand.Append(":regulatorGateTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[7];

            arParams[0] = new NpgsqlParameter("regulatorGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("ventNumberRegulator", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.RegulatorManager.VentNumberRegulator;

            arParams[2] = new NpgsqlParameter("ventHeightRegulator", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.RegulatorManager.VentHeightRegulator;

            arParams[3] = new NpgsqlParameter("ventWidthRegulator", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.RegulatorManager.VentWidthRegulator;

            arParams[4] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.AssetGuid;

            arParams[5] = new NpgsqlParameter("regulatorTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.RegulatorManager.RegulatorTypeManager.RegulatorTypeGuid;

            arParams[6] = new NpgsqlParameter("regulatorGateTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.RegulatorManager.RegulatorGateTypeManager.RegulatorGateTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            if (result == 1)
            {
                new GateDB().AddGateForRegulator(assetManager);
            } 

            return result;
        }

        protected internal int UpdateRegulatorData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_regulator ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("vent_number= :ventNumberRegulator, ");
            sqlCommand.Append("vent_height= :ventHeightRegulator, ");
            sqlCommand.Append("vent_width= :ventWidthRegulator, ");
            sqlCommand.Append("regulator_type_uuid= :regulatorTypeGuid, ");
            sqlCommand.Append("regulator_gate_type_uuid= :regulatorGateTypeGuid ");          
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[7];

            arParams[0] = new NpgsqlParameter("regulatorGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.RegulatorManager.RegulatorGuid;

            arParams[1] = new NpgsqlParameter("ventNumberRegulator", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.RegulatorManager.VentNumberRegulator;

            arParams[2] = new NpgsqlParameter("ventHeightRegulator", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.RegulatorManager.VentHeightRegulator;

            arParams[3] = new NpgsqlParameter("ventWidthRegulator", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.RegulatorManager.VentWidthRegulator;

            arParams[4] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.AssetGuid;

            arParams[5] = new NpgsqlParameter("regulatorTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.RegulatorManager.RegulatorTypeManager.RegulatorTypeGuid;

            arParams[6] = new NpgsqlParameter("regulatorGateTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.RegulatorManager.RegulatorGateTypeManager.RegulatorGateTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            if (result == 1)
            {
                new GateDB().UpdateGateDataForRegulator(assetManager);
            }
            return result;
        }   
    }
}