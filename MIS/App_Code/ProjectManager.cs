﻿using NodaMoney;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class ProjectManager
    {

        #region Data Members

        private Guid projectGuid;
        private DistrictManager districtManager;
        private DivisionManager divisionManager;
        private string projectName;
        private string subProjectName;
        private string projectChainage;
        private DateTime projectStartDate;
        private DateTime projectExpectedCompletionDate;
        private DateTime projectActualCompletionDate;
        private Money projectValue;
        private decimal projectValueInput;
        private List<ProjectAndFremaaOfficersRelationManager> officersEngaged;
        private List<ProjectAndPmcSiteEngineersRelationManager> pmcSiteEngineers;
        private short projectStatus;
        private List<SubChainageManager> subChainageManagerList;
        private List<ProjectValueDistributionManager> projectValueDistributionManagerList;
        private bool landAcquisitionIncluded;
        private DateTime projectCreationDate;
        private short projectSavedPartiallyStep;

        #endregion




        #region Getters/Setters

        public Guid ProjectGuid
        {
            get
            {
                return projectGuid;
            }
            set
            {
                projectGuid = value;
            }
        }

        public DistrictManager DistrictManager
        {
            get
            {
                if (districtManager == null)
                {
                    districtManager = new DistrictManager();
                }
                return districtManager;
            }
            set
            {
                districtManager = value;
            }
        }

        public DivisionManager DivisionManager
        {
            get
            {
                if (divisionManager == null)
                {
                    divisionManager = new DivisionManager();
                }
                return divisionManager;
            }
            set
            {
                divisionManager = value;
            }
        }

        public string ProjectName
        {
            get 
            {
                return projectName; 
            }
            set
            {
                projectName = value;
            }
        }

        public string SubProjectName
        {
            get
            {
                return subProjectName;
            }
            set
            {
                subProjectName = value;
            }
        }

        public string ProjectChainage
        {
            get 
            { 
                return projectChainage; 
            }
            set
            {
                projectChainage = value;
            }
        }

        public DateTime ProjectStartDate
        {
            get
            {
                return projectStartDate;
            }
            set
            {
                projectStartDate = value;
            }
        }

        public DateTime ProjectExpectedCompletionDate
        {
            get
            {
                return projectExpectedCompletionDate;
            }
            set
            {
                projectExpectedCompletionDate = value;
            }
        }

        public DateTime ProjectActualCompletionDate
        {
            get
            {
                return projectActualCompletionDate;
            }
            set
            {
                projectActualCompletionDate = value;
            }
        }

        public Money ProjectValue
        {
            get
            {
                if (projectValue.Amount.Equals(0))
                {
                    projectValue = ProjectValueInput;
                }
                return projectValue;
            }
            set
            {
                projectValue = value;
            }
        }

        public decimal ProjectValueInput
        {
            private get
            {
                return projectValueInput;
            }
            set
            {
                projectValueInput = value;
            }
        }

        public List<ProjectAndFremaaOfficersRelationManager> OfficersEngaged
        {
            get
            {
                return officersEngaged;
            }
            set
            {
                officersEngaged = value;
            }
        }

        public List<ProjectAndPmcSiteEngineersRelationManager> PmcSiteEngineers
        {
            get
            {
                return pmcSiteEngineers;
            }
            set
            {
                pmcSiteEngineers = value;
            }
        }

        public short ProjectStatus
        {
            get
            {
                return projectStatus;
            }
            set
            {
                projectStatus = value;
            }
        }

        public List<SubChainageManager> SubChainageManagerList
        {
            get
            {
                return subChainageManagerList;
            }
            set
            {
                subChainageManagerList = value;
            }
        }

        public List<ProjectValueDistributionManager> ProjectValueDistributionManagerList
        {
            get
            {
                return projectValueDistributionManagerList;
            }
            set
            {
                projectValueDistributionManagerList = value;
            }
        }

        public bool LandAcquisitionIncluded
        {
            get
            {
                return landAcquisitionIncluded;
            }
            set
            {
                landAcquisitionIncluded = value;
            }
        }

        public DateTime ProjectCreationDate
        {
            get
            {
                return projectCreationDate;
            }
            set
            {
                projectCreationDate = value;
            }
        }

        public short ProjectSavedPartiallyStep
        {
            get
            {
                return projectSavedPartiallyStep;
            }
            set
            {
                projectSavedPartiallyStep = value;
            }
        }

        #endregion

    }
}