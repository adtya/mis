﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class UserDB
    {
        protected internal int AddUser(Guid userGuid, string userFirstName, string userMiddleName, string userLastName, string userEmailId, Guid userRoleGuid, string userPassword)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_users ");
            sqlCommand.Append("( ");
            sqlCommand.Append("user_uuid, ");
            sqlCommand.Append("first_name, ");
            sqlCommand.Append("middle_name, ");
            sqlCommand.Append("last_name, ");
            sqlCommand.Append("email, ");
            sqlCommand.Append("role_uuid, ");
            sqlCommand.Append("password ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":userGuid, ");
            sqlCommand.Append(":userFirstName, ");
            sqlCommand.Append(":userMiddleName, ");
            sqlCommand.Append(":userLastName, ");
            sqlCommand.Append(":userEmailId, ");
            sqlCommand.Append(":userRoleGuid, ");
            sqlCommand.Append(":userPassword ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[7];

            arParams[0] = new NpgsqlParameter("userGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = userGuid;

            arParams[1] = new NpgsqlParameter("userFirstName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = userFirstName;

            arParams[2] = new NpgsqlParameter("userMiddleName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = userMiddleName;

            arParams[3] = new NpgsqlParameter("userLastName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = userLastName;

            arParams[4] = new NpgsqlParameter("userEmailId", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = userEmailId;

            arParams[5] = new NpgsqlParameter("userRoleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = userRoleGuid;

            arParams[6] = new NpgsqlParameter("userPassword", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = userPassword;           

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;       
        }

        protected internal bool CheckUserExistence(string userEmailId)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.email ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_users obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.email = :userEmailId ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("userEmailId", NpgsqlTypes.NpgsqlDbType.Text);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = userEmailId;

            bool userExist = false;
            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    userExist = true;               
                }
            }
            return userExist;
        }    

        protected internal List<UserManager> GetSioUsers()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_users obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_user_roles obj2 ON obj1.role_uuid = obj2.role_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj2.role_name = :roleName ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("roleName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = "SIO";

            List<UserManager> userList = new List<UserManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    UserManager userObj = new UserManager();

                    userObj.UserGuid = new Guid(reader["user_uuid"].ToString());

                    //userObj.EmailId = reader["email"].ToString();

                    //userObj.DivisionManager.DivisionGuid = new Guid(reader["division_id"].ToString());
                    //userObj.DivisionManager.DivisionName = reader["division_name"].ToString();

                    //userObj.DesignationManager.DesignationGuid = new Guid(reader["designation_id"].ToString());
                    //userObj.DesignationManager.DesignationName = reader["designation_name"].ToString();

                    //userObj.DepartmentManager.DepartmentGuid = new Guid(reader["department_id"].ToString());
                    //userObj.DepartmentManager.DepartmentName = reader["department_name"].ToString();

                    //userObj.DateOfJoining = DateTime.Parse(reader["date_of_joining"].ToString());

                    //userObj.Title = Convert.ToInt16(reader["title"].ToString());
                    userObj.FirstName = reader["first_name"].ToString();
                    userObj.MiddleName = reader["middle_name"].ToString();
                    userObj.LastName = reader["last_name"].ToString();

                    //userObj.Gender = Convert.ToInt16(reader["gender"].ToString());

                    //userObj.DateOfBirth = DateTime.Parse(reader["date_of_birth"].ToString());

                    //userObj.MobileNumber = reader["mobile_number"].ToString();

                    //userObj.AddressManager.AddressGuid = new Guid(reader["address_id"].ToString());
                    //userObj.AddressManager.Address1 = reader["address1"].ToString();
                    //userObj.AddressManager.Address2 = reader["address2"].ToString();
                    //userObj.AddressManager.State = reader["state"].ToString();
                    //userObj.AddressManager.Country = reader["country"].ToString();
                    //userObj.AddressManager.PinCode = reader["pin"].ToString();

                    //userObj.UserType = Convert.ToInt16(reader["user_type"].ToString());

                    //userObj.Password = reader["password"].ToString();

                    //userObj.SuperUserGuid = new Guid(reader["super_user_id"].ToString());
                    //userObj.AdminGuid = new Guid(reader["admin_id"].ToString());
                    //userObj.SuperAdminGuid = new Guid(reader["super_admin_id"].ToString());

                    //userObj.SuperUserManager.SuperUserGuid = new Guid(reader["super_user_id"].ToString());

                    //userObj.AdminManager.AdminGuid = new Guid(reader["admin_id"].ToString());

                    //userObj.SuperAdminManager.SuperAdminGuid = new Guid(reader["super_admin_id"].ToString());

                    //userObj.LocalAccessDate = DateTime.Parse(reader["local_access_date"].ToString());
                    //userObj.LocalAccessCount = Convert.ToInt16(reader["local_count"].ToString());

                    //userObj.NationalAccessDate = DateTime.Parse(reader["national_access_date"].ToString());
                    //userObj.NationalAccessCount = Convert.ToInt16(reader["national_count"].ToString());

                    //userObj.InternationalAccessDate = DateTime.Parse(reader["international_access_date"].ToString());
                    //userObj.InternationalAccessCount = Convert.ToInt16(reader["international_count"].ToString());

                    //userObj.UserDisabled = Convert.ToBoolean(reader["user_disabled"].ToString());

                    userList.Add(userObj);
                }
            }
            return userList;
        }    

        protected internal UserManager LoginByEmail(string email, string password)
        {           
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.role_name, ");
            sqlCommand.Append("obj2.first_name, ");
            sqlCommand.Append("obj2.middle_name, ");
            sqlCommand.Append("obj2.last_name, ");
            sqlCommand.Append("obj2.user_uuid, ");
            sqlCommand.Append("obj2.email, ");
            sqlCommand.Append("obj2.password ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_user_roles obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_users obj2 ON obj2.role_uuid = obj1.role_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj2.email = :email ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj2.password = :password ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("email", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = email;

            arParams[1] = new NpgsqlParameter("password", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = password;

            UserManager userObj = new UserManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                if (reader.Read())
                {
                    userObj.FirstName = reader["first_name"].ToString();
                    userObj.MiddleName = reader["middle_name"].ToString();
                    userObj.LastName = reader["last_name"].ToString();
                    userObj.Email = reader["email"].ToString();
                    userObj.RoleManager.RoleName = reader["role_name"].ToString();
                    userObj.UserGuid = new Guid(reader["user_uuid"].ToString());
                    userObj.Password = reader["password"].ToString();
                }

            }
            return userObj;
           
        }

        protected internal List<UserManager> GetUserData()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.first_name, ");
            sqlCommand.Append("obj1.email, ");
            sqlCommand.Append("obj2.role_name, ");
            sqlCommand.Append("obj1.user_uuid ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_users obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_user_roles obj2 ON obj1.role_uuid = obj2.role_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.is_user_deleted = :isUserDeleted ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj2.role_can_be_deleted = :roleCanBeDeleted ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("isUserDeleted", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = false;

            arParams[1] = new NpgsqlParameter("roleCanBeDeleted", NpgsqlTypes.NpgsqlDbType.Boolean);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = true;

            List<UserManager> userList = new List<UserManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    UserManager userObj = new UserManager();

                    userObj.FirstName = reader["first_name"].ToString();
                    userObj.Email = reader["email"].ToString();
                    userObj.RoleManager.RoleName = reader["role_name"].ToString();
                    userObj.UserGuid = new Guid(reader["user_uuid"].ToString());

                    userList.Add(userObj);
                }
            }

            return userList;

        }

        protected internal bool DeleteUserData(List<Guid> userGuidList)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_users ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("is_user_deleted = true ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("user_uuid ");
            sqlCommand.Append("IN ");

            if (userGuidList.Count > 0)
            {
                sqlCommand.Append("( ");

                foreach (Guid guidObj in userGuidList)
                {
                    int index = userGuidList.IndexOf(guidObj);

                    if (index == userGuidList.Count - 1)
                    {
                        sqlCommand.Append("'" + guidObj + "' ");
                    }
                    else
                    {
                        sqlCommand.Append("'" + guidObj + "', ");
                    }
                }

                sqlCommand.Append(") ");
            }

            sqlCommand.Append(";");

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());

            return (rowsAffected > 0);

        }

        protected internal List<UserManager> GetUsersBasedOnRoleName(string roleName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_users obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_user_roles obj2 ON obj1.role_uuid = obj2.role_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj2.role_name = :roleName ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("roleName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = roleName;

            List<UserManager> userList = new List<UserManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    UserManager userObj = new UserManager();

                    userObj.UserGuid = new Guid(reader["user_uuid"].ToString());

                    //userObj.EmailId = reader["email"].ToString();

                    //userObj.DivisionManager.DivisionGuid = new Guid(reader["division_id"].ToString());
                    //userObj.DivisionManager.DivisionName = reader["division_name"].ToString();

                    //userObj.DesignationManager.DesignationGuid = new Guid(reader["designation_id"].ToString());
                    //userObj.DesignationManager.DesignationName = reader["designation_name"].ToString();

                    //userObj.DepartmentManager.DepartmentGuid = new Guid(reader["department_id"].ToString());
                    //userObj.DepartmentManager.DepartmentName = reader["department_name"].ToString();

                    //userObj.DateOfJoining = DateTime.Parse(reader["date_of_joining"].ToString());

                    //userObj.Title = Convert.ToInt16(reader["title"].ToString());
                    userObj.FirstName = reader["first_name"].ToString();
                    userObj.MiddleName = reader["middle_name"].ToString();
                    userObj.LastName = reader["last_name"].ToString();

                    //userObj.Gender = Convert.ToInt16(reader["gender"].ToString());

                    //userObj.DateOfBirth = DateTime.Parse(reader["date_of_birth"].ToString());

                    //userObj.MobileNumber = reader["mobile_number"].ToString();

                    //userObj.AddressManager.AddressGuid = new Guid(reader["address_id"].ToString());
                    //userObj.AddressManager.Address1 = reader["address1"].ToString();
                    //userObj.AddressManager.Address2 = reader["address2"].ToString();
                    //userObj.AddressManager.State = reader["state"].ToString();
                    //userObj.AddressManager.Country = reader["country"].ToString();
                    //userObj.AddressManager.PinCode = reader["pin"].ToString();

                    //userObj.UserType = Convert.ToInt16(reader["user_type"].ToString());

                    //userObj.Password = reader["password"].ToString();

                    //userObj.SuperUserGuid = new Guid(reader["super_user_id"].ToString());
                    //userObj.AdminGuid = new Guid(reader["admin_id"].ToString());
                    //userObj.SuperAdminGuid = new Guid(reader["super_admin_id"].ToString());

                    //userObj.SuperUserManager.SuperUserGuid = new Guid(reader["super_user_id"].ToString());

                    //userObj.AdminManager.AdminGuid = new Guid(reader["admin_id"].ToString());

                    //userObj.SuperAdminManager.SuperAdminGuid = new Guid(reader["super_admin_id"].ToString());

                    //userObj.LocalAccessDate = DateTime.Parse(reader["local_access_date"].ToString());
                    //userObj.LocalAccessCount = Convert.ToInt16(reader["local_count"].ToString());

                    //userObj.NationalAccessDate = DateTime.Parse(reader["national_access_date"].ToString());
                    //userObj.NationalAccessCount = Convert.ToInt16(reader["national_count"].ToString());

                    //userObj.InternationalAccessDate = DateTime.Parse(reader["international_access_date"].ToString());
                    //userObj.InternationalAccessCount = Convert.ToInt16(reader["international_count"].ToString());

                    //userObj.UserDisabled = Convert.ToBoolean(reader["user_disabled"].ToString());

                    userList.Add(userObj);
                }
            }
            return userList;
        }

        protected internal bool UpdateUserPassword(Guid userGuid, string newPassword)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_users ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("password = :newPassword ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("user_uuid = :userGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("newPassword", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = newPassword;

            arParams[1] = new NpgsqlParameter("userGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = userGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return (result == 1);
        }
    }
}