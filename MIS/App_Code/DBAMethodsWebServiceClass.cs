﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace MIS.App_Code
{
    //[WebService(Namespace = "http://tempuri.org/")]
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    //[System.ComponentModel.ToolboxItem(false)]
    //// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    //[System.Web.Script.Services.ScriptService]
    //public class DBAMethodsWebServiceClass
    //{
    //    [WebMethod]
    //    public string[] GetRoleNames()
    //    {
    //        List<RoleManager> roleList = new List<RoleManager>();
    //        roleList = new RoleDB().GetRoles();

    //        List<String> roleNameList = new List<string>();

    //        if (roleList.Count > 0)
    //        {
    //            foreach (RoleManager roleObj in roleList)
    //            {
    //                roleNameList.Add(roleObj.RoleGuid.ToString());
    //                roleNameList.Add(roleObj.RoleName.ToString());
    //            }
    //        }

    //        return roleNameList.ToArray();
    //    }

    //    [WebMethod]
    //    public int SaveUserData(string userFirstName, string userMiddleName, string userLastName, string userEmailId, string userRoleGuid)
    //    {
    //        int rowUpdateStatus;
    //        Guid userGuid = Guid.NewGuid();
    //        string userPassword = new PasswordGenerator().CreateRandomPassword();
    //        try
    //        {
    //            rowUpdateStatus = new UserDB().CreateUser(userGuid, userFirstName, userMiddleName, userLastName, userEmailId, new Guid(userRoleGuid), userPassword);

    //            if (rowUpdateStatus == 1)
    //            {
    //                string subject = "Successful Registration at MIS";
    //                string body = "Hello" + " " + "\n";
    //                body += "You have registered on MIS Successfully.\n";
    //                body += "Please wait for the password of the account.\n";
    //                body += "Thanks n Regards\n";
    //                body += "FREMAA\n";
    //                body += "This is a system generated mail. Please do not reply to this.";
    //                new MailManager().SendMailToUser(userEmailId, subject, body);
    //            }

    //            return rowUpdateStatus;
    //        }
    //        catch
    //        {
    //            return 0;
    //        }
    //    }

    //    [WebMethod]
    //    public List<UserManager> GetUsers()
    //    {
    //        List<UserManager> userList = new List<UserManager>();
    //        userList = new UserDB().GetUserData();

    //        List<String> userNameList = new List<string>();

    //        if (userList.Count > 0)
    //        {
    //            foreach (UserManager userObj in userList)
    //            {
    //                userNameList.Add(userObj.UserGuid.ToString());
    //                userNameList.Add(userObj.FirstName.ToString());
    //                userNameList.Add(userObj.Email);
    //                userNameList.Add(userObj.RoleManager.RoleName);
    //            }
    //        }

    //        //return userNameList;
    //        return userList;

    //    }

    //    [WebMethod]
    //    public bool DeleteUserData(string[] userGuidListToDelete)
    //    {
    //        List<Guid> userGuidList = new List<Guid>();

    //        foreach (string userGuidString in userGuidListToDelete)
    //        {
    //            userGuidList.Add(new Guid(userGuidString));
    //        }

    //        return new UserDB().DeleteUserData(userGuidList);
    //    }

    //    [WebMethod]
    //    public int SaveCircle(string circleCode, string circleName)
    //    {
    //        return new CircleDB().AddCircle(Guid.NewGuid(), circleCode, circleName);
    //    }

    //    [WebMethod]
    //    public string[] GetCircleNames()
    //    {
    //        List<CircleManager> circleList = new List<CircleManager>();
    //        circleList = new CircleDB().GetCircles();

    //        List<String> circleNameList = new List<string>();

    //        if (circleList.Count > 0)
    //        {
    //            foreach (CircleManager circleObj in circleList)
    //            {
    //                circleNameList.Add(circleObj.CircleGuid.ToString());
    //                circleNameList.Add(circleObj.CircleCode.ToString());
    //                circleNameList.Add(circleObj.CircleName.ToString());
    //            }
    //        }

    //        return circleNameList.ToArray();
    //    }

    //    [WebMethod]
    //    public bool UpdateCircle(string circleGuid, string circleName, string circleCode)
    //    {
    //        return new CircleDB().UpdateCircle(new Guid(circleGuid), circleName, circleCode);
    //    }

    //    [WebMethod]
    //    public int SaveDivision(string divisionCode, string divisionName, string circleGuid)
    //    {
    //        return new DivisionDB().AddDivision(Guid.NewGuid(), divisionCode, divisionName, new Guid(circleGuid));
    //    }

    //    //[WebMethod]
    //    //public string[] GetDivisions(Guid circleGuid)
    //    //{
    //    //    List<DivisionManager> divisionList = new List<DivisionManager>();
    //    //    divisionList = new DivisionDB().GetDivisionsFromCircleGuid(circleGuid);

    //    //    List<String> divisionNameList = new List<string>();

    //    //    if (divisionList.Count > 0)
    //    //    {
    //    //        foreach (DivisionManager divisionObj in divisionList)
    //    //        {
    //    //            divisionNameList.Add(divisionObj.DivisionGuid.ToString());
    //    //            divisionNameList.Add(divisionObj.DivisionCode.ToString());
    //    //            divisionNameList.Add(divisionObj.DivisionName.ToString());

    //    //        }
    //    //    }

    //    //    return divisionNameList.ToArray();
    //    //}

    //    [WebMethod]
    //    public bool UpdateDivision(string circleGuid, string divisionGuid, string edittedDivisionCode, string edittedDivisionName)
    //    {
    //        return new DivisionDB().UpdateDivision(new Guid(circleGuid), new Guid(divisionGuid), edittedDivisionName, edittedDivisionCode);
    //    }

    //    [WebMethod]
    //    public int SaveScheme(string schemeCode, string schemeType, string schemeName, string utmEast1, string utmEast2, string utmNorth1, string utmNorth2, string area, string circleGuid, string divisionGuid, string drawingId)
    //    {
    //        return new SchemeDB().AddScheme(Guid.NewGuid(), schemeCode, schemeType, schemeName, (string.IsNullOrEmpty(utmEast1) ? 0 : int.Parse(utmEast1)), (string.IsNullOrEmpty(utmEast2) ? 0 : int.Parse(utmEast2)), (string.IsNullOrEmpty(utmNorth1) ? 0 : int.Parse(utmNorth1)), (string.IsNullOrEmpty(utmNorth2) ? 0 : int.Parse(utmNorth2)), (string.IsNullOrEmpty(area) ? 0 : int.Parse(area)), new Guid(circleGuid), new Guid(divisionGuid), drawingId);
    //    }

    //    [WebMethod]
    //    public string[] GetSchemeNames(string circleGuid, string divisionGuid)
    //    {
    //        List<SchemeManager> schemeList = new List<SchemeManager>();
    //        schemeList = new SchemeDB().GetSchemesBasedOnCircleAndDivision(new Guid(circleGuid), new Guid(divisionGuid));

    //        List<String> schemeNameList = new List<string>();

    //        if (schemeList.Count > 0)
    //        {
    //            foreach (SchemeManager schemeObj in schemeList)
    //            {
    //                schemeNameList.Add(schemeObj.SchemeGuid.ToString());
    //                schemeNameList.Add(schemeObj.SchemeCode.ToString());
    //            }
    //        }
    //        return schemeNameList.ToArray();
    //    }

    //    [WebMethod]
    //    public string[] GetSchemeData(string circleGuid, string divisionGuid, string schemeGuid)
    //    {
    //        List<SchemeManager> schemeList = new List<SchemeManager>();
    //        schemeList = new SchemeDB().GetSchemeDataBySchemeGuid(new Guid(circleGuid), new Guid(divisionGuid), new Guid(schemeGuid));

    //        List<String> schemeNameList = new List<string>();

    //        if (schemeList.Count > 0)
    //        {
    //            foreach (SchemeManager schemeObj in schemeList)
    //            {
    //                schemeNameList.Add(schemeObj.SchemeGuid.ToString());
    //                schemeNameList.Add(schemeObj.SchemeCode.ToString());
    //                schemeNameList.Add(schemeObj.SchemeName.ToString());
    //                schemeNameList.Add(schemeObj.SchemeType.ToString());
    //                schemeNameList.Add(schemeObj.UtmEast1.ToString());
    //                schemeNameList.Add(schemeObj.UtmEast2.ToString());
    //                schemeNameList.Add(schemeObj.UtmNorth1.ToString());
    //                schemeNameList.Add(schemeObj.UtmNorth2.ToString());
    //                schemeNameList.Add(schemeObj.Area.ToString());
    //                schemeNameList.Add(schemeObj.DrawingId.ToString());
    //            }
    //        }
    //        return schemeNameList.ToArray();
    //    }


    //    [WebMethod]
    //    public bool UpdateScheme(string circleGuid, string divisionGuid, string schemeGuid, string edittedSchemeCode, string edittedSchemeName, string edittedSchemeType, string edittedUtmEast1, string edittedUtmEast2, string edittedUtmNorth1, string edittedUtmNorth2, string edittedArea, string edittedDrawingId)
    //    {
    //        return new SchemeDB().UpdateSchemeData(new Guid(circleGuid), new Guid(divisionGuid), new Guid(schemeGuid), edittedSchemeCode, edittedSchemeType, edittedSchemeName, (string.IsNullOrEmpty(edittedUtmEast1) ? 0 : int.Parse(edittedUtmEast1)), (string.IsNullOrEmpty(edittedUtmEast2) ? 0 : int.Parse(edittedUtmEast2)), (string.IsNullOrEmpty(edittedUtmNorth1) ? 0 : int.Parse(edittedUtmNorth1)), (string.IsNullOrEmpty(edittedUtmNorth2) ? 0 : int.Parse(edittedUtmNorth2)), (string.IsNullOrEmpty(edittedArea) ? 0 : int.Parse(edittedArea)), edittedDrawingId);
    //    }

    //    #region DISTRICT

    //    [WebMethod]
    //    public List<DivisionManager> GetDivisions()
    //    {
    //        List<DivisionManager> divisionManagerList = new List<DivisionManager>();
    //        divisionManagerList = new DivisionDB().GetDivisions();

    //        return divisionManagerList;
    //    }

    //    #endregion




    //    #region WORKITEM

    //    [WebMethod]
    //    public List<WorkItemManager> GetWorkItems()
    //    {
    //        List<WorkItemManager> workItemList = new List<WorkItemManager>();
    //        workItemList = new WorkItemDB().GetWorkItemsWithUnitAndWorkItemSubHeads();

    //        //Testing
    //        //WorkItemManager a = new WorkItemDB().GetWorkItem(new Guid());
    //        //WorkItemManager b = new WorkItemDB().GetWorkItemWithUnit(new Guid());
    //        //WorkItemManager c = new WorkItemDB().GetWorkItemWithWorkItemSubHeads(new Guid());
    //        //WorkItemManager d = new WorkItemDB().GetWorkItemWithUnitAndWorkItemSubHeads(new Guid());

    //        return workItemList;
    //    }

    //    [WebMethod]
    //    public WorkItemManager GetWorkItem(Guid workItemGuid)
    //    {
    //        WorkItemManager workItemManagerObj = new WorkItemManager();
    //        workItemManagerObj = new WorkItemDB().GetWorkItemWithUnitAndWorkItemSubHeads(workItemGuid);

    //        return workItemManagerObj;
    //    }

    //    [WebMethod]
    //    public bool CheckWorkItemExistence(string workItemName)
    //    {
    //        return new WorkItemDB().CheckWorkItemExistence(workItemName);
    //    }

    //    [WebMethod]
    //    public Guid SaveWorkItem(WorkItemManager workItemManager)
    //    {
    //        workItemManager.WorkItemGuid = Guid.NewGuid();
    //        int returnValue = new WorkItemDB().AddWorkItem(workItemManager);

    //        if (returnValue == 1)
    //        {
    //            return workItemManager.WorkItemGuid;
    //        }
    //        else
    //        {
    //            return new Guid();
    //        }
    //    }

    //    [WebMethod]
    //    public int UpdateWorkItem(WorkItemManager workItemManager, bool editWorkItemName)
    //    {
    //        if (editWorkItemName == false)
    //        {
    //            return new WorkItemDB().UpdateWorkItem(workItemManager);
    //        }
    //        else
    //        {
    //            return new WorkItemDB().UpdateWorkItemWithoutWorkItemName(workItemManager);
    //        }
    //    }

    //    [WebMethod]
    //    public int DeleteWorkItem(Guid workItemGuid)
    //    {
    //        return new WorkItemDB().DeleteWorkItem(workItemGuid);
    //    }

    //    #endregion




    //    #region WORKITEM SUBHEAD

    //    [WebMethod]
    //    public List<WorkItemSubHeadManager> GetWorkItemSubHeads()
    //    {
    //        List<WorkItemSubHeadManager> workItemSubHeadManagerList = new List<WorkItemSubHeadManager>();
    //        workItemSubHeadManagerList = new WorkItemSubHeadDB().GetWorkItemSubHeads();

    //        return workItemSubHeadManagerList;
    //    }

    //    [WebMethod]
    //    public bool CheckWorkItemSubHeadExistence(string subHeadName)
    //    {
    //        return new WorkItemSubHeadDB().CheckWorkItemSubHeadExistence(subHeadName);
    //    }

    //    [WebMethod]
    //    public Guid SaveWorkItemSubHead(string workItemSubHeadName)
    //    {
    //        Guid workItemSubHeadGuid = Guid.NewGuid();
    //        int returnValue = new WorkItemSubHeadDB().AddWorkItemSubHead(workItemSubHeadGuid, workItemSubHeadName);

    //        if (returnValue == 1)
    //        {
    //            return workItemSubHeadGuid;
    //        }
    //        else
    //        {
    //            return new Guid();
    //        }
    //    }

    //    [WebMethod]
    //    public int UpdateWorkItemSubHead(string workItemSubHeadName, Guid workItemSubHeadGuid)
    //    {
    //        return new WorkItemSubHeadDB().UpdateWorkItemSubHead(workItemSubHeadGuid, workItemSubHeadName);
    //    }

    //    [WebMethod]
    //    public int DeleteWorkItemSubHead(Guid workItemSubHeadGuid)
    //    {
    //        return new WorkItemSubHeadDB().DeleteWorkItemSubHead(workItemSubHeadGuid);
    //    }

    //    #endregion




    //    #region FINANCIAL SUBHEAD

    //    [WebMethod]
    //    public List<FinancialSubHeadManager> GetFinancialSubHeads()
    //    {
    //        List<FinancialSubHeadManager> financialSubHeadManagerList = new List<FinancialSubHeadManager>();
    //        financialSubHeadManagerList = new FinancialSubHeadDB().GetFinancialSubHeads();

    //        return financialSubHeadManagerList;

    //    }

    //    [WebMethod]
    //    public bool CheckFinancialSubHeadExistence(string financialSubHeadName)
    //    {
    //        return new FinancialSubHeadDB().CheckFinancialSubHeadExistence(financialSubHeadName);
    //    }

    //    [WebMethod]
    //    public Guid SaveFinancialSubHead(string financialSubHeadName)
    //    {
    //        Guid financialSubHeadGuid = Guid.NewGuid();
    //        int returnValue = new FinancialSubHeadDB().AddFinancialSubHead(financialSubHeadGuid, financialSubHeadName);

    //        if (returnValue == 1)
    //        {
    //            return financialSubHeadGuid;
    //        }
    //        else
    //        {
    //            return new Guid();
    //        }
    //    }

    //    [WebMethod]
    //    public int UpdateFinancialSubHead(string financialSubHeadName, Guid financialSubHeadGuid)
    //    {
    //        return new FinancialSubHeadDB().UpdateFinancialSubHead(financialSubHeadGuid, financialSubHeadName);
    //    }

    //    [WebMethod]
    //    public int DeleteFinancialSubHead(Guid financialSubHeadGuid)
    //    {
    //        return new FinancialSubHeadDB().DeleteFinancialSubHead(financialSubHeadGuid);
    //    }

    //    #endregion




    //    #region DIVISION

    //    [WebMethod]
    //    public List<DivisionManager> GetDivisions()
    //    {
    //        List<DivisionManager> divisionManagerList = new List<DivisionManager>();
    //        divisionManagerList = new DivisionDB().GetDivisions();

    //        return divisionManagerList;
    //    }

    //    //[WebMethod]
    //    //public WorkItemManager GetWorkItem(Guid workItemGuid)
    //    //{
    //    //    WorkItemManager workItemManagerObj = new WorkItemManager();
    //    //    workItemManagerObj = new WorkItemDB().GetWorkItemWithUnitAndWorkItemSubHeads(workItemGuid);

    //    //    return workItemManagerObj;
    //    //}

    //    //[WebMethod]
    //    //public bool CheckWorkItemExistence(string workItemName)
    //    //{
    //    //    return new WorkItemDB().CheckWorkItemExistence(workItemName);
    //    //}

    //    //[WebMethod]
    //    //public Guid SaveWorkItem(WorkItemManager workItemManager)
    //    //{
    //    //    workItemManager.WorkItemGuid = Guid.NewGuid();
    //    //    int returnValue = new WorkItemDB().AddWorkItem(workItemManager);

    //    //    if (returnValue == 1)
    //    //    {
    //    //        return workItemManager.WorkItemGuid;
    //    //    }
    //    //    else
    //    //    {
    //    //        return new Guid();
    //    //    }
    //    //}

    //    //[WebMethod]
    //    //public int UpdateWorkItem(WorkItemManager workItemManager, bool editWorkItemName)
    //    //{
    //    //    if (editWorkItemName == false)
    //    //    {
    //    //        return new WorkItemDB().UpdateWorkItem(workItemManager);
    //    //    }
    //    //    else
    //    //    {
    //    //        return new WorkItemDB().UpdateWorkItemWithoutWorkItemName(workItemManager);
    //    //    }
    //    //}

    //    //[WebMethod]
    //    //public int DeleteWorkItem(Guid workItemGuid)
    //    //{
    //    //    return new WorkItemDB().DeleteWorkItem(workItemGuid);
    //    //}

    //    #endregion




    //    #region UNIT TYPES

    //    [WebMethod]
    //    public List<UnitTypeManager> GetUnitTypes()
    //    {
    //        List<UnitTypeManager> unitTypeManagerList = new List<UnitTypeManager>();
    //        unitTypeManagerList = new UnitTypeDB().GetUnitTypes();

    //        return unitTypeManagerList;
    //    }

    //    [WebMethod]
    //    public bool CheckUnitTypeExistence(string unitTypeName)
    //    {
    //        return new UnitTypeDB().CheckUnitTypeExistence(unitTypeName);
    //    }

    //    [WebMethod]
    //    public Guid SaveUnitType(string unitTypeName)
    //    {
    //        Guid unitTypeGuid = Guid.NewGuid();
    //        int returnValue = new UnitTypeDB().AddUnitType(unitTypeGuid, unitTypeName);

    //        if (returnValue == 1)
    //        {
    //            return unitTypeGuid;
    //        }
    //        else
    //        {
    //            return new Guid();
    //        }
    //    }

    //    [WebMethod]
    //    public int UpdateUnitType(string unitTypeName, Guid unitTypeGuid)
    //    {
    //        return new UnitTypeDB().UpdateUnitType(unitTypeGuid, unitTypeName);
    //    }

    //    [WebMethod]
    //    public int DeleteUnitType(Guid unitTypeGuid)
    //    {
    //        return new UnitTypeDB().DeleteUnitType(unitTypeGuid);
    //    }

    //    #endregion




    //    #region UNITS

    //    [WebMethod]
    //    public List<UnitManager> GetUnits(Guid unitTypeGuid)
    //    {
    //        List<UnitManager> unitManagerList = new List<UnitManager>();
    //        unitManagerList = new UnitDB().GetUnitsBasedOnUnitType(unitTypeGuid);

    //        return unitManagerList;
    //    }

    //    [WebMethod]
    //    public bool CheckUnitExistence(string unitName, Guid unitTypeGuid)
    //    {
    //        return new UnitDB().CheckUnitExistence(unitName, unitTypeGuid);
    //    }

    //    [WebMethod]
    //    public Guid SaveUnit(UnitManager unitManager)
    //    {
    //        unitManager.UnitGuid = Guid.NewGuid();
    //        int returnValue = new UnitDB().AddUnit(unitManager);

    //        if (returnValue == 1)
    //        {
    //            return unitManager.UnitGuid;
    //        }
    //        else
    //        {
    //            return new Guid();
    //        }
    //    }

    //    [WebMethod]
    //    public int UpdateUnit(UnitManager unitManager)
    //    {
    //        return new UnitDB().UpdateUnit(unitManager);
    //    }

    //    [WebMethod]
    //    public int DeleteUnit(Guid unitTypeGuid, Guid unitGuid)
    //    {
    //        return new UnitDB().DeleteUnit(unitTypeGuid, unitGuid);
    //    }

    //    #endregion




    //    [WebMethod(EnableSession = true)]
    //    public void UserLogout()
    //    {
    //        System.Web.HttpContext.Current.Session.Clear();
    //        System.Web.HttpContext.Current.Session.Abandon();
    //    }

    //}
}