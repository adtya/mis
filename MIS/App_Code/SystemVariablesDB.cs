﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SystemVariablesDB
    {
        protected internal SystemVariablesManager GetSystemVariablesGuid()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.system_variables_uuid, ");
            sqlCommand.Append("obj1.fiscal_year, ");
            sqlCommand.Append("obj2.circle_uuid, ");
            sqlCommand.Append("obj2.circle_code ");
            sqlCommand.Append("FROM system_variables obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_circles obj2 ");
            sqlCommand.Append("ON obj1.circle_uuid = obj2.circle_uuid ");                        
            sqlCommand.Append(";");
            
            SystemVariablesManager systemVariablesManagerObj = new SystemVariablesManager();
            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {                    
                    systemVariablesManagerObj.SystemVariablesGuid = new Guid(reader["system_variables_uuid"].ToString());
                    systemVariablesManagerObj.FiscalYear = reader["fiscal_year"].ToString();
                    systemVariablesManagerObj.CircleManager.CircleGuid = new Guid(reader["circle_uuid"].ToString());
                    systemVariablesManagerObj.CircleManager.CircleCode = reader["circle_code"].ToString();
                  
                }
            }
            return systemVariablesManagerObj;
        }

        protected internal SystemVariablesManager GetCurrentMonitoringDate()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.current_monitoring_date ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("system_variables obj1 ");
            sqlCommand.Append(";");
           
            SystemVariablesManager systemVariablesManagerObj = new SystemVariablesManager();
            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    systemVariablesManagerObj.CurrentMonitoringDate = Convert.ToDateTime(reader["current_monitoring_date"].ToString());                  
                }
            }
            return systemVariablesManagerObj;
        }

        //protected internal int AddSystemVariables(Guid systemVariablesGuid, string fiscalYear, DateTime currentMonitoringDate, int quarterNumber)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("INSERT INTO ");
        //    sqlCommand.Append("system_variables ");
        //    sqlCommand.Append("( ");
        //    sqlCommand.Append("system_variables_uuid, ");
        //    sqlCommand.Append("fiscal_year, ");
        //    sqlCommand.Append("current_monitoring_date, ");
        //    sqlCommand.Append("quarter_number ");
        //    sqlCommand.Append(") ");
        //    sqlCommand.Append("VALUES ");
        //    sqlCommand.Append("( ");
        //    sqlCommand.Append(":systemVariablesGuid, ");
        //    sqlCommand.Append(":fiscalYear, ");
        //    sqlCommand.Append(":currentMonitoringDate, ");
        //    sqlCommand.Append(":quarterNumber ");
        //    sqlCommand.Append(") ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter[] arParams = new NpgsqlParameter[4];

        //    arParams[0] = new NpgsqlParameter("systemVariablesGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[0].Direction = ParameterDirection.Input;
        //    arParams[0].Value = systemVariablesGuid;

        //    arParams[1] = new NpgsqlParameter("fiscalYear", NpgsqlTypes.NpgsqlDbType.Text);
        //    arParams[1].Direction = ParameterDirection.Input;
        //    arParams[1].Value = fiscalYear;

        //    arParams[2] = new NpgsqlParameter("currentMonitoringDate", NpgsqlTypes.NpgsqlDbType.Date);
        //    arParams[2].Direction = ParameterDirection.Input;
        //    arParams[2].Value = currentMonitoringDate;

        //    arParams[3] = new NpgsqlParameter("quarterNumber", NpgsqlTypes.NpgsqlDbType.Integer);
        //    arParams[3].Direction = ParameterDirection.Input;
        //    arParams[3].Value = quarterNumber;

        //    int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

        //    return result;
        //}

        protected internal bool UpdateSystemVariables(Guid systemVariablesGuid, string fiscalYear, DateTime currentMonitoringDate, int quarterNumber,Guid circleGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("system_variables ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("fiscal_year = :fiscalYear, ");
            sqlCommand.Append("current_monitoring_date = :currentMonitoringDate, ");
            sqlCommand.Append("quarter_number = :quarterNumber, ");
            sqlCommand.Append("circle_uuid = :circleGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("system_variables_uuid = :systemVariablesGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[5];

            arParams[0] = new NpgsqlParameter("systemVariablesGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = systemVariablesGuid;

            arParams[1] = new NpgsqlParameter("fiscalYear", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = fiscalYear;

            arParams[2] = new NpgsqlParameter("currentMonitoringDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = currentMonitoringDate;

            arParams[3] = new NpgsqlParameter("quarterNumber", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = quarterNumber;

            arParams[4] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = circleGuid;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return (rowsAffected > 0);
        }
    }
}