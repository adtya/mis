﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class EmbankmentFillTypeDB
    {
        protected internal List<EmbankmentFillTypeManager> GetEmbankmentFillTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("fill_type_uuid, ");
            sqlCommand.Append("fill_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_embankment_fill_type ");
            sqlCommand.Append("ORDER BY fill_type_name ");
            sqlCommand.Append(";");

            List<EmbankmentFillTypeManager> embankmentFillTypeManagerList = new List<EmbankmentFillTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    EmbankmentFillTypeManager embankmentFillTypeManagerObj = new EmbankmentFillTypeManager();

                    embankmentFillTypeManagerObj.FillTypeGuid = new Guid(reader["fill_type_uuid"].ToString());
                    embankmentFillTypeManagerObj.FillTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["fill_type_name"].ToString());

                    embankmentFillTypeManagerList.Add(embankmentFillTypeManagerObj);
                }
            }

            return embankmentFillTypeManagerList;
        }
    }
}