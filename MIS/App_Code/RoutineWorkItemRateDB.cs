﻿using NodaMoney;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineWorkItemRateDB
    {
        protected internal RoutineWorkItemRateManager GetRoutineWorkItemRateForRoutineAnalysis(Guid routineWorkItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");          
            sqlCommand.Append("obj1.routine_work_item_rate_uuid, ");
            sqlCommand.Append("obj1.routine_work_item_rate, ");
            sqlCommand.Append("obj2.fiscal_year ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_work_item_rate obj1 ");            
            sqlCommand.Append("LEFT OUTER JOIN system_variables obj2 ON obj1.routine_work_item_rate_fiscal_year = obj2.fiscal_year ");
            sqlCommand.Append("AND  ");
            sqlCommand.Append("obj1.circle_uuid= obj2.circle_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_uuid = :routineWorkItemGuid  ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            RoutineWorkItemRateManager routineWorkItemRateManagerObj = new RoutineWorkItemRateManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    routineWorkItemRateManagerObj.RoutineWorkItemRateGuid = new Guid(reader["routine_work_item_rate_uuid"].ToString());
                    routineWorkItemRateManagerObj.RoutineWorkItemRate = string.IsNullOrEmpty(reader["routine_work_item_rate"].ToString()) ? Money.Parse("0") : Money.Parse(reader["routine_work_item_rate"].ToString());
                    routineWorkItemRateManagerObj.SystemVariablesManager.FiscalYear = reader["fiscal_year"].ToString();
                   
                }
            }
            return routineWorkItemRateManagerObj;
        }

        protected internal RoutineWorkItemRateManager GetRoutineWorkItemRateForFieldSurvey(Guid routineWorkItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");    
            sqlCommand.Append("obj1.routine_work_item_rate ");     
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_work_item_rate obj1 ");         
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_uuid = :routineWorkItemGuid  ");
            sqlCommand.Append("AND obj1.circle_uuid = ANY(SELECT circle_uuid FROM system_variables) ");
            sqlCommand.Append("AND obj1.routine_work_item_rate_fiscal_year = ANY(SELECT fiscal_year FROM system_variables) ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            RoutineWorkItemRateManager routineWorkItemRateManagerObj = new RoutineWorkItemRateManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {                    
                    routineWorkItemRateManagerObj.RoutineWorkItemRate = string.IsNullOrEmpty(reader["routine_work_item_rate"].ToString()) ? Money.Parse("0") : Money.Parse(reader["routine_work_item_rate"].ToString());                   
                }
            }
            return routineWorkItemRateManagerObj;
        }

        protected internal List<RoutineWorkItemManager> GetRoutineWorkItemRateData()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_work_item_class_name, ");
            sqlCommand.Append("obj2.routine_work_item_uuid, ");
            sqlCommand.Append("obj2.routine_work_item_type_code, ");
            sqlCommand.Append("obj2.routine_work_item_short_desc, ");
            sqlCommand.Append("obj3.unit_name, ");
            sqlCommand.Append("obj5.routine_work_item_rate_uuid, ");
            sqlCommand.Append("obj5.routine_work_item_rate, ");
            sqlCommand.Append("obj6.fiscal_year, ");
            sqlCommand.Append("obj6.circle_uuid, ");
            sqlCommand.Append("obj7.circle_code ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_work_items obj2 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_routine_work_item_class obj1 ON obj2.routine_work_item_class_uuid = obj1.routine_work_item_class_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_routine_work_item_rate obj5 ON obj2.routine_work_item_uuid = obj5.routine_work_item_uuid ");
            sqlCommand.Append("AND obj5.routine_work_item_rate_fiscal_year = (SELECT fiscal_year FROM system_variables)");
            sqlCommand.Append("AND obj5.circle_uuid = (SELECT circle_uuid FROM system_variables)");
            sqlCommand.Append("LEFT OUTER JOIN system_variables obj6 ON obj5.routine_work_item_rate_fiscal_year = obj6.fiscal_year ");
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_quantity_parameters obj4 ON obj2.asset_quantity_parameter_uuid = obj4.asset_quantity_parameter_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_units obj3 ON obj3.unit_uuid = obj4.unit_uuid  ");
            sqlCommand.Append("LEFT OUTER JOIN mis_circles obj7 ON obj6.circle_uuid = obj7.circle_uuid  ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("routine_work_item_type_code ");
            sqlCommand.Append(";");

            List<RoutineWorkItemManager> routineWorkItemRateManagerList = new List<RoutineWorkItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RoutineWorkItemManager routineWorkItemManagerObj = new RoutineWorkItemManager();

                    RoutineWorkItemRateManager routineWorkItemRateManagerObj = new RoutineWorkItemRateManager();

                    routineWorkItemRateManagerObj.RoutineWorkItemRateGuid = string.IsNullOrEmpty(reader["routine_work_item_rate_uuid"].ToString()) ? new Guid() : new Guid(reader["routine_work_item_rate_uuid"].ToString());
                    routineWorkItemRateManagerObj.RoutineWorkItemRate = string.IsNullOrEmpty(reader["routine_work_item_rate"].ToString()) ? Money.Parse("0") : Money.Parse(reader["routine_work_item_rate"].ToString());
                    routineWorkItemRateManagerObj.SystemVariablesManager.FiscalYear = reader["fiscal_year"].ToString();
                    routineWorkItemRateManagerObj.SystemVariablesManager.CircleManager.CircleGuid = string.IsNullOrEmpty(reader["circle_uuid"].ToString()) ? new Guid() : new Guid(reader["circle_uuid"].ToString());
                    routineWorkItemRateManagerObj.SystemVariablesManager.CircleManager.CircleCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_type_code"].ToString());
                  
                    routineWorkItemManagerObj.RoutineWorkItemClassManager.RoutineWorkItemClassName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_class_name"].ToString());

                    routineWorkItemManagerObj.RoutineWorkItemGuid = new Guid(reader["routine_work_item_uuid"].ToString());
                    routineWorkItemManagerObj.RoutineWorkItemTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_type_code"].ToString());
                    routineWorkItemManagerObj.ShortDescription = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_short_desc"].ToString());
                    routineWorkItemManagerObj.AssetQuantityParameterManager.UnitManager.UnitName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["unit_name"].ToString());

                    routineWorkItemManagerObj.RoutineWorkItemRateManagerList = new List<RoutineWorkItemRateManager>();
                    routineWorkItemManagerObj.RoutineWorkItemRateManagerList.Add(routineWorkItemRateManagerObj);

                    routineWorkItemRateManagerList.Add(routineWorkItemManagerObj);
                }
            }
            return routineWorkItemRateManagerList;
        }

        protected internal List<RoutineWorkItemRateManager> GetRoutineWorkItemRateForRoutineWorkItemForRoutineAnalysis(Guid routineWorkItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_work_item_rate ");
            sqlCommand.Append("FROM mis_om_routine_work_item_rate obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_uuid= :routineWorkItemGuid ");
            sqlCommand.Append("; ");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            List<RoutineWorkItemRateManager> routineWorkItemRateList = new List<RoutineWorkItemRateManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    RoutineWorkItemRateManager routineWorkItemRateObj = new RoutineWorkItemRateManager();
                    
                    routineWorkItemRateObj.RoutineWorkItemRate = string.IsNullOrEmpty(reader["routine_work_item_rate"].ToString()) ? Money.Parse("0") : Money.Parse(reader["routine_work_item_rate"].ToString());
                    
                    routineWorkItemRateList.Add(routineWorkItemRateObj);
                }
            }

            return routineWorkItemRateList;
        }

        protected internal int AddRoutineWorkItemRateData(RoutineWorkItemRateManager routineWorkItemRateManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_routine_work_item_rate ");
            sqlCommand.Append("( ");
            sqlCommand.Append("routine_work_item_rate_uuid, ");
            sqlCommand.Append("routine_work_item_uuid, ");
            sqlCommand.Append("routine_work_item_rate, ");
            sqlCommand.Append("routine_work_item_rate_fiscal_year, ");
            sqlCommand.Append("circle_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":routineWorkItemRateGuid, ");
            sqlCommand.Append(":routineWorkItemGuid, ");
            sqlCommand.Append(":routineWorkItemRate, ");
            sqlCommand.Append(":fiscalYear, ");
            sqlCommand.Append(":circleGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[5];

            arParams[0] = new NpgsqlParameter("routineWorkItemRateGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemRateManager.RoutineWorkItemRateGuid;

            arParams[1] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = routineWorkItemRateManager.RoutineWorkItemManager.RoutineWorkItemGuid;

            arParams[2] = new NpgsqlParameter("routineWorkItemRate", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = routineWorkItemRateManager.RoutineWorkItemRate.Amount;

            arParams[3] = new NpgsqlParameter("fiscalYear", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = routineWorkItemRateManager.SystemVariablesManager.FiscalYear;

            arParams[4] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = routineWorkItemRateManager.SystemVariablesManager.CircleManager.CircleGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateRoutineWorkItemRateData(Guid routineWorkItemRateGuid, Money routineWorkItemRate)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_om_routine_work_item_rate ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("routine_work_item_rate= :routineWorkItemRate ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("routine_work_item_rate_uuid= :routineWorkItemRateGuid");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("routineWorkItemRateGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemRateGuid;

            arParams[1] = new NpgsqlParameter("routineWorkItemRate", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = routineWorkItemRate.Amount;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;
        }
    }
}