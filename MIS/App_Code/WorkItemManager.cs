﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class WorkItemManager
    {
        #region Private Members

        private Guid workItemGuid;
        private string workItemName;
        private UnitManager unitManager;
        private string workItemTotalValue;
        private string workItemCumulativeValue;
        private string workItemBalanceValue;
        private List<WorkItemSubHeadManager> workItemSubHeadManager;
        private List<WorkItemProgressManager> workItemProgressManager;
        
        #endregion


        #region Public Getter/Setter Properties

        public Guid WorkItemGuid
        {
            get
            {
                return workItemGuid;
            }
            set
            {
                workItemGuid = value;
            }
        }

        public string WorkItemName
        {
            get
            {
                return workItemName;
            }
            set
            {
                workItemName = value;
            }
        }

        public UnitManager UnitManager
        {
            get
            {
                if (unitManager == null)
                {
                    unitManager = new UnitManager();
                }
                return unitManager;
            }
            set
            {
                unitManager = value;
            }
        }

        public string WorkItemTotalValue
        {
            get
            {
                return workItemTotalValue;
            }
            set
            {
                workItemTotalValue = value;
            }
        }

        public string WorkItemCumulativeValue
        {
            get
            {
                return workItemCumulativeValue;
            }
            set
            {
                workItemCumulativeValue = value;
            }
        }

        public string WorkItemBalanceValue
        {
            get
            {
                return workItemBalanceValue;
            }
            set
            {
                workItemBalanceValue = value;
            }
        }

        public List<WorkItemSubHeadManager> WorkItemSubHeadManager
        {
            get
            {
                if (workItemSubHeadManager == null)
                {
                    workItemSubHeadManager = new List<WorkItemSubHeadManager>();
                }
                return workItemSubHeadManager;
            }
            set
            {
                workItemSubHeadManager = value;
            }
        }

        public List<WorkItemProgressManager> WorkItemProgressManager
        {
            get
            {
                if (workItemProgressManager == null)
                {
                    workItemProgressManager = new List<WorkItemProgressManager>();
                }
                return workItemProgressManager;
            }
            set
            {
                workItemProgressManager = value;
            }
        }

        #endregion
    }
}