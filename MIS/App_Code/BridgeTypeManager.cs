﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class BridgeTypeManager
    {
        #region Private Members

        private Guid bridgeTypeGuid;        
        private string bridgeTypeName;      

        #endregion

        #region Public Getter/Setter Properties

        public Guid BridgeTypeGuid
        {
            get
            {
                return bridgeTypeGuid;
            }
            set
            {
                bridgeTypeGuid = value;
            }
        }

        public string BridgeTypeName
        {
            get
            {
                return bridgeTypeName;
            }
            set
            {
                bridgeTypeName = value;
            }
        }        

        #endregion
    }
}