﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class WorkItemWorkItemSubHeadRelationDB
    {
        protected internal void AddWorkItemWorkItemSubHeadRelation(Guid workItemGuid, Array workItemSubHeadsGuidList)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_work_item_project_work_item_sub_heads_relation ");
            sqlCommand.Append("( ");
            sqlCommand.Append("work_item_and_work_item_sub_heads_relation_uuid, ");
            sqlCommand.Append("work_item_uuid, ");
            sqlCommand.Append("work_item_sub_head_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            if (workItemSubHeadsGuidList.Length > 0)
            {
                foreach (string workItemSubHeadsGuidObj in workItemSubHeadsGuidList)
                {
                    int index = Array.IndexOf(workItemSubHeadsGuidList, workItemSubHeadsGuidObj);

                    sqlCommand.Append("( ");

                    sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                    sqlCommand.Append("'" + workItemGuid.ToString() + "', ");
                    sqlCommand.Append("'" + workItemSubHeadsGuidObj + "' ");

                    if (index == workItemSubHeadsGuidList.Length - 1)
                    {
                        sqlCommand.Append(") ");
                    }
                    else
                    {
                        sqlCommand.Append("), ");
                    }
                }

                sqlCommand.Append(";");

                NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());
            }

        }
    }
}