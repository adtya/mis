﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class WorkType
    {
        #region Private Members

        private Guid workTypeGuid;
        private string workTypeName;
        private string[] workTypeSubHeadTitle;
        private string workTypeUnit;

        #endregion

        #region Public Getter/Setter Properties

        public Guid WorkTypeGuid
        {
            get
            {
                return workTypeGuid;
            }
            set
            {
                workTypeGuid = value;
            }
        }

        public string WorkTypeName
        {
            get
            {
                return workTypeName;
            }
            set
            {
                workTypeName = value;
            }
        }

        public string[] WorkTypeSubHeadTitle
        {
            get
            {
                return workTypeSubHeadTitle;
            }
            set
            {
                workTypeSubHeadTitle = value;
            }
        }

        public string WorkTypeUnit
        {
            get
            {
                return workTypeUnit;
            }
            set
            {
                workTypeUnit = value;
            }
        }

        #endregion
    }
}