﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class DivisonDB
    {
        #region Public Methods

        protected internal int AddDivison(Guid divisonGuid, string divisonName, string divisionCode, Guid circleGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_divisons ");
            sqlCommand.Append("( ");
            sqlCommand.Append("divison_uuid, ");
            sqlCommand.Append("divison_name, ");
            sqlCommand.Append("divison_code, ");
            sqlCommand.Append("circle_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":divisonGuid, ");
            sqlCommand.Append(":divisonName, ");
            sqlCommand.Append(":divisonCode, ");
            sqlCommand.Append(":circleGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[4];

            arParams[0] = new NpgsqlParameter("divisonGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = divisonGuid;

            arParams[1] = new NpgsqlParameter("divisonName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = divisonName.ToUpper();

            arParams[2] = new NpgsqlParameter("divisonCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = divisionCode;

            arParams[3] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = circleGuid;

            return Convert.ToInt32(NpgsqlHelper.ExecuteScalar(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams));

        }

        protected internal List<DivisonManager> GetDivisons()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("divison_uuid, ");
            sqlCommand.Append("divison_name,");
            sqlCommand.Append("divison_code "); 
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_divisons ");
            sqlCommand.Append("ORDER BY divison_name ");
            sqlCommand.Append(";");             

            List<DivisonManager> divisonList = new List<DivisonManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    DivisonManager divisonObj = new DivisonManager();

                    divisonObj.DivisonGuid = new Guid(reader["divison_uuid"].ToString());
                    divisonObj.DivisonName = reader["divison_name"].ToString();
                    divisonObj.DivisonCode = reader["divison_code"].ToString(); 
                    divisonList.Add(divisonObj);
                }
            }
            return divisonList;
        }


        protected internal List<DivisonManager> GetDivisonsForCircle(Guid circleGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("divison_uuid, ");
            sqlCommand.Append("divison_name, ");
            sqlCommand.Append("divison_code ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_divisons ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("circle_uuid = :circleGuid ");
            sqlCommand.Append("ORDER BY divison_name ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = circleGuid;

            List<DivisonManager> divisonList = new List<DivisonManager>();           

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(),arParams))
            {
                while (reader.Read())
                {
                    DivisonManager divisonObj = new DivisonManager();
                    divisonObj.DivisonGuid = new Guid(reader["divison_uuid"].ToString());
                    divisonObj.DivisonName = reader["divison_name"].ToString();
                    divisonObj.DivisonCode = reader["divison_code"].ToString();
                    divisonList.Add(divisonObj);
                }
            }
            return divisonList;        
        
        }

        protected internal bool UpdateDivison(Guid divisonGuid, string newDivisonName, string newDivisonCode)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_divisons ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("divison_name = :newDivisonName, ");
            sqlCommand.Append("divison_code = :newDivisonCode ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("divison_uuid = :divisonGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("newDivisonName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = newDivisonName.ToUpper();

            arParams[1] = new NpgsqlParameter("newDivisonCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = newDivisonCode.ToUpper();

            arParams[2] = new NpgsqlParameter("divisonGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = divisonGuid;

            int result = Convert.ToInt32(NpgsqlHelper.ExecuteScalar(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams));
            return (result > -1);
        }

        protected internal bool UpdateDivisonData(Guid circleGuid, Guid divisonGuid, string editDivisonCode, string editDivisonName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_divisons ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("divison_name = :editDivisonName, ");
            sqlCommand.Append("divison_code = :editDivisonCode ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("divison_uuid = :divisonGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("circle_uuid = :circleGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[4];

            arParams[0] = new NpgsqlParameter("editDivisonName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = editDivisonName.ToUpper();

            arParams[1] = new NpgsqlParameter("editDivisonCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = editDivisonCode.ToUpper();

            arParams[2] = new NpgsqlParameter("divisonGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = divisonGuid;

            arParams[3] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = circleGuid;

            int result = Convert.ToInt32(NpgsqlHelper.ExecuteScalar(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams));
            return (result > -1);
        }
        //protected internal bool DeleteDivison(Guid divisonGuid)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("DELETE ");
        //    sqlCommand.Append("FROM ");
        //    sqlCommand.Append("otp_divisons ");
        //    sqlCommand.Append("WHERE ");
        //    sqlCommand.Append("divison_id = :divisonGuid ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter arParams = new NpgsqlParameter();

        //    arParams = new NpgsqlParameter("divisonGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams.Direction = ParameterDirection.Input;
        //    arParams.Value = divisonGuid;

        //    int result = -1;
        //    try
        //    {
        //        result = Convert.ToInt32(NpgsqlHelper.ExecuteScalar(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams));
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        String a = e.StackTrace;
        //    }
        //    return (result > -1);
        //}

        #endregion
    }
}