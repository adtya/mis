﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class CanalLiningTypeManager
    {
        #region Private Members

        private Guid liningTypeGuid;
        private string liningTypeName;

        #endregion

        #region Public Getter/Setter Properties

        public Guid LiningTypeGuid
        {
            get
            {
                return liningTypeGuid;
            }
            set
            {
                liningTypeGuid = value;
            }
        }

        public string LiningTypeName
        {
            get
            {
                return liningTypeName;
            }
            set
            {
                liningTypeName = value;
            }
        }

        #endregion
    }
}