﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineIntensityFactorDB
    {
        protected internal RoutineIntensityFactorManager GetRoutineIntensityFactorDataForRoutineAnalysis(Guid routineWorkItemGuid, Guid assetGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_intensity_factor_uuid, ");
            sqlCommand.Append("obj1.routine_intensity_factor, ");
            sqlCommand.Append("obj1.routine_intensity_factor_justification ");            
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_intensity_factor obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_uuid = :routineWorkItemGuid ");
            sqlCommand.Append("AND  ");
            sqlCommand.Append("obj1.asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            arParams[1] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetGuid;

            RoutineIntensityFactorManager routineIntensityFactorManagerObj = new RoutineIntensityFactorManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    routineIntensityFactorManagerObj.RoutineIntensityFactorGuid = string.IsNullOrEmpty(reader["routine_intensity_factor_uuid"].ToString()) ? new Guid() : new Guid(reader["routine_intensity_factor_uuid"].ToString());
                    routineIntensityFactorManagerObj.RoutineIntensityFactor = Convert.ToInt16(reader["routine_intensity_factor"].ToString());
                    routineIntensityFactorManagerObj.RoutineIntensityFactorJustification = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["routine_intensity_factor_justification"].ToString());
                }
            }
            return routineIntensityFactorManagerObj;
        }

        protected internal List<RoutineIntensityFactorManager> GetRoutineIntensityFactorData()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.asset_uuid, ");
            sqlCommand.Append("obj2.asset_code, ");
            sqlCommand.Append("obj2.asset_name, ");
            sqlCommand.Append("obj5.routine_work_item_uuid, ");
            sqlCommand.Append("obj5.routine_work_item_type_code, ");
            sqlCommand.Append("obj5.routine_work_item_short_desc, ");
            sqlCommand.Append("obj1.routine_intensity_factor_uuid, ");
            sqlCommand.Append("obj1.routine_intensity_factor, ");
            sqlCommand.Append("obj1.routine_intensity_factor_justification, ");
            sqlCommand.Append("obj4.unit_name ");  
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_intensity_factor obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_assets obj2 ON obj2.asset_uuid = obj1.asset_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_routine_work_items obj5 ON obj5.routine_work_item_uuid = obj1.routine_work_item_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_quantity_parameters obj3 ON obj5.asset_quantity_parameter_uuid = obj3.asset_quantity_parameter_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_units obj4 ON obj3.unit_uuid = obj4.unit_uuid  ");            
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("asset_code ");
            sqlCommand.Append(";");

            List<RoutineIntensityFactorManager> routineIntensityFactorManagerList = new List<RoutineIntensityFactorManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RoutineIntensityFactorManager routineIntensityFactorManagerObj = new RoutineIntensityFactorManager();

                    routineIntensityFactorManagerObj.RoutineIntensityFactorGuid = new Guid(reader["routine_intensity_factor_uuid"].ToString());
                    routineIntensityFactorManagerObj.RoutineIntensityFactor = Convert.ToInt16(reader["routine_intensity_factor"].ToString());
                    routineIntensityFactorManagerObj.RoutineIntensityFactorJustification = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["routine_intensity_factor_justification"].ToString());

                    routineIntensityFactorManagerObj.AssetManager.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    routineIntensityFactorManagerObj.AssetManager.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_code"].ToString());
                    routineIntensityFactorManagerObj.AssetManager.AssetName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["asset_name"].ToString());

                    routineIntensityFactorManagerObj.RoutineWorkItemManager.RoutineWorkItemGuid = new Guid(reader["routine_work_item_uuid"].ToString());
                    routineIntensityFactorManagerObj.RoutineWorkItemManager.RoutineWorkItemTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_type_code"].ToString());
                    routineIntensityFactorManagerObj.RoutineWorkItemManager.ShortDescription = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_short_desc"].ToString());
                    routineIntensityFactorManagerObj.RoutineWorkItemManager.AssetQuantityParameterManager.UnitManager.UnitName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["unit_name"].ToString());                   
                    routineIntensityFactorManagerList.Add(routineIntensityFactorManagerObj);
                }
            }
            return routineIntensityFactorManagerList;
        }

        protected internal int SaveRoutineIntensityFactorData(Guid routineIntensityFactorGuid, Guid routineWorkItemGuid, Guid assetGuid, int routineIntensityFactor, string routineIntensityFactorJustification)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_routine_intensity_factor ");
            sqlCommand.Append("( ");
            sqlCommand.Append("routine_intensity_factor_uuid, ");
            sqlCommand.Append("routine_work_item_uuid, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("routine_intensity_factor, ");
            sqlCommand.Append("routine_intensity_factor_justification ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":routineIntensityFactorGuid, ");
            sqlCommand.Append(":routineWorkItemGuid, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":routineIntensityFactor, ");
            sqlCommand.Append(":routineIntensityFactorJustification ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[5];

            arParams[0] = new NpgsqlParameter("routineIntensityFactorGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineIntensityFactorGuid;

            arParams[1] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = routineWorkItemGuid;

            arParams[2] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetGuid;

            arParams[3] = new NpgsqlParameter("routineIntensityFactor", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = routineIntensityFactor;

            arParams[4] = new NpgsqlParameter("routineIntensityFactorJustification", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = routineIntensityFactorJustification;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateRoutineIntensityFactorData(Guid routineIntensityFactorGuid, int routineIntensityFactor, string routineIntensityFactorJustification)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_om_routine_intensity_factor ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("routine_intensity_factor= :routineIntensityFactor, ");
            sqlCommand.Append("routine_intensity_factor_justification= :routineIntensityFactorJustification ");          
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("routine_intensity_factor_uuid= :routineIntensityFactorGuid");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("routineIntensityFactorGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineIntensityFactorGuid;

            arParams[1] = new NpgsqlParameter("routineIntensityFactor", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = routineIntensityFactor;

            arParams[2] = new NpgsqlParameter("routineIntensityFactorJustification", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(routineIntensityFactorJustification);

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;
        }

        protected internal int DeleteRoutineIntensityFactorData(Guid routineWorkItemGuid, Guid assetGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_intensity_factor ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("routine_work_item_uuid = :routineWorkItemGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("asset_uuid = :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            arParams[1] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

    }
}