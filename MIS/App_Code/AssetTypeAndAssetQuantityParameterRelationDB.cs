﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class AssetTypeAndAssetQuantityParameterRelationDB
    {
        protected internal List<AssetTypeAndAssetQuantityParameterRelationManager> GetAssetTypeBasedOnAssetQuantityParameterForRoutineAnalysis(Guid assetQuantityParameterGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");            
            sqlCommand.Append("obj1.asset_type_uuid, ");          
            sqlCommand.Append("obj2.asset_type_code, ");
            sqlCommand.Append("obj3.asset_quantity_parameter_uuid, ");
            sqlCommand.Append("obj3.asset_quantity_parameter_name, ");
            sqlCommand.Append("obj4.unit_uuid, ");
            sqlCommand.Append("obj4.unit_name ");
            sqlCommand.Append("FROM mis_om_asset_type_asset_quantity_parameter_relationship obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_types obj2 ");
            sqlCommand.Append("ON obj1.asset_type_uuid = obj2.asset_type_uuid ");
            sqlCommand.Append("LEFT JOIN mis_asset_quantity_parameters obj3 ");
            sqlCommand.Append("ON obj3.asset_quantity_parameter_uuid = obj1.asset_quantity_parameter_uuid ");  
            sqlCommand.Append("LEFT JOIN mis_units obj4 ");
            sqlCommand.Append("ON obj4.unit_uuid = obj3.unit_uuid ");            
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.asset_quantity_parameter_uuid= :assetQuantityParameterGuid ");
            sqlCommand.Append("; ");            

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("assetQuantityParameterGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetQuantityParameterGuid;

            List<AssetTypeAndAssetQuantityParameterRelationManager> assetTypeAndAssetQuantityParameterRelationManagerList = new List<AssetTypeAndAssetQuantityParameterRelationManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    AssetTypeAndAssetQuantityParameterRelationManager assetTypeAndAssetQuantityParameterRelationManagerObj = new AssetTypeAndAssetQuantityParameterRelationManager();

                    assetTypeAndAssetQuantityParameterRelationManagerObj.AssetManager.AssetTypeManager.AssetTypeGuid = new Guid(reader["asset_type_uuid"].ToString());
                    assetTypeAndAssetQuantityParameterRelationManagerObj.AssetManager.AssetTypeManager.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_type_code"].ToString());
                    assetTypeAndAssetQuantityParameterRelationManagerObj.AssetQuantityParameterManager.AssetQuantityParameterGuid = new Guid(reader["asset_quantity_parameter_uuid"].ToString());
                    assetTypeAndAssetQuantityParameterRelationManagerObj.AssetQuantityParameterManager.AssetQuantityParameterName = reader["asset_quantity_parameter_name"].ToString();                  
                    assetTypeAndAssetQuantityParameterRelationManagerObj.AssetQuantityParameterManager.UnitManager.UnitGuid = new Guid(reader["unit_uuid"].ToString());
                    assetTypeAndAssetQuantityParameterRelationManagerObj.AssetQuantityParameterManager.UnitManager.UnitName = reader["unit_name"].ToString();

                    assetTypeAndAssetQuantityParameterRelationManagerList.Add(assetTypeAndAssetQuantityParameterRelationManagerObj);
                }
            }
            return assetTypeAndAssetQuantityParameterRelationManagerList;
        }
       
    }
}