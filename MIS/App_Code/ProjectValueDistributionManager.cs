﻿using NodaMoney;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    [Serializable]
    public class ProjectValueDistributionManager
    {
        private Guid projectValueDistributionGuid;
        private FinancialSubHeadManager financialSubHeadManager;
        private Money financialSubHeadBudget;
        private decimal financialSubHeadBudgetInput;
        private ProjectManager projectManager;

        public Guid ProjectValueDistributionGuid
        {
            get
            {
                return projectValueDistributionGuid;
            }
            set
            {
                projectValueDistributionGuid = value;
            }
        }

        public Money FinancialSubHeadBudget
        {
            get
            {
                if (financialSubHeadBudget.Amount.Equals(0))
                {
                    financialSubHeadBudget = financialSubHeadBudgetInput;
                }
                return financialSubHeadBudget;
            }
            set
            {
                financialSubHeadBudget = value;
            }
        }

        public decimal FinancialSubHeadBudgetInput
        {
            private get
            {
                return financialSubHeadBudgetInput;
            }
            set
            {
                financialSubHeadBudgetInput = value;
            }
        }

        public FinancialSubHeadManager FinancialSubHeadManager
        {
            get
            {
                if (financialSubHeadManager == null)
                {
                    financialSubHeadManager = new FinancialSubHeadManager();
                }
                return financialSubHeadManager;
            }
            set
            {
                financialSubHeadManager = value;
            }
        }

        public ProjectManager ProjectManager
        {
            get
            {
                if (projectManager == null)
                {
                    projectManager = new ProjectManager();
                }
                return projectManager;
            }
            set
            {
                projectManager = value;
            }
        }
        
    }
}