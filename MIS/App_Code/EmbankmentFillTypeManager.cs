﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class EmbankmentFillTypeManager
    {
         #region Private Members

        private Guid fillTypeGuid;
        private string fillTypeName;
       
       #endregion


        #region Public Constructors

        public EmbankmentFillTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid FillTypeGuid
        {
            get
            {
                return fillTypeGuid;
            }
            set
            {
                fillTypeGuid = value;
            }
        }

        public string FillTypeName
        {
            get
            {
                return fillTypeName;
            }
            set
            {
                fillTypeName = value;
            }
        }        

        #endregion
    }
}