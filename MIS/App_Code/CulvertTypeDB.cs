﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class CulvertTypeDB
    {
        protected internal List<CulvertTypeManager> GetCulvertTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("culvert_type_uuid, ");
            sqlCommand.Append("culvert_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_culvert_type ");
            sqlCommand.Append("ORDER BY culvert_type_name ");
            sqlCommand.Append(";");

            List<CulvertTypeManager> culvertTypeManagerList = new List<CulvertTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    CulvertTypeManager culvertTypeManagerObj = new CulvertTypeManager();

                    culvertTypeManagerObj.CulvertTypeGuid = new Guid(reader["culvert_type_uuid"].ToString());
                    culvertTypeManagerObj.CulvertTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["culvert_type_name"].ToString());

                    culvertTypeManagerList.Add(culvertTypeManagerObj);
                }
            }

            return culvertTypeManagerList;
        }
    }
}