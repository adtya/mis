﻿using NodaMoney;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineAnalysisDetailsManager
    {
        #region Private Members

        private Guid routineAnalysisDetailsGuid;       
        private AssetManager assetManager;
        private RoutineWorkItemManager routineWorkItemManager;
        private Money routineWorkItemCost;
        private decimal routineWorkItemCostInput;
        private double assetQuantity;   

        #endregion


        #region Public Getter/Setter Properties

        public Guid RoutineAnalysisDetailsGuid
        {
            get
            {
                return routineAnalysisDetailsGuid;
            }
            set
            {
                routineAnalysisDetailsGuid = value;
            }
        }

        public Money RoutineWorkItemCost
        {
            get
            {
                if (routineWorkItemCost.Amount.Equals(0))
                {
                    routineWorkItemCost = RoutineWorkItemCostInput;
                }
                return routineWorkItemCost;
            }
            set
            {
                routineWorkItemCost = value;
            }
        }

        public decimal RoutineWorkItemCostInput
        {
            private get
            {
                return routineWorkItemCostInput;
            }
            set
            {
                routineWorkItemCostInput = value;
            }
        }

        public double AssetQuantity
        {
            get
            {
                return assetQuantity;
            }
            set
            {
                assetQuantity = value;
            }
        }

        public AssetManager AssetManager
        {
            get
            {
                if (assetManager == null)
                {
                    assetManager = new AssetManager();
                }
                return assetManager;
            }
            set
            {
                assetManager = value;
            }
        }

        public RoutineWorkItemManager RoutineWorkItemManager
        {
            get
            {
                if (routineWorkItemManager == null)
                {
                    routineWorkItemManager = new RoutineWorkItemManager();
                }
                return routineWorkItemManager;
            }
            set
            {
                routineWorkItemManager = value;
            }
        }

        #endregion
    }
}