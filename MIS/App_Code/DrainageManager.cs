﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class DrainageManager
    {
        #region Private Members

        private Guid drainageGuid;
        private double usElevationDrainage;             
        private double dsElevationDrainage;
        private double lengthDrainage;
        private double bedWidthDrainage;
        private double slope1Drainage;
        private double slope2Drainage;
        private double averageDepthDrainage;
        //private AssetManager assetManager;             

        #endregion


        #region Public Constructors

        public DrainageManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid DrainageGuid
        {
            get
            {
                return drainageGuid;
            }
            set
            {
                drainageGuid = value;
            }
        }

        public double UsElevationDrainage
        {
            get
            {
                return usElevationDrainage;
            }
            set
            {
                usElevationDrainage = value;
            }
        }

        public double DsElevationDrainage
        {
            get
            {
                return dsElevationDrainage;
            }
            set
            {
                dsElevationDrainage = value;
            }
        }

        public double LengthDrainage
        {
            get
            {
                return lengthDrainage;
            }
            set
            {
                lengthDrainage = value;
            }
        }

        public double BedWidthDrainage
        {
            get
            {
                return bedWidthDrainage;
            }
            set
            {
                bedWidthDrainage = value;
            }
        }

        public double Slope1Drainage
        {
            get
            {
                return slope1Drainage;
            }
            set
            {
                slope1Drainage = value;
            }
        }

        public double Slope2Drainage
        {
            get
            {
                return slope2Drainage;
            }
            set
            {
                slope2Drainage = value;
            }
        }

        public double AverageDepthDrainage
        {
            get
            {
                return averageDepthDrainage;
            }
            set
            {
                averageDepthDrainage = value;
            }
        }      
         
        // public AssetManager AssetManager
        //{
        //    get
        //    {
        //        if (assetManager == null)
        //        {
        //            assetManager = new AssetManager();
        //        }
        //        return assetManager;
        //    }
        //    set
        //    {
        //        assetManager = value;
        //    }
        //}

        #endregion
    }
}