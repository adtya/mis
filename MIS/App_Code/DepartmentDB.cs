﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class DepartmentDB
    {
        //protected internal List<DepartmentManager> GetDepartments()
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("SELECT ");
        //    sqlCommand.Append("* ");
        //    sqlCommand.Append("FROM ");
        //    sqlCommand.Append("mis_departments ");
        //    sqlCommand.Append("ORDER BY department_name ");
        //    sqlCommand.Append(";");

        //    List<DepartmentManager> departmentList = new List<DepartmentManager>();

        //    using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
        //    {
        //        while (reader.Read())
        //        {
        //            DepartmentManager departmentObj = new DepartmentManager();

        //            departmentObj.DepartmentGuid = new Guid(reader["department_uuid"].ToString());
        //            departmentObj.DepartmentName = reader["department_name"].ToString();

        //            departmentList.Add(departmentObj);
        //        }
        //    }

        //    return departmentList;
        //}

        //protected internal bool CheckDepartmentExists(string departmentName)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("SELECT ");
        //    sqlCommand.Append("department_uuid ");
        //    sqlCommand.Append("FROM ");
        //    sqlCommand.Append("mis_departments ");
        //    sqlCommand.Append("WHERE ");
        //    sqlCommand.Append("department_name = :departmentName ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter arParams = new NpgsqlParameter();

        //    arParams = new NpgsqlParameter("departmentName", NpgsqlTypes.NpgsqlDbType.Text);
        //    arParams.Direction = ParameterDirection.Input;
        //    arParams.Value = departmentName.ToUpper();

        //    bool divisonExists = false;

        //    using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
        //    {
        //        while (reader.Read())
        //        {
        //            divisonExists = true;
        //        }
        //    }

        //    return divisonExists;
        //}

        //protected internal int AddDepartment(Guid departmentGuid, string departmentName)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("INSERT INTO ");
        //    sqlCommand.Append("mis_departments ");
        //    sqlCommand.Append("( ");
        //    sqlCommand.Append("department_uuid, ");
        //    sqlCommand.Append("department_name ");
        //    sqlCommand.Append(") ");
        //    sqlCommand.Append("VALUES ");
        //    sqlCommand.Append("( ");
        //    sqlCommand.Append(":departmentGuid, ");
        //    sqlCommand.Append(":departmentName ");
        //    sqlCommand.Append(") ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter[] arParams = new NpgsqlParameter[2];

        //    arParams[0] = new NpgsqlParameter("departmentGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[0].Direction = ParameterDirection.Input;
        //    arParams[0].Value = departmentGuid;

        //    arParams[1] = new NpgsqlParameter("departmentName", NpgsqlTypes.NpgsqlDbType.Text);
        //    arParams[1].Direction = ParameterDirection.Input;
        //    arParams[1].Value = departmentName.ToUpper();

        //    return NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);
        //}

        //protected internal int UpdateDepartment(Guid departmentGuid, string newDepartmentName)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("UPDATE ");
        //    sqlCommand.Append("mis_departments ");
        //    sqlCommand.Append("SET ");
        //    sqlCommand.Append("department_name = :newDepartmentName ");
        //    sqlCommand.Append("WHERE ");
        //    sqlCommand.Append("department_uuid = :departmentGuid ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter[] arParams = new NpgsqlParameter[2];

        //    arParams[0] = new NpgsqlParameter("newDepartmentName", NpgsqlTypes.NpgsqlDbType.Text);
        //    arParams[0].Direction = ParameterDirection.Input;
        //    arParams[0].Value = newDepartmentName.ToUpper();

        //    arParams[1] = new NpgsqlParameter("departmentGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[1].Direction = ParameterDirection.Input;
        //    arParams[1].Value = departmentGuid;

        //    return NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);
        //}

        //protected internal int DeleteDepartment(Guid departmentGuid)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("DELETE ");
        //    sqlCommand.Append("FROM ");
        //    sqlCommand.Append("mis_departments ");
        //    sqlCommand.Append("WHERE ");
        //    sqlCommand.Append("department_uuid = :departmentGuid ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter arParams = new NpgsqlParameter();

        //    arParams = new NpgsqlParameter("departmentGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams.Direction = ParameterDirection.Input;
        //    arParams.Value = departmentGuid;

        //    return NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);
        //}
    }
}