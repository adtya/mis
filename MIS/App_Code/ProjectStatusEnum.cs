﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIS.App_Code
{
    public enum ProjectStatusEnum
    {
        Saved_Partially = 1,

        Created = 2,

        Started = 3,

        Inactive = 4,

        Dropped = 5,

        Halted = 6,

        Completed = 7
    }
}
