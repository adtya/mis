﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineIntensityFactorManager
    {
        #region Private Members

        private Guid routineIntensityFactorGuid;    
        private int routineIntensityFactor;
        private string routineIntensityFactorJustification;
        private AssetManager assetManager;
        private RoutineWorkItemManager routineWorkItemManager;

        #endregion


        #region Public Getter/Setter Properties

        public Guid RoutineIntensityFactorGuid
        {
            get
            {
                return routineIntensityFactorGuid;
            }
            set
            {
                routineIntensityFactorGuid = value;
            }
        }

        public int RoutineIntensityFactor
        {
            get
            {
                return routineIntensityFactor;
            }
            set
            {
                routineIntensityFactor = value;
            }
        }

        public string RoutineIntensityFactorJustification
        {
            get
            {
                return routineIntensityFactorJustification;
            }
            set
            {
                routineIntensityFactorJustification = value;
            }
        }

        public AssetManager AssetManager
        {
            get
            {
                if (assetManager == null)
                {
                    assetManager = new AssetManager();
                }
                return assetManager;
            }
            set
            {
                assetManager = value;
            }
        }

        public RoutineWorkItemManager RoutineWorkItemManager
        {
            get
            {
                if (routineWorkItemManager == null)
                {
                    routineWorkItemManager = new RoutineWorkItemManager();
                }
                return routineWorkItemManager;
            }
            set
            {
                routineWorkItemManager = value;
            }
        }

        #endregion
    }
}