﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SystemVariablesManager
    {
        #region Private Members

        private Guid systemVariablesGuid;
        private string fiscalYear;
        private DateTime currentMonitoringDate;
        private int quarterNumber;
        private CircleManager circleManager;

        #endregion


        #region Public Getter/Setter Properties

        public Guid SystemVariablesGuid
        {
            get
            {
                return systemVariablesGuid;
            }
            set
            {
                systemVariablesGuid = value;
            }
        }

        public string FiscalYear
        {
            get
            {
                return fiscalYear;
            }
            set
            {
                fiscalYear = value;
            }
        }       

        public DateTime CurrentMonitoringDate
        {
            get
            {
                return currentMonitoringDate;
            }
            set
            {
                currentMonitoringDate = value;
            }
        }

        public int QuarterNumber
        {
            get
            {
                return quarterNumber;
            }
            set
            {
                quarterNumber = value;
            }
        }

        public CircleManager CircleManager
        {
            get
            {
                if (circleManager == null)
                {
                    circleManager = new CircleManager();
                }
                return circleManager;
            }
            set
            {
                circleManager = value;
            }
        }

        #endregion
    }
}