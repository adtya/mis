﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class GateConstructionMaterialDB
    {
        protected internal List<GateConstructionMaterialManager> GetGateConstructionMaterialList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("gate_construction_material_uuid, ");
            sqlCommand.Append("gate_construction_material_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_gate_construction_material ");
            sqlCommand.Append("ORDER BY gate_construction_material_name ");
            sqlCommand.Append(";");

            List<GateConstructionMaterialManager> gateConstructionMaterialManagerList = new List<GateConstructionMaterialManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    GateConstructionMaterialManager gateConstructionMaterialManagerObj = new GateConstructionMaterialManager();

                    gateConstructionMaterialManagerObj.GateConstructionMaterialGuid = new Guid(reader["gate_construction_material_uuid"].ToString());
                    gateConstructionMaterialManagerObj.GateConstructionMaterialName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["gate_construction_material_name"].ToString());

                    gateConstructionMaterialManagerList.Add(gateConstructionMaterialManagerObj);
                }
            }

            return gateConstructionMaterialManagerList;
        }
    }
}