﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class TurnoutGateTypeManager
    {
        #region Private Members

        private Guid turnoutGateTypeGuid;
        private string turnoutGateTypeName;
       
       #endregion


        #region Public Constructors

        public TurnoutGateTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid TurnoutGateTypeGuid
        {
            get
            {
                return turnoutGateTypeGuid;
            }
            set
            {
                turnoutGateTypeGuid = value;
            }
        }

        public string TurnoutGateTypeName
        {
            get
            {
                return turnoutGateTypeName;
            }
            set
            {
                turnoutGateTypeName = value;
            }
        }   

        #endregion
    }
}