﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class DropStructureDB
    {
        protected internal int AddDropStructureData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_drop_structure ");
            sqlCommand.Append("( ");
            sqlCommand.Append("drop_structure_uuid, ");
            sqlCommand.Append("us_invert_level, ");
            sqlCommand.Append("ds_invert_level, ");
            sqlCommand.Append("stilling_basin_length, ");
            sqlCommand.Append("stilling_basin_width, ");           
            sqlCommand.Append("asset_uuid ");            
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":dropStructureGuid, ");
            sqlCommand.Append(":usInvertLevel, ");
            sqlCommand.Append(":dsInvertLevel, ");
            sqlCommand.Append(":stillingBasinLength, ");
            sqlCommand.Append(":stillingBasinWidth, ");           
            sqlCommand.Append(":assetGuid ");            
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[6];

            arParams[0] = new NpgsqlParameter("dropStructureGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("usInvertLevel", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.DropStructureManager.UsInvertLevel;

            arParams[2] = new NpgsqlParameter("dsInvertLevel", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.DropStructureManager.DsInvertLevel;

            arParams[3] = new NpgsqlParameter("stillingBasinLength", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.DropStructureManager.StillingBasinLength;

            arParams[4] = new NpgsqlParameter("stillingBasinWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.DropStructureManager.StillingBasinWidth;

            arParams[5] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.AssetGuid;          

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateDropStructureData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_drop_structure ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("us_invert_level= :usInvertLevel, ");
            sqlCommand.Append("ds_invert_level= :dsInvertLevel, ");
            sqlCommand.Append("stilling_basin_length= :stillingBasinLength, ");
            sqlCommand.Append("stilling_basin_width= :stillingBasinWidth ");           
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[6];

            arParams[0] = new NpgsqlParameter("dropStructureGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.DropStructureManager.DropStructureGuid;

            arParams[1] = new NpgsqlParameter("usInvertLevel", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.DropStructureManager.UsInvertLevel;

            arParams[2] = new NpgsqlParameter("dsInvertLevel", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.DropStructureManager.DsInvertLevel;

            arParams[3] = new NpgsqlParameter("stillingBasinLength", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.DropStructureManager.StillingBasinLength;

            arParams[4] = new NpgsqlParameter("stillingBasinWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.DropStructureManager.StillingBasinWidth;

            arParams[5] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.AssetGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}