﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SubChainageWorkItemRelationManager
    {
        private Guid subChainageWorkItemRelationGuid;
        private SubChainageManager subChainageManager;
        private WorkItemManager workItemManager;
        private ProjectManager projectManager;

        public Guid SubChainageWorkItemRelationGuid
        {
            get
            {
                return subChainageWorkItemRelationGuid;
            }
            set
            {
                subChainageWorkItemRelationGuid = value;
            }
        }

        public SubChainageManager SubChainageManager
        {
            get
            {
                if (subChainageManager == null)
                {
                    subChainageManager = new SubChainageManager();
                }
                return subChainageManager;
            }
            set
            {
                subChainageManager = value;
            }
        }

        public WorkItemManager WorkItemManager
        {
            get
            {
                if (workItemManager == null)
                {
                    workItemManager = new WorkItemManager();
                }
                return workItemManager;
            }
            set
            {
                workItemManager = value;
            }
        }

        public ProjectManager ProjectManager
        {
            get
            {
                if (projectManager == null)
                {
                    projectManager = new ProjectManager();
                }
                return projectManager;
            }
            set
            {
                projectManager = value;
            }
        }

    }
}