﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class TurnoutTypeDB
    {
        protected internal List<TurnoutTypeManager> GetTurnoutTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("turnout_type_uuid, ");
            sqlCommand.Append("turnout_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_turnout_type ");
            sqlCommand.Append("ORDER BY turnout_type_name ");
            sqlCommand.Append(";");

            List<TurnoutTypeManager> turnoutTypeManagerList = new List<TurnoutTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    TurnoutTypeManager turnoutTypeManagerObj = new TurnoutTypeManager();

                    turnoutTypeManagerObj.TurnoutTypeGuid = new Guid(reader["turnout_type_uuid"].ToString());
                    turnoutTypeManagerObj.TurnoutTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["turnout_type_name"].ToString());

                    turnoutTypeManagerList.Add(turnoutTypeManagerObj);
                }
            }

            return turnoutTypeManagerList;
        }
    }
}