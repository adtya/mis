﻿using NodaMoney;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RepairAnalysisDB
    {
        protected internal List<RepairAnalysisManager> GetRepairAnalysisData()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT DISTINCT ");
            sqlCommand.Append("obj1.*, ");
            sqlCommand.Append("obj2.probablity_failure, ");
            sqlCommand.Append("obj2.probablity_failure_number, ");
            sqlCommand.Append("obj3.current_asset_functionality, ");
            sqlCommand.Append("obj3.current_asset_functionality_number, ");
            sqlCommand.Append("obj4.repair_analysis_status, ");
            sqlCommand.Append("obj5.asset_code, ");
            sqlCommand.Append("obj6.asset_risk_rating_number, ");
            sqlCommand.Append("obj7.scheme_risk_rating_number ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_repair_analysis_details obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_repair_analyis_probablity_failure obj2 ON obj1.probablity_failure_uuid=obj2.probablity_failure_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_repair_analysis_current_asset_functionality obj3 ON obj1.current_asset_functionality_uuid=obj3.current_asset_functionality_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_repair_analysis_status obj4 ON obj1.repair_analysis_status_uuid=obj4.repair_analysis_status_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_assets obj5 ON obj1.asset_uuid=obj5.asset_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_asset_code_asset_risk_rating_relationship obj8 ON obj5.asset_uuid = obj8.asset_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_asset_risk_rating obj6 ON obj6.asset_risk_rating_uuid = obj8.asset_risk_rating_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_scheme_code_scheme_risk_rating_relationship obj9 ON obj5.scheme_uuid = obj9.scheme_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_scheme_risk_rating obj7 ON obj7.scheme_risk_rating_uuid = obj9.scheme_risk_rating_uuid ");
            sqlCommand.Append(";");

            List<RepairAnalysisManager> repairAnalysisManagerList = new List<RepairAnalysisManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RepairAnalysisManager repairAnalysisManagerObj = new RepairAnalysisManager();

                    repairAnalysisManagerObj.RepairAnalysisGuid = new Guid(reader["repair_analysis_uuid"].ToString());

                    repairAnalysisManagerObj.RepairIntensityFactor = Convert.ToInt16(reader["repair_intensity_factor"].ToString());
                    repairAnalysisManagerObj.Total = Convert.ToInt16(reader["total"].ToString());                    
                    repairAnalysisManagerObj.RepairCost = string.IsNullOrEmpty(reader["repair_cost"].ToString()) ? Money.Parse("0") : Money.Parse(reader["repair_cost"].ToString(), Currency.FromCode("INR", "ISO-4217"));
                    repairAnalysisManagerObj.SubmissionDate = DateTime.Parse(reader["submission_date"].ToString());
                    repairAnalysisManagerObj.IntensityFactorJustification = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["intensity_factor_justification"].ToString());
                    repairAnalysisManagerObj.FailureModeDescription = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["failure_mode_description"].ToString());
                    repairAnalysisManagerObj.RepairDescription = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["repair_description"].ToString());
                    repairAnalysisManagerObj.DamageDescription = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["damage_description"].ToString());

                    repairAnalysisManagerObj.ProbabilityFailureManager.ProbabilityFailureGuid = new Guid(reader["probablity_failure_uuid"].ToString());
                    repairAnalysisManagerObj.ProbabilityFailureManager.ProbabilityFailure = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["probablity_failure"].ToString());
                    repairAnalysisManagerObj.ProbabilityFailureManager.ProbabilityFailureNumber = Convert.ToInt16(reader["probablity_failure_number"].ToString());

                    repairAnalysisManagerObj.CurrentAssetFunctionalityManager.CurrentAssetFunctionalityGuid = new Guid(reader["current_asset_functionality_uuid"].ToString());
                    repairAnalysisManagerObj.CurrentAssetFunctionalityManager.CurrentAssetFunctionality = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["current_asset_functionality"].ToString());
                    repairAnalysisManagerObj.CurrentAssetFunctionalityManager.CurrentAssetFunctionalityNumber = Convert.ToInt16(reader["current_asset_functionality_number"].ToString());

                    repairAnalysisManagerObj.RepairAnalysisStatusManger.RepairAnalysisStatusGuid = new Guid(reader["repair_analysis_status_uuid"].ToString());
                    repairAnalysisManagerObj.RepairAnalysisStatusManger.RepairAnalysisStatus = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["repair_analysis_status"].ToString());

                    repairAnalysisManagerObj.AssetManager.AssetGuid = new Guid(reader["asset_uuid"].ToString());
                    repairAnalysisManagerObj.AssetManager.AssetCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_code"].ToString());


                    repairAnalysisManagerObj.SystemVariablesManager.FiscalYear = reader["fiscal_year"].ToString();

                    repairAnalysisManagerObj.AssetCodeAndAssetRiskRatingRelationManager.AssetRiskRatingManager.AssetRiskRatingNumber = string.IsNullOrEmpty(reader["asset_risk_rating_number"].ToString()) ? 0 : Convert.ToInt16(reader["asset_risk_rating_number"].ToString());

                    repairAnalysisManagerObj.SchemeCodeAndSchemeRiskRatingRelationManager.SchemeRiskRatingManager.SchemeRiskRatingNumber = string.IsNullOrEmpty(reader["scheme_risk_rating_number"].ToString()) ? 0 : Convert.ToInt16(reader["scheme_risk_rating_number"].ToString());

                    repairAnalysisManagerList.Add(repairAnalysisManagerObj);
                }
            }
            return repairAnalysisManagerList;
        }

        protected internal int SaveRepairAnalysisData(RepairAnalysisManager repairAnalysisManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_repair_analysis_details ");
            sqlCommand.Append("( ");            
            sqlCommand.Append("repair_analysis_uuid, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("probablity_failure_uuid, ");
            sqlCommand.Append("current_asset_functionality_uuid, ");
            sqlCommand.Append("repair_intensity_factor, ");
            sqlCommand.Append("total, ");
            sqlCommand.Append("repair_cost, ");
            sqlCommand.Append("submission_date, ");
            sqlCommand.Append("repair_analysis_status_uuid, ");
            sqlCommand.Append("intensity_factor_justification, ");
            sqlCommand.Append("failure_mode_description, ");
            sqlCommand.Append("repair_description, ");
            sqlCommand.Append("damage_description, ");
            sqlCommand.Append("fiscal_year ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":repairAnalysisGuid, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":probabilityFailureGuid, ");
            sqlCommand.Append(":currentAssetFunctionalityGuid, ");
            sqlCommand.Append(":repairIntensityFactor, ");
            sqlCommand.Append(":total, ");
            sqlCommand.Append(":repairCost, ");
            sqlCommand.Append(":submissionDate, ");
            sqlCommand.Append(":repairAnalysisStatusGuid, ");
            sqlCommand.Append(":intensityFactorJustification, ");
            sqlCommand.Append(":failureModeDescription, ");
            sqlCommand.Append(":repairDescription, ");
            sqlCommand.Append(":damageDescription, ");
            sqlCommand.Append(":fiscalYear ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[14];

            arParams[0] = new NpgsqlParameter("repairAnalysisGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = repairAnalysisManager.RepairAnalysisGuid; //assetGuid;

            arParams[1] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = repairAnalysisManager.AssetManager.AssetGuid;

            arParams[2] = new NpgsqlParameter("probabilityFailureGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = repairAnalysisManager.ProbabilityFailureManager.ProbabilityFailureGuid;

            arParams[3] = new NpgsqlParameter("currentAssetFunctionalityGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = repairAnalysisManager.CurrentAssetFunctionalityManager.CurrentAssetFunctionalityGuid;

            arParams[4] = new NpgsqlParameter("repairIntensityFactor", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = repairAnalysisManager.RepairIntensityFactor;

            arParams[5] = new NpgsqlParameter("total", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = repairAnalysisManager.Total;

            arParams[6] = new NpgsqlParameter("repairCost", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = repairAnalysisManager.RepairCost.Amount;

            arParams[7] = new NpgsqlParameter("submissionDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = repairAnalysisManager.SubmissionDate;

            arParams[8] = new NpgsqlParameter("repairAnalysisStatusGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = repairAnalysisManager.RepairAnalysisStatusManger.RepairAnalysisStatusGuid;

            arParams[9] = new NpgsqlParameter("intensityFactorJustification", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = repairAnalysisManager.IntensityFactorJustification;

            arParams[10] = new NpgsqlParameter("failureModeDescription", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[10].Direction = ParameterDirection.Input;
            arParams[10].Value = repairAnalysisManager.FailureModeDescription;

            arParams[11] = new NpgsqlParameter("repairDescription", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[11].Direction = ParameterDirection.Input;
            arParams[11].Value = repairAnalysisManager.RepairDescription;

            arParams[12] = new NpgsqlParameter("damageDescription", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[12].Direction = ParameterDirection.Input;
            arParams[12].Value = repairAnalysisManager.DamageDescription;

            arParams[13] = new NpgsqlParameter("fiscalYear", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[13].Direction = ParameterDirection.Input;
            arParams[13].Value = repairAnalysisManager.SystemVariablesManager.FiscalYear;          

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateRepairAnalysisData(Guid repairAnalysisGuid, RepairAnalysisManager repairAnalysisManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_om_repair_analysis_details ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("probablity_failure_uuid= :probabilityFailureGuid, ");
            sqlCommand.Append("current_asset_functionality_uuid=:currentAssetFunctionalityGuid, ");
            sqlCommand.Append("repair_intensity_factor= :repairIntensityFactor, ");
            sqlCommand.Append("total= :total, ");
            sqlCommand.Append("repair_cost= :repairCost, ");
            sqlCommand.Append("submission_date=:submissionDate, ");
            sqlCommand.Append("repair_analysis_status_uuid= :repairAnalysisStatusGuid, ");
            sqlCommand.Append("intensity_factor_justification= :intensityFactorJustification, ");
            sqlCommand.Append("failure_mode_description= :failureModeDescription, ");
            sqlCommand.Append("repair_description= :repairDescription, ");
            sqlCommand.Append("damage_description= :damageDescription ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("repair_analysis_uuid= :repairAnalysisGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[12];

            arParams[0] = new NpgsqlParameter("repairAnalysisGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = repairAnalysisGuid;

            arParams[1] = new NpgsqlParameter("probabilityFailureGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = repairAnalysisManager.ProbabilityFailureManager.ProbabilityFailureGuid;

            arParams[2] = new NpgsqlParameter("currentAssetFunctionalityGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = repairAnalysisManager.CurrentAssetFunctionalityManager.CurrentAssetFunctionalityGuid;

            arParams[3] = new NpgsqlParameter("repairIntensityFactor", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = repairAnalysisManager.RepairIntensityFactor;

            arParams[4] = new NpgsqlParameter("total", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = repairAnalysisManager.Total;

            arParams[5] = new NpgsqlParameter("repairCost", NpgsqlTypes.NpgsqlDbType.Money);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = repairAnalysisManager.RepairCost.Amount;

            arParams[6] = new NpgsqlParameter("submissionDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = repairAnalysisManager.SubmissionDate;

            arParams[7] = new NpgsqlParameter("repairAnalysisStatusGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = repairAnalysisManager.RepairAnalysisStatusManger.RepairAnalysisStatusGuid;

            arParams[8] = new NpgsqlParameter("intensityFactorJustification", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = repairAnalysisManager.IntensityFactorJustification;

            arParams[9] = new NpgsqlParameter("failureModeDescription", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = repairAnalysisManager.FailureModeDescription;

            arParams[10] = new NpgsqlParameter("repairDescription", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[10].Direction = ParameterDirection.Input;
            arParams[10].Value = repairAnalysisManager.RepairDescription;

            arParams[11] = new NpgsqlParameter("damageDescription", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[11].Direction = ParameterDirection.Input;
            arParams[11].Value = repairAnalysisManager.DamageDescription;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int DeleteRepairAnalysisData(Guid repairAnalysisGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_repair_analysis_details ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("repair_analysis_uuid = :repairAnalysisGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("repairAnalysisGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = repairAnalysisGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}