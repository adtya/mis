﻿using NodaMoney;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SubChainageManager
    {
        private Guid subChainageGuid;
        private string subChainageName;
        private Guid subChainageSioGuid;
        private Money subChainageValue;
        private decimal subChainageValueInput;
        private string administrativeApprovalReference;
        private string technicalSanctionReference;
        private string contractorName;
        private string workOrderReference;
        private DateTime subChainageStartingDate;
        private DateTime subChainageExpectedCompletionDate;
        private DateTime subChainageActualCompletionDate;
        private string typeOfWorks;
        private short subChainageStatus;
        private ProjectManager projectManager;
        private UserManager userManager;
        private List<WorkItemManager> workItemManager;
        private List<SubChainagePhysicalProgressManager> subChainagePhysicalProgressManager;

        public Guid SubChainageGuid
        {
            get
            {
                return subChainageGuid;
            }
            set
            {
                subChainageGuid = value;
            }
        }

        public string SubChainageName
        {
            get
            {
                return subChainageName;
            }
            set
            {
                subChainageName = value;
            }
        }

        public Guid SubChainageSioGuid
        {
            get 
            { 
                return subChainageSioGuid; 
            }
            set
            { 
                subChainageSioGuid = value; 
            }
        }

        public Money SubChainageValue
        {
            get
            {
                if (subChainageValue.Amount.Equals(0))
                {
                    subChainageValue = SubChainageValueInput;
                }
                return subChainageValue;
            }
            set
            {
                subChainageValue = value;
            }
        }

        public decimal SubChainageValueInput
        {
            private get
            {
                return subChainageValueInput;
            }
            set
            {
                subChainageValueInput = value;
            }
        }

        public string AdministrativeApprovalReference
        {
            get
            {
                return administrativeApprovalReference;
            }
            set
            {
                administrativeApprovalReference = value;
            }
        }

        public string TechnicalSanctionReference
        {
            get
            {
                return technicalSanctionReference;
            }
            set
            {
                technicalSanctionReference = value;
            }
        }

        public string ContractorName
        {
            get
            {
                return contractorName;
            }
            set
            {
                contractorName = value;
            }
        }

        public string WorkOrderReference
        {
            get
            {
                return workOrderReference;
            }
            set
            {
                workOrderReference = value;
            }
        }

        public DateTime SubChainageStartingDate
        {
            get
            {
                return subChainageStartingDate;
            }
            set
            {
                subChainageStartingDate = value;
            }
        }

        public DateTime SubChainageExpectedCompletionDate
        {
            get
            {
                return subChainageExpectedCompletionDate;
            }
            set
            {
                subChainageExpectedCompletionDate = value;
            }
        }

        public DateTime SubChainageActualCompletionDate
        {
            get
            {
                return subChainageActualCompletionDate;
            }
            set
            {
                subChainageActualCompletionDate = value;
            }
        }

        public string TypeOfWorks
        {
            get
            {
                return typeOfWorks;
            }
            set
            {
                typeOfWorks = value;
            }
        }

        public short SubChainageStatus
        {
            get
            {
                return subChainageStatus;
            }
            set
            {
                subChainageStatus = value;
            }
        }

        public ProjectManager ProjectManager
        {
            get
            {
                if (projectManager == null)
                {
                    projectManager = new ProjectManager();
                }
                return projectManager;
            }
            set
            {
                projectManager = value;
            }
        }

        public UserManager UserManager
        {
            get
            {
                if (userManager == null)
                {
                    userManager = new UserManager();
                }
                return userManager;
            }
            set
            {
                userManager = value;
            }
        }

        public List<WorkItemManager> WorkItemManager
        {
            get
            {
                if (workItemManager == null)
                {
                    workItemManager = new List<WorkItemManager>();
                }
                return workItemManager;
            }
            set
            {
                workItemManager = value;
            }
        }

        public List<SubChainagePhysicalProgressManager> SubChainagePhysicalProgressManager
        {
            get
            {
                if (subChainagePhysicalProgressManager == null)
                {
                    subChainagePhysicalProgressManager = new List<SubChainagePhysicalProgressManager>();
                }
                return subChainagePhysicalProgressManager;
            }
            set
            {
                subChainagePhysicalProgressManager = value;
            }
        }

    }
}