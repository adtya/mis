﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class FremaaOfficerProjectRelationshipManager
    {
        private Guid fremaaOfficerProjectRelationshipGuid;
        private ProjectManager projectManager;
        private string fremaaOfficerName;

        public Guid FremaaOfficerProjectRelationshipGuid
        {
            get
            {
                return fremaaOfficerProjectRelationshipGuid;
            }
            set
            {
                fremaaOfficerProjectRelationshipGuid = value;
            }
        }

        public ProjectManager ProjectManager
        {
            get
            {
                if (projectManager == null)
                {
                    projectManager = new ProjectManager();
                }
                return projectManager;
            }
            set
            {
                projectManager = value;
            }
        }

        public string FremaaOfficerName
        {
            get
            {
                return fremaaOfficerName;
            }
            set
            {
                fremaaOfficerName = value;
            }
        }
    }
}