﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RolesDB
    {
        protected internal List<RolesManager> GetRoles()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_user_roles ");            
            sqlCommand.Append("ORDER BY role_name ");
            sqlCommand.Append(";");
           
            List<RolesManager> roleList = new List<RolesManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RolesManager roleObj = new RolesManager();

                    roleObj.RoleGuid = new Guid(reader["role_uuid"].ToString());
                    roleObj.RoleName = reader["role_name"].ToString();

                    roleList.Add(roleObj);
                }
            }

            return roleList;
        }
    }
}