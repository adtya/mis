﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class WorkItemAndWorkItemSubHeadsRelationManager
    {
        private Guid workItemWorkItemSubHeadRelationGuid;
        private WorkItemManager workItemManager;
        private WorkItemSubHeadManager workItemSubHeadManager;

        public Guid WorkItemWorkItemSubHeadRelationGuid
        {
            get
            {
                return workItemWorkItemSubHeadRelationGuid;
            }
            set
            {
                workItemWorkItemSubHeadRelationGuid = value;
            }
        }

        public WorkItemManager WorkItemManager
        {
            get
            {
                if (workItemManager == null)
                {
                    workItemManager = new WorkItemManager();
                }
                return workItemManager;
            }
            set
            {
                workItemManager = value;
            }
        }

        public WorkItemSubHeadManager WorkItemSubHeadManager
        {
            get
            {
                if (workItemSubHeadManager == null)
                {
                    workItemSubHeadManager = new WorkItemSubHeadManager();
                }
                return workItemSubHeadManager;
            }
            set
            {
                workItemSubHeadManager = value;
            }
        }
    }
}