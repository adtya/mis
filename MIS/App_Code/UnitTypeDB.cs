﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class UnitTypeDB
    {
        protected internal UnitTypeManager GetUnitType(Guid unitTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.unit_type_uuid, ");
            sqlCommand.Append("obj1.unit_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_unit_types obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.unit_type_uuid = :unitTypeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("unitTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = unitTypeGuid;

            UnitTypeManager unitTypeManagerObj = new UnitTypeManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    unitTypeManagerObj.UnitTypeGuid = new Guid(reader["unit_type_uuid"].ToString());
                    unitTypeManagerObj.UnitTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_type_name"].ToString());
                }
            }

            return unitTypeManagerObj;

        }

        protected internal UnitTypeManager GetUnitTypeWithUnits(Guid unitTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.unit_type_uuid, ");
            sqlCommand.Append("obj1.unit_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_unit_types obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.unit_type_uuid = :unitTypeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("unitTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = unitTypeGuid;

            UnitTypeManager unitTypeManagerObj = new UnitTypeManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    unitTypeManagerObj.UnitTypeGuid = new Guid(reader["unit_type_uuid"].ToString());
                    unitTypeManagerObj.UnitTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_type_name"].ToString());

                    unitTypeManagerObj.UnitManagerList = new UnitDB().GetUnitsBasedOnUnitType(unitTypeManagerObj.UnitTypeGuid);
                }
            }

            return unitTypeManagerObj;

        }


        protected internal List<UnitTypeManager> GetUnitTypes()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.unit_type_uuid, ");
            sqlCommand.Append("obj1.unit_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_unit_types obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.unit_type_name ");
            sqlCommand.Append(";");

            List<UnitTypeManager> unitTypeManagerList = new List<UnitTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    UnitTypeManager unitTypeManagerObj = new UnitTypeManager();

                    unitTypeManagerObj.UnitTypeGuid = new Guid(reader["unit_type_uuid"].ToString());
                    unitTypeManagerObj.UnitTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_type_name"].ToString());

                    unitTypeManagerList.Add(unitTypeManagerObj);
                }
            }

            return unitTypeManagerList;

        }

        protected internal List<UnitTypeManager> GetUnitTypesWithUnits()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.unit_type_uuid, ");
            sqlCommand.Append("obj1.unit_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_unit_types obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.unit_type_name ");
            sqlCommand.Append(";");

            List<UnitTypeManager> unitTypeManagerList = new List<UnitTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    UnitTypeManager unitTypeManagerObj = new UnitTypeManager();

                    unitTypeManagerObj.UnitTypeGuid = new Guid(reader["unit_type_uuid"].ToString());
                    unitTypeManagerObj.UnitTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["unit_type_name"].ToString());

                    unitTypeManagerObj.UnitManagerList = new UnitDB().GetUnitsBasedOnUnitType(unitTypeManagerObj.UnitTypeGuid);

                    unitTypeManagerList.Add(unitTypeManagerObj);
                }
            }

            return unitTypeManagerList;

        }


        protected internal bool CheckUnitTypeExistence(string unitTypeName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_unit_types obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.unit_type_name = :unitTypeName ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("unitTypeName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(unitTypeName);

            bool result = false;

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    result = true;
                }
            }

            return result;
        }


        protected internal int AddUnitType(Guid unitTypeGuid, string unitTypeName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_unit_types ");
            sqlCommand.Append("( ");
            sqlCommand.Append("unit_type_uuid, ");
            sqlCommand.Append("unit_type_name ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":unitTypeGuid, ");
            sqlCommand.Append(":unitTypeName ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("unitTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = unitTypeGuid;

            arParams[1] = new NpgsqlParameter("unitTypeName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(unitTypeName);

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }


        protected internal int UpdateUnitType(Guid unitTypeGuid, string unitTypeName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_unit_types ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("unit_type_name = :unitTypeName ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("unit_type_uuid = :unitTypeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("unitTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = unitTypeGuid;

            arParams[1] = new NpgsqlParameter("unitTypeName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(unitTypeName);

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }


        protected internal int DeleteUnitType(Guid unitTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_unit_types ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("unit_type_uuid = :unitTypeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("unitTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = unitTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal UnitManager GetUnit(Guid assetQuantityParameterGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            //sqlCommand.Append("obj1.unit_uuid, ");
            sqlCommand.Append("obj1.unit_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_units obj1 ");
            sqlCommand.Append("LEFT JOIN mis_asset_quantity_parameters ");
            sqlCommand.Append("ON mis_asset_quantity_parameters.unit_uuid = obj1.unit_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("mis_asset_quantity_parameters.asset_quantity_parameter_uuid =:assetQuantityParameterGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("assetQuantityParameterGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = assetQuantityParameterGuid;

            UnitManager unitManagerObj = new UnitManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    //assetQuantityParameterObj.UnitManager.UnitGuid = new Guid(reader["unit_uuid"].ToString());
                    unitManagerObj.UnitName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["unit_name"].ToString());
                }
            }

            return unitManagerObj;
        }

    }
}