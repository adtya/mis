﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class WorkItemSubHeadManager
    {
        private Guid workItemSubHeadGuid;
        private string workItemSubHeadName;
        private short workItemSubHeadSequence;

        public Guid WorkItemSubHeadGuid
        {
            get
            {
                return workItemSubHeadGuid;
            }
            set
            {
                workItemSubHeadGuid = value;
            }
        }

        public string WorkItemSubHeadName
        {
            get
            {
                return workItemSubHeadName;
            }
            set
            {
                workItemSubHeadName = value;
            }
        }

        public short WorkItemSubHeadSequence
        {
            get
            {
                return workItemSubHeadSequence;
            }
            set
            {
                workItemSubHeadSequence = value;
            }
        }
    }
}