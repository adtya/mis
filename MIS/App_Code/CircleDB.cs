﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class CircleDB
    {
        protected internal CircleManager GetCircle(Guid circleGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.circle_uuid, ");
            sqlCommand.Append("obj1.circle_code, ");
            sqlCommand.Append("obj1.circle_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_circles obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.circle_uuid = :circleGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = circleGuid;

            CircleManager circleManagerObj = new CircleManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    circleManagerObj.CircleGuid = new Guid(reader["circle_uuid"].ToString());
                    circleManagerObj.CircleCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["circle_code"].ToString());
                    circleManagerObj.CircleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["circle_name"].ToString());
                }
            }

            return circleManagerObj;
        }

        protected internal List<CircleManager> GetCircleWithDivisons(Guid circleGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.circle_uuid, ");
            sqlCommand.Append("obj1.circle_code, ");
            sqlCommand.Append("obj1.circle_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_circles obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.circle_uuid = :circleGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = circleGuid;

            List<CircleManager> circleManagerList = new List<CircleManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    CircleManager circleManagerObj = new CircleManager();

                    circleManagerObj.CircleGuid = new Guid(reader["circle_uuid"].ToString());
                    circleManagerObj.CircleCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["circle_code"].ToString());
                    circleManagerObj.CircleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["circle_name"].ToString());

                    circleManagerObj.DivisionManagerList = new DivisionDB().GetDivisionsBasedOnCircleGuid(circleManagerObj.CircleGuid);

                    circleManagerList.Add(circleManagerObj);
                }
            }

            return circleManagerList;
        }

        protected internal List<CircleManager> GetCircles()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.circle_uuid, ");
            sqlCommand.Append("obj1.circle_code, ");
            sqlCommand.Append("obj1.circle_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_circles obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("circle_code ");
            sqlCommand.Append(";");

            List<CircleManager> circleManagerList = new List<CircleManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    CircleManager circleManagerObj = new CircleManager();

                    circleManagerObj.CircleGuid = new Guid(reader["circle_uuid"].ToString());
                    circleManagerObj.CircleCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["circle_code"].ToString());
                    circleManagerObj.CircleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["circle_name"].ToString());

                    circleManagerList.Add(circleManagerObj);
                }
            }

            return circleManagerList;
        }

        protected internal List<CircleManager> GetCirclesWithDivisons()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.circle_uuid, ");
            sqlCommand.Append("obj1.circle_code, ");
            sqlCommand.Append("obj1.circle_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_circles obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("circle_code ");
            sqlCommand.Append(";");

            List<CircleManager> circleManagerList = new List<CircleManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    CircleManager circleManagerObj = new CircleManager();

                    circleManagerObj.CircleGuid = new Guid(reader["circle_uuid"].ToString());
                    circleManagerObj.CircleCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["circle_code"].ToString());
                    circleManagerObj.CircleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["circle_name"].ToString());

                    circleManagerObj.DivisionManagerList = new DivisionDB().GetDivisionsBasedOnCircleGuid(circleManagerObj.CircleGuid);

                    circleManagerList.Add(circleManagerObj);
                }
            }

            return circleManagerList;
        }


        protected internal int AddCircle(Guid circleGuid, string circleCode, string circleName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_circles ");
            sqlCommand.Append("( ");
            sqlCommand.Append("circle_uuid, ");            
            sqlCommand.Append("circle_code, ");
            sqlCommand.Append("circle_name ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":circleGuid, ");
            sqlCommand.Append(":circleCode, ");
            sqlCommand.Append(":circleName ");           
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = circleGuid;

            arParams[1] = new NpgsqlParameter("circleCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToUpper(circleCode);

            arParams[2] = new NpgsqlParameter("circleName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(circleName);

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }


        protected internal bool UpdateCircle(Guid circleGuid, string circleName, string circleCode)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_circles ");
            sqlCommand.Append("SET ");         
            sqlCommand.Append("circle_name = :circleName, ");
            sqlCommand.Append("circle_code = :circleCode ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("circle_uuid = :circleGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = circleGuid;

            arParams[1] = new NpgsqlParameter("circleName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(circleName);

            arParams[2] = new NpgsqlParameter("circleCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = CultureInfo.CurrentCulture.TextInfo.ToUpper(circleCode);

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return (rowsAffected > 0);
        }

        protected internal int DeleteCircle(Guid circleGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_circles ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("circle_uuid = :circleGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = circleGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }


        protected internal bool CheckCircleExistence(string circleName, string circleCode)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_circles obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.circle_name = :circleName ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.circle_code = :circleCode ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("circleName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(circleName);

            arParams[1] = new NpgsqlParameter("circleCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(circleCode);

            bool result = false;

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    result = true;
                }
            }

            return result;
        }
    }
}