﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SpurConstructionTypeDB
    {
        protected internal List<SpurConstructionTypeManager> GetSpurConstructionTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("construction_type_uuid, ");
            sqlCommand.Append("construction_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_spur_construction_type ");
            sqlCommand.Append("ORDER BY construction_type_name ");
            sqlCommand.Append(";");

            List<SpurConstructionTypeManager> spurConstructionTypeManagerList = new List<SpurConstructionTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    SpurConstructionTypeManager spurConstructionTypeManagerObj = new SpurConstructionTypeManager();

                    spurConstructionTypeManagerObj.ConstructionTypeGuid = new Guid(reader["construction_type_uuid"].ToString());
                    spurConstructionTypeManagerObj.ConstructionTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["construction_type_name"].ToString());

                    spurConstructionTypeManagerList.Add(spurConstructionTypeManagerObj);
                }
            }

            return spurConstructionTypeManagerList;
        }
    }
}