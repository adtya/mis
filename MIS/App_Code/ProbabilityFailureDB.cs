﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class ProbabilityFailureDB
    {
        protected internal List<ProbabilityFailureManager> GetProbabilityFailure()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_repair_analyis_probablity_failure ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("probablity_failure_number ");
            sqlCommand.Append(";");

            List<ProbabilityFailureManager> probabilityFailureManagerList = new List<ProbabilityFailureManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    ProbabilityFailureManager probabilityFailureManagerObj = new ProbabilityFailureManager();

                    probabilityFailureManagerObj.ProbabilityFailureGuid = new Guid(reader["probablity_failure_uuid"].ToString());
                    probabilityFailureManagerObj.ProbabilityFailure = reader["probablity_failure"].ToString();
                    probabilityFailureManagerObj.ProbabilityFailureNumber = Convert.ToInt16(reader["probablity_failure_number"].ToString());

                    probabilityFailureManagerList.Add(probabilityFailureManagerObj);
                }
            }

            return probabilityFailureManagerList;
        }
    }
}