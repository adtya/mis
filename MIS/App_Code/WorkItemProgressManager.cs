﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class WorkItemProgressManager
    {
        private Guid workItemProgressGuid;
        private string workItemProgressValue;
        private string workItemCumulativeValue;
        private string workItemBalanceValue;
        private WorkItemManager workItemManager;

        public Guid WorkItemProgressGuid
        {
            get
            {
                return workItemProgressGuid;
            }
            set
            {
                workItemProgressGuid = value;
            }
        }

        public string WorkItemProgressValue
        {
            get
            {
                return workItemProgressValue;
            }
            set
            {
                workItemProgressValue = value;
            }
        }

        public string WorkItemCumulativeValue
        {
            get
            {
                return workItemCumulativeValue;
            }
            set
            {
                workItemCumulativeValue = value;
            }
        }

        public string WorkItemBalanceValue
        {
            get
            {
                return workItemBalanceValue;
            }
            set
            {
                workItemBalanceValue = value;
            }
        }

        public WorkItemManager WorkItemManager
        {
            get
            {
                if (workItemManager == null)
                {
                    workItemManager = new WorkItemManager();
                }
                return workItemManager;
            }
            set
            {
                workItemManager = value;
            }
        }

    }
}