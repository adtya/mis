﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class CulvertManager
    {
        #region Private Members

        private Guid culvertGuid;
        private int ventNumber;      
        private double ventHeight;        
        private double ventWidth;
        private double lengthCulvert;
        //private AssetManager assetManager;
        private CulvertTypeManager culvertTypeManager;        

       #endregion


        #region Public Constructors

        public CulvertManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid CulvertGuid
        {
            get
            {
                return culvertGuid;
            }
            set
            {
                culvertGuid = value;
            }
        }
        
        public int VentNumber
        {
            get
            {
                return ventNumber;
            }
            set
            {
                ventNumber = value;
            }
        }

        public double VentHeight
        {
            get
            {
                return ventHeight;
            }
            set
            {
                ventHeight = value;
            }
        }

        public double VentWidth
        {
            get
            {
                return ventWidth;
            }
            set
            {
                ventWidth = value;
            }
        } 

        public double LengthCulvert
        {
            get
            {
                return lengthCulvert;
            }
            set
            {
                lengthCulvert = value;
            }
        }

        public CulvertTypeManager CulvertTypeManager
        {
            get
            {
                if (culvertTypeManager == null)
                {
                    culvertTypeManager = new CulvertTypeManager();
                }
                return culvertTypeManager;
            }
            set
            {
                culvertTypeManager = value;
            }
        }   
     
        // public AssetManager AssetManager
        //{
        //    get
        //    {
        //        if (assetManager == null)
        //        {
        //            assetManager = new AssetManager();
        //        }
        //        return assetManager;
        //    }
        //    set
        //    {
        //        assetManager = value;
        //    }
        //}

        #endregion
    }
}