﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class DivisonManager
    {
        #region Private Members

        private Guid _divisonGuid = Guid.Empty;
        private string _divisonName = string.Empty;
        private string _divisonCode = string.Empty;
        private CircleManager circleManager;
        #endregion


        #region Public Constructors

        public DivisonManager()
        {

        }

        public DivisonManager(Guid value)
        {
            _divisonGuid = value;
        }

        public DivisonManager(string value)
        {
            _divisonName = value;
        }       

        public DivisonManager(Guid value1, string value2,string value3)
        {
            _divisonGuid = value1;
            _divisonName = value2;
            _divisonCode = value3;
        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid DivisonGuid
        {
            get
            {
                return _divisonGuid;
            }
            set
            {
                _divisonGuid = value;
            }
        }

        public string DivisonName
        {
            get
            {
                return _divisonName;
            }
            set
            {
                _divisonName = value;
            }
        }

        public string DivisonCode
        {
            get
            {
                return _divisonCode;
            }
            set
            {
                _divisonCode = value;
            }
        }

        public CircleManager CircleManager
        {
            get
            {
                if (circleManager == null)
                {
                    circleManager = new CircleManager();
                }
                return circleManager;
            }
            set
            {
                circleManager = value;
            }
        }
        #endregion
    }
}