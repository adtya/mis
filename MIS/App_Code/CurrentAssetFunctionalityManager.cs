﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class CurrentAssetFunctionalityManager
    {
        #region Private Members

        private Guid currentAssetFunctionalityGuid;
        private string currentAssetFunctionality;
        private int currentAssetFunctionalityNumber;
                
        #endregion

        #region Public Getter/Setter Properties

        public Guid CurrentAssetFunctionalityGuid
        {
            get
            {
                return currentAssetFunctionalityGuid;
            }
            set
            {
                currentAssetFunctionalityGuid = value;
            }
        }

        public string CurrentAssetFunctionality
        {
            get
            {
                return currentAssetFunctionality;
            }
            set
            {
                currentAssetFunctionality = value;
            }
        }

        public int CurrentAssetFunctionalityNumber
        {
            get
            {
                return currentAssetFunctionalityNumber;
            }
            set
            {
                currentAssetFunctionalityNumber = value;
            }
        }
     
        #endregion
    }
}