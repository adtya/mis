﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class GateTypeManager
    {
        #region Private Members

        private Guid gateTypeGuid;
        private string gateTypeName;

        #endregion

        #region Public Getter/Setter Properties

        public Guid GateTypeGuid
        {
            get
            {
                return gateTypeGuid;
            }
            set
            {
                gateTypeGuid = value;
            }
        }

        public string GateTypeName
        {
            get
            {
                return gateTypeName;
            }
            set
            {
                gateTypeName = value;
            }
        }

        #endregion
    }
}