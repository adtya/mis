﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class ProjectAndFremaaOfficersRelationDB
    {
        protected internal List<ProjectAndFremaaOfficersRelationManager> GetFremaaOfficersListRelatedToProject(Guid projectGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_project_and_fremaa_officers_relation obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = projectGuid;

            List<ProjectAndFremaaOfficersRelationManager> projectAndFremaaOfficersRelationManagerList = new List<ProjectAndFremaaOfficersRelationManager>();

            using (NpgsqlDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    ProjectAndFremaaOfficersRelationManager projectAndFremaaOfficersRelationManagerObj = new ProjectAndFremaaOfficersRelationManager();

                    projectAndFremaaOfficersRelationManagerObj.FremaaOfficerProjectRelationshipGuid = new Guid(reader["project_and_fremaa_officers_relation_uuid"].ToString());
                    projectAndFremaaOfficersRelationManagerObj.FremaaOfficerName = reader["fremaa_officer_engaged_name"].ToString();

                    projectAndFremaaOfficersRelationManagerList.Add(projectAndFremaaOfficersRelationManagerObj);
                }
            }
            return projectAndFremaaOfficersRelationManagerList;
        }

        protected internal int AddProjectAndFremaaOfficersRelation(ProjectManager projectManager)
        {
            int rowsAffected = 0;

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_project_and_fremaa_officers_relation ");
            sqlCommand.Append("( ");
            sqlCommand.Append("project_and_fremaa_officers_relation_uuid, ");
            sqlCommand.Append("project_uuid, ");
            sqlCommand.Append("fremaa_officer_engaged_name ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            if (projectManager.OfficersEngaged.Count > 0)
            {
                foreach (ProjectAndFremaaOfficersRelationManager projectAndFremaaOfficersRelationManagerObj in projectManager.OfficersEngaged)
                {
                    int index = projectManager.OfficersEngaged.IndexOf(projectAndFremaaOfficersRelationManagerObj);

                    //int index = Array.IndexOf(workItemSubHeadsGuidList, workItemSubHeadsGuidObj);

                    sqlCommand.Append("( ");

                    sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                    sqlCommand.Append("'" + projectManager.ProjectGuid + "', ");
                    sqlCommand.Append("'" + projectAndFremaaOfficersRelationManagerObj.FremaaOfficerName + "' ");

                    if (index == projectManager.OfficersEngaged.Count - 1)
                    {
                        sqlCommand.Append(") ");
                    }
                    else
                    {
                        sqlCommand.Append("), ");
                    }
                }

                sqlCommand.Append(";");

                rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());
            }

            return rowsAffected;

        }

        protected internal int AddProjectAndFremaaOfficersRelation1(ProjectAndFremaaOfficersRelationManager projectAndFremaaOfficersRelationManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_project_and_fremaa_officers_relation ");
            sqlCommand.Append("( ");
            sqlCommand.Append("project_and_fremaa_officers_relation_uuid, ");
            sqlCommand.Append("project_uuid, ");
            sqlCommand.Append("fremaa_officer_engaged_name ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":fremaaOfficerProjectRelationshipGuid, ");
            sqlCommand.Append(":projectGuid, ");
            sqlCommand.Append(":fremaaOfficerName ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("fremaaOfficerProjectRelationshipGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = projectAndFremaaOfficersRelationManager.FremaaOfficerProjectRelationshipGuid;

            arParams[1] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = projectAndFremaaOfficersRelationManager.ProjectManager.ProjectGuid;

            arParams[2] = new NpgsqlParameter("fremaaOfficerName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = projectAndFremaaOfficersRelationManager.FremaaOfficerName;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;

        }

        protected internal int UpdateProjectAndFremaaOfficersRelation1(ProjectAndFremaaOfficersRelationManager projectAndFremaaOfficersRelationManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_project_project_and_fremaa_officers_relation ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("fremaa_officer_engaged_name = :fremaaOfficerName ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("project_and_fremaa_officers_relation_uuid = :fremaaOfficerProjectRelationshipGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("fremaaOfficerProjectRelationshipGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = projectAndFremaaOfficersRelationManager.FremaaOfficerProjectRelationshipGuid;

            arParams[1] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = projectAndFremaaOfficersRelationManager.ProjectManager.ProjectGuid;

            arParams[2] = new NpgsqlParameter("fremaaOfficerName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = projectAndFremaaOfficersRelationManager.FremaaOfficerName;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;

        }

        protected internal int DeleteProjectAndFremaaOfficersRelation1(Guid projectGuid, Guid fremaaOfficerProjectRelationshipGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_project_and_fremaa_officers_relation ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("project_and_fremaa_officers_relation_uuid = :fremaaOfficerProjectRelationshipGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("fremaaOfficerProjectRelationshipGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = fremaaOfficerProjectRelationshipGuid;

            arParams[1] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = projectGuid;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;

        }

    }
}