﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SchemeCodeAndSchemeRiskRatingRelationManager
    {
        #region Private Members

        private Guid schemeCodeSchemeRiskRatingRelationGuid;
        private SchemeManager schemeManager;
        private SchemeRiskRatingManager schemeRiskRatingManager;

        #endregion


        #region Public Getter/Setter Properties

        public Guid SchemeCodeSchemeRiskRatingRelationGuid
        {
            get
            {
                return schemeCodeSchemeRiskRatingRelationGuid;
            }
            set
            {
                schemeCodeSchemeRiskRatingRelationGuid = value;
            }
        }

        public SchemeManager SchemeManager
        {
            get
            {
                if (schemeManager == null)
                {
                    schemeManager = new SchemeManager();
                }
                return schemeManager;
            }
            set
            {
                schemeManager = value;
            }
        }

        public SchemeRiskRatingManager SchemeRiskRatingManager
        {
            get
            {
                if (schemeRiskRatingManager == null)
                {
                    schemeRiskRatingManager = new SchemeRiskRatingManager();
                }
                return schemeRiskRatingManager;
            }
            set
            {
                schemeRiskRatingManager = value;
            }
        }

        #endregion
    }
}