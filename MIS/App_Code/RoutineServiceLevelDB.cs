﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineServiceLevelDB
    {
        protected internal RoutineServiceLevelManager GetRoutineServiceLevelForRoutineAnalysis(Guid routineWorkItemGuid, Guid assetTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_service_level ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_service_level obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_uuid= :routineWorkItemGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.asset_type_uuid= :assetTypeGuid ");           
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            arParams[1] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetTypeGuid;

            RoutineServiceLevelManager routineServiceLevelManagerObj = new RoutineServiceLevelManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    routineServiceLevelManagerObj.RoutineServiceLevel = Convert.ToInt16(reader["routine_service_level"].ToString());                   
                }
            }
            return routineServiceLevelManagerObj;
        }

        protected internal List<RoutineServiceLevelManager> GetRoutineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis(Guid routineWorkItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_service_level_uuid, ");
            sqlCommand.Append("obj1.routine_service_level, ");
            sqlCommand.Append("obj1.asset_type_uuid, ");
            sqlCommand.Append("obj2.asset_type_code ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_service_level obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_types obj2 ");
            sqlCommand.Append("ON ");
            sqlCommand.Append("obj1.asset_type_uuid= obj2.asset_type_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_uuid= :routineWorkItemGuid ");            
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            List<RoutineServiceLevelManager> routineServiceLevelManagerList = new List<RoutineServiceLevelManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    RoutineServiceLevelManager routineServiceLevelManagerObj = new RoutineServiceLevelManager();

                    routineServiceLevelManagerObj.RoutineServiceLevelGuid = new Guid(reader["routine_service_level_uuid"].ToString());
                    routineServiceLevelManagerObj.RoutineServiceLevel = Convert.ToInt16(reader["routine_service_level"].ToString());
                    routineServiceLevelManagerObj.AssetTypeManager.AssetTypeGuid = new Guid(reader["asset_type_uuid"].ToString());
                    routineServiceLevelManagerObj.AssetTypeManager.AssetTypeCode = reader["asset_type_code"].ToString();
                    
                    routineServiceLevelManagerList.Add(routineServiceLevelManagerObj);                
                }
            }
            return routineServiceLevelManagerList;
        }

        protected internal List<RoutineServiceLevelManager> GetRoutineServiceLevelData()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj2.asset_type_code, ");    
            sqlCommand.Append("obj5.routine_work_item_short_desc, ");
            sqlCommand.Append("obj1.routine_service_level_uuid, ");
            sqlCommand.Append("obj1.routine_service_level, ");
            sqlCommand.Append("obj1.routine_service_level_code, ");
            sqlCommand.Append("obj4.unit_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_service_level obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_types obj2 ON obj2.asset_type_uuid = obj1.asset_type_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_routine_work_items obj5 ON obj5.routine_work_item_uuid = obj1.routine_work_item_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_quantity_parameters obj3 ON obj5.asset_quantity_parameter_uuid = obj3.asset_quantity_parameter_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_units obj4 ON obj3.unit_uuid = obj4.unit_uuid  ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("routine_service_level_code ");
            sqlCommand.Append(";");

            List<RoutineServiceLevelManager> routineServiceLevelManagerList = new List<RoutineServiceLevelManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RoutineServiceLevelManager routineServiceLevelManagerObj = new RoutineServiceLevelManager();

                    routineServiceLevelManagerObj.RoutineServiceLevelGuid = new Guid(reader["routine_service_level_uuid"].ToString());
                    routineServiceLevelManagerObj.RoutineServiceLevel = Convert.ToInt16(reader["routine_service_level"].ToString());
                    routineServiceLevelManagerObj.RoutineServiceLevelCode = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["routine_service_level_code"].ToString());

                    routineServiceLevelManagerObj.AssetTypeManager.AssetTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_type_code"].ToString());
                    routineServiceLevelManagerObj.RoutineWorkItemManager.ShortDescription = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_short_desc"].ToString());
                    routineServiceLevelManagerObj.RoutineWorkItemManager.AssetQuantityParameterManager.UnitManager.UnitName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["unit_name"].ToString());

                    routineServiceLevelManagerList.Add(routineServiceLevelManagerObj);
                }
            }
            return routineServiceLevelManagerList;
        }

        protected internal int SaveRoutineServiceLevelData(Guid routineServiceLevelGuid, Guid routineWorkItemGuid, Guid assetTypeGuid, int routineServiceLevel, string routineServiceLevelCode)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_routine_service_level ");
            sqlCommand.Append("( ");
            sqlCommand.Append("routine_service_level_uuid, ");
            sqlCommand.Append("routine_work_item_uuid, ");
            sqlCommand.Append("asset_type_uuid, ");
            sqlCommand.Append("routine_service_level, ");
            sqlCommand.Append("routine_service_level_code ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":routineServiceLevelGuid, ");
            sqlCommand.Append(":routineWorkItemGuid, ");
            sqlCommand.Append(":assetTypeGuid, ");
            sqlCommand.Append(":routineServiceLevel, ");
            sqlCommand.Append(":routineServiceLevelCode ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[5];

            arParams[0] = new NpgsqlParameter("routineServiceLevelGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineServiceLevelGuid;

            arParams[1] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = routineWorkItemGuid;

            arParams[2] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetTypeGuid;

            arParams[3] = new NpgsqlParameter("routineServiceLevel", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = routineServiceLevel;

            arParams[4] = new NpgsqlParameter("routineServiceLevelCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = routineServiceLevelCode;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateRoutineServiceLevelData(Guid routineServiceLevelGuid, int routineServiceLevel)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_om_routine_service_level ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("routine_service_level= :routineServiceLevel ");            
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("routine_service_level_uuid= :routineServiceLevelGuid");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("routineServiceLevelGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineServiceLevelGuid;

            arParams[1] = new NpgsqlParameter("routineServiceLevel", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = routineServiceLevel;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;
        }

        protected internal int DeleteRoutineServiceLevelData(Guid routineWorkItemGuid, Guid assetTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_service_level ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("routine_work_item_uuid = :routineWorkItemGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("asset_type_uuid = :assetTypeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            arParams[1] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}