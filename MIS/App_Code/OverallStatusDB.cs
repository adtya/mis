﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class OverallStatusDB
    {
        protected internal List<OverallStatusManager> GetOverallStatus()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");            
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_overall_status ");
            sqlCommand.Append("ORDER BY overall_status_format ");
            sqlCommand.Append(";");

            List<OverallStatusManager> overallStatusList = new List<OverallStatusManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    OverallStatusManager overallStatusObj = new OverallStatusManager();

                    overallStatusObj.OverallStatusGuid = new Guid(reader["overall_status_uuid"].ToString());
                    overallStatusObj.OverallStatusName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["overall_status"].ToString());
                    overallStatusObj.OverallStatusFormat = Convert.ToInt16(reader["overall_status_format"].ToString());
                    overallStatusList.Add(overallStatusObj);
                }
            }
            return overallStatusList;
        }
        
    }
}