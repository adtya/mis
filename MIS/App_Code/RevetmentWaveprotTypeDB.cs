﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RevetmentWaveprotTypeDB
    {
        protected internal List<RevetmentWaveprotTypeManager> GetRevWaveprotTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("waveprot_type_uuid, ");
            sqlCommand.Append("waveprot_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_revetment_waveprot_type ");
            sqlCommand.Append("ORDER BY waveprot_type_name ");
            sqlCommand.Append(";");

            List<RevetmentWaveprotTypeManager> revWaveTypeManagerList = new List<RevetmentWaveprotTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RevetmentWaveprotTypeManager revWaveTypeManagerObj = new RevetmentWaveprotTypeManager();

                    revWaveTypeManagerObj.WaveprotTypeGuid = new Guid(reader["waveprot_type_uuid"].ToString());
                    revWaveTypeManagerObj.WaveprotTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["waveprot_type_name"].ToString());

                    revWaveTypeManagerList.Add(revWaveTypeManagerObj);
                }
            }

            return revWaveTypeManagerList;
        }
    }
}