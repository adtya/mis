﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Globalization;

namespace MIS.App_Code
{
    public class DesignationDB
    {
        protected internal List<DesignationManager> GetDesignations()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_designations ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("designation_name ");
            sqlCommand.Append(";");

            List<DesignationManager> designationList = new List<DesignationManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    DesignationManager designationObj = new DesignationManager();

                    designationObj.DesignationGuid = new Guid(reader["designation_uuid"].ToString());
                    designationObj.DesignationName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["designation_name"].ToString());

                    designationList.Add(designationObj);
                }
            }
            return designationList;
        }
    }
}