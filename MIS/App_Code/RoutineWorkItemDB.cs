﻿using NodaMoney;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineWorkItemDB
    {
        protected internal List<RoutineWorkItemManager> GetRoutineWorkItemsForRoutineAnalysis()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_work_item_uuid, ");
            sqlCommand.Append("obj1.routine_work_item_type_code, ");
            sqlCommand.Append("obj2.routine_work_item_type_full_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_work_items obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_routine_work_item_type obj2 ON obj2.routine_work_item_type_uuid = obj1.routine_work_item_type_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_class_uuid = ANY(SELECT routine_work_item_class_uuid FROM mis_om_routine_work_item_class WHERE routine_work_item_class_name = 'Routine') ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("routine_work_item_type_code ");
            sqlCommand.Append(";");

            List<RoutineWorkItemManager> routineWorkItemsManagerList = new List<RoutineWorkItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RoutineWorkItemManager routineWorkItemManagerObj = new RoutineWorkItemManager();

                    routineWorkItemManagerObj.RoutineWorkItemGuid = new Guid(reader["routine_work_item_uuid"].ToString());
                    routineWorkItemManagerObj.RoutineWorkItemTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_type_code"].ToString());
                    routineWorkItemManagerObj.RoutineWorkItemTypeManager.RoutineWorkItemTypeFullName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["routine_work_item_type_full_name"].ToString());
                    
                    routineWorkItemsManagerList.Add(routineWorkItemManagerObj);
                }
            }

            return routineWorkItemsManagerList;
        }

        protected internal List<RoutineWorkItemManager> GetRoutineWorkItemsForFieldSurvey()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_work_item_uuid, ");
            sqlCommand.Append("obj1.routine_work_item_type_code ");     
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_work_items obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_type_uuid = ANY(SELECT routine_work_item_type_uuid FROM mis_om_routine_work_item_type WHERE routine_work_item_type_name = 'EMBK') ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.routine_work_item_class_uuid = ANY(SELECT routine_work_item_class_uuid FROM mis_om_routine_work_item_class WHERE routine_work_item_class_name = 'Periodic') ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("routine_work_item_type_code ");
            sqlCommand.Append(";");

            List<RoutineWorkItemManager> routineWorkItemsManagerList = new List<RoutineWorkItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RoutineWorkItemManager routineWorkItemManagerObj = new RoutineWorkItemManager();

                    routineWorkItemManagerObj.RoutineWorkItemGuid = new Guid(reader["routine_work_item_uuid"].ToString());
                    routineWorkItemManagerObj.RoutineWorkItemTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_type_code"].ToString());
                    routineWorkItemsManagerList.Add(routineWorkItemManagerObj);
                }
            }

            return routineWorkItemsManagerList;
        }

        protected internal List<RoutineWorkItemManager> GetRoutineWorkItemsData()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_work_item_uuid, ");
            sqlCommand.Append("obj2.routine_work_item_class_name, ");
            sqlCommand.Append("obj1.routine_work_item_type_code, ");
            sqlCommand.Append("obj3.asset_quantity_parameter_name, ");
            sqlCommand.Append("obj1.routine_work_item_short_desc, ");
            sqlCommand.Append("obj1.routine_work_item_long_desc, ");
            sqlCommand.Append("obj4.unit_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_work_items obj1 ");
            sqlCommand.Append("LEFT JOIN mis_om_routine_work_item_class obj2 ");
            sqlCommand.Append("ON obj1.routine_work_item_class_uuid= obj2.routine_work_item_class_uuid ");            
            sqlCommand.Append("LEFT JOIN mis_asset_quantity_parameters obj3 ");
            sqlCommand.Append("ON obj1.asset_quantity_parameter_uuid = obj3.asset_quantity_parameter_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_units obj4 ON obj3.unit_uuid = obj4.unit_uuid ");
            sqlCommand.Append(";");

            List<RoutineWorkItemManager> routineWorkItemManagerList = new List<RoutineWorkItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RoutineWorkItemManager routineWorkItemManagerObj = new RoutineWorkItemManager();

                    routineWorkItemManagerObj.RoutineWorkItemGuid = new Guid(reader["routine_work_item_uuid"].ToString());
                    routineWorkItemManagerObj.RoutineWorkItemClassManager.RoutineWorkItemClassName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_class_name"].ToString());
                    routineWorkItemManagerObj.RoutineWorkItemTypeCode = reader["routine_work_item_type_code"].ToString();
                    routineWorkItemManagerObj.AssetQuantityParameterManager.AssetQuantityParameterName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_quantity_parameter_name"].ToString());
                    routineWorkItemManagerObj.ShortDescription = reader["routine_work_item_short_desc"].ToString();
                    routineWorkItemManagerObj.LongDescription = reader["routine_work_item_long_desc"].ToString();
                    routineWorkItemManagerObj.AssetQuantityParameterManager.UnitManager.UnitName = reader["unit_name"].ToString(); 
                   routineWorkItemManagerList.Add(routineWorkItemManagerObj);
                }
            }

            return routineWorkItemManagerList;

        }

        protected internal RoutineWorkItemManager GetRoutineWorkItemDataByRoutineWorkItemUuid(Guid routineWorkItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj2.routine_work_item_class_uuid, ");
            sqlCommand.Append("obj2.routine_work_item_class_name, ");
            sqlCommand.Append("obj5.routine_work_item_type_uuid, ");
            sqlCommand.Append("obj5.routine_work_item_type_name, ");
            sqlCommand.Append("obj1.routine_work_item_type_code, ");
            sqlCommand.Append("obj1.routine_work_item_short_desc, ");
            sqlCommand.Append("obj1.routine_work_item_long_desc, ");
            sqlCommand.Append("obj3.asset_quantity_parameter_uuid, ");
            sqlCommand.Append("obj3.asset_quantity_parameter_name, ");
            sqlCommand.Append("obj4.unit_name ");
            sqlCommand.Append("FROM mis_om_routine_work_items obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_quantity_parameters obj3 ");
            sqlCommand.Append("ON obj1.asset_quantity_parameter_uuid = obj3.asset_quantity_parameter_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_routine_work_item_type obj5 ");
            sqlCommand.Append("ON obj1.routine_work_item_type_uuid = obj5.routine_work_item_type_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_units obj4 ");
            sqlCommand.Append("ON obj3.unit_uuid = obj4.unit_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_routine_work_item_class obj2 ");
            sqlCommand.Append("ON obj1.routine_work_item_class_uuid = obj2.routine_work_item_class_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_uuid= :routineWorkItemGuid ");
            sqlCommand.Append("; ");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            RoutineWorkItemManager routineWorkItemDataObj = new RoutineWorkItemManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    routineWorkItemDataObj.RoutineWorkItemClassManager.RoutineWorkItemClassGuid = new Guid(reader["routine_work_item_class_uuid"].ToString());
                    routineWorkItemDataObj.RoutineWorkItemClassManager.RoutineWorkItemClassName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_class_name"].ToString());
                    routineWorkItemDataObj.RoutineWorkItemTypeManager.RoutineWorkItemTypeGuid = new Guid(reader["routine_work_item_type_uuid"].ToString());
                    routineWorkItemDataObj.RoutineWorkItemTypeManager.RoutineWorkItemTypeName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_type_name"].ToString());
                    routineWorkItemDataObj.RoutineWorkItemTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_type_code"].ToString());
                    routineWorkItemDataObj.ShortDescription = reader["routine_work_item_short_desc"].ToString();
                    routineWorkItemDataObj.LongDescription = reader["routine_work_item_long_desc"].ToString();
                    routineWorkItemDataObj.AssetQuantityParameterManager.AssetQuantityParameterGuid = new Guid(reader["asset_quantity_parameter_uuid"].ToString());
                    routineWorkItemDataObj.AssetQuantityParameterManager.AssetQuantityParameterName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_quantity_parameter_name"].ToString());
                    routineWorkItemDataObj.AssetQuantityParameterManager.UnitManager.UnitName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["unit_name"].ToString());                                    
                }
            }

            return routineWorkItemDataObj;

        }

        protected internal List<RoutineWorkItemManager> GetRoutineWorkItemDataForRoutineAnalysis(Guid routineWorkItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");            
            sqlCommand.Append("obj1.routine_work_item_uuid, ");
            sqlCommand.Append("obj1.routine_work_item_short_desc, ");
            sqlCommand.Append("obj1.routine_work_item_long_desc, ");      
            sqlCommand.Append("obj3.asset_quantity_parameter_uuid, ");
            sqlCommand.Append("obj3.asset_quantity_parameter_name, ");
            sqlCommand.Append("obj4.unit_name ");
            sqlCommand.Append("FROM mis_om_routine_work_items obj1 ");           
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_quantity_parameters obj3 ");
            sqlCommand.Append("ON obj1.asset_quantity_parameter_uuid = obj3.asset_quantity_parameter_uuid ");           
            sqlCommand.Append("LEFT OUTER JOIN mis_units obj4 ");
            sqlCommand.Append("ON obj3.unit_uuid = obj4.unit_uuid ");            
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_uuid= :routineWorkItemGuid ");
            sqlCommand.Append("; ");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            List<RoutineWorkItemManager> routineWorkItemDataList = new List<RoutineWorkItemManager>();
            

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    RoutineWorkItemManager routineWorkItemDataObj = new RoutineWorkItemManager();

                    routineWorkItemDataObj.RoutineWorkItemGuid = new Guid(reader["routine_work_item_uuid"].ToString());                   
                    routineWorkItemDataObj.ShortDescription = reader["routine_work_item_short_desc"].ToString();
                    routineWorkItemDataObj.LongDescription = reader["routine_work_item_long_desc"].ToString();                   
                    routineWorkItemDataObj.AssetQuantityParameterManager.AssetQuantityParameterGuid = new Guid(reader["asset_quantity_parameter_uuid"].ToString());
                    routineWorkItemDataObj.AssetQuantityParameterManager.AssetQuantityParameterName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["asset_quantity_parameter_name"].ToString());
                    routineWorkItemDataObj.AssetQuantityParameterManager.UnitManager.UnitName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["unit_name"].ToString());

                    routineWorkItemDataList.Add(routineWorkItemDataObj);
                }
            }

            return routineWorkItemDataList;

        }

        protected internal RoutineWorkItemManager GetRoutineWorkItemDataForFieldSurvey(Guid routineWorkItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.routine_work_item_uuid, ");
            sqlCommand.Append("obj1.routine_work_item_type_code, ");
            sqlCommand.Append("obj1.routine_work_item_short_desc ");       
            sqlCommand.Append("FROM mis_om_routine_work_items obj1 ");           
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.routine_work_item_uuid= :routineWorkItemGuid ");            

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            RoutineWorkItemManager routineWorkItemDataObj = new RoutineWorkItemManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {                   
                    routineWorkItemDataObj.RoutineWorkItemGuid = new Guid(reader["routine_work_item_uuid"].ToString());
                    routineWorkItemDataObj.RoutineWorkItemTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_type_code"].ToString());
                    routineWorkItemDataObj.ShortDescription = reader["routine_work_item_short_desc"].ToString();
                    
                }
            }
            return routineWorkItemDataObj;
        }

        //protected internal RoutineWorkItemManager GetRoutineWorkItemDataForFieldSurvey(Guid routineWorkItemGuid)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("SELECT ");
        //    sqlCommand.Append("obj1.routine_work_item_uuid, ");            
        //    sqlCommand.Append("obj1.routine_work_item_type_code, ");
        //    sqlCommand.Append("obj1.routine_work_item_short_desc, ");
        //    sqlCommand.Append("obj2.routine_work_item_rate ");
        //    sqlCommand.Append("FROM mis_om_routine_work_items obj1 ");
        //    sqlCommand.Append("LEFT OUTER JOIN mis_om_routine_work_item_rate obj2 ON obj1.routine_work_item_uuid=obj2.routine_work_item_uuid ");
        //    sqlCommand.Append("WHERE ");
        //    sqlCommand.Append("obj1.routine_work_item_uuid= :routineWorkItemGuid ");
        //    sqlCommand.Append("AND ");
        //    sqlCommand.Append("obj2.circle_uuid = ANY(SELECT circle_uuid FROM system_variables) ");
        //    sqlCommand.Append("AND ");
        //    sqlCommand.Append("obj2.routine_work_item_rate_fiscal_year = ANY(SELECT fiscal_year FROM system_variables) ");
        //    sqlCommand.Append("; ");

        //    NpgsqlParameter[] arParams = new NpgsqlParameter[1];

        //    arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[0].Direction = ParameterDirection.Input;
        //    arParams[0].Value = routineWorkItemGuid;

        //    RoutineWorkItemManager routineWorkItemDataObj = new RoutineWorkItemManager();

        //    using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
        //    {
        //        while (reader.Read())
        //        {
        //            RoutineWorkItemRateManager routineWorkItemRateManagerObj = new RoutineWorkItemRateManager();
                    
        //            routineWorkItemRateManagerObj.RoutineWorkItemRate = string.IsNullOrEmpty(reader["routine_work_item_rate"].ToString()) ? Money.Parse("0") : Money.Parse(reader["routine_work_item_rate"].ToString());

        //            routineWorkItemDataObj.RoutineWorkItemGuid = new Guid(reader["routine_work_item_uuid"].ToString());
        //            routineWorkItemDataObj.RoutineWorkItemTypeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["routine_work_item_type_code"].ToString());
        //            routineWorkItemDataObj.ShortDescription = reader["routine_work_item_short_desc"].ToString();
                    
        //            routineWorkItemDataObj.RoutineWorkItemRateManagerList = new List<RoutineWorkItemRateManager>();
        //            routineWorkItemDataObj.RoutineWorkItemRateManagerList.Add(routineWorkItemRateManagerObj);                    
        //        }
        //    }
        //    return routineWorkItemDataObj;
        //}

        protected internal int AddRoutineWorkItem(RoutineWorkItemManager routineWorkItemManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_routine_work_items ");
            sqlCommand.Append("( ");
            sqlCommand.Append("routine_work_item_uuid, ");
            sqlCommand.Append("routine_work_item_class_uuid, ");
            sqlCommand.Append("routine_work_item_type_uuid, ");
            sqlCommand.Append("routine_work_item_short_desc, ");
            sqlCommand.Append("routine_work_item_long_desc, ");
            sqlCommand.Append("asset_quantity_parameter_uuid, ");
            sqlCommand.Append("routine_work_item_type_code ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":routineWorkItemGuid, ");
            sqlCommand.Append(":routineWorkItemClassGuid, ");
            sqlCommand.Append(":routineWorkItemTypeGuid, ");
            sqlCommand.Append(":shortDescription, ");
            sqlCommand.Append(":longDescription, ");
            sqlCommand.Append(":assetQuantityParameterGuid, ");
            sqlCommand.Append(":routineWorkItemTypeCode ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[7];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemManager.RoutineWorkItemGuid;

            arParams[1] = new NpgsqlParameter("routineWorkItemClassGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = routineWorkItemManager.RoutineWorkItemClassManager.RoutineWorkItemClassGuid;

            arParams[2] = new NpgsqlParameter("routineWorkItemTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = routineWorkItemManager.RoutineWorkItemTypeManager.RoutineWorkItemTypeGuid;

            arParams[3] = new NpgsqlParameter("shortDescription", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = routineWorkItemManager.ShortDescription;

            arParams[4] = new NpgsqlParameter("longDescription", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = routineWorkItemManager.LongDescription;

            arParams[5] = new NpgsqlParameter("assetQuantityParameterGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = routineWorkItemManager.AssetQuantityParameterManager.AssetQuantityParameterGuid;

            arParams[6] = new NpgsqlParameter("routineWorkItemTypeCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = routineWorkItemManager.RoutineWorkItemTypeManager.RoutineWorkItemTypeName + routineWorkItemManager.RoutineWorkItemTypeManager.RoutineWorkItemTypeNumber;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateRoutineWorkItemData(Guid routineWorkItemGuid, RoutineWorkItemManager routineWorkItemManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_om_routine_work_items ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("routine_work_item_short_desc= :shortDescription, ");
            sqlCommand.Append("routine_work_item_long_desc= :longDescription, ");
            sqlCommand.Append("asset_quantity_parameter_uuid= :assetQuantityParameterGuid ");         
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("routine_work_item_uuid= :routineWorkItemGuid");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[5];

            arParams[0] = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = routineWorkItemGuid;

            arParams[1] = new NpgsqlParameter("shortDescription", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = routineWorkItemManager.ShortDescription;

            arParams[2] = new NpgsqlParameter("longDescription", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = routineWorkItemManager.LongDescription;

            arParams[3] = new NpgsqlParameter("assetQuantityParameterGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = routineWorkItemManager.AssetQuantityParameterManager.AssetQuantityParameterGuid;
            
            arParams[4] = new NpgsqlParameter("routineWorkItemTypeCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = routineWorkItemManager.RoutineWorkItemTypeCode;

            return NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);


        }

        protected internal int DeleteRoutineWorkItemData(Guid routineWorkItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_routine_work_items ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("routine_work_item_uuid = :routineWorkItemGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("routineWorkItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = routineWorkItemGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}