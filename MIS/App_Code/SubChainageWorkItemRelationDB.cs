﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SubChainageWorkItemRelationDB
    {
        protected internal bool CreateNewSubChainageWorkItemRelation(List<SubChainageWorkItemRelationManager> subChainageWorkItemRelationList)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_sub_chainage_work_item_relation ");
            sqlCommand.Append("( ");
            sqlCommand.Append("sub_chainage_work_item_relation_uuid, ");
            sqlCommand.Append("sub_chainage_uuid, ");
            sqlCommand.Append("work_item_uuid, ");
            sqlCommand.Append("project_uuid, ");
            sqlCommand.Append("work_item_value ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            foreach (SubChainageWorkItemRelationManager subChainageWorkItemRelationObj in subChainageWorkItemRelationList)
            {
                int index = subChainageWorkItemRelationList.IndexOf(subChainageWorkItemRelationObj);

                sqlCommand.Append("( ");

                sqlCommand.Append("'" + subChainageWorkItemRelationObj.SubChainageWorkItemRelationGuid + "', ");
                sqlCommand.Append("'" + subChainageWorkItemRelationObj.SubChainageManager.SubChainageGuid + "', ");
                sqlCommand.Append("'" + subChainageWorkItemRelationObj.WorkItemManager.WorkItemGuid + "', ");
                sqlCommand.Append("'" + subChainageWorkItemRelationObj.ProjectManager.ProjectGuid + "', ");
                sqlCommand.Append("'" + subChainageWorkItemRelationObj.WorkItemManager.WorkItemTotalValue + "' ");

                if (index == subChainageWorkItemRelationList.Count - 1)
                {
                    sqlCommand.Append(") ");
                }
                else
                {
                    sqlCommand.Append("), ");
                }
            }

            sqlCommand.Append(";");

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());

            return (result > 0);
        }
    }
}