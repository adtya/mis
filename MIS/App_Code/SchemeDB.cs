﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SchemeDB
    {
        //protected internal List<SchemeManager> GetAllSchemes()
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("SELECT ");
        //    sqlCommand.Append("obj1.scheme_uuid, ");
        //    sqlCommand.Append("obj1.scheme_name, ");
        //    sqlCommand.Append("obj1.scheme_code ");
        //    sqlCommand.Append("FROM ");
        //    sqlCommand.Append("mis_schemes obj1 ");
        //    sqlCommand.Append(";");

        //    List<SchemeManager> schemeNameManagerList = new List<SchemeManager>();

        //    using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
        //    {
        //        while (reader.Read())
        //        {
        //            SchemeManager schemeNameManagerObj = new SchemeManager();

        //            schemeNameManagerObj.SchemeGuid = new Guid(reader["scheme_uuid"].ToString());
        //            schemeNameManagerObj.SchemeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["scheme_name"].ToString());
        //            schemeNameManagerObj.SchemeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["scheme_code"].ToString());

        //            schemeNameManagerList.Add(schemeNameManagerObj);
        //        }
        //    }

        //    return schemeNameManagerList;
        //}

        protected internal List<SchemeManager> GetAllSchemesData()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.circle_name, ");
            sqlCommand.Append("obj1.circle_code,");
            sqlCommand.Append("obj2.division_name,");
            sqlCommand.Append("obj2.division_code, ");
            sqlCommand.Append("obj3.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_schemes obj3 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_circles obj1 ON obj1.circle_uuid = obj3.circle_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_divisions obj2 ON obj2.division_uuid = obj3.division_uuid ");
            sqlCommand.Append(";");

            List<SchemeManager> schemeManagerList = new List<SchemeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    SchemeManager schemeManagerObj = new SchemeManager();

                    schemeManagerObj.SchemeGuid = new Guid(reader["scheme_uuid"].ToString());
                    schemeManagerObj.SchemeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["scheme_name"].ToString());
                    schemeManagerObj.SchemeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["scheme_code"].ToString());
                    schemeManagerObj.SchemeType = reader["scheme_type"].ToString();
                    schemeManagerObj.UtmEast1 = Convert.ToInt16(reader["utm_east1"].ToString());
                    schemeManagerObj.UtmEast2 = Convert.ToInt16(reader["utm_east2"].ToString());
                    schemeManagerObj.UtmNorth1 = Convert.ToInt16(reader["utm_north1"].ToString());
                    schemeManagerObj.UtmNorth2 = Convert.ToInt16(reader["utm_north2"].ToString());
                    schemeManagerObj.Area = Convert.ToInt16(reader["area"].ToString());
                    schemeManagerObj.DrawingId = reader["drawing_id"].ToString();

                    schemeManagerObj.CircleManager.CircleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["circle_name"].ToString());
                    schemeManagerObj.CircleManager.CircleCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["circle_code"].ToString());

                    schemeManagerObj.DivisionManager.DivisionName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["division_name"].ToString());
                    schemeManagerObj.DivisionManager.DivisionCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["division_code"].ToString());
                                       
                    schemeManagerList.Add(schemeManagerObj);
                }
            }

            return schemeManagerList;
        }

        protected internal int AddScheme(Guid schemeGuid, string schemeCode, string schemeType, string schemeName, int utmEast1, int utmEast2, int utmNorth1, int utmNorth2, int area, Guid circleGuid, Guid divisionGuid, string drawingId)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_schemes ");
            sqlCommand.Append("( ");
            sqlCommand.Append("scheme_uuid, ");
            sqlCommand.Append("scheme_code, ");
            sqlCommand.Append("scheme_type, ");
            sqlCommand.Append("scheme_name, ");
            sqlCommand.Append("utm_east1, ");
            sqlCommand.Append("utm_east2, ");
            sqlCommand.Append("utm_north1, ");
            sqlCommand.Append("utm_north2, ");
            sqlCommand.Append("area, ");
            sqlCommand.Append("circle_uuid, ");
            sqlCommand.Append("division_uuid, ");
            sqlCommand.Append("drawing_id ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":schemeGuid, ");
            sqlCommand.Append(":schemeCode, ");
            sqlCommand.Append(":schemeType, ");
            sqlCommand.Append(":schemeName, ");
            sqlCommand.Append(":utmEast1, ");
            sqlCommand.Append(":utmEast2, ");
            sqlCommand.Append(":utmNorth1, ");
            sqlCommand.Append(":utmNorth2, ");
            sqlCommand.Append(":area, ");
            sqlCommand.Append(":circleGuid, ");
            sqlCommand.Append(":divisionGuid, ");
            sqlCommand.Append(":drawingId ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[12];

            arParams[0] = new NpgsqlParameter("schemeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = schemeGuid;

            arParams[1] = new NpgsqlParameter("schemeCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToUpper(schemeCode);

            arParams[2] = new NpgsqlParameter("schemeType", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(schemeType);

            arParams[3] = new NpgsqlParameter("schemeName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(schemeName);

            arParams[4] = new NpgsqlParameter("utmEast1", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = utmEast1;

            arParams[5] = new NpgsqlParameter("utmEast2", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = utmEast2;

            arParams[6] = new NpgsqlParameter("utmNorth1", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = utmNorth1;

            arParams[7] = new NpgsqlParameter("utmNorth2", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = utmNorth2;

            arParams[8] = new NpgsqlParameter("area", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = area;

            arParams[9] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = circleGuid;

            arParams[10] = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[10].Direction = ParameterDirection.Input;
            arParams[10].Value = divisionGuid;

            arParams[11] = new NpgsqlParameter("drawingId", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[11].Direction = ParameterDirection.Input;
            arParams[11].Value = drawingId;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal List<SchemeManager> GetSchemesBasedOnCircleAndDivision(Guid circleGuid, Guid divisionGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("scheme_uuid, ");
            sqlCommand.Append("scheme_code, ");
            sqlCommand.Append("scheme_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_schemes ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("circle_uuid = :circleGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("division_uuid = :divisionGuid ");
            sqlCommand.Append("ORDER BY scheme_name ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = circleGuid;

            arParams[1] = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = divisionGuid;

            List<SchemeManager> schemeDataList = new List<SchemeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(),arParams))
            {
                while (reader.Read())
                {
                    SchemeManager schemeObj = new SchemeManager();

                    schemeObj.SchemeGuid = new Guid(reader["scheme_uuid"].ToString());
                    schemeObj.SchemeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["scheme_code"].ToString());
                    schemeDataList.Add(schemeObj);
                }
            }

            return schemeDataList;
        }

        protected internal List<SchemeManager> GetSchemesBasedOnDivisions(Guid divisionGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("scheme_uuid, ");
            sqlCommand.Append("scheme_code, ");
            sqlCommand.Append("scheme_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_schemes ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("division_uuid = :divisionGuid ");
            sqlCommand.Append("ORDER BY scheme_code ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = divisionGuid;            

            List<SchemeManager> schemeDataList = new List<SchemeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    SchemeManager schemeObj = new SchemeManager();

                    schemeObj.SchemeGuid = new Guid(reader["scheme_uuid"].ToString());
                    schemeObj.SchemeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["scheme_code"].ToString());
                    schemeObj.SchemeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["scheme_name"].ToString());
                    schemeDataList.Add(schemeObj);
                }
            }

            return schemeDataList;
        }

        protected internal SchemeManager GetSchemeDataBySchemeGuid(Guid schemeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.scheme_uuid, ");
            sqlCommand.Append("obj1.scheme_code, ");
            sqlCommand.Append("obj1.scheme_type, ");
            sqlCommand.Append("obj1.scheme_name, ");
            sqlCommand.Append("obj1.utm_east1, ");
            sqlCommand.Append("obj1.utm_east2, ");
            sqlCommand.Append("obj1.utm_north1, ");
            sqlCommand.Append("obj1.utm_north2, ");
            sqlCommand.Append("obj1.area, ");
            sqlCommand.Append("obj1.drawing_id, ");
            sqlCommand.Append("obj2.circle_name, ");
            sqlCommand.Append("obj2.circle_code, ");
            sqlCommand.Append("obj3.division_name, ");
            sqlCommand.Append("obj3.division_code ");  
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_schemes obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_circles obj2 ON obj1.circle_uuid = obj2.circle_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_divisions obj3 ON obj1.division_uuid = obj3.division_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("scheme_uuid = :schemeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("schemeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = schemeGuid;

            SchemeManager schemeObj = new SchemeManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    schemeObj.SchemeGuid = new Guid(reader["scheme_uuid"].ToString());
                    schemeObj.SchemeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["scheme_name"].ToString());
                    schemeObj.SchemeCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["scheme_code"].ToString());
                    schemeObj.SchemeType = reader["scheme_type"].ToString();
                    schemeObj.UtmEast1 = Convert.ToInt16(reader["utm_east1"].ToString());
                    schemeObj.UtmEast2 = Convert.ToInt16(reader["utm_east2"].ToString());
                    schemeObj.UtmNorth1 = Convert.ToInt16(reader["utm_north1"].ToString());
                    schemeObj.UtmNorth2 = Convert.ToInt16(reader["utm_north2"].ToString());
                    schemeObj.Area = Convert.ToInt16(reader["area"].ToString());
                    schemeObj.DrawingId = reader["drawing_id"].ToString();

                    schemeObj.CircleManager.CircleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["circle_name"].ToString());
                    schemeObj.CircleManager.CircleCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["circle_code"].ToString());

                    schemeObj.DivisionManager.DivisionName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["division_name"].ToString());
                    schemeObj.DivisionManager.DivisionCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["division_code"].ToString());
                    
                }
            }

            return schemeObj;       
           
        }

        protected internal bool UpdateSchemeData(Guid schemeGuid, string editSchemeCode, string editSchemeType, string editSchemeName, int editUtmEast1, int editUtmEast2, int editUtmNorth1, int editUtmNorth2, int editArea, string editDrawingID)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_schemes ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("scheme_code= :editSchemeCode, ");            
            sqlCommand.Append("scheme_name= :editSchemeName, ");
            sqlCommand.Append("scheme_type= :editSchemeType, ");
            sqlCommand.Append("utm_east1= :editUtmEast1, ");
            sqlCommand.Append("utm_east2= :editUtmEast2, ");
            sqlCommand.Append("utm_north1= :editUtmNorth1, ");
            sqlCommand.Append("utm_north2= :editUtmNorth2, ");
            sqlCommand.Append("area= :editArea, ");         
            sqlCommand.Append("drawing_id= :editDrawingID ");          
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("scheme_uuid = :schemeGuid ");            
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[10];

            arParams[0] = new NpgsqlParameter("schemeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = schemeGuid;

            arParams[1] = new NpgsqlParameter("editSchemeCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = editSchemeCode;

            arParams[2] = new NpgsqlParameter("editSchemeType", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = editSchemeType;

            arParams[3] = new NpgsqlParameter("editSchemeName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = editSchemeName;

            arParams[4] = new NpgsqlParameter("editUtmEast1", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = editUtmEast1;

            arParams[5] = new NpgsqlParameter("editUtmEast2", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = editUtmEast2;

            arParams[6] = new NpgsqlParameter("editUtmNorth1", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = editUtmNorth1;

            arParams[7] = new NpgsqlParameter("editUtmNorth2", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = editUtmNorth2;

            arParams[8] = new NpgsqlParameter("editArea", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = editArea;

            arParams[9] = new NpgsqlParameter("editDrawingID", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = editDrawingID;

            int a = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return (a > 0);
        }

        protected internal int DeleteScheme(Guid schemeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_schemes ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("scheme_uuid = :schemeGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("schemeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = schemeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
        
        protected internal bool CheckSchemeExistence(string schemeCode, string schemeName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_schemes obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.scheme_name = :schemeName ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.scheme_code = :schemeCode ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("schemeName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(schemeName);

            arParams[1] = new NpgsqlParameter("schemeCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(schemeCode);

            bool result = false;

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    result = true;
                }
            }

            return result;
        }
    }
}