﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class PorcupineMaterialTypeManager
    {
        #region Private Members

        private Guid porcupineMaterialTypeGuid;
        private string porcupineMaterialTypeName;
       
       #endregion


        #region Public Constructors

        public PorcupineMaterialTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid PorcupineMaterialTypeGuid
        {
            get
            {
                return porcupineMaterialTypeGuid;
            }
            set
            {
                porcupineMaterialTypeGuid = value;
            }
        }

        public string PorcupineMaterialTypeName
        {
            get
            {
                return porcupineMaterialTypeName;
            }
            set
            {
                porcupineMaterialTypeName = value;
            }
        }        

        #endregion
    }
}