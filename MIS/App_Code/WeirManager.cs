﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class WeirManager
    {
         #region Private Members

        private Guid weirGuid;               
        private double invertLevel;
        private double crestTopLevel;
        private double crestTopWidth;   
        //private AssetManager assetManager;
        private WeirTypeManager weirTypeManager;        

        #endregion


        #region Public Constructors

        public WeirManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid WeirGuid
        {
            get
            {
                return weirGuid;
            }
            set
            {
                weirGuid = value;
            }
        }

        public double InvertLevel
        {
            get
            {
                return invertLevel;
            }
            set
            {
                invertLevel = value;
            }
        }

        public double CrestTopLevel
        {
            get
            {
                return crestTopLevel;
            }
            set
            {
                crestTopLevel = value;
            }
        }

        public double CrestTopWidth
        {
            get
            {
                return crestTopWidth;
            }
            set
            {
                crestTopWidth = value;
            }
        }

        public WeirTypeManager WeirTypeManager
        {
            get
            {
                if (weirTypeManager == null)
                {
                    weirTypeManager = new WeirTypeManager();
                }
                return weirTypeManager;
            }
            set
            {
                weirTypeManager = value;
            }
        }   
     
        // public AssetManager AssetManager
        //{
        //    get
        //    {
        //        if (assetManager == null)
        //        {
        //            assetManager = new AssetManager();
        //        }
        //        return assetManager;
        //    }
        //    set
        //    {
        //        assetManager = value;
        //    }
        //}

        #endregion
    }
}