﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Web;

namespace MIS.App_Code
{
    public static class TimerManager
    {
        private static Timer timer; // From System.Timers
        private static TimeSpan AutoRestartTime = new TimeSpan(12, 0, 0);

        public static void Start()
        {
            timer = new Timer(24 * 60 * 60 * 1000); // Set up the timer for 12 hours
            //
            // Type "_timer.Elapsed += " and press tab twice.
            //
            timer.Elapsed += new ElapsedEventHandler(_timer_Elapsed);
            timer.Enabled = true; // Enable it
        }

        static void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ActualCompletionDateUpdater.UpdateSubChainageActualDate();
        }

        public static void AutoRestartThreadRun()
        {
            try
            {
                DateTime nextRestart = DateTime.Today.Add(AutoRestartTime);

                if (nextRestart < DateTime.Now) {
                    nextRestart = nextRestart.AddDays(1);
                }

                while (true)
                {
                    if (nextRestart < DateTime.Now)
                    {
                        ActualCompletionDateUpdater.UpdateSubChainageActualDate();
                    }
                    else
                    {
                        dynamic sleepMs = Convert.ToInt32(Math.Max(1000, nextRestart.Subtract(DateTime.Now).TotalMilliseconds / 2));
                        System.Threading.Thread.Sleep(sleepMs);
                    }
                }
            }
            //catch (ThreadAbortException taex)
            //{

            //}
            catch (Exception ex) 
            {
                
            }
        }

    }
}