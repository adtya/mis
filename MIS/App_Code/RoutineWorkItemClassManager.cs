﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RoutineWorkItemClassManager
    {
        #region Private Members

        private Guid routineWorkItemClassGuid;
        private string routineWorkItemClassName;       

        #endregion


        #region Public Getter/Setter Properties

        public Guid RoutineWorkItemClassGuid
        {
            get
            {
                return routineWorkItemClassGuid;
            }
            set
            {
                routineWorkItemClassGuid = value;
            }
        }

        public string RoutineWorkItemClassName
        {
            get
            {
                return routineWorkItemClassName;
            }
            set
            {
                routineWorkItemClassName = value;
            }
        }
        #endregion
    }
}