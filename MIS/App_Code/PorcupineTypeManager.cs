﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class PorcupineTypeManager
    {
         #region Private Members

        private Guid porcupineTypeGuid;
        private string porcupineTypeName;
       
       #endregion


        #region Public Constructors

        public PorcupineTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid PorcupineTypeGuid
        {
            get
            {
                return porcupineTypeGuid;
            }
            set
            {
                porcupineTypeGuid = value;
            }
        }

        public string PorcupineTypeName
        {
            get
            {
                return porcupineTypeName;
            }
            set
            {
                porcupineTypeName = value;
            }
        }        

        #endregion
    }
}