﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class ProbabilityFailureManager
    {
        #region Private Members

        private Guid probabilityFailureGuid;
        private string probabilityFailure;
        private int probabilityFailureNumber;
                
        #endregion

        #region Public Getter/Setter Properties

        public Guid ProbabilityFailureGuid
        {
            get
            {
                return probabilityFailureGuid;
            }
            set
            {
                probabilityFailureGuid = value;
            }
        }

        public string ProbabilityFailure
        {
            get
            {
                return probabilityFailure;
            }
            set
            {
                probabilityFailure = value;
            }
        }

        public int ProbabilityFailureNumber
        {
            get
            {
                return probabilityFailureNumber;
            }
            set
            {
                probabilityFailureNumber = value;
            }
        }
     
        #endregion
    }
}
