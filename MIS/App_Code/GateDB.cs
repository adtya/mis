﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class GateDB
    {
        protected internal void AddGateForRegulator(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_gates ");
            sqlCommand.Append("( ");
            sqlCommand.Append("gate_uuid, ");
            sqlCommand.Append("number_of_gates, ");
            sqlCommand.Append("height, ");
            sqlCommand.Append("width, ");
            sqlCommand.Append("diameter, ");
            sqlCommand.Append("install_date, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("gate_type_uuid, ");
            sqlCommand.Append("gate_construction_material_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
           
            if (assetManager.RegulatorManager.GateManagerList.Count > 0)
            {
                foreach (GateManager gateManagerObj in assetManager.RegulatorManager.GateManagerList)
                {
                    int index = assetManager.RegulatorManager.GateManagerList.IndexOf(gateManagerObj);

                    sqlCommand.Append("( ");
                    sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                    sqlCommand.Append("'" + gateManagerObj.NumberOfGates + "', ");
                    sqlCommand.Append("'" + gateManagerObj.Height + "', ");
                    sqlCommand.Append("'" + gateManagerObj.Width + "', ");
                    sqlCommand.Append("'" + gateManagerObj.Diameter + "', ");
                    sqlCommand.Append("to_date('" + gateManagerObj.InstallDate.ToString("dd/MM/yyyy") + "', 'DD/MM/YYYY'), ");
                    sqlCommand.Append("'" + assetManager.AssetGuid + "', ");
                    sqlCommand.Append("'" + gateManagerObj.GateTypeManager.GateTypeGuid + "', ");
                    sqlCommand.Append("'" + gateManagerObj.GateConstructionMaterialManager.GateConstructionMaterialGuid + "'");

                    if (index <= assetManager.RegulatorManager.GateManagerList.Count - 2)
                    {
                        sqlCommand.Append("), ");
                    }
                    else
                    {
                        sqlCommand.Append(") ");

                    }
                }

                sqlCommand.Append(";");
                NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());
            }            
        }

        protected internal void AddGateForSluice(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_gates ");
            sqlCommand.Append("( ");
            sqlCommand.Append("gate_uuid, ");
            sqlCommand.Append("number_of_gates, ");
            sqlCommand.Append("height, ");
            sqlCommand.Append("width, ");
            sqlCommand.Append("diameter, ");
            sqlCommand.Append("install_date, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("gate_type_uuid, ");
            sqlCommand.Append("gate_construction_material_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            
            if (assetManager.SluiceManager.GateManagerList.Count > 0)
            {
                foreach (GateManager gateManagerObj in assetManager.SluiceManager.GateManagerList)
                {
                    int index = assetManager.SluiceManager.GateManagerList.IndexOf(gateManagerObj);

                    sqlCommand.Append("( ");
                    sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                    sqlCommand.Append("'" + gateManagerObj.NumberOfGates + "', ");
                    sqlCommand.Append("'" + gateManagerObj.Height + "', ");
                    sqlCommand.Append("'" + gateManagerObj.Width + "', ");
                    sqlCommand.Append("'" + gateManagerObj.Diameter + "', ");
                    sqlCommand.Append("to_date('" + gateManagerObj.InstallDate.ToString("dd/MM/yyyy") + "', 'DD/MM/YYYY'), ");
                    sqlCommand.Append("'" + assetManager.AssetGuid + "', ");
                    sqlCommand.Append("'" + gateManagerObj.GateTypeManager.GateTypeGuid + "', ");
                    sqlCommand.Append("'" + gateManagerObj.GateConstructionMaterialManager.GateConstructionMaterialGuid + "'");

                    if (index <= assetManager.SluiceManager.GateManagerList.Count - 2)
                    {
                        sqlCommand.Append("), ");
                    }
                    else
                    {
                        sqlCommand.Append(") ");          
                        
                    }
                }

                sqlCommand.Append(";");
                NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());
            }
        }

        protected internal void AddGateForTurnout(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_gates ");
            sqlCommand.Append("( ");
            sqlCommand.Append("gate_uuid, ");
            sqlCommand.Append("number_of_gates, ");
            sqlCommand.Append("height, ");
            sqlCommand.Append("width, ");
            sqlCommand.Append("diameter, ");
            sqlCommand.Append("install_date, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("gate_type_uuid, ");
            sqlCommand.Append("gate_construction_material_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
           
            if (assetManager.TurnoutManager.GateManagerList.Count > 0)
            {
                foreach (GateManager gateManagerObj in assetManager.TurnoutManager.GateManagerList)
                {
                    int index = assetManager.TurnoutManager.GateManagerList.IndexOf(gateManagerObj);
                    sqlCommand.Append("( ");
                    sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                    sqlCommand.Append("'" + gateManagerObj.NumberOfGates + "', ");
                    sqlCommand.Append("'" + gateManagerObj.Height + "', ");
                    sqlCommand.Append("'" + gateManagerObj.Width + "', ");
                    sqlCommand.Append("'" + gateManagerObj.Diameter + "', ");
                    sqlCommand.Append("to_date('" + gateManagerObj.InstallDate.ToString("dd/MM/yyyy") + "', 'DD/MM/YYYY'), ");
                    sqlCommand.Append("'" + assetManager.AssetGuid + "', ");
                    sqlCommand.Append("'" + gateManagerObj.GateTypeManager.GateTypeGuid + "', ");
                    sqlCommand.Append("'" + gateManagerObj.GateConstructionMaterialManager.GateConstructionMaterialGuid + "'");
                    if (index <= assetManager.TurnoutManager.GateManagerList.Count - 2)
                    {
                        sqlCommand.Append("), ");
                    }
                    else
                    {
                        sqlCommand.Append(") ");
                        sqlCommand.Append(";");
                    }                                    
                }                
                NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());
            }           
        }

        protected internal List<GateManager> GetGateDataListByAssetUuid(Guid assetGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.gate_uuid, ");
            sqlCommand.Append("obj1.gate_type_uuid , ");
            sqlCommand.Append("obj1.gate_construction_material_uuid, ");
            sqlCommand.Append("obj1.number_of_gates, ");
            sqlCommand.Append("obj1.height, ");
            sqlCommand.Append("obj1.width, ");
            sqlCommand.Append("obj1.diameter, ");
            sqlCommand.Append("obj1.install_date, ");
            sqlCommand.Append("obj2.gate_type_name, ");
            sqlCommand.Append("obj3.gate_construction_material_name ");
            sqlCommand.Append("FROM mis_asset_gates obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_gate_types obj2 ON obj1.gate_type_uuid=obj2.gate_type_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_asset_gate_construction_material obj3 ON obj1.gate_construction_material_uuid=obj3.gate_construction_material_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.asset_uuid = :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetGuid;

            List<GateManager> gateDataManagerList = new List<GateManager>();
            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    GateManager gateDataObj = new GateManager();

                    gateDataObj.GateGuid = new Guid(reader["gate_uuid"].ToString());
                    gateDataObj.NumberOfGates = Convert.ToInt32(reader["number_of_gates"].ToString());
                    gateDataObj.Height = Convert.ToDouble(reader["height"].ToString());
                    gateDataObj.Width = Convert.ToDouble(reader["width"].ToString());
                    gateDataObj.Diameter = Convert.ToDouble(reader["diameter"].ToString());
                    gateDataObj.InstallDate = DateTime.Parse(reader["install_date"].ToString());
                    gateDataObj.GateTypeManager.GateTypeGuid = new Guid(reader["gate_type_uuid"].ToString());
                    gateDataObj.GateConstructionMaterialManager.GateConstructionMaterialGuid = new Guid(reader["gate_construction_material_uuid"].ToString());
                    gateDataObj.GateTypeManager.GateTypeName = reader["gate_type_name"].ToString();
                    gateDataObj.GateConstructionMaterialManager.GateConstructionMaterialName = reader["gate_construction_material_name"].ToString();

                    gateDataManagerList.Add(gateDataObj);

                }
            }

            return gateDataManagerList;
        }

        protected internal void UpdateGateDataForRegulator(AssetManager assetManager)
        {
            StringBuilder numberOfGates = new StringBuilder();
            StringBuilder height = new StringBuilder();
            StringBuilder width = new StringBuilder();
            StringBuilder diameter = new StringBuilder();
            StringBuilder installDate = new StringBuilder();
            StringBuilder assetString = new StringBuilder();
            StringBuilder gateTypeGuid = new StringBuilder();
            StringBuilder gateConstructionMaterial = new StringBuilder();
            StringBuilder gateGuidString = new StringBuilder();

            StringBuilder insertSqlCommand = new StringBuilder();
            insertSqlCommand.Append("INSERT INTO ");
            insertSqlCommand.Append("mis_asset_gates ");
            insertSqlCommand.Append("( ");
            insertSqlCommand.Append("gate_uuid, ");
            insertSqlCommand.Append("number_of_gates, ");
            insertSqlCommand.Append("height, ");
            insertSqlCommand.Append("width, ");
            insertSqlCommand.Append("diameter, ");
            insertSqlCommand.Append("install_date, ");
            insertSqlCommand.Append("asset_uuid, ");
            insertSqlCommand.Append("gate_type_uuid, ");
            insertSqlCommand.Append("gate_construction_material_uuid ");
            insertSqlCommand.Append(") ");
            insertSqlCommand.Append("VALUES ");

            if (assetManager.RegulatorManager.GateManagerList.Count > 0)
            {
                int i = 0;
                int j = 0;

                foreach (GateManager gateManagerObj in assetManager.RegulatorManager.GateManagerList)
                {
                    int index = assetManager.RegulatorManager.GateManagerList.IndexOf(gateManagerObj);

                    if (gateManagerObj.GateGuid == Guid.Empty)
                    {
                        i = i + 1;
                        if (i != 1)
                        {
                            insertSqlCommand.Append(", ");
                        }
                        insertSqlCommand.Append("( ");
                        insertSqlCommand.Append("'" + Guid.NewGuid() + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.NumberOfGates + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.Height + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.Width + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.Diameter + "', ");
                        insertSqlCommand.Append("to_date('" + gateManagerObj.InstallDate.ToString("dd/MM/yyyy") + "', 'DD/MM/YYYY'), ");
                        insertSqlCommand.Append("'" + assetManager.AssetGuid + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.GateTypeManager.GateTypeGuid + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.GateConstructionMaterialManager.GateConstructionMaterialGuid + "'");
                        insertSqlCommand.Append(") ");

                    }
                    else
                    {
                        j = j + 1;
                        if (j != 1)
                        {
                            gateGuidString.Append(", ");
                        }
                        numberOfGates.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + gateManagerObj.NumberOfGates + " ");
                        height.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + gateManagerObj.Height + " ");
                        width.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + gateManagerObj.Width + " ");
                        diameter.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + gateManagerObj.Diameter + " ");
                        installDate.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN to_date(" + "'" + gateManagerObj.InstallDate.ToString("dd/MM/yyyy") + "'" + ", 'DD-MM-YYYY') ");
                        assetString.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + "'" + assetManager.AssetGuid + "'::uuid" + " ");
                        gateTypeGuid.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + "'" + gateManagerObj.GateTypeManager.GateTypeGuid + "'::uuid" + " ");
                        gateConstructionMaterial.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + "'" + gateManagerObj.GateConstructionMaterialManager.GateConstructionMaterialGuid + "'::uuid" + " ");
                        gateGuidString.Append("'" + gateManagerObj.GateGuid + "'" );
                    }
                }

                NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, insertSqlCommand.ToString());

                StringBuilder updateSqlCommand = new StringBuilder();
                updateSqlCommand.Append("Update ");
                updateSqlCommand.Append("mis_asset_gates ");
                updateSqlCommand.Append("SET ");
                updateSqlCommand.Append("number_of_gates= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(numberOfGates.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("height= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(height.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("width= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(width.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("diameter= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(diameter.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("install_date= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(installDate.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("asset_uuid= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(assetString.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("gate_type_uuid= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(gateTypeGuid.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("gate_construction_material_uuid= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(gateConstructionMaterial.ToString());
                updateSqlCommand.Append(" END ");
                updateSqlCommand.Append(" WHERE gate_uuid IN (");
                updateSqlCommand.Append(gateGuidString.ToString());
                updateSqlCommand.Append(" )");
                NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, updateSqlCommand.ToString());
            }
        }

        protected internal void UpdateGateDataForSluice(AssetManager assetManager)
        {
            StringBuilder numberOfGates = new StringBuilder();
            StringBuilder height = new StringBuilder();
            StringBuilder width = new StringBuilder();
            StringBuilder diameter = new StringBuilder();
            StringBuilder installDate = new StringBuilder();
            StringBuilder assetString = new StringBuilder();
            StringBuilder gateTypeGuid = new StringBuilder();
            StringBuilder gateConstructionMaterial = new StringBuilder();
            StringBuilder gateGuidString = new StringBuilder();

            StringBuilder insertSqlCommand = new StringBuilder();
            insertSqlCommand.Append("INSERT INTO ");
            insertSqlCommand.Append("mis_asset_gates ");
            insertSqlCommand.Append("( ");
            insertSqlCommand.Append("gate_uuid, ");
            insertSqlCommand.Append("number_of_gates, ");
            insertSqlCommand.Append("height, ");
            insertSqlCommand.Append("width, ");
            insertSqlCommand.Append("diameter, ");
            insertSqlCommand.Append("install_date, ");
            insertSqlCommand.Append("asset_uuid, ");
            insertSqlCommand.Append("gate_type_uuid, ");
            insertSqlCommand.Append("gate_construction_material_uuid ");
            insertSqlCommand.Append(") ");
            insertSqlCommand.Append("VALUES ");

            if (assetManager.SluiceManager.GateManagerList.Count > 0)
            {
                int i = 0;

                int j = 0;

                foreach (GateManager gateManagerObj in assetManager.SluiceManager.GateManagerList)
                {
                    int index = assetManager.SluiceManager.GateManagerList.IndexOf(gateManagerObj);

                    if (gateManagerObj.GateGuid == Guid.Empty)
                    {
                        i = i + 1;
                        if (i != 1)
                        {
                            insertSqlCommand.Append(", ");
                        }
                        insertSqlCommand.Append("( ");
                        insertSqlCommand.Append("'" + Guid.NewGuid() + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.NumberOfGates + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.Height + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.Width + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.Diameter + "', ");
                        insertSqlCommand.Append("to_date('" + gateManagerObj.InstallDate.ToString("dd/MM/yyyy") + "', 'DD/MM/YYYY'), ");
                        insertSqlCommand.Append("'" + assetManager.AssetGuid + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.GateTypeManager.GateTypeGuid + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.GateConstructionMaterialManager.GateConstructionMaterialGuid + "'");
                        insertSqlCommand.Append(") ");

                    }
                    else
                    {
                        j = j+ 1;
                        if (j != 1)
                        {
                            gateGuidString.Append(", ");
                        }
                        gateGuidString.Append("'" + gateManagerObj.GateGuid + "'");
                        numberOfGates.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + gateManagerObj.NumberOfGates + " ");
                        height.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + gateManagerObj.Height + " ");
                        width.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + gateManagerObj.Width + " ");
                        diameter.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + gateManagerObj.Diameter + " ");
                        installDate.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN to_date(" + "'" + gateManagerObj.InstallDate.ToString("dd/MM/yyyy") + "'" + ", 'DD-MM-YYYY') ");
                        assetString.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + "'" + assetManager.AssetGuid + "'::uuid" + " ");
                        gateTypeGuid.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + "'" + gateManagerObj.GateTypeManager.GateTypeGuid + "'::uuid" + " ");
                        gateConstructionMaterial.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + "'" + gateManagerObj.GateConstructionMaterialManager.GateConstructionMaterialGuid + "'::uuid" + " ");
                        
                    }
                }

                NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, insertSqlCommand.ToString());

                StringBuilder updateSqlCommand = new StringBuilder();
                updateSqlCommand.Append("Update ");
                updateSqlCommand.Append("mis_asset_gates ");
                updateSqlCommand.Append("SET ");
                updateSqlCommand.Append("number_of_gates= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(numberOfGates.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("height= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(height.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("width= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(width.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("diameter= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(diameter.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("install_date= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(installDate.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("asset_uuid= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(assetString.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("gate_type_uuid= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(gateTypeGuid.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("gate_construction_material_uuid= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(gateConstructionMaterial.ToString());
                updateSqlCommand.Append(" END ");
                updateSqlCommand.Append(" WHERE gate_uuid IN (");
                updateSqlCommand.Append(gateGuidString.ToString());
                updateSqlCommand.Append(" )");
                NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, updateSqlCommand.ToString());
            }
        }

        protected internal void UpdateGateDataForTurnout(AssetManager assetManager)
        {
            StringBuilder numberOfGates = new StringBuilder();
            StringBuilder height = new StringBuilder();
            StringBuilder width = new StringBuilder();
            StringBuilder diameter = new StringBuilder();
            StringBuilder installDate = new StringBuilder();
            StringBuilder assetString = new StringBuilder();
            StringBuilder gateTypeGuid = new StringBuilder();
            StringBuilder gateConstructionMaterial = new StringBuilder();
            StringBuilder gateGuidString = new StringBuilder();

            StringBuilder insertSqlCommand = new StringBuilder();
            insertSqlCommand.Append("INSERT INTO ");
            insertSqlCommand.Append("mis_asset_gates ");
            insertSqlCommand.Append("( ");
            insertSqlCommand.Append("gate_uuid, ");
            insertSqlCommand.Append("number_of_gates, ");
            insertSqlCommand.Append("height, ");
            insertSqlCommand.Append("width, ");
            insertSqlCommand.Append("diameter, ");
            insertSqlCommand.Append("install_date, ");
            insertSqlCommand.Append("asset_uuid, ");
            insertSqlCommand.Append("gate_type_uuid, ");
            insertSqlCommand.Append("gate_construction_material_uuid ");
            insertSqlCommand.Append(") ");
            insertSqlCommand.Append("VALUES ");                     

            if (assetManager.TurnoutManager.GateManagerList.Count > 0)
            {
                int i = 0;
                int j = 0;
                foreach (GateManager gateManagerObj in assetManager.TurnoutManager.GateManagerList)
                {
                    int index = assetManager.TurnoutManager.GateManagerList.IndexOf(gateManagerObj);

                    

                    if (gateManagerObj.GateGuid == Guid.Empty)
                    {
                        i = i + 1;
                        if (i != 1)
                        {
                            insertSqlCommand.Append(", ");
                        }
                        insertSqlCommand.Append("( ");  
                        insertSqlCommand.Append("'" + Guid.NewGuid() + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.NumberOfGates + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.Height + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.Width + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.Diameter + "', ");
                        insertSqlCommand.Append("to_date('" + gateManagerObj.InstallDate.ToString("dd/MM/yyyy") + "', 'DD/MM/YYYY'), ");
                        insertSqlCommand.Append("'" + assetManager.AssetGuid + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.GateTypeManager.GateTypeGuid + "', ");
                        insertSqlCommand.Append("'" + gateManagerObj.GateConstructionMaterialManager.GateConstructionMaterialGuid + "'");
                        insertSqlCommand.Append(") ");

                    }
                    else
                    {
                        j = j + 1;
                        if (j != 1)
                        {
                            gateGuidString.Append(", ");
                        }
                        numberOfGates.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + gateManagerObj.NumberOfGates + " ");
                        height.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + gateManagerObj.Height + " ");
                        width.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + gateManagerObj.Width + " ");
                        diameter.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + gateManagerObj.Diameter + " ");
                        installDate.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN to_date(" + "'" + gateManagerObj.InstallDate.ToString("dd/MM/yyyy") + "'" + ", 'DD-MM-YYYY') ");
                        assetString.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + "'" + assetManager.AssetGuid + "'::uuid" + " ");
                        gateTypeGuid.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + "'" + gateManagerObj.GateTypeManager.GateTypeGuid + "'::uuid" + " ");
                        gateConstructionMaterial.Append("WHEN " + "'" + gateManagerObj.GateGuid + "'" + " " + "THEN " + "'" + gateManagerObj.GateConstructionMaterialManager.GateConstructionMaterialGuid + "'::uuid" + " ");
                        gateGuidString.Append("'" + gateManagerObj.GateGuid + "'");

                    }
                }

                NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, insertSqlCommand.ToString());

                StringBuilder updateSqlCommand = new StringBuilder();
                updateSqlCommand.Append("Update ");
                updateSqlCommand.Append("mis_asset_gates ");
                updateSqlCommand.Append("SET ");
                updateSqlCommand.Append("number_of_gates= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(numberOfGates.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("height= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(height.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("width= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(width.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("diameter= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(diameter.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("install_date= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(installDate.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("asset_uuid= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(assetString.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("gate_type_uuid= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(gateTypeGuid.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("gate_construction_material_uuid= ");
                updateSqlCommand.Append("CASE gate_uuid ");
                updateSqlCommand.Append(gateConstructionMaterial.ToString());
                updateSqlCommand.Append(" END ");
                updateSqlCommand.Append(" WHERE gate_uuid IN (");
                updateSqlCommand.Append(gateGuidString.ToString());
                updateSqlCommand.Append(" )");
                NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, updateSqlCommand.ToString());
            }
        }

        protected internal int DeleteGateData(Guid gateGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_gates ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("gate_uuid = :gateGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("gateGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = gateGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

    }
}