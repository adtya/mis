﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class MonitoringItemTypeManager
    {
         #region Private Members

        private Guid monitoringItemTypeGuid;
        private string monitoringItemTypeName;
        //private AssetTypeManager assetTypeManager;
        private List<MonitoringItemManager> monitoringItemManagerList;
        private List<RelationOfAssetMonitoringItemTypeManager> relationOfAssetMonitoringItemTypeManager;
       
       #endregion


        #region Public Constructors

        public MonitoringItemTypeManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid MonitoringItemTypeGuid
        {
            get
            {
                return monitoringItemTypeGuid;
            }
            set
            {
                monitoringItemTypeGuid = value;
            }
        }

        public string MonitoringItemTypeName
        {
            get
            {
                return monitoringItemTypeName;
            }
            set
            {
                monitoringItemTypeName = value;
            }
        }

        //public AssetTypeManager AssetTypeManager
        //{
        //    get
        //    {
        //        if (assetTypeManager == null)
        //        {
        //            assetTypeManager = new AssetTypeManager();
        //        }
        //        return assetTypeManager;
        //    }
        //    set
        //    {
        //        assetTypeManager = value;
        //    }
        //}

        public List<MonitoringItemManager> MonitoringItemManagerList
        {
            get
            {
                return monitoringItemManagerList;
            }
            set
            {
                monitoringItemManagerList = value;
            }
        }

        public List<RelationOfAssetMonitoringItemTypeManager> RelationOfAssetMonitoringItemTypeManager
        {
            get
            {
               return relationOfAssetMonitoringItemTypeManager;
            }
            set
            {
                relationOfAssetMonitoringItemTypeManager = value;
            }
        }

        #endregion
    }
}