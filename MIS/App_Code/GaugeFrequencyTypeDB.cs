﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class GaugeFrequencyTypeDB
    {
        protected internal List<GaugeFrequencyTypeManager> GetGaugeFrequencyTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("frequency_type_uuid, ");
            sqlCommand.Append("frequency_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_gauge_frequency_type ");
            sqlCommand.Append("ORDER BY frequency_type_name ");
            sqlCommand.Append(";");

            List<GaugeFrequencyTypeManager> gaugeFrequencyTypeManagerList = new List<GaugeFrequencyTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    GaugeFrequencyTypeManager gaugeFrequencyTypeManagerObj = new GaugeFrequencyTypeManager();

                    gaugeFrequencyTypeManagerObj.FrequencyTypeGuid = new Guid(reader["frequency_type_uuid"].ToString());
                    gaugeFrequencyTypeManagerObj.FrequencyTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["frequency_type_name"].ToString());

                    gaugeFrequencyTypeManagerList.Add(gaugeFrequencyTypeManagerObj);
                }
            }

            return gaugeFrequencyTypeManagerList;
        }
    }
}