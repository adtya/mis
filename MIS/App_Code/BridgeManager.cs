﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class BridgeManager
    {
        #region Private Members
        private Guid bridgeGuid;
        private double length;
        private int pierNumber;
        private double roadWidth;   
        //private AssetManager assetManager;
        private BridgeTypeManager bridgeTypeManager;        

        #endregion


        #region Public Constructors

        public BridgeManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid BridgeGuid
        {
            get
            {
                return bridgeGuid;
            }
            set
            {
                bridgeGuid = value;
            }
        }

        public double Length
        {
            get
            {
                return length;
            }
            set
            {
                length = value;
            }
        }
       
        public int PierNumber
        {
            get
            {
                return pierNumber;
            }
            set
            {
                pierNumber = value;
            }
        }

        public double RoadWidth
        {
            get
            {
                return roadWidth;
            }
            set
            {
                roadWidth = value;
            }
        }

        public BridgeTypeManager BridgeTypeManager
        {
            get
            {
                if (bridgeTypeManager == null)
                {
                    bridgeTypeManager = new BridgeTypeManager();
                }
                return bridgeTypeManager;
            }
            set
            {
                bridgeTypeManager = value;
            }
        }   
     
        // public AssetManager AssetManager
        //{
        //    get
        //    {
        //        if (assetManager == null)
        //        {
        //            assetManager = new AssetManager();
        //        }
        //        return assetManager;
        //    }
        //    set
        //    {
        //        assetManager = value;
        //    }
        //}

        #endregion
    }    
}