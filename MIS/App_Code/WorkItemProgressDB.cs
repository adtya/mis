﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class WorkItemProgressDB
    {
        protected internal List<WorkItemProgressManager> GetWorkItemProgressBasedOnSubChainage(Guid workItemGuid, Guid subChainageGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.*, ");
            sqlCommand.Append("obj2.work_item_name, ");
            sqlCommand.Append("obj3.unit_symbol, ");
            sqlCommand.Append("obj5.progress_date ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_sub_chainage_work_item_progress obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_project_work_items obj2 ON obj2.work_item_uuid = obj1.work_item_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_units obj3 ON obj3.unit_uuid = (SELECT obj4.unit_uuid FROM mis_project_work_items obj4 WHERE obj4.work_item_uuid = obj1.work_item_uuid) ");
            sqlCommand.Append("LEFT OUTER JOIN mis_project_sub_chainage_physical_progress obj5 ON obj5.physical_progress_uuid = obj1.physical_progress_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.work_item_uuid = :workItemGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj5.sub_chainage_uuid = :subChainageGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj5.progress_date ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("workItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = workItemGuid;

            arParams[1] = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = subChainageGuid;

            List<WorkItemProgressManager> workItemProgressManagerList = new List<WorkItemProgressManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    WorkItemProgressManager workItemProgressManagerObj = new WorkItemProgressManager();

                    workItemProgressManagerObj.WorkItemProgressGuid = new Guid(reader["physical_progress_uuid"].ToString());
                    workItemProgressManagerObj.WorkItemManager.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemProgressManagerObj.WorkItemManager.WorkItemName = reader["work_item_name"].ToString();
                    workItemProgressManagerObj.WorkItemProgressValue = reader["work_item_progress_value"].ToString();
                    workItemProgressManagerObj.WorkItemCumulativeValue = reader["work_item_cumulative_value"].ToString();
                    workItemProgressManagerObj.WorkItemBalanceValue = reader["work_item_balance_value"].ToString();

                    workItemProgressManagerObj.WorkItemManager.UnitManager.UnitSymbol = reader["unit_symbol"].ToString();
                    //workItemProgressManagerObj.

                    workItemProgressManagerList.Add(workItemProgressManagerObj);
                }
            }

            return workItemProgressManagerList;
        }

        protected internal List<WorkItemProgressManager> GetWorkItemProgressBasedOnSubChainagePhysicalProgress(Guid subChainagePhysicalProgressGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.*, ");
            sqlCommand.Append("obj2.work_item_name, ");
            sqlCommand.Append("obj3.unit_symbol ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_sub_chainage_work_item_progress obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_project_work_items obj2 ON obj2.work_item_uuid = obj1.work_item_uuid ");
            sqlCommand.Append("LEFT OUTER JOIN mis_units obj3 ON obj3.unit_uuid = (SELECT obj4.unit_uuid FROM mis_project_work_items obj4 WHERE obj4.work_item_uuid = obj1.work_item_uuid) ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.physical_progress_uuid = :subChainagePhysicalProgressGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("subChainagePhysicalProgressGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = subChainagePhysicalProgressGuid;

            List<WorkItemProgressManager> workItemProgressManagerList = new List<WorkItemProgressManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    WorkItemProgressManager workItemProgressManagerObj = new WorkItemProgressManager();

                    workItemProgressManagerObj.WorkItemProgressGuid = new Guid(reader["physical_progress_uuid"].ToString());
                    workItemProgressManagerObj.WorkItemManager.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemProgressManagerObj.WorkItemManager.WorkItemName = reader["work_item_name"].ToString();
                    workItemProgressManagerObj.WorkItemProgressValue = reader["work_item_progress_value"].ToString();
                    workItemProgressManagerObj.WorkItemManager.UnitManager.UnitSymbol = reader["unit_symbol"].ToString();

                    workItemProgressManagerList.Add(workItemProgressManagerObj);
                }
            }

            return workItemProgressManagerList;
        }

        protected internal int AddProjectWorkItemProgress(SubChainagePhysicalProgressManager subChainagePhysicalProgressManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_sub_chainage_work_item_progress ");
            sqlCommand.Append("( ");
            sqlCommand.Append("work_item_progress_uuid, ");
            sqlCommand.Append("physical_progress_uuid, ");
            sqlCommand.Append("work_item_uuid, ");
            sqlCommand.Append("work_item_progress_value, ");
            sqlCommand.Append("work_item_cumulative_value, ");
            sqlCommand.Append("work_item_balance_value ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            foreach (WorkItemProgressManager workItemProgressManagerObj in subChainagePhysicalProgressManager.WorkItemProgressManager)
            {
                int index = subChainagePhysicalProgressManager.WorkItemProgressManager.IndexOf(workItemProgressManagerObj);

                sqlCommand.Append("( ");

                sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                sqlCommand.Append("'" + subChainagePhysicalProgressManager.PhysicalProgressGuid + "', ");
                sqlCommand.Append("'" + workItemProgressManagerObj.WorkItemManager.WorkItemGuid + "', ");
                sqlCommand.Append("'" + workItemProgressManagerObj.WorkItemProgressValue + "', ");
                sqlCommand.Append("'" + workItemProgressManagerObj.WorkItemCumulativeValue + "', ");
                sqlCommand.Append("'" + workItemProgressManagerObj.WorkItemBalanceValue + "' ");

                if (index == subChainagePhysicalProgressManager.WorkItemProgressManager.Count - 1)
                {
                    sqlCommand.Append(") ");
                }
                else
                {
                    sqlCommand.Append("), ");
                }
            }

            sqlCommand.Append(";");

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());

            if (rowsAffected > 0)
            {
                new SubChainageAndWorkItemsRelationDB().UpdateSubChainageAndWorkItemsRelationValues(subChainagePhysicalProgressManager);
            }

            return rowsAffected;
        }
    }
}