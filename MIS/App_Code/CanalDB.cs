﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class CanalDB
    {
        protected internal int AddCanalData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_canal ");
            sqlCommand.Append("( ");
            sqlCommand.Append("canal_uuid, ");
            sqlCommand.Append("bed_elevation1, ");
            sqlCommand.Append("bed_elevation2, ");
            sqlCommand.Append("length, ");
            sqlCommand.Append("bed_width, ");
            sqlCommand.Append("slope, ");
            sqlCommand.Append("slope2, ");
            sqlCommand.Append("average_depth, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("lining_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":canalGuid, ");
            sqlCommand.Append(":usElevation, ");
            sqlCommand.Append(":dsElevation, ");
            sqlCommand.Append(":length, ");
            sqlCommand.Append(":bedWidth, ");
            sqlCommand.Append(":slope1, ");
            sqlCommand.Append(":slope2, ");
            sqlCommand.Append(":averageDepth, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":liningTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[10];

            arParams[0] = new NpgsqlParameter("canalGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("usElevation", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.CanalManager.UsElevation;

            arParams[2] = new NpgsqlParameter("dsElevation", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.CanalManager.DSElevation;

            arParams[3] = new NpgsqlParameter("length", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.CanalManager.Length;

            arParams[4] = new NpgsqlParameter("bedWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.CanalManager.BedWidth;

            arParams[5] = new NpgsqlParameter("slope1", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.CanalManager.Slope1;

            arParams[6] = new NpgsqlParameter("slope2", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.CanalManager.Slope2;

            arParams[7] = new NpgsqlParameter("averageDepth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.CanalManager.AverageDepth;

            arParams[8] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.AssetGuid;

            arParams[9] = new NpgsqlParameter("liningTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = assetManager.CanalManager.CanalLiningTypeManager.LiningTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateCanalData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_canal ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("bed_elevation1= :usElevation, ");
            sqlCommand.Append("bed_elevation2= :dsElevation, ");
            sqlCommand.Append("length= :length, ");
            sqlCommand.Append("bed_width= :bedWidth, ");
            sqlCommand.Append("slope= :slope1, ");
            sqlCommand.Append("slope2= :slope2, ");
            sqlCommand.Append("average_depth= :averageDepth, ");
            sqlCommand.Append("lining_type_uuid= :liningTypeGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[10];

            arParams[0] = new NpgsqlParameter("canalGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.CanalManager.CanalGuid;

            arParams[1] = new NpgsqlParameter("usElevation", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.CanalManager.UsElevation;

            arParams[2] = new NpgsqlParameter("dsElevation", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.CanalManager.DSElevation;

            arParams[3] = new NpgsqlParameter("length", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.CanalManager.Length;

            arParams[4] = new NpgsqlParameter("bedWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.CanalManager.BedWidth;

            arParams[5] = new NpgsqlParameter("slope1", NpgsqlTypes.NpgsqlDbType.Integer);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.CanalManager.Slope1;

            arParams[6] = new NpgsqlParameter("slope2", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.CanalManager.Slope2;

            arParams[7] = new NpgsqlParameter("averageDepth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.CanalManager.AverageDepth;

            arParams[8] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[8].Direction = ParameterDirection.Input;
            arParams[8].Value = assetManager.AssetGuid;

            arParams[9] = new NpgsqlParameter("liningTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[9].Direction = ParameterDirection.Input;
            arParams[9].Value = assetManager.CanalManager.CanalLiningTypeManager.LiningTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }
}