﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using System.Text;
using System.Data;
using System.Globalization;
namespace MIS.App_Code
{
    public class WorkItemDB
    {
        protected internal WorkItemManager GetWorkItem(Guid workItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_uuid, ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.work_item_uuid = :workItemGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("workItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = workItemGuid;

            WorkItemManager workItemManagerObj = new WorkItemManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    workItemManagerObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemManagerObj.WorkItemName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["work_item_name"].ToString());
                }
            }

            return workItemManagerObj;

        }

        protected internal WorkItemManager GetWorkItemWithUnit(Guid workItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_uuid, ");
            sqlCommand.Append("obj1.work_item_name, ");
            sqlCommand.Append("obj1.unit_uuid ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.work_item_uuid = :workItemGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("workItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = workItemGuid;

            WorkItemManager workItemManagerObj = new WorkItemManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    workItemManagerObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemManagerObj.WorkItemName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["work_item_name"].ToString());

                    workItemManagerObj.UnitManager = new UnitDB().GetUnitWithUnitType(new Guid(reader["unit_uuid"].ToString()));
                }
            }

            return workItemManagerObj;

        }

        protected internal WorkItemManager GetWorkItemWithWorkItemSubHeads(Guid workItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_uuid, ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.work_item_uuid = :workItemGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("workItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = workItemGuid;

            WorkItemManager workItemManagerObj = new WorkItemManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    workItemManagerObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemManagerObj.WorkItemName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["work_item_name"].ToString());

                    workItemManagerObj.WorkItemSubHeadManager = new WorkItemSubHeadDB().GetWorkItemSubHeadsBasedOnWorkItem(workItemManagerObj.WorkItemGuid);
                }
            }

            return workItemManagerObj;

        }

        protected internal WorkItemManager GetWorkItemWithUnitAndWorkItemSubHeads(Guid workItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_uuid, ");
            sqlCommand.Append("obj1.work_item_name, ");
            sqlCommand.Append("obj1.unit_uuid ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.work_item_uuid = :workItemGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("workItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = workItemGuid;

            WorkItemManager workItemManagerObj = new WorkItemManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    workItemManagerObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemManagerObj.WorkItemName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["work_item_name"].ToString());

                    workItemManagerObj.UnitManager = new UnitDB().GetUnitWithUnitType(new Guid(reader["unit_uuid"].ToString()));

                    workItemManagerObj.WorkItemSubHeadManager = new WorkItemSubHeadDB().GetWorkItemSubHeadsBasedOnWorkItem(workItemManagerObj.WorkItemGuid);
                }
            }

            return workItemManagerObj;

        }


        protected internal List<WorkItemManager> GetWorkItems()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_uuid, ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append(";");

            List<WorkItemManager> workItemManagerList = new List<WorkItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    WorkItemManager workItemManagerObj = new WorkItemManager();

                    workItemManagerObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemManagerObj.WorkItemName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["work_item_name"].ToString());

                    workItemManagerList.Add(workItemManagerObj);
                }
            }

            return workItemManagerList;

        }

        protected internal List<WorkItemManager> GetWorkItemsWithUnit()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_uuid, ");
            sqlCommand.Append("obj1.work_item_name, ");
            sqlCommand.Append("obj1.unit_uuid ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append(";");

            List<WorkItemManager> workItemManagerList = new List<WorkItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    WorkItemManager workItemManagerObj = new WorkItemManager();

                    workItemManagerObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemManagerObj.WorkItemName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["work_item_name"].ToString());

                    workItemManagerObj.UnitManager = new UnitDB().GetUnitWithUnitType(new Guid(reader["unit_uuid"].ToString()));

                    workItemManagerList.Add(workItemManagerObj);
                }
            }

            return workItemManagerList;

        }

        protected internal List<WorkItemManager> GetWorkItemsWithWorkItemSubHeads()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_uuid, ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append(";");

            List<WorkItemManager> workItemManagerList = new List<WorkItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    WorkItemManager workItemManagerObj = new WorkItemManager();

                    workItemManagerObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemManagerObj.WorkItemName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["work_item_name"].ToString());

                    workItemManagerObj.WorkItemSubHeadManager = new WorkItemSubHeadDB().GetWorkItemSubHeadsBasedOnWorkItem(workItemManagerObj.WorkItemGuid);

                    workItemManagerList.Add(workItemManagerObj);
                }
            }

            return workItemManagerList;

        }

        protected internal List<WorkItemManager> GetWorkItemsWithUnitAndWorkItemSubHeads()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_uuid, ");
            sqlCommand.Append("obj1.work_item_name, ");
            sqlCommand.Append("obj1.unit_uuid ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append(";");

            List<WorkItemManager> workItemManagerList = new List<WorkItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    WorkItemManager workItemManagerObj = new WorkItemManager();

                    workItemManagerObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemManagerObj.WorkItemName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["work_item_name"].ToString());

                    workItemManagerObj.UnitManager = new UnitDB().GetUnitWithUnitType(new Guid(reader["unit_uuid"].ToString()));

                    workItemManagerObj.WorkItemSubHeadManager = new WorkItemSubHeadDB().GetWorkItemSubHeadsBasedOnWorkItem(workItemManagerObj.WorkItemGuid);

                    workItemManagerList.Add(workItemManagerObj);
                }
            }

            return workItemManagerList;

        }


        protected internal List<WorkItemManager> GetWorkItemsBasedOnSubChainage(Guid subChainageGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_uuid, ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.work_item_uuid = ANY(SELECT obj2.work_item_uuid FROM mis_project_sub_chainage_and_work_items_relation obj2 WHERE obj2.sub_chainage_uuid = :subChainageGuid) ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = subChainageGuid;

            List<WorkItemManager> workItemManagerList = new List<WorkItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    WorkItemManager workItemManagerObj = new WorkItemManager();

                    workItemManagerObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemManagerObj.WorkItemName = reader["work_item_name"].ToString();

                    workItemManagerList.Add(workItemManagerObj);
                }
            }

            return workItemManagerList;

        }

        protected internal List<WorkItemManager> GetWorkItemsWithUnitBasedOnSubChainage(Guid subChainageGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            //sqlCommand.Append("SELECT ");
            //sqlCommand.Append("obj1.work_item_uuid, ");
            //sqlCommand.Append("obj1.work_item_name, ");
            //sqlCommand.Append("obj1.unit_uuid ");
            //sqlCommand.Append("FROM ");
            //sqlCommand.Append("mis_project_work_items obj1 ");
            //sqlCommand.Append("WHERE ");
            //sqlCommand.Append("obj1.work_item_uuid = ANY(SELECT obj2.work_item_uuid FROM mis_project_sub_chainage_and_work_items_relation obj2 WHERE obj2.sub_chainage_uuid = :subChainageGuid) ");
            //sqlCommand.Append("ORDER BY ");
            //sqlCommand.Append("obj1.work_item_name ");
            //sqlCommand.Append(";");

            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_uuid, ");
            sqlCommand.Append("obj1.work_item_name, ");
            sqlCommand.Append("obj1.unit_uuid, ");
            sqlCommand.Append("obj2.work_item_total_value, ");
            sqlCommand.Append("obj2.work_item_cumulative_value, ");
            sqlCommand.Append("obj2.work_item_balance_value ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_project_sub_chainage_and_work_items_relation obj2 ON obj2.work_item_uuid = obj1.work_item_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj2.sub_chainage_uuid = :subChainageGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = subChainageGuid;

            List<WorkItemManager> workItemManagerList = new List<WorkItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    WorkItemManager workItemManagerObj = new WorkItemManager();

                    workItemManagerObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemManagerObj.WorkItemName = reader["work_item_name"].ToString();
                    workItemManagerObj.WorkItemTotalValue = reader["work_item_total_value"].ToString();
                    workItemManagerObj.WorkItemCumulativeValue = reader["work_item_cumulative_value"].ToString();
                    workItemManagerObj.WorkItemBalanceValue = reader["work_item_balance_value"].ToString();

                    workItemManagerObj.UnitManager = new UnitDB().GetUnit(new Guid(reader["unit_uuid"].ToString()));

                    workItemManagerList.Add(workItemManagerObj);
                }
            }

            return workItemManagerList;

        }

        protected internal List<WorkItemManager> GetWorkItemsWithWorkItemSubHeadsBasedOnSubChainage(Guid subChainageGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_uuid, ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.work_item_uuid = ANY(SELECT obj2.work_item_uuid FROM mis_project_sub_chainage_and_work_items_relation obj2 WHERE obj2.sub_chainage_uuid = :subChainageGuid) ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = subChainageGuid;

            List<WorkItemManager> workItemManagerList = new List<WorkItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    WorkItemManager workItemManagerObj = new WorkItemManager();

                    workItemManagerObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemManagerObj.WorkItemName = reader["work_item_name"].ToString();

                    workItemManagerObj.WorkItemSubHeadManager = new WorkItemSubHeadDB().GetWorkItemSubHeadsBasedOnWorkItem(workItemManagerObj.WorkItemGuid);

                    workItemManagerList.Add(workItemManagerObj);
                }
            }

            return workItemManagerList;

        }

        protected internal List<WorkItemManager> GetWorkItemsWithUnitAndWorkItemSubHeadsBasedOnSubChainage(Guid subChainageGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.work_item_uuid, ");
            sqlCommand.Append("obj1.work_item_name, ");
            sqlCommand.Append("obj1.unit_uuid ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.work_item_uuid = ANY(SELECT obj2.work_item_uuid FROM mis_project_sub_chainage_and_work_items_relation obj2 WHERE obj2.sub_chainage_uuid = :subChainageGuid) ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.work_item_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("subChainageGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = subChainageGuid;

            List<WorkItemManager> workItemManagerList = new List<WorkItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    WorkItemManager workItemManagerObj = new WorkItemManager();

                    workItemManagerObj.WorkItemGuid = new Guid(reader["work_item_uuid"].ToString());
                    workItemManagerObj.WorkItemName = reader["work_item_name"].ToString();

                    workItemManagerObj.UnitManager = new UnitDB().GetUnitWithUnitType(new Guid(reader["unit_uuid"].ToString()));

                    workItemManagerObj.WorkItemSubHeadManager = new WorkItemSubHeadDB().GetWorkItemSubHeadsBasedOnWorkItem(workItemManagerObj.WorkItemGuid);

                    workItemManagerList.Add(workItemManagerObj);
                }
            }

            return workItemManagerList;

        }




        protected internal bool CheckWorkItemExistence(string workItemName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.work_item_name = :workItemName ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("workItemName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(workItemName);

            bool result = false;

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    result = true;
                }
            }

            return result;
        }

        protected internal int AddWorkItem(WorkItemManager workItemManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_work_items ");
            sqlCommand.Append("( ");
            sqlCommand.Append("work_item_uuid, ");
            sqlCommand.Append("work_item_name, ");
            sqlCommand.Append("unit_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":workItemGuid, ");
            sqlCommand.Append(":workItemName, ");
            sqlCommand.Append(":workItemUnitGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("workItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = workItemManager.WorkItemGuid;

            arParams[1] = new NpgsqlParameter("workItemName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(workItemManager.WorkItemName);

            arParams[2] = new NpgsqlParameter("workItemUnitGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = workItemManager.UnitManager.UnitGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            if (result == 1)
            {
                new WorkItemAndWorkItemSubHeadsRelationDB().AddWorkItemAndWorkItemSubHeadRelation(workItemManager);
            }

            return result;

        }

        protected internal int UpdateWorkItem(WorkItemManager workItemManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_project_work_items ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("work_item_name = :workItemName, ");
            sqlCommand.Append("unit_uuid = :workItemUnitGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("work_item_uuid = :workItemGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("workItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = workItemManager.WorkItemGuid;

            arParams[1] = new NpgsqlParameter("workItemName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(workItemManager.WorkItemName);

            arParams[2] = new NpgsqlParameter("workItemUnitGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = workItemManager.UnitManager.UnitGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            if (result == 1)
            {
                new WorkItemAndWorkItemSubHeadsRelationDB().UpdateWorkItemAndWorkItemSubHeadRelation(workItemManager);
            }

            return result;
        }

        protected internal int UpdateWorkItemWithoutWorkItemName(WorkItemManager workItemManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_project_work_items ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("unit_uuid = :workItemUnitGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("work_item_uuid = :workItemGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("workItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = workItemManager.WorkItemGuid;

            arParams[1] = new NpgsqlParameter("workItemUnitGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = workItemManager.UnitManager.UnitGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            if (result == 1)
            {
                new WorkItemAndWorkItemSubHeadsRelationDB().UpdateWorkItemAndWorkItemSubHeadRelation(workItemManager);
            }

            return result;
        }

        protected internal int DeleteWorkItem(Guid workItemGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_work_items ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("work_item_uuid = :workItemGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("workItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = workItemGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

    }
}