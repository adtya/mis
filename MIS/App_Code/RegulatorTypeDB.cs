﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RegulatorTypeDB
    {
        protected internal List<RegulatorTypeManager> GetRegulatorTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("regulator_type_uuid, ");
            sqlCommand.Append("regulator_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_regulator_type ");
            sqlCommand.Append("ORDER BY regulator_type_name ");
            sqlCommand.Append(";");

            List<RegulatorTypeManager> regulatorTypeManagerList = new List<RegulatorTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RegulatorTypeManager regulatorTypeManagerObj = new RegulatorTypeManager();

                    regulatorTypeManagerObj.RegulatorTypeGuid = new Guid(reader["regulator_type_uuid"].ToString());
                    regulatorTypeManagerObj.RegulatorTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["regulator_type_name"].ToString());

                    regulatorTypeManagerList.Add(regulatorTypeManagerObj);
                }
            }

            return regulatorTypeManagerList;
        }
    }
}