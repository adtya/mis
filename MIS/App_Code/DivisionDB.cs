﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class DivisionDB
    {
        protected internal DivisionManager GetDivision(Guid divisionGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.division_uuid, ");
            sqlCommand.Append("obj1.division_code, ");
            sqlCommand.Append("obj1.division_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_divisions obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.division_uuid = :divisionGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = divisionGuid;

            DivisionManager divisionManagerObj = new DivisionManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    divisionManagerObj.DivisionGuid = new Guid(reader["division_uuid"].ToString());
                    divisionManagerObj.DivisionCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["division_code"].ToString());
                    divisionManagerObj.DivisionName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["division_name"].ToString());
                }
            }

            return divisionManagerObj;

        }

        protected internal DivisionManager GetDivisionDataWithCircle(Guid divisionGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");            
            sqlCommand.Append("obj1.division_code, ");
            sqlCommand.Append("obj1.division_name, ");
            sqlCommand.Append("obj2.circle_name,");
            sqlCommand.Append("obj2.circle_code ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_divisions obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_circles obj2 ON ");
            sqlCommand.Append("obj1.circle_uuid = obj2.circle_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.division_uuid = :divisionGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = divisionGuid;

            DivisionManager divisionManagerObj = new DivisionManager();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    divisionManagerObj.DivisionCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["division_code"].ToString());
                    divisionManagerObj.DivisionName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["division_name"].ToString());

                    divisionManagerObj.CircleManager.CircleCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["circle_code"].ToString());
                    divisionManagerObj.CircleManager.CircleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["circle_name"].ToString());
                }
            }

            return divisionManagerObj;

        }

        protected internal List<DivisionManager> GetDivisions()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.division_uuid, ");
            sqlCommand.Append("obj1.division_name,");
            sqlCommand.Append("obj1.division_code ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_divisions obj1 ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.division_name ");
            sqlCommand.Append(";");

            List<DivisionManager> divisionManagerList = new List<DivisionManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    DivisionManager divisionManagerObj = new DivisionManager();

                    divisionManagerObj.DivisionGuid = new Guid(reader["division_uuid"].ToString());
                    divisionManagerObj.DivisionName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["division_name"].ToString());
                    divisionManagerObj.DivisionCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["division_code"].ToString());

                    divisionManagerList.Add(divisionManagerObj);
                }
            }

            return divisionManagerList;
        }

        protected internal List<DivisionManager> GetDivisionsWithCircle()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.division_uuid, ");
            sqlCommand.Append("obj1.division_name,");
            sqlCommand.Append("obj1.division_code, ");
            sqlCommand.Append("obj2.circle_name,");
            sqlCommand.Append("obj2.circle_code ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_divisions obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_circles obj2 ON ");
            sqlCommand.Append("obj1.circle_uuid = obj2.circle_uuid ");
            sqlCommand.Append(";");

            List<DivisionManager> divisionManagerList = new List<DivisionManager>();           

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    DivisionManager divisionManagerObj = new DivisionManager();

                    divisionManagerObj.DivisionGuid = new Guid(reader["division_uuid"].ToString());
                    divisionManagerObj.DivisionName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["division_name"].ToString());
                    divisionManagerObj.DivisionCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["division_code"].ToString());

                    divisionManagerObj.CircleManager.CircleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["circle_name"].ToString());
                    divisionManagerObj.CircleManager.CircleCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["circle_code"].ToString());

                    divisionManagerList.Add(divisionManagerObj);
                }
            }

            return divisionManagerList;
        }

        protected internal List<DivisionManager> GetDivisionsBasedOnCircleGuid(Guid circleGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.division_uuid, ");
            sqlCommand.Append("obj1.division_name,");
            sqlCommand.Append("obj1.division_code ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_divisions obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.circle_uuid = :circleGuid ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("obj1.division_name ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = circleGuid;

            List<DivisionManager> divisionManagerList = new List<DivisionManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    DivisionManager divisionManagerObj = new DivisionManager();

                    divisionManagerObj.DivisionGuid = new Guid(reader["division_uuid"].ToString());
                    divisionManagerObj.DivisionName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["division_name"].ToString());
                    divisionManagerObj.DivisionCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["division_code"].ToString());

                    divisionManagerList.Add(divisionManagerObj);
                }
            }

            return divisionManagerList;
        }

        protected internal int AddDivision(Guid divisionGuid, string divisionCode, string divisionName, Guid circleGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_divisions ");
            sqlCommand.Append("( ");
            sqlCommand.Append("division_uuid, ");
            sqlCommand.Append("division_name, ");
            sqlCommand.Append("division_code, ");
            sqlCommand.Append("circle_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":divisionGuid, ");
            sqlCommand.Append(":divisionName, ");
            sqlCommand.Append(":divisionCode, ");
            sqlCommand.Append(":circleGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[4];

            arParams[0] = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = divisionGuid;

            arParams[1] = new NpgsqlParameter("divisionName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(divisionName);

            arParams[2] = new NpgsqlParameter("divisionCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = CultureInfo.CurrentCulture.TextInfo.ToUpper(divisionCode);

            arParams[3] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = circleGuid;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;
        }

        protected internal bool UpdateDivision(Guid divisionGuid, string newDivisionCode, string newDivisionName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_divisions ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("division_name = :newDivisionName, ");
            sqlCommand.Append("division_code = :newDivisionCode ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("division_uuid = :divisionGuid ");            
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("newDivisionName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(newDivisionName);

            arParams[1] = new NpgsqlParameter("newDivisionCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToUpper(newDivisionCode);

            arParams[2] = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = divisionGuid;            

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return (result > 0);
        }

        protected internal int DeleteDivision(Guid divisionGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_divisions ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("division_uuid = :divisionGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("divisionGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = divisionGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal bool CheckDivisionExistence(string divisionCode, string divisionName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_divisions obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.division_name = :divisionName ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.division_code = :divisionCode ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("divisionName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(divisionName);

            arParams[1] = new NpgsqlParameter("divisionCode", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(divisionCode);

            bool result = false;

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    result = true;
                }
            }

            return result;
        }

        //protected internal List<DivisionManager> GetDivisionsFromCircleGuid(Guid circleGuid)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("SELECT ");
        //    sqlCommand.Append("division_uuid, ");
        //    sqlCommand.Append("division_name, ");
        //    sqlCommand.Append("division_code ");
        //    sqlCommand.Append("FROM ");
        //    sqlCommand.Append("mis_divisions ");
        //    sqlCommand.Append("WHERE ");
        //    sqlCommand.Append("circle_uuid = :circleGuid ");
        //    sqlCommand.Append("ORDER BY division_name ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter[] arParams = new NpgsqlParameter[1];

        //    arParams[0] = new NpgsqlParameter("circleGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[0].Direction = ParameterDirection.Input;
        //    arParams[0].Value = circleGuid;

        //    List<DivisionManager> divisionList = new List<DivisionManager>();           

        //    using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
        //    {
        //        while (reader.Read())
        //        {
        //            DivisionManager divisionObj = new DivisionManager();

        //            divisionObj.DivisionGuid = new Guid(reader["division_uuid"].ToString());
        //            divisionObj.DivisionName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["division_name"].ToString());
        //            divisionObj.DivisionCode = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["division_code"].ToString());

        //            divisionList.Add(divisionObj);
        //        }
        //    }

        //    return divisionList;

        //}

    }
}