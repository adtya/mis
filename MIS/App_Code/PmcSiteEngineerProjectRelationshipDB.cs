﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class PmcSiteEngineerProjectRelationshipDB
    {
        protected internal int SavePmcSiteEngineerRelationship(List<PmcSiteEngineerProjectRelationshipManager> pmcSiteEngineerList)
        {
            List<PmcSiteEngineerProjectRelationshipManager> subChainageWorkItemRelationManagerList = new List<PmcSiteEngineerProjectRelationshipManager>();

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_pmc_site_engineer_project_relationship ");
            sqlCommand.Append("( ");
            sqlCommand.Append("pmc_site_engineer_project_relationship_uuid, ");
            sqlCommand.Append("project_uuid, ");
            sqlCommand.Append("pmc_site_engineer_name ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            foreach (PmcSiteEngineerProjectRelationshipManager pmcSiteEngineerProjectRelationshipObj in pmcSiteEngineerList)
            {
                int index = pmcSiteEngineerList.IndexOf(pmcSiteEngineerProjectRelationshipObj);

                sqlCommand.Append("( ");

                sqlCommand.Append("'" + pmcSiteEngineerProjectRelationshipObj.PmcSiteEngineerProjectRelationshipGuid + "', ");
                sqlCommand.Append("'" + pmcSiteEngineerProjectRelationshipObj.ProjectManager.ProjectGuid + "', ");
                sqlCommand.Append("'" + pmcSiteEngineerProjectRelationshipObj.SiteEngineerName + "' ");

                if (index == pmcSiteEngineerList.Count - 1)
                {
                    sqlCommand.Append(") ");
                }
                else
                {
                    sqlCommand.Append("), ");
                }
            }

            sqlCommand.Append(";");

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());

            return rowsAffected;
        }

        protected internal List<PmcSiteEngineerProjectRelationshipManager> GetPmcSiteEngineersListRelatedToProject(Guid projectGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_pmc_site_engineer_project_relationship ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = projectGuid;

            List<PmcSiteEngineerProjectRelationshipManager> pmcSiteEngineerProjectRelationshipManagerList = new List<PmcSiteEngineerProjectRelationshipManager>();

            using (NpgsqlDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    PmcSiteEngineerProjectRelationshipManager pmcSiteEngineerProjectRelationshipManagerObj = new PmcSiteEngineerProjectRelationshipManager();

                    pmcSiteEngineerProjectRelationshipManagerObj.PmcSiteEngineerProjectRelationshipGuid = new Guid(reader["pmc_site_engineer_project_relationship_uuid"].ToString());
                    pmcSiteEngineerProjectRelationshipManagerObj.SiteEngineerName = reader["pmc_site_engineer_name"].ToString();

                    pmcSiteEngineerProjectRelationshipManagerList.Add(pmcSiteEngineerProjectRelationshipManagerObj);
                }
            }
            return pmcSiteEngineerProjectRelationshipManagerList;
        }

    }
}