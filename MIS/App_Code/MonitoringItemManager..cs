﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class MonitoringItemManager
    {
        #region Private Members

        private Guid monitoringItemGuid;
        private string monitoringItemName;
        private MonitoringItemTypeManager monitoringItemTypeManager;
       
       #endregion


        #region Public Constructors

        public MonitoringItemManager()
        {

        }

        #endregion

        #region Public Getter/Setter Properties

        public Guid MonitoringItemGuid
        {
            get
            {
                return monitoringItemGuid;
            }
            set
            {
                monitoringItemGuid = value;
            }
        }

        public string MonitoringItemName
        {
            get
            {
                return monitoringItemName;
            }
            set
            {
                monitoringItemName = value;
            }
        }

        public MonitoringItemTypeManager MonitoringItemTypeManager
        {
            get
            {
                if (monitoringItemTypeManager == null)
                {
                    monitoringItemTypeManager = new MonitoringItemTypeManager();
                }
                return monitoringItemTypeManager;
            }
            set
            {
                monitoringItemTypeManager = value;
            }
        }

        #endregion
    }
}