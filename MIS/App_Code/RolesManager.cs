﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RolesManager
    {
        private Guid roleGuid;
        private string roleName;

        public Guid RoleGuid
        {
            get
            {
                return roleGuid;
            }
            set
            {
                roleGuid = value;
            }
        }

        public string RoleName
        {
            get
            {
                return roleName;
            }
            set
            {
                roleName = value;
            }
        }

    }
}