﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class Units
    {
   
        #region Private Members

        private Guid unitGuid;
        private string unitName;
        private string unitSymbol;

        #endregion

        #region Public Getter/Setter Properties

        public Guid UnitGuid
        {
            get
            {
                return unitGuid;
            }
            set
            {
                unitGuid = value;
            }
        }

        public string UnitName
        {
            get
            {
                return unitName;
            }
            set
            {
                unitName = value;
            }
        }

        public string UnitSymbol
        {
            get
            {
                return unitSymbol;
            }
            set
            {
                unitSymbol = value;
            }
        }

        #endregion
    }
}