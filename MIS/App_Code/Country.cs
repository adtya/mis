﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class Country
    {
        private Guid _countryGuid;
        private string _countryName;
        private string _countryISOCode2;
        private string _countryISOCode3;
        private string _countryISOCodeNumeric;

        public Country()
        {

        }

        public Guid CountryGuid
        {
            get
            {
                return _countryGuid;
            }
            set
            {
                _countryGuid = value;
            }
        }

        public string CountryName
        {
            get
            {
                return _countryName;
            }
            set
            {
                _countryName = value;
            }
        }

        public string CountryISOCode2
        {
            get
            {
                return _countryISOCode2;
            }
            set
            {
                _countryISOCode2 = value;
            }
        }

        public string CountryISOCode3
        {
            get
            {
                return _countryISOCode3;
            }
            set
            {
                _countryISOCode3 = value;
            }
        }

        public string CountryISOCodeNumeric
        {
            get
            {
                return _countryISOCodeNumeric;
            }
            set
            {
                _countryISOCodeNumeric = value;
            }
        }
    }
}