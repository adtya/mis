﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class MonitoringItemPerformanceDB
    {        
        protected internal void AddItemPerformance(PerformanceMonitoringManager performanceMonitoringManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_monitoring_item_performance ");
            sqlCommand.Append("( ");
            sqlCommand.Append("monitoring_item_performance_uuid, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("current_monitoring_date, ");
            sqlCommand.Append("monitoring_item_uuid, ");
            sqlCommand.Append("monitoring_item_performance_status_uuid, ");
            sqlCommand.Append("monitoring_item_performance_description, ");
            sqlCommand.Append("performance_monitoring_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            if (performanceMonitoringManager.MonitoringItemPerformanceManager.Count > 0)
            {
                int i = 0;
                for (i = 0; i <= performanceMonitoringManager.MonitoringItemPerformanceManager.Count - 2; i++)
                {
                    sqlCommand.Append("( ");
                    sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                    sqlCommand.Append("'" + performanceMonitoringManager.AssetManager.AssetGuid.ToString() + "', ");
                    sqlCommand.Append("'" + performanceMonitoringManager.MonitoringItemPerformanceManager[i].SystemVariablesManager.CurrentMonitoringDate + "', ");
                    sqlCommand.Append("'" + performanceMonitoringManager.MonitoringItemPerformanceManager[i].MonitoringItemManager.MonitoringItemGuid.ToString() + "', ");
                    sqlCommand.Append("'" + performanceMonitoringManager.MonitoringItemPerformanceManager[i].OverallStatusManager.OverallStatusGuid.ToString() + "', ");
                    sqlCommand.Append("'" + performanceMonitoringManager.MonitoringItemPerformanceManager[i].MonitoringItemPerformanceDescription.ToString() + "', ");
                    sqlCommand.Append("'" + performanceMonitoringManager.PerformanceMonitoringGuid.ToString() + "' ");
                    sqlCommand.Append("), ");
                }

                    sqlCommand.Append("( ");
                    sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                    sqlCommand.Append("'" + performanceMonitoringManager.AssetManager.AssetGuid.ToString() + "', ");
                    sqlCommand.Append("'" + performanceMonitoringManager.MonitoringItemPerformanceManager[i].SystemVariablesManager.CurrentMonitoringDate + "', ");
                    sqlCommand.Append("'" + performanceMonitoringManager.MonitoringItemPerformanceManager[i].MonitoringItemManager.MonitoringItemGuid.ToString() + "', ");
                    sqlCommand.Append("'" + performanceMonitoringManager.MonitoringItemPerformanceManager[i].OverallStatusManager.OverallStatusGuid.ToString() + "', ");
                    sqlCommand.Append("'" + performanceMonitoringManager.MonitoringItemPerformanceManager[i].MonitoringItemPerformanceDescription.ToString() + "', ");
                    sqlCommand.Append("'" + performanceMonitoringManager.PerformanceMonitoringGuid.ToString() + "' ");
                    sqlCommand.Append(") ");
                
                sqlCommand.Append(";");

                NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());
            }           
        }

        //protected internal int AddMonitoringItem(Guid monitoringItemGuid, string monitoringItemName, Guid monitoringItemTypeGuid)
        //{
        //    StringBuilder sqlCommand = new StringBuilder();
        //    sqlCommand.Append("INSERT INTO ");
        //    sqlCommand.Append("mis_om_assettype_monitoring_itemtype_relation ");
        //    sqlCommand.Append("( ");
        //    sqlCommand.Append("relation_uuid, ");
        //    sqlCommand.Append("asset_type_uuid, ");
        //    sqlCommand.Append("monitoring_item_type_uuid ");
        //    sqlCommand.Append(") ");
        //    sqlCommand.Append("VALUES ");
        //    sqlCommand.Append("( ");
        //    sqlCommand.Append(":monitoringItemGuid, ");
        //    sqlCommand.Append(":monitoringItemName, ");
        //    sqlCommand.Append(":monitoringItemTypeGuid ");
        //    sqlCommand.Append(") ");
        //    sqlCommand.Append(";");

        //    NpgsqlParameter[] arParams = new NpgsqlParameter[3];

        //    arParams[0] = new NpgsqlParameter("monitoringItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[0].Direction = ParameterDirection.Input;
        //    arParams[0].Value = monitoringItemGuid;

        //    arParams[1] = new NpgsqlParameter("monitoringItemName", NpgsqlTypes.NpgsqlDbType.Text);
        //    arParams[1].Direction = ParameterDirection.Input;
        //    arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(monitoringItemName);

        //    arParams[2] = new NpgsqlParameter("monitoringItemTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
        //    arParams[2].Direction = ParameterDirection.Input;
        //    arParams[2].Value = monitoringItemTypeGuid;

        //    int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

        //    return rowsAffected;
        //}

        protected internal List<MonitoringItemPerformanceManager> GetMonitoringItemPerformanceDataByAssetUuid(Guid assetGuid, DateTime currentMonitoringDate,Guid assetTypeGuid,Guid performanceMonitoringGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.monitoring_item_performance_uuid, ");
            sqlCommand.Append("obj1.monitoring_item_performance_description, ");
            sqlCommand.Append("obj2.monitoring_item_uuid, ");
            sqlCommand.Append("obj2.monitoring_item_name, ");
            sqlCommand.Append("obj3.monitoring_item_type_uuid, ");
            sqlCommand.Append("obj3.monitoring_item_type_name,  ");
            sqlCommand.Append("obj4.overall_status_uuid, ");
            sqlCommand.Append("obj4.overall_status ");          
            sqlCommand.Append("FROM mis_om_monitoring_item_performance obj1 ");
            sqlCommand.Append("LEFT JOIN mis_om_monitoring_item obj2 ON obj1.monitoring_item_uuid = obj2.monitoring_item_uuid ");
            sqlCommand.Append("LEFT JOIN mis_om_monitoring_item_type obj3 ON obj3.monitoring_item_type_uuid = obj2.monitoring_item_type_uuid ");
            sqlCommand.Append("LEFT JOIN mis_om_overall_status obj4 ON obj4.overall_status_uuid = obj1.monitoring_item_performance_status_uuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj2.monitoring_item_type_uuid = ANY(SELECT monitoring_item_type_uuid FROM mis_om_assettype_monitoring_itemtype_relation obj3 WHERE obj3.asset_type_uuid= :assetTypeGuid) ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.asset_uuid = :assetGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.current_monitoring_date = :currentMonitoringDate ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("obj1.performance_monitoring_uuid = :performanceMonitoringGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[4];

            arParams[0] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetGuid;

            arParams[1] = new NpgsqlParameter("currentMonitoringDate", NpgsqlTypes.NpgsqlDbType.Date);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = currentMonitoringDate;

            arParams[2] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetTypeGuid;

            arParams[3] = new NpgsqlParameter("performanceMonitoringGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = performanceMonitoringGuid;

            List<MonitoringItemPerformanceManager> monitoringItemPerformanceDataList = new List<MonitoringItemPerformanceManager>();          

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    MonitoringItemPerformanceManager monitoringItemPerformanceDataObj = new MonitoringItemPerformanceManager();

                    monitoringItemPerformanceDataObj.MonitoringItemPerformanceGuid = new Guid(reader["monitoring_item_performance_uuid"].ToString());
                    monitoringItemPerformanceDataObj.MonitoringItemPerformanceDescription = reader["monitoring_item_performance_description"].ToString();

                    monitoringItemPerformanceDataObj.MonitoringItemManager.MonitoringItemGuid = new Guid(reader["monitoring_item_uuid"].ToString());
                    monitoringItemPerformanceDataObj.MonitoringItemManager.MonitoringItemName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["monitoring_item_name"].ToString());

                    monitoringItemPerformanceDataObj.MonitoringItemManager.MonitoringItemTypeManager.MonitoringItemTypeGuid = new Guid(reader["monitoring_item_type_uuid"].ToString());
                    monitoringItemPerformanceDataObj.MonitoringItemManager.MonitoringItemTypeManager.MonitoringItemTypeName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["monitoring_item_type_name"].ToString());

                    monitoringItemPerformanceDataObj.OverallStatusManager.OverallStatusGuid = new Guid(reader["overall_status_uuid"].ToString());
                    monitoringItemPerformanceDataObj.OverallStatusManager.OverallStatusName = reader["overall_status"].ToString();

                    monitoringItemPerformanceDataList.Add(monitoringItemPerformanceDataObj);

                }
            }

            return monitoringItemPerformanceDataList;

        }

        protected internal void UpdateItemPerformance(PerformanceMonitoringManager performanceMonitoringManager)
        {
            StringBuilder monitoringItemGuidString = new StringBuilder();
            StringBuilder overallStatusGuid = new StringBuilder();
            StringBuilder monitoringItemPerformanceDescriptionString = new StringBuilder();
            StringBuilder monitoringItemPerformanceGuidString = new StringBuilder();

            if (performanceMonitoringManager.MonitoringItemPerformanceManager.Count > 0)
            {
                int j = 0;
                foreach (MonitoringItemPerformanceManager monitoringItemPerformanceManagerObj in performanceMonitoringManager.MonitoringItemPerformanceManager)
                {
                    j = j + 1;
                    if (j != 1)
                    {
                        monitoringItemPerformanceGuidString.Append(", ");
                    }
                    monitoringItemPerformanceGuidString.Append("'" + monitoringItemPerformanceManagerObj.MonitoringItemPerformanceGuid + "'");
                    monitoringItemPerformanceDescriptionString.Append("WHEN " + "'" + monitoringItemPerformanceManagerObj.MonitoringItemPerformanceGuid + "'" + " " + "THEN " + "'" + monitoringItemPerformanceManagerObj.MonitoringItemPerformanceDescription + "'" + " ");
                    monitoringItemGuidString.Append("WHEN " + "'" + monitoringItemPerformanceManagerObj.MonitoringItemPerformanceGuid + "'" + " " + "THEN " + "'" + monitoringItemPerformanceManagerObj.MonitoringItemManager.MonitoringItemGuid + "'::uuid" + " ");
                    overallStatusGuid.Append("WHEN " + "'" + monitoringItemPerformanceManagerObj.MonitoringItemPerformanceGuid + "'" + " " + "THEN " + "'" + monitoringItemPerformanceManagerObj.OverallStatusManager.OverallStatusGuid + "'::uuid" + " ");
                }

                StringBuilder updateSqlCommand = new StringBuilder();
                updateSqlCommand.Append("Update ");
                updateSqlCommand.Append("mis_om_monitoring_item_performance ");
                updateSqlCommand.Append("SET ");
                updateSqlCommand.Append("monitoring_item_uuid= ");
                updateSqlCommand.Append("CASE monitoring_item_performance_uuid ");
                updateSqlCommand.Append(monitoringItemGuidString.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("monitoring_item_performance_status_uuid= ");
                updateSqlCommand.Append("CASE monitoring_item_performance_uuid ");
                updateSqlCommand.Append(overallStatusGuid.ToString());
                updateSqlCommand.Append(" END, ");
                updateSqlCommand.Append("monitoring_item_performance_description= ");
                updateSqlCommand.Append("CASE monitoring_item_performance_uuid ");
                updateSqlCommand.Append(monitoringItemPerformanceDescriptionString.ToString());
                updateSqlCommand.Append(" END ");
                updateSqlCommand.Append(" WHERE monitoring_item_performance_uuid IN (");
                updateSqlCommand.Append(monitoringItemPerformanceGuidString.ToString());
                updateSqlCommand.Append(" )");

                NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, updateSqlCommand.ToString());

            }
        }
        
    }
}