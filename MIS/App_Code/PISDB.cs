﻿using Npgsql;
using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class PISDB
    {
        protected internal void GenerateNewProject(string districtName)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_projects ");
            sqlCommand.Append("( ");
            sqlCommand.Append("project_uuid, ");
            sqlCommand.Append("district_uuid ");
            //sqlCommand.Append("divison_deleted ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":projectGuid, ");
            sqlCommand.Append(":districtGuid ");
            //sqlCommand.Append(":divisonDeleted ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("districtGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = Guid.NewGuid();

            //arParams[2] = new NpgsqlParameter("divisonDeleted", NpgsqlTypes.NpgsqlDbType.Composite);
            //arParams[2].Direction = ParameterDirection.Input;
            //arParams[2].Value = divisonDeleted;

            int result = Convert.ToInt32(NpgsqlHelper.ExecuteScalar(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams));

            //using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            //{
            //    while (reader.Read())
            //    {
            //        DivisonManager divisonObj = new DivisonManager();

            //        divisonObj.DivisonGuid = new Guid(reader["divison_id"].ToString());
            //        divisonObj.DivisonName = reader["divison_name"].ToString();
            //        divisonObj.DivisonDeleted = Convert.ToBoolean(reader["divison_deleted"].ToString());

            //        divisonList.Add(divisonObj);
            //    }
            //}
        }

        
    }
}