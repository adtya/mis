﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class ProjectAndPmcSiteEngineersRelationDB
    {
        protected internal List<ProjectAndPmcSiteEngineersRelationManager> GetPmcSiteEngineersListRelatedToProject(Guid projectGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_project_and_pmc_site_engineers_relation obj1 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj1.project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter arParams = new NpgsqlParameter();

            arParams = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams.Direction = ParameterDirection.Input;
            arParams.Value = projectGuid;

            List<ProjectAndPmcSiteEngineersRelationManager> projectAndPmcSiteEngineersRelationManagerList = new List<ProjectAndPmcSiteEngineersRelationManager>();

            using (NpgsqlDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    ProjectAndPmcSiteEngineersRelationManager projectAndPmcSiteEngineersRelationManagerObj = new ProjectAndPmcSiteEngineersRelationManager();

                    projectAndPmcSiteEngineersRelationManagerObj.PmcSiteEngineerProjectRelationshipGuid = new Guid(reader["project_and_pmc_site_engineers_relation_uuid"].ToString());
                    projectAndPmcSiteEngineersRelationManagerObj.SiteEngineerName = reader["pmc_site_engineer_name"].ToString();

                    projectAndPmcSiteEngineersRelationManagerList.Add(projectAndPmcSiteEngineersRelationManagerObj);
                }
            }
            return projectAndPmcSiteEngineersRelationManagerList;
        }

        protected internal int AddProjectAndPmcSiteEngineersRelation(ProjectManager projectManager)
        {
            int rowsAffected = 0;

            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_project_and_pmc_site_engineers_relation ");
            sqlCommand.Append("( ");
            sqlCommand.Append("project_and_pmc_site_engineers_relation_uuid, ");
            sqlCommand.Append("project_uuid, ");
            sqlCommand.Append("pmc_site_engineer_name ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");

            if (projectManager.PmcSiteEngineers.Count > 0)
            {
                foreach (ProjectAndPmcSiteEngineersRelationManager projectAndPmcSiteEngineersRelationManagerObj in projectManager.PmcSiteEngineers)
                {
                    int index = projectManager.PmcSiteEngineers.IndexOf(projectAndPmcSiteEngineersRelationManagerObj);

                    sqlCommand.Append("( ");

                    sqlCommand.Append("'" + Guid.NewGuid() + "', ");
                    sqlCommand.Append("'" + projectManager.ProjectGuid + "', ");
                    sqlCommand.Append("'" + projectAndPmcSiteEngineersRelationManagerObj.SiteEngineerName + "' ");

                    if (index == projectManager.PmcSiteEngineers.Count - 1)
                    {
                        sqlCommand.Append(") ");
                    }
                    else
                    {
                        sqlCommand.Append("), ");
                    }
                }

                sqlCommand.Append(";");

                rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString());
            }

            return rowsAffected;

        }

        protected internal int AddProjectAndPmcSiteEngineersRelation1(ProjectAndPmcSiteEngineersRelationManager projectAndPmcSiteEngineersRelationManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_project_project_and_pmc_site_engineers_relation ");
            sqlCommand.Append("( ");
            sqlCommand.Append("project_and_pmc_site_engineers_relation_uuid, ");
            sqlCommand.Append("project_uuid, ");
            sqlCommand.Append("pmc_site_engineer_name ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":pmcSiteEngineerProjectRelationshipGuid, ");
            sqlCommand.Append(":projectGuid, ");
            sqlCommand.Append(":siteEngineerName ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("pmcSiteEngineerProjectRelationshipGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = projectAndPmcSiteEngineersRelationManager.PmcSiteEngineerProjectRelationshipGuid;

            arParams[1] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = projectAndPmcSiteEngineersRelationManager.ProjectManager.ProjectGuid;

            arParams[2] = new NpgsqlParameter("siteEngineerName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = projectAndPmcSiteEngineersRelationManager.SiteEngineerName;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;

        }

        protected internal int UpdateProjectAndPmcSiteEngineersRelation1(ProjectAndPmcSiteEngineersRelationManager projectAndPmcSiteEngineersRelationManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("UPDATE ");
            sqlCommand.Append("mis_project_project_and_pmc_site_engineers_relation ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("pmc_site_engineer_name = :siteEngineerName ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("project_and_pmc_site_engineers_relation_uuid = :pmcSiteEngineerProjectRelationshipGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("pmcSiteEngineerProjectRelationshipGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = projectAndPmcSiteEngineersRelationManager.PmcSiteEngineerProjectRelationshipGuid;

            arParams[1] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = projectAndPmcSiteEngineersRelationManager.ProjectManager.ProjectGuid;

            arParams[2] = new NpgsqlParameter("siteEngineerName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = projectAndPmcSiteEngineersRelationManager.SiteEngineerName;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;

        }

        protected internal int DeleteProjectAndPmcSiteEngineersRelation1(Guid projectGuid, Guid pmcSiteEngineerProjectRelationshipGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("DELETE ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_project_project_and_pmc_site_engineers_relation ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("project_and_pmc_site_engineers_relation_uuid = :pmcSiteEngineerProjectRelationshipGuid ");
            sqlCommand.Append("AND ");
            sqlCommand.Append("project_uuid = :projectGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[2];

            arParams[0] = new NpgsqlParameter("pmcSiteEngineerProjectRelationshipGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = pmcSiteEngineerProjectRelationshipGuid;

            arParams[1] = new NpgsqlParameter("projectGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = projectGuid;

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;

        }

    }
}