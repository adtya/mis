﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class SchemeRiskRatingManager
    {
        #region Private Members
        private Guid schemeRiskRatingGuid;
        private string schemeRiskRating;
        private int schemeRiskRatingNumber;

        #endregion

        #region Public Getter/Setter Properties

        public Guid SchemeRiskRatingGuid
        {
            get
            {
                return schemeRiskRatingGuid;
            }
            set
            {
                schemeRiskRatingGuid = value;
            }
        }

        public string SchemeRiskRating
        {
            get
            {
                return schemeRiskRating;
            }
            set
            {
                schemeRiskRating = value;
            }
        }

        public int SchemeRiskRatingNumber
        {
            get
            {
                return schemeRiskRatingNumber;
            }
            set
            {
                schemeRiskRatingNumber = value;
            }
        }

        #endregion
    }
}