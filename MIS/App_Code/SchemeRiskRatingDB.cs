﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SchemeRiskRatingDB
    {
        protected internal List<SchemeRiskRatingManager> GetSchemeRiskRating()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_scheme_risk_rating ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("scheme_risk_rating ");
            sqlCommand.Append(";");

            List<SchemeRiskRatingManager> schemeRiskRatingManagerList = new List<SchemeRiskRatingManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    SchemeRiskRatingManager schemeRiskRatingManagerObj = new SchemeRiskRatingManager();

                    schemeRiskRatingManagerObj.SchemeRiskRatingGuid = new Guid(reader["scheme_risk_rating_uuid"].ToString());
                    schemeRiskRatingManagerObj.SchemeRiskRating = reader["scheme_risk_rating"].ToString();

                    schemeRiskRatingManagerList.Add(schemeRiskRatingManagerObj);
                }
            }

            return schemeRiskRatingManagerList;
        }
    }
}