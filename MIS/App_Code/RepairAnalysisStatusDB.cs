﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RepairAnalysisStatusDB
    {
        protected internal List<RepairAnalysisStatusManger> GetRepairAnalysisStatus()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("* ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_repair_analysis_status ");
            sqlCommand.Append("ORDER BY ");
            sqlCommand.Append("repair_analysis_status ");
            sqlCommand.Append(";");

            List<RepairAnalysisStatusManger> repairAnalysisStatusManagerList = new List<RepairAnalysisStatusManger>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RepairAnalysisStatusManger repairAnalysisStatusManagerObj = new RepairAnalysisStatusManger();

                    repairAnalysisStatusManagerObj.RepairAnalysisStatusGuid = new Guid(reader["repair_analysis_status_uuid"].ToString());
                    repairAnalysisStatusManagerObj.RepairAnalysisStatus = reader["repair_analysis_status"].ToString();
                    
                    repairAnalysisStatusManagerList.Add(repairAnalysisStatusManagerObj);
                }
            }

            return repairAnalysisStatusManagerList;
        }
    }
}