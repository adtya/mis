﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class SpurDB
    {       
        protected internal int AddSpurData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_spur ");
            sqlCommand.Append("( ");
            sqlCommand.Append("spur_uuid, ");
            sqlCommand.Append("orientation, ");          
            sqlCommand.Append("length, ");
            sqlCommand.Append("width, ");          
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("construction_type_uuid, ");
            sqlCommand.Append("spur_revet_type_uuid, ");
            sqlCommand.Append("shape_type_uuid ");            
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":spurGuid, ");
            sqlCommand.Append(":orientation, ");
            sqlCommand.Append(":lengthSpur, ");            
            sqlCommand.Append(":widthSpur, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":constructionTypeGuid, ");
            sqlCommand.Append(":spurRevetTypeGuid, ");            
            sqlCommand.Append(":shapeTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[8];

            arParams[0] = new NpgsqlParameter("spurGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("orientation", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.SpurManager.Orientation;

            arParams[2] = new NpgsqlParameter("lengthSpur", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.SpurManager.LengthSpur;

            arParams[3] = new NpgsqlParameter("widthSpur", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.SpurManager.WidthSpur;

            arParams[4] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.AssetGuid;

            arParams[5] = new NpgsqlParameter("constructionTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.SpurManager.SpurConstructionTypeManager.ConstructionTypeGuid;

            arParams[6] = new NpgsqlParameter("spurRevetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.SpurManager.SpurRevetTypeManager.SpurRevetTypeGuid;

            arParams[7] = new NpgsqlParameter("shapeTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.SpurManager.SpurShapeTypeManager.ShapeTypeGuid;          

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateSpurData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_spur ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("orientation= :orientation, ");
            sqlCommand.Append("length= :lengthSpur, ");
            sqlCommand.Append("width= :widthSpur, ");
            sqlCommand.Append("construction_type_uuid= :constructionTypeGuid, ");
            sqlCommand.Append("spur_revet_type_uuid= :spurRevetTypeGuid, ");
            sqlCommand.Append("shape_type_uuid= :shapeTypeGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[8];

            arParams[0] = new NpgsqlParameter("spurGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.SpurManager.SpurGuid;

            arParams[1] = new NpgsqlParameter("orientation", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.SpurManager.Orientation;

            arParams[2] = new NpgsqlParameter("lengthSpur", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.SpurManager.LengthSpur;

            arParams[3] = new NpgsqlParameter("widthSpur", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.SpurManager.WidthSpur;

            arParams[4] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.AssetGuid;

            arParams[5] = new NpgsqlParameter("constructionTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.SpurManager.SpurConstructionTypeManager.ConstructionTypeGuid;

            arParams[6] = new NpgsqlParameter("spurRevetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.SpurManager.SpurRevetTypeManager.SpurRevetTypeGuid;

            arParams[7] = new NpgsqlParameter("shapeTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.SpurManager.SpurShapeTypeManager.ShapeTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
    }

}