﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RevetmentDB
    {
        protected internal int AddRevetmentData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_asset_revetment ");
            sqlCommand.Append("( ");
            sqlCommand.Append("revetment_uuid, ");
            sqlCommand.Append("length, ");
            sqlCommand.Append("plain_width, ");
            sqlCommand.Append("slope, ");
            sqlCommand.Append("asset_uuid, ");
            sqlCommand.Append("revet_type_uuid, ");
            sqlCommand.Append("riverprot_type_uuid, ");
            sqlCommand.Append("waveprot_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":revetmentGuid, ");
            sqlCommand.Append(":lengthRevetment, ");
            sqlCommand.Append(":plainWidth, ");
            sqlCommand.Append(":slopeRevetment, ");
            sqlCommand.Append(":assetGuid, ");
            sqlCommand.Append(":revetTypeGuid, ");
            sqlCommand.Append(":riverprotTypeGuid, ");
            sqlCommand.Append(":waveprotTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[8];

            arParams[0] = new NpgsqlParameter("revetmentGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = Guid.NewGuid();

            arParams[1] = new NpgsqlParameter("lengthRevetment", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.RevetmentManager.LengthRevetment;

            arParams[2] = new NpgsqlParameter("plainWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.RevetmentManager.PlainWidth;

            arParams[3] = new NpgsqlParameter("slopeRevetment", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.RevetmentManager.SlopeRevetment;

            arParams[4] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.AssetGuid;

            arParams[5] = new NpgsqlParameter("revetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.RevetmentManager.RevetmentTypeManager.RevetTypeGuid;

            arParams[6] = new NpgsqlParameter("riverprotTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.RevetmentManager.RevetmentRiverprotTypeManager.RiverprotTypeGuid;

            arParams[7] = new NpgsqlParameter("waveprotTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.RevetmentManager.RevetmentWaveprotTypeManager.WaveprotTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }

        protected internal int UpdateRevetmentData(AssetManager assetManager)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("Update ");
            sqlCommand.Append("mis_asset_revetment ");
            sqlCommand.Append("SET ");
            sqlCommand.Append("length= :lengthRevetment, ");
            sqlCommand.Append("plain_width= :plainWidth, ");
            sqlCommand.Append("slope= :slopeRevetment, ");
            sqlCommand.Append("revet_type_uuid= :revetTypeGuid, ");
            sqlCommand.Append("riverprot_type_uuid= :riverprotTypeGuid, ");
            sqlCommand.Append("waveprot_type_uuid= :waveprotTypeGuid ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("asset_uuid= :assetGuid ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[8];

            arParams[0] = new NpgsqlParameter("revetmentGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetManager.RevetmentManager.RevetmentGuid;

            arParams[1] = new NpgsqlParameter("lengthRevetment", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = assetManager.RevetmentManager.LengthRevetment;

            arParams[2] = new NpgsqlParameter("plainWidth", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = assetManager.RevetmentManager.PlainWidth;

            arParams[3] = new NpgsqlParameter("slopeRevetment", NpgsqlTypes.NpgsqlDbType.Double);
            arParams[3].Direction = ParameterDirection.Input;
            arParams[3].Value = assetManager.RevetmentManager.SlopeRevetment;

            arParams[4] = new NpgsqlParameter("assetGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[4].Direction = ParameterDirection.Input;
            arParams[4].Value = assetManager.AssetGuid;

            arParams[5] = new NpgsqlParameter("revetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[5].Direction = ParameterDirection.Input;
            arParams[5].Value = assetManager.RevetmentManager.RevetmentTypeManager.RevetTypeGuid;

            arParams[6] = new NpgsqlParameter("riverprotTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[6].Direction = ParameterDirection.Input;
            arParams[6].Value = assetManager.RevetmentManager.RevetmentRiverprotTypeManager.RiverprotTypeGuid;

            arParams[7] = new NpgsqlParameter("waveprotTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[7].Direction = ParameterDirection.Input;
            arParams[7].Value = assetManager.RevetmentManager.RevetmentWaveprotTypeManager.WaveprotTypeGuid;

            int result = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return result;
        }
        
    }
}