﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class RevetmentRiverprotTypeDB
    {
        protected internal List<RevetmentRiverprotTypeManager> GetRevRiverprotTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("riverprot_type_uuid, ");
            sqlCommand.Append("riverprot_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_revetment_riverprot_type ");
            sqlCommand.Append("ORDER BY riverprot_type_name ");
            sqlCommand.Append(";");

            List<RevetmentRiverprotTypeManager> revRiverTypeManagerList = new List<RevetmentRiverprotTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    RevetmentRiverprotTypeManager revRiverTypeManagerObj = new RevetmentRiverprotTypeManager();

                    revRiverTypeManagerObj.RiverprotTypeGuid = new Guid(reader["riverprot_type_uuid"].ToString());
                    revRiverTypeManagerObj.RiverprotTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["riverprot_type_name"].ToString());

                    revRiverTypeManagerList.Add(revRiverTypeManagerObj);
                }
            }

            return revRiverTypeManagerList;
        }
    }
}