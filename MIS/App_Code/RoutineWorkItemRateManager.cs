﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NodaMoney;

namespace MIS.App_Code
{
    public class RoutineWorkItemRateManager
    {
        #region Private Members

        private Guid routineWorkItemRateGuid;
        private Money routineWorkItemRate;
        private decimal routineWorkItemRateInput;
        //private CircleManager circleManager;
        private SystemVariablesManager systemVariablesManager;
        private RoutineWorkItemManager routineWorkItemManager;

        #endregion


        #region Public Getter/Setter Properties

        public Guid RoutineWorkItemRateGuid
        {
            get
            {
                return routineWorkItemRateGuid;
            }
            set
            {
                routineWorkItemRateGuid = value;
            }
        }

        public Money RoutineWorkItemRate
        {
            get
            {
                if (routineWorkItemRate.Amount.Equals(0))
                {
                    routineWorkItemRate = RoutineWorkItemRateInput;
                }
                return routineWorkItemRate;
            }
            set
            {
                routineWorkItemRate = value;
            }
        }

        public decimal RoutineWorkItemRateInput
        {
            private get
            {
                return routineWorkItemRateInput;
            }
            set
            {
                routineWorkItemRateInput = value;
            }
        }

        //public CircleManager CircleManager
        //{
        //    get
        //    {
        //        if (circleManager == null)
        //        {
        //            circleManager = new CircleManager();
        //        }
        //        return circleManager;
        //    }
        //    set
        //    {
        //        circleManager = value;
        //    }
        //}

        public SystemVariablesManager SystemVariablesManager
        {
            get
            {
                if (systemVariablesManager == null)
                {
                    systemVariablesManager = new SystemVariablesManager();
                }
                return systemVariablesManager;
            }
            set
            {
                systemVariablesManager = value;
            }
        }

        public RoutineWorkItemManager RoutineWorkItemManager
        {
            get
            {
                if (routineWorkItemManager == null)
                {
                    routineWorkItemManager = new RoutineWorkItemManager();
                }
                return routineWorkItemManager;
            }
            set
            {
                routineWorkItemManager = value;
            }
        }

        #endregion
    }
}