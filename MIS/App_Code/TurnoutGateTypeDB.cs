﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class TurnoutGateTypeDB
    {
        protected internal List<TurnoutGateTypeManager> GetTrnGateTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("turnout_gate_type_uuid, ");
            sqlCommand.Append("turnout_gate_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_turnout_gate_type ");
            sqlCommand.Append("ORDER BY turnout_gate_type_name ");
            sqlCommand.Append(";");

            List<TurnoutGateTypeManager> trnGateTypeManagerList = new List<TurnoutGateTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    TurnoutGateTypeManager trnGateTypeManagerObj = new TurnoutGateTypeManager();

                    trnGateTypeManagerObj.TurnoutGateTypeGuid = new Guid(reader["turnout_gate_type_uuid"].ToString());
                    trnGateTypeManagerObj.TurnoutGateTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["turnout_gate_type_name"].ToString());

                    trnGateTypeManagerList.Add(trnGateTypeManagerObj);
                }
            }

            return trnGateTypeManagerList;
        }
    }
}