﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS.App_Code
{
    public class RevetmentManager
    {        
       #region Private Members

        private Guid revetmentGuid;          
        private double lengthRevetment; 
        private double plainWidth;
        private double slopeRevetment;       
        //private AssetManager assetManager;
        private RevetmentTypeManager revetmentTypeManager;
        private RevetmentRiverprotTypeManager revetmentRiverprotTypeManager;
        private RevetmentWaveprotTypeManager revetmentWaveprotTypeManager;
        
        #endregion


        #region Public Constructors

        public RevetmentManager()
        {

        }

        #endregion


        #region Public Getter/Setter Properties

        public Guid RevetmentGuid
        {
            get
            {
                return revetmentGuid;
            }
            set
            {
                revetmentGuid = value;
            }
        }
      
        public double LengthRevetment
        {
            get
            {
                return lengthRevetment;
            }
            set
            {
                lengthRevetment = value;
            }
        }

        public double PlainWidth
        {
            get
            {
                return plainWidth;
            }
            set
            {
                plainWidth = value;
            }
        }

        public double SlopeRevetment
        {
            get
            {
                return slopeRevetment;
            }
            set
            {
                slopeRevetment = value;
            }
        }

        public RevetmentTypeManager RevetmentTypeManager
        {
            get
            {
                if (revetmentTypeManager == null)
                {
                    revetmentTypeManager = new RevetmentTypeManager();
                }
                return revetmentTypeManager;
            }
            set
            {
                revetmentTypeManager = value;
            }
        }

        public RevetmentRiverprotTypeManager RevetmentRiverprotTypeManager
        {
            get
            {
                if (revetmentRiverprotTypeManager == null)
                {
                    revetmentRiverprotTypeManager = new RevetmentRiverprotTypeManager();
                }
                return revetmentRiverprotTypeManager;
            }
            set
            {
                revetmentRiverprotTypeManager = value;
            }
        }

        public RevetmentWaveprotTypeManager RevetmentWaveprotTypeManager
        {
            get
            {
                if (revetmentWaveprotTypeManager == null)
                {
                    revetmentWaveprotTypeManager = new RevetmentWaveprotTypeManager();
                }
                return revetmentWaveprotTypeManager;
            }
            set
            {
                revetmentWaveprotTypeManager = value;
            }
        }
        //public AssetManager AssetManager
        //{
        //    get
        //    {
        //        if (assetManager == null)
        //        {
        //            assetManager = new AssetManager();
        //        }
        //        return assetManager;
        //    }
        //    set
        //    {
        //        assetManager = value;
        //    }
        //}

        #endregion
    }
}