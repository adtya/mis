﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class GateTypeDB
    {
        protected internal List<GateTypeManager> GetGateTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("gate_type_uuid, ");
            sqlCommand.Append("gate_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_gate_types ");
            sqlCommand.Append("ORDER BY gate_type_name ");
            sqlCommand.Append(";");

            List<GateTypeManager> gateTypeManagerList = new List<GateTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    GateTypeManager gateTypeManagerObj = new GateTypeManager();

                    gateTypeManagerObj.GateTypeGuid = new Guid(reader["gate_type_uuid"].ToString());
                    gateTypeManagerObj.GateTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["gate_type_name"].ToString());

                    gateTypeManagerList.Add(gateTypeManagerObj);
                }
            }

            return gateTypeManagerList;
        }
    }
}