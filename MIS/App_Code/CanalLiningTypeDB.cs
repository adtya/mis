﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class CanalLiningTypeDB
    {
        protected internal List<CanalLiningTypeManager> GetCanalLiningTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("lining_type_uuid, ");
            sqlCommand.Append("lining_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_canal_lining_type ");
            sqlCommand.Append("ORDER BY lining_type_name ");
            sqlCommand.Append(";");

            List<CanalLiningTypeManager> canalLiningTypeManagerList = new List<CanalLiningTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    CanalLiningTypeManager canalLiningTypeManagerObj = new CanalLiningTypeManager();

                    canalLiningTypeManagerObj.LiningTypeGuid = new Guid(reader["lining_type_uuid"].ToString());
                    canalLiningTypeManagerObj.LiningTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["lining_type_name"].ToString());

                    canalLiningTypeManagerList.Add(canalLiningTypeManagerObj);
                }
            }

            return canalLiningTypeManagerList;
        }
    }
}