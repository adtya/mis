﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class BridgeTypeDB
    {
        protected internal List<BridgeTypeManager> GetBridgeTypeList()
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("bridge_type_uuid, ");
            sqlCommand.Append("bridge_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_asset_bridge_type ");
            sqlCommand.Append("ORDER BY bridge_type_name ");
            sqlCommand.Append(";");

            List<BridgeTypeManager> bridgeTypeManagerList = new List<BridgeTypeManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString()))
            {
                while (reader.Read())
                {
                    BridgeTypeManager bridgeypeManagerObj = new BridgeTypeManager();

                    bridgeypeManagerObj.BridgeTypeGuid = new Guid(reader["bridge_type_uuid"].ToString());
                    bridgeypeManagerObj.BridgeTypeName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reader["bridge_type_name"].ToString());

                    bridgeTypeManagerList.Add(bridgeypeManagerObj);
                }
            }

            return bridgeTypeManagerList;
        }
    }
}