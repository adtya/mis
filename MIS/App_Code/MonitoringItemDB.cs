﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace MIS.App_Code
{
    public class MonitoringItemDB
    {
        protected internal List<MonitoringItemManager> GetMonitoringItemData(Guid assetTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("SELECT ");
            sqlCommand.Append("obj1.monitoring_item_uuid, ");
            sqlCommand.Append("obj1.monitoring_item_name, ");
            sqlCommand.Append("obj2.monitoring_item_type_uuid, ");
            sqlCommand.Append("obj2.monitoring_item_type_name ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_monitoring_item obj1 ");
            sqlCommand.Append("LEFT OUTER JOIN mis_om_monitoring_item_type obj2 ON obj2.monitoring_item_type_uuid=obj1.monitoring_item_type_uuid ");
            sqlCommand.Append("WHERE obj2.monitoring_item_type_uuid = ANY ");
            sqlCommand.Append("(SELECT ");
            sqlCommand.Append("monitoring_item_type_uuid ");
            sqlCommand.Append("FROM ");
            sqlCommand.Append("mis_om_assettype_monitoring_itemtype_relation obj3 ");
            sqlCommand.Append("WHERE ");
            sqlCommand.Append("obj3.asset_type_uuid= :assetTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[1];

            arParams[0] = new NpgsqlParameter("assetTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = assetTypeGuid;

            List<MonitoringItemManager> monitoringItemDataList = new List<MonitoringItemManager>();

            using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
            {
                while (reader.Read())
                {
                    MonitoringItemManager monitoringItemObj = new MonitoringItemManager();
                    monitoringItemObj.MonitoringItemGuid = new Guid(reader["monitoring_item_uuid"].ToString());
                    monitoringItemObj.MonitoringItemName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["monitoring_item_name"].ToString());
                    monitoringItemObj.MonitoringItemTypeManager.MonitoringItemTypeGuid = new Guid(reader["monitoring_item_type_uuid"].ToString());
                    monitoringItemObj.MonitoringItemTypeManager.MonitoringItemTypeName = CultureInfo.CurrentCulture.TextInfo.ToUpper(reader["monitoring_item_type_name"].ToString());
                    monitoringItemDataList.Add(monitoringItemObj);
                }
            }
            return monitoringItemDataList;
        }

        protected internal int AddMonitoringItem(Guid monitoringItemGuid, string monitoringItemName, Guid monitoringItemTypeGuid)
        {
            StringBuilder sqlCommand = new StringBuilder();
            sqlCommand.Append("INSERT INTO ");
            sqlCommand.Append("mis_om_monitoring_item ");
            sqlCommand.Append("( ");
            sqlCommand.Append("monitoring_item_uuid, ");
            sqlCommand.Append("monitoring_item_name, ");
            sqlCommand.Append("monitoring_item_type_uuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append("VALUES ");
            sqlCommand.Append("( ");
            sqlCommand.Append(":monitoringItemGuid, ");
            sqlCommand.Append(":monitoringItemName, ");          
            sqlCommand.Append(":monitoringItemTypeGuid ");
            sqlCommand.Append(") ");
            sqlCommand.Append(";");

            NpgsqlParameter[] arParams = new NpgsqlParameter[3];

            arParams[0] = new NpgsqlParameter("monitoringItemGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[0].Direction = ParameterDirection.Input;
            arParams[0].Value = monitoringItemGuid;

            arParams[1] = new NpgsqlParameter("monitoringItemName", NpgsqlTypes.NpgsqlDbType.Text);
            arParams[1].Direction = ParameterDirection.Input;
            arParams[1].Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(monitoringItemName);

            arParams[2] = new NpgsqlParameter("monitoringItemTypeGuid", NpgsqlTypes.NpgsqlDbType.Uuid);
            arParams[2].Direction = ParameterDirection.Input;
            arParams[2].Value = monitoringItemTypeGuid;          

            int rowsAffected = NpgsqlHelper.ExecuteNonQuery(ConnectionString.GetWriteConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams);

            return rowsAffected;
        }
    }
}