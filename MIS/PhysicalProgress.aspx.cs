﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class PhysicalProgress : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            DataRow dr = null;

            dt.Columns.Add(new DataColumn("No.", typeof(string)));
            dt.Columns.Add(new DataColumn("Name of Package", typeof(string)));
            dt.Columns.Add(new DataColumn("Total Value of Package", typeof(string)));
            dt.Columns.Add(new DataColumn("Payment made during month", typeof(string)));
            dt.Columns.Add(new DataColumn("Cumulative Payment", typeof(string)));
            dt.Columns.Add(new DataColumn("Balance", typeof(string)));

            dr = dt.NewRow();

            dr["No."] = "";
            dr["Name of Package"] = "";
            dr["Total Value of Package"] = "";
            dr["Payment made during month"] = "";
            dr["Cumulative Payment"] = "";
            dr["Balance"] = "";
            dt.Rows.Add(dr);

            //fillvalues();
        }

        //private void fillvalues()
        //{     
        //    string selectQuery = "SELECT * from mis_project_report_general_information where project_report_id='0f8d7d33-bf30-4d63-8d4b-e6756cdde27a'";

        //    using (IDataReader reader = NpgsqlHelper.ExecuteReader(ConnectionString.GetReadConnectionString(), CommandType.Text, sqlCommand.ToString(), arParams))
        //    {
        //        while (reader.Read())
        //        {
        //            TrainingManager trainingObj = new TrainingManager();

        //            trainingObj.CourseGuid = new Guid(reader["training_id"].ToString());
        //            trainingObj.CourseId = reader["training_course_id"].ToString();
        //            trainingObj.CourseName = reader["training_course_name"].ToString();
        //            trainingObj.CourseDescription = reader["training_course_description"].ToString();
        //            trainingObj.CourseLocation = reader["training_course_location"].ToString();
        //            trainingObj.CourseLocationType = Convert.ToInt16(reader["training_course_location_type"].ToString());
        //            trainingObj.CourseStartDate = DateTime.Parse(reader["training_course_start_date"].ToString());
        //            trainingObj.CourseEndDate = DateTime.Parse(reader["training_course_end_date"].ToString());
        //            trainingObj.CourseTargetUser = reader["training_course_target_user"].ToString();

        //            trainingDataList.Add(trainingObj);
        //        }
        //    }
            
        //}
    }
}