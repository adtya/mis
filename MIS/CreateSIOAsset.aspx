﻿<%@ Page Title="Add Asset" Language="C#" MasterPageFile="~/SIO.Master" AutoEventWireup="true" CodeBehind="CreateSIOAsset.aspx.cs" Inherits="MIS.CreateSIOAsset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    <link href="Content/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
    <script src="Scripts/jquery.glob.js" type="text/javascript"></script>
    <script src="Scripts/globinfo/jQuery.glob.en-IN.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.GlobalMoneyInput2.js" type="text/javascript"></script>

    <script src="Scripts/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="Scripts/Datejs/build/date.js" type="text/javascript"></script>
    <script src="Scripts/date.format.js" type="text/javascript"></script>
    <script src="Scripts/jquery.Guid.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div class="container" style="padding-top:10px;">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>New Asset</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server" id="addSIOAssetForm" name="addSIOAssetForm">
                            <fieldset>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                    <label for="divisionList" id="divisionListLabel" class="control-label">Division</label>
                                                </div>
                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                    <select class="form-control select2" name="divisionList" id="divisionList" style="width:100%;">

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                    <label for="schemeList" id="schemeListLabel" class="control-label">Scheme</label>
                                                </div>
                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                    <select class="form-control select2" id="schemeList" name="schemeList" style="width:100%;">

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                    <label for="assetTypeList" id="assetTypeLabel" class="control-label">Asset Type</label>
                                                </div>
                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                    <asp:Literal ID="ltAssetTypeList" runat="server"></asp:Literal>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                    <label for="assetCodeNumberSelect" id="assetCodeNumberSelectLabel" class="control-label">Asset Number</label>
                                                </div>
                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                    <select class="form-control select2" id="assetCodeNumberSelect" name="assetCodeNumberSelect" style="width:100%;">

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="assetCode" id="assetCodeLabel" class="control-label">Asset Code</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <input type="text" id="assetCode" name="assetCode" placeholder="Asset Code" class="form-control" readonly="readonly" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="display:none" id="showKeyAssetSelect">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="keyAsset" id="keyAssetLabel" class="control-label text-left">Select Key Asset</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <select class="form-control select2" id="keyAssetSelect" name="keyAssetSelect" style="width:100%;">

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="drawingCode" id="drawingCodeLabel" class="control-label">Drawing Code</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <div id="uploadDrawing" class="input-group">
                                                                <input type="text" id="drawingCode" name="drawingCode" placeholder="Drawing Code" class="form-control upload" readonly="readonly"/>
                                                                <span class="input-group-addon form-control-static upload">
                                                                    <i class="fa fa-upload" title="Upload Drawing"></i>
                                                                </span>
                                                            </div>
                                                            <input type="file" id="drawingCode1" name="drawingCode1" class="form-control hidden" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="amtd" id="amtdLabel"class="control-label">AMTD</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <input type="text" id="amtd" name="amtd" placeholder="AMTD" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="assetName" id="assetNameLabel" class="control-label">Asset Name</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <input type="text" id="assetName" name="assetName" placeholder="Asset Name" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="assetCost" id="assetCostLabel"class="control-label">Asset Cost (Rs.)</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <input type="text" id="assetCost" name="assetCost" placeholder="Asset Cost (Rs.)" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="chainageStart" id="chainageStartLabel"class="control-label">Chainage Start</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <input type="text" id="chainageStart" name="chainageStart" placeholder="Chainage Start" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="chainageEnd" id="chainageEndLabel" class="control-label">Chainage End</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <input type="text" id="chainageEnd" name="chainageEnd" placeholder="Chainage End" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="eastingUS" id="eastingUSLabel"class="control-label">Easting US</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <input type="text" id="eastingUS" name="eastingUS" placeholder="Easting US" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="eastingDS" id="eastingDSLabel"class="control-label">Easting DS</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <input type="text" id="eastingDS" name="eastingDS" placeholder="Easting DS" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="northingUS" id="northingUSLabel"class="control-label">Northing US</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <input type="text" id="northingUS" name="northingUS" placeholder="Northing US" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="northingDS" id="northingDSLabel"class="control-label">Northing DS</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <input type="text" id="northingDS" name="northingDS" placeholder="Northing DS" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="dateOfInitiationOfAsset" id="dateOfInitiationOfAssetLabel"class="control-label">Initiation Date</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <div id="dpDateOfInitiationOfAsset" class="input-group date">
                                                                <input type="text" name="dateOfInitiationOfAsset" class="form-control" id="dateOfInitiationOfAsset" placeholder="Date Of Initiation (dd/mm/yyyy)" />
                                                                <span class="input-group-addon form-control-static">
                                                                    <i class="fa fa-calendar fa-fw"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="dateOfCompletionOfAsset" id="dateOfCompletionOfAssetLabel"class="control-label">Completion Date</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <div id="dpDateOfCompletionOfAsset" class="input-group date">
                                                                <input type="text" name="dateOfCompletionOfAsset" class="form-control" id="dateOfCompletionOfAsset" placeholder="Date Of Completion (dd/mm/yyyy)" />
                                                                <span class="input-group-addon form-control-static">
                                                                    <i class="fa fa-calendar fa-fw"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <%--Asset Type - BRG--%>
                                <div id="bridgeAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Bridge</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="bridgeTypeList" id="bridgeTypeListLabel" class="control-label text-left">Bridge Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="bridgeTypeList" id="bridgeTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="bridgeLength" id="bridgeLengthLabel" class="control-label text-left">Length (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="bridgeLength" name="bridgeLength" placeholder="Length (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="bridgeNumberOfPiers" id="bridgeNumberOfPiersLabel" class="control-label text-left">Number Of Piers</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="bridgeNumberOfPiers" name="bridgeNumberOfPiers" placeholder="Number Of Piers" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="bridgeRoadWidth" id="bridgeRoadWidthLabel" class="control-label text-left">Road Width (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="bridgeRoadWidth" name="bridgeRoadWidth" placeholder="Road Width (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - CAN--%>
                                <div id="canalAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Canal</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="canalUsCrestElevation" id="canalUsCrestElevationLabel" class="control-label text-left">U/S Crest Elevation (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="canalUsCrestElevation" name="canalUsCrestElevation" placeholder="U/S Crest Elevation (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="canalDsCrestElevation" id="canalDsCrestElevationLabel" class="control-label text-left">D/S Crest Elevation (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="canalDsCrestElevation" name="canalDsCrestElevation" placeholder="D/S Crest Elevation (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="canalLength" id="canalLengthLabel" class="control-label text-left">Length (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="canalLength" name="canalLength" placeholder="Length (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="canalBedWidth" id="canalBedWidthLabel" class="control-label text-left">Bed Width(m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="canalBedWidth" name="canalBedWidth" placeholder="Bed Width(m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="canalSlipeSlope" id="canalSlipeSlopeLabel" class="control-label text-left">Slipe Slope</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div class="input-group">
                                                                    <input type="text" id="canalSlipeSlope1" name="canalSlipeSlope1" placeholder="Slipe Slope" class="form-control" />
                                                                    <span class="input-group-addon" value=":">:</span>
                                                                    <input type="text" id="canalSlipeSlope2" name="canalSlipeSlope2" placeholder="Slipe Slope" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="canalAverageDepth" id="canalAverageDepthLabel" class="control-label text-left">Average Depth</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="canalAverageDepth" name="canalAverageDepth" placeholder="Average Depth" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="canalLiningTypeList" id="canalLiningTypeListLabel" class="control-label text-left">Lining Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="canalLiningTypeList" id="canalLiningTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - CUL--%>
                                <div id="culvertAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Culvert</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="culvertTypeList" id="culvertTypeListLabel" class="control-label text-left">Culvert Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="culvertTypeList" id="culvertTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="culvertNumberOfVents" id="culvertNumberOfVentsLabel" class="control-label text-left">Number Of Vents</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="culvertNumberOfVents" name="culvertNumberOfVents" placeholder="Number Of Vents" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="culvertVentHeight" id="culvertVentHeightLabel" class="control-label text-left">Vent Height</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="culvertVentHeight" name="culvertVentHeight" placeholder="Vent Height" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="culvertVentWidth" id="culvertVentWidthLabel" class="control-label text-left">Vent Width</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="culvertVentWidth" name="culvertVentWidth" placeholder="Vent Width" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="culvertLength" id="culvertLengthLabel" class="control-label text-left">Length (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="culvertLength" name="culvertLength" placeholder="Length (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - DRN--%>
                                <div id="drainageAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Drainage</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="drainageUsBedElevation" id="drainageUsBedElevationLabel" class="control-label text-left">U/S Bed Elevation(m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="drainageUsBedElevation" name="drainageUsBedElevation" placeholder="U/S Bed Elevation(m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="drainageDsBedElevation" id="drainageDsBedElevationLabel" class="control-label text-left">D/S Bed Elevation(m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="drainageDsBedElevation" name="drainageDsBedElevation" placeholder="D/S Bed Elevation(m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="drainageLength" id="drainageLengthLabel" class="control-label text-left">Length (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="drainageLength" name="drainageLength" placeholder="Length (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="drainageBedWidth" id="drainageBedWidthLabel" class="control-label text-left">Bed Width(m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="drainageBedWidth" name="drainageBedWidth" placeholder="Bed Width(m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="drainageSlope" id="drainageSlopeLabel" class="control-label text-left">Slope</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div class="input-group">
                                                                    <input type="text" id="drainageSlope1" name="drainageSlope1" placeholder="Slope" class="form-control" />
                                                                    <span class="input-group-addon" value=":">:</span>
                                                                    <input type="text" id="drainageSlope2" name="drainageSlope2" placeholder="Slope" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="drainageAverageDepth" id="drainageAverageDepthLabel" class="control-label text-left">Average Depth (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="drainageAverageDepth" name="drainageAverageDepth" placeholder="Average Depth (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - DRP--%>
                                <div id="dropStructureAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Drop Structure</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="dropStructureUsInvertLevel" id="dropStructureUsInvertLevelLabel" class="control-label text-left">U/S Invert Level</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="dropStructureUsInvertLevel" name="dropStructureUsInvertLevel" placeholder="U/S Invert Level" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="dropStructureDsInvertLevel" id="dropStructureDsInvertLevelLabel" class="control-label text-left">D/S Invert Level</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="dropStructureDsInvertLevel" name="dropStructureDsInvertLevel" placeholder="D/S Invert Level" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="dropStructureStillingBasinLength" id="dropStructureStillingBasinLengthLabel" class="control-label text-left">Stilling Basin Length (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="dropStructureStillingBasinLength" name="dropStructureStillingBasinLength" placeholder="Stilling Basin Length (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="dropStructureStillingBasinWidth" id="dropStructureStillingBasinWidthLabel" class="control-label text-left">Stilling Basin Width (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="dropStructureStillingBasinWidth" name="dropStructureStillingBasinWidth" placeholder="Stilling Basin Width (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - EMB--%>
                                <div id="embankmentAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Embankment</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="embankmentUsCrestElevation" id="embankmentUsCrestElevationLabel" class="control-label text-left">U/S Crest Elevation (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="embankmentUsCrestElevation" name="embankmentUsCrestElevation" placeholder="U/S Crest Elevation (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="embankmentDsCrestElevation" id="embankmentDsCrestElevationLabel" class="control-label text-left">D/S Crest Elevation (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="embankmentDsCrestElevation" name="embankmentDsCrestElevation" placeholder="D/S Crest Elevation (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="embankmentLength" id="embankmentLengthLabel" class="control-label text-left">Length (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="embankmentLength" name="embankmentLength" placeholder="Length (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="embankmentCrestWidth" id="embankmentCrestWidthLabel" class="control-label text-left">Crest Width (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="embankmentCrestWidth" name="embankmentCrestWidth" placeholder="Crest Width (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="embankmentCsSlope" id="embankmentCsSlopeLabel" class="control-label text-left">CS Slope</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div class="input-group">
                                                                    <input type="text" id="embankmentCsSlope1" name="embankmentCsSlope1" placeholder="CS Slope" class="form-control" />
                                                                    <span class="input-group-addon" value=":">:</span>
                                                                    <input type="text" id="embankmentCsSlope2" name="embankmentCsSlope2" placeholder="CS Slope" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="embankmentRsSlope" id="embankmentRsSlopeLabel" class="control-label text-left">RS Slope</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div class="input-group">
                                                                    <input type="text" id="embankmentRsSlope1" name="embankmentRsSlope1" placeholder="RS Slope" class="form-control" />
                                                                    <span class="input-group-addon" value=":">:</span>
                                                                    <input type="text" id="embankmentRsSlope2" name="embankmentRsSlope2" placeholder="RS Slope" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="embankmentBermSlope" id="embankmentBermSlopeLabel" class="control-label text-left">Berm Slope</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div class="input-group">
                                                                    <input type="text" id="embankmentBermSlope1" name="embankmentBermSlope1" placeholder="Berm Slope" class="form-control" />
                                                                    <span class="input-group-addon" value=":">:</span>
                                                                    <input type="text" id="embankmentBermSlope2" name="embankmentBermSlope2" placeholder="Berm Slope" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="embankmentPlatformWidth" id="embankmentPlatformWidthLabel" class="control-label text-left">Platform Width(m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="embankmentPlatformWidth" name="embankmentPlatformWidth" placeholder="Platform Width (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="embankmentCrestTypeList" id="embankmentCrestTypeListLabel" class="control-label text-left">Crest Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="embankmentCrestTypeList" id="embankmentCrestTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="embankmentFillTypeList" id="embankmentFillTypeListLabel" class="control-label text-left">Fill Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="embankmentFillTypeList" id="embankmentFillTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="embankmentAverageHeight" id="embankmentAverageHeightLabel" class="control-label text-left">Average Height (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="embankmentAverageHeight" name="embankmentAverageHeight" placeholder="Average Height (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - GAU--%>
                                <div id="gaugeAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Gauge</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="gaugeTypeList" id="gaugeTypeListLabel" class="control-label text-left">Gauge Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="gaugeTypeList" id="gaugeTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="gaugeSeasonTypeList" id="gaugeSeasonTypeListLabel" class="control-label text-left">Season Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" id="gaugeSeasonTypeList" name="gaugeSeasonTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="gaugeFrequencyTypeList" id="gaugeFrequencyTypeListLabel" class="control-label text-left">Frequency Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" id="gaugeFrequencyTypeList" name="gaugeFrequencyTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="gaugeZeroDatum" id="gaugeZeroDatumLabel" class="control-label text-left">Zero Datum (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="gaugeZeroDatum" name="gaugeZeroDatum" placeholder="Zero Datum (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="gaugeStartDate" id="gaugeStartDateLabel" class="control-label text-left">Start Date</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div id="dpGaugeStartDate" class="input-group date">
                                                                    <input type="text" name="gaugeStartDate" class="form-control" id="gaugeStartDate" placeholder="Start Date (dd/mm/yyyy)" />
                                                                    <span class="input-group-addon form-control-static">
                                                                        <i class="fa fa-calendar fa-fw"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="gaugeEndDate" id="gaugeEndDateLabel" class="control-label text-left">End Date</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div id="dpGaugeEndDate" class="input-group date">
                                                                    <input type="text" name="gaugeEndDate" class="form-control" id="gaugeEndDate" placeholder="End Date (dd/mm/yyyy)" />
                                                                    <span class="input-group-addon form-control-static">
                                                                        <i class="fa fa-calendar fa-fw"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                                        <div class="form-group">
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <label for="gaugeActive" id="gaugeActiveLabel"class="control-label text-left">Active</label>
                                                            </div>
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <input type="checkbox" name="gaugeActive" class="form-control" id="gaugeActive" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                                        <div class="form-group">
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <label for="gaugeLwlPresent" id="gaugeLwlPresentLabel"class="control-label text-left">LWL Present</label>
                                                            </div>
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <input type="checkbox" name="gaugeLwlPresent" class="form-control" id="gaugeLwlPresent" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                                        <div class="form-group">
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <label for="gaugeHwlPresent" id="gaugeHwlPresentLabel"class="control-label text-left">HWL Present</label>
                                                            </div>
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <input type="checkbox" name="gaugeHwlPresent" class="form-control" id="gaugeHwlPresent" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                                        <div class="form-group">
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <label for="gaugeLevelGeo" id="gaugeLevelGeoLabel"class="control-label text-left">Level Geo</label>
                                                            </div>
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <input type="checkbox" name="gaugeLevelGeo" class="form-control" id="gaugeLevelGeo" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - POR--%>
                                <div id="porcupineAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Porcupine</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="porcupineTypeList" id="porcupineTypeListLabel" class="control-label text-left">Porcupine Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="porcupineTypeList" id="porcupineTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="porcupineMaterialTypeList" id="porcupineMaterialTypeListLabel" class="control-label text-left">Material Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" id="porcupineMaterialTypeList" name="porcupineMaterialTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="porcupineScreenLength" id="porcupineScreenLengthLabel" class="control-label text-left">Screen Length</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="porcupineScreenLength" name="porcupineScreenLength" placeholder="Screen Length" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="porcupineSpacingAlongScreen" id="porcupineSpacingAlongScreenLabel" class="control-label text-left">Spacing Along Screen</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="porcupineSpacingAlongScreen" name="porcupineSpacingAlongScreen" placeholder="Spacing Along Screen" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="porcupineScreenRows" id="porcupineScreenRowsLabel"class="control-label text-left">Screen Rows</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="porcupineScreenRows" name="porcupineScreenRows" placeholder="Screen Rows" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="porcupineNumberOfLayers" id="porcupineNumberOfLayersLabel" class="control-label text-left">Number Of Layers</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="porcupineNumberOfLayers" name="porcupineNumberOfLayers" placeholder="Number Of Layers" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="porcupineMamberLength" id="porcupineMamberLengthLabel"class="control-label text-left">Mamber Length</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="porcupineMamberLength" name="porcupineMamberLength" placeholder="Mamber Length" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="porcupineLength" id="porcupineLengthLabel" class="control-label text-left">Length</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="porcupineLength" name="porcupineLength" placeholder="Length" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - REG--%>
                                <div id="regulatorAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Regulator</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="regulatorNumberOfVents" id="regulatorNumberOfVentsLabel" class="control-label text-left">Number Of Vents</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="regulatorNumberOfVents" name="regulatorNumberOfVents" placeholder="Number Of Vents" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="regulatorVentHeight" id="regulatorVentHeightLabel" class="control-label text-left">Vent Height</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="regulatorVentHeight" name="regulatorVentHeight" placeholder="Vent Height" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="regulatorVentWidth" id="regulatorVentWidthLabel" class="control-label text-left">Vent Width</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="regulatorVentWidth" name="regulatorVentWidth" placeholder="Vent Width" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="regulatorTypeList" id="regulatorTypeListLabel" class="control-label text-left">Regulator Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="regulatorTypeList" id="regulatorTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="regulatorGateTypeList" id="regulatorGateTypeListLabel" class="control-label text-left">Gate Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="regulatorGateTypeList" id="regulatorGateTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div id="regulator-table-container" class="table-container" style="padding:1%;">
                                                        <div class="table-responsive">

                                                            <table id="regulatorGatesTable" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">

                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Gate Type Guid</th>
                                                                        <th>Construction Material Guid</th>
                                                                        <th>Gate Type</th>
                                                                        <th>Construction Material</th>
                                                                        <th>Number Of Gates</th>
                                                                        <th>Height</th>
                                                                        <th>Width</th>
                                                                        <th>Diameter</th>
                                                                        <th>Install Date</th>
                                                                        <th>Operation</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>

                                                                </tbody>

                                                            </table>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - REV--%>
                                <div id="revetmentAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Revetment</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="revetmentTypeList" id="revetmentTypeListLabel" class="control-label text-left">Revetment Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="revetmentTypeList" id="revetmentTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="revetmentRiverProtectionTypeList" id="revetmentRiverProtectionTypeListLabel" class="control-label text-left">River Protection Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" id="revetmentRiverProtectionTypeList" name="revetmentRiverProtectionTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="revetmentWaveProtectionTypeList" id="revetmentWaveProtectionTypeListLabel" class="control-label text-left">Wave Protection Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" id="revetmentWaveProtectionTypeList" name="revetmentWaveProtectionTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="revetmentSlope" id="revetmentSlopeLabel" class="control-label text-left">Slope (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input id="revetmentSlope" name="revetmentSlope" placeholder="Slope (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="revetmentLength" id="revetmentLengthLabel"class="control-label text-left">Length (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="revetmentLength" name="revetmentLength" placeholder="Length (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="revetmentPlainWidth" id="revetmentPlainWidthLabel" class="control-label text-left">Plain Width (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="revetmentPlainWidth" name="revetmentPlainWidth" placeholder="Plain Width (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - SLU--%>
                                <div id="sluiceAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Sluice</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="sluiceTypeList" id="sluiceTypeListLabel" class="control-label text-left">Sluice Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" id="sluiceTypeList" name="sluiceTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="sluiceNumberOfVents" id="sluiceNumberOfVentsLabel" class="control-label text-left">Number Of Vents</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="sluiceNumberOfVents" name="sluiceNumberOfVents" placeholder="Number Of Vents" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="sluiceVentDiameter" id="sluiceVentDiameterLabel" class="control-label text-left">Vent Diameter (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="sluiceVentDiameter" name="sluiceVentDiameter" placeholder="Vent Diameter (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="sluiceVentWidth" id="sluiceVentWidthLabel" class="control-label text-left">Vent Width (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="sluiceVentWidth" name="sluiceVentWidth" placeholder="Vent Width (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="sluiceVentHeight" id="sluiceVentHeightLabel" class="control-label text-left">Vent Heigh t(m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="sluiceVentHeight" name="sluiceVentHeight" placeholder="Vent Height (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div id="sluice-table-container" class="table-container" style="padding:1%;">
                                                        <div class="table-responsive">

                                                            <table id="sluiceGatesTable" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">

                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Gate Type Guid</th>
                                                                        <th>Construction Material Guid</th>
                                                                        <th>Gate Type</th>
                                                                        <th>Construction Material</th>
                                                                        <th>Number Of Gates</th>
                                                                        <th>Height</th>
                                                                        <th>Width</th>
                                                                        <th>Diameter</th>
                                                                        <th>Install Date</th>
                                                                        <th>Operation</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>

                                                                </tbody>

                                                            </table>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - SPU--%>
                                <div id="spurAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Spur</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="spurOrientation" id="spurOrientationLabel" class="control-label text-left">Orientation</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="spurOrientation" name="spurOrientation" placeholder="Orientation" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="spurLength" id="spurLengthLabel" class="control-label text-left">Length (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="spurLength" name="spurLength" placeholder="Length (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="spurWidth" id="spurWidthLabel" class="control-label text-left">Width (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="spurWidth" name="spurWidth" placeholder="Width (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="spurShapeTypeList" id="spurShapeTypeListLabel" class="control-label text-left">Shape Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" id="spurShapeTypeList" name="spurShapeTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="spurConstructionTypeList" id="spurConstructionTypeListLabel" class="control-label text-left">Construction Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" id="spurConstructionTypeList" name="spurConstructionTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="spurRevetTypeList" id="spurRevetTypeListLabel" class="control-label text-left">Revet Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" id="spurRevetTypeList" name="spurRevetTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - TRN--%>
                                <div id="turnoutAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Turnout</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="turnoutInletBoxWidth" id="turnoutInletBoxWidthLabel" class="control-label text-left">Inlet Box Width (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="turnoutInletBoxWidth" name="turnoutInletBoxWidth" placeholder="Inlet Box Width (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="turnoutInletBoxLength" id="turnoutInletBoxLengthLabel" class="control-label text-left">Inlet Box Length (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="turnoutInletBoxLength" name="turnoutInletBoxLength" placeholder="Inlet Box Length (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="turnoutOutletNumber" id="turnoutOutletNumberLabel" class="control-label text-left">Outlet Number</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="turnoutOutletNumber" name="turnoutOutletNumber" placeholder="Outlet Number" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="turnoutOutletWidth" id="turnoutOutletWidthLabel" class="control-label text-left">Outlet Width (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="turnoutOutletWidth" name="turnoutOutletWidth" placeholder="Outlet Width (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="turnoutOutletHeight" id="turnoutOutletHeightLabel" class="control-label text-left">Outlet Height (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="turnoutOutletHeight" name="turnoutOutletHeight" placeholder="Outlet Height (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="turnoutTypeList" id="turnoutTypeListLabel" class="control-label text-left">Turnout Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="turnoutTypeList" id="turnoutTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="turnoutGateTypeList" id="turnoutGateTypeListLabel" class="control-label text-left">Gate Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="turnoutGateTypeList" id="turnoutGateTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div id="turnout-table-container" style="padding:1%;">
                                                        <div class="table-responsive">

                                                            <table id="turnoutGatesTable" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">

                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Gate Type Guid</th>
                                                                        <th>Construction Material Guid</th>
                                                                        <th>Gate Type</th>
                                                                        <th>Construction Material</th>
                                                                        <th>Number Of Gates</th>
                                                                        <th>Height</th>
                                                                        <th>Width</th>
                                                                        <th>Diameter</th>
                                                                        <th>Install Date</th>
                                                                        <th>Operation</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>

                                                                </tbody>

                                                            </table>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                                <%--Asset Type - WEI--%>
                                <div id="weirAssetTypeDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assetType" style="display:none">
                                    <div class="form-group">
                                        <fieldset class="scheduler-border">

                                            <legend class="scheduler-border">
                                                <u>Technical Specification: Weir</u>
                                            </legend>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="weirInvertLevel" id="weirInvertLevelLabel" class="control-label text-left">Invert Level</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="weirInvertLevel" name="weirInvertLevel" placeholder="Invert Level" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="weirCrestTopLevel" id="weirCrestTopLevelLabel" class="control-label text-left">Crest Top Level</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="weirCrestTopLevel" name="weirCrestTopLevel" placeholder="Crest Top Level" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="weirCrestTopWidth" id="weirCrestTopWidthLabel" class="control-label text-left">Crest Top Width (m)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" id="weirCrestTopWidth" name="weirCrestTopWidth" placeholder="Crest Top Width (m)" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="weirTypeList" id="weirTypeListLabel" class="control-label text-left">Weir Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control select2" name="weirTypeList" id="weirTypeList" style="width:100%;">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary" type="submit" aria-hidden="true" id="saveSIOAssetBtn">Save</button>
                                            <button class="btn btn-primary" id="cancelSIOAssetBtn" aria-hidden="true">Cancel</button>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
    <script src="CustomScripts/SIO/Asset/add-sio-asset.js" type="text/javascript"></script>
</asp:Content>
