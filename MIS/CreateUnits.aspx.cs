﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class CreateUnits : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static void CreateNewUnits(string unitName, string unitSymbol)
        {
            new UnitDB().AddNewUnit(unitName, unitSymbol);
        }
    }
}