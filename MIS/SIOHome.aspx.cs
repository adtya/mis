﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class SIOHome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "SIO")
                {
                    if (!Page.IsPostBack)
                    {
                        
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                    //Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("DBALogin.aspx");
            }
        }

        //private DateTime GetNetworkTime()
        //{
        //    //default Windows time server
        //    const string ntpServer = "time.windows.com";

        //    // NTP message size - 16 bytes of the digest (RFC 2030)
        //    var ntpData = new byte[48];

        //    //Setting the Leap Indicator, Version Number and Mode values
        //    ntpData[0] = 0x1B; //LI = 0 (no warning), VN = 3 (IPv4 only), Mode = 3 (Client Mode)

        //    var addresses = Dns.GetHostEntry(ntpServer).AddressList;

        //    //The UDP port number assigned to NTP is 123
        //    var ipEndPoint = new IPEndPoint(addresses[0], 123);
        //    //NTP uses UDP
        //    var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

        //    socket.Connect(ipEndPoint);

        //    //Stops code hang if NTP is blocked
        //    socket.ReceiveTimeout = 3000;

        //    socket.Send(ntpData);
        //    socket.Receive(ntpData);
        //    socket.Close();

        //    //Offset to get to the "Transmit Timestamp" field (time at which the reply 
        //    //departed the server for the client, in 64-bit timestamp format."
        //    const byte serverReplyTime = 40;

        //    //Get the seconds part
        //    ulong intPart = BitConverter.ToUInt32(ntpData, serverReplyTime);

        //    //Get the seconds fraction
        //    ulong fractPart = BitConverter.ToUInt32(ntpData, serverReplyTime + 4);

        //    //Convert From big-endian to little-endian
        //    intPart = SwapEndianness(intPart);
        //    fractPart = SwapEndianness(fractPart);

        //    var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);

        //    //**UTC** time
        //    var networkDateTime = (new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddMilliseconds((long)milliseconds);

        //    return networkDateTime.ToLocalTime();
        //}

        //// stackoverflow.com/a/3294698/162671
        //static uint SwapEndianness(ulong x)
        //{
        //    return (uint)(((x & 0x000000ff) << 24) +
        //                   ((x & 0x0000ff00) << 8) +
        //                   ((x & 0x00ff0000) >> 8) +
        //                   ((x & 0xff000000) >> 24));
        //}
    }
}