﻿<%@ Page Title="Projects" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Projects.aspx.cs" Inherits="MIS.Projects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3 col-sm-12 col-sm-offset-3 col-md-12 col-md-offset-3 col-lg-12 col-lg-offset-3">
                <a id="btnCreateProject" class="btn btn-primary" href="CreateNewProject.aspx">Create New Project</a>
            </div>
        </div>
    </div>
    <br />
    <asp:Literal runat="server" ID="projectsList" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
</asp:Content>