﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;

namespace MIS
{
    public partial class SIOProjects : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "SIO")
                {
                    if (!Page.IsPostBack)
                    {
                        DrawSIOProjectList();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;
                    System.Web.HttpContext.Current.Session["sessionSubChainageGuid"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                    //Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("DBALogin.aspx");
            }
        }

        private void DrawSIOProjectList()
        {
            List<SubChainageManager> subChainageManagerList = new List<SubChainageManager>();
            subChainageManagerList = new SubChainageDB().GetSubChainagesListForSio(new Guid(Session["sessionUserGuid"].ToString()));

            StringBuilder htmlSubChainageList = new StringBuilder();
            htmlSubChainageList.Append("<div id=\"table-container\" style=\"padding:1%;\">");

            //if (subChainageManagerList.Count > 0)
            //{
                htmlSubChainageList.Append("<div class=\"table-responsive\">");
                htmlSubChainageList.Append("<table id=\"sioSubChainageList\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

                htmlSubChainageList.Append("<thead>");
                htmlSubChainageList.Append("<tr>");
                htmlSubChainageList.Append("<th>" + "#" + "</th>");
                htmlSubChainageList.Append("<th>" + "SubChainage ID" + "</th>");
                htmlSubChainageList.Append("<th>" + "SubChainage Name" + "</td>");
                htmlSubChainageList.Append("<th>" + "Project Name" + "</th>");
                htmlSubChainageList.Append("<th>" + "SubChainage Start Date" + "</th>");
                htmlSubChainageList.Append("<th>" + "SubChainage End Date" + "</th>");
                htmlSubChainageList.Append("<th>" + "SubChainage Dates Used" + "</th>");
                htmlSubChainageList.Append("<th>" + "Current Date" + "</th>");
                htmlSubChainageList.Append("<th>" + "Progress" + "</th>");
                htmlSubChainageList.Append("</tr>");
                htmlSubChainageList.Append("</thead>");

                htmlSubChainageList.Append("<tfoot>");
                htmlSubChainageList.Append("<tr>");
                htmlSubChainageList.Append("<th>" + "#" + "</th>");
                htmlSubChainageList.Append("<th>" + "SubChainage ID" + "</th>");
                htmlSubChainageList.Append("<th>" + "SubChainage Name" + "</td>");
                htmlSubChainageList.Append("<th>" + "Project Name" + "</th>");
                htmlSubChainageList.Append("<th>" + "SubChainage Start Date" + "</th>");
                htmlSubChainageList.Append("<th>" + "SubChainage End Date" + "</th>");
                htmlSubChainageList.Append("<th>" + "SubChainage Dates Used" + "</th>");
                htmlSubChainageList.Append("<th>" + "Current Date" + "</th>");
                htmlSubChainageList.Append("<th>" + "Progress" + "</th>");
                htmlSubChainageList.Append("</tr>");
                htmlSubChainageList.Append("</tfoot>");

                htmlSubChainageList.Append("<tbody>");

                foreach (SubChainageManager subChainageManagerObj in subChainageManagerList)
                {
                    bool editEnabled = false;
                    StringBuilder disabledDates = new StringBuilder();

                    htmlSubChainageList.Append("<tr>");

                    htmlSubChainageList.Append("<td>" + "" + "</td>");
                    htmlSubChainageList.Append("<td>" + subChainageManagerObj.SubChainageGuid + "</td>");
                    htmlSubChainageList.Append("<td>" + subChainageManagerObj.SubChainageName + "</td>");
                    htmlSubChainageList.Append("<td>" + subChainageManagerObj.ProjectManager.ProjectName + "</td>");
                    htmlSubChainageList.Append("<td>" + subChainageManagerObj.SubChainageStartingDate.ToString(@"dd\/MM\/yyyy") + "</td>");

                    int result = DateTime.Compare(subChainageManagerObj.SubChainageActualCompletionDate, DateTime.Today);
                    if (result < 0)
                    {
                        htmlSubChainageList.Append("<td>" + DateTime.Today.ToString(@"dd\/MM\/yyyy") + "</td>");//date1<date2
                    }
                    else if (result == 0)
                    {
                        htmlSubChainageList.Append("<td>" + DateTime.Today.ToString(@"dd\/MM\/yyyy") + "</td>");//date1==date2
                    }
                    else
                    {
                        htmlSubChainageList.Append("<td>" + subChainageManagerObj.SubChainageActualCompletionDate.ToString(@"dd\/MM\/yyyy") + "</td>");//date1>date2
                    }

                    foreach (SubChainagePhysicalProgressManager subChainagePhysicalProgressManagerObj in subChainageManagerObj.SubChainagePhysicalProgressManager)
                    {
                        int index = subChainageManagerObj.SubChainagePhysicalProgressManager.IndexOf(subChainagePhysicalProgressManagerObj);

                        if (index == subChainageManagerObj.SubChainagePhysicalProgressManager.Count - 1)
                        {
                            disabledDates.Append(subChainagePhysicalProgressManagerObj.PhysicalProgressDate.ToString(@"dd\/MM\/yyyy"));
                        }
                        else
                        {
                            disabledDates.Append(subChainagePhysicalProgressManagerObj.PhysicalProgressDate.ToString(@"dd\/MM\/yyyy") + ",");
                        }

                        if (subChainagePhysicalProgressManagerObj.CanBeEdited)
                        {
                            editEnabled = true;
                        }
                    }

                    htmlSubChainageList.Append("<td>" + disabledDates + "</td>");

                    htmlSubChainageList.Append("<td>" + DateTime.Today.ToString(@"dd\/MM\/yyyy") + "</td>");

                    htmlSubChainageList.Append("<td>");

                    htmlSubChainageList.Append("<a href=\"#\" class=\"btn addSubChainageProgressBtn\"><i class=\"ui-tooltip fa fa-plus-square-o\" style=\"font-size: 22px;\" data-original-title=\"Add Progress\" title=\"Add Progress\"></i></a>");

                    htmlSubChainageList.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                    if (subChainageManagerObj.SubChainagePhysicalProgressManager.Count > 0)
                    {
                        htmlSubChainageList.Append("<a href=\"#\" class=\"btn viewSubChainageProgressBtn\"><i class=\"ui-tooltip fa fa-file-text-o\" style=\"font-size: 22px;\" data-original-title=\"View Progress\" title=\"View Progress\"></i></a>");

                        htmlSubChainageList.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                        if (editEnabled)
                        {
                            htmlSubChainageList.Append("<a href=\"#\" class=\"btn editSubChainageProgressBtn\"><i class=\"ui-tooltip fa fa-pencil-square-o\" style=\"font-size: 22px;\" data-original-title=\"Edit Progress\" title=\"Edit Progress\"></i></a>");
                        }
                        else
                        {
                            htmlSubChainageList.Append("<a href=\"#\" class=\"btn editSubChainageProgressBtn disabled\"><i class=\"ui-tooltip fa fa-pencil-square-o\" style=\"font-size: 22px;\" data-original-title=\"Edit Progress\" title=\"Edit Progress\"></i></a>");
                        }

                        htmlSubChainageList.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                        htmlSubChainageList.Append("<a href=\"#\" class=\"btn deleteSubChainageProgressBtn disabled\"><i class=\"ui-tooltip fa fa-trash-o\" style=\"font-size: 22px;\" data-original-title=\"Delete Progress\" title=\"Delete Progress\"></i></a>");

                        htmlSubChainageList.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                        htmlSubChainageList.Append("<a href=\"#\" class=\"btn viewSubChainageProgressChartBtn\"><i class=\"ui-tooltip fa fa-bar-chart-o\" style=\"font-size: 22px;\" data-original-title=\"View Progress Chart\" title=\"View Progress Chart\"></i></a>" + "</td>");
                    }
                    else
                    {
                        htmlSubChainageList.Append("<a href=\"#\" class=\"btn viewSubChainageProgressBtn disabled\"><i class=\"ui-tooltip fa fa-file-text-o\" style=\"font-size: 22px;\" data-original-title=\"View Progress\" title=\"View Progress\"></i></a>");

                        htmlSubChainageList.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                        if (editEnabled)
                        {
                            htmlSubChainageList.Append("<a href=\"#\" class=\"btn editSubChainageProgressBtn\"><i class=\"ui-tooltip fa fa-pencil-square-o\" style=\"font-size: 22px;\" data-original-title=\"Edit Progress\" title=\"Edit Progress\"></i></a>");
                        }
                        else
                        {
                            htmlSubChainageList.Append("<a href=\"#\" class=\"btn editSubChainageProgressBtn disabled\"><i class=\"ui-tooltip fa fa-pencil-square-o\" style=\"font-size: 22px;\" data-original-title=\"Edit Progress\" title=\"Edit Progress\"></i></a>");
                        }

                        htmlSubChainageList.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                        htmlSubChainageList.Append("<a href=\"#\" class=\"btn deleteSubChainageProgressBtn disabled\"><i class=\"ui-tooltip fa fa-trash-o\" style=\"font-size: 22px;\" data-original-title=\"Delete Progress\" title=\"Delete Progress\"></i></a>");

                        htmlSubChainageList.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                        htmlSubChainageList.Append("<a href=\"#\" class=\"btn viewSubChainageProgressChartBtn disabled\"><i class=\"ui-tooltip fa fa-bar-chart-o\" style=\"font-size: 22px;\" data-original-title=\"View Progress Chart\" title=\"View Progress Chart\"></i></a>" + "</td>");
                    }

                    //htmlSubChainageList.Append("<td>" + "<button class=\"enterProgressBtn\">Enter</button>" + "<button class=\"editProgressBtn\">Edit</button>" + "<button class=\"viewProgressBtn\">View</button>" + "</td>");

                    htmlSubChainageList.Append("</tr>");
                }

                htmlSubChainageList.Append("</tbody>");

                htmlSubChainageList.Append("</table>");
                htmlSubChainageList.Append("</div>");
            //}
            //else
            //{
            //    htmlSubChainageList.Append("<p>" + "No Project has been assigned to you yet." + "</p>");
            //}

            htmlSubChainageList.Append("</div>");

            subChainagesList.Text = htmlSubChainageList.ToString();
        }

        [WebMethod]
        public static void SetSessionParameter(Guid subChainageGuid)
        {
            System.Web.HttpContext.Current.Session["sessionSubChainageGuid"] = subChainageGuid;
        }

        [WebMethod]
        public static bool CheckSubChainageProgressExistenceForGivenMonth(Guid subChainageGuid, DateTime selectedDate)
        {
            return new SubChainagePhysicalProgressDB().CheckSubChainageCurrentMonthProgressExistence(subChainageGuid, selectedDate);
        }

        [WebMethod]
        public static string CreateAddMonthlyProgressPopupModalBodyString(Guid subChainageGuid, DateTime selectedDate)
        {
            SubChainageManager subChainageObj = new SubChainageManager();
            subChainageObj = new SubChainageDB().GetSubChainageDetails(subChainageGuid);
            
            StringBuilder popupModalBodyString = new StringBuilder();

            popupModalBodyString.Append("<div class=\"container-fluid col-md-12\">");
            popupModalBodyString.Append("<div class=\"row\">");
            popupModalBodyString.Append("<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">");
            popupModalBodyString.Append("<div class=\"panel panel-default\">");
            popupModalBodyString.Append("<div class=\"panel-body\">");
            popupModalBodyString.Append("<div class=\"text-center\">");
            popupModalBodyString.Append("<img src=\"Images/calender2.png\" class=\"login\" height=\"70\" />");
            popupModalBodyString.Append("<h3 class=\"text-center\">" + "Enter the Progress for " + selectedDate.ToString("MMMM, yyyy") + "." + "</h3>");

            popupModalBodyString.Append("<div class=\"panel-body\">");
            popupModalBodyString.Append("<form id=\"addMonthlyProgressPopupForm\" name=\"addMonthlyProgressPopupForm\" role=\"form\" class=\"form form-horizontal\" method=\"post\">");

            popupModalBodyString.Append("<fieldset class=\"scheduler-border\">");
            popupModalBodyString.Append("<legend class=\"scheduler-border\">Fremaa Officers</legend>");
            if (subChainageObj.ProjectManager.OfficersEngaged.Count > 0)
            {
                //popupModalBodyString.Append("<div class=\"form-group\">");

                foreach (ProjectAndFremaaOfficersRelationManager projectAndFremaaOfficersRelationManagerObj in subChainageObj.ProjectManager.OfficersEngaged)
                {
                    var index = subChainageObj.ProjectManager.OfficersEngaged.IndexOf(projectAndFremaaOfficersRelationManagerObj);

                    popupModalBodyString.Append("<div class=\"form-group\">");

                    popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-left\">");
                    popupModalBodyString.Append(projectAndFremaaOfficersRelationManagerObj.FremaaOfficerName);
                    popupModalBodyString.Append("</div>");

                    //popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 fremaaOfficers\">");
                    //popupModalBodyString.Append("<input id=\"hdFremaaOfficer" + index + "\" name=\"hdFremaaOfficer" + index + "\" type=\"hidden\" value=\"" + projectAndFremaaOfficersRelationManagerObj.FremaaOfficerProjectRelationshipGuid + "\" />");
                    //popupModalBodyString.Append("<div class=\"btn-group\" data-toggle=\"buttons\">");
                    //popupModalBodyString.Append("<label class=\"btn btn-default btn-success\">");
                    //popupModalBodyString.Append("<input type=\"radio\" id=\"officerVisited" + index + "1\" name=\"officerVisited" + index + "\" value=\"yes\">Visited</input>");
                    //popupModalBodyString.Append("</label>");
                    //popupModalBodyString.Append("<label class=\"btn btn-default active btn-danger\">");
                    //popupModalBodyString.Append("<input type=\"radio\" id=\"officerVisited" + index + "2\" name=\"officerVisited" + index + "\" value=\"no\" checked=\"checked\">Not Visited</input>");
                    //popupModalBodyString.Append("</label>");
                    //popupModalBodyString.Append("</div>");
                    //popupModalBodyString.Append("</div>");


                    popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
                    popupModalBodyString.Append("<label class=\"radio-inline fremaaOfficers\">");
                    popupModalBodyString.Append("<input id=\"hdFremaaOfficer" + index + "\" name=\"hdFremaaOfficer" + index + "\" type=\"hidden\" value=\"" + projectAndFremaaOfficersRelationManagerObj.FremaaOfficerProjectRelationshipGuid + "\" />");
                    popupModalBodyString.Append("<input type=\"radio\" name=\"officerVisited" + index + "\" value=\"yes\">Visited</input>");
                    popupModalBodyString.Append("</label>");
                    popupModalBodyString.Append("<label class=\"radio-inline\">");
                    popupModalBodyString.Append("<input type=\"radio\" name=\"officerVisited" + index + "\" value=\"no\" checked=\"checked\">Not Visited</input>");
                    popupModalBodyString.Append("</label>");
                    popupModalBodyString.Append("</div>");

                    popupModalBodyString.Append("</div>");
                }

                //popupModalBodyString.Append("</div>");
            }
            else
            {
                popupModalBodyString.Append("No Fremaa Officer.");
            }
            popupModalBodyString.Append("</fieldset>");


            popupModalBodyString.Append("<fieldset class=\"scheduler-border\">");
            popupModalBodyString.Append("<legend class=\"scheduler-border\">Pmc Site Engineers</legend>");
            if (subChainageObj.ProjectManager.PmcSiteEngineers.Count > 0)
            {
                //popupModalBodyString.Append("<div class=\"form-group\">");

                foreach (ProjectAndPmcSiteEngineersRelationManager projectAndPmcSiteEngineersRelationManagerObj in subChainageObj.ProjectManager.PmcSiteEngineers)
                {
                    var index = subChainageObj.ProjectManager.PmcSiteEngineers.IndexOf(projectAndPmcSiteEngineersRelationManagerObj);

                    popupModalBodyString.Append("<div class=\"form-group\">");

                    popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-left\">");
                    popupModalBodyString.Append(projectAndPmcSiteEngineersRelationManagerObj.SiteEngineerName);
                    popupModalBodyString.Append("</div>");

                    //popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 pmcSiteEngineers\">");
                    //popupModalBodyString.Append("<input id=\"hdPmcSiteEngineer" + index + "\" name=\"hdPmcSiteEngineer" + index + "\" type=\"hidden\" value=\"" + projectAndPmcSiteEngineersRelationManagerObj.PmcSiteEngineerProjectRelationshipGuid + "\" />");
                    //popupModalBodyString.Append("<div class=\"btn-group\" data-toggle=\"buttons\">");
                    //popupModalBodyString.Append("<label class=\"btn btn-default btn-success\">");
                    //popupModalBodyString.Append("<input type=\"radio\" id=\"siteEngineers" + index + "1\" name=\"siteEngineers" + index + "\" value=\"yes\">Visited</input>");
                    //popupModalBodyString.Append("</label>");
                    //popupModalBodyString.Append("<label class=\"btn btn-default active btn-danger\">");
                    //popupModalBodyString.Append("<input type=\"radio\" id=\"siteEngineers" + index + "2\" name=\"siteEngineers" + index + "\" value=\"no\" checked=\"checked\">Not Visited</input>");
                    //popupModalBodyString.Append("</label>");
                    //popupModalBodyString.Append("</div>");
                    //popupModalBodyString.Append("</div>");


                    popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
                    popupModalBodyString.Append("<label class=\"radio-inline pmcSiteEngineers\">");
                    popupModalBodyString.Append("<input id=\"hdPmcSiteEngineer" + index + "\" name=\"hdPmcSiteEngineer" + index + "\" type=\"hidden\" value=\"" + projectAndPmcSiteEngineersRelationManagerObj.PmcSiteEngineerProjectRelationshipGuid + "\" />");
                    popupModalBodyString.Append("<input type=\"radio\" name=\"siteEngineers" + index + "\" value=\"yes\" />Visited");
                    popupModalBodyString.Append("</label>");
                    popupModalBodyString.Append("<label class=\"radio-inline\">");
                    popupModalBodyString.Append("<input type=\"radio\" name=\"siteEngineers" + index + "\" value=\"no\" checked=\"checked\" />Not Visited");
                    popupModalBodyString.Append("</label>");
                    popupModalBodyString.Append("</div>");

                    popupModalBodyString.Append("</div>");
                }

                //popupModalBodyString.Append("</div>");
            }
            else
            {
                popupModalBodyString.Append("No PMC Site Engineer.");
            }
            popupModalBodyString.Append("</fieldset>");


            popupModalBodyString.Append("<fieldset class=\"scheduler-border\">");
            popupModalBodyString.Append("<legend class=\"scheduler-border\">Monthly Physical Progress</legend>");
            if (subChainageObj.WorkItemManager.Count > 0)
            {
                foreach (WorkItemManager workItemObj in subChainageObj.WorkItemManager)
                {
                    int workItemIndex = subChainageObj.WorkItemManager.IndexOf(workItemObj);

                    string itemName = workItemObj.WorkItemName;

                    itemName = itemName.Replace(" ", string.Empty);

                    popupModalBodyString.Append("<div class=\"form-group\">");

                    popupModalBodyString.Append("<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden\">");
                    popupModalBodyString.Append("<div class=\"workItems0\">");
                    popupModalBodyString.Append("<input id=\"hdWorkItemGuid" + workItemIndex + "\" name=\"hdWorkItemGuid" + workItemIndex + "\" type=\"hidden\" value=\"" + workItemObj.WorkItemGuid + "\" />");
                    popupModalBodyString.Append("<input id=\"hdWorkItemTotalValue" + workItemIndex + "\" name=\"hdWorkItemTotalValue" + workItemIndex + "\" type=\"hidden\" value=\"" + workItemObj.WorkItemTotalValue + "\" />");
                    popupModalBodyString.Append("<input id=\"hdWorkItemCumulativeValue" + workItemIndex + "\" name=\"hdWorkItemCumulativeValue" + workItemIndex + "\" type=\"hidden\" value=\"" + workItemObj.WorkItemCumulativeValue + "\" />");
                    popupModalBodyString.Append("<input id=\"hdWorkItemBalanceValue" + workItemIndex + "\" name=\"hdWorkItemBalanceValue" + workItemIndex + "\" type=\"hidden\" value=\"" + workItemObj.WorkItemBalanceValue + "\" />");
                    popupModalBodyString.Append("</div>");
                    popupModalBodyString.Append("</div>");

                    popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
                    popupModalBodyString.Append("<label for=\"" + itemName.ToLower() + "\" id=\"" + itemName.ToLower() + "Label\" class=\"control-label text-left\">" + workItemObj.WorkItemName + "</label>");
                    popupModalBodyString.Append("</div>");

                    popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
                    popupModalBodyString.Append("<div class=\"input-group workItems\">");
                    popupModalBodyString.Append("<input type=\"text\" name=\"" + itemName.ToLower() + "\" class=\"form-control workItemsMonthlyValues\" id=\"" + itemName.ToLower() + "\" placeholder=\"" + workItemObj.WorkItemName + "\" number=\"number\" />");
                    popupModalBodyString.Append("<span class=\"input-group-addon form-control-static\">");
                    popupModalBodyString.Append("<i>" + workItemObj.UnitManager.UnitSymbol + "</i>");
                    popupModalBodyString.Append("</span>");
                    popupModalBodyString.Append("</div>");
                    popupModalBodyString.Append("</div>");

                    popupModalBodyString.Append("</div>");
                }
            }
            else
            {
                popupModalBodyString.Append("No WorkItem.");
            }
            popupModalBodyString.Append("</fieldset>");

            popupModalBodyString.Append("<div class=\"form-group\">");

            popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
            popupModalBodyString.Append("<a href=\"#\" id=\"saveNewMonthlyProgressBtn\" name=\"saveNewMonthlyProgressBtn\" type=\"button\" class=\"btn btn-lg btn-success btn-block\">");
            popupModalBodyString.Append("<span>");
            popupModalBodyString.Append("<i class=\"ui-tooltip fa fa-floppy-o\" style=\"font-size: 22px;\" data-original-title=\"Save\" title=\"Save\"></i>");
            popupModalBodyString.Append("</span>");
            popupModalBodyString.Append("  Save");
            popupModalBodyString.Append("</a>");
            popupModalBodyString.Append("</div>");

            popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
            popupModalBodyString.Append("<a href=\"#\" id=\"cancelNewMonthlyProgressBtn\" name=\"cancelNewMonthlyProgressBtn\" type=\"button\" class=\"btn btn-lg btn-danger btn-block\">");
            popupModalBodyString.Append("<span>");
            popupModalBodyString.Append("<i class=\"ui-tooltip fa fa-times-circle-o\" style=\"font-size: 22px;\" data-original-title=\"Cancel\" title=\"Cancel\"></i>");
            popupModalBodyString.Append("</span>");
            popupModalBodyString.Append(" Cancel");
            popupModalBodyString.Append("</a>");
            popupModalBodyString.Append("</div>");

            popupModalBodyString.Append("</div>");

            popupModalBodyString.Append("</form>");
            popupModalBodyString.Append("</div>");

            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");

            return popupModalBodyString.ToString();
        }

        [WebMethod]
        public static int AddProjectPhysicalProgress(SubChainagePhysicalProgressManager subChainagePhysicalProgressManager)
        {
            subChainagePhysicalProgressManager.PhysicalProgressGuid = Guid.NewGuid();

            return new SubChainagePhysicalProgressDB().AddSubChainagePhysicalProgress(subChainagePhysicalProgressManager);
        }

        [WebMethod]
        public static string CreateViewMonthlyProgressPopupModalBodyString(Guid subChainageGuid, DateTime selectedDate)
        {
            SubChainagePhysicalProgressManager subChainagePhysicalProgressManagerObj = new SubChainagePhysicalProgressManager();
            subChainagePhysicalProgressManagerObj = new SubChainagePhysicalProgressDB().GetSubChainagePhysicalProgress(subChainageGuid, selectedDate);

            StringBuilder popupModalBodyString = new StringBuilder();

            popupModalBodyString.Append("<div class=\"container-fluid col-md-12\">");
            popupModalBodyString.Append("<div class=\"row\">");
            popupModalBodyString.Append("<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">");
            popupModalBodyString.Append("<div class=\"panel panel-default\">");
            popupModalBodyString.Append("<div class=\"panel-body\">");
            popupModalBodyString.Append("<div class=\"text-center\">");
            popupModalBodyString.Append("<img src=\"Images/calender2.png\" class=\"login\" height=\"70\" />");
            popupModalBodyString.Append("<h3 class=\"text-center\">" + "Progress for " + selectedDate.ToString("MMMM, yyyy") + "." + "</h3>");

            popupModalBodyString.Append("<div class=\"panel-body\">");
            popupModalBodyString.Append("<form id=\"viewMonthlyProgressPopupForm\" name=\"viewMonthlyProgressPopupForm\" role=\"form\" class=\"form form-horizontal\" method=\"post\">");

            //popupModalBodyString.Append("<fieldset class=\"scheduler-border\">");
            //popupModalBodyString.Append("<legend class=\"scheduler-border\">Fremaa Officers</legend>");
            //if (subChainagePhysicalProgressManagerObj.FremaaOfficerProgressManager.Count > 0)
            //{
            //    //popupModalBodyString.Append("<div class=\"form-group\">");

            //    foreach (FremaaOfficerProgressManager fremaaOfficerProgressManagerObj in subChainagePhysicalProgressManagerObj.FremaaOfficerProgressManager)
            //    {
            //        popupModalBodyString.Append("<div class=\"form-group\">");

            //        popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-left\">");
            //        popupModalBodyString.Append(fremaaOfficerProgressManagerObj.ProjectAndFremaaOfficersRelationManager.FremaaOfficerName);
            //        popupModalBodyString.Append("</div>");
            //        popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 fremaaOfficers\">");
            //        if (fremaaOfficerProgressManagerObj.FremaaOfficerArose)
            //        {
            //            popupModalBodyString.Append("<label class=\"label-success\">Visited</label>");
            //        }
            //        else
            //        {
            //            popupModalBodyString.Append("<label class=\"label-danger\">Not Visited</label>");
            //        }
            //        popupModalBodyString.Append("</div>");

            //        popupModalBodyString.Append("</div>");
            //    }

            //    //popupModalBodyString.Append("</div>");
            //}
            //else
            //{
            //    popupModalBodyString.Append("No Fremaa Officer.");
            //}
            //popupModalBodyString.Append("</fieldset>");


            //popupModalBodyString.Append("<fieldset class=\"scheduler-border\">");
            //popupModalBodyString.Append("<legend class=\"scheduler-border\">Pmc Site Engineers</legend>");
            //if (subChainagePhysicalProgressManagerObj.PmcSiteEngineerProgressManager.Count > 0)
            //{
            //    //popupModalBodyString.Append("<div class=\"form-group\">");

            //    foreach (PmcSiteEngineerProgressManager pmcSiteEngineerProgressManagerObj in subChainagePhysicalProgressManagerObj.PmcSiteEngineerProgressManager)
            //    {
            //        popupModalBodyString.Append("<div class=\"form-group\">");

            //        popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-left\">");
            //        popupModalBodyString.Append(pmcSiteEngineerProgressManagerObj.ProjectAndPmcSiteEngineersRelationManager.SiteEngineerName);
            //        popupModalBodyString.Append("</div>");
            //        popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 pmcSiteEngineers\">");
            //        if (pmcSiteEngineerProgressManagerObj.PmcSiteEngineerArose)
            //        {
            //            popupModalBodyString.Append("<label class=\"label-success\">Visited</label>");
            //        }
            //        else
            //        {
            //            popupModalBodyString.Append("<label class=\"label-danger\">Not Visited</label>");
            //        }
            //        popupModalBodyString.Append("</div>");

            //        popupModalBodyString.Append("</div>");
            //    }

            //    //popupModalBodyString.Append("</div>");
            //}
            //else
            //{
            //    popupModalBodyString.Append("No PMC Site Engineer.");
            //}
            //popupModalBodyString.Append("</fieldset>");


            //popupModalBodyString.Append("<fieldset class=\"scheduler-border\">");
            //popupModalBodyString.Append("<legend class=\"scheduler-border\">Monthly Physical Progress</legend>");
            //if (subChainagePhysicalProgressManagerObj.WorkItemProgressManager.Count > 0)
            //{
            //    foreach (WorkItemProgressManager workItemprogressManagerObj in subChainagePhysicalProgressManagerObj.WorkItemProgressManager)
            //    {
            //        string itemName = workItemprogressManagerObj.WorkItemManager.WorkItemName;

            //        itemName = itemName.Replace(" ", string.Empty);

            //        popupModalBodyString.Append("<div class=\"form-group\">");
            //        popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
            //        popupModalBodyString.Append("<label for=\"" + itemName.ToLower() + "\" id=\"" + itemName.ToLower() + "Label\" class=\"control-label\">" + workItemprogressManagerObj.WorkItemManager.WorkItemName + "</label>");
            //        popupModalBodyString.Append("</div>");
            //        popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
            //        popupModalBodyString.Append("<div class=\"input-group workItems\">");
            //        popupModalBodyString.Append("<input type=\"text\" name=\"" + itemName.ToLower() + "\" class=\"form-control workItemsMonthlyValues\" id=\"" + itemName.ToLower() + "\" placeholder=\"" + workItemprogressManagerObj.WorkItemManager.WorkItemName + "\" number=\"number\" />");
            //        popupModalBodyString.Append("<span class=\"input-group-addon form-control-static\">");
            //        popupModalBodyString.Append("<i>" + workItemprogressManagerObj.WorkItemManager.UnitManager.UnitSymbol + "</i>");
            //        popupModalBodyString.Append("</span>");
            //        popupModalBodyString.Append("</div>");
            //        popupModalBodyString.Append("</div>");
            //        popupModalBodyString.Append("</div>");
            //    }
            //}
            //else
            //{
            //    popupModalBodyString.Append("No WorkItem.");
            //}
            //popupModalBodyString.Append("</fieldset>");





            popupModalBodyString.Append("<div class=\"form-group\">");
            popupModalBodyString.Append("<div id=\"table-container\" style=\"padding:1%;\">");

            popupModalBodyString.Append("<div class=\"table-responsive\">");
            popupModalBodyString.Append("<table id=\"subChainageMonthlyProgressTable\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

            popupModalBodyString.Append("<thead>");
            popupModalBodyString.Append("</thead>");

            popupModalBodyString.Append("<tbody>");

            if (subChainagePhysicalProgressManagerObj.FremaaOfficerProgressManager.Count >= 0)
            {
                popupModalBodyString.Append("<tr>");
                popupModalBodyString.Append("<td colspan=\"3\">" + "Fremaa Officers" + "</td>");
                popupModalBodyString.Append("</tr>");

                if (subChainagePhysicalProgressManagerObj.FremaaOfficerProgressManager.Count == 0)
                {
                    popupModalBodyString.Append("<tr>");
                    popupModalBodyString.Append("<td colspan=\"3\">" + "No Fremaa Officers" + "</td>");
                    popupModalBodyString.Append("</tr>");
                }

                foreach (FremaaOfficerProgressManager fremaaOfficerProgressManagerObj in subChainagePhysicalProgressManagerObj.FremaaOfficerProgressManager)
                {
                    popupModalBodyString.Append("<tr>");
                    popupModalBodyString.Append("<td>" + fremaaOfficerProgressManagerObj.ProjectAndFremaaOfficersRelationManager.FremaaOfficerName + "</td>");
                    popupModalBodyString.Append("<td>" + ":" + "</td>");
                    popupModalBodyString.Append("<td>");
                    if (fremaaOfficerProgressManagerObj.FremaaOfficerArose)
                    {
                        popupModalBodyString.Append("<label class=\"label-success\">Visited</label>");
                    }
                    else
                    {
                        popupModalBodyString.Append("<label class=\"label-danger\">Not Visited</label>");
                    }
                    popupModalBodyString.Append("</td>");
                    popupModalBodyString.Append("</tr>");
                }

            }

            if (subChainagePhysicalProgressManagerObj.PmcSiteEngineerProgressManager.Count >= 0)
            {
                popupModalBodyString.Append("<tr>");
                popupModalBodyString.Append("<td colspan=\"3\">" + "Pmc Site Engineers" + "</td>");
                popupModalBodyString.Append("</tr>");

                if (subChainagePhysicalProgressManagerObj.FremaaOfficerProgressManager.Count == 0)
                {
                    popupModalBodyString.Append("<tr>");
                    popupModalBodyString.Append("<td colspan=\"3\">" + "No Pmc Site Engineers" + "</td>");
                    popupModalBodyString.Append("</tr>");
                }

                foreach (PmcSiteEngineerProgressManager pmcSiteEngineerProgressManagerObj in subChainagePhysicalProgressManagerObj.PmcSiteEngineerProgressManager)
                {
                    popupModalBodyString.Append("<tr>");
                    popupModalBodyString.Append("<td>" + pmcSiteEngineerProgressManagerObj.ProjectAndPmcSiteEngineersRelationManager.SiteEngineerName + "</td>");
                    popupModalBodyString.Append("<td>" + ":" + "</td>");
                    popupModalBodyString.Append("<td>");
                    if (pmcSiteEngineerProgressManagerObj.PmcSiteEngineerArose)
                    {
                        popupModalBodyString.Append("<label class=\"label-success\">Visited</label>");
                    }
                    else
                    {
                        popupModalBodyString.Append("<label class=\"label-danger\">Not Visited</label>");
                    }
                    popupModalBodyString.Append("</td>");
                    popupModalBodyString.Append("</tr>");
                }

            }

            if (subChainagePhysicalProgressManagerObj.WorkItemProgressManager.Count >= 0)
            {
                popupModalBodyString.Append("<tr>");
                popupModalBodyString.Append("<td colspan=\"3\">" + "Project WorkItem Progress" + "</td>");
                popupModalBodyString.Append("</tr>");

                if (subChainagePhysicalProgressManagerObj.FremaaOfficerProgressManager.Count == 0)
                {
                    popupModalBodyString.Append("<tr>");
                    popupModalBodyString.Append("<td colspan=\"3\">" + "No WorkItems" + "</td>");
                    popupModalBodyString.Append("</tr>");
                }

                foreach (WorkItemProgressManager workItemprogressManagerObj in subChainagePhysicalProgressManagerObj.WorkItemProgressManager)
                {
                    popupModalBodyString.Append("<tr>");
                    popupModalBodyString.Append("<td>" + workItemprogressManagerObj.WorkItemManager.WorkItemName + "</td>");
                    popupModalBodyString.Append("<td>" + ":" + "</td>");
                    popupModalBodyString.Append("<td>" + workItemprogressManagerObj.WorkItemProgressValue + " " + workItemprogressManagerObj.WorkItemManager.UnitManager.UnitSymbol + "</td>");
                    popupModalBodyString.Append("</tr>");
                }

            }

            popupModalBodyString.Append("</tbody>");

            popupModalBodyString.Append("</table>");
            popupModalBodyString.Append("</div>");

            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");



            




            popupModalBodyString.Append("</form>");
            popupModalBodyString.Append("</div>");


            //popupModalBodyString.Append("<div id=\"progressChartContainer\" style=\"width:100%; height:400px; margin: 0 auto\"></div>");

            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");

            return popupModalBodyString.ToString();
        }

        [WebMethod]
        public static string CreateEditMonthlyProgressPopupModalBodyString(Guid subChainageGuid, DateTime selectedDate)
        {
            SubChainagePhysicalProgressManager subChainagePhysicalProgressManagerObj = new SubChainagePhysicalProgressManager();
            subChainagePhysicalProgressManagerObj = new SubChainagePhysicalProgressDB().GetSubChainagePhysicalProgress(subChainageGuid, selectedDate);

            SubChainageManager subChainageManagerObj = new SubChainageManager();
            subChainageManagerObj = new SubChainageDB().GetSubChainageDetails(subChainageGuid);

            StringBuilder popupModalBodyString = new StringBuilder();

            popupModalBodyString.Append("<div class=\"container-fluid col-md-12\">");
            popupModalBodyString.Append("<div class=\"row\">");
            popupModalBodyString.Append("<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">");
            popupModalBodyString.Append("<div class=\"panel panel-default\">");
            popupModalBodyString.Append("<div class=\"panel-body\">");
            popupModalBodyString.Append("<div class=\"text-center\">");
            popupModalBodyString.Append("<img src=\"Images/calender2.png\" class=\"login\" height=\"70\" />");
            popupModalBodyString.Append("<h3 class=\"text-center\">" + "Enter the Progress for " + selectedDate.ToString("MMMM, yyyy") + "." + "</h3>");

            popupModalBodyString.Append("<div class=\"panel-body\">");
            popupModalBodyString.Append("<form id=\"editMonthlyProgressPopupForm\" name=\"editMonthlyProgressPopupForm\" role=\"form\" class=\"form form-horizontal\" method=\"post\">");

            popupModalBodyString.Append("<input id=\"hdSubChainagePhysicalProgressGuid\" name=\"hdSubChainagePhysicalProgressGuid\" type=\"hidden\" value=\"" + subChainagePhysicalProgressManagerObj.PhysicalProgressGuid + "\" />");

            popupModalBodyString.Append("<fieldset class=\"scheduler-border\">");
            popupModalBodyString.Append("<legend class=\"scheduler-border\">Fremaa Officers</legend>");
            if (subChainageManagerObj.ProjectManager.OfficersEngaged.Count > 0)
            {
                //popupModalBodyString.Append("<div class=\"form-group\">");

                foreach (ProjectAndFremaaOfficersRelationManager projectAndFremaaOfficersRelationManagerObj in subChainageManagerObj.ProjectManager.OfficersEngaged)
                {
                    int index = subChainageManagerObj.ProjectManager.OfficersEngaged.IndexOf(projectAndFremaaOfficersRelationManagerObj);
                    bool fremaaOfficerArose = false;

                    foreach (FremaaOfficerProgressManager fremaaOfficerProgressManagerObj in subChainagePhysicalProgressManagerObj.FremaaOfficerProgressManager)
                    {
                        if (fremaaOfficerProgressManagerObj.ProjectAndFremaaOfficersRelationManager.FremaaOfficerProjectRelationshipGuid == projectAndFremaaOfficersRelationManagerObj.FremaaOfficerProjectRelationshipGuid)
                        {
                            fremaaOfficerArose = fremaaOfficerProgressManagerObj.FremaaOfficerArose;
                            //if (fremaaOfficerProgressManagerObj.FremaaOfficerArose)
                            //{

                            //}
                            break;
                        }
                    }

                    popupModalBodyString.Append("<div class=\"form-group\">");

                    popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-left\">");
                    popupModalBodyString.Append(projectAndFremaaOfficersRelationManagerObj.FremaaOfficerName);
                    popupModalBodyString.Append("</div>");

                    popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
                    popupModalBodyString.Append("<label class=\"radio-inline fremaaOfficers\">");
                    popupModalBodyString.Append("<input id=\"hdFremaaOfficer" + index + "\" name=\"hdFremaaOfficer" + index + "\" type=\"hidden\" value=\"" + projectAndFremaaOfficersRelationManagerObj.FremaaOfficerProjectRelationshipGuid + "\" />");

                    if (fremaaOfficerArose)
                    {
                        popupModalBodyString.Append("<input type=\"radio\" name=\"officerVisited" + index + "\" value=\"yes\" checked=\"checked\">Visited</input>");
                        popupModalBodyString.Append("</label>");
                        popupModalBodyString.Append("<label class=\"radio-inline\">");
                        popupModalBodyString.Append("<input type=\"radio\" name=\"officerVisited" + index + "\" value=\"no\">Not Visited</input>");
                    }
                    else
                    {
                        popupModalBodyString.Append("<input type=\"radio\" name=\"officerVisited" + index + "\" value=\"yes\">Visited</input>");
                        popupModalBodyString.Append("</label>");
                        popupModalBodyString.Append("<label class=\"radio-inline\">");
                        popupModalBodyString.Append("<input type=\"radio\" name=\"officerVisited" + index + "\" value=\"no\" checked=\"checked\">Not Visited</input>");
                    }

                    popupModalBodyString.Append("</label>");
                    popupModalBodyString.Append("</div>");

                    popupModalBodyString.Append("</div>");
                }

                //popupModalBodyString.Append("</div>");
            }
            else
            {
                popupModalBodyString.Append("No Fremaa Officer.");
            }
            popupModalBodyString.Append("</fieldset>");


            popupModalBodyString.Append("<fieldset class=\"scheduler-border\">");
            popupModalBodyString.Append("<legend class=\"scheduler-border\">Pmc Site Engineers</legend>");
            if (subChainageManagerObj.ProjectManager.PmcSiteEngineers.Count > 0)
            {
                //popupModalBodyString.Append("<div class=\"form-group\">");

                foreach (ProjectAndPmcSiteEngineersRelationManager projectAndPmcSiteEngineersRelationManagerObj in subChainageManagerObj.ProjectManager.PmcSiteEngineers)
                {
                    var index = subChainageManagerObj.ProjectManager.PmcSiteEngineers.IndexOf(projectAndPmcSiteEngineersRelationManagerObj);
                    bool pmcSiteEngineerArose = false;

                    foreach (PmcSiteEngineerProgressManager pmcSiteEngineerProgressManagerObj in subChainagePhysicalProgressManagerObj.PmcSiteEngineerProgressManager)
                    {
                        if (pmcSiteEngineerProgressManagerObj.ProjectAndPmcSiteEngineersRelationManager.PmcSiteEngineerProjectRelationshipGuid == projectAndPmcSiteEngineersRelationManagerObj.PmcSiteEngineerProjectRelationshipGuid)
                        {
                            pmcSiteEngineerArose = pmcSiteEngineerProgressManagerObj.PmcSiteEngineerArose;
                            //if (fremaaOfficerProgressManagerObj.FremaaOfficerArose)
                            //{

                            //}
                            break;
                        }
                    }

                    popupModalBodyString.Append("<div class=\"form-group\">");

                    popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-left\">");
                    popupModalBodyString.Append(projectAndPmcSiteEngineersRelationManagerObj.SiteEngineerName);
                    popupModalBodyString.Append("</div>");

                    //popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 pmcSiteEngineers\">");
                    //popupModalBodyString.Append("<input id=\"hdPmcSiteEngineer" + index + "\" name=\"hdPmcSiteEngineer" + index + "\" type=\"hidden\" value=\"" + projectAndPmcSiteEngineersRelationManagerObj.PmcSiteEngineerProjectRelationshipGuid + "\" />");
                    //popupModalBodyString.Append("<div class=\"btn-group\" data-toggle=\"buttons\">");
                    //popupModalBodyString.Append("<label class=\"btn btn-default btn-success\">");
                    //popupModalBodyString.Append("<input type=\"radio\" id=\"siteEngineers" + index + "1\" name=\"siteEngineers" + index + "\" value=\"yes\">Visited</input>");
                    //popupModalBodyString.Append("</label>");
                    //popupModalBodyString.Append("<label class=\"btn btn-default active btn-danger\">");
                    //popupModalBodyString.Append("<input type=\"radio\" id=\"siteEngineers" + index + "2\" name=\"siteEngineers" + index + "\" value=\"no\" checked=\"checked\">Not Visited</input>");
                    //popupModalBodyString.Append("</label>");
                    //popupModalBodyString.Append("</div>");
                    //popupModalBodyString.Append("</div>");


                    popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
                    popupModalBodyString.Append("<label class=\"radio-inline pmcSiteEngineers\">");
                    popupModalBodyString.Append("<input id=\"hdPmcSiteEngineer" + index + "\" name=\"hdPmcSiteEngineer" + index + "\" type=\"hidden\" value=\"" + projectAndPmcSiteEngineersRelationManagerObj.PmcSiteEngineerProjectRelationshipGuid + "\" />");

                    if (pmcSiteEngineerArose)
                    {
                        popupModalBodyString.Append("<input type=\"radio\" name=\"siteEngineers" + index + "\" value=\"yes\" checked=\"checked\" />Visited");
                        popupModalBodyString.Append("</label>");
                        popupModalBodyString.Append("<label class=\"radio-inline\">");
                        popupModalBodyString.Append("<input type=\"radio\" name=\"siteEngineers" + index + "\" value=\"no\" />Not Visited");
                    }
                    else
                    {
                        popupModalBodyString.Append("<input type=\"radio\" name=\"siteEngineers" + index + "\" value=\"yes\" />Visited");
                        popupModalBodyString.Append("</label>");
                        popupModalBodyString.Append("<label class=\"radio-inline\">");
                        popupModalBodyString.Append("<input type=\"radio\" name=\"siteEngineers" + index + "\" value=\"no\" checked=\"checked\" />Not Visited");
                    }
                    
                    popupModalBodyString.Append("</label>");
                    popupModalBodyString.Append("</div>");

                    popupModalBodyString.Append("</div>");
                }

                //popupModalBodyString.Append("</div>");
            }
            else
            {
                popupModalBodyString.Append("No PMC Site Engineer.");
            }
            popupModalBodyString.Append("</fieldset>");


            popupModalBodyString.Append("<fieldset class=\"scheduler-border\">");
            popupModalBodyString.Append("<legend class=\"scheduler-border\">Monthly Physical Progress</legend>");
            if (subChainageManagerObj.WorkItemManager.Count > 0)
            {
                foreach (WorkItemManager workItemObj in subChainageManagerObj.WorkItemManager)
                {
                    int workItemIndex = subChainageManagerObj.WorkItemManager.IndexOf(workItemObj);

                    string workItemvalue = "";

                    string itemName = workItemObj.WorkItemName;

                    foreach (WorkItemProgressManager workItemProgressManagerObj in subChainagePhysicalProgressManagerObj.WorkItemProgressManager)
                    {
                        if (workItemObj.WorkItemGuid == workItemProgressManagerObj.WorkItemManager.WorkItemGuid)
                        {
                            workItemvalue = workItemProgressManagerObj.WorkItemProgressValue;
                        }
                    }

                    itemName = itemName.Replace(" ", string.Empty);

                    popupModalBodyString.Append("<div class=\"form-group\">");
                    popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
                    popupModalBodyString.Append("<label for=\"" + itemName.ToLower() + "\" id=\"" + itemName.ToLower() + "Label\" class=\"control-label\">" + workItemObj.WorkItemName + "</label>");
                    popupModalBodyString.Append("</div>");
                    popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
                    popupModalBodyString.Append("<div class=\"input-group workItems\">");
                    popupModalBodyString.Append("<input id=\"hdWorkItemGuid" + workItemIndex + "\" name=\"hdWorkItemGuid" + workItemIndex + "\" type=\"hidden\" value=\"" + workItemObj.WorkItemGuid + "\" />");
                    popupModalBodyString.Append("<input type=\"text\" name=\"" + itemName.ToLower() + "\" class=\"form-control workItemsMonthlyValues\" id=\"" + itemName.ToLower() + "\" placeholder=\"" + workItemObj.WorkItemName + "\" number=\"number\" value=\"" + workItemvalue + "\" />");
                    popupModalBodyString.Append("<span class=\"input-group-addon form-control-static\">");
                    popupModalBodyString.Append("<i>" + workItemObj.UnitManager.UnitSymbol + "</i>");
                    popupModalBodyString.Append("</span>");
                    popupModalBodyString.Append("</div>");
                    popupModalBodyString.Append("</div>");
                    popupModalBodyString.Append("</div>");
                }
            }
            else
            {
                popupModalBodyString.Append("No WorkItem.");
            }
            popupModalBodyString.Append("</fieldset>");

            popupModalBodyString.Append("<div class=\"form-group\">");

            popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
            popupModalBodyString.Append("<a href=\"#\" id=\"updateMonthlyProgressBtn\" name=\"updateMonthlyProgressBtn\" type=\"button\" class=\"btn btn-lg btn-success btn-block\">");
            popupModalBodyString.Append("<span>");
            popupModalBodyString.Append("<i class=\"ui-tooltip fa fa-floppy-o\" style=\"font-size: 22px;\" data-original-title=\"Save\" title=\"Save\"></i>");
            popupModalBodyString.Append("</span>");
            popupModalBodyString.Append("  Update");
            popupModalBodyString.Append("</a>");
            popupModalBodyString.Append("</div>");

            popupModalBodyString.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
            popupModalBodyString.Append("<a href=\"#\" id=\"cancelUpdateMonthlyProgressBtn\" name=\"cancelUpdateMonthlyProgressBtn\" type=\"button\" class=\"btn btn-lg btn-danger btn-block\">");
            popupModalBodyString.Append("<span>");
            popupModalBodyString.Append("<i class=\"ui-tooltip fa fa-times-circle-o\" style=\"font-size: 22px;\" data-original-title=\"Cancel\" title=\"Cancel\"></i>");
            popupModalBodyString.Append("</span>");
            popupModalBodyString.Append(" Cancel");
            popupModalBodyString.Append("</a>");
            popupModalBodyString.Append("</div>");

            popupModalBodyString.Append("</div>");

            popupModalBodyString.Append("</form>");
            popupModalBodyString.Append("</div>");

            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");
            popupModalBodyString.Append("</div>");

            return popupModalBodyString.ToString();
        }

        [WebMethod]
        public static List<WorkItemManager> GetWorkItemsBasedOnSubChainage(Guid subChainageGuid)
        {
            List<WorkItemManager> workItemManagerList = new List<WorkItemManager>();
            workItemManagerList = new WorkItemDB().GetWorkItemsWithUnitBasedOnSubChainage(subChainageGuid);

            return workItemManagerList;
        }

        [WebMethod]
        public static string GetSubChainageWorkItemMonthlyProgress(Guid workItemGuid, Guid subChainageGuid)
        {
            List<WorkItemProgressManager> workItemProgressManagerList = new List<WorkItemProgressManager>();
            workItemProgressManagerList = new WorkItemProgressDB().GetWorkItemProgressBasedOnSubChainage(workItemGuid, subChainageGuid);

            StringBuilder series = new StringBuilder();

            if (workItemProgressManagerList.Count > 0)
            {
                series.Append("[");

                series.Append("{ ");

                series.Append("\"type\":\"column\",");

                var workItemName = workItemProgressManagerList.FirstOrDefault(x => x.WorkItemManager.WorkItemGuid == workItemGuid);

                series.Append("\"name\":\"" + workItemName.WorkItemManager.WorkItemName + "\",");

                series.Append("\"data\": [");
                foreach (WorkItemProgressManager workItemProgressManagerObj in workItemProgressManagerList)
                {
                    int index = workItemProgressManagerList.IndexOf(workItemProgressManagerObj);

                    series.Append(workItemProgressManagerObj.WorkItemProgressValue);

                    if (index < workItemProgressManagerList.Count - 1)
                    {
                        series.Append(",");
                    }

                }
                series.Append("]");

                series.Append("},");

                series.Append("{ ");

                series.Append("\"type\":\"line\",");

                series.Append("\"name\":\"Total Progress\",");

                series.Append("\"data\": [");
                foreach (WorkItemProgressManager workItemProgressManagerObj in workItemProgressManagerList)
                {
                    int index = workItemProgressManagerList.IndexOf(workItemProgressManagerObj);

                    series.Append(workItemProgressManagerObj.WorkItemCumulativeValue);

                    if (index < workItemProgressManagerList.Count - 1)
                    {
                        series.Append(",");
                    }

                }
                series.Append("]");

                series.Append("}");

                series.Append("]");
            }
            else
            {
                series.Append("-1");
            }

            return series.ToString();
        }

        [WebMethod]
        public static void GetSubChainageMonthlyProgress(Guid subChainageGuid, DateTime startDate, DateTime endDate)
        {
            List<SubChainagePhysicalProgressManager> subChainagePhysicalProgressManagerList = new List<SubChainagePhysicalProgressManager>();
            subChainagePhysicalProgressManagerList = new SubChainagePhysicalProgressDB().GetSubChainagePhysicalProgressAll(subChainageGuid);

            List<WorkItemManager> workItemManagerList = new List<WorkItemManager>();
            workItemManagerList = new WorkItemDB().GetWorkItemsBasedOnSubChainage(subChainageGuid);

            

            //StringBuilder series = new StringBuilder();

            //if (subChainagePhysicalProgressManagerList.Count > 0)
            //{
            //    series.Append("[");

            //    foreach (SubChainagePhysicalProgressManager subChainagePhysicalProgressManagerObj in subChainagePhysicalProgressManagerList)
            //    {
            //        if (subChainagePhysicalProgressManagerObj.WorkItemProgressManager.Count > 0)
            //        {

            //        }
            //    }

            //    series.Append("]");
            //}
            //else
            //{
            //    series.Append("-1");
            //}









            Dictionary<String, Dictionary<String, String>> outerDictionary = new Dictionary<String, Dictionary<String, String>>();

            if (subChainagePhysicalProgressManagerList.Count > 0)
            {
                foreach (SubChainagePhysicalProgressManager subChainagePhysicalProgressManagerObj in subChainagePhysicalProgressManagerList)
                {
                    if (subChainagePhysicalProgressManagerObj.WorkItemProgressManager.Count > 0)
                    {
                        foreach (WorkItemProgressManager workItemProgressManagerObj in subChainagePhysicalProgressManagerObj.WorkItemProgressManager)
                        {
                            string outerDictionarykey = workItemProgressManagerObj.WorkItemManager.WorkItemName;

                            Dictionary<String, String> innerDictionary = new Dictionary<String, String>();

                            string innerDictionarykey = subChainagePhysicalProgressManagerObj.PhysicalProgressDate.ToString("dd/MM/yyyy");

                            if (innerDictionary.ContainsKey(innerDictionarykey))
                            {
                                innerDictionary[innerDictionarykey] = workItemProgressManagerObj.WorkItemProgressValue;
                            }
                            else
                            {
                                innerDictionary.Add(innerDictionarykey, workItemProgressManagerObj.WorkItemProgressValue);
                            }



                            if (outerDictionary.ContainsKey(outerDictionarykey))
                            {
                                outerDictionary[outerDictionarykey] = innerDictionary;
                            }
                            else
                            {
                                outerDictionary.Add(outerDictionarykey, innerDictionary);
                            }
                        }
                    }
                }

                //writer.WriteEndArray();
            }

            ////Simple test
            //foreach (KeyValuePair<String, Object> kvp in dictionary)
            //{
            //    JTokenWriter writer = new JTokenWriter();
            //    writer.WriteStartArray();

            //    Console.WriteLine(String.Format("Key: {0} - Value: {1}", kvp.Key, kvp.Value));
            //}

                    





// 2writer.WriteStartObject();
// 3writer.WritePropertyName("name1");
// 4writer.WriteValue("value1");
// 5writer.WritePropertyName("name2");
// 6
// 7writer.WriteValue(1);
// 8writer.WriteValue(2);
// 9;
//10writer.WriteEndObject();
            

            




            //series: [{
            //    name: 'Tokyo',
            //    data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6, 23.3, 18.3, 13.9, 9.6]
            //}, {
            //    name: 'New York',
            //    data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5, 20.1, 14.1, 8.6, 2.5]
            //}, {
            //    name: 'Berlin',
            //    data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0, 14.3, 9.0, 3.9, 1.0]
            //}, {
            //    name: 'London',
            //    data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8, 14.2, 10.3, 6.6, 4.8]
            //}]

            //return series.ToString();
        }

    }
}