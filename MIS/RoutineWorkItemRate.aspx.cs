﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class RoutineWorkItemRate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dtRoutineWorkItemRate = new DataTable();
            DataRow drRoutineWorkItemRate = null;

            dtRoutineWorkItemRate.Columns.Add(new DataColumn("Code", typeof(string)));
            dtRoutineWorkItemRate.Columns.Add(new DataColumn("Class", typeof(string)));
            dtRoutineWorkItemRate.Columns.Add(new DataColumn("Short Description", typeof(string)));            
            dtRoutineWorkItemRate.Columns.Add(new DataColumn("Units", typeof(string)));
            dtRoutineWorkItemRate.Columns.Add(new DataColumn("FY 09/10 Rate(Rs)", typeof(string)));

            drRoutineWorkItemRate = dtRoutineWorkItemRate.NewRow();

            drRoutineWorkItemRate["Code"] = "";
            drRoutineWorkItemRate["Class"] = "";
            drRoutineWorkItemRate["Short Description"] = "";           
            drRoutineWorkItemRate["Units"] = "";
            drRoutineWorkItemRate["FY 09/10 Rate(Rs)"] = "";

            dtRoutineWorkItemRate.Rows.Add(drRoutineWorkItemRate);

            GridView1.DataSource = dtRoutineWorkItemRate;
            GridView1.DataBind();

        }
    }
}