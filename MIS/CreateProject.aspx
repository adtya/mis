﻿<%@ Page Title="Create Project" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CreateProject.aspx.cs" Inherits="MIS.CreateProject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="CustomStyles/create-project.css" type="text/css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container" style="margin-top:5%;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>Create New Project</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" action="#" method="post" runat="server">
                            <fieldset>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <h3>Enter the details for the new project:</h3>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="districtList" id="districtListLabel" class="control-label">Name of District</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <asp:Literal ID="ltDistrictList" runat="server"></asp:Literal>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="divisonList" id="divisonListLabel" class="control-label">Name of Divison</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <asp:Literal ID="ltDivisonList" runat="server"></asp:Literal>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="projectName" id="projectNameLabel" class="control-label">Name of Project</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="projectName" class="form-control" id="projectName" placeholder="Project Name" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="chainageOfProject" id="chainageOfProjectLabel" class="control-label">Chainage of Project</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="chainageOfProject" class="form-control" id="chainageOfProject" placeholder="Chainage of Project" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="dateOfStart" id="dateOfStartLabel" class="control-label">Date Of Start</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="dateOfStart" class="form-control" id="dateOfStart" placeholder="Date Of Start" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="expectedDateOfCompletion" id="expectedDateOfCompletionLabel" class="control-label">Date Of Completion (Expected)</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="expectedDateOfCompletion" class="form-control" id="expectedDateOfCompletion" placeholder="Date Of Completion (Expected)" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="actualDateOfCompletion" id="actualDateOfCompletionLabel" class="control-label">Date Of Completion (Actual)</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="actualDateOfCompletion" class="form-control" id="actualDateOfCompletion" placeholder="Date Of Completion (Actual)" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="projectValue" id="projectValueLabel" class="control-label">Project Value</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="projectValue" class="form-control" id="projectValue" placeholder="Project Value" />
                                            </div>
                                        </div>

                                        <hr class="separator" />

                                        <div id="officersEngagedListDisplay" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden"></div>

                                        <div class="form-group">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <label for="officersEngaged" id="officersEngagedLabel" class="control-label" style="margin-bottom:1%;">Name and designation of the other officer engaged for FREMAA works</label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <input type="text" name="officersEngaged" class="form-control" id="officersEngaged" placeholder="Name and designation of the other officer engaged for FREMAA works" />
                                            </div>
                                            <div class="col-xs-4 col-xs-offset-1 col-sm-4 col-sm-offset-1 col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1">
                                                <button type="button" name="addOfficersEngagedBtn" class="btn btn-block btn-primary" id="addOfficersEngagedBtn">Add</button>
                                            </div>
                                        </div>

                                        <hr class="separator" />

                                        <div id="pmcSiteEngineersListDisplay" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden"></div>

                                        <div class="form-group">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <label for="pmcSiteEngineers" id="pmcSiteEngineersLabel" class="control-label" style="margin-bottom:1%;">Name and designation of the PMC Site Engineer</label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <input type="text" name="pmcSiteEngineers" class="form-control" id="pmcSiteEngineers" placeholder="Name and designation of the PMC Site Engineer" />
                                            </div>
                                            <div class="col-xs-4 col-xs-offset-1 col-sm-4 col-sm-offset-1 col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1">
                                                <button type="button" name="addPmcSiteEngineersBtn" class="btn btn-block btn-primary" id="addPmcSiteEngineersBtn">Add</button>
                                            </div>
                                        </div>

                                        <hr class="separator" />

                                        <div id="subChainagesListDisplay" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden"></div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="subChainages" id="subChainagesLabel" class="control-label" style="margin-bottom:1%;">Sub Chainages</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="subChainages" class="form-control" id="subChainages" placeholder="Sub Chainages" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="associatedSIO" id="associatedSIOLabel" class="control-label" style="margin-bottom:1%;">Associated SIO</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="associatedSIO" class="form-control" id="associatedSIO" placeholder="Associated SIO" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="joiningDateOfSio" id="joiningDateOfSioLabel" class="control-label">Date of Joining of SIO</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="joiningDateOfSio" class="form-control" id="joiningDateOfSio" placeholder="Date of Joining of SIO" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 pull-right">
                                                <button type="button" name="addSubChainagesBtn" class="btn btn-block btn-primary" id="addSubChainagesBtn">Add</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <hr />

                                <div class="row">
                                    <div class="col-xs-4 col-xs-offset-4 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
                                        <button type="button" name="createProjectBtn" class="btn btn-block btn-primary" id="createProjectBtn" onclick="createProject();">Create Project</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="pull-left">
                                    <a class="btn btn-primary" href="DBAHome.aspx" onclick="">Back to Home</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
    <script src="CustomScripts/create-project.js" type="text/javascript"></script>
</asp:Content>