
(function ($) {

    function isArray(obj) {
        return nativeIsArray ? nativeIsArray(obj) : toString.call(obj) === '[object Array]';
    }

    $.fn.formatMoney = function (number, symbol, precision, thousand, decimal, format) {
	    settings = $.extend({
	        symbol: $.culture.numberFormat.currency.symbol,
	        decimal: $.culture.numberFormat['.'],
	        precision: $.culture.numberFormat.decimals,
	        thousands: $.culture.numberFormat[','],
	        showSymbol: true
	    }, settings);

	    return function (number, symbol, precision, thousand, decimal, format) {
	        // Resursively format arrays:
	        if (isArray(number)) {
	            return map(number, function (val) {
	                return formatMoney(val, symbol, precision, thousand, decimal, format);
	            });
	        }

	        // Clean up number:
	        number = unformat(number);

	        // Build options object from second param (if object) or all params, extending defaults:
	        var opts = defaults(
                    (isObject(symbol) ? symbol : {
                        symbol: symbol,
                        precision: precision,
                        thousand: thousand,
                        decimal: decimal,
                        format: format
                    }),
                    lib.settings.currency
                ),

                // Check format (returns object with pos, neg and zero):
                formats = checkCurrencyFormat(opts.format),

                // Choose which format to use for this value:
                useFormat = number > 0 ? formats.pos : number < 0 ? formats.neg : formats.zero;

	        // Return with currency symbol added:
	        return useFormat.replace('%s', opts.symbol).replace('%v', $.format(parseFloat(number)));//formatNumber(Math.abs(number), checkPrecision(opts.precision), opts.thousand, opts.decimal));
	    };
	}

	$.fn.unmaskMoney=function() {
		return this.trigger("unmaskMoney");
	};

})(jQuery);
