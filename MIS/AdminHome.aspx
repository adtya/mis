﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminHome.aspx.cs" Inherits="MIS.AdminHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    <link href="Content/font-awesome.min.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
    <script src="Scripts/bootbox.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.validate.min.js" type="text/javascript"></script>
    <script src="CustomScripts/my-validation-methods.js" type="text/javascript"></script>
    <script src="Scripts/jquery-validate.bootstrap-tooltip.flickerfix.min.js" type="text/javascript"></script>    
    <script src="CustomScripts/create_user.js" type="text/javascript"></script>
    <script src="CustomScripts/delete-user.js" type="text/javascript"></script>    
    <%--<script src="CustomScripts/admin-home.js" type="text/javascript"></script>--%>     
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div id="popupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header" style="display:none"></div>
                <%--<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h1 class="text-center">What's My Password?</h1>
                </div>--%>

                <div class="modal-body">
                    <%--<div class="col-md-12">--%>
                    <div class="container-fluid col-md-12">
                        <div class="row">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="text-center">
                                                <img src="Images/createuser.jpg" class="login" height="70" />
                                                <h2 class="text-center">Create User</h2>
                                                
                                                <div class="panel-body">
                                                    <form id="popupForm" class="form form-horizontal" method="post"><!--start form--><!--add form action as needed-->
                                                        <fieldset>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user color-blue"></i></span>
                                                                    <!--User Name-->
                                                                    <input id="userFirstName" name="userFirstName" placeholder="First Name" class="form-control" />
                                                                    <span class="input-group-btn" style="width:0px;"></span>
                                                                    <input id="userMiddleName" name="userMiddleName" placeholder="Middle Name" class="form-control" />
                                                                    <span class="input-group-btn" style="width:0px;"></span>
                                                                    <input id="userLastName" name="userLastName" placeholder="Last Name" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                                                    <!--EMAIL ADDRESS-->
                                                                    <input id="userEmailId" name="userEmailId" placeholder="Email Address" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-users color-blue"></i></span>
                                                                    <asp:Literal ID="ltRoleList" runat="server"></asp:Literal>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <input id="createUserBtn" class="btn btn-lg btn-primary btn-block" value="Create User" type="submit" />
                                                            </div>
                                                        </fieldset>
                                                    </form><!--/end form-->
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <%--</div>--%>
            </div>

            <div class="modal-footer">
                <div class="col-md-12">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="popupModalDelete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header" style="display:none"></div>
                <%--<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h1 class="text-center">What's My Password?</h1>
                </div>--%>

                <div class="modal-body">
                    <%--<div class="col-md-12">--%>
                    <div class="container-fluid col-md-12">
                        <div class="row">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="text-center">
                                                <img src="Images/createuser.jpg" class="login" height="70" />
                                                <h2 class="text-center">Delete User</h2>
                                                
                                                <div class="panel-body">
                                                    <form id="popupFormDeleteUser" class="form form-horizontal" method="post"><!--start form--><!--add form action as needed-->
                                                        <fieldset>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <asp:Literal ID="ltUserList" runat="server">                                                                       
                                                                    </asp:Literal>                                                                     
                                                                    <%--<table id="tableDelete" border="1" style="width:450px">
                                                                           <tr>
                                                                              <td style="text-align:center" width="50px">Sr No.</td>                                                                           
                                                                              <td style="text-align:center" width="120px">Name</td>                                                                          
                                                                              <td style="text-align:center" width="150px">Email ID</td>                                                                           
                                                                              <td style="text-align:center" width="80px">Role Name</td>                                                                           
                                                                              <td><input type="checkbox" name="delete" width="40px"/></td>
                                                                           </tr>
                                                                     </table>--%>
                                                                </div>
                                                            </div>                                                                                                                                                                           
                                                        </fieldset>
                                                    </form><!--/end form-->
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <%--</div>--%>
            </div>

            <div class="modal-footer">
                <div class="col-md-12">
                     <button class="btn" data-dismiss="modal" aria-hidden="true" id="deletebutton" onclick="deleteUserConfirmation();">Delete</button>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>

        </div>
    </div>
</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server"></asp:Content>
