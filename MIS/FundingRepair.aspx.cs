﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class FundingRepair : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dtFundingRepair = new DataTable();
            DataRow drFundingRepair = null;

            dtFundingRepair.Columns.Add(new DataColumn("Fiscal Year", typeof(string)));
            dtFundingRepair.Columns.Add(new DataColumn("Available Funds(Rs)", typeof(string)));
            dtFundingRepair.Columns.Add(new DataColumn("Used Funds(Rs)", typeof(string)));


            drFundingRepair = dtFundingRepair.NewRow();

            drFundingRepair["Fiscal Year"] = "";
            drFundingRepair["Available Funds(Rs)"] = "";
            drFundingRepair["Used Funds(Rs)"] = "";


            dtFundingRepair.Rows.Add(drFundingRepair);

            GridView1.DataSource = dtFundingRepair;
            GridView1.DataBind();

        }
    }
}