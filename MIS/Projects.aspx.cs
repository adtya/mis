﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class Projects : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "Admin")
                {
                    if (!Page.IsPostBack)
                    {
                        DrawProjectList();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                    //Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("DBALogin.aspx");
            }
        }

        private void DrawProjectList()
        {
            List<ProjectManager> projectList = new List<ProjectManager>();
            projectList = new ProjectDB().GetProjectNameWithStatus();

            string projectTable = "<div id=" + "\"table-container\"" + "style=" + "\"padding:1%;\">";
            projectTable += "<table id=\"trainingList\" class=\"table table-striped table-bordered table-hover table-responsive\" cellspacing=\"0\" width=\"100%\">";

            projectTable += "<thead>";
            projectTable += "<tr>";
            projectTable += "<th>";
            projectTable += "Project Name";
            projectTable += "</th>";
            projectTable += "<th>";
            projectTable += "Status";
            projectTable += "</th>";
            projectTable += "<th>";
            projectTable += "Operations";
            projectTable += "</th>";
            projectTable += "</tr>";
            projectTable += "</thead>";

            projectTable += "<tbody>";
            foreach (ProjectManager projectObj in projectList)
            {
                projectTable += "<tr>";
                projectTable += "<td>" + projectObj.ProjectName + "</td>";
                projectTable += "<td>" + (ProjectStatusEnum)Enum.Parse(typeof(ProjectStatusEnum), projectObj.ProjectStatus.ToString()) + "</td>";
                projectTable += "<td>" + "<a href=\"http://www.google.com\" target=\"_parent\"><button>View</button></a>" + "<a href=\"http://www.google.com\" target=\"_parent\"><button>Edit</button></a>" + "<a href=\"http://www.google.com\" target=\"_parent\"><button>Delete</button></a>" + "</td>";
                projectTable += "</tr>";
            }
            projectTable += "</tbody>";

            projectsList.Text = projectTable;
        }
    }
}