﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class AssetFullDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dtAssetFull = new DataTable();
            DataRow drAssetFull = null;

            dtAssetFull.Columns.Add(new DataColumn("Asset Code", typeof(string)));
            dtAssetFull.Columns.Add(new DataColumn("Key Asset", typeof(string)));
            dtAssetFull.Columns.Add(new DataColumn("Asset Name", typeof(string)));
            dtAssetFull.Columns.Add(new DataColumn("UTM U/s E", typeof(string)));
            dtAssetFull.Columns.Add(new DataColumn("UTM U/s N", typeof(string)));
            dtAssetFull.Columns.Add(new DataColumn("UTM D/s E", typeof(string)));
            dtAssetFull.Columns.Add(new DataColumn("UTM D/s N", typeof(string)));
            dtAssetFull.Columns.Add(new DataColumn("AMTD", typeof(string)));
            dtAssetFull.Columns.Add(new DataColumn("Chainage(m) U/s", typeof(string)));
            dtAssetFull.Columns.Add(new DataColumn("Chainage(m) D/s", typeof(string)));
            dtAssetFull.Columns.Add(new DataColumn("Cost (Rs Lakh)", typeof(string)));
            dtAssetFull.Columns.Add(new DataColumn("Completion Date", typeof(string)));

            drAssetFull = dtAssetFull.NewRow();

            drAssetFull["Asset Code"] = "";
            drAssetFull["Key Asset"] = "";
            drAssetFull["Asset Name"] = "";
            drAssetFull["UTM U/s E"] = "";
            drAssetFull["UTM U/s N"] = "";
            drAssetFull["UTM D/s E"] = "";
            drAssetFull["UTM D/s N"] = "";
            drAssetFull["AMTD"] = "";
            drAssetFull["Chainage(m) U/s"] = "";
            drAssetFull["Chainage(m) D/s"] = "";
            drAssetFull["Cost (Rs Lakh)"] = "";
            drAssetFull["Completion Date"] = "";

            dtAssetFull.Rows.Add(drAssetFull);

            GridView1.DataSource = dtAssetFull;
            GridView1.DataBind();
        }
    }
}