﻿using MIS.App_Code;
using NodaMoney;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class FieldSurvey : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "DBA")
                {
                    if (!Page.IsPostBack)
                    {
                        DisplaySystemVariables();
                        DisplayAssetCodeForFieldSurvey();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
            }
        }

        private void DisplaySystemVariables()
        {
            SystemVariablesManager systemVariablesManagerObj = new SystemVariablesManager();
            systemVariablesManagerObj = new SystemVariablesDB().GetSystemVariablesGuid();

            StringBuilder htmlFiscalYear = new StringBuilder();
            StringBuilder htmlCircleGuid = new StringBuilder();
            StringBuilder htmlCircleCode = new StringBuilder();

            htmlFiscalYear.Append("<input name=\"fiscalYear\" class=\"form-control\" id=\"fiscalYear\" readonly=\"readonly\" value= \"" + systemVariablesManagerObj.FiscalYear + "\" >");
            htmlCircleGuid.Append("<input name=\"circleGuid\" class=\"form-control\" id=\"circleGuid\" readonly=\"readonly\" value= \"" + systemVariablesManagerObj.CircleManager.CircleGuid.ToString() + "\" style=\"display: none;\" >");
            htmlCircleCode.Append("<input name=\"circleCode\" class=\"form-control\" id=\"circleCode\" readonly=\"readonly\" value= \"" + systemVariablesManagerObj.CircleManager.CircleCode.ToString() + "\" >");

            ltFiscalYear.Text = htmlFiscalYear.ToString();
            ltCircleGuid.Text = htmlCircleGuid.ToString();
            ltCircleCode.Text = htmlCircleCode.ToString();
          
        }
        
        private void DisplayAssetCodeForFieldSurvey()
        {
            List<AssetManager> assetCodeManagerList = new List<AssetManager>();
            assetCodeManagerList = new AssetDB().GetAssetCodeForFieldSurvey();

            StringBuilder htmlAssetCodeList = new StringBuilder();
            
            htmlAssetCodeList.Append("<select class=\"form-control multiselect\" name=\"assetCodeList\" id=\"assetCodeList\" onfocus=\"dbaFocusFunction(this.id)\" onblur=\"dbaBlurFunction(this.id)\">");
            htmlAssetCodeList.Append("<option value=\"\">" + "--Select Asset Code--" + "</option>");

            if (assetCodeManagerList.Count > 0)
            {
                foreach (AssetManager assetCodeObj in assetCodeManagerList)
                {
                    htmlAssetCodeList.Append("<option value=\"" + assetCodeObj.AssetGuid.ToString() + "\">" + assetCodeObj.AssetCode.ToString() + "</option>");
                }
            }
            htmlAssetCodeList.Append("</select>");

            ltAssetCodeForFieldSurvey.Text = htmlAssetCodeList.ToString();

            
        }

        [WebMethod]
        public static AssetManager GetAssetCodeDataForFieldSurvey(Guid assetGuid)
        {
            AssetManager assetCodeDataObj = new AssetManager();
            assetCodeDataObj = new AssetDB().GetAssetCodeDataForRoutineAnalysis(assetGuid);

            return assetCodeDataObj;
        }

        [WebMethod]
        public static List<FieldSurveyManager> GetFieldSurveyData(Guid assetGuid,Guid circleGuid, string fiscalYear)
        {
            List<FieldSurveyManager> fieldSurveyManagerList = new List<FieldSurveyManager>();
            fieldSurveyManagerList = new FieldSurveyDB().GetFieldSurveyData(assetGuid,circleGuid,fiscalYear);
            return fieldSurveyManagerList;
        }

        [WebMethod]
        public static List<RoutineWorkItemManager> GetRoutineWorkItemsForFieldSurvey()
        {
            List<RoutineWorkItemManager> routineWorkItemManagerList = new List<RoutineWorkItemManager>();
            routineWorkItemManagerList = new RoutineWorkItemDB().GetRoutineWorkItemsForFieldSurvey();

            return routineWorkItemManagerList;
        }

        [WebMethod]
        public static RoutineWorkItemManager GetRoutineWorkItemDataForFieldSurvey(Guid routineWorkItemGuid)
        {
            RoutineWorkItemManager routineWorkItemDataObj = new RoutineWorkItemManager();
            routineWorkItemDataObj = new RoutineWorkItemDB().GetRoutineWorkItemDataForFieldSurvey(routineWorkItemGuid);

            return routineWorkItemDataObj;
        }

        [WebMethod]
        public static RoutineWorkItemRateManager GetRoutineWorkRateDataForFieldSurvey(Guid routineWorkItemGuid)
        {
            RoutineWorkItemRateManager routineWorkItemRateDataObj = new RoutineWorkItemRateManager();
            routineWorkItemRateDataObj = new RoutineWorkItemRateDB().GetRoutineWorkItemRateForFieldSurvey(routineWorkItemGuid);

            return routineWorkItemRateDataObj;
        }

        [WebMethod]
        public static Guid SaveFieldSurveyData(FieldSurveyManager fieldSurveyManager)
        {
            fieldSurveyManager.FieldSurveyGuid = Guid.NewGuid();
            int result = new FieldSurveyDB().SaveFieldSurveyData(fieldSurveyManager);
            if (result == 1)
            {
                return fieldSurveyManager.FieldSurveyGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public static int UpdateFieldSurveyData(string fieldSurveyGuid, string earthVolumeQuantity, string fieldSurveyWorkItemCost)
        {
            return new FieldSurveyDB().UpdateFieldSurveyData(new Guid(fieldSurveyGuid), Convert.ToDouble(earthVolumeQuantity), Money.Parse(fieldSurveyWorkItemCost));
        }

        [WebMethod]
        public static int DeleteFieldSurveyData(Guid fieldSurveyGuid)
        {
            return new FieldSurveyDB().DeleteFieldSurveyData(fieldSurveyGuid);
        }
    }
}