﻿<%@ Page Title="Edit Scheme" MasterPageFile="~/Main.Master" Language="C#" AutoEventWireup="true" CodeBehind="EditScheme.aspx.cs" Inherits="MIS.EditScheme" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="CustomStyles/ams.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
    <script src="Scripts/bootbox.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.validate.min.js" type="text/javascript"></script>
    <script src="CustomScripts/my-validation-methods.js" type="text/javascript"></script>
    <script src="Scripts/jquery-validate.bootstrap-tooltip.flickerfix.min.js" type="text/javascript"></script>     
    <script src="CustomScripts/edit-scheme.js" type="text/javascript"></script>   
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <%--<div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Edit Scheme</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <label for="circleList" id="circleListForSchemeLabel" class="control-label">Select Circle</label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <asp:Literal ID="ltcircleForSchemeList" runat="server"></asp:Literal>
                                            </div>                                                                
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <label for="divisionList" id="divisionListLabel" class="control-label">Select Division</label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">                                                
                                                <input id="divisionName" name="divisionName" placeholder="Division Name" class="form-control" />
                                                <span class="input-group-btn" style="width:0px;"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="schemeName" id="schemeNameLabel" class="control-label">Scheme Name</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input id="schemeName" name="schemeName" placeholder="Scheme Name" class="form-control" />
                                                <span class="input-group-btn" style="width:0px;"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="schemeCode" id="schemeCodeLabel" class="control-label">Scheme Code</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input id="schemeCode" name="schemeCode" placeholder="Scheme Code" class="form-control" />
                                                 <span class="input-group-btn" style="width:0px;"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="schemeType" id="schemeTypeLabel" class="control-label">Scheme Type</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input id="schemeType" name="schemeType" placeholder="Scheme Type" class="form-control" />
                                                 <span class="input-group-btn" style="width:0px;"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="utmEast1" id="utmEast1Label" class="control-label">UTM East1</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input id="utmEast1" name="utmEast1" placeholder="UTM East1" class="form-control" />
                                                 <span class="input-group-btn" style="width:0px;"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="utmEast2" id="utmEast2Label" class="control-label">UTM East2</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input id="utmEast2" name="utmEast2" placeholder="UTM East2" class="form-control" />
                                                 <span class="input-group-btn" style="width:0px;"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="utmNorth1" id="utmNorth1Label" class="control-label">UTM North1</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input id="utmNorth1" name="utmNorth1" placeholder="UTM North1" class="form-control" />
                                                 <span class="input-group-btn" style="width:0px;"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="utmNorth2" id="utmNorth2label" class="control-label">UTM North2</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input id="utmNorth2" name="utmNorth2" placeholder="UTM North2" class="form-control" />
                                                 <span class="input-group-btn" style="width:0px;"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="drawingID" id="drawingIDlabel" class="control-label">Drawing ID</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input id="drawingID" name="drawingID" placeholder="Drawing ID" class="form-control" />
                                                 <span class="input-group-btn" style="width:0px;"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="area" id="arealabel" class="control-label">Area</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input id="area" name="area" placeholder="Area" class="form-control" />
                                                 <span class="input-group-btn" style="width:0px;"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                                  

                            </fieldset>
                        </form>
                    </div>

                    <div class="panel-footer clearfix">
                        <div class="btn pull-right">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Update</button>
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>                        
                    </div>
                </div>

             </div>
       
       </div>
    </div>--%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">

</asp:Content>
