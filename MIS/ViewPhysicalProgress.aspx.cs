﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class ViewPhysicalProgress : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "SIO")
                {
                    if (!Page.IsPostBack)
                    {
                        DisplaySubChainageDetails();
                        //DisplayRoleList();
                        //DisplayUserData();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;
                    System.Web.HttpContext.Current.Session["sessionSubChainageGuid"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                    //Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("DBALogin.aspx");
            }
        }

        private void DisplaySubChainageDetails()
        {
            SubChainageManager subChainageObj = new SubChainageManager();
            subChainageObj = new SubChainageDB().GetSubChainageDetails(new Guid(System.Web.HttpContext.Current.Session["sessionSubChainageGuid"].ToString()));

            ltDistrictName.Text = subChainageObj.ProjectManager.DistrictManager.DistrictName;
            ltDivisionName.Text = subChainageObj.ProjectManager.DivisionManager.DivisionName;
            ltProjectName.Text = subChainageObj.ProjectManager.ProjectName;
            ltSubProjectName.Text = subChainageObj.ProjectManager.SubProjectName;
            ltProjectChainage.Text = subChainageObj.ProjectManager.ProjectChainage;
            ltProjectStartingDate.Text = subChainageObj.ProjectManager.ProjectStartDate.ToString("dd/MM/yyyy");
            ltExpectedCompletionDateOfProject.Text = subChainageObj.ProjectManager.ProjectExpectedCompletionDate.ToString("dd/MM/yyyy");
            ltProjectValue.Text = subChainageObj.ProjectManager.ProjectValue.ToString();

            if (subChainageObj.ProjectManager.OfficersEngaged.Count > 0)
            {
                StringBuilder freemaaOfficers = new StringBuilder();
                freemaaOfficers.Append("<div class=\"form-group\">");

                //foreach (string officerName in subChainageObj.ProjectManager.OfficersEngaged)
                //{
                //    var index = subChainageObj.ProjectManager.OfficersEngaged.IndexOf(officerName);

                //    freemaaOfficers.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
                //    freemaaOfficers.Append(officerName);
                //    freemaaOfficers.Append("</div>");
                //    freemaaOfficers.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
                //    freemaaOfficers.Append("<label class=\"radio-inline\">");
                //    freemaaOfficers.Append("<input type=\"radio\" name=\"officerVisited" + index + "\" value=\"yes\">Visited</input>");
                //    freemaaOfficers.Append("</label>");
                //    freemaaOfficers.Append("<label class=\"radio-inline\">");
                //    freemaaOfficers.Append("<input type=\"radio\" name=\"officerVisited" + index + "\" value=\"no\" checked=\"checked\">Not Visited</input>");
                //    freemaaOfficers.Append("</label>");
                //    freemaaOfficers.Append("</div>");
                //}

                freemaaOfficers.Append("</div>");

                ltFremaaOfficers.Text = freemaaOfficers.ToString();
            }
            else
            {
                ltFremaaOfficers.Text = "";
            }



            if (subChainageObj.ProjectManager.PmcSiteEngineers.Count > 0)
            {
                StringBuilder siteEngineers = new StringBuilder();
                siteEngineers.Append("<div class=\"form-group\">");

                //foreach (string siteEngineerName in subChainageObj.ProjectManager.PmcSiteEngineers)
                //{
                //    var index = subChainageObj.ProjectManager.PmcSiteEngineers.IndexOf(siteEngineerName);

                //    siteEngineers.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
                //    siteEngineers.Append(siteEngineerName);
                //    siteEngineers.Append("</div>");
                //    siteEngineers.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">");
                //    siteEngineers.Append("<label class=\"radio-inline\">");
                //    siteEngineers.Append("<input type=\"radio\" name=\"siteEngineers" + index + "\" value=\"yes\" />Visited");
                //    siteEngineers.Append("</label>");
                //    siteEngineers.Append("<label class=\"radio-inline\">");
                //    siteEngineers.Append("<input type=\"radio\" name=\"siteEngineers" + index + "\" value=\"no\" checked=\"checked\" />Not Visited");
                //    siteEngineers.Append("</label>");
                //    siteEngineers.Append("</div>");
                //}

                siteEngineers.Append("</div>");

                ltPmcSiteEngineers.Text = siteEngineers.ToString();
            }
            else
            {
                ltPmcSiteEngineers.Text = "";
            }



            ltSubChainageName.Text = subChainageObj.SubChainageName;
            ltSubChainageValue.Text = subChainageObj.SubChainageValue.ToString();
            ltAdministrativeApprovalReference.Text = subChainageObj.AdministrativeApprovalReference.ToString();
            ltTechnicalSanctionReference.Text = subChainageObj.TechnicalSanctionReference.ToString();
            ltContractorName.Text = subChainageObj.ContractorName.ToString();
            ltWorkOrderReference.Text = subChainageObj.WorkOrderReference.ToString();
            ltSubChainageStartingDate.Text = subChainageObj.SubChainageStartingDate.ToString("dd/MM/yyyy");
            ltSubChainageExpectedCompletionDate.Text = subChainageObj.SubChainageExpectedCompletionDate.ToString("dd/MM/yyyy");
            ltWorkType.Text = subChainageObj.TypeOfWorks.ToString();




            if (subChainageObj.WorkItemManager.Count > 0)
            {
                StringBuilder workItems = new StringBuilder();
                workItems.Append("<div class=\"form-group\">");
                workItems.Append("<div id=\"table-container\" style=\"padding:1%;\">");

                if (subChainageObj.WorkItemManager.Count > 0)
                {
                    foreach (WorkItemManager workItemObj in subChainageObj.WorkItemManager)
                    {
                        int workItemIndex = subChainageObj.WorkItemManager.IndexOf(workItemObj);

                        workItems.Append("<p>" + BasicUtility.IndexToColumn(workItemIndex + 1) + ") " + workItemObj.WorkItemName + "</p>");

                        if (workItemObj.WorkItemSubHeadManager.Count > 0)
                        {
                            workItems.Append("<div class=\"table-responsive\">");
                            workItems.Append("<table id=\"workItemProgressList\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

                            workItems.Append("<tbody>");

                            foreach (WorkItemSubHeadManager workItemSubHeadObj in workItemObj.WorkItemSubHeadManager)
                            {
                                int workItemSubHeadIndex = workItemObj.WorkItemSubHeadManager.IndexOf(workItemSubHeadObj);

                                workItems.Append("<tr>");

                                workItems.Append("<td>" + BasicUtility.ToRomanLower(workItemSubHeadIndex + 1) + "</td>");
                                workItems.Append("<td>" + workItemSubHeadObj.WorkItemSubHeadName + "</td>");
                                workItems.Append("<td>" + "" + "</td>");

                                workItems.Append("</tr>");
                            }

                            workItems.Append("</tbody>");

                            workItems.Append("</table>");
                            workItems.Append("</div>");
                        }
                    }
                }
                else
                {
                    workItems.Append("<p>" + "No Data" + "</p>");
                }

                workItems.Append("</div>");
                workItems.Append("</div>");

                ltWorkItemsProgress.Text = workItems.ToString();
            }
        }
    }
}