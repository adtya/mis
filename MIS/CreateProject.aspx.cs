﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MIS.App_Code;
using System.Text;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.IO;

namespace MIS
{
    public partial class CreateProject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<DistrictManager> districtsList = new List<DistrictManager>();

            districtsList = new DistrictDB().GetDistricts();

            StringBuilder htmlDistrictList = new StringBuilder();

            if (districtsList.Count > 0)
            {
                htmlDistrictList.Append("<select class=\"form-control\" name=\"districtList\" id=\"districtList\">");

                foreach (DistrictManager districtObj in districtsList)
                {
                    htmlDistrictList.Append("<option value=\"" + districtObj.DistrictGuid.ToString() + "\">" + districtObj.DistrictName.ToString() + "</option>");
                }

                htmlDistrictList.Append("</select>");

            }

            ltDistrictList.Text = htmlDistrictList.ToString();






            List<DivisonManager> divisonList = new List<DivisonManager>();

            divisonList = new DivisonDB().GetDivisons();

            StringBuilder htmlDivisonList = new StringBuilder();

            htmlDivisonList.Append("<select class=\"form-control\" name=\"divisonList\" id=\"divisonList\">");
            if (divisonList.Count > 0)
            {
                foreach (DivisonManager divisonObj in divisonList)
                {
                    htmlDivisonList.Append("<option value=\"" + divisonObj.DivisonGuid.ToString() + "\">" + divisonObj.DivisonName.ToString() + "</option>");
                }
            }
            htmlDivisonList.Append("</select>");

            ltDivisonList.Text = htmlDivisonList.ToString();

            //ltDistrictList.Text = htmlDistrictList.ToString();
        }

        [WebMethod]
        public static void CreateNewProject(string districtGuid, string divisonGuid, string projectName, string projectChainage, string dateOfStart, string expectedDateOfCompletion, string actualDateOfCompletion,
            string projectValue, string[] arrOfficersEngaged, string[] arrPmcSiteEngineers, List<SubChainages> arrSubChainages)
        {
            new ProjectDB().CreateProject(new Guid(districtGuid), new Guid(), projectName, projectChainage, new CreateProject().CalculateDateTime(dateOfStart), new CreateProject().CalculateDateTime(expectedDateOfCompletion), new CreateProject().CalculateDateTime(actualDateOfCompletion), projectValue, arrOfficersEngaged, arrPmcSiteEngineers);
        }

        public class SubChainages
        {
            public string chainageName { get; set; }
            public string sioName { get; set; }
        }

        private DateTime CalculateDateTime(string date)
        {
            char[] delimiter = new char[] { '/' };

            string[] dateParts = date.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

            string day = dateParts[0];
            string month = dateParts[1];
            string year = dateParts[2];

            DateTime dt = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));

            return dt;
        }

    }
}