﻿<%@ Page Title="Enter Progress" Language="C#" MasterPageFile="~/SIO.Master" AutoEventWireup="true" CodeBehind="EnterProgress.aspx.cs" Inherits="MIS.EnterProgress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    <link href="Content/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="Content/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    
    <link href="CustomStyles/enter-progress.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
    <script src="Scripts/bootbox.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.validate.min.js" type="text/javascript"></script>
    <script src="Scripts/additional-methods.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.glob.js" type="text/javascript"></script>
    <script src="Scripts/globinfo/jQuery.glob.en-IN.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.GlobalMoneyInput2.js" type="text/javascript"></script>
    <script src="Scripts/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="Scripts/Datejs/build/date.js" type="text/javascript"></script>
    <script src="Scripts/date.format.js" type="text/javascript"></script>
    <script src="Scripts/jquery.Guid.js" type="text/javascript"></script>
    <script src="Scripts/accounting.js" type="text/javascript"></script>
    
    <script src="CustomScripts/my-validation-methods.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div class="container"><%-- style="margin-top:5%;">--%>

        <div class="row">
            <div class="col-xs-6 col-xs-offset-3 col-sm-12 col-sm-offset-3 col-md-12 col-md-offset-3 col-lg-12 col-lg-offset-3">
                <a id="btnBack" class="btn btn-primary" href="SIOProjects.aspx">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>Enter Sub-Chainage Progress</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" name="subChainageProgressForm" id="subChainageProgressForm" class="form form-horizontal" action="#" method="post" runat="server">
                            <fieldset>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="box">

                                            <div class="well well-sm">
                                                <div class="panel panel-default">

                                                    <div class="panel-body">

                                                        <fieldset class="scheduler-border">
                                                            <legend class="scheduler-border">Sub Chainage Info</legend>

                                                            <div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="subChainageName" id="subChainageNameLabel" class="control-label pull-right">SubChainage Name:</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <asp:Literal ID="ltSubChainageName" runat="server"></asp:Literal>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="projectName" id="projectNameLabel" class="control-label pull-right">Project Name:</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <asp:Literal ID="ltProjectName" runat="server"></asp:Literal>
                                                                </div>
                                                            </div>

                                                        </fieldset>
                                                        
                                                        <fieldset class="scheduler-border">
                                                            <legend class="scheduler-border">Sub Chainage Info</legend>

                                                            <%--<div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="progressYear" id="progressYearLabel" class="control-label pull-right">Progress Year</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <div id="dpProgressYear" class="input-group date">
                                                                        <input type="text" name="progressYear" class="form-control" id="progressYear" placeholder="Year" />
                                                                        <span class="input-group-addon form-control-static">
                                                                            <i class="fa fa-calendar fa-fw"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>--%>

                                                            <div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="progressMonth" id="progressMonthLabel" class="control-label pull-right">Progress Month</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <div id="dpProgressMonth" class="input-group date">
                                                                        <input type="text" name="progressMonth" class="form-control" id="progressMonth" placeholder="Month" />
                                                                        <span class="input-group-addon form-control-static">
                                                                            <i class="fa fa-calendar fa-fw"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </fieldset>

                                                        <fieldset class="scheduler-border">
                                                            <legend class="scheduler-border">Fremaa Officers</legend>

                                                            <asp:Literal ID="ltFremaaOfficers" runat="server"></asp:Literal>
                                                            
                                                        </fieldset>

                                                        <fieldset class="scheduler-border">
                                                            <legend class="scheduler-border">Pmc Site Engineers</legend>

                                                            <asp:Literal ID="ltPmcSiteEngineers" runat="server"></asp:Literal>
                                                            
                                                        </fieldset>

                                                        <fieldset class="scheduler-border">
                                                            <legend class="scheduler-border">Monthly Physical Progress</legend>

                                                            <div class="form-group">
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                    <label for="workItemWiseProgress" id="workItemWiseProgressLabel" class="control-label">Progress of Work Item Wise</label>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                                                            </div>

                                                            <asp:Literal ID="ltWorkItemsProgress" runat="server"></asp:Literal>

                                                        </fieldset>

                                                        <%--<fieldset class="scheduler-border">
                                                            <legend class="scheduler-border">Monthly Financial Progress</legend>

                                                            <div class="form-group">
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                    <label for="workItemWiseProgress" id="a" class="control-label">Progress of Work Item Wise</label>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                                                            </div>

                                                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>

                                                        </fieldset>--%>

                                                        <div class="form-group">
                                                            <div class="col-sm-10 col-md-6 col-md-offset-3">
                                                                <button type="button" class="action submit btn btn-success" id="btnSubmitProgress">Submit Progress</button>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>

    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
    <script src="CustomScripts/enter-progress.js" type="text/javascript"></script>
</asp:Content>