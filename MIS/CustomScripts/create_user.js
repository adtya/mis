﻿$(document).ready(function () {

    $('#popupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {       
            userFirstName: {
                required: true,
                noSpace: true
            },
            userLastName: {
                required: true,
                noSpace: true
            },
            userEmailId: {
                required: true,
                //email: true
                email1: true
            },
            roleList: "required",
        },
        messages: {
            userEmailId: {
                required: "Please Enter your Email Id.",
                email1: "Please check the Email Id entered."
            }
        },
        errorPlacement: function (error, element) {

            switch (element.attr("name")) {
                case "userFirstName":
                    error.insertAfter($("#userFirstName"));
                    break;               
                case "userLastName":
                    error.insertAfter($("#userLastName"));
                    break;
                case "ltRoleList":
                    error.insertAfter($("#ltRoleList"));
                    break;
            }
        },
        tooltip_options: {
            userEmailId: {
                trigger: "focus",
                html: true
            }
        }
    });

    $('#createUserBtn').on('click', function () {
        var check;

        if ($("#popupForm").valid()) {
            $("#preloader").show();
            $("#status").show();
            check = saveUserData();

            if (check == 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data is saved successfully.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'AdminHome.aspx';
                });
            }
            else if (check == 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data cannot be saved.\nPlease contact your database administrator.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'AdminHome.aspx';
                });
            }
        }

    });
 
});

saveUserData = function () {

    var userFirstName = $('#userFirstName').val();
    var userMiddleName = $('#userMiddleName').val();
    var userLastName = $('#userLastName').val();

    var userEmailId = $('#userEmailId').val();

    var userRoleGuid = $("#roleList").val();

    var ret = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "AdminHome.aspx/SaveUserData",
        data: '{"userFirstName":"' + userFirstName + '","userMiddleName":"' + userMiddleName + '","userLastName":"' + userLastName + '","userEmailId":"' + userEmailId + '","userRoleGuid":"' + userRoleGuid + '"}',
        //'","superUserGuid":"' + superUserId + '","adminGuid":"' + adminId + '","superAdminGuid":"' + superAdminId + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });
    
}

