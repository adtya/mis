﻿var assetRiskRatingRelationGuid;
var assetCodeGuid;
var assetRiskRatingGuid;
var assetRiskData = [];
$(document).ready(function () {

    $('#assetRiskRatingBtn').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetAssetCodeDataWithAssetRisk",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                getAssetRiskRating();

                $('#assetRiskRatingPopupModal .modal-body').append(response.d);
               
                $('#assetRiskRatingPopupModal').modal('show');
                assetRiskRatingTableRelatedFunctions();
            },
            failure: function (result) {
                alert("Error");
            }
        });
        e.stopPropagation();
    });

    $('#assetRiskRatingPopupModal').on('click', '.modal-footer button#backFromAssetRiskRatingPopUpModalToDbaBtn', function (e) {
        e.preventDefault();

        window.location = 'DBAHome.aspx';

        e.stopPropagation();
    });

});

assetRiskRatingTableRelatedFunctions = function () {
    var assetRiskRatingRowEditing = null;

    var assetRiskRatingTable = $('#assetRiskRatingDataList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false
            },            
            {
                className: 'never',//className:'hidden',
                targets: [4],
                visible: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [5],
                visible: false,
                searchable: false
            }

        ],
            language: {
            //"lengthMenu": "Display _MENU_ records per page",
                lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
        zeroRecords: "Nothing found - sorry",
        info: "Showing page _PAGE_ of _PAGES_",
        infoEmpty: "No records available",
        infoFiltered: "(filtered from _MAX_ total records)"
    },
            pageLength: 5
        
    });

    assetRiskRatingTable.on('order.dt search.dt', function () {
        assetRiskRatingTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

   $('#assetRiskRatingPopupModal').on('click', '#assetRiskRatingDataList a.editAssetRiskRatingRowBtn', function (e){
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (assetRiskRatingRowEditing !== null && assetRiskRatingRowEditing != selectedRow) {

            /* A different row is being edited - the edit should be cancelled and this row edited */

            restoreAssetCodeWithAssetRiskRatingRelationData(assetRiskRatingTable, assetRiskRatingRowEditing);
            editAssetCodeWithAssetRiskRatingRelationRow(assetRiskRatingTable, selectedRow);
            assetRiskRatingRowEditing = selectedRow;

            var el = $('#assetRiskRatingPopupModal select#assetRiskRatingSelect');
            var elemLen = el.val().length;
            el.focus();


            el.setCursorPosition(elemLen);
        }
        else {
            /* No row currently being edited */
            editAssetCodeWithAssetRiskRatingRelationRow(assetRiskRatingTable, selectedRow);
            assetRiskRatingRowEditing = selectedRow;

            var el = $('#assetRiskRatingPopupModal select#assetRiskRatingSelect');
            var elemLen = el.val().length;
            el.focus();

            el.setCursorPosition(elemLen);
        }
    });

    $('#assetRiskRatingPopupModal').on('click', '#assetRiskRatingDataList a.updateAssetCodeWithAssetRiskRatingRowBtn', function (e) {
        e.preventDefault();

        if ($("#assetRiskRatingPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            assetRiskRatingRowEditing = selectedRow;

            var guid = assetRiskRatingTable.row(selectedRow).data()[4];
            if (guid == "00000000-0000-0000-0000-000000000000") {

                assetCodeGuid = assetRiskRatingTable.row(selectedRow).data()[1];
                
                var assetRiskRatingRelationSavedGuid = saveAssetCodeAndAssetRiskRatingRelation(assetRiskRatingTable, selectedRow);

                if (jQuery.Guid.IsValid(assetRiskRatingRelationSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(assetRiskRatingRelationSavedGuid.toUpperCase())) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your Data is saved successfully.", function () {
                        if (assetRiskRatingRowEditing !== null && assetRiskRatingRowEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */

                            saveNewAssetCodeWithAssetRiskRatingRelationRow(assetRiskRatingTable, assetRiskRatingRowEditing, assetRiskRatingRelationSavedGuid);
                            assetRiskRatingRowEditing = null;
                        }
                    });
                }
                else {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }

            }
            else {
                assetRiskRatingRelationGuid = assetRiskRatingTable.row(selectedRow).data()[4];
                var assetRiskRatingRelationUpdated = updateAssetCodeAndAssetRiskRatingRelationData(assetRiskRatingTable, selectedRow);

                if (assetRiskRatingRelationUpdated == 1) {

                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your Data is updated successfully.", function () {
                        if (assetRiskRatingRowEditing !== null && assetRiskRatingRowEditing == selectedRow) {

                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            updateAssetCodeWithAssetRiskRatingRelationRow(assetRiskRatingTable, assetRiskRatingRowEditing);
                            assetRiskRatingRowEditing = null;
                          

                        }
                    });
                } else if (assetRiskRatingRelationUpdated == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
                }
            }
        }

    });

    $('#assetRiskRatingPopupModal').on('click', '#assetRiskRatingDataList a.cancelAssetCodeWithAssetRiskRatingRowBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        var selectedRowData = assetRiskRatingTable.row(selectedRow).data();

        var availableTds = $('>td', selectedRow);

        var selectedRowSerialNum = availableTds[0].innerHTML;

        var selectedAssetRiskRatingGuidRow = assetRiskRatingTable.row(selectedRow).data()[5];

        var selectedAssetRiskRating = assetRiskRatingTable.row(selectedRow).data()[6];

        selectedRowData[5] = selectedAssetRiskRatingGuidRow;
        selectedRowData[6] = selectedAssetRiskRating;

        assetRiskRatingTable.row(selectedRow).data(selectedRowData);

        availableTds[0].innerHTML = selectedRowSerialNum;

    });

}

editAssetCodeWithAssetRiskRatingRelationRow = function (assetRiskRatingTable, selectedRow) {
    var selectedRowData = assetRiskRatingTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    var assetRiskRatingGuid = assetRiskRatingTable.row(selectedRow).data()[5];

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.

    var html = '<select name=assetRiskRatingSelect class=form-control assetRiskRatingSelect>';

    for (var i = 0; i < assetRiskData.length; i++) {
        if (assetRiskData[i].AssetRiskRatingGuid == assetRiskRatingGuid) {
            html += '<option selected="selected" value="' + assetRiskData[i].AssetRiskRatingGuid + '">' + assetRiskData[i].AssetRiskRating + '</option>';
        }
        else {
            html += '<option value="' + assetRiskData[i].AssetRiskRatingGuid + '">' + assetRiskData[i].AssetRiskRating + '</option>';
        }

    }

    html += '</select>';
    
    availableTds[3].innerHTML = html;
    availableTds[4].innerHTML = '<a href="#" class="updateAssetCodeWithAssetRiskRatingRowBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelAssetCodeWithAssetRiskRatingRowBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

getAssetRiskRating = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "DBAMethods.asmx/GetAssetRiskRating",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            assetRiskData = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

saveNewAssetCodeWithAssetRiskRatingRelationRow = function (assetRiskRatingTable, selectedRow, assetRiskRatingRelationSavedGuid) {

    var selectedRowData = assetRiskRatingTable.row(selectedRow).data();

    var selectedAssetRiskRatingGuid;

    var selectedAssetRiskRating;

    var availableSelects = $('select', selectedRow)[0].value;

    for (var i = 0; i < assetRiskData.length; i++) {
        if (assetRiskData[i].AssetRiskRatingGuid == availableSelects) {

            selectedAssetRiskRatingGuid = assetRiskData[i].AssetRiskRatingGuid;
            selectedAssetRiskRating = assetRiskData[i].AssetRiskRating;
        }
    }

    selectedRowData[4] = assetRiskRatingRelationSavedGuid;
    selectedRowData[5] = selectedAssetRiskRatingGuid;
    selectedRowData[6] = selectedAssetRiskRating;

    assetRiskRatingTable.row(selectedRow).data(selectedRowData);

    assetRiskRatingTable.on('order.dt search.dt', function () {
        assetRiskRatingTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

updateAssetCodeWithAssetRiskRatingRelationRow = function (assetRiskRatingTable, selectedRow) {
    var selectedRowData = assetRiskRatingTable.row(selectedRow).data();    

    var selectedAssetRiskRatingGuid;

    var selectedAssetRiskRating;

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    var availableSelects = $('select', selectedRow)[0].value;      

    for (var i = 0; i < assetRiskData.length; i++) {
        if (assetRiskData[i].AssetRiskRatingGuid == availableSelects) {
            selectedAssetRiskRatingGuid = assetRiskData[i].AssetRiskRatingGuid;
            selectedAssetRiskRating = assetRiskData[i].AssetRiskRating;
        }
    }
    
    selectedRowData[5] = selectedAssetRiskRatingGuid;
    selectedRowData[6] = selectedAssetRiskRating;

    assetRiskRatingTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

}

restoreAssetCodeWithAssetRiskRatingRelationData = function (assetRiskRatingTable, previousRow) {
    var previousRowData = assetRiskRatingTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        //removeNewWorkItemRateRowCreatedOnCancel(assetRiskRatingTable, previousRow);
    } else {
        assetRiskRatingTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;
    }

}

saveAssetCodeAndAssetRiskRatingRelation = function (table, selectedRow) {
    var availableSelects = $('select', selectedRow);

    var selectValue = $.trim(availableSelects[0].value);

    var assetRiskRatingManager = {
        assetRiskRatingGuid: selectValue
}

    var assetManager=
       {
           assetGuid: assetCodeGuid
       }

    var assetCodeAndAssetRiskRatingRelationManager = {
        assetRiskRatingManager: assetRiskRatingManager,
        assetManager: assetManager
    }

    var assetCodeAndAssetRiskRatingRelationManagerJsonString = JSON.stringify(assetCodeAndAssetRiskRatingRelationManager);


    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveAssetCodeAndAssetRiskRatingRelation",
        data: '{"assetCodeAndAssetRiskRatingRelationManager":' + assetCodeAndAssetRiskRatingRelationManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updateAssetCodeAndAssetRiskRatingRelationData = function (table, selectedRow) {
    var availableSelects = $('select', selectedRow);

    var selectValue = $.trim(availableSelects[0].value);

    var assetRiskRatingGuid = selectValue;

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateAssetCodeAndAssetRiskRatingRelationData",
        data: '{"assetCodeAssetRiskRatingRelationGuid":"' + assetRiskRatingRelationGuid + '","assetRiskRatingGuid":"' + assetRiskRatingGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}


