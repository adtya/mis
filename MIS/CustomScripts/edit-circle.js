﻿$(document).ready(function () {

    $("#editCircleMenuBtn").on('click', function () {
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetCircleNames",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createEditCircleForm(response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

    $('#updateCircleBtn').on('click', function () {
        if ($("#editCirclePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            var circleGuid = $("#circleListToEditCircle").val();
            var circleName = $.trim($("#edittedCircleName").val());
            var circleCode = $.trim($("#edittedCircleCode").val());

            $('#editCirclePopupModal').modal('hide');

            $.ajax({
                type: "Post",
                async: false,
                url: "DBAMethods.asmx/UpdateCircle",
                data: '{"circleGuid":"' + circleGuid + '","circleName":"' + circleName + '","circleCode":"' + circleCode + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    var str = response.d;
                    if (str == true) {
                        bootbox.alert("Circle updated successfully");
                    }
                    else {
                        bootbox.alert("Circle cannot be updated.");
                    }
                },
                failure: function (msg) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    alert(msg);
                }
            });
        }
    });

});

createEditCircleForm = function (circleArr) {
    $("#editCirclePopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Edit Circle</h2>';

    html += '<div class="panel-body">';
    html += '<form id="editCirclePopupForm" class="form form-horizontal" method="post">';
    html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="circleListToEditCircle" id="circleListToEditCircleLabel" class="control-label">Circle Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<select id="circleListToEditCircle" name="circleListToEditCircle" class="form-control">';
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedCircleName" id="edittedCircleNameLabel" class="control-label">Circle Name</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedCircleName" name="edittedCircleName" placeholder="Circle Name" class="form-control text-capitalize" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedCircleCode" id="edittedCircleCodeLabel" class="control-label">Circle Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6\">';
    html += '<input id="edittedCircleCode" name="edittedCircleCode" placeholder="Circle Code" class="form-control text-uppercase" />';
    html += '</div>';

    html += '</fieldset>';
    html += '</form>';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#editCirclePopupModal .modal-body").append(html);
    
    showCircleListOptionsToEditCircle('circleListToEditCircle', circleArr);

    editCircleValidationRules();

    $('#editCirclePopupModal').modal('show');

    $('#circleListToEditCircle').on('change', function () {
        var selectedCircle = $(this).val();
        if (selectedCircle == "") {
            $('#updateCircleBtn').addClass('hidden');
        } else {
            for (var i = 0; i < circleArr.length;) {
                if (circleArr[i] == selectedCircle) {
                    $('#edittedCircleCode').val(circleArr[i + 1]);
                    $('#edittedCircleName').val(circleArr[i + 2]);
                }
                i = i + 3;
            }
            $('#updateCircleBtn').removeClass('hidden');
        }
    });
}

showCircleListOptionsToEditCircle = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Circle--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 3;
    }
}

editCircleValidationRules = function () {
    $('#editCirclePopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            edittedCircleCode: {
                required: true,
                noSpace: true,
                minlength: 3,
                maxlength: 5
            },
            edittedCircleName: {
                required: true,
                noSpace: true
            }
        }
    });
}