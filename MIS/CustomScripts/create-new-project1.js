﻿var arrOfficersEngaged = [];
var arrPmcSiteEngineers = [];
var arrSubChainages = [];
var arrWorkTypeItems = [];
var arrHeadBudgets = [];
var projectGuid;

document.onkeydown = function (e) {
    stopDefaultBackspaceBehaviour(e);
}

document.onkeypress = function (e) {
    stopDefaultBackspaceBehaviour(e);
}

function stopDefaultBackspaceBehaviour(event) {
    var event = event || window.event;
    if (event.keyCode == 8) {
        var elements = "HTML, BODY, TABLE, TBODY, TR, TD, DIV";
        var d = event.srcElement || event.target;
        var regex = new RegExp(d.tagName.toUpperCase());
        if (d.contentEditable != 'true') { //it's not REALLY true, checking the boolean value (!== true) always passes, so we can use != 'true' rather than !== true/
            if (regex.test(elements)) {
                event.preventDefault ? event.preventDefault() : event.returnValue = false;
            }
        }
    }
}

$(window).on('beforeunload', function () {
    console.log("beforeUnload event!");
    saveSelections(document.forms[0]);
});

$(window).unload(function () {
    console.log("beforeUnload event!");
    saveSelections(document.forms[0]);
});

$(document).ready(function () {
    
    var cfgCulture = 'en-IN';
    $.preferCulture(cfgCulture);
    //$.preferCulture("en-IN");

    var current = 1;

    $('#projectValue').maskMoney();
    $('#subChainageValue').maskMoney();

    widget = $(".step");

    btnNext = $(".next");
    btnBack = $(".back");
    btnSkip = $(".skip");

    btnSubmit = $(".submit");

    
    //Init Buttons and UI
    widget.not(':eq(0)').hide();
    hideButtons(current);
    setProgress(current);

    //Next Button Click Action
    btnNext.click(function () {
        //Step 1
        if (current == 1) {
            
            if ($(".form").valid()) {
                projectGuid = saveStep1();

                if ($.Guid.IsValid(projectGuid) && !$.Guid.IsEmpty(projectGuid)) {

                    bootbox.alert("Saved Properly.", function () {

                        if (current < widget.length) {
                            // Check validation
                            if ($(".form").valid()) {
                                widget.show();
                                widget.not(':eq(' + (current++) + ')').hide();
                                hideButtons(current);
                                setProgress(current);
                            }
                        }

                    });

                }
                else {
                    bootbox.alert("Not Saved Properly.");
                }
            }
        }

        //Step 2
        if (current == 2) {
            //if ($(".form").valid()) {
            //var savedProperly;
            if (arrOfficersEngaged.length > 0) {
                var savedProperly = saveStep2();

                if (savedProperly == true) {

                    bootbox.alert("Saved Properly.", function () {
                        //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                        //window.location.href = nextUrl;
                        //window.location = 'Default.aspx';

                        if (current < widget.length) {
                            // Check validation
                            //if ($(".form").valid()) {
                            widget.show();
                            widget.not(':eq(' + (current++) + ')').hide();
                            hideButtons(current);
                            setProgress(current);
                            //}
                        }

                    });

                }
                else {
                    bootbox.alert("Not Saved Properly.");
                }
            }
            else {
                bootbox.confirm({
                    title: '',
                    message: 'Are you sure yount want to leave officers list empty?<br>Are you sure?',
                    buttons: {
                        'cancel': {
                            label: 'No',
                            className: 'btn-default pull-left'
                        },
                        'confirm': {
                            label: 'Yes',
                            className: 'btn-danger pull-right'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            //window.location = $("a[data-bb='confirm']").attr('href');
                            //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                            //window.location.href = nextUrl;
                            //window.location = 'Default.aspx';
                            if (current < widget.length) {
                                // Check validation
                                //if ($(".form").valid()) {
                                widget.show();
                                widget.not(':eq(' + (current++) + ')').hide();
                                setProgress(current);
                                //}
                            }
                        }
                    }
                });
            }

        }
        //}

        //Step 3
        if (current == 3) {
            //if ($(".form").valid()) {
            //var savedProperly;
            if (arrPmcSiteEngineers.length > 0) {
                var savedProperly = saveStep3();

                if (savedProperly == true) {

                    bootbox.alert("Saved Properly.", function () {
                        //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                        //window.location.href = nextUrl;
                        //window.location = 'Default.aspx';

                        if (current < widget.length) {
                            // Check validation
                            //if ($(".form").valid()) {
                            widget.show();
                            widget.not(':eq(' + (current++) + ')').hide();
                            hideButtons(current);
                            setProgress(current);
                            // }
                        }

                    });

                }
                else {
                    bootbox.alert("Not Saved Properly.");
                }
            }
            else {
                bootbox.confirm({
                    title: '',
                    message: 'Are you sure yount want to leave site engineers list empty?<br>Are you sure?',
                    buttons: {
                        'cancel': {
                            label: 'No',
                            className: 'btn-default pull-left'
                        },
                        'confirm': {
                            label: 'Yes',
                            className: 'btn-danger pull-right'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            //window.location = $("a[data-bb='confirm']").attr('href');
                            //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                            //window.location.href = nextUrl;
                            //window.location = 'Default.aspx';
                            if (current < widget.length) {
                                // Check validation
                                //if ($(".form").valid()) {
                                widget.show();
                                widget.not(':eq(' + (current++) + ')').hide();
                                setProgress(current);
                                //}
                            }
                        }
                    }
                });
            }

        }
        //}

        //Step 4
        if (current == 4) {
            //if ($(".form").valid()) {
            //var savedProperly;
            if (arrSubChainages.length > 0) {
                var savedProperly = saveStep4();

                if (savedProperly == true) {

                    bootbox.alert("Saved Properly.", function () {
                        //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                        //window.location.href = nextUrl;
                        //window.location = 'Default.aspx';

                        if (current < widget.length) {
                            // Check validation
                            if ($(".form").valid()) {
                                widget.show();
                                widget.not(':eq(' + (current++) + ')').hide();
                                hideButtons(current);
                                setProgress(current);
                            }
                        }

                    });

                }
                else {
                    bootbox.alert("Not Saved Properly.");
                }
            }
            else {
                bootbox.confirm({
                    title: '',
                    message: 'Are you sure yount want to leave site engineers list empty?<br>Are you sure?',
                    buttons: {
                        'cancel': {
                            label: 'No',
                            className: 'btn-default pull-left'
                        },
                        'confirm': {
                            label: 'Yes',
                            className: 'btn-danger pull-right'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            //window.location = $("a[data-bb='confirm']").attr('href');
                            //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                            //window.location.href = nextUrl;
                            //window.location = 'Default.aspx';
                            if (current < widget.length) {
                                // Check validation
                                //if ($(".form").valid()) {
                                    widget.show();
                                    widget.not(':eq(' + (current++) + ')').hide();
                                    setProgress(current);
                                //}
                            }
                        }
                    }
                });
            }

            //}
        }

        //Step 5
        if (current == 5) {
            if ($(".form").valid()) {
                //var savedProperly;
                var savedProperly = saveStep5();

                if (savedProperly == true) {

                    bootbox.alert("Saved Properly.", function () {
                        //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                        //window.location.href = nextUrl;
                        //window.location = 'Default.aspx';

                        if (current < widget.length) {
                            // Check validation
                            if ($(".form").valid()) {
                                widget.show();
                                widget.not(':eq(' + (current++) + ')').hide();
                                hideButtons(current);
                                setProgress(current);
                            }
                        }

                    });

                }
                else {
                    bootbox.alert("Not Saved Properly.");
                }
            }
        }

        //Step 6
        if (current == 6) {
            if ($(".form").valid()) {
                var savedProperly = saveStep6();

                if (savedProperly == true) {

                    bootbox.alert("Saved Properly.", function () {
                        //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                        //window.location.href = nextUrl;
                        //window.location = 'Default.aspx';

                        if (current < widget.length) {
                            // Check validation
                            if ($(".form").valid()) {
                                widget.show();
                                widget.not(':eq(' + (current++) + ')').hide();
                                hideButtons(current);
                                setProgress(current);
                            }
                        }

                    });

                }
                else {
                    bootbox.alert("Not Saved Properly.");
                }
            }
        }

        //Step 7
        //if (current == 7) {
        //    if ($(".form").valid()) {
        //        var savedProperly = saveStep7();

        //        if (savedProperly == true) {

        //            bootbox.alert("The form is submitted successfully.", function () {
        //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
        //                //window.location.href = nextUrl;
        //                //window.location = 'Default.aspx';

        //                if (current < widget.length) {
        //                    // Check validation
        //                    if ($(".form").valid()) {
        //                        widget.show();
        //                        widget.not(':eq(' + (current++) + ')').hide();
        //                        hideButtons(current);
        //                        setProgress(current);
        //                    }
        //                }

        //            });

        //        }
        //        else {
        //            bootbox.alert("Not Saved Properly.");
        //        }
        //    }
        //}

        //hideButtons(current);
        
    });     



    

    // Submit button click
    btnSubmit.click(function () {
        $("#preloader").show();
        $("#status").show();

        $(function () {
            var check = saveStep7();
            if (check == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data is saved successfully.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'Default.aspx';
                });
            }
            else if (check == false) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data cannot be saved.\nPlease contact your administrator.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'Default.aspx';
                });
            }
        });
    });

    // Back button click action
    btnBack.click(function () {
        if (current > 1) {
            current = current - 2;
            if (current < widget.length) {
                widget.show();
                widget.not(':eq(' + (current++) + ')').hide();
                setProgress(current);
            }
        }
        hideButtons(current);
    });

    $('.form').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            districtList: "required",
            divisionList: "required",
            projectName: {
                required: true,
                noSpace: true
            },
            subProjectName: {
                required: true,
                noSpace: true
            },
            chainageOfProject: {
                required: true,
                noSpace: true
            },
            dateOfStartOfProject: {
                required: true,
                dateITA: true
            },
            expectedDateOfCompletionOfProject: {
                required: true,
                dateITA: true
            },
            projectValue: "required",

            officersEngaged: {
                required: true,
                noSpace: true
            },

            pmcSiteEngineers: {
                required: true,
                noSpace: true
            },

            subChainageName: {
                required: true,
                noSpace: true
            },
            subChainageSio: "required",
            subChainageProjectValue: "required",
            administrativeApprovalReference: {
                required: true,
                noSpace: true
            },
            technicalSanctionReference: {
                required: true,
                noSpace: true
            },
            contractorName: {
                required: true,
                noSpace: true
            },
            workOrderReference: {
                required: true,
                noSpace: true
            },
            subChainageStartingDate: {
                required: true,
                dateITA: true
            },
            subChainageCompletionDate: {
                required: true,
                dateITA: true
            },
            typeOfWorks: {
                required: true,
                noSpace: true
            }
        },
        messages: {
            districtList: {
                required: "Please Enter Project District."
            },
            divisionList: {
                required: "Please Enter Project Division."
            },
            projectName: {
                required: "Please Enter Project Name.",
                noSpace: "Project Name cannot be blank."
            },
            subProjectName: {
                required: "Please Enter Sub-Project Name.",
                noSpace: "Sub-Project Name cannot be blank."
            },
            chainageOfProject: {
                required: "Please Enter Chainage of the Project.",
                noSpace: "Chainage of the Project cannot be blank."
            },
            dateOfStartOfProject: {
                required: "Please Enter the Starting Date of the Project.",
                dateITA: "Please Enter the Starting Date of the Project as: dd/mm/yyyy"
            },
            expectedDateOfCompletionOfProject: {
                required: "Please Enter the Expected Completion Date of the Project.",
                dateITA: "Please Enter the Expected Completion Date of the Project as: dd/mm/yyyy"
            },
            projectValue: {
                required: "Please Enter the Value of the Project."
            },

            officersEngaged: {
                required: "Please Enter the Officer Name.",
                noSpace: "Officer Name cannot be blank."
            },

            pmcSiteEngineers: {
                required: "Please Enter the Site Engineer Name.",
                noSpace: "Site Engineer Name cannot be blank."
            },


            userTitle: "Please Select your Title.",
            firstName: {
                required: "Please Enter your First Name.",
                noSpace: "First Name cannot be blank"
            },
            lastName: "Please Enter your Last Name.",
            userGender: "Please Select your Gender.",
            userDob: {
                required: "Please Enter your Date Of Birth.",
                //date: "Please Enter a valid Date."
                dateITA: "Please enter the date as: dd/mm/yyyy"
            },
            userMobile: {
                required: "Please Enter your Mobile Number.",
                number: "Please Enter a valid Mobile Number.",
                maxlength: "Mobile Number cannot be of more than 10 digits.",
                minlength: "Mobile Number cannot be of less than 10 digits.",
                mobileIN: "Please Enter a Valid Mobile Number"
            },
            userAddressLine1: {
                required: "Please Enter your Address."
            },
            pinCode: "Please Enter your Pin Code.",
            userDivision: "Please Select your Division.",
            userDesignation: "Please Select your Designation.",
            userDepartment: "Please Select your Department.",
            userDoj: {
                required: "Please Enter your Date Of Joining.",
                //date: "Please Enter a valid Date.",
                dateITA: "Please enter the date as: dd/mm/yyyy"
            },
            superUserList: "Please select your Super User.",
            adminList: "Please select your Admin.",
            superAdminList: "Please select your Super Admin."
        },
        errorPlacement: function (error, element) {
            
            switch (element.attr("name")) {
                case "districtList":
                    error.insertAfter($("#districtList"));
                    break;
                case "divisionList":
                    error.insertAfter($("#divisionList"));
                    break;
                case "projectName":
                    error.insertAfter($("#projectName"));
                    break;
                case "subProjectName":
                    error.insertAfter($("#subProjectName"));
                    break;
                case "chainageOfProject":
                    error.insertAfter($("#chainageOfProject"));
                    break;
                case "dateOfStartOfProject":
                    error.insertAfter($("#dpDateOfStartOfProject"));
                    break;
                case "expectedDateOfCompletionOfProject":
                    error.insertAfter($("#dpExpectedDateOfCompletionOfProject"));
                    break;
                case "projectValue":
                    error.insertAfter($("#projectValueInputGroup"));
                    break;

                case "officersEngaged":
                    error.insertAfter($("#officersEngaged"));
                    break;

                case "pmcSiteEngineers":
                    error.insertAfter($("#pmcSiteEngineers"));
                    break;

                default:
                    //nothing
            }
        },
    });

    var fullDate = new Date();

    var twoDigitMonth = fullDate.getMonth() + 1 + "";
    if (twoDigitMonth.length == 1) {
        twoDigitMonth = "0" + twoDigitMonth;
    }

    var twoDigitDate = fullDate.getDate() + "";
    if (twoDigitDate.length == 1) {
        twoDigitDate = "0" + twoDigitDate;
    }

    var currentDate = twoDigitDate + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

    
    $('#dpDateOfStartOfProject').datepicker({
        format: "dd/mm/yyyy",
        startDate: currentDate.toString(),
        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        //setDate: fullDate
    }).on('changeDate', function (ev) {
        var projectExpectedCompletionDateStartDate = new Date(ev.date.valueOf());
        projectExpectedCompletionDateStartDate = projectExpectedCompletionDateStartDate.add(1).days();
        $('#dpExpectedDateOfCompletionOfProject').datepicker('setStartDate', projectExpectedCompletionDateStartDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpExpectedDateOfCompletionOfProject').datepicker('setStartDate', null);
    });

    $('#dpExpectedDateOfCompletionOfProject').datepicker({
        format: "dd/mm/yyyy",
        //startDate: currentDate.toString(),
        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        //setDate: fullDate
    }).on('changeDate', function (ev) {
        var projectStartDateEndDate = new Date(ev.date.valueOf());
        projectStartDateEndDate = projectStartDateEndDate.add(-1).days();
        $('#dpDateOfStartOfProject').datepicker('setEndDate', projectStartDateEndDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpDateOfStartOfProject').datepicker('setEndDate', null);
    });



    $('#dpSubChainageStartingDate').datepicker({
        format: "dd/mm/yyyy",
        startDate: currentDate.toString(),
        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        //setDate: fullDate
    }).on('changeDate', function (ev) {
        var subChainageCompletionDateStartDate = new Date(ev.date.valueOf());
        subChainageCompletionDateStartDate = subChainageCompletionDateStartDate.add(1).days();
        $('#dpSubChainageExpectedCompletionDate').datepicker('setStartDate', subChainageCompletionDateStartDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpSubChainageExpectedCompletionDate').datepicker('setStartDate', null);
    });

    $('#dpSubChainageExpectedCompletionDate').datepicker({
        format: "dd/mm/yyyy",
        //startDate: currentDate.toString(),
        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        //setDate: fullDate
    }).on('changeDate', function (ev) {
        var subChainageStartDateEndDate = new Date(ev.date.valueOf());
        subChainageStartDateEndDate = subChainageStartDateEndDate.add(-1).days();
        $('#dpSubChainageStartingDate').datepicker('setEndDate', subChainageStartDateEndDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpSubChainageStartingDate').datepicker('setEndDate', null);
    });

    //$('.date').datepicker({
    //    format: "dd/mm/yyyy",
    //    //endDate: currentDate.toString(),
    //    todayBtn: "linked",
    //    clearBtn: true,
    //    autoclose: true,
    //    todayHighlight: true
    //}).on('changeDate', function (ev) {
    //    //calculateAge();
    //    $(this).blur();
    //    $(this).datepicker('hide');
    //});

    $('#addOfficersEngagedBtn').on('click', function (e) {

        if ($(".form").valid()) {

            $('#officersListDisplay').removeClass('hidden');

            var data = $('#officersEngaged').val();
            arrOfficersEngaged.push(data);
            $('#officersEngaged').val("");

            displayEngagedOfficersList('officersListDisplay', arrOfficersEngaged);
        }

    });

    $('#addPmcSiteEngineersBtn').on('click', function (e) {

        if ($(".form").valid()) {

            $('#pmcSiteEngineersListDisplay').removeClass('hidden');

            var data = $('#pmcSiteEngineers').val();
            arrPmcSiteEngineers.push(data);
            $('#pmcSiteEngineers').val("");

            displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);
        }

    });

    $('#addSubChainagesBtn').on('click', function (e)  {

        if ($(".form").valid()) {

            $('#subChainagesListDisplay').removeClass('hidden');

            var subChainageName = $('#subChainageName').val();
            var subChainageSioGuid = $('#sioUsersList').val();

            var subChainageValue = $('#subChainageValue').val();
            var newchar = '';
            subChainageValue = subChainageValue.split(',').join(newchar);
            //subChainageValue = subChainageValue.replace(/,/g , newchar);
            subChainageValue = parseFloat(subChainageValue);

            var administrativeApprovalReference = $('#administrativeApprovalReference').val();
            var technicalSanctionReference = $('#technicalSanctionReference').val();
            var contractorName = $('#contractorName').val();
            var workOrderReference = $('#workOrderReference').val();

            var subChainageStartingDate = $('#subChainageStartingDate').val();
            subChainageStartingDate = Date.parse(subChainageStartingDate).toString('yyyy/MM/dd');

            var subChainageExpectedCompletionDate = $('#subChainageExpectedCompletionDate').val();
            subChainageExpectedCompletionDate = Date.parse(subChainageExpectedCompletionDate).toString('yyyy/MM/dd');

            var typeOfWorks = $('#typeOfWorks').val();
            //var workTypeItems = $('#workTypeItems').val();

            arrSubChainages.push({
                subChainageName: subChainageName,
                subChainageSioGuid: subChainageSioGuid,
                subChainageValue: subChainageValue,
                administrativeApprovalReference: administrativeApprovalReference,
                technicalSanctionReference: technicalSanctionReference,
                contractorName: contractorName,
                workOrderReference: workOrderReference,
                subChainageStartingDate: subChainageStartingDate,
                subChainageExpectedCompletionDate: subChainageExpectedCompletionDate,
                typeOfWorks: typeOfWorks,
                arrWorkTypeItems: arrWorkTypeItems
            });

            displaySubChainagesList('subChainagesListDisplay', arrSubChainages);
        }

    });

    $('#addWorkTypeItemsBtn').on('click', function (e) {

        $('#workTypeItemsListDisplay').removeClass('hidden');

        var workTypeItems = $('#workTypeItemList').val();

        arrWorkTypeItems.push(workTypeItems);

        $('option:selected', this).remove();

        displayWorkTypeItemsList('workTypeItemsListDisplay', arrWorkTypeItems);
    });

    $('#createNewHeadBtn').on('click', function (e) {
        $("#popupModal .modal-body").empty();

        var projectValue = $("#projectValue").val();
        var newchar = '';
        projectValue = projectValue.split(',').join(newchar);
        //subChainageValue = subChainageValue.replace(/,/g , newchar);
        projectValue = parseFloat(projectValue);

        var remainingProjectValue = projectValue;

        if (arrHeadBudgets.length > 0) {
            for (var i = 0; i < arrHeadBudgets.length; i++) {
                var a = arrHeadBudgets[i].headBudget;
                remainingProjectValue = remainingProjectValue - a;
            }
        }

        if (remainingProjectValue > 0) {
            var html = "<div class=\"container-fluid col-md-12\">";
            html += "<div class=\"row\">";
            html += "<div class=\"row\">";
            html += "<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">";
            html += "<div class=\"panel panel-default\">";
            html += "<div class=\"panel-body\">";
            html += "<div class=\"text-center\">";
            html += "<img src=\"Images/create-financial.png\" class=\"login\" height=\"70\" />";
            html += "<h2 class=\"text-center\">Financial Head</h2>";

            html += "<div class=\"panel-body\">";
            html += "<form id=\"popupForm\" class=\"popupForm form form-horizontal\" method=\"post\">";
            html += "<fieldset>";

            html += "<div class=\"form-group\">";
            html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
            html += "<label id=\"totalBudgetLblText\" class=\"control-label\">" + "Total Budget: " + "</label>";
            html += "</div>";
            html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
            html += "<label id=\"totalBudgetLbl\" class=\"control-label\">" + accounting.formatMoney(projectValue, { symbol: "Rs.", format: "%s %v" }) + "</label>";
            html += "</div>";

            html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
            html += "<label id=\"remainingBudgetLblText\" class=\"control-label\">" + "Remaining Budget: " + "</label>";
            html += "</div>";
            html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
            html += "<label id=\"remainingBudgetLbl\" name=\"remainingBudgetLbl\" class=\"control-label\" value=\"" + accounting.formatMoney(remainingProjectValue, { symbol: "Rs.", format: "%s %v" }) + "\">" + accounting.formatMoney(remainingProjectValue, { symbol: "Rs.", format: "%s %v" }) + "</label>";
            html += "</div>";
            html += "</div>";

            //html += "<div class=\"form-group\">";
            //html += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
            //html += "<label id=\"remainingBudgetLblText\" class=\"control-label\">" + "Remaining Budget: " + "</label>";
            //html += "</div>";
            //html += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
            //html += "<label id=\"remainingBudgetLbl\" name=\"remainingBudgetLbl\" class=\"control-label\">" + accounting.formatMoney(remainingProjectValue, { symbol: "Rs.", format: "%s %v" }) + "</label>";
            //html += "</div>";
            //html += "</div>";

            html += "<div class=\"form-group\">";
            html += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
            html += "<label for=\"headName\" id=\"headNameLabel\" class=\"control-label\">Name of Head</label>";
            html += "</div>";
            html += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
            html += "<input id=\"headName\" name=\"headName\" placeholder=\"Head Name\" class=\"form-control\" />";
            html += "</div>";
            html += "</div>";

            html += "<div class=\"form-group\">";
            html += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
            html += "<label for=\"headBudget\" id=\"headBudgetLabel\" class=\"control-label\">Budget</label>";
            html += "</div>";
            html += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
            html += "<input id=\"headBudget\" name=\"headBudget\" placeholder=\"Budget\" class=\"form-control\" />";
            html += "</div>";
            html += "</div>";

            html += "<div class=\"form-group\">";
            html += "<button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\" id=\"createHeadBtn\">Create Head</button>";
            html += "</div>";

            html += "</fieldset>";
            html += "</form>";
            html += "</div>";

            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";

            $("#popupModal .modal-body").append(html);
            $('#popupModal').modal('show');

            callOtherFuctions();

            $('#headBudget').maskMoney();

            $('#createHeadBtn').on('click', function (e) {

                if ($(".popupForm").valid()) {
                    var headName = $('#headName').val();

                    var headBudget = $('#headBudget').val();
                    var newchar = '';
                    headBudget = headBudget.split(',').join(newchar);
                    //subChainageValue = subChainageValue.replace(/,/g , newchar);
                    headBudget = parseFloat(headBudget);

                    arrHeadBudgets.push({
                        headName: headName,
                        headBudget: headBudget
                    });

                    createHeadTable();
                }

            });
        }

        

    });

});

// Hide buttons according to the current step
hideButtons = function (current) {
    var limit = parseInt(widget.length);

    $(".action").hide();

    if (current < limit) {
        btnNext.show();
    }

    if (current > 1) {
        btnBack.show();
    }

    if (current == limit) {
        btnNext.hide();
        btnSubmit.show();
    }
}

// Change progress bar action
setProgress = function (currstep) {
    var percent = parseFloat(100 / widget.length) * currstep;
    percent = percent.toFixed();
    $(".progress-bar").css("width", percent + "%").html(percent + "%");
    $("#step-display").html("Step " + currstep + " of " + widget.length);
}




callOtherFuctions = function () {
    $('.popupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            headName: "required",
            headBudget: {
                required: true,
                currencySmallerThan: [name = 'remainingBudgetLbl']
            }
        },
    });

}

createHeadTable = function () {
    $('#headTableDiv').empty();

    var projectValue = $("#projectValue").val();
    var newchar = '';
    projectValue = projectValue.split(',').join(newchar);
    //subChainageValue = subChainageValue.replace(/,/g , newchar);
    projectValue = parseFloat(projectValue);

    var totalAssignedValue = 0;

    if (arrHeadBudgets.length > 0) {
        for (var i = 0; i < arrHeadBudgets.length; i++) {
            var a = arrHeadBudgets[i].headBudget;
            totalAssignedValue = totalAssignedValue + a;
        }
    }

    if (arrHeadBudgets.length > 0) {
        var html1 = "<table class=\"table table-striped\">";

        html1 += "<thead>";
        html1 += "<tr>";
        html1 += "<th>";
        html1 += "Head Name";
        html1 += "</th>";
        html1 += "<th>";
        html1 += "Head Budget";
        html1 += "</th>";
        html1 += "<th>";
        html1 += "";
        html1 += "</th>";
        html1 += "</tr>";
        html1 += "</thead>";

        html1 += "<tfoot>";
        html1 += "<tr>";
        html1 += "<th>";
        html1 += "Total Value Assigned to Heads";
        html1 += "</th>";
        html1 += "<th>";
        html1 += accounting.formatMoney(totalAssignedValue, { symbol: "Rs.", format: "%s %v" });
        html1 += "</th>";
        html1 += "<th>";
        html1 += "";
        html1 += "</th>";
        html1 += "</tr>";
        html1 += "</tfoot>";

        html1 += "<tbody>";

        for (var i = 0; i < arrHeadBudgets.length; i++) {
            html1 += "<tr>";

            html1 += "<th>";
            html1 += arrHeadBudgets[i].headName;
            html1 += "</th>";

            html1 += "<th>";
            html1 += accounting.formatMoney(arrHeadBudgets[i].headBudget, { symbol: "Rs.", format: "%s %v" });
            html1 += "</th>";

            html1 += "<th>";
            html1 += '<button type="button" class="btn btn-primary" onclick="removeHeadTable(' + i + ');">Remove</button>';;
            html1 += "</th>";

            html1 += "</tr>";
        }


        html1 += "</tbody>";

        html1 += "</table>";

        $('#headTableDiv').append(html1);
    }
}

removeHeadTable = function (index) {
    //alert("aditya");
    //alert(index);
    arrHeadBudgets.splice(index, 1);

    createHeadTable();

}





//Save Step 1 Details
saveStep1 = function () {
    var districtGuid = $("#districtList").val();
    var divisionGuid = $("#divisionList").val();
    var projectName = $("#projectName").val();
    var subProjectName = $("#subProjectName").val();
    var chainageOfProject = $("#chainageOfProject").val();
    var dateOfStartOfProject = $("#dateOfStartOfProject").val();
    dateOfStartOfProject = Date.parse(dateOfStartOfProject).toString('yyyy/MM/dd');
    //var a = Date.parse(dateOfStartOfProject);
    //var b = a.toString('yyyy/MM/dd');
    
    //var c = a.format('Y/m/d');
    var expectedDateOfCompletionOfProject = $("#expectedDateOfCompletionOfProject").val();
    expectedDateOfCompletionOfProject = Date.parse(expectedDateOfCompletionOfProject).toString('yyyy/MM/dd');
    //var actualDateOfCompletionOfProject = $("#actualDateOfCompletionOfProject").val();

    var projectValue = $("#projectValue").val();
    var a = accounting.unformat(projectValue);
    var newchar = '';
    projectValue = projectValue.split(',').join(newchar);
    //projectValue = projectValue.replace(/,/g , newchar);
    projectValue = parseFloat(projectValue);

    //$("#preloader").show();
    //$("#status").show();
    var str = "00000000-0000-0000-0000-000000000000";
    $.ajax({
        type: "Post",
        async: false,
        url: "CreateNewProject.aspx/SaveProjectStep1",
        data: '{"districtGuid":"' + districtGuid + '","divisionGuid":"' + divisionGuid + '","projectName":"' + projectName + '","subProjectName":"' + subProjectName + '","chainageOfProject":"' + chainageOfProject +
                '","dateOfStartOfProject":"' + dateOfStartOfProject + '","expectedDateOfCompletionOfProject":"' + expectedDateOfCompletionOfProject + '","projectValue":"' + projectValue + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //if (str == true) {
            //    bootbox.alert("Step 1 Saved.");
            //}
            //else {
            //    bootbox.alert("Data cannot be saved.");
            //}
            //str = str1;
        },
        failure: function (msg) {
            alert(msg);
            //$('#status').delay(300).fadeOut(); // will first fade out the loading animation
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return str;
}

//Save Step 2 Details
saveStep2 = function () {
    var arrOfficersEngagedJsonString = JSON.stringify(arrOfficersEngaged);

    //$("#preloader").show();
    //$("#status").show();
    var str;
    $.ajax({
        type: "Post",
        async: false,
        url: "CreateNewProject.aspx/SaveProjectStep2",
        data: '{"projectGuid":"' + projectGuid + '","arrOfficersEngaged":' + arrOfficersEngagedJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //if (str == true) {
            //    bootbox.alert("Step 1 Saved.");
            //}
            //else {
            //    bootbox.alert("Data cannot be saved.");
            //}
            //str = str1;
        },
        failure: function (msg) {
            alert(msg);
            //$('#status').delay(300).fadeOut(); // will first fade out the loading animation
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return str;
}

//Save Step 3 Details
saveStep3 = function () {
    var arrPmcSiteEngineersJsonString = JSON.stringify(arrPmcSiteEngineers);

    //$("#preloader").show();
    //$("#status").show();
    var str;
    $.ajax({
        type: "Post",
        async: false,
        url: "CreateNewProject.aspx/SaveProjectStep3",
        data: '{"projectGuid":"' + projectGuid + '","arrPmcSiteEngineers":' + arrPmcSiteEngineersJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //if (str == true) {
            //    bootbox.alert("Step 1 Saved.");
            //}
            //else {
            //    bootbox.alert("Data cannot be saved.");
            //}
            //str = str1;
        },
        failure: function (msg) {
            alert(msg);
            //$('#status').delay(300).fadeOut(); // will first fade out the loading animation
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return str;
}

saveStep4 = function () {
    var arrSubChainagesJsonString = JSON.stringify(arrSubChainages);
    var retValue = false;
    $.ajax({
        type: "Post",
        async: false,
        url: "CreateNewProject.aspx/SaveProjectStep4",
        data: '{"projectGuid":"' + projectGuid + '","arrSubChainages":' + arrSubChainagesJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');

            //alert("Success");
            retValue = response.d;
            //var str = response.d;
            //if (str == true) {
            //    bootbox.confirm({
            //        title: '!!Attention!!',
            //        message: 'There is already a person with this designation in this division in our database. To change click Change otherwise click Exit.',
            //        buttons: {
            //            'cancel': {
            //                label: 'Change',
            //                className: 'btn-default pull-left'
            //            },
            //            'confirm': {
            //                label: 'Exit',
            //                className: 'btn-danger pull-right'
            //            }
            //        },
            //        callback: function (result) {
            //            if (result) {
            //                //window.location = $("a[data-bb='confirm']").attr('href');
            //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
            //                //window.location.href = nextUrl;
            //                window.location = 'Default.aspx';
            //            }
            //        }
            //    });
            //}
            //ret = str;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your administrator.");
        }
    });

    return retValue;
}

saveStep5 = function () {
    var includeLandAcquisition = $('input[name=landAcquisitionRadio]:checked', '#createNewProjectForm').val();
    var retValue = false;

    if (includeLandAcquisition == "yes") {
        //includeLandAcquisition = "true";

        $.ajax({
            type: "Post",
            async: false,
            url: "CreateNewProject.aspx/SaveProjectStep5",
            data: '{"projectGuid":"' + projectGuid + '","includeLandAcquisition":"true"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //$('#status').delay(300).fadeOut();
                //$('#preloader').delay(350).fadeOut('slow');

                //alert("Success");
                retValue = response.d;
                //var str = response.d;
                //if (str == true) {
                //    bootbox.confirm({
                //        title: '!!Attention!!',
                //        message: 'There is already a person with this designation in this division in our database. To change click Change otherwise click Exit.',
                //        buttons: {
                //            'cancel': {
                //                label: 'Change',
                //                className: 'btn-default pull-left'
                //            },
                //            'confirm': {
                //                label: 'Exit',
                //                className: 'btn-danger pull-right'
                //            }
                //        },
                //        callback: function (result) {
                //            if (result) {
                //                //window.location = $("a[data-bb='confirm']").attr('href');
                //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                //                //window.location.href = nextUrl;
                //                window.location = 'Default.aspx';
                //            }
                //        }
                //    });
                //}
                //ret = str;
            },
            failure: function (msg) {
                //$('#status').delay(300).fadeOut();
                //$('#preloader').delay(350).fadeOut('slow');
                //bootbox.alert("Please contact your administrator.");
            }
        });

        
    } else {
        retValue = true;
    }
    
    return retValue;
}

saveStep6 = function () {
    var arrHeadBudgetsJsonString = JSON.stringify(arrHeadBudgets);
    var retValue = false;
    $.ajax({
        type: "Post",
        async: false,
        url: "CreateNewProject.aspx/SaveProjectStep6",
        data: '{"projectGuid":"' + projectGuid + '","arrHeadBudgets":' + arrHeadBudgetsJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');

            //alert("Success");
            retValue = response.d;
            //var str = response.d;
            //if (str == true) {
            //    bootbox.confirm({
            //        title: '!!Attention!!',
            //        message: 'There is already a person with this designation in this division in our database. To change click Change otherwise click Exit.',
            //        buttons: {
            //            'cancel': {
            //                label: 'Change',
            //                className: 'btn-default pull-left'
            //            },
            //            'confirm': {
            //                label: 'Exit',
            //                className: 'btn-danger pull-right'
            //            }
            //        },
            //        callback: function (result) {
            //            if (result) {
            //                //window.location = $("a[data-bb='confirm']").attr('href');
            //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
            //                //window.location.href = nextUrl;
            //                window.location = 'Default.aspx';
            //            }
            //        }
            //    });
            //}
            //ret = str;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your administrator.");
        }
    });

    return retValue;
}

saveStep7 = function () {
    var retValue = false;
    $.ajax({
        type: "Post",
        async: false,
        url: "CreateNewProject.aspx/SaveProjectStep7",
        data: '{"projectGuid":"' + projectGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');

            //alert("Success");
            retValue = response.d;
            //var str = response.d;
            //if (str == true) {
            //    bootbox.confirm({
            //        title: '!!Attention!!',
            //        message: 'There is already a person with this designation in this division in our database. To change click Change otherwise click Exit.',
            //        buttons: {
            //            'cancel': {
            //                label: 'Change',
            //                className: 'btn-default pull-left'
            //            },
            //            'confirm': {
            //                label: 'Exit',
            //                className: 'btn-danger pull-right'
            //            }
            //        },
            //        callback: function (result) {
            //            if (result) {
            //                //window.location = $("a[data-bb='confirm']").attr('href');
            //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
            //                //window.location.href = nextUrl;
            //                window.location = 'Default.aspx';
            //            }
            //        }
            //    });
            //}
            //ret = str;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your administrator.");
        }
    });

    return retValue;
}




displayEngagedOfficersList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    //var newDiv = document.createElement('div');
    // newDiv.className = "panel panel-default";

    //var html = '<div class="panel-heading">Panel heading</div>';
    //html += '<div class="panel-body">';

    var html = '<ul class = "list-group">';
    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += arr[index];
        html += '<button type="button" class="btn btn-primary" onclick="removeOfficersEngaged(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    //html += '</div>';

    //newDiv.innerHTML = html;

    //document.getElementById(divname).appendChild(newDiv);

    document.getElementById(divname).innerHTML = html;

    if (arr.length == 0) {
        div.addClass('hidden');
    }
}

removeOfficersEngaged = function (index) {
    //alert("aditya");
    //alert(index);
    arrOfficersEngaged.splice(index, 1);

    displayEngagedOfficersList('officersListDisplay', arrOfficersEngaged);

}

displayPmcSiteEngineersList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    //var newDiv = document.createElement('div');
    //newDiv.className = "panel panel-default";

    //var html = '<div class="panel-heading">Panel heading</div>';
    //html += '<div class="panel-body">';

    var html = '<ul class = "list-group">';
    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += arr[index];
        html += '<button type="button" class="btn btn-primary" onclick="removePmcSiteEngineers(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    //html += '</div>';

    //newDiv.innerHTML = html;

    //document.getElementById(divname).appendChild(newDiv);

    document.getElementById(divname).innerHTML = html;

    if (arr.length == 0) {
        div.addClass('hidden');
    }
}

removePmcSiteEngineers = function (index) {
    //alert("aditya");
    //alert(index);
    arrPmcSiteEngineers.splice(index, 1);

    displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);

}

displaySubChainagesList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    //var newDiv = document.createElement('div');
    //newDiv.className = "panel panel-default";

    //var html = '<div class="panel-heading">Panel heading</div>';
    //html += '<div class="panel-body">';

    var html = '<ul class = "list-group">';
    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += "Sub Chainage: " + arr[index].subChainageName + " ";
        //alert(arr[index].subChainageName + " " + arr[index].subChainageSioName + " " + arr[index].subChainageProjectValue + " " + arr[index].aaRef + " " + arr[index].tsRef + " " + arr[index].contractorName + " " + arr[index].workOrderRef + " " + arr[index].subChainageStartingDate + " " + arr[index].subChainageCompletionDate + " " + arr[index].typeOfWorks);
        html += '<button type="button" class="btn btn-primary" onclick="removeSubChainages(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    //html += '</div>';

    //newDiv.innerHTML = html;

    //document.getElementById(divname).appendChild(newDiv);

    document.getElementById(divname).innerHTML = html;

    if (arr.length == 0) {
        div.addClass('hidden');
    }
}

removeSubChainages = function (index) {
    //alert("aditya");
    //alert(index);
    arrSubChainages.splice(index, 1);
    displaySubChainagesList('subChainagesListDisplay', arrSubChainages);
}

displayWorkTypeItemsList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    //var newDiv = document.createElement('div');
    //newDiv.className = "panel panel-default";

    //var html = '<div class="panel-heading">Panel heading</div>';
    //html += '<div class="panel-body">';

    var html = '<ul class = "list-group">';
    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += arr[index] + " ";
        html += '<button type="button" class="btn btn-primary" onclick="removeWorkTypeItems(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    //html += '</div>';

    //newDiv.innerHTML = html;

    //document.getElementById(divname).appendChild(newDiv);

    document.getElementById(divname).innerHTML = html;

    if (arr.length == 0) {
        div.addClass('hidden');
    }
}

removeWorkTypeItems = function (index) {
    //alert("aditya");
    //alert(index);
    arrWorkTypeItems.splice(index, 1);

    displayWorkTypeItemsList('workTypeItemsListDisplay', arrWorkTypeItems);

}