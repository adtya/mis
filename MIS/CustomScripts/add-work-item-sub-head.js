﻿$(document).ready(function () {

    $('#addWorkItemSubHeadMenuBtn').on('click', function (e) {
        e.preventDefault();
        $('#addWorkItemSubHeadPopupModal').modal('show');
    });

    $('#addWorkItemSubHeadPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            workItemSubHeadName: {
                required: true,
                noSpace: true
            }
        }
    });

    $('#addWorkItemSubHeadBtn').on('click', function (e) {
        e.preventDefault();

        if ($("#addWorkItemSubHeadPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            var checkExistence = checkWorkItemSubHeadExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a WorkItem SubHead with this name.");
            } else if (checkExistence == false) {
                var check = saveWorkItemSubHead();

                if (check == 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    $('#workItemSubHeadName').val('');
                    bootbox.alert("Your Sub-Head is created successfully.");
                } else if (check == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }
            }
            
        }

    });

});

checkWorkItemSubHeadExistence = function () {
    var subHeadName = $.trim($('#workItemSubHeadName').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckWorkItemSubHeadExistence",
        data: '{"subHeadName":"' + subHeadName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveWorkItemSubHead = function () {
    var subHeadName = $.trim($('#workItemSubHeadName').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveWorkItemSubHead",
        data: '{"subHeadName":"' + subHeadName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}