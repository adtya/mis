﻿$(document).ready(function () {

    $("#routineServiceLevelBtn").on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetRoutineServiceLevelData",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createRoutineServiceLevelForm(response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });
        e.stopPropagation();
    });

});

createRoutineServiceLevelForm = function (routineServiceLevelData) {
    $("#routineServiceLevelPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/view-data.png" class="login" height="70" />';
    html += '<h2 class="text-center">Routine Service Level</h2>';

    html += '<div class="panel-body">';
    html += '<form id="routineServiceLevelPopupForm" name="routineServiceLevelPopupForm" role="form" class="form form-horizontal" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    if (routineServiceLevelData.length > 0) {
        html += '<div class="table-responsive">';
        html += '<table id="routineServiceLevelDataList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

        html += '<thead>';
        html += '<tr>';
        html += '<th>' + '#' + '</th>';
        html += '<th>' + 'Routine Service Level Guid' + '</th>';
        html += '<th>' + 'Service Level Code' + '</th>';      
        html += '<th>' + 'Work Item Description' + '</th>';
        html += '<th>' + 'Unit' + '</th>';
        html += '<th>' + 'Asset Type Code' + '</th>';
        html += '<th>' + 'Service  Level' + '</th>';
        html += '<th>' + 'Operation' + '</th>';
        html += '</tr>';
        html += '</thead>';

        html += '<tbody>';

        for (var i = 0; i < routineServiceLevelData.length; i++) {
            html += '<tr>';

            html += '<td>' + '' + '</td>';
            html += '<td id="routineServiceLevelGuid">' + routineServiceLevelData[i].RoutineServiceLevelGuid + '</td>';

            html += '<td>' + routineServiceLevelData[i].RoutineServiceLevelCode + '</td>';
            html += '<td>' + routineServiceLevelData[i].RoutineWorkItemManager.ShortDescription + '</td>';
            html += '<td>' + routineServiceLevelData[i].RoutineWorkItemManager.AssetQuantityParameterManager.UnitManager.UnitName + '</td>';
            html += '<td>' + routineServiceLevelData[i].AssetTypeManager.AssetTypeCode + '</td>';
            html += '<td>' + routineServiceLevelData[i].RoutineServiceLevel + '</td>';
            html += '<td>' + '<a href="#" class="editRoutineServiceLevelRowBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '</td>';
            html += '</tr>';
        }

        html += '</tbody>';

        html += '</table>';
        html += '</div>';
    } else {
        html += '<p>There is no data to be displayed.</p>';
    }

    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#routineServiceLevelPopupModal .modal-body").append(html);

    $('#routineServiceLevelPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });
    routineServiceLevelTableRelatedFunctions();

    $('#routineServiceLevelPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            routineServiceLevel: {
                required: true
            }
        },
        messages: {
            routineServiceLevel: {
                required: "Please Enter Routine Service Level."
            }
        },

    });
}

routineServiceLevelTableRelatedFunctions = function () {
    var routineServiceLevelRowEditing = null;

    var routineServiceLevelTable = $('#routineServiceLevelDataList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                searchable: false
            }
        ]
    });
   
    routineServiceLevelTable.on('order.dt search.dt', function () {
        routineServiceLevelTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $('#routineServiceLevelPopupModal').on('click', '#routineServiceLevelDataList a.editRoutineServiceLevelRowBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (routineServiceLevelRowEditing !== null && routineServiceLevelRowEditing != selectedRow) {

            /* A different row is being edited - the edit should be cancelled and this row edited */

            restoreRoutineServiceLevelRowData(routineServiceLevelTable, routineServiceLevelRowEditing);
            editRoutineServiceLevelRow(routineServiceLevelTable, selectedRow);
            routineServiceLevelRowEditing = selectedRow;
            
            var el = $('#routineServiceLevelPopupModal input#routineServiceLevel');
            var elemLen = el.val().length;
            el.focus();


            el.setCursorPosition(elemLen);
        }
        else {
            /* No row currently being edited */
            editRoutineServiceLevelRow(routineServiceLevelTable, selectedRow);
            routineServiceLevelRowEditing = selectedRow;

            var el = $('#routineServiceLevelPopupModal input#routineServiceLevel');
            var elemLen = el.val().length;
            el.focus();

            el.setCursorPosition(elemLen);
        }
    });

    $('#routineServiceLevelPopupModal').on('click', '#routineServiceLevelDataList a.updateRoutineServiceLevelRowBtn', function (e) {
        e.preventDefault();

        if ($("#routineServiceLevelPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var routineServiceLevelUpdated = updateRoutineServiceLevelData(routineServiceLevelTable.row(selectedRow).data()[1]);

            if (routineServiceLevelUpdated == 1) {

                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your Data is updated successfully.", function () {
                    if (routineServiceLevelRowEditing !== null && routineServiceLevelRowEditing == selectedRow) {

                        /* A different row is being edited - the edit should be cancelled and this row edited */

                        updateRoutineServiceLevelRow(routineServiceLevelTable, routineServiceLevelRowEditing);
                        routineServiceLevelRowEditing = null;
                    }
                });
            } else if (routineServiceLevelUpdated == 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
            }
        }

    });

    $('#routineServiceLevelPopupModal').on('click', '#routineServiceLevelDataList a.cancelRoutineServiceLevelRowBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (routineServiceLevelRowEditing !== null && routineServiceLevelRowEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */

            restoreRoutineServiceLevelRowData(routineServiceLevelTable, routineServiceLevelRowEditing);
            routineServiceLevelRowEditing = null;
        }

    });
}

editRoutineServiceLevelRow = function (routineServiceLevelTable, selectedRow) {
    var selectedRowData = routineServiceLevelTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.

    availableTds[5].innerHTML = '<input type="text" id="routineServiceLevel" name="routineServiceLevel" placeholder="Routine Service Level" class="form-control text-capitalize" value="' + selectedRowData[6] + '">';
    availableTds[6].innerHTML = '<a href="#" class="updateRoutineServiceLevelRowBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelRoutineServiceLevelRowBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

updateRoutineServiceLevelRow = function (routineServiceLevelTable, selectedRow) {
    var selectedRowData = routineServiceLevelTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML; 

    selectedRowData[6] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
    
    routineServiceLevelTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

}

restoreRoutineServiceLevelRowData = function (routineServiceLevelTable, previousRow) {
    var previousRowData = routineServiceLevelTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewServiceLevelRowCreatedOnCancel(routineServiceLevelTable, previousRow);
    } else {
        routineServiceLevelTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;

        //workItemSubHeadTable.cell(previousRowIndex, 0).innerHTML = previousRowSerialNum;

        //workItemSubHeadTable.draw();
    }

}

updateRoutineServiceLevelData = function (routineServiceLevelGuid) {
    var routineServiceLevel = $.trim($('#routineServiceLevelPopupModal input#routineServiceLevel').val());   

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateRoutineServiceLevelData",
        data: '{"routineServiceLevelGuid":"' + routineServiceLevelGuid + '","routineServiceLevel":"' + routineServiceLevel + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}
