﻿jQuery.validator.addMethod("noSpace", function (value, element) {
    //return value.indexOf(" ") < 0 && value != "";
    if (jQuery.trim(value) == "") {
        return false;
    } else {
        return true;
    }
}, "No space please");

//jQuery.validator.addMethod("noLeadingSpace", function (value, element) {
//    //return value.indexOf(" ") < 0 && value != "";
//    if (value.replace(/^\s+/, '') == "") {
//        return false;
//    } else {
//        return true;
//    }
//}, "No space in the starting");

//jQuery.validator.addMethod("noLeadingSpaceCheck", function (value, element) {
//    //return value.indexOf(" ") < 0 && value != "";
//    //if (/^\s+$/.test(value)) {
//    //    return false;
//    //} else {
//    //    return true;
//    //}
//    return /^\s/.test(value);
//}, "No space in the startingcccccccccccccccgg");

//jQuery.validator.addMethod("noTrailingSpace", function (value, element) {
//    //return value.indexOf(" ") < 0 && value != "";
//    if (value.replace(/\s+$/, '') == "") {
//        return false;
//    } else {
//        return true;
//    }
//}, "No space in the starting");

//jQuery.validator.addMethod("otpEmail",
//    function (value, element) { return this.optional(element) || /^.+@webucator.com$/.test(value); },
//    "Only webucator email address is allowed."); /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/

jQuery.validator.addMethod("email1",
    function (value, element) { return this.optional(element) || /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(value); },
    "Only proper email address is allowed.");

//jQuery.validator.addMethod("email2",
//    function (value, element) { return this.optional(element) || /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(value); },
//    "Only proper email address is allowed.");

jQuery.validator.addMethod("mobileIN",
    function (value, element) { return this.optional(element) || /^[1-9]{1}[0-9]{9}$/.test(value); },
    "Only proper mobile number is allowed.");

jQuery.validator.addMethod('smallerThan', function (value, element, param) {
    return + $(element).val() < + $(param).val();
});

jQuery.validator.addMethod('smallerThanEqualTo', function (value, element, param) {
    value1 = parseFloat(value);
    value2 = parseFloat($('#' + param).val());

    return value1 <= value2;
});

$.validator.addMethod("currencySmallerThan", function (value, element, param) {
    //var value1 = $(element).val();
    var newchar = '';
    value = value.split('Rs. ').join(newchar);
    value = value.split(',').join(newchar);
    //projectValue = projectValue.replace(/,/g , newchar);
    value = parseFloat(value);

    var value2 = $('#' + param).val();
    var newchar = '';
    value2 = value2.split('Rs. ').join(newchar);
    value2 = value2.split(',').join(newchar);
    //projectValue = projectValue.replace(/,/g , newchar);
    value2 = parseFloat(value2);

    return value <= value2;
}, "Please enter a lower currency");

$.validator.addMethod("dateInSpecifiedFormat", function (value, element, format) {
    var check = false,
        myDate = Date.parseExact(value, format);
		//re = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/,
		//adata, gg, mm, aaaa, xdata;

    if (myDate) {
        check = true;
    } else {
        check = false;
    }

    return this.optional(element) || check;
}, "Please enter a correct date");

$.validator.addMethod("needsSelection", function (value, element) {
    var count = $(element).find('option:selected').length;
    return count > 0;
});