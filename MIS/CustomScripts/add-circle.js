﻿$(document).ready(function () {

    $('#addCirclePopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {            
            circleCode: {
                required: true,
                noSpace: true,
                maxlength: 5,
                minlength: 3
            },
            circleName: {
                required: true,
                noSpace: true
            }
        }    
    });

    $('#addCircleBtn').on('click', function () {

        if ($("#addCirclePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            var check = saveCircle();

            if (check == 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your Circle is created successfully.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'DBAHome.aspx';
                });
            }
            else if (check == 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data cannot be saved.\nPlease contact your database administrator.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'DBAHome.aspx';
                });
            }
        }
    });

    $('#addCirclePopupModal').on('click', '.modal-footer button#addCirclePopupModalCloseBtn', function (e) {
        e.preventDefault();       

        location.reload(true);

        e.stopPropagation();
    });

});

saveCircle = function () {
    var circleCode = $.trim($('#circleCode').val());
    var circleName = $.trim($('#circleName').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveCircle",
        data: '{"circleCode":"' + circleCode + '","circleName":"' + circleName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}