﻿$(document).ready(function () {

    $("#editDivisionBtn").on('click', function () {
        var a;
        var b;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "DBAMethods.asmx/DisplayCircleName",
            data: "{}",
            dataType: "json",
            success: function (data) {
                a = data.d;
                editDivisionDesign(a);
            },
            error: function (result) {
                alert("Error");
            }
        });      
    });

    $("#circleNameForDivisionSelect").on('change', function () {
        
    });

    $('#updateDivisonbutton').on('click', function () {
        $("#preloader").show();
        $("#status").show();
        //alert("aditya");
        var circleGuid = $("#circleNameForDivisionSelect").val();
        var divisionGuid = $("#divisionNameSelect").val();
        var editDivisonCode = $("#editDivisionCode").val();
        var editDivisonName = $("#editDivisionName").val();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/UpdateDivisionData",
            data: '{"circleGuid":"' + circleGuid + '","divisionGuid":"' + divisionGuid + '","editDivisonCode":"' + editDivisonCode + '","editDivisonName":"' + editDivisonName + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                var str = response.d;
                if (str == true) {
                    bootbox.alert("Division updated successfully");
                }
                else {
                    bootbox.alert("Division cannot be updated.");
                }
            },

            failure: function (msg) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                alert(msg);
            }
        });
    });

});

editDivisionDesign = function (a) {
    $("#popupModalEditDivison .modal-body").empty();
    var html = "<div class=\"container-fluid col-md-12\">";
    html += "<div class=\"row\">";
    html += "<div class=\"row\">";
    html += "<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">";
    html += "<div class=\"panel-body\">";
    html += "<div class=\"text-center\">";
    html += "<img src=\"Images/add-circle.png\" class=\"login\" height=\"70\" />";
    html += "<h2 class=\"text-center\">Edit Division</h2>";
    html += "<div class=\"panel-body\">";
    html += "<form id=\"popupFormEditDivision\" class=\"form form-horizontal\" method=\"post\">";
    html += "<fieldset>";
    html += "<div class=\"form-group\">";
    html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    html += "<label for=\"circleCodeForDivision\" id=\"EditCircleCodeForDivisionSelectLabel\" class=\"control-label\">Select Circle Code</label>";
    html += "</div>";
    html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    html += "<select id=\"circleNameForDivisionSelect\" name=\"circleNameForDivision\">";
    html += "<option value='" + 0 + "'>" + "Select Circle" + "</option>";
    for (var i = 1; i < a.length;) {        
        html += "<option value='" + a[i - 1] + "'>";
        html += a[i];
        html += "</option>";
        i = i + 3;
    }
    html += "</select>";
    html += "</div>";
    html += "</div>";
    html += "<div class=\"form-group\">";
    html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    html += "<label for=\"divisionCodeSelect\" id=\"EditDivisionCodeSelectLabel\" class=\"control-label\">Select Division Code</label>";
    html += "</div>";
    html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    html += "<select id=\"divisionNameSelect\" name=\"divisionName\">";
    html += "<option value='" + 0 + "'>" + "Select Division" + "</option>";
    html += "</select>";
    html += "</div>";
    html += "</div>";   
    html += "<div class=\"form-group\">";
    html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    html += "<label for=\"divisionCode\" id=\"EditDivisionCodeLabel\" class=\"control-label\">Update Division Code</label>";
    html += "</div>";
    html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    html += "<input id=\"editDivisionCode\" name=\"divisionCode\" placeholder=\"Division Code\" class=\"form-control\" />";
    html += "<span class=\"input-group-btn\" style=\"width:0px;\"></span>";
    html += "</div>";
    html += "</div>";
    html += "<div class=\"form-group\">";
    html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    html += "<label for=\"divisionName\" id=\"EditDivisionNameLabel\" class=\"control-label\">Update Division Name</label>";
    html += "</div>";
    html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    html += "<input id=\"editDivisionName\" name=\"divisionName\" placeholder=\"Division Name\" class=\"form-control\" />";
    html += "<span class=\"input-group-btn\" style=\"width:0px;\"></span>";
    html += "</div>";
    html += "</div>";
    html += "</fieldset>";
    html += "</form>";
    html += "</div>";
    html += "</div>";
    html += "</div>";
    html += "</div>";
    html += "</div>";
    html += "</div>";
    html += "</div>";
    html += "</div>";
    $("#popupModalEditDivison .modal-body").append(html);//.html('<div class=\"descp\"></div>');
    $('#popupModalEditDivison').modal('show');

    $('#circleNameForDivisionSelect').on('change', function () {
        $('#divisionNameSelect').empty();
        var selectedCircle = $(this).val();        
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "DBAMethods.asmx/DisplayDivisionName",
            data: '{"circleGuid":"' + selectedCircle + '"}',
            dataType: "json",
            success: function (data) {
                b = data.d;
                $('#divisionNameSelect').append("<option value='" + 0 + "'>" + "Select Division" + "</option>");
                for (var i = 0; i < b.length;) {
                    
                    $('#divisionNameSelect').append('<option value="' + b[i] + '">' + b[i+1] + '</option>');                   
                    i = i + 3;                   
                }       

            },
            error: function (result) {
                alert("Error");
            }
        });
    });

    $('#divisionNameSelect').on('change', function () {
        var selectedDivision = $(this).val();
        for (var i = 0; i < b.length;) {
            if (b[i] == selectedDivision)
            {
                $('#editDivisionCode').val(b[i + 1]);
                $('#editDivisionName').val(b[i + 2]);
            }
            i = i + 3;
        }
    });
}