﻿$(document).ready(function () {
    var selectedDivision;
    var selectedDivisionText;
    var selectedSchemeText;
    var selectedAssetTypeText;

    showDivisionOptions('divisionList', getDivisions());

    var table = $('table#assetListForDBA').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        //stateSave: true,
        //destroy: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [4],
                visible: false,
                orderable: false,
                //searchable: false
            },
            {
                targets: [5],
                orderable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [6],
                visible: false,
                orderable: false,
                //searchable: false
            },
            {
                targets: [7],
                orderable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [8],
                visible: false,
                orderable: false,
                //searchable: false
            },
            {
                targets: [9],
                orderable: false,
            },
            {
                targets: [10],
                orderable: false,
            },
            {
                targets: [11],
                orderable: false,
            },
            {
                targets: [12],
                orderable: false
                //searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewAssetListBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editAssetBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteAssetBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>'
            }
        ],
        order: [[2, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Create New Asset',
                className: 'btn-success addNewAssetBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();
                    window.location = 'CreateAsset.aspx';
                    e.stopPropagation();
                }
            }
        ]
    });

    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    //Scheme List
    $('#schemeList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select Scheme--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$(element).closest('.multiselect').valid();

            if (checked === true) {
                var selectedSchemeGuid = $(element).val();
                var selectedSchemeText = $(element).text();

                if (selectedSchemeGuid === '') {
                    table.column(6).search('').draw();
                } else {
                    var selectedSchemeTextSplit = multiSplit(selectedSchemeText, [' (', ')']);

                    var val = $.fn.dataTable.util.escapeRegex(selectedSchemeTextSplit[1]);

                    table.column(6).search(val ? '^' + val + '$' : '', true, false).draw();
                }

                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>',
        }
    });

    //Division List
    $('#divisionList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select Division--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$(element).closest('.multiselect').valid();

            if (checked === true) {
                var selectedDivisionGuid = $(element).val();
                var selectedDivisionText = $(element).text();

                if (selectedDivisionGuid === '') {
                    $('#schemeList').multiselect('select', '', true);

                    //$('#schemeList').multiselect('refresh');

                    showSchemeOptionsToDbaAssets('schemeList', []);

                    table.column(4).search('').draw();
                } else {
                    showSchemeOptionsToDbaAssets('schemeList', getSchemes(selectedDivisionGuid));

                    var selectedDivisionTextSplit = multiSplit(selectedDivisionText, [' (', ')']);

                    val = $.fn.dataTable.util.escapeRegex(selectedDivisionTextSplit[1]);

                    table.column(4).search(val ? '^' + val + '$' : '', true, false).draw();
                }

                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    //AssetType List
    $('#assetTypeList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select AssetType--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified text-left" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$(element).closest('.multiselect').valid();

            if (checked === true) {
                var selectedAssetTypeGuid = $(element).val();
                var selectedAssetTypeText = $(element).text();

                if (selectedAssetTypeGuid === '') {
                    table.column(8).search('').draw();
                } else {
                    var selectedAssetTypeTextSplit = multiSplit(selectedAssetTypeText, [' (', ')']);

                    var val = $.fn.dataTable.util.escapeRegex(selectedAssetTypeTextSplit[1]);

                    table.column(8).search(val ? '^' + val + '$' : '', true, false).draw();
                }

                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });



    //$('#divisionList').on('change', function () {
    //    $('#schemeSelect').empty();
    //    selectedDivision = $(this).val();

    //    if (selectedDivision === "") {
    //        $('#schemeSelect').append('<option value="">' + '--Select Scheme--' + '</option>');
    //    } else {
    //        $.ajax({
    //            type: "POST",
    //            contentType: "application/json; charset=utf-8",
    //            url: "DBAAssets.aspx/DisplaySchemeNameForDivisionsForAsset",
    //            data: '{"divisionGuid":"' + selectedDivision + '"}',
    //            dataType: "json",
    //            success: function (data) {
    //                b = data.d;
    //                $('#schemeSelect').append("<option value='" + 0 + "'>" + "--Select Scheme--" + "</option>");
    //                for (var i = 0; i < b.length; i++) {
    //                    $('#schemeSelect').append('<option value="' + b[i].SchemeGuid + '">' + b[i].SchemeCode + "-" + b[i].SchemeName + '</option>');

    //                }

    //            },
    //            error: function (result) {
    //                alert("Error");
    //            }
    //        });
    //    }


    //    selectedDivisionText = $('#divisionList option:selected').text().split("-");

    //    var val = $.fn.dataTable.util.escapeRegex(
    //                       selectedDivisionText[0]
    //                   );

    //    table.column(4).search(val ? '^' + val + '$' : '', true, false).draw();
    //});

    //$('#schemeSelect').on('change', function (e) {
    //    e.preventDefault();
    //    selectedSchemeText = $('#schemeSelect option:selected').text().split("-");
    //    var val = $.fn.dataTable.util.escapeRegex(
    //                       selectedSchemeText[0]
    //                   );

    //    table.column(6).search(val ? '^' + val + '$' : '', true, false).draw();

    //});

    //$('#assetTypeList').on('change', function (e) {
    //    e.preventDefault();

    //    selectedAssetTypeText = $('#assetTypeList option:selected').text();

    //    if (selectedAssetTypeText === "--Select Asset Type--") {
    //        table.column(6).search(val ? '^' + '' + '$' : '', true, false).draw();
    //    } else {
    //        selectedAssetTypeTextToBeSearched = selectedAssetTypeText.split("-");

    //        var val = $.fn.dataTable.util.escapeRegex(
    //                       selectedAssetTypeTextToBeSearched[0]
    //                   );
    //        table.column(8).search(val ? '^' + val + '$' : '', true, false).draw();
    //    }

    //});

    $('#dpDateOfInitiationYear').datepicker({
        format: "yyyy",
        minView: 2,
        startView: 2,
        minViewMode: "years",
        maxViewMode: 2,//2-years
    }).on('changeDate', function (ev) {

        var searchInitiationYear = new Date(ev.date.valueOf()).toString('yyyy');

        var val = $.fn.dataTable.util.escapeRegex(searchInitiationYear);

        table.column(10).search(val ? '^' + val + '$' : '', true, false).draw();

        $(this).blur();
        $(this).datepicker('hide');
    });

    $('#dpDateOfCompletionYear').datepicker({
        format: "yyyy",
        minView: 2,
        startView: 2,
        minViewMode: "years",
        maxViewMode: 2,//2-years
    }).on('changeDate', function (ev) {

        var searchCompletionYear = new Date(ev.date.valueOf()).toString('yyyy');

        var val = $.fn.dataTable.util.escapeRegex(searchCompletionYear);

        table.column(11).search(val ? '^' + val + '$' : '', true, false).draw();

        $(this).blur();
        $(this).datepicker('hide');
    });

    $(document).on('click', '#assetListForDBA a.viewAssetListBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        var assetData = getAssetModalDetails(table.row(selectedRow).data()[1], table.row(selectedRow).data()[8]);

        $("#viewAssetPopupModal .modal-body").append(assetData);

        $('#viewAssetPopupModal').modal('show');

    });

    $(document).on('click', '#assetListForDBA a.editAssetBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        var sessionCreated = setAssetSessionParameters(table.row(selectedRow).data()[1], table.row(selectedRow).data()[8]);

        if (sessionCreated === true) {
            window.location = 'EditAsset.aspx';
        }

    });

    $(document).on('click', '#assetListForDBA a.printAssetBtn', function (e) {

        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        var assetData = getAssetModalDetails(table.row(selectedRow).data()[1], table.row(selectedRow).data()[8]);

        var myWindow = window.open("");

        myWindow.document.write(assetData);

        myWindow.print();

    });

    $('#closeBtn').on('click', function () {
        $("#viewAssetPopupModal .modal-body").empty();
    });

});

setAssetSessionParameters = function (assetGuid, assetTypeCode) {

    $("#preloader").show();
    $("#status").show();

    var result = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAAssets.aspx/SetAssetSessionParameters",
        data: '{"assetGuid":"' + assetGuid + '","assetTypeCode":"' + assetTypeCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            result = true;
            str = response.d;
            //str = str1;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });

    return result;
}

getAssetModalDetails = function (assetGuid, assetTypeCode) {

    //$("#preloader").show();
    //$("#status").show();
    var str;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAAssets.aspx/ViewAssetData",
        data: '{"assetGuid":"' + assetGuid + '","assetTypeCode":"' + assetTypeCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //str = str1;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return str;
}

showSchemeOptionsToDbaAssets = function (id, schemeArray) {

    var options = [];

    if (schemeArray.length > 0) {
        options.push({
            label: '--Select Scheme--',
            value: '',
            selected: true
        });

        for (var i = 0; i < schemeArray.length ; i++) {
            options.push({
                label: schemeArray[i].SchemeName + ' (' + schemeArray[i].SchemeCode + ')',
                value: schemeArray[i].SchemeGuid,
            });
        }
    }

    $('#' + id).multiselect('dataprovider', options);

    $('#' + id).multiselect('refresh');

}