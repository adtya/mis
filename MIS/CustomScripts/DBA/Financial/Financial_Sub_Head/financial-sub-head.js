﻿$(document).ready(function () {

    $('#financialSubHeadListMenuBtn').on('click', function (e) {
        e.preventDefault();
        //$.ajax({
        //    type: "Post",
        //    async: false,
        //    url: "DBAMethods.asmx/GetFinancialSubHeads",
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    success: function (response) {
        //        createFinancialSubHeadPopupForm(response.d);
        //    },
        //    failure: function (result) {
        //        bootbox.alert("Error");
        //    }
        //});
        createFinancialSubHeadPopupForm(getFinancialSubHeads());
        e.stopPropagation();
    });

    $('#financialSubHeadPopupModal').on('click', '.modal-footer button#financialSubHeadPopupModalCloseBtn', function (e) {
        e.preventDefault();

        location.reload(true);

        e.stopPropagation();
    });

});

createFinancialSubHeadPopupForm = function (financialSubHeadData) {
    $("#financialSubHeadPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Financial SubHeads</h2>';

    html += '<div class="panel-body">';
    html += '<form id="financialSubHeadPopupForm" name="financialSubHeadPopupForm" role="form" class="form form-horizontal" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    //if (financialSubHeadData.length > 0) {
    html += '<div class="table-responsive">';
    html += '<table id="financialSubHeadsList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'Financial SubHead Guid' + '</th>';
    html += '<th>' + 'SubHead Name' + '</th>';
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    for (var i = 0; i < financialSubHeadData.length; i++) {
        html += '<tr>';

        html += '<td>' + '' + '</td>';
        html += '<td>' + financialSubHeadData[i].FinancialSubHeadGuid + '</td>';
        html += '<td>' + financialSubHeadData[i].FinancialSubHeadName + '</td>';
        html += '<td>' + '<a href="#" class="viewFinancialSubHeadBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editFinancialSubHeadBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteFinancialSubHeadBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';

        html += '</tr>';
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';
    //} else {
    //    html += '<p>There is no WorkItem SubHead available to be displayed.</p>';
    //}

    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#financialSubHeadPopupModal .modal-body").append(html);

    //$('#workItemSubHeadPopupModal').modal('show');

    $('#financialSubHeadPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    financialSubHeadTableRelatedFunctions();

    $('#financialSubHeadPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            financialSubHeadName: {
                required: true,
                noSpace: true
            }
        },
        messages: {
            financialSubHeadName: {
                required: "Please Enter the SubHead Name.",
                noSpace: "SubHead Name cannot be empty."
            }
        },
        //errorPlacement: function (error, element) {

        //    switch (element.attr("name")) {
        //        case "dpProgressMonth":
        //            error.insertAfter($("#dpProgressMonth"));
        //            break;

        //        default:
        //            //nothing
        //    }
        //}
    });

}

financialSubHeadTableRelatedFunctions = function () {
    var financialSubHeadEditing = null;

    var financialSubHeadTable = $('#financialSubHeadPopupModal table#financialSubHeadsList').DataTable({
        //var financialSubHeadTable = $('#financialSubHeadsList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        //destroy: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false,
                searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewFinancialSubHeadBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editFinancialSubHeadBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteFinancialSubHeadBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>'
            }
        ],
        order: [[2, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Create New Financial SubHead',
                className: 'btn-success addNewFinancialSubHeadBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewFinancialSubHead();

                    e.stopPropagation();
                }
            }
        ]
    });

    financialSubHeadTable.on('order.dt search.dt', function () {
        financialSubHeadTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addFinancialSubHeadBtn = financialSubHeadTable.button(['.addNewFinancialSubHeadBtn']);

    addNewFinancialSubHead = function () {

        if (financialSubHeadEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreFinancialSubHeadRowData(financialSubHeadTable, financialSubHeadEditing);
            financialSubHeadEditing = null;
            addFinancialSubHeadBtn.enable();
        }

        var newFinancialSubHeadRow = financialSubHeadTable.row.add([
            '',
            '',
            '',
            ''
        ]).draw(false);

        var newFinancialSubHeadRowNode = newFinancialSubHeadRow.node();

        addNewFinancialSubHeadRow(financialSubHeadTable, newFinancialSubHeadRowNode);

        financialSubHeadEditing = newFinancialSubHeadRowNode;

        addFinancialSubHeadBtn.disable();

        $('#financialSubHeadPopupModal input#financialSubHeadName').focus();

        $(newFinancialSubHeadRowNode).css('color', 'red').animate({ color: 'black' });
    };

    $('#financialSubHeadPopupModal').on('click', '#financialSubHeadsList a.saveNewFinancialSubHeadBtn', function (e) {
        e.preventDefault();

        if ($("#financialSubHeadPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var checkExistence = checkFinancialSubHeadExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a Financial SubHead with this name.");
            } else if (checkExistence == false) {
                var financialSubHeadSavedGuid = saveFinancialSubHead();

                if (jQuery.Guid.IsValid(financialSubHeadSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(financialSubHeadSavedGuid.toUpperCase())) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your Financial SubHead is created successfully.", function () {
                        if (financialSubHeadEditing !== null && financialSubHeadEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            saveNewFinancialSubHeadRow(financialSubHeadTable, financialSubHeadEditing, financialSubHeadSavedGuid);
                            financialSubHeadEditing = null;
                            addFinancialSubHeadBtn.enable();
                        }
                    });
                } else {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }
            }

        }

    });

    $('#financialSubHeadPopupModal').on('click', '#financialSubHeadsList a.cancelNewFinancialSubHeadBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (financialSubHeadEditing !== null && financialSubHeadEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            removeNewFinancialSubHeadRowCreatedOnCancel(financialSubHeadTable, selectedRow);
            financialSubHeadEditing = null;
            addFinancialSubHeadBtn.enable();
        }

    });

    $('#financialSubHeadPopupModal').on('click', '#financialSubHeadsList a.viewFinancialSubHeadBtn', function (e) {
        e.preventDefault();

        ///* Get the row as a parent of the link that was clicked on */
        //var selectedRow = $(this).parents('tr')[0];

        ////var selectedRowIndex = unitTypeTable.row($(this).parents('tr')).index();

        //if (workItemEditing !== null && workItemEditing != selectedRow) {
        //    /* A different row is being edited - the edit should be cancelled and this row edited */
        //    restoreWorkItemRowData(unitTypeTable, workItemEditing);
        //    editWorkItemRow(unitTypeTable, selectedRow);
        //    workItemEditing = selectedRow;
        //    addWorkItemBtn.enable();
        //}
        //    //else if (nEditing == selectedRow && this.innerHTML == "Save") {
        //    //    /* This row is being edited and should be saved */
        //    //    saveRow(oTable, nEditing);
        //    //    nEditing = null;
        //    //    createBtnClicked = 0;
        //    //}
        //else {
        //    /* No row currently being edited */
        //    editWorkItemRow(unitTypeTable, selectedRow);
        //    workItemEditing = selectedRow;
        //    addWorkItemBtn.enable();
        //}
    });

    $('#financialSubHeadPopupModal').on('click', '#financialSubHeadsList a.editFinancialSubHeadBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (financialSubHeadEditing !== null && financialSubHeadEditing != selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreFinancialSubHeadRowData(financialSubHeadTable, financialSubHeadEditing);
            editFinancialSubHeadRow(financialSubHeadTable, selectedRow);
            financialSubHeadEditing = selectedRow;
            addFinancialSubHeadBtn.enable();
            //$('#workItemSubHeadPopupModal input#workItemSubHeadName').focus();

            var el = $('#financialSubHeadPopupModal input#financialSubHeadName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);
        } else {
            /* No row currently being edited */
            editFinancialSubHeadRow(financialSubHeadTable, selectedRow);
            financialSubHeadEditing = selectedRow;
            addFinancialSubHeadBtn.enable();
            //$('#workItemSubHeadPopupModal input#workItemSubHeadName').focus();

            var el = $('#financialSubHeadPopupModal input#financialSubHeadName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);
        }
    });

    $('#financialSubHeadPopupModal').on('click', '#financialSubHeadsList a.updateFinancialSubHeadBtn', function (e) {
        e.preventDefault();

        if ($("#financialSubHeadPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var checkExistence = checkFinancialSubHeadExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a Financial SubHead with this name.");
            } else if (checkExistence == false) {
                var financialSubHeadUpdated = updateFinancialSubHead(financialSubHeadTable.row(selectedRow).data()[1]);

                if (financialSubHeadUpdated == 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    //$('#workItemSubHeadName').val('');
                    bootbox.alert("Your Financial SubHead is updated successfully.", function () {
                        if (financialSubHeadEditing !== null && financialSubHeadEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            updateFinancialSubHeadRow(financialSubHeadTable, financialSubHeadEditing);
                            financialSubHeadEditing = null;
                            addFinancialSubHeadBtn.enable();
                        }
                    });
                } else if (financialSubHeadUpdated == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
                }
            }

        }

    });

    $('#financialSubHeadPopupModal').on('click', '#financialSubHeadsList a.cancelFinancialSubHeadEditBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (financialSubHeadEditing !== null && financialSubHeadEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreFinancialSubHeadRowData(financialSubHeadTable, financialSubHeadEditing);
            financialSubHeadEditing = null;
            addFinancialSubHeadBtn.enable();
        }

    });

    $('#financialSubHeadPopupModal').on('click', '#financialSubHeadsList a.deleteFinancialSubHeadBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        var financialSubHeadDeleted = deleteFinancialSubHead(financialSubHeadTable.row(selectedRow).data()[1]);

        if (financialSubHeadDeleted == 1) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Your Financial SubHead is deleted successfully.", function () {
                financialSubHeadTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (financialSubHeadDeleted == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
        }

    });

}

addNewFinancialSubHeadRow = function (financialSubHeadTable, selectedRow) {
    var selectedRowData = financialSubHeadTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //financialSubHeadTable.order([2, 'asc']).draw();

    financialSubHeadTable.column('1:visible').order('asc').draw();

    financialSubHeadTable.on('order.dt search.dt', function () {

        var x = financialSubHeadTable.order();

        if (x[0][1] == "asc") {
            var a = financialSubHeadTable.column(0, { search: 'applied', order: 'applied' }).nodes();
            for (i = 1; i < a.length; i++) {
                a[i].innerHTML = i;
            }
        } else {
            financialSubHeadTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }
        availableTds[0].innerHTML = '*';
    }).draw(false);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[0].innerHTML = '*';
    availableTds[1].innerHTML = '<input type="text" id="financialSubHeadName" name="financialSubHeadName" placeholder="Financial SubHead Name" class="form-control text-capitalize" value="' + selectedRowData[2] + '" onfocus="financialSubHeadNameFocusFunction()" onblur="financialSubHeadNameBlurFunction()" style="width:100%;">';//Financial SubHead
    availableTds[2].innerHTML = '<a href="#" class="saveNewFinancialSubHeadBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelNewFinancialSubHeadBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

saveNewFinancialSubHeadRow = function (financialSubHeadTable, selectedRow, financialSubHeadSavedGuid) {

    var selectedRowData = financialSubHeadTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    selectedRowData[1] = financialSubHeadSavedGuid;
    selectedRowData[2] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
    selectedRowData[3] = '<a href="#" class="viewFinancialSubHeadBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editFinancialSubHeadBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteFinancialSubHeadBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    financialSubHeadTable.row(selectedRow).data(selectedRowData);

    financialSubHeadTable.on('order.dt search.dt', function () {
        financialSubHeadTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

removeNewFinancialSubHeadRowCreatedOnCancel = function (financialSubHeadTable, selectedRow) {

    financialSubHeadTable
        .row(selectedRow)
        .remove()
        .draw();

    financialSubHeadTable.on('order.dt search.dt', function () {
        financialSubHeadTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

editFinancialSubHeadRow = function (financialSubHeadTable, selectedRow) {
    var selectedRowData = financialSubHeadTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[1].innerHTML = '<input type="text" id="financialSubHeadName" name="financialSubHeadName" placeholder="Financial SubHead Name" class="form-control text-capitalize" value="' + selectedRowData[2] + '" onfocus="financialSubHeadNameFocusFunction()" onblur="financialSubHeadNameBlurFunction()" style="width:100%;">';//Financial SubHead
    availableTds[2].innerHTML = '<a href="#" class="updateFinancialSubHeadBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelFinancialSubHeadEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

updateFinancialSubHeadRow = function (financialSubHeadTable, selectedRow) {
    var selectedRowData = financialSubHeadTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[2] = capitalizeFirstAllWords($.trim(availableInputs[0].value));

    financialSubHeadTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

    //financialSubHeadTable.draw();
}

restoreFinancialSubHeadRowData = function (financialSubHeadTable, previousRow) {
    var previousRowData = financialSubHeadTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewFinancialSubHeadRowCreatedOnCancel(financialSubHeadTable, previousRow);
    } else {
        financialSubHeadTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;

        //financialSubHeadTable.cell(previousRowIndex, 0).innerHTML = previousRowSerialNum;

        //financialSubHeadTable.draw();
    }

}






financialSubHeadNameFocusFunction = function () {
    // Focus = Changes the background color of input to yellow
    //document.getElementById("workItemSubHeadName").style.background = "yellow";
    $('#financialSubHeadPopupModal input#financialSubHeadName').css('background-color', 'yellow');
}

financialSubHeadNameBlurFunction = function () {
    // No focus = Changes the background color of input to nothing
    //document.getElementById("workItemSubHeadName").style.background = "";
    $('#financialSubHeadPopupModal input#financialSubHeadName').css('background-color', '');
}






checkFinancialSubHeadExistence = function () {
    var financialSubHeadName = $.trim($('#financialSubHeadPopupModal input#financialSubHeadName').val());

    var retValue = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckFinancialSubHeadExistence",
        data: '{"financialSubHeadName":"' + financialSubHeadName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveFinancialSubHead = function () {
    var financialSubHeadName = $.trim($('#financialSubHeadPopupModal input#financialSubHeadName').val());

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveFinancialSubHead",
        data: '{"financialSubHeadName":"' + financialSubHeadName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updateFinancialSubHead = function (financialSubHeadGuid) {
    var financialSubHeadName = $.trim($('#financialSubHeadPopupModal input#financialSubHeadName').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateFinancialSubHead",
        data: '{"financialSubHeadName":"' + financialSubHeadName + '","financialSubHeadGuid":"' + financialSubHeadGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteFinancialSubHead = function (financialSubHeadGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/DeleteFinancialSubHead",
        data: '{"financialSubHeadGuid":"' + financialSubHeadGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}