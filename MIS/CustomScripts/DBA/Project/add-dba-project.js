﻿var arrOfficersEngaged = [];
var arrPmcSiteEngineers = [];
var arrSubChainages = [];
var arrWorkItems = [];
var arrFinancialHeadBudgets = [];
var arrWorkItemsToDisplay = [];
var arrFinancialSubHeadsToDisplay = [];
var addedFinancialSubHeadsTable;
var projectValue;

//document.onkeydown = function (e) {
//    stopDefaultBackspaceBehaviour(e);
//}

//document.onkeypress = function (e) {
//    stopDefaultBackspaceBehaviour(e);
//}

//function stopDefaultBackspaceBehaviour(event) {
//    var event = event || window.event;
//    if (event.keyCode == 8) {
//        var elements = "HTML, BODY, TABLE, TBODY, TR, TD, DIV";
//        var d = event.srcElement || event.target;
//        var regex = new RegExp(d.tagName.toUpperCase());
//        if (d.contentEditable != 'true') { //it's not REALLY true, checking the boolean value (!== true) always passes, so we can use != 'true' rather than !== true/
//            if (regex.test(elements)) {
//                event.preventDefault ? event.preventDefault() : event.returnValue = false;
//            }
//        }
//    }
//}

$(document).ready(function () {

    var cfgCulture = 'en-IN';
    $.preferCulture(cfgCulture);
    //$.preferCulture("en-IN");

    showDistrictOptions('districtList', getDistricts());

    showDivisionOptions('divisionList', getDivisions());

    var current = 1;

    $('#projectValue').maskMoney();
    $('#subChainageValue').maskMoney();

    widget = $('.step');

    btnNext = $('.next');
    btnBack = $('.back');
    btnSkip = $('.skip');

    btnSubmit = $('.submit');

    //Init Buttons and UI
    widget.not(':eq(0)').hide();
    hideButtons(current);
    setProgress(current);

    // Next button click action
    btnNext.click(function () {
        if (current < widget.length) {

            //Step 1
            if (current == 1) {
                // Check validation
                //if ($('#addDbaProjectForm').valid()) {
                if ($('select[name="districtList"], select[name="divisionList"], input[name="projectName"], input[name="subProjectName"], input[name="chainageOfProject"], input[name="dateOfStartOfProject"], input[name="expectedDateOfCompletionOfProject"], input[name="projectValue"]').valid()) {
                    widget.show();
                    widget.not(':eq(' + (current++) + ')').hide();
                    setProgress(current);
                }
            }

                //Step 2
            else if (current == 2) {
                if (arrOfficersEngaged.length > 0) {
                    // Check validation
                    //if ($('#addDbaProjectForm').valid()) {
                    widget.show();
                    widget.not(':eq(' + (current++) + ')').hide();
                    setProgress(current);
                    //}
                } else {
                    bootbox.alert("Officers List cannot be Empty.");
                }
            }

                //Step 3
            else if (current == 3) {
                if (arrPmcSiteEngineers.length > 0) {
                    showSioUserOptionsToAddDbaProjects('sioUsersList', getUsersBasedOnRoleName());

                    arrWorkItemsToDisplay = getWorkItems();
                    showWorkItemOptionsToAddDbaProjects('workItemList', arrWorkItemsToDisplay);

                    // Check validation
                    //if ($('#addDbaProjectForm').valid()) {
                    widget.show();
                    widget.not(':eq(' + (current++) + ')').hide();
                    setProgress(current);
                    //}
                } else {
                    bootbox.alert("Site Engineers List cannot be Empty.");
                }
            }

                //Step 4
            else if (current == 4) {
                if (arrSubChainages.length > 0) {
                    // Check validation
                    //if ($('#addDbaProjectForm').valid()) {
                    widget.show();
                    widget.not(':eq(' + (current++) + ')').hide();
                    setProgress(current);
                    //}
                } else {
                    bootbox.alert("Sub Chainages List cannot be Empty.");
                }
            }

                //Step 5
            else if (current == 5) {
                // Check validation
                //if ($('#addDbaProjectForm').valid()) {
                if ($('input[name="landAcquisitionRadio"]').valid()) {

                    projectValue = $('#projectValue').val();
                    var newchar = '';
                    projectValue = projectValue.split(',').join(newchar);
                    //projectValue = projectValue.replace(/,/g , newchar);
                    projectValue = Math.abs(parseFloat(projectValue));

                    $('#totalBudgetTable').empty();
                    $('#totalBudgetTable').append('Rs. ' + $.format(parseFloat(projectValue).toFixed(2)));

                    $('#totalValueAssignedTable').empty();
                    $('#totalValueAssignedTable').append('Rs. ' + $.format(parseFloat(0.00).toFixed(2)));

                    $('#remainingBudgetTable').empty();
                    $('#remainingBudgetTable').append('Rs. ' + $.format(parseFloat(projectValue).toFixed(2)));

                    $('#totalValueAssignedTableFoot').empty();
                    $('#totalValueAssignedTableFoot').append('Rs. ' + $.format(parseFloat(0.00).toFixed(2)));

                    arrFinancialSubHeadsToDisplay = getFinancialSubHeads();

                    widget.show();
                    widget.not(':eq(' + (current++) + ')').hide();
                    setProgress(current);
                }
            }

                //Step 6
            else if (current == 6) {
                if (arrFinancialHeadBudgets.length > 0) {
                    // Check validation
                    //if ($('#addDbaProjectForm').valid()) {
                    widget.show();
                    widget.not(':eq(' + (current++) + ')').hide();
                    setProgress(current);
                    //}
                } else {
                    bootbox.alert("Financial Head Budgets List cannot be Empty.");
                }
            }

        }
        hideButtons(current);
    });

    // Back button click action
    btnBack.click(function () {
        if (current > 1) {
            current = current - 2;
            if (current < widget.length) {
                widget.show();
                widget.not(':eq(' + (current++) + ')').hide();
                setProgress(current);
            }
        }
        hideButtons(current);
    });

    // Skip button click action
    btnSkip.click(function () {
        if (current < widget.length) {

            //Step 2
            if (current == 2) {
                while (arrOfficersEngaged.length > 0) {
                    arrOfficersEngaged.pop();
                }

                displayEngagedOfficersList('officersListDisplay', arrOfficersEngaged);

                widget.show();
                widget.not(':eq(' + (current++) + ')').hide();
                setProgress(current);
            }

                //Step 3
            else if (current == 3) {
                while (arrPmcSiteEngineers.length > 0) {
                    arrPmcSiteEngineers.pop();
                }

                displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);

                showSioUserOptionsToAddDbaProjects('sioUsersList', getUsersBasedOnRoleName());

                arrWorkItemsToDisplay = getWorkItems();
                showWorkItemOptionsToAddDbaProjects('workItemList', arrWorkItemsToDisplay);

                widget.show();
                widget.not(':eq(' + (current++) + ')').hide();
                setProgress(current);
            }

            ////Step 4
            //else if (current == 4) {
            //    if (arrSubChainages.length > 0) {
            //        arrSubChainages.pop();
            //    }
            //    widget.show();
            //    widget.not(':eq(' + (current++) + ')').hide();
            //    setProgress(current);
            //}

        }

        hideButtons(current);
    });

    // Submit button click action
    btnSubmit.click(function () {
        //if ($('#addDbaProjectForm').valid()) {
            $("#preloader").show();
            $("#status").show();

            var dbaProjectSaved = saveProject();

            if (dbaProjectSaved === 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("Your Project is created successfully.", function () {
                    window.location = 'DBAProjects.aspx';
                });
            } else if (dbaProjectSaved === 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("Your data cannot be saved.\nPlease contact your administrator.", function () {
                    window.location = 'DBAProjects.aspx';
                });
            }
        //}
    });

    //District List
    $('#districtList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select District--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            $(element).closest('.multiselect').valid();
            if (checked === true) {
                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    //Division List
    $('#divisionList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select Division--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            $(element).closest('.multiselect').valid();
            if (checked === true) {
                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    //SIO List
    $('#sioUsersList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select SIO--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            $(element).closest('.multiselect').valid();
            if (checked === true) {
                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    //WorkItem List
    $('#workItemList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select WorkItem--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            $(element).closest('.multiselect').valid();
            if (checked === true) {
                displayWorkItemValueDiv(element.val());
                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#addDbaProjectForm').validate({ // initialize plugin
        //ignore: ":not(:visible)",
        ignore: ':hidden:not(".multiselect")',
        rules: {
            districtList: "required",
            divisionList: "required",
            projectName: {
                required: true,
                noSpace: true
            },
            subProjectName: {
                required: true,
                noSpace: true
            },
            chainageOfProject: {
                required: true,
                noSpace: true
            },
            dateOfStartOfProject: {
                required: true,
                dateITA: true
            },
            expectedDateOfCompletionOfProject: {
                required: true,
                dateITA: true
            },
            projectValue: "required",

            officersEngaged: {
                required: true,
                noSpace: true
            },

            pmcSiteEngineers: {
                required: true,
                noSpace: true
            },

            subChainageName: {
                required: true,
                noSpace: true
            },
            sioUsersList: "required",
            subChainageValue: "required",
            administrativeApprovalReference: {
                required: true,
                noSpace: true
            },
            technicalSanctionReference: {
                required: true,
                noSpace: true
            },
            contractorName: {
                required: true,
                noSpace: true
            },
            workOrderReference: {
                required: true,
                noSpace: true
            },
            subChainageStartingDate: {
                required: true,
                dateITA: true
            },
            subChainageExpectedCompletionDate: {
                required: true,
                dateITA: true
            },
            typeOfWorks: {
                required: true,
                noSpace: true
            },
            workItemValue: {
                required: true,
                number: true
            },

            landAcquisitionRadio: "required"
        },
        messages: {
            districtList: {
                required: "Please Enter Project District."
            },
            divisionList: {
                required: "Please Enter Project Division."
            },
            projectName: {
                required: "Please Enter Project Name.",
                noSpace: "Project Name cannot be blank."
            },
            subProjectName: {
                required: "Please Enter Sub-Project Name.",
                noSpace: "Sub-Project Name cannot be blank."
            },
            chainageOfProject: {
                required: "Please Enter Chainage of the Project.",
                noSpace: "Chainage of the Project cannot be blank."
            },
            dateOfStartOfProject: {
                required: "Please Enter the Starting Date of the Project.",
                dateITA: "Please Enter the Starting Date of the Project as: dd/mm/yyyy"
            },
            expectedDateOfCompletionOfProject: {
                required: "Please Enter the Expected Completion Date of the Project.",
                dateITA: "Please Enter the Expected Completion Date of the Project as: dd/mm/yyyy"
            },
            projectValue: {
                required: "Please Enter the Value of the Project."
            },

            officersEngaged: {
                required: "Please Enter the Officer Name.",
                noSpace: "Officer Name cannot be blank."
            },

            pmcSiteEngineers: {
                required: "Please Enter the Site Engineer Name.",
                noSpace: "Site Engineer Name cannot be blank."
            },

            subChainageName: {
                required: "Please Enter the Name of the Sub-Chainage.",
                noSpace: "Sub-Chainage Name cannot be blank."
            },
            sioUsersList: {
                required: "Please Enter the SIO for the Sub-Chainage."
            },
            subChainageValue: {
                required: "Please Enter the Value of the Sub-Chainage."
            },
            administrativeApprovalReference: {
                required: "Please Enter the AA Reference of the Sub-Chainage.",
                noSpace: "AA Reference cannot be blank."
            },
            technicalSanctionReference: {
                required: "Please Enter the TS Reference of the Sub-Chainage.",
                noSpace: "TS Reference cannot be blank."
            },
            contractorName: {
                required: "Please Enter the Name of the Contractor of the Sub-Chainage.",
                noSpace: "Contractor Name cannot be blank."
            },
            workOrderReference: {
                required: "Please Enter the W.O. Reference of the Sub-Chainage.",
                noSpace: "W.O. Reference cannot be blank."
            },
            subChainageStartingDate: {
                required: "Please Enter the Starting Date of the Sub-Chainage.",
                dateITA: "Please Enter the Starting Date of the Sub-Chainage as: dd/mm/yyyy"
            },
            subChainageExpectedCompletionDate: {
                required: "Please Enter the Expected Completion Date of the Sub-Chainage.",
                dateITA: "Please Enter the Expected Completion Date of the Sub-Chainage as: dd/mm/yyyy"
            },
            typeOfWorks: {
                required: "Please Enter the Type of Work.",
                noSpace: "Type of Work cannot be blank."
            },
            workItemValue: {
                required: "Please Enter the WorkItem Value.",
                number: "Please Enter a Valid WorkItem Value."
            },

            landAcquisitionRadio: "Please Select the Land Acquisition Option."
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block small',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('multiselect')) {
                //error.insertAfter($('#' + element.attr('id')));
                //error.insertBefore(element);
                error.insertAfter(element.parent().find('.btn-group'));
                //$(this).parents("fieldset").find("*:not('legend')").toggle();
            } else {
                //element.closest('.form-group').append(error);
                error.insertAfter(element);
            }
        },
        submitHandler: function () {
            alert('valid form');
            return false;
        }
    });

    var fullDate = new Date();

    var twoDigitMonth = fullDate.getMonth() + 1 + '';

    if (twoDigitMonth.length == 1) {
        twoDigitMonth = "0" + twoDigitMonth;
    }

    var twoDigitDate = fullDate.getDate() + '';

    if (twoDigitDate.length == 1) {
        twoDigitDate = "0" + twoDigitDate;
    }

    var currentDate = twoDigitDate + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

    $('#dpDateOfStartOfProject').datepicker({
        format: "dd/mm/yyyy",
        startDate: currentDate.toString(),
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var projectExpectedCompletionDateStartDate = new Date(ev.date.valueOf());
        projectExpectedCompletionDateStartDate = projectExpectedCompletionDateStartDate.add(1).days();
        $('#dpExpectedDateOfCompletionOfProject').datepicker('setStartDate', projectExpectedCompletionDateStartDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpExpectedDateOfCompletionOfProject').datepicker('setStartDate', null);
    });

    $('#dpExpectedDateOfCompletionOfProject').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var projectStartDateEndDate = new Date(ev.date.valueOf());
        projectStartDateEndDate = projectStartDateEndDate.add(-1).days();
        $('#dpDateOfStartOfProject').datepicker('setEndDate', projectStartDateEndDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpDateOfStartOfProject').datepicker('setEndDate', null);
    });

    $('#dpSubChainageStartingDate').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var subChainageCompletionDateStartDate = new Date(ev.date.valueOf());
        subChainageCompletionDateStartDate = subChainageCompletionDateStartDate.add(1).days();
        $('#dpSubChainageExpectedCompletionDate').datepicker('setStartDate', subChainageCompletionDateStartDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpSubChainageExpectedCompletionDate').datepicker('setStartDate', null);
    });

    $('#dpSubChainageExpectedCompletionDate').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var subChainageStartDateEndDate = new Date(ev.date.valueOf());
        subChainageStartDateEndDate = subChainageStartDateEndDate.add(-1).days();
        $('#dpSubChainageStartingDate').datepicker('setEndDate', subChainageStartDateEndDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpSubChainageStartingDate').datepicker('setEndDate', null);
    });

    addedFinancialSubHeadsTable = $('table#addedFinancialSubHeadsTable').DataTable({
        //var workItemTable = $('#workItemsList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        //destroy: true,
        orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                targets: [1],
                //orderable: false
            },
            {
                className: 'dt-right',
                targets: [2],
                orderable: false
            },
            {
                targets: [3],
                orderable: false,
                searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewWorkItemBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>'
            }
        ],
        order: [[1, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Assign Value',
                className: 'btn-success assignValueToFinancialSubHeadBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    createAssignValueToFinancialSubHeadPopupForm();

                    e.stopPropagation();
                }
            }
        ]

    });

    addedFinancialSubHeadsTable.on('order.dt search.dt', function () {
        addedFinancialSubHeadsTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $('#addOfficersEngagedBtn').on('click', function (e) {

        //if ($('#addDbaProjectForm').valid()) {
        if ($('input[name="officersEngaged"]').valid()) {

            $('#officersListDisplay').removeClass('hidden');

            arrOfficersEngaged.push({
                fremaaOfficerName: $('#officersEngaged').val()
            });

            $('#officersEngaged').val('');

            displayEngagedOfficersList('officersListDisplay', arrOfficersEngaged);
        }

    });

    $('#addPmcSiteEngineersBtn').on('click', function (e) {

        //if ($('#addDbaProjectForm').valid()) {
        if ($('input[name="pmcSiteEngineers"]').valid()) {

            $('#pmcSiteEngineersListDisplay').removeClass('hidden');

            arrPmcSiteEngineers.push({
                siteEngineerName: $('#pmcSiteEngineers').val()
            });

            $('#pmcSiteEngineers').val('');

            displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);
        }

    });

    $('#addSubChainagesBtn').on('click', function (e) {

        //if ($('#addDbaProjectForm').valid()) {
        if ($('input[name="subChainageName"], select[name="sioUsersList"], input[name="subChainageValue"], input[name="administrativeApprovalReference"], input[name="technicalSanctionReference"], input[name="contractorName"], input[name="workOrderReference"], input[name="subChainageStartingDate"], input[name="subChainageExpectedCompletionDate"], input[name="typeOfWorks"]').valid()) {

            $('#subChainagesListDisplay').removeClass('hidden');

            var subChainageValue = $('#subChainageValue').val();
            var newchar = '';
            subChainageValue = subChainageValue.split(',').join(newchar);
            //subChainageValue = subChainageValue.replace(/,/g , newchar);
            subChainageValue = Math.abs(parseFloat(subChainageValue));

            if (arrWorkItems.length > 0) {

                arrSubChainages.push({
                    subChainageName: $('#subChainageName').val(),
                    subChainageSioGuid: $('#sioUsersList').val(),
                    subChainageValueInput: subChainageValue,
                    administrativeApprovalReference: $('#administrativeApprovalReference').val(),
                    technicalSanctionReference: $('#technicalSanctionReference').val(),
                    contractorName: $('#contractorName').val(),
                    workOrderReference: $('#workOrderReference').val(),
                    subChainageStartingDate: Date.parse($('#subChainageStartingDate').val()).toString('yyyy/MM/dd'),
                    subChainageExpectedCompletionDate: Date.parse($('#subChainageExpectedCompletionDate').val()).toString('yyyy/MM/dd'),
                    typeOfWorks: $('#typeOfWorks').val(),
                    workItemManager: JSON.parse(JSON.stringify(arrWorkItems))
                });

                $('#subChainageName').val('');
                $('#sioUsersList').val('');
                $('#subChainageValue').val('');
                $('#administrativeApprovalReference').val('');
                $('#technicalSanctionReference').val('');
                $('#contractorName').val('');
                $('#workOrderReference').val('');
                $('#subChainageStartingDate').val('');
                $('#subChainageExpectedCompletionDate').val('');
                $('#typeOfWorks').val('');

                arrWorkItems = [];
                displayWorkItemsList('workItemsListDisplay', arrWorkItems);
                $('#workItemsListDisplay').addClass('hidden');

                showWorkItemOptionsToAddDbaProjects('workItemList', arrWorkItemsToDisplay);

                displaySubChainagesList('subChainagesListDisplay', arrSubChainages);
            } else {
                bootbox.alert("Work Items List cannot be empty.");
            }

        }

    });

    $('#addWorkItemsBtn').on('click', function (e) {

        var workItemGuid = $('#workItemList').val();

        if (workItemGuid != "") {

            if ($('select[name="workItemList"], input[name="workItemValue"]').valid()) {

                $('#workItemsListDisplay').removeClass('hidden');

                arrWorkItems.push({
                    workItemGuid: workItemGuid,
                    workItemName: $('#workItemList option:selected').text(),
                    workItemTotalValue: $.trim($('#workItemValue').val())
                });

                $('#workItemValue').val('');

                displayWorkItemsList('workItemsListDisplay', arrWorkItems);

                removeWorkItemOptionsToAddDbaProjects('workItemList', arrWorkItemsToDisplay);

                $('#workItemValueDiv').addClass('hidden');
            }
        } else {
            bootbox.alert("Please Select WorkItem.");
        }


    });

});

// Hide buttons according to the current step
hideButtons = function (current) {
    var limit = parseInt(widget.length);

    $(".action").hide();

    if (current < limit) {
        btnNext.show();
    }

    if (current > 1) {
        btnBack.show();
    }

    if (current == limit) {
        btnNext.hide();
        btnSubmit.show();
    }

    if (current == 2 || current == 3) {
        btnSkip.show();
    }
}

// Change progress bar action
setProgress = function (currstep) {
    var percent = parseFloat(100 / widget.length) * currstep;
    percent = percent.toFixed();
    $(".progress-bar").css("width", percent + "%").html(percent + "%");
    $("#step-display").html("Step " + currstep + " of " + widget.length);
}

displayEngagedOfficersList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    var html = '<ul class="list-group">';

    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += (index + 1) + '.  ';
        html += arr[index].fremaaOfficerName + '  ';
        html += '<button type="button" class="btn btn-primary" onclick="removeOfficersEngaged(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    div.append(html);

    if (arr.length == 0) {
        div.addClass('hidden');
    }

}

removeOfficersEngaged = function (index) {

    arrOfficersEngaged.splice(index, 1);

    displayEngagedOfficersList('officersListDisplay', arrOfficersEngaged);

}

displayPmcSiteEngineersList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    var html = '<ul class="list-group">';

    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += (index + 1) + '.  ';
        html += arr[index].siteEngineerName + '  ';
        html += '<button type="button" class="btn btn-primary" onclick="removePmcSiteEngineers(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    div.append(html);

    if (arr.length == 0) {
        div.addClass('hidden');
    }

}

removePmcSiteEngineers = function (index) {

    arrPmcSiteEngineers.splice(index, 1);

    displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);

}

showSioUserOptionsToAddDbaProjects = function (id, sioUserArray) {

    var options = [];
    
    options.push({
        label: '--Select SIO--',
        value: '',
        selected: true
    });

    for (var i = 0; i < sioUserArray.length ; i++) {
        options.push({
            label: sioUserArray[i].FirstName + ' ' + sioUserArray[i].LastName,
            value: sioUserArray[i].UserGuid,
        });
    }

    $('#' + id).multiselect('dataprovider', options);

    $('#' + id).multiselect('refresh');

}

showWorkItemOptionsToAddDbaProjects = function (id, workItemArray) {

    var options = [];

    options.push({
        label: '--Select WorkItem--',
        value: '',
        selected: true
    });

    for (var i = 0; i < workItemArray.length ; i++) {
        options.push({
            label: workItemArray[i].WorkItemName,
            value: workItemArray[i].WorkItemGuid,
        });
    }

    $('#' + id).multiselect('dataprovider', options);

    $('#' + id).multiselect('refresh');

    if (arrWorkItems.length > 0) {
        removeWorkItemOptionsToAddDbaProjects(id);
    }
    
}

displayWorkItemValueDiv = function (selectedWorkItemOptionValue) {

    if (jQuery.Guid.IsValid(selectedWorkItemOptionValue.toUpperCase()) && !jQuery.Guid.IsEmpty(selectedWorkItemOptionValue.toUpperCase())) {
        $('#workItemValueDiv').removeClass('hidden');
    } else {
        $('#workItemValueDiv').addClass('hidden');
    }

}

displayWorkItemsList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    var html = '<ul class="list-group">';

    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += (index + 1) + '.  ';
        html += arr[index].workItemName + '  ';
        html += '<button type="button" class="btn btn-primary" onclick="removeWorkItems(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    div.append(html);

    if (arr.length == 0) {
        div.addClass('hidden');
    }

}

removeWorkItems = function (index) {

    arrWorkItems.splice(index, 1);

    displayWorkItemsList('workItemsListDisplay', arrWorkItems);

    removeWorkItemOptionsToAddDbaProjects('workItemList');

    showWorkItemOptionsToAddDbaProjects('workItemList', arrWorkItemsToDisplay);

}

removeWorkItemOptionsToAddDbaProjects = function (id) {

    if (arrWorkItems.length > 0) {

        for (var a = 0; a < arrWorkItems.length; a++) {
            $('option[value="' + arrWorkItems[a].workItemGuid + '"]', $('#' + id)).remove();
        }

        $('#' + id).multiselect('rebuild');

    }

}

displaySubChainagesList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    var html = '<ul class="list-group">';

    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += (index + 1) + '.  ';
        html += arr[index].subChainageName + '  ';
        //alert(arr[index].subChainageName + " " + arr[index].subChainageSioName + " " + arr[index].subChainageProjectValue + " " + arr[index].aaRef + " " + arr[index].tsRef + " " + arr[index].contractorName + " " + arr[index].workOrderRef + " " + arr[index].subChainageStartingDate + " " + arr[index].subChainageCompletionDate + " " + arr[index].typeOfWorks);
        html += '<button type="button" class="btn btn-primary" onclick="removeSubChainages(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    div.append(html);

    if (arr.length == 0) {
        div.addClass('hidden');
    }

}

removeSubChainages = function (index) {

    arrSubChainages.splice(index, 1);

    displaySubChainagesList('subChainagesListDisplay', arrSubChainages);

}

createAssignValueToFinancialSubHeadPopupForm = function () {

    $('#assignValueToFinancialSubHeadPopupModal .modal-body').empty();

    var remainingProjectValue = projectValue;

    if (arrFinancialHeadBudgets.length > 0) {
        for (var i = 0; i < arrFinancialHeadBudgets.length; i++) {
            var a = arrFinancialHeadBudgets[i].financialSubHeadBudgetInput;
            //remainingProjectValue = remainingProjectValue - a;
            remainingProjectValue = Math.abs(numbers.basic.subtraction([remainingProjectValue, a]));
        }
    }

    if (remainingProjectValue > 0) {
        var html = '<div class="container-fluid col-md-12">';
        html += '<div class="row">';
        html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
        html += '<div class="panel panel-default">';
        html += '<div class="panel-body">';
        html += '<div class="text-center">';
        html += '<img src="Images/create-financial.png" class="login" height="70" />';
        html += '<h2 class="text-center">Financial Head</h2>';

        html += '<div class="panel-body">';
        html += '<form id="assignValueToFinancialSubHeadPopupForm" class="popupForm form form-horizontal" method="post">';
        html += '<fieldset>';

        html += '<div class="form-group">';
        html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
        html += '<label id="totalBudgetLblText" class="control-label">' + 'Total Budget: ' + '</label>';
        html += '</div>';
        html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
        html += '<label id="totalBudgetLbl" class="control-label">Rs. ' + $.format(projectValue) + '</label>';
        html += '</div>';

        html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
        html += '<label id="remainingBudgetLblText" class="control-label">' + 'Remaining Budget: ' + '</label>';
        html += '</div>';
        html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
        html += '<label id="remainingBudgetLbl" name="remainingBudgetLbl" class="control-label">Rs. ' + $.format(remainingProjectValue) + '</label>';
        html += '</div>';
        html += '<input id="remainingBudgetHdn" name="remainingBudgetHdn" type="hidden" value="Rs. ' + $.format(remainingProjectValue) + '" />';
        html += '</div>';

        html += '<div class="form-group">';
        html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
        html += '<label for="financialSubHeadList" id="financialSubHeadListLabel" class="control-label">Financial SubHead Name</label>';
        html += '</div>';
        html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
        html += '<select class="form-control multiselect" name="financialSubHeadList" id="financialSubHeadList"></select>';
        html += '</div>';
        html += '</div>';

        html += '<div class="form-group">';
        html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
        html += '<label for="financialSubHeadBudget" id="financialSubHeadBudgetLabel" class="control-label">Budget</label>';
        html += '</div>';
        html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
        html += '<input id="financialSubHeadBudget" name="financialSubHeadBudget" placeholder="Budget" class="form-control" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)" />';
        html += '</div>';
        html += '</div>';

        html += '<div class="form-group">';
        html += '<button class="btn btn-primary" aria-hidden="true" id="createFinancialHeadBtn" name="createFinancialHeadBtn">Create Head</button>';
        html += '</div>';

        html += '</fieldset>';
        html += '</form>';
        html += '</div>';

        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';

        $("#assignValueToFinancialSubHeadPopupModal .modal-body").append(html);

        $('#financialSubHeadList').multiselect({
            maxHeight: 150,
            //enableCaseInsensitiveFiltering: true,
            numberDisplayed: 1,
            nonSelectedText: '--Select Financial SubHead--',
            disableIfEmpty: true,
            buttonClass: 'btn btn-default',
            buttonContainer: '<div class="btn-group btn-group-justified" />',
            //disabledText: 'There is no WorkItem SubHead...'
            onChange: function (element, checked) {

                $(element).closest('.multiselect').valid();

                if (checked === true) {
                    $(document).click();
                } else if (checked === false) {

                }
            },
            templates: {
                button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
            }
        });

        showFinancialSubHeadOptionsToAddDbaProjects('financialSubHeadList', arrFinancialSubHeadsToDisplay);

        $('#assignValueToFinancialSubHeadPopupModal').modal('show');

        callOtherFuctions();

        $('#financialSubHeadBudget').maskMoney();

        $('#createFinancialHeadBtn').on('click', function (e) {
            e.preventDefault();
            if ($('#assignValueToFinancialSubHeadPopupForm').valid()) {

                var arrFinancialSubHeads = {
                    financialSubHeadGuid: $('#financialSubHeadList').val(),
                    financialSubHeadName: $('#financialSubHeadList option:selected').text()
                };

                var financialSubHeadBudget = $('#financialSubHeadBudget').val();
                var newchar = '';
                financialSubHeadBudget = financialSubHeadBudget.split(',').join(newchar);
                //subChainageValue = subChainageValue.replace(/,/g , newchar);
                financialSubHeadBudget = Math.abs(parseFloat(financialSubHeadBudget));

                arrFinancialHeadBudgets.push({
                    financialSubHeadManager: arrFinancialSubHeads,
                    financialSubHeadBudgetInput: financialSubHeadBudget
                });

                createFinancialSubHeadTable();

                $('#assignValueToFinancialSubHeadPopupModal').modal('hide');
            }

        });
    } else {
        bootbox.alert("You have assigned the whole Project Value.");
    }

}

showFinancialSubHeadOptionsToAddDbaProjects = function (id, financialSubHeadArray) {
    var options = [];

    options.push({
        label: '--Select Financial SubHead--',
        value: '',
        selected: true
    });

    for (var i = 0; i < financialSubHeadArray.length ; i++) {
        options.push({
            label: financialSubHeadArray[i].FinancialSubHeadName,
            value: financialSubHeadArray[i].FinancialSubHeadGuid,
        });
    }

    $('#' + id).multiselect('dataprovider', options);

    $('#' + id).multiselect('refresh');

    if (arrFinancialHeadBudgets.length > 0) {
        removeFinancialSubHeadOptionsToAddDbaProjects(id);
    }

}

removeFinancialSubHeadOptionsToAddDbaProjects = function (id) {

    if (arrFinancialHeadBudgets.length > 0) {

        for (var a = 0; a < arrFinancialHeadBudgets.length; a++) {
            $('option[value="' + arrFinancialHeadBudgets[a].financialSubHeadManager.financialSubHeadGuid + '"]', $('#' + id)).remove();
        }

        $('#' + id).multiselect('rebuild');

    }

}

callOtherFuctions = function () {

    $('#assignValueToFinancialSubHeadPopupForm').validate({ // initialize plugin
        //ignore: ":not(:visible)",
        ignore: ':hidden:not(".multiselect")',
        rules: {
            financialSubHeadList: "required",
            financialSubHeadBudget: {
                required: true,
                currencySmallerThan: [name = 'remainingBudgetHdn']
            }
        },
        messages: {
            financialSubHeadList: {
                required: "Please Select Financial SubHead."
            },
            financialSubHeadBudget: {
                required: "Please Enter Financial SubHead Budget.",
                currencySmallerThan: "Financial SubHead Budget should be less than the Remaining Budget Value."
            },
            subProjectName: {
                required: "Please Enter Sub-Project Name.",
                noSpace: "Sub-Project Name cannot be blank."
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block small',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('multiselect')) {
                //error.insertAfter($('#' + element.attr('id')));
                //error.insertBefore(element);
                error.insertAfter(element.parent().find('.btn-group'));
                //$(this).parents("fieldset").find("*:not('legend')").toggle();
            } else {
                //element.closest('.form-group').append(error);
                error.insertAfter(element);
            }
        },
        submitHandler: function () {
            alert('valid form');
            return false;
        }
    });

}

createFinancialSubHeadTable = function () {
    
    $('#financialSubHeadTableDiv').empty();

    var totalAssignedValue = 0;

    if (arrFinancialHeadBudgets.length > 0) {
        for (var i = 0; i < arrFinancialHeadBudgets.length; i++) {
            var a = arrFinancialHeadBudgets[i].financialSubHeadBudgetInput;
            //totalAssignedValue = totalAssignedValue + a;
            totalAssignedValue = Math.abs(numbers.basic.sum([totalAssignedValue, a]));
        }
    }

    var remainingProjectValue = projectValue;

    if (arrFinancialHeadBudgets.length > 0) {
        for (var i = 0; i < arrFinancialHeadBudgets.length; i++) {
            var a = arrFinancialHeadBudgets[i].financialSubHeadBudgetInput;
            //remainingProjectValue = remainingProjectValue - a;
            remainingProjectValue = Math.abs(numbers.basic.subtraction([remainingProjectValue, a]));
        }
    }

    addedFinancialSubHeadsTable
        .clear()
        .draw();

    for (var i = 0; i < arrFinancialHeadBudgets.length; i++) {
        var newUnitRow = addedFinancialSubHeadsTable.row.add([
                    '',
                    arrFinancialHeadBudgets[i].financialSubHeadManager.financialSubHeadName,
                    $.format(parseFloat(arrFinancialHeadBudgets[i].financialSubHeadBudgetInput)),
                    '<button type="button" class="btn btn-primary" onclick="removeFinancialSubHeadTable(' + i + ');">Remove</button>'
        ]).draw(false);
    }

    //addedFinancialSubHeadsTable
    //    .column(3)
    //    .nodes()
    //    .to$()      // Convert to a jQuery object
    //    .addClass( 'ready' );

    $('#totalValueAssignedTableFoot').empty();
    $('#totalValueAssignedTableFoot').append('Rs. ' + $.format(parseFloat(totalAssignedValue)));

    //$('#totalBudgetTable').empty();
    //$('#totalBudgetTable').append('Rs. ' + $.format(parseFloat(projectValue)));

    $('#totalValueAssignedTable').empty();
    $('#totalValueAssignedTable').append('Rs. ' + $.format(parseFloat(totalAssignedValue)));

    $('#remainingBudgetTable').empty();
    $('#remainingBudgetTable').append('Rs. ' + $.format(parseFloat(remainingProjectValue)));

    
}

removeFinancialSubHeadTable = function (index) {

    arrFinancialHeadBudgets.splice(index, 1);

    createFinancialSubHeadTable();

}

saveProject = function () {
    var retValue = 0;

    var districtManager = {
        districtGuid: $.trim($('#districtList').val())
    };

    var divisionManager = {
        divisionGuid: $.trim($('#divisionList').val())
    };


    var projectManager = {
        districtManager: districtManager,
        divisionManager: divisionManager,
        projectName: $.trim($('#projectName').val()),
        subProjectName: $.trim($('#subProjectName').val()),
        projectChainage: $.trim($('#chainageOfProject').val()),
        dateOfStartOfProject: Date.parse($('#dateOfStartOfProject').val()).toString('yyyy/MM/dd'),
        expectedDateOfCompletionOfProject: Date.parse($('#expectedDateOfCompletionOfProject').val()).toString('yyyy/MM/dd'),
        projectValueInput: projectValue,
        officersEngaged: arrOfficersEngaged,
        pmcSiteEngineers: arrPmcSiteEngineers,
        subChainageManagerList: arrSubChainages,
        landAcquisitionIncluded: $('input[name=landAcquisitionRadio]:checked', '#createNewProjectForm').val(),
        projectValueDistributionManagerList: arrFinancialHeadBudgets
    };

    var arrProjectManagerJsonString = JSON.stringify(projectManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "AddDBAProject.aspx/AddProject",
        data: '{"projectManager":' + arrProjectManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        error: function (msg) {
            //$('#status').delay(300).fadeOut(); // will first fade out the loading animation
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return retValue;
}