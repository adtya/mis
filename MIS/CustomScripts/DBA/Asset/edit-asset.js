﻿var drawingUrl = '';
var regulatorGateTable = null;
var sluiceGateTable = null;
var turnoutGateTable = null;

$(document).ready(function () {

    $('.assetType').hide();

    var cfgCulture = 'en-IN';
    $.preferCulture(cfgCulture);
    //$.preferCulture("en-IN");

    $('#assetCost').maskMoney();

    //Key Asset List
    $('#keyAssetList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Key Asset--',
        minimumResultsForSearch: -1,
        //disabled: true,
        allowClear: true
    }).on('change', function (e) {
        //$(this).valid();

        var selectedKeyAssetGuid = $(this).val();
        var selectedKeyAssetText = $('option:selected', this).text();

    });

    $('#uploadDrawing').click(function (e) {
        e.preventDefault();
        $('#drawingCode1').click();
    });

    $('#drawingCode1').change(function (e) {
        e.preventDefault();
        var filePath = $('#drawingCode1').val();
        var fileName = filePath.split('\\');
        $('#drawingCode').val(fileName[fileName.length - 1]);
    });

    $('#dpDateOfInitiationOfAsset').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var dateOfCompletionOfAssetStartDate = new Date(ev.date.valueOf());
        dateOfCompletionOfAssetStartDate = dateOfCompletionOfAssetStartDate.add(1).days();
        $('#dpDateOfCompletionOfAsset').datepicker('setStartDate', dateOfCompletionOfAssetStartDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpDateOfCompletionOfAsset').datepicker('setStartDate', null);
    });

    $('#dpDateOfCompletionOfAsset').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var dateOfInitiationOfAssetEndDate = new Date(ev.date.valueOf());
        dateOfInitiationOfAssetEndDate = dateOfInitiationOfAssetEndDate.add(-1).days();
        $('#dpDateOfInitiationOfAsset').datepicker('setEndDate', dateOfInitiationOfAssetEndDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpDateOfInitiationOfAsset').datepicker('setEndDate', null);
    });

    displayAssetTechnicalSpecification($('#assetTypeCode').val());

    $('#updateAssetBtn').on('click', function (e) {
        e.preventDefault();

        if ($('#editAssetForm').valid()) {
            $("#preloader").show();
            $("#status").show();

            var fileUploaded = true;//uploadAssetDrawing(codeArray);

            if (fileUploaded) {
                var assetUpdated = updateAsset($('#assetTypeCode').val());

                if (assetUpdated === 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Asset is updated successfully.", function () {
                        window.location = 'DBAAssets.aspx';
                    });
                } else {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Data cannot be updated.");
                }

            }
            else {

            }
        }

    });

    $('#cancelAssetEditBtn').click(function (e) {
        e.preventDefault();
        window.location = 'DBAAssets.aspx';
    });

    $('#editAssetForm').validate({ // initialize plugin
        //ignore: ":not(:visible)",
        ignore: ':hidden:not(".multiselect")',
        rules: {
            //divisionList: "required",
            //schemeList: "required",
            //assetTypeList: "required",
            //assetCodeNumberSelect: "required",
            //assetCode: "required",
            //keyAssetSelect: "required",
            //drawingCode: "required",
            amtd: {
                number: true
            },
            assetName: {
                required: true,
                noSpace: true
            },
            //assetCost: "required",
            chainageStart: {
                number: true
            },
            chainageEnd: {
                number: true
            },
            eastingUS: {
                number: true
            },
            eastingDS: {
                number: true
            },
            northingUS: {
                number: true
            },
            northingDS: {
                number: true
            },
            dateOfInitiationOfAsset: {
                required: true,
                dateITA: true
            },
            dateOfCompletionOfAsset: {
                required: true,
                dateITA: true
            },

            //Bridge
            bridgeTypeList: "required",
            bridgeLength: {
                number: true
            },
            bridgeNumberOfPiers: {
                number: true
            },
            bridgeRoadWidth: {
                number: true
            },

            //Canal
            canalUsCrestElevation: {
                number: true
            },
            canalDsCrestElevation: {
                number: true
            },
            canalLength: {
                number: true
            },
            canalBedWidth: {
                number: true
            },
            canalSlipeSlope1: {
                number: true
            },
            canalSlipeSlope2: {
                number: true
            },
            canalAverageDepth: {
                number: true
            },
            canalLiningTypeList: "required",

            //Culvert
            culvertTypeList: "required",
            culvertNumberOfVents: {
                number: true
            },
            culvertVentHeight: {
                number: true
            },
            culvertVentWidth: {
                number: true
            },
            culvertLength: {
                number: true
            },

            //Drainage
            drainageUsBedElevation: {
                number: true
            },
            drainageDsBedElevation: {
                number: true
            },
            drainageLength: {
                number: true
            },
            drainageBedWidth: {
                number: true
            },
            drainageSlope1: {
                number: true
            },
            drainageSlope2: {
                number: true
            },
            drainageAverageDepth: {
                number: true
            },

            //Drop Structure
            dropStructureUsInvertLevel: {
                number: true
            },
            dropStructureDsInvertLevel: {
                number: true
            },
            dropStructureStillingBasinLength: {
                number: true
            },
            dropStructureStillingBasinWidth: {
                number: true
            },

            //Embankment
            embankmentUsCrestElevation: {
                number: true
            },
            embankmentDsCrestElevation: {
                number: true
            },
            embankmentLength: {
                number: true
            },
            embankmentCrestWidth: {
                number: true
            },
            embankmentCsSlope1: {
                number: true
            },
            embankmentCsSlope2: {
                number: true
            },
            embankmentRsSlope1: {
                number: true
            },
            embankmentRsSlope2: {
                number: true
            },
            embankmentBermSlope1: {
                number: true
            },
            embankmentBermSlope2: {
                number: true
            },
            embankmentPlatformWidth: {
                number: true
            },
            embankmentCrestTypeList: "required",
            embankmentFillTypeList: "required",
            embankmentAverageHeight: {
                number: true
            },

            //Gauge
            gaugeTypeList: "required",
            gaugeSeasonTypeList: "required",
            gaugeFrequencyTypeList: "required",
            gaugeZeroDatum: {
                number: true
            },
            gaugeStartDate: {
                //required: true,
                dateITA: true
            },
            gaugeEndDate: {
                //required: true,
                dateITA: true
            },
            //gaugeActive: "required",
            //gaugeLwlPresent: "required",
            //gaugeHwlPresent: "required",
            //gaugeLevelGeo: "required",

            //Porcupine
            porcupineTypeList: "required",
            porcupineMaterialTypeList: "required",
            porcupineScreenLength: {
                number: true
            },
            porcupineSpacingAlongScreen: {
                number: true
            },
            porcupineScreenRows: {
                number: true
            },
            porcupineNumberOfLayers: {
                number: true
            },
            porcupineMamberLength: {
                number: true
            },
            porcupineLength: {
                number: true
            },

            //Regulator
            regulatorNumberOfVents: {
                number: true
            },
            regulatorVentHeight: {
                number: true
            },
            regulatorVentWidth: {
                number: true
            },
            regulatorTypeList: "required",
            regulatorGateTypeList: "required",

            //Revetment
            revetmentTypeList: "required",
            revetmentRiverProtectionTypeList: "required",
            revetmentWaveProtectionTypeList: "required",
            revetmentSlope: {
                number: true
            },
            revetmentLength: {
                number: true
            },
            revetmentPlainWidth: {
                number: true
            },

            //Sluice
            sluiceTypeList: "required",
            sluiceNumberOfVents: {
                number: true
            },
            sluiceVentDiameter: {
                number: true
            },
            sluiceVentWidth: {
                number: true
            },
            sluiceVentHeight: {
                number: true
            },

            //Spur
            spurOrientation: {
                number: true
            },
            spurLength: {
                number: true
            },
            spurWidth: {
                number: true
            },
            spurShapeTypeList: "required",
            spurConstructionTypeList: "required",
            spurRevetTypeList: "required",

            //Turnout
            turnoutInletBoxWidth: {
                number: true
            },
            turnoutInletBoxLength: {
                number: true
            },
            turnoutOutletNumber: {
                number: true
            },
            turnoutOutletWidth: {
                number: true
            },
            turnoutOutletHeight: {
                number: true
            },
            turnoutTypeList: "required",
            turnoutGateTypeList: "required",

            //Weir
            weirInvertLevel: {
                number: true
            },
            weirCrestTopLevel: {
                number: true
            },
            weirCrestTopWidth: {
                number: true
            },
            weirTypeList: "required",

            //Regulator Gate
            regulatorGatesGateTypeList: "required",
            regulatorGatesGateConstructionMaterialTypeList: "required",
            regulatorGatesGateNumbers: {
                number: true
            },
            regulatorGatesGateHeight: {
                number: true
            },
            regulatorGatesGateWidth: {
                number: true
            },
            regulatorGatesGateDiameter: {
                number: true
            },
            regulatorGatesGateInstallationDate: {
                //required: true,
                dateITA: true
            },

            //Sluice Gate
            sluiceGatesGateTypeList: "required",
            sluiceGatesGateConstructionMaterialTypeList: "required",
            sluiceGatesGateNumbers: {
                number: true
            },
            sluiceGatesGateHeight: {
                number: true
            },
            sluiceGatesGateWidth: {
                number: true
            },
            sluiceGatesGateDiameter: {
                number: true
            },
            sluiceGatesGateInstallationDate: {
                //required: true,
                dateITA: true
            },

            //Turnout Gate
            turnoutGatesGateTypeList: "required",
            turnoutGatesGateConstructionMaterialTypeList: "required",
            turnoutGatesGateNumbers: {
                number: true
            },
            turnoutGatesGateHeight: {
                number: true
            },
            turnoutGatesGateWidth: {
                number: true
            },
            turnoutGatesGateDiameter: {
                number: true
            },
            turnoutGatesGateInstallationDate: {
                //required: true,
                dateITA: true
            },
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block small',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('multiselect')) {
                //error.insertAfter($('#' + element.attr('id')));
                //error.insertBefore(element);
                error.insertAfter(element.parent().find('.btn-group'));
                //$(this).parents("fieldset").find("*:not('legend')").toggle();
            } else if (element.hasClass('select2')) {
                //error.insertAfter($('#' + element.attr('id')));
                //error.insertBefore(element);
                error.insertAfter(element.parent().find('.select2-container'));
                //$(this).parents("fieldset").find("*:not('legend')").toggle();
            } else {
                //element.closest('.form-group').append(error);
                error.insertAfter(element);
            }
        },
        submitHandler: function () {
            //alert('valid form');
            return false;
        }
    });

});

function displayAssetTechnicalSpecification(assetTypeCode) {

    $('.assetType').hide();

    if (regulatorGateTable != null) {
        regulatorGateTable.clear().draw();
    }

    if (sluiceGateTable != null) {
        sluiceGateTable.clear().draw();
    }

    if (turnoutGateTable != null) {
        turnoutGateTable.clear().draw();
    }

    if (assetTypeCode === 'BRG') {
        $('#bridgeTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Bridge Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#bridgeAssetTypeDiv').show();
    }
    else if (assetTypeCode === 'CAN') {
        $('#canalLiningTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Lining Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#canalAssetTypeDiv').show();
    }
    else if (assetTypeCode === 'CUL') {
        $('#culvertTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Culvert Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#culvertAssetTypeDiv').show();
    }
    else if (assetTypeCode === 'DRN') {
        $('#drainageAssetTypeDiv').show();
    }
    else if (assetTypeCode === 'DRP') {
        $('#dropStructureAssetTypeDiv').show();
    }
    else if (assetTypeCode === 'EMB') {
        $('#embankmentCrestTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Crest Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#embankmentFillTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Fill Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#embankmentAssetTypeDiv').show();
    }
    else if (assetTypeCode === 'GAU') {
        $('#gaugeTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Gauge Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#gaugeSeasonTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Season Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#gaugeFrequencyTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Frequency Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#dpGaugeStartDate').datepicker({
            format: "dd/mm/yyyy",
            clearBtn: true,
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (ev) {
            var gaugeEndDateStartDate = new Date(ev.date.valueOf());
            gaugeEndDateStartDate = gaugeEndDateStartDate.add(1).days();
            $('#dpGaugeEndDate').datepicker('setStartDate', gaugeEndDateStartDate);
            $(this).blur();
            $(this).datepicker('hide');
        }).on('clearDate', function (selected) {
            $('#dpGaugeEndDate').datepicker('setStartDate', null);
        });

        $('#dpGaugeEndDate').datepicker({
            format: "dd/mm/yyyy",
            clearBtn: true,
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (ev) {
            var gaugeStartDateEndDate = new Date(ev.date.valueOf());
            gaugeStartDateEndDate = gaugeStartDateEndDate.add(-1).days();
            $('#dpGaugeStartDate').datepicker('setEndDate', gaugeStartDateEndDate);
            $(this).blur();
            $(this).datepicker('hide');
        }).on('clearDate', function (selected) {
            $('#dpGaugeStartDate').datepicker('setEndDate', null);
        });

        $('#gaugeAssetTypeDiv').show();
    }
    else if (assetTypeCode === 'POR') {
        $('#porcupineTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Porcupine Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#porcupineMaterialTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Material Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#porcupineAssetTypeDiv').show();
    }
    else if (assetTypeCode === 'REG') {
        $('#regulatorTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Regulator Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#regulatorGateTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Gate Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#regulatorAssetTypeDiv').show();
        regulatorGatesTableRelatedFunctions();
    }
    else if (assetTypeCode === 'REV') {
        $('#revetmentTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Revetment Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#revetmentRiverProtectionTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select River Protection Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#revetmentWaveProtectionTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Wave Protection Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#revetmentAssetTypeDiv').show();
    }
    else if (assetTypeCode === 'SLU') {
        $('#sluiceTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Sluice Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#sluiceAssetTypeDiv').show();
        sluiceGatesTableRelatedFunctions();
    }
    else if (assetTypeCode === 'SPU') {
        $('#spurShapeTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Shape Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#spurConstructionTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Construction Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#spurRevetTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Revet Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#spurAssetTypeDiv').show();
    }
    else if (assetTypeCode === 'TRN') {
        $('#turnoutTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Turnout Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#turnoutGateTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Gate Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#turnoutAssetTypeDiv').show();
        turnoutGatesTableRelatedFunctions();
    }
    else if (assetTypeCode === 'WEI') {
        $('#weirTypeList').select2({
            theme: 'bootstrap',
            placeholder: '--Select Weir Type--',
            minimumResultsForSearch: -1,
            allowClear: true
        });

        $('#weirAssetTypeDiv').show();
    }

}


//Gate

function regulatorGatesTableRelatedFunctions() {
    var regulatorGateEditing = null;

    var tableId = $('#regulatorAssetTypeDiv table#regulatorGatesTable');

    regulatorGateTable = tableId.DataTable({
        destroy: true,
        paging: false,
        info: false,
        searching: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [2],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [3],
                visible: false,
                orderable: false,
                searchable: false
            },
        ],
        order: [[4, 'asc']],
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        buttons: [
            {
                text: '+ Add New Gate',
                className: 'btn-success addNewRegulatorGateBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewRegulatorGate();

                    e.stopPropagation();
                }
            }
        ]
    });


    regulatorGateTable.on('order.dt search.dt', function () {
        regulatorGateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addRegulatorGateBtn = regulatorGateTable.button(['.addNewRegulatorGateBtn']);

    function addNewRegulatorGate() {

        if (regulatorGateEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreGateRowData(regulatorGateTable, regulatorGateEditing);
            regulatorGateEditing = null;
            addRegulatorGateBtn.enable();
        }

        var newGateRow = regulatorGateTable.row.add([
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ]).draw(false);

        var newRegulatorGateRowNode = newGateRow.node();

        addNewGateRow(regulatorGateTable, newRegulatorGateRowNode, 'regulator', 'Regulator');

        regulatorGateEditing = newRegulatorGateRowNode;

        addRegulatorGateBtn.disable();
    }

    tableId.on('click', 'a.saveNewRegulatorGateBtn', function (e) {
        e.preventDefault();

        if ($('select[name="regulatorGatesGateTypeList"], select[name="regulatorGatesGateConstructionMaterialTypeList"], input[name="regulatorGatesGateNumbers"], input[name="regulatorGatesGateHeight"], input[name="regulatorGatesGateWidth"], input[name="regulatorGatesGateDiameter"], input[name="regulatorGatesGateInstallationDate"]').valid()) {
            var selectedRow = $(this).parents('tr')[0];

            if (regulatorGateEditing !== null && regulatorGateEditing == selectedRow) {
                /* A different row is being edited - the edit should be cancelled and this row edited */
                saveNewGateRow(regulatorGateTable, regulatorGateEditing, 'editRegulatorGateBtn', 'deleteRegulatorGateBtn');
                regulatorGateEditing = null;
                addRegulatorGateBtn.enable();
            }
        }

    });

    tableId.on('click', 'a.cancelNewRegulatorGateSaveBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        if (regulatorGateEditing !== null && regulatorGateEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            removeNewGateRowCreatedOnCancel(regulatorGateTable, selectedRow);
            regulatorGateEditing = null;
            addRegulatorGateBtn.enable();
        }

    });

    tableId.on('click', 'a.editRegulatorGateBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        if (regulatorGateEditing !== null && regulatorGateEditing != selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreGateRowData(regulatorGateTable, regulatorGateEditing);
            editGateRow(regulatorGateTable, selectedRow, 'regulator', 'Regulator');
            regulatorGateEditing = selectedRow;
            addRegulatorGateBtn.enable();
        } else {
            /* No row currently being edited */
            editGateRow(regulatorGateTable, selectedRow, 'regulator', 'Regulator');
            regulatorGateEditing = selectedRow;
            addRegulatorGateBtn.enable();
        }

    });

    tableId.on('click', 'a.updateRegulatorGateBtn', function (e) {
        e.preventDefault();

        if ($('select[name="regulatorGatesGateTypeList"], select[name="regulatorGatesGateConstructionMaterialTypeList"], input[name="regulatorGatesGateNumbers"], input[name="regulatorGatesGateHeight"], input[name="regulatorGatesGateWidth"], input[name="regulatorGatesGateDiameter"], input[name="regulatorGatesGateInstallationDate"]').valid()) {
            var selectedRow = $(this).parents('tr')[0];

            if (regulatorGateEditing !== null && regulatorGateEditing == selectedRow) {
                /* A different row is being edited - the edit should be cancelled and this row edited */
                updateGateRow(regulatorGateTable, regulatorGateEditing, 'editRegulatorGateBtn', 'deleteRegulatorGateBtn');
                regulatorGateEditing = null;
                addRegulatorGateBtn.enable();
            }

        }

    });

    tableId.on('click', 'a.cancelRegulatorGateEditBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        if (regulatorGateEditing !== null && regulatorGateEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreGateRowData(regulatorGateTable, regulatorGateEditing);
            regulatorGateEditing = null;
            addRegulatorGateBtn.enable();
        }

    });

    tableId.on('click', 'a.deleteRegulatorGateBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        var selectedRow = $(this).parents('tr')[0];

        if (jQuery.Guid.IsValid(regulatorGateTable.row(selectedRow).data()[1].toUpperCase())) {
            var regulatorGateRowDeleted = deleteGateRow(regulatorGateTable.row(selectedRow).data()[1]);

            if (regulatorGateRowDeleted === 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                regulatorGateTable.row(selectedRow).remove().draw(false);
            } else if (regulatorGateRowDeleted === 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
            }
        } else {
            regulatorGateTable.row(selectedRow).remove().draw(false);
        }

        addRegulatorGateBtn.enable();

    });

}

function sluiceGatesTableRelatedFunctions() {
    var sluiceGateEditing = null;

    var tableId = $('#sluiceAssetTypeDiv table#sluiceGatesTable');

    sluiceGateTable = tableId.DataTable({
        destroy: true,
        paging: false,
        info: false,
        searching: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [2],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [3],
                visible: false,
                orderable: false,
                searchable: false
            },
        ],
        order: [[4, 'asc']],
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        buttons: [
            {
                text: '+ Add New Gate',
                className: 'btn-success addNewSluiceGateBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewSluiceGate();

                    e.stopPropagation();
                }
            }
        ]
    });


    sluiceGateTable.on('order.dt search.dt', function () {
        sluiceGateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addSluiceGateBtn = sluiceGateTable.button(['.addNewSluiceGateBtn']);

    function addNewSluiceGate() {

        if (sluiceGateEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreGateRowData(sluiceGateTable, sluiceGateEditing);
            sluiceGateEditing = null;
            addSluiceGateBtn.enable();
        }

        var newGateRow = sluiceGateTable.row.add([
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ]).draw(false);

        var newSluiceGateRowNode = newGateRow.node();

        addNewGateRow(sluiceGateTable, newSluiceGateRowNode, 'sluice', 'Sluice');

        sluiceGateEditing = newSluiceGateRowNode;

        addSluiceGateBtn.disable();
    }

    tableId.on('click', 'a.saveNewSluiceGateBtn', function (e) {
        e.preventDefault();

        if ($('select[name="sluiceGatesGateTypeList"], select[name="sluiceGatesGateConstructionMaterialTypeList"], input[name="sluiceGatesGateNumbers"], input[name="sluiceGatesGateHeight"], input[name="sluiceGatesGateWidth"], input[name="sluiceGatesGateDiameter"], input[name="sluiceGatesGateInstallationDate"]').valid()) {
            var selectedRow = $(this).parents('tr')[0];

            if (sluiceGateEditing !== null && sluiceGateEditing == selectedRow) {
                /* A different row is being edited - the edit should be cancelled and this row edited */
                saveNewGateRow(sluiceGateTable, sluiceGateEditing, 'editSluiceGateBtn', 'deleteSluiceGateBtn');
                sluiceGateEditing = null;
                addSluiceGateBtn.enable();
            }

        }

    });

    tableId.on('click', 'a.cancelNewSluiceGateSaveBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        if (sluiceGateEditing !== null && sluiceGateEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            removeNewGateRowCreatedOnCancel(sluiceGateTable, selectedRow);
            sluiceGateEditing = null;
            addSluiceGateBtn.enable();
        }

    });

    tableId.on('click', 'a.editSluiceGateBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        if (sluiceGateEditing !== null && sluiceGateEditing != selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreGateRowData(sluiceGateTable, sluiceGateEditing);
            editGateRow(sluiceGateTable, selectedRow, 'sluice', 'Sluice');
            sluiceGateEditing = selectedRow;
            addSluiceGateBtn.enable();
        } else {
            /* No row currently being edited */
            editGateRow(sluiceGateTable, selectedRow, 'sluice', 'Sluice');
            sluiceGateEditing = selectedRow;
            addSluiceGateBtn.enable();
        }

    });

    tableId.on('click', 'a.updateSluiceGateBtn', function (e) {
        e.preventDefault();

        if ($('select[name="sluiceGatesGateTypeList"], select[name="sluiceGatesGateConstructionMaterialTypeList"], input[name="sluiceGatesGateNumbers"], input[name="sluiceGatesGateHeight"], input[name="sluiceGatesGateWidth"], input[name="sluiceGatesGateDiameter"], input[name="sluiceGatesGateInstallationDate"]').valid()) {
            var selectedRow = $(this).parents('tr')[0];

            if (sluiceGateEditing !== null && sluiceGateEditing == selectedRow) {
                /* A different row is being edited - the edit should be cancelled and this row edited */
                updateGateRow(sluiceGateTable, sluiceGateEditing, 'editSluiceGateBtn', 'deleteSluiceGateBtn');
                sluiceGateEditing = null;
                addSluiceGateBtn.enable();
            }

        }

    });

    tableId.on('click', 'a.cancelSluiceGateEditBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        if (sluiceGateEditing !== null && sluiceGateEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreGateRowData(sluiceGateTable, sluiceGateEditing);
            sluiceGateEditing = null;
            addSluiceGateBtn.enable();
        }

    });

    tableId.on('click', 'a.deleteSluiceGateBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        var selectedRow = $(this).parents('tr')[0];

        if (jQuery.Guid.IsValid(sluiceGateTable.row(selectedRow).data()[1].toUpperCase())) {
            var sluiceGateRowDeleted = deleteGateRow(sluiceGateTable.row(selectedRow).data()[1]);

            if (sluiceGateRowDeleted === 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                sluiceGateTable.row(selectedRow).remove().draw(false);
            } else if (sluiceGateRowDeleted === 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
            }
        } else {
            sluiceGateTable.row(selectedRow).remove().draw(false);
        }

        addSluiceGateBtn.enable();

    });

}

function turnoutGatesTableRelatedFunctions() {
    var turnoutGateEditing = null;

    var tableId = $('#turnoutAssetTypeDiv table#turnoutGatesTable');

    turnoutGateTable = tableId.DataTable({
        destroy: true,
        paging: false,
        info: false,
        searching: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [2],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [3],
                visible: false,
                orderable: false,
                searchable: false
            },
        ],
        order: [[4, 'asc']],
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        buttons: [
            {
                text: '+ Add New Gate',
                className: 'btn-success addNewTurnoutGateBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewTurnoutGate();

                    e.stopPropagation();
                }
            }
        ]
    });


    turnoutGateTable.on('order.dt search.dt', function () {
        turnoutGateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addTurnoutGateBtn = turnoutGateTable.button(['.addNewTurnoutGateBtn']);

    function addNewTurnoutGate() {

        if (turnoutGateEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreGateRowData(turnoutGateTable, turnoutGateEditing);
            turnoutGateEditing = null;
            addTurnoutGateBtn.enable();
        }

        var newGateRow = turnoutGateTable.row.add([
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ]).draw(false);

        var newTurnoutGateRowNode = newGateRow.node();

        addNewGateRow(turnoutGateTable, newTurnoutGateRowNode, 'turnout', 'Turnout');

        turnoutGateEditing = newTurnoutGateRowNode;

        addTurnoutGateBtn.disable();
    }

    tableId.on('click', 'a.saveNewTurnoutGateBtn', function (e) {
        e.preventDefault();

        if ($('select[name="turnoutGatesGateTypeList"], select[name="turnoutGatesGateConstructionMaterialTypeList"], input[name="turnoutGatesGateNumbers"], input[name="turnoutGatesGateHeight"], input[name="turnoutGatesGateWidth"], input[name="turnoutGatesGateDiameter"], input[name="turnoutGatesGateInstallationDate"]').valid()) {
            var selectedRow = $(this).parents('tr')[0];

            if (turnoutGateEditing !== null && turnoutGateEditing == selectedRow) {
                /* A different row is being edited - the edit should be cancelled and this row edited */
                saveNewGateRow(turnoutGateTable, turnoutGateEditing, 'editTurnoutGateBtn', 'deleteTurnoutGateBtn');
                turnoutGateEditing = null;
                addTurnoutGateBtn.enable();
            }

        }

    });

    tableId.on('click', 'a.cancelNewTurnoutGateSaveBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        if (turnoutGateEditing !== null && turnoutGateEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            removeNewGateRowCreatedOnCancel(turnoutGateTable, selectedRow);
            turnoutGateEditing = null;
            addTurnoutGateBtn.enable();
        }

    });

    tableId.on('click', 'a.editTurnoutGateBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        if (turnoutGateEditing !== null && turnoutGateEditing != selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreGateRowData(turnoutGateTable, turnoutGateEditing);
            editGateRow(turnoutGateTable, selectedRow, 'turnout', 'Turnout');
            turnoutGateEditing = selectedRow;
            addTurnoutGateBtn.enable();
        } else {
            /* No row currently being edited */
            editGateRow(turnoutGateTable, selectedRow, 'turnout', 'Turnout');
            turnoutGateEditing = selectedRow;
            addTurnoutGateBtn.enable();
        }

    });

    tableId.on('click', 'a.updateRegulatorGateBtn', function (e) {
        e.preventDefault();

        if ($('select[name="turnoutGatesGateTypeList"], select[name="turnoutGatesGateConstructionMaterialTypeList"], input[name="turnoutGatesGateNumbers"], input[name="turnoutGatesGateHeight"], input[name="turnoutGatesGateWidth"], input[name="turnoutGatesGateDiameter"], input[name="turnoutGatesGateInstallationDate"]').valid()) {
            var selectedRow = $(this).parents('tr')[0];

            if (turnoutGateEditing !== null && turnoutGateEditing == selectedRow) {
                /* A different row is being edited - the edit should be cancelled and this row edited */
                updateGateRow(turnoutGateTable, turnoutGateEditing, 'editTurnoutGateBtn', 'deleteTurnoutGateBtn');
                turnoutGateEditing = null;
                addTurnoutGateBtn.enable();
            }

        }

    });

    tableId.on('click', 'a.cancelTurnoutGateEditBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        if (turnoutGateEditing !== null && turnoutGateEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreGateRowData(turnoutGateTable, turnoutGateEditing);
            turnoutGateEditing = null;
            addTurnoutGateBtn.enable();
        }

    });

    tableId.on('click', 'a.deleteTurnoutGateBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        var selectedRow = $(this).parents('tr')[0];

        if (jQuery.Guid.IsValid(turnoutGateTable.row(selectedRow).data()[1].toUpperCase())) {
            var turnoutGateRowDeleted = deleteGateRow(turnoutGateTable.row(selectedRow).data()[1]);

            if (turnoutGateRowDeleted === 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                turnoutGateTable.row(selectedRow).remove().draw(false);
            } else if (turnoutGateRowDeleted === 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
            }
        } else {
            turnoutGateTable.row(selectedRow).remove().draw(false);
        }

        addTurnoutGateBtn.enable();

    });

}

function getGateType(id) {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetGateType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showGateType(id, response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showGateType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Gate Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].GateTypeName, arr[i].GateTypeGuid);
    }
}

function getGateConstructionMaterialType(id) {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetGateConstructionMaterialType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showGateConstructionMaterialType(id, response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showGateConstructionMaterialType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Construction Material Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].GateConstructionMaterialName, arr[i].GateConstructionMaterialGuid);
    }
}


function addNewGateRow(gateTable, selectedRow, assetTypeName, assetTypeName1) {
    var selectedRowData = gateTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    gateTable.column('1:visible').order('asc').draw();

    gateTable.on('order.dt search.dt', function () {

        var x = gateTable.order();

        if (x[0][1] == "asc") {
            var a = gateTable.column(0, { search: 'applied', order: 'applied' }).nodes();
            for (i = 1; i < a.length; i++) {
                a[i].innerHTML = i;
            }
        } else {
            gateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }
        availableTds[0].innerHTML = '*';
    }).draw(false);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[0].innerHTML = '*';
    availableTds[1].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><select class="form-control select1" name="' + assetTypeName + 'GatesGateTypeList" id="' + assetTypeName + 'GatesGateTypeList" style="width:100%;"></select></div></div>';//Gate Type List
    availableTds[2].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><select class="form-control select1" name="' + assetTypeName + 'GatesGateConstructionMaterialTypeList" id="' + assetTypeName + 'GatesGateConstructionMaterialTypeList" style="width:100%;"></select></div></div>';//Gate Construction Material Type List
    availableTds[3].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="' + assetTypeName + 'GatesGateNumbers" name="' + assetTypeName + 'GatesGateNumbers" placeholder="Number of Gates" class="form-control numberOfGates" style="width:100%;" /></div></div>';//Number of Gates
    availableTds[4].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="' + assetTypeName + 'GatesGateHeight" name="' + assetTypeName + 'GatesGateHeight" placeholder="Height" class="form-control height"  style="width:100%;" /></div></div>';//Gate Height
    availableTds[5].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="' + assetTypeName + 'GatesGateWidth" name="' + assetTypeName + 'GatesGateWidth" placeholder="Width" class="form-control width"  style="width:100%;" /></div></div>';//Gate Width
    availableTds[6].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="' + assetTypeName + 'GatesGateDiameter" name="' + assetTypeName + 'GatesGateDiameter" placeholder="Diameter" class="form-control diameter" style="width:100%;" /></div></div>';//Gate Diameter
    availableTds[7].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div id="dp' + assetTypeName1 + 'GatesGateInstallationDate" class="input-group date"><input type="text" name="' + assetTypeName + 'GatesGateInstallationDate" id="' + assetTypeName + 'GatesGateInstallationDate" class="form-control" placeholder="Installation Date (dd/mm/yyyy)" /><span class="input-group-addon form-control-static"><i class="fa fa-calendar fa-fw"></i></span></div></div></div>';//Gate Installation Date
    availableTds[8].innerHTML = '<a href="#" class="saveNew' + assetTypeName1 + 'GateBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelNew' + assetTypeName1 + 'GateSaveBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';

    getGateType(assetTypeName + 'GatesGateTypeList');
    getGateConstructionMaterialType(assetTypeName + 'GatesGateConstructionMaterialTypeList');

    $('#dp' + assetTypeName1 + 'GatesGateInstallationDate').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    });

}

function saveNewGateRow(gateTable, selectedRow, editBtnClass, deleteBtnClass) {
    var selectedRowData = gateTable.row(selectedRow).data();

    var availableSelects = $('select', selectedRow);
    var availableInputs = $('input', selectedRow);

    selectedRowData[1] = jQuery.Guid.Empty();
    selectedRowData[2] = availableSelects[0].value;
    selectedRowData[3] = availableSelects[1].value;
    selectedRowData[4] = availableSelects[0].options[availableSelects[0].selectedIndex].text;
    selectedRowData[5] = availableSelects[1].options[availableSelects[1].selectedIndex].text;
    selectedRowData[6] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
    selectedRowData[7] = capitalizeFirstAllWords($.trim(availableInputs[1].value));
    selectedRowData[8] = capitalizeFirstAllWords($.trim(availableInputs[2].value));
    selectedRowData[9] = capitalizeFirstAllWords($.trim(availableInputs[3].value));
    selectedRowData[10] = capitalizeFirstAllWords($.trim(availableInputs[4].value));
    selectedRowData[11] = '<a href="#" class="' + editBtnClass + '"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="' + deleteBtnClass + '"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    gateTable.row(selectedRow).data(selectedRowData);

    gateTable.on('order.dt search.dt', function () {
        gateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

function removeNewGateRowCreatedOnCancel(gateTable, selectedRow) {

    gateTable
        .row(selectedRow)
        .remove()
        .draw();

    gateTable.on('order.dt search.dt', function () {
        gateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

function editGateRow(gateTable, selectedRow, assetTypeName, assetTypeName1) {
    var selectedRowData = gateTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //Only 9 tds are there, but there will be 12 data. The invisible td will not be counted in tds.
    availableTds[0].innerHTML = '';
    availableTds[1].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><select class="form-control select1" name="' + assetTypeName + 'GatesGateTypeList" id="' + assetTypeName + 'GatesGateTypeList" style="width:100%;"></select></div></div>';//Gate Type List
    availableTds[2].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><select class="form-control select1" name="' + assetTypeName + 'GatesGateConstructionMaterialTypeList" id="' + assetTypeName + 'GatesGateConstructionMaterialTypeList" style="width:100%;"></select></div></div>';//Gate Construction Material Type List
    availableTds[3].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="' + assetTypeName + 'GatesGateNumbers" name="' + assetTypeName + 'GatesGateNumbers" placeholder="Number of Gates" class="form-control numberOfGates" value="' + selectedRowData[6] + '" style="width:100%;" /></div></div>';//Number of Gates
    availableTds[4].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="' + assetTypeName + 'GatesGateHeight" name="' + assetTypeName + 'GatesGateHeight" placeholder="Height" class="form-control height" value="' + selectedRowData[7] + '"  style="width:100%;" /></div></div>';//Gate Height
    availableTds[5].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="' + assetTypeName + 'GatesGateWidth" name="' + assetTypeName + 'GatesGateWidth" placeholder="Width" class="form-control width" value="' + selectedRowData[8] + '"  style="width:100%;" /></div></div>';//Gate Width
    availableTds[6].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="' + assetTypeName + 'GatesGateDiameter" name="' + assetTypeName + 'GatesGateDiameter" placeholder="Diameter" class="form-control diameter" value="' + selectedRowData[9] + '" style="width:100%;" /></div></div>';//Gate Diameter
    availableTds[7].innerHTML = '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div id="dp' + assetTypeName1 + 'GatesGateInstallationDate" class="input-group date"><input type="text" name="' + assetTypeName + 'GatesGateInstallationDate" id="' + assetTypeName + 'GatesGateInstallationDate" class="form-control" value="' + selectedRowData[10] + '" placeholder="Installation Date (dd/mm/yyyy)" /><span class="input-group-addon form-control-static"><i class="fa fa-calendar fa-fw"></i></span></div></div></div>';//Gate Installation Date
    availableTds[8].innerHTML = '<a href="#" class="update' + assetTypeName1 + 'GateBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancel' + assetTypeName1 + 'GateEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';

    getGateType(assetTypeName + 'GatesGateTypeList');
    getGateConstructionMaterialType(assetTypeName + 'GatesGateConstructionMaterialTypeList');

    $('#' + assetTypeName + 'GatesGateTypeList').val(selectedRowData[2]);
    $('#' + assetTypeName + 'GatesGateConstructionMaterialTypeList').val(selectedRowData[3]);

    $('#dp' + assetTypeName1 + 'GatesGateInstallationDate').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    });
}

function updateGateRow(gateTable, selectedRow, editBtnClass, deleteBtnClass) {
    var selectedRowData = gateTable.row(selectedRow).data();

    var availableSelects = $('select', selectedRow);
    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[2] = availableSelects[0].value;
    selectedRowData[3] = availableSelects[1].value;
    selectedRowData[4] = availableSelects[0].options[availableSelects[0].selectedIndex].text;
    selectedRowData[5] = availableSelects[1].options[availableSelects[1].selectedIndex].text;
    selectedRowData[6] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
    selectedRowData[7] = capitalizeFirstAllWords($.trim(availableInputs[1].value));
    selectedRowData[8] = capitalizeFirstAllWords($.trim(availableInputs[2].value));
    selectedRowData[9] = capitalizeFirstAllWords($.trim(availableInputs[3].value));
    selectedRowData[10] = capitalizeFirstAllWords($.trim(availableInputs[4].value));
    selectedRowData[11] = '<a href="#" class="' + editBtnClass + '"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="' + deleteBtnClass + '"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    gateTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;
}

function restoreGateRowData(gateTable, previousRow) {
    var previousRowData = gateTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewGateRowCreatedOnCancel(gateTable, previousRow);
    } else {
        gateTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;
    }

}

function deleteGateRow(gateGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/DeleteGateData",
        data: '{"gateGuid":"' + gateGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}


//Upload Drawing
function uploadAssetDrawing(assetCode) {
    var retValue = false;

    var fileUpload = $("#drawingCode1").get(0);
    var files = fileUpload.files;

    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }

    data.append("assetType", assetCode[2]);
    data.append("assetCode", assetCode);

    var options = {};
    options.url = "Handler1.ashx";
    options.type = "POST";
    options.data = data;
    options.contentType = false;
    options.processData = false;
    options.async = false;
    options.success = function (result) {
        retValue = true;
        drawingUrl = result;
    };
    options.error = function (err) {
        alert(err.statusText);
    };

    $.ajax(options);

    return retValue;
}


//Update Asset Functions

function updateAsset(assetTypeCode) {
    var returnValue = 0;

    var divisionManager = {
        divisionGuid: $('#divisionCode').val()
    };

    var schemeManager = {
        schemeGuid: $('#schemeCode').val()
    };

    var assetTypeManager = {
        assetTypeCode: $('#assetTypeCode').val()
    };

    //var assetDrawingManager = {
    //    drawingFileName: $('#drawingCode').val(),
    //    drawingFileUrl: drawingUrl
    //}

    var initiationDate = $('#dateOfInitiationOfAsset').val();
    initiationDate = Date.parseExact(initiationDate, 'dd/MM/yyyy').toString('yyyy/MM/dd');

    var completionDate = $('#dateOfCompletionOfAsset').val();
    completionDate = Date.parseExact(completionDate, 'dd/MM/yyyy').toString('yyyy/MM/dd');

    var keyAssetGuid = stringIsNullOrEmpty($("#keyAssetList").val()) ? '00000000-0000-0000-0000-000000000000' : $("#keyAssetList").val()

    var assetCost = $('#assetCost').val();
    var newchar = '';
    assetCost = assetCost.split(',').join(newchar);
    assetCost = stringIsNullOrEmpty($.trim(assetCost)) ? parseFloat(0) : parseFloat(assetCost);
    //assetCost = parseFloat(assetCost);

    var assetManager = {
        assetCode: $('#assetCode').val(),
        drawingCode: $('#drawingCode').val(),
        amtd: $('#amtd').val(),
        assetName: $('#assetName').val(),
        startChainage: stringIsNullOrEmpty($.trim($('#chainageStart').val())) ? 0 : parseInt($.trim($('#chainageStart').val())),
        endChainage: stringIsNullOrEmpty($.trim($('#chainageEnd').val())) ? 0 : parseInt($.trim($('#chainageEnd').val())),
        utmEast1: stringIsNullOrEmpty($.trim($('#eastingUS').val())) ? 0 : parseInt($.trim($('#eastingUS').val())),
        utmEast2: stringIsNullOrEmpty($.trim($('#eastingDS').val())) ? 0 : parseInt($.trim($('#eastingDS').val())),
        utmNorth1: stringIsNullOrEmpty($.trim($('#northingUS').val())) ? 0 : parseInt($.trim($('#northingUS').val())),
        utmNorth2: stringIsNullOrEmpty($.trim($('#northingDS').val())) ? 0 : parseInt($.trim($('#northingDS').val())),
        initiationDate: initiationDate,
        completionDate: completionDate,
        assetCostInput: assetCost,
        //divisionManager: divisionManager,
        //schemeManager: schemeManager,
        assetTypeManager: assetTypeManager,
        keyAssetGuid: keyAssetGuid,
        //assetDrawingManager: assetDrawingManager
    }

    if (assetTypeCode === 'BRG') {
        var bridgeTypeManager = {
            bridgeTypeGuid: $('#bridgeTypeList').val(),
        }

        var bridgeManager = {
            bridgeGuid: $('#bridgeGuidHidden').val(),
            length: stringIsNullOrEmpty($.trim($('#bridgeLength').val())) ? 0 : parseFloat($.trim($('#bridgeLength').val())),
            pierNumber: stringIsNullOrEmpty($.trim($('#bridgeNumberOfPiers').val())) ? 0 : parseInt($.trim($('#bridgeNumberOfPiers').val())),
            roadWidth: stringIsNullOrEmpty($.trim($('#bridgeRoadWidth').val())) ? 0 : parseFloat($.trim($('#bridgeRoadWidth').val())),
            bridgeTypeManager: bridgeTypeManager
        }

        assetManager.bridgeManager = bridgeManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'CAN') {
        var canalLiningTypeManager = {
            liningTypeGuid: $('#canalLiningTypeList').val(),
        }

        var canalManager = {
            canalGuid: $('#canalGuidHidden').val(),
            usElevation: stringIsNullOrEmpty($.trim($('#canalUsCrestElevation').val())) ? 0 : parseFloat($.trim($('#canalUsCrestElevation').val())),
            dsElevation: stringIsNullOrEmpty($.trim($('#canalDsCrestElevation').val())) ? 0 : parseFloat($.trim($('#canalDsCrestElevation').val())),
            length: stringIsNullOrEmpty($.trim($('#canalLength').val())) ? 0 : parseFloat($.trim($('#canalLength').val())),
            bedWidth: stringIsNullOrEmpty($.trim($('#canalBedWidth').val())) ? 0 : parseFloat($.trim($('#canalBedWidth').val())),
            slope1: stringIsNullOrEmpty($.trim($('#canalSlipeSlope1').val())) ? 0 : parseFloat($.trim($('#canalSlipeSlope1').val())),
            slope2: stringIsNullOrEmpty($.trim($('#canalSlipeSlope2').val())) ? 0 : parseFloat($.trim($('#canalSlipeSlope2').val())),
            averageDepth: stringIsNullOrEmpty($.trim($('#canalAverageDepth').val())) ? 0 : parseFloat($.trim($('#canalAverageDepth').val())),
            canalLiningTypeManager: canalLiningTypeManager
        }

        assetManager.canalManager = canalManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'CUL') {
        var culvertTypeManager = {
            culvertTypeGuid: $('#culvertTypeList').val(),
        }

        var culvertManager = {
            culvertGuid: $('#culvertGuidHidden').val(),
            ventNumber: stringIsNullOrEmpty($.trim($('#culvertNumberOfVents').val())) ? 0 : parseInt($.trim($('#culvertNumberOfVents').val())),
            ventHeight: stringIsNullOrEmpty($.trim($('#culvertVentHeight').val())) ? 0 : parseFloat($.trim($('#culvertVentHeight').val())),
            ventWidth: stringIsNullOrEmpty($.trim($('#culvertVentWidth').val())) ? 0 : parseFloat($.trim($('#culvertVentWidth').val())),
            lengthCulvert: stringIsNullOrEmpty($.trim($('#culvertLength').val())) ? 0 : parseFloat($.trim($('#culvertLength').val())),
            culvertTypeManager: culvertTypeManager
        }

        assetManager.culvertManager = culvertManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'DRN') {
        var drainageManager = {
            drainageGuid: $('#drainageGuidHidden').val(),
            usElevationDrainage: stringIsNullOrEmpty($.trim($('#drainageUsBedElevation').val())) ? 0 : parseFloat($.trim($('#drainageUsBedElevation').val())),
            dsElevationDrainage: stringIsNullOrEmpty($.trim($('#drainageDsBedElevation').val())) ? 0 : parseFloat($.trim($('#drainageDsBedElevation').val())),
            lengthDrainage: stringIsNullOrEmpty($.trim($('#drainageLength').val())) ? 0 : parseFloat($.trim($('#drainageLength').val())),
            bedWidthDrainage: stringIsNullOrEmpty($.trim($('#drainageBedWidth').val())) ? 0 : parseFloat($.trim($('#drainageBedWidth').val())),
            slope1Drainage: stringIsNullOrEmpty($.trim($('#drainageSlope1').val())) ? 0 : parseFloat($.trim($('#drainageSlope1').val())),
            slope2Drainage: stringIsNullOrEmpty($.trim($('#drainageSlope2').val())) ? 0 : parseFloat($.trim($('#drainageSlope2').val())),
            averageDepthDrainage: stringIsNullOrEmpty($.trim($('#drainageAverageDepth').val())) ? 0 : parseFloat($.trim($('#drainageAverageDepth').val()))
        }

        assetManager.drainageManager = drainageManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'DRP') {
        var dropStructureManager = {
            dropStructureGuid: $('#dropStructureGuidHidden').val(),
            usInvertLevel: stringIsNullOrEmpty($.trim($('#dropStructureUsInvertLevel').val())) ? 0 : parseFloat($.trim($('#dropStructureUsInvertLevel').val())),
            dsInvertLevel: stringIsNullOrEmpty($.trim($('#dropStructureDsInvertLevel').val())) ? 0 : parseFloat($.trim($('#dropStructureDsInvertLevel').val())),
            stillingBasinLength: stringIsNullOrEmpty($.trim($('#dropStructureStillingBasinLength').val())) ? 0 : parseFloat($.trim($('#dropStructureStillingBasinLength').val())),
            stillingBasinWidth: stringIsNullOrEmpty($.trim($('#dropStructureStillingBasinWidth').val())) ? 0 : parseFloat($.trim($('#dropStructureStillingBasinWidth').val()))
        }

        assetManager.dropStructureManager = dropStructureManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'EMB') {
        var embankmentCrestTypeManager = {
            crestTypeGuid: $('#embankmentCrestTypeList').val(),
        }

        var embankmentFillTypeManager = {
            fillTypeGuid: $('#embankmentFillTypeList').val(),
        }

        var embankmentManager = {
            embankmentGuid: $('#embankmentGuidHidden').val(),
            usElevation: stringIsNullOrEmpty($.trim($('#embankmentUsCrestElevation').val())) ? 0 : parseFloat($.trim($('#embankmentUsCrestElevation').val())),
            dsElevation: stringIsNullOrEmpty($.trim($('#embankmentDsCrestElevation').val())) ? 0 : parseFloat($.trim($('#embankmentDsCrestElevation').val())),
            length: stringIsNullOrEmpty($.trim($('#embankmentLength').val())) ? 0 : parseFloat($.trim($('#embankmentLength').val())),
            crestWidth: stringIsNullOrEmpty($.trim($('#embankmentCrestWidth').val())) ? 0 : parseFloat($.trim($('#embankmentCrestWidth').val())),
            csSlope1: stringIsNullOrEmpty($.trim($('#embankmentCsSlope1').val())) ? 0 : parseFloat($.trim($('#embankmentCsSlope1').val())),
            csSlope2: stringIsNullOrEmpty($.trim($('#embankmentCsSlope2').val())) ? 0 : parseFloat($.trim($('#embankmentCsSlope2').val())),
            rsSlope1: stringIsNullOrEmpty($.trim($('#embankmentRsSlope1').val())) ? 0 : parseFloat($.trim($('#embankmentRsSlope1').val())),
            rsSlope2: stringIsNullOrEmpty($.trim($('#embankmentRsSlope2').val())) ? 0 : parseFloat($.trim($('#embankmentRsSlope2').val())),
            bermSlope1: stringIsNullOrEmpty($.trim($('#embankmentBermSlope1').val())) ? 0 : parseFloat($.trim($('#embankmentBermSlope1').val())),
            bermSlope2: stringIsNullOrEmpty($.trim($('#embankmentBermSlope2').val())) ? 0 : parseFloat($.trim($('#embankmentBermSlope2').val())),
            platformWidth: stringIsNullOrEmpty($.trim($('#embankmentPlatformWidth').val())) ? 0 : parseFloat($.trim($('#embankmentPlatformWidth').val())),
            averageHeight: stringIsNullOrEmpty($.trim($('#embankmentAverageHeight').val())) ? 0 : parseFloat($.trim($('#embankmentAverageHeight').val())),
            embankmentCrestTypeManager: embankmentCrestTypeManager,
            embankmentFillTypeManager: embankmentFillTypeManager
        }

        assetManager.embankmentManager = embankmentManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'GAU') {
        var gaugeTypeManager = {
            gaugeTypeGuid: $('#gaugeTypeList').val(),
        }

        var gaugeSeasonTypeManager = {
            seasonTypeGuid: $('#gaugeSeasonTypeList').val(),
        }

        var gaugeFrequencyTypeManager = {
            frequencyTypeGuid: $('#gaugeFrequencyTypeList').val(),
        }

        var startDate = $('#gaugeStartDate').val();
        startDate = stringIsNullOrEmpty($.trim($('#gaugeStartDate').val())) ? "" : Date.parseExact(startDate, 'dd/MM/yyyy').toString('yyyy/MM/dd');

        var endDate = $('#gaugeEndDate').val();
        endDate = stringIsNullOrEmpty($.trim($('#gaugeEndDate').val())) ? "" : Date.parseExact(endDate, 'dd/MM/yyyy').toString('yyyy/MM/dd');

        var gaugeManager = {
            gaugeGuid: $('#gaugeGuidHidden').val(),
            startDate: startDate,
            endDate: endDate,
            active: $('#gaugeActive').is(':checked'),
            lwl: $('#gaugeLwlPresent').is(':checked'),
            hwl: $('#gaugeHwlPresent').is(':checked'),
            levelGeo: $('#gaugeLevelGeo').is(':checked'),
            zeroDatum: stringIsNullOrEmpty($.trim($('#gaugeZeroDatum').val())) ? 0 : parseFloat($.trim($('#gaugeZeroDatum').val())),
            gaugeTypeManager: gaugeTypeManager,
            gaugeFrequencyTypeManager: gaugeFrequencyTypeManager,
            gaugeSeasonTypeManager: gaugeSeasonTypeManager
        }

        assetManager.gaugeManager = gaugeManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'POR') {
        var porcupineTypeManager = {
            porcupineTypeGuid: $('#porcupineTypeList').val(),
        }

        var porcupineMaterialTypeManager = {
            porcupineMaterialTypeGuid: $('#porcupineMaterialTypeList').val(),
        }

        var porcupineManager = {
            porcupineGuid: $('#porcupineGuidHidden').val(),
            screenLength: stringIsNullOrEmpty($.trim($('#porcupineScreenLength').val())) ? 0 : parseFloat($.trim($('#porcupineScreenLength').val())),
            spacingAlongScreen: stringIsNullOrEmpty($.trim($('#porcupineSpacingAlongScreen').val())) ? 0 : parseFloat($.trim($('#porcupineSpacingAlongScreen').val())),
            screenRows: stringIsNullOrEmpty($.trim($('#porcupineScreenRows').val())) ? 0 : parseInt($.trim($('#porcupineScreenRows').val())),
            numberOfLayers: stringIsNullOrEmpty($.trim($('#porcupineNumberOfLayers').val())) ? 0 : parseInt($.trim($('#porcupineNumberOfLayers').val())),
            memberLength: stringIsNullOrEmpty($.trim($('#porcupineMamberLength').val())) ? 0 : parseFloat($.trim($('#porcupineMamberLength').val())),
            lengthPorcupine: stringIsNullOrEmpty($.trim($('#porcupineLength').val())) ? 0 : parseFloat($.trim($('#porcupineLength').val())),
            porcupineTypeManager: porcupineTypeManager,
            porcupineMaterialTypeManager: porcupineMaterialTypeManager,
        }

        assetManager.porcupineManager = porcupineManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'REG') {
        var output = regulatorGateTable.buttons.exportData();

        var regulatorTypeManager = {
            regulatorTypeGuid: $('#regulatorTypeList').val(),
        }

        var regulatorGateTypeManager = {
            regulatorGateTypeGuid: $('#regulatorGateTypeList').val(),
        }

        var gateManager = [];

        for (var i = 0; i < output.body.length; i++) {
            var gateTypeManager = {
                gateTypeGuid: output.body[i][2],
            };

            var gateConstructionMaterialManager = {
                gateConstructionMaterialGuid: output.body[i][3],
            }

            gateManager.push({
                gateGuid: output.body[i][1],
                gateTypeManager: gateTypeManager,
                gateConstructionMaterialManager: gateConstructionMaterialManager,
                numberOfGates: output.body[i][6],
                height: stringIsNullOrEmpty(output.body[i][7]) ? 0 : parseFloat(output.body[i][7]),
                width: stringIsNullOrEmpty(output.body[i][8]) ? 0 : parseFloat(output.body[i][8]),
                diameter: stringIsNullOrEmpty(output.body[i][9]) ? 0 : parseFloat(output.body[i][9]),
                installDate: Date.parseExact(output.body[i][10], 'dd/MM/yyyy').toString('yyyy/MM/dd'),
            });
        }

        var regulatorManager = {
            regulatorGuid: $('#regulatorGuidHidden').val(),
            ventNumberRegulator: stringIsNullOrEmpty($.trim($('#regulatorNumberOfVents').val())) ? 0 : parseInt($.trim($('#regulatorNumberOfVents').val())),
            ventHeightRegulator: stringIsNullOrEmpty($.trim($('#regulatorVentHeight').val())) ? 0 : parseFloat($.trim($('#regulatorVentHeight').val())),
            ventWidthRegulator: stringIsNullOrEmpty($.trim($('#regulatorVentWidth').val())) ? 0 : parseFloat($.trim($('#regulatorVentWidth').val())),
            regulatorTypeManager: regulatorTypeManager,
            regulatorGateTypeManager: regulatorGateTypeManager,
            gateManagerList: gateManager,
        }

        assetManager.regulatorManager = regulatorManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'REV') {
        var revetmentTypeManager = {
            revetTypeGuid: $('#revetmentTypeList').val(),
        }

        var revetmentRiverprotTypeManager = {
            riverprotTypeGuid: $('#revetmentRiverProtectionTypeList').val(),
        }

        var revetmentWaveprotTypeManager = {
            waveprotTypeGuid: $('#revetmentWaveProtectionTypeList').val(),
        }

        var revetmentManager = {
            revetmentGuid: $('#revetmentGuidHidden').val(),
            lengthRevetment: stringIsNullOrEmpty($.trim($('#revetmentLength').val())) ? 0 : parseFloat($.trim($('#revetmentLength').val())),
            plainWidth: stringIsNullOrEmpty($.trim($('#revetmentPlainWidth').val())) ? 0 : parseFloat($.trim($('#revetmentPlainWidth').val())),
            slopeRevetment: stringIsNullOrEmpty($.trim($('#revetmentSlope').val())) ? 0 : parseInt($.trim($('#revetmentSlope').val())),
            revetmentTypeManager: revetmentTypeManager,
            revetmentRiverprotTypeManager: revetmentRiverprotTypeManager,
            revetmentWaveprotTypeManager: revetmentWaveprotTypeManager
        }

        assetManager.revetmentManager = revetmentManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'SLU') {
        var output = sluiceGateTable.buttons.exportData();

        var sluiceTypeManager = {
            sluiceTypeGuid: $('#sluiceTypeList').val(),
        };

        var gateManager = [];

        for (var i = 0; i < output.body.length; i++) {
            var gateTypeManager = {
                gateTypeGuid: output.body[i][2],
            };

            var gateConstructionMaterialManager = {
                gateConstructionMaterialGuid: output.body[i][3],
            }

            gateManager.push({
                gateGuid: output.body[i][1],
                gateTypeManager: gateTypeManager,
                gateConstructionMaterialManager: gateConstructionMaterialManager,
                numberOfGates: output.body[i][6],
                height: stringIsNullOrEmpty(output.body[i][7]) ? 0 : parseFloat(output.body[i][7]),
                width: stringIsNullOrEmpty(output.body[i][8]) ? 0 : parseFloat(output.body[i][8]),
                diameter: stringIsNullOrEmpty(output.body[i][9]) ? 0 : parseFloat(output.body[i][9]),
                installDate: Date.parseExact(output.body[i][10], 'dd/MM/yyyy').toString('yyyy/MM/dd'),
            });
        }

        var sluiceManager = {
            sluiceGuid: $('#sluiceGuidHidden').val(),
            ventNumberSluice: stringIsNullOrEmpty($.trim($('#sluiceNumberOfVents').val())) ? 0 : parseInt($.trim($('#sluiceNumberOfVents').val())),
            ventDiameterSluice: stringIsNullOrEmpty($.trim($('#sluiceVentDiameter').val())) ? 0 : parseFloat($.trim($('#sluiceVentDiameter').val())),
            ventHeightSluice: stringIsNullOrEmpty($.trim($('#sluiceVentHeight').val())) ? 0 : parseFloat($.trim($('#sluiceVentHeight').val())),
            ventWidthSluice: stringIsNullOrEmpty($.trim($('#sluiceVentWidth').val())) ? 0 : parseFloat($.trim($('#sluiceVentWidth').val())),
            sluiceTypeManager: sluiceTypeManager,
            gateManagerList: gateManager
        };

        assetManager.sluiceManager = sluiceManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'SPU') {
        var spurConstructionTypeManager = {
            constructionTypeGuid: $('#spurConstructionTypeList').val(),
        }

        var spurRevetTypeManager = {
            spurRevetTypeGuid: $('#spurRevetTypeList').val(),
        }

        var spurShapeTypeManager = {
            shapeTypeGuid: $('#spurShapeTypeList').val(),
        }

        var spurManager = {
            spurGuid: $('#spurGuidHidden').val(),
            orientation: stringIsNullOrEmpty($.trim($('#spurOrientation').val())) ? 0 : parseFloat($.trim($('#spurOrientation').val())),
            lengthSpur: stringIsNullOrEmpty($.trim($('#spurLength').val())) ? 0 : parseFloat($.trim($('#spurLength').val())),
            widthSpur: stringIsNullOrEmpty($.trim($('#spurWidth').val())) ? 0 : parseFloat($.trim($('#spurWidth').val())),
            spurConstructionTypeManager: spurConstructionTypeManager,
            spurRevetTypeManager: spurRevetTypeManager,
            spurShapeTypeManager: spurShapeTypeManager
        }

        assetManager.spurManager = spurManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'TRN') {
        var output = gateTable.buttons.exportData();

        var turnoutTypeManager = {
            turnoutTypeGuid: $('#turnoutTypeList').val(),
        }

        var turnoutGateTypeManager = {
            turnoutGateTypeGuid: $('#turnoutGateTypeList').val(),
        }

        var gateManager = [];

        for (var i = 0; i < output.body.length; i++) {
            var gateTypeManager = {
                gateTypeGuid: output.body[i][2],
            };

            var gateConstructionMaterialManager = {
                gateConstructionMaterialGuid: output.body[i][3],
            }

            gateManager.push({
                gateGuid: output.body[i][1],
                gateTypeManager: gateTypeManager,
                gateConstructionMaterialManager: gateConstructionMaterialManager,
                numberOfGates: output.body[i][6],
                height: stringIsNullOrEmpty(output.body[i][7]) ? 0 : parseFloat(output.body[i][7]),
                width: stringIsNullOrEmpty(output.body[i][8]) ? 0 : parseFloat(output.body[i][8]),
                diameter: stringIsNullOrEmpty(output.body[i][9]) ? 0 : parseFloat(output.body[i][9]),
                installDate: Date.parseExact(output.body[i][10], 'dd/MM/yyyy').toString('yyyy/MM/dd'),
            });
        }

        var turnoutManager = {
            turnoutGuid: $('#turnoutGuidHidden').val(),
            inletboxWidth: stringIsNullOrEmpty($.trim($('#turnoutInletBoxWidth').val())) ? 0 : parseFloat($.trim($('#turnoutInletBoxWidth').val())),
            inletboxLength: stringIsNullOrEmpty($.trim($('#turnoutInletBoxLength').val())) ? 0 : parseFloat($.trim($('#turnoutInletBoxLength').val())),
            outletNumber: stringIsNullOrEmpty($.trim($('#turnoutOutletNumber').val())) ? 0 : parseInt($.trim($('#turnoutOutletNumber').val())),
            outletWidth: stringIsNullOrEmpty($.trim($('#turnoutOutletWidth').val())) ? 0 : parseFloat($.trim($('#turnoutOutletWidth').val())),
            outletHeight: stringIsNullOrEmpty($.trim($('#turnoutOutletHeight').val())) ? 0 : parseFloat($.trim($('#turnoutOutletHeight').val())),
            turnoutTypeManager: turnoutTypeManager,
            turnoutGateTypeManager: turnoutGateTypeManager,
            gateManagerList: gateManager
        }

        assetManager.turnoutManager = turnoutManager;

        returnValue = updateAssetData(assetManager);
    }
    else if (assetTypeCode === 'WEI') {
        weirTypeManager = {
            weirTypeGuid: $('#weirTypeList').val(),
        }

        weirManager = {
            weirGuid: $('#weirGuidHidden').val(),
            invertLevel: stringIsNullOrEmpty($.trim($('#weirInvertLevel').val())) ? 0 : parseFloat($.trim($('#weirInvertLevel').val())),
            crestTopLevel: stringIsNullOrEmpty($.trim($('#weirCrestTopLevel').val())) ? 0 : parseFloat($.trim($('#weirCrestTopLevel').val())),
            crestTopWidth: stringIsNullOrEmpty($.trim($('#weirCrestTopWidth').val())) ? 0 : parseFloat($.trim($('#weirCrestTopWidth').val())),
            weirTypeManager: weirTypeManager
        }

        assetManager.weirManager = weirManager;

        returnValue = updateAssetData(assetManager);
    }
    else {
        returnValue = 0;
    }

    return returnValue;
}

function updateAssetData(assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditSIOAsset.aspx/UpdateAsset",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}