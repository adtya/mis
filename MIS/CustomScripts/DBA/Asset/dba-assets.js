﻿var viewSelectedRow = null;

$(document).ready(function () {
    var selectedDivision;
    var selectedDivisionText;
    var selectedSchemeText;
    var selectedAssetTypeText;

    showDivisionOptions('divisionList', getDivisions());

    //showSchemeOptions('schemeList', getSchemes());

    var table = $('table#assetListForDBA').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: true
            },
            {
                className: 'never',//className:'hidden',
                targets: [4],
                visible: false,
                orderable: true
            },
            {
                targets: [5],
                orderable: true
            },
            {
                className: 'never',//className:'hidden',
                targets: [6],
                visible: false,
                orderable: true
            },
            {
                targets: [7],
                orderable: true
            },
            {
                className: 'never',//className:'hidden',
                targets: [8],
                visible: false,
                orderable: true
            },
            {               
                targets: [9],                
                orderable: true,
            },
            {
                className: 'never',//className:'hidden',
                targets: [10],
                visible: false,
                orderable: true,
                
            },
            {
                className: 'never',//className:'hidden',
                targets: [11],
                visible: false,
                orderable: true,
            },
            {
                className: 'never',//className:'hidden',
                targets: [12],
                visible: false,
                orderable: true,
            },
            {
                className: 'never',//className:'hidden',
                targets: [13],
                visible: false,
                orderable: true,
            },
            {
                className: 'never',//className:'hidden',
                targets: [14],
                visible: false,
                orderable: true,
            },
            {
                className: 'operations',
                targets: [15],
                orderable: false
            }
        ],
        order: [[2, 'desc']],
        language: {
           lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',       
        buttons: [
            {
                text: '+ Create New Asset',
                className: 'btn-success addNewAssetBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();
                    window.location = 'CreateAsset.aspx';
                    e.stopPropagation();
                }                
            },
            {
                extend: 'print',
                className: 'btn-default',
                exportOptions: {
                    columns: [0, 2, 3, 5, 7, 9]
                }
            }
        ]
    });

    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
            table.cell(cell).invalidate('dom');
        });
    }).draw();

    //Scheme List
    $('#schemeList').multiselect({
        maxHeight: 150,
        numberDisplayed: 1,
        nonSelectedText: '--Select Scheme--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        onChange: function (element, checked) {
            //$(element).closest('.multiselect').valid();

            if (checked === true) {
                var selectedSchemeGuid = $(element).val();
                var selectedSchemeText = $(element).text();

                if (selectedSchemeGuid === '') {
                    table.column(6).search('').draw();
                } else {
                    var selectedSchemeTextSplit = multiSplit(selectedSchemeText, [' (', ')']);

                    var val = $.fn.dataTable.util.escapeRegex(selectedSchemeTextSplit[1]);

                    table.column(6).search(val ? '^' + val + '$' : '', true, false).draw();
                }

                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>',
        }
    });

    //Division List
    $('#divisionList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select Division--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$(element).closest('.multiselect').valid();

            if (checked === true) {
                var selectedDivisionGuid = $(element).val();
                var selectedDivisionText = $(element).text();

                if (selectedDivisionGuid === '') {
                    $('#schemeList').multiselect('select', '', true);

                    //$('#schemeList').multiselect('refresh');

                    showSchemeOptionsToDbaAssets('schemeList', []);

                    table.column(4).search('').draw();
                } else {
                    showSchemeOptionsToDbaAssets('schemeList', getSchemes(selectedDivisionGuid));

                    var selectedDivisionTextSplit = multiSplit(selectedDivisionText, [' (', ')']);

                    val = $.fn.dataTable.util.escapeRegex(selectedDivisionTextSplit[1]);

                    table.column(4).search(val ? '^' + val + '$' : '', true, false).draw();
                }

                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    //AssetType List
    $('#assetTypeList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select AssetType--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$(element).closest('.multiselect').valid();

            if (checked === true) {
                var selectedAssetTypeGuid = $(element).val();
                var selectedAssetTypeText = $(element).text();

                if (selectedAssetTypeGuid === '') {
                    table.column(8).search('').draw();
                } else {
                    var selectedAssetTypeTextSplit = multiSplit(selectedAssetTypeText, [' (', ')']);

                    var val = $.fn.dataTable.util.escapeRegex(selectedAssetTypeTextSplit[1]);

                    table.column(8).search(val ? '^' + val + '$' : '', true, false).draw();
                }

                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#dpDateOfInitiationYear').datepicker({
        format: "yyyy",
        minView: 2,
        startView: 2,
        minViewMode: "years",
        maxViewMode: 2,//2-years
    }).on('changeDate', function (ev) {

        var searchInitiationYear = new Date(ev.date.valueOf()).toString('yyyy');

        var val = $.fn.dataTable.util.escapeRegex(searchInitiationYear);

        table.column(10).search(val ? '^' + val + '$' : '', true, false).draw();

        $(this).blur();
        $(this).datepicker('hide');
    });

    $('#dpDateOfCompletionYear').datepicker({
        format: "yyyy",
        minView: 2,
        startView: 2,
        minViewMode: "years",
        maxViewMode: 2,//2-years
    }).on('changeDate', function (ev) {

        var searchCompletionYear = new Date(ev.date.valueOf()).toString('yyyy');

        var val = $.fn.dataTable.util.escapeRegex(searchCompletionYear);

        table.column(11).search(val ? '^' + val + '$' : '', true, false).draw();

        $(this).blur();
        $(this).datepicker('hide');
    });
    
    $(document).on('click', '#assetListForDBA a.viewAssetListBtn', function (e) {
        e.preventDefault();

        viewSelectedRow = $(this).parents('tr')[0];

        var assetData = getAssetModalDetails(table.row(viewSelectedRow).data()[1], table.row(viewSelectedRow).data()[8]);

        $("#viewAssetPopupModal .modal-body").empty();

        $("#viewAssetPopupModal .modal-body").append(assetData);

        $('#viewAssetPopupModal').modal({
            backdrop: 'static',
            keyboard: false//,
            //show: true
        });

    });

    $(document).on('click', '#assetListForDBA a.editAssetBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        var sessionCreated = setAssetSessionParameters(table.row(selectedRow).data()[1], table.row(selectedRow).data()[14],table.row(selectedRow).data()[12],table.row(selectedRow).data()[13],table.row(selectedRow).data()[8]);

        if (sessionCreated === true) {
            window.location = 'EditAsset.aspx';
        }

    });

    $('#clearSearchBtn').on('click', function (e) {
        e.preventDefault();

        table.column(4).search('').draw();
        table.column(6).search('').draw();
        table.column(8).search('').draw();        
        table.column(10).search('').draw();
        table.column(11).search('').draw();

        $('#divisionList').append('<option value="" selected="selected">' + '--Select Division--' + '</option>');
        $('#divisionList').multiselect('rebuild');

        $('#schemeList').append('<option value="" selected="selected">' + '--Select Scheme--' + '</option>');
        $('#schemeList').multiselect('rebuild');

        $('#assetTypeList').append('<option value="" selected="selected">' + '--Select AssetType--' + '</option>');
        $('#assetTypeList').multiselect('rebuild');

        $('#dateOfInitiationYear').val("");
        $('#dateOfCompletionYear').val("");

    });

    $('#printAssetBtn').on('click', function () {

        //var selectedRow = $(this).parents('tr')[0];

        var assetData = getAssetModalDetails(table.row(viewSelectedRow).data()[1], table.row(viewSelectedRow).data()[8]);

        var myWindow = window.open("");

        myWindow.document.write(assetData);

        myWindow.print();

    });

    $('#closeBtn').on('click', function () {
        $("#viewAssetPopupModal .modal-body").empty();
    });

});

setAssetSessionParameters = function (assetGuid,assetTypeGuid,divisionGuid,schemeGuid,assetTypeCode) {

    $("#preloader").show();
    $("#status").show();

    var result = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAAssets.aspx/SetAssetSessionParameters",
        data: '{"assetGuid":"' + assetGuid + '","assetTypeGuid":"' + assetTypeGuid + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeCode":"' + assetTypeCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            result = true;
            str = response.d;
           
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });

    return result;
}

getAssetModalDetails = function (assetGuid, assetTypeCode) {

    var str;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAAssets.aspx/ViewAssetData",
        data: '{"assetGuid":"' + assetGuid + '","assetTypeCode":"' + assetTypeCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //str = str1;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return str;
}

//getSchemes= function () {
//    var retValue;
//    $.ajax({
//        type: "Post",
//        async: false,
//        url: "DBAAssets.aspx/DisplaySchemeListForAsset",
//        //data: '{"workItemGuid":"' + workItemGuid + '"}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            retValue = response.d;
//        },
//        failure: function (result) {
//            bootbox.alert("Error");
//        }
//    });
//    return retValue;
//}

showSchemeOptionsToDbaAssets = function (id, schemeArray) {

    var options = [];

    if (schemeArray.length > 0) {
        options.push({
            label: '--Select Scheme--',
            value: '',
            selected: true
        });

        for (var i = 0; i < schemeArray.length ; i++) {
            options.push({
                label: schemeArray[i].SchemeName + ' (' + schemeArray[i].SchemeCode + ')',
                value: schemeArray[i].SchemeGuid,
            });
        }
    }

    $('#' + id).multiselect('dataprovider', options);

    $('#' + id).multiselect('refresh');

}