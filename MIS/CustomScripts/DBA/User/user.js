﻿//var unitTypeTable;

$(document).ready(function () {

    $('#userListMenuBtn').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetUsers",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createUnitTypePopupForm(response.d);
            },
            failure: function (result) {
                bootbox.alert("Error");
            }
        });

        e.stopPropagation();
    });

    $('#unitTypePopupModal').on('click', '.modal-footer button#unitTypePopupModalCloseBtn', function (e) {
        e.preventDefault();

        //unitTypeTable.buttons().destroy();

        //unitTypeTable.destroy();

        location.reload(true);

        e.stopPropagation();
    });

});

createUnitTypePopupForm = function (unitTypeData) {
    //$('#unitTypePopupModal').on('show.bs.modal', function (e) {
    //    //$('#unitTypePopupModal a.btn').on('click', function (e) {
    //    //    //$('#unitTypePopupModal').modal('hide');     // dismiss the dialog
    //    //});
    //    unitTypeTableRelatedFunctions();
    //});

    //$('#unitTypePopupModal').on('shown.bs.modal', function (e) {
    //    //unitTypeTableRelatedFunctions();
    //});

    //$('#unitTypePopupModal').on('hide.bs.modal', function (e) {    // remove the event listeners when the dialog is dismissed
    //    //$('#unitTypePopupModal a.btn').off('click');
    //});

    //$('#unitTypePopupModal').on('hidden.bs.modal', function (e) {  // remove the actual elements from the DOM when fully hidden
    //    //$('#unitTypePopupModal').remove();
    //    //$('#unitTypePopupModal .modal-body').empty();
    //    //$(this).data('bs.modal', null);
    //    //alert("aditya");
    //});

    $("#unitTypePopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Unit Types</h2>';

    html += '<div class="panel-body">';
    html += '<form id="unitTypePopupForm" name="unitTypePopupForm" role="form" class="form form-horizontal" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    //if (UnitTypeData.length > 0) {
    html += '<div class="table-responsive">';
    html += '<table id="unitTypesList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'UnitType Guid' + '</th>';
    html += '<th>' + 'UnitType Name' + '</th>';
    html += '<th>' + 'Unit List' + '</th>';
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    for (var i = 0; i < unitTypeData.length; i++) {
        html += '<tr>';

        html += '<td>' + '' + '</td>';
        html += '<td>' + unitTypeData[i].UnitTypeGuid + '</td>';
        html += '<td>' + unitTypeData[i].UnitTypeName + '</td>';
        html += '<td>' + '<a href="#" class="unitListMenuBtn">Unit</a>' + '</td>';
        html += '<td>' + '<a href="#" class="viewUnitTypeBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editUnitTypeBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteUnitTypeBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';
        html += '</tr>';
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';
    //} else {
    // html += '<p>There is no Unit Type available to be displayed.</p>';
    // }

    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#unitTypePopupModal .modal-body").append(html);

    //$('#unitTypePopupModal').modal('show');

    $('#unitTypePopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    unitTypeTableRelatedFunctions();

    $('#unitTypePopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            unitTypeName: {
                required: true,
                noSpace: true
            }
        },
        messages: {
            unitTypeName: {
                required: "Please Enter the UnitType Name.",
                noSpace: "UnitType Name cannot be empty."
            }
        },
        //errorPlacement: function (error, element) {

        //    switch (element.attr("name")) {
        //        case "dpProgressMonth":
        //            error.insertAfter($("#dpProgressMonth"));
        //            break;

        //        default:
        //            //nothing
        //    }
        //}
    });

}

unitTypeTableRelatedFunctions = function () {
    var unitTypeEditing = null;

    var unitTypeTable = $('#unitTypePopupModal table#unitTypesList').DataTable({
    //unitTypeTable = $('#unitTypesList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        //destroy: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false,
                searchable: false
            },
            {
                targets: [4],
                orderable: false,
                searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewUnitTypeBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editUnitTypeBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteUnitTypeBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>'
            }
        ],
        order: [[2, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Create New UnitType',
                className: 'btn-success addNewUnitTypeBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewUnitType();

                    e.stopPropagation();
                }
            }
        ]

    });

    unitTypeTable.on('order.dt search.dt', function () {
        unitTypeTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addUnitTypeBtn = unitTypeTable.button(['.addNewUnitTypeBtn']);

    addNewUnitType = function () {

        if (unitTypeEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreUnitTypeRowData(unitTypeTable, unitTypeEditing);
            unitTypeEditing = null;
            addUnitTypeBtn.enable();
        }

        var newUnitTypeRow = unitTypeTable.row.add([
            '',
            '',
            '',
            '',
            ''
        ]).draw(false);

        var newUnitTypeRowNode = newUnitTypeRow.node();

        addNewUnitTypeRow(unitTypeTable, newUnitTypeRowNode);

        unitTypeEditing = newUnitTypeRowNode;

        addUnitTypeBtn.disable();

        $('#unitTypePopupModal input#unitTypeName').focus();

        $(newUnitTypeRowNode).css('color', 'red').animate({ color: 'black' });
    };

    $('#unitTypePopupModal').on('click', '#unitTypesList a.saveNewUnitTypeBtn', function (e) {
        e.preventDefault();

        if ($("#unitTypePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var checkExistence = checkUnitTypeExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a UnitType with this name.");
            } else if (checkExistence == false) {
                var unitTypeSavedGuid = saveUnitType();

                if (jQuery.Guid.IsValid(unitTypeSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(unitTypeSavedGuid.toUpperCase())) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your UnitType is created successfully.", function () {
                        if (unitTypeEditing !== null && unitTypeEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            saveNewUnitTypeRow(unitTypeTable, unitTypeEditing, unitTypeSavedGuid);
                            unitTypeEditing = null;
                            addUnitTypeBtn.enable();
                        }
                    });
                } else {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }
            }

        }

    });

    $('#unitTypePopupModal').on('click', '#unitTypesList a.cancelNewUnitTypeEditBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (unitTypeEditing !== null && unitTypeEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            removeNewUnitTypeRowCreatedOnCancel(unitTypeTable, selectedRow);
            unitTypeEditing = null;
            addUnitTypeBtn.enable();
        }

    });

    $('#unitTypePopupModal').on('click', '#unitTypesList a.unitListMenuBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];
        var unitTypeGuid = unitTypeTable.row(selectedRow).data()[1];
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetUnits",
            data: '{"unitTypeGuid":"' + unitTypeGuid + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#unitTypePopupModal').modal('hide');
                createUnitPopupForm(response.d, unitTypeGuid);
            },
            failure: function (result) {
                bootbox.alert("Error");
            }
        });

        //e.stopPropagation();
    });

    $('#unitTypePopupModal').on('click', '#unitTypesList a.viewUnitTypeBtn', function (e) {
        e.preventDefault();

        ///* Get the row as a parent of the link that was clicked on */
        //var selectedRow = $(this).parents('tr')[0];

        ////var selectedRowIndex = unitTypeTable.row($(this).parents('tr')).index();

        //if (workItemEditing !== null && workItemEditing != selectedRow) {
        //    /* A different row is being edited - the edit should be cancelled and this row edited */
        //    restoreWorkItemRowData(unitTypeTable, workItemEditing);
        //    editWorkItemRow(unitTypeTable, selectedRow);
        //    workItemEditing = selectedRow;
        //    addWorkItemBtn.enable();
        //}
        //    //else if (nEditing == selectedRow && this.innerHTML == "Save") {
        //    //    /* This row is being edited and should be saved */
        //    //    saveRow(oTable, nEditing);
        //    //    nEditing = null;
        //    //    createBtnClicked = 0;
        //    //}
        //else {
        //    /* No row currently being edited */
        //    editWorkItemRow(unitTypeTable, selectedRow);
        //    workItemEditing = selectedRow;
        //    addWorkItemBtn.enable();
        //}
    });

    $('#unitTypePopupModal').on('click', '#unitTypesList a.editUnitTypeBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        //var selectedRowIndex = unitTypeTable.row($(this).parents('tr')).index();

        if (unitTypeEditing !== null && unitTypeEditing != selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreUnitTypeRowData(unitTypeTable, unitTypeEditing);
            editUnitTypeRow(unitTypeTable, selectedRow);
            unitTypeEditing = selectedRow;
            addUnitTypeBtn.enable();
            //$('#unitTypePopupModal input#unitTypeName').focus();

            var el = $('#unitTypePopupModal input#unitTypeName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);
        } else {
            /* No row currently being edited */
            editUnitTypeRow(unitTypeTable, selectedRow);
            unitTypeEditing = selectedRow;
            addUnitTypeBtn.enable();
            //$('#unitTypePopupModal input#unitTypeName').focus();

            var el = $('#unitTypePopupModal input#unitTypeName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);
        }
    });

    $('#unitTypePopupModal').on('click', '#unitTypesList a.updateUnitTypeBtn', function (e) {
        e.preventDefault();

        if ($("#unitTypePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var checkExistence = checkUnitTypeExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a UnitType with this name.");
            } else if (checkExistence == false) {
                var unitTypeUpdated = updateUnitType(unitTypeTable.row(selectedRow).data()[1]);

                if (unitTypeUpdated == 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    //$('#workItemSubHeadName').val('');
                    bootbox.alert("Your UnitType is updated successfully.", function () {
                        if (unitTypeEditing !== null && unitTypeEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            updateUnitTypeRow(unitTypeTable, unitTypeEditing);
                            unitTypeEditing = null;
                            addUnitTypeBtn.enable();
                        }
                    });
                } else if (unitTypeUpdated == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
                }
            }

        }

    });

    $('#unitTypePopupModal').on('click', '#unitTypesList a.cancelUnitTypeEditBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (unitTypeEditing !== null && unitTypeEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreUnitTypeRowData(unitTypeTable, unitTypeEditing);
            unitTypeEditing = null;
            addUnitTypeBtn.enable();
        }

    });

    $('#unitTypePopupModal').on('click', '#unitTypesList a.deleteUnitTypeBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        //var selectedRow1 = $(this).parents('tr');

        var unitTypeDeleted = deleteUnitType(unitTypeTable.row(selectedRow).data()[1]);

        if (unitTypeDeleted == 1) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Your UnitType is deleted successfully.", function () {
                unitTypeTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (unitTypeDeleted == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
        }

    });    

}

addNewUnitTypeRow = function (unitTypeTable, selectedRow) {
    var selectedRowData = unitTypeTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //unitTypeTable.order([2, 'asc']).draw();

    unitTypeTable.column('1:visible').order('asc').draw();

    unitTypeTable.on('order.dt search.dt', function () {

        var x = unitTypeTable.order();

        if (x[0][1] == "asc") {
            var a = unitTypeTable.column(0, { search: 'applied', order: 'applied' }).nodes();
            for (i = 1; i < a.length; i++) {
                a[i].innerHTML = i;
            }
        } else {
            unitTypeTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }
        availableTds[0].innerHTML = '*';
    }).draw(false);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[0].innerHTML = '*';
    availableTds[1].innerHTML = '<input type="text" id="unitTypeName" name="unitTypeName" placeholder="UnitType Name" class="form-control text-capitalize" value="' + selectedRowData[2] + '" onfocus="unitTypeNameFocusFunction()" onblur="unitTypeNameBlurFunction()" style="width:100%;">';//UnitType
    availableTds[2].innerHTML = '';//Unit
    availableTds[3].innerHTML = '<a href="#" class="saveNewUnitTypeBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelNewUnitTypeEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

saveNewUnitTypeRow = function (unitTypeTable, selectedRow, unitTypeSavedGuid) {

    var selectedRowData = unitTypeTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    selectedRowData[1] = unitTypeSavedGuid;
    selectedRowData[2] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
    selectedRowData[3] = '<a href="#" class="unitListMenuBtn">Unit</a>';
    selectedRowData[4] = '<a href="#" class="viewUnitTypeBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editUnitTypeBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteUnitTypeBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    unitTypeTable.row(selectedRow).data(selectedRowData);

    unitTypeTable.on('order.dt search.dt', function () {
        unitTypeTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

removeNewUnitTypeRowCreatedOnCancel = function (unitTypeTable, selectedRow) {

    unitTypeTable
        .row(selectedRow)
        .remove()
        .draw();

    unitTypeTable.on('order.dt search.dt', function () {
        unitTypeTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

editUnitTypeRow = function (unitTypeTable, selectedRow) {
    var selectedRowData = unitTypeTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    availableTds[1].innerHTML = '<input type="text" id="unitTypeName" name="unitTypeName" placeholder="UnitType Name" class="form-control text-capitalize" value="' + selectedRowData[2] + '" onfocus="unitTypeNameFocusFunction()" onblur="unitTypeNameBlurFunction()" style="width:100%;">';//UnitType
    availableTds[2].innerHTML = '';
    availableTds[3].innerHTML = '<a href="#" class="updateUnitTypeBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelUnitTypeEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>'
}

updateUnitTypeRow = function (unitTypeTable, selectedRow) {
    var selectedRowData = unitTypeTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[2] = capitalizeFirstAllWords($.trim(availableInputs[0].value));

    unitTypeTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

    //unitTypeTable.draw();
}

restoreUnitTypeRowData = function (unitTypeTable, previousRow) {
    var previousRowData = unitTypeTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewUnitTypeRowCreatedOnCancel(unitTypeTable, previousRow);
    } else {
        unitTypeTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;

        //unitTypeTable.cell(previousRowIndex, 0).innerHTML = previousRowSerialNum;

        //unitTypeTable.draw();
    }

}






unitTypeNameFocusFunction = function () {
    // Focus = Changes the background color of input to yellow
    //document.getElementById("unitTypeName").style.background = "yellow";
    $('#unitTypePopupModal input#unitTypeName').css('background-color', 'yellow');
}

unitTypeNameBlurFunction = function () {
    // No focus = Changes the background color of input to nothing
    //document.getElementById("unitTypeName").style.background = "";
    $('#unitTypePopupModal input#unitTypeName').css('background-color', '');
}






capitalizeFirstAllWords = function (str) {
    var pieces = str.split(" ");
    for (var i = 0; i < pieces.length; i++) {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
}

checkUnitTypeExistence = function () {
    var unitTypeName = $.trim($('#unitTypePopupModal input#unitTypeName').val());

    var retValue = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckUnitTypeExistence",
        data: '{"unitTypeName":"' + unitTypeName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveUnitType = function () {
    var unitTypeName = $.trim($('#unitTypePopupModal input#unitTypeName').val());

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveUnitType",
        data: '{"unitTypeName":"' + unitTypeName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updateUnitType = function (unitTypeGuid) {
    var unitTypeName = $.trim($('#unitTypePopupModal input#unitTypeName').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateUnitType",
        data: '{"unitTypeName":"' + unitTypeName + '","unitTypeGuid":"' + unitTypeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteUnitType = function (unitTypeGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/DeleteUnitType",
        data: '{"unitTypeGuid":"' + unitTypeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}