﻿$(document).ready(function () {

    var dbaProjectsTable = $('table#dbaProjectsList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        //destroy: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false,
                searchable: false
            },
            {
                targets: [4],
                orderable: false,
                searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewDbaProjectBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editDbaProjectBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteDbaProjectBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>'
            }
        ],
        order: [[2, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Create New Project',
                className: 'btn-success addNewProjectBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    //addNewUnitType();

                    e.stopPropagation();
                }
            }
        ]
    });

    dbaProjectsTable.on('order.dt search.dt', function () {
        dbaProjectsTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    dbaProjectsTable.on('select', function (e, dt, type, indexes) {
        var rows = dbaProjectsTable.rows({ selected: true }).count();
        if (rows > 0) {
            $('#deleteUserBtn').removeClass('disabled');
        }
        //var rowData = dbaProjectsTable.rows(indexes).data().toArray();
        //userGuidToBeDeleted.push(rowData[0][2]);
        //if (userGuidToBeDeleted.length > 0) {
        // $('#deleteUserBtn').removeClass('disabled');
        //}
    });

    dbaProjectsTable.on('deselect', function (e, dt, type, indexes) {
        var rows = dbaProjectsTable.rows({ selected: true }).count();
        if (rows > 0) {
            $('#deleteUserBtn').removeClass('disabled');
        } else {
            $('#deleteUserBtn').addClass('disabled');
        }
        //var rowData = dbaProjectsTable.rows(indexes).data().toArray();
        //var index = userGuidToBeDeleted.indexOf(rowData[0][2]);
        //if (index > -1) {
        //    userGuidToBeDeleted.splice(index, 1);
        //}

        //if (userGuidToBeDeleted.length > 0) {
        //    $('#deleteUserBtn').removeClass('disabled');
        //} else {
        //    $('#deleteUserBtn').addClass('disabled');
        //}
    });

    //$('#registeredUserList tbody').on('click', 'tr', function () {
    //    if ($(this).hasClass('selected')) {
    //        //$(this).removeClass('selected');
    //    }
    //    else {
    //        dbaProjectsTable.$('tr.selected').removeClass('selected');
    //        $(this).addClass('selected');
    //    }

    //    rowIndex = table1.row(this).index();

    //    fid0 = dbaProjectsTable.fnGetData(this, 0);//for user_id
    //    var fid1 = dbaProjectsTable.fnGetData(this, 1);//user name
    //    userName = fid1;
    //    var fid2 = dbaProjectsTable.fnGetData(this, 2);//designation
    //    var fid3 = dbaProjectsTable.fnGetData(this, 3);//divison
    //    var fid4 = dbaProjectsTable.fnGetData(this, 4);//department
    //    var fid5 = dbaProjectsTable.fnGetData(this, 5);//emailId
    //    var fid6 = dbaProjectsTable.fnGetData(this, 6);//mobile
    //    var fid7 = dbaProjectsTable.fnGetData(this, 7);//superUser
    //    var fid8 = dbaProjectsTable.fnGetData(this, 8);//admin
    //    var fid9 = dbaProjectsTable.fnGetData(this, 9);//superAdmin
    //    var fid10 = dbaProjectsTable.fnGetData(this, 10);//selfApproval

    //    $("#popupModal .modal-title").empty();
    //    var title = fid1;
    //    $("#popupModal .modal-title").append(title);

    //    $("#popupModal .modal-body").empty();
    //    var html = "<div class=\"desc\">";
    //    html += "<dbaProjectsTable class=\"dbaProjectsTable dbaProjectsTable-striped\" style=\"width:100%\">";
    //    html += "<tr>";
    //    html += "<td>Designation:</td>";
    //    html += "<td>" + fid2 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Divison:</td>";
    //    html += "<td>" + fid3 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Department:</td>";
    //    html += "<td>" + fid4 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>EmailId:</td>";
    //    html += "<td>" + fid5 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Mobile:</td>";
    //    html += "<td>" + fid6 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Self Approval:</td>";
    //    html += "<td>" + fid10 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>SuperUser:</td>";
    //    html += "<td>" + fid7 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Admin:</td>";
    //    html += "<td>" + fid8 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>SuperAdmin:</td>";
    //    html += "<td>" + fid9 + "</td>";
    //    html += "</tr>";
    //    html += "</dbaProjectsTable>";
    //    html += "</div>";
    //    $("#popupModal .modal-body").append(html);//.html('<div class=\"descp\"></div>');
    //    $('#popupModal').modal('show');

    //    $('.selfApproval').on('click', function () {
    //        if ($("#selfApproval" + rowIndex).is(":checked")) {
    //            $("#superUserList" + rowIndex).prop('disabled', 'disabled');
    //            $("#adminList" + rowIndex).prop('disabled', 'disabled');
    //            $("#superAdminList" + rowIndex).prop('disabled', 'disabled');
    //        }
    //        else {
    //            $("#superUserList" + rowIndex).prop('disabled', false);
    //            $("#adminList" + rowIndex).prop('disabled', false);
    //            $("#superAdminList" + rowIndex).prop('disabled', false);
    //        }
    //    });
    //});



    //$('.deleteUserCheck').on('click', function (e) {
    //    myRowIndex = $(this).parent().parent().index();
    //    //myColIndex = $(this).index();
    //    //myColIndex = $(this).parent().index()

    //    //var fid2 = dbaProjectsTable.fnGetData(myRowIndex, 2);//SubChainage Guid

    //    //alert(myRowIndex);

    //    //var count = dbaProjectsTable.rows({ selected: true });

    //    //alert(count);

    //    e.stopPropagation();
    //});

    $('#deleteUserBtn').on('click', function (e) {
        e.preventDefault();

        var rowsData = dbaProjectsTable.rows({ selected: true }).data();
        var userGuidToBeDeleted = [];

        for (var i = 0; i < rowsData.length; i++) {
            userGuidToBeDeleted.push(rowsData[i][2]);
        }

        if ($(this).hasClass('disabled')) {

        } else {
            deleteUserConfirmation(userGuidToBeDeleted);
        }
    });

});