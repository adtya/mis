﻿$(document).ready(function () {

    $('#unitAllListMenuBtn').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetUnitsAll",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createUnitAllPopupForm(response.d);
            },
            failure: function (result) {
                bootbox.alert("Error");
            }
        });

        e.stopPropagation();
    });

    $('#unitAllPopupModal').on('click', '.modal-footer button#unitAllModalCloseBtn', function (e) {
        e.preventDefault();

        location.reload(true);

        e.stopPropagation();
    });

});

createUnitAllPopupForm = function (unitData) {
    $("#unitAllPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Units</h2>';

    html += '<div class="panel-body">';
    html += '<form id="unitAllPopupForm" name="unitAllPopupForm" role="form" class="form form-horizontal" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    //if (UnitTypeData.length > 0) {
    html += '<div class="table-responsive">';
    html += '<table id="unitsAllList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'Unit Type Guid' + '</th>';
    html += '<th>' + 'Unit Type' + '</th>';
    html += '<th>' + 'Unit Guid' + '</th>';
    html += '<th>' + 'Unit Name' + '</th>';
    html += '<th>' + 'Unit Abbr' + '</th>';
    html += '<th>' + 'Unit Symbol' + '</th>';
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    for (var i = 0; i < unitData.length; i++) {
        html += '<tr>';

        html += '<td>' + '' + '</td>';
        html += '<td>' + unitData[i].UnitTypeManager.UnitTypeGuid + '</td>';
        html += '<td>' + unitData[i].UnitTypeManager.UnitTypeName + '</td>';
        html += '<td>' + unitData[i].UnitGuid + '</td>';
        html += '<td>' + unitData[i].UnitName + '</td>';
        html += '<td>' + unitData[i].UnitAbbreviation + '</td>';
        html += '<td>' + unitData[i].UnitSymbol + '</td>';
        html += '<td>' + '' + '</td>';
        //html += '<td>' + '<a href="#" class="viewUnitBtn disabled"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editUnitBtn disabled"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteUnitBtn disabled"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';

        html += '</tr>';
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';
    //} else {
    // html += '<p>There is no Unit Type available to be displayed.</p>';
    // }

    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#unitAllPopupModal .modal-body").append(html);

    //$('#unitPopupModal').modal('show');

    $('#unitAllPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    unitAllTableRelatedFunctions();

    $('#unitAllPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            unitName: {
                required: true,
                noSpace: true
            },
            unitAbbreviation: {
                required: true,
                noSpace: true
            },
            unitSymbol: {
                required: true,
                noSpace: true
            }
        },
        messages: {
            unitName: {
                required: "Please Enter the Unit Name.",
                noSpace: "Unit Name cannot be empty."
            },
            unitAbbreviation: {
                required: "Please Enter the Unit Abbreviation.",
                noSpace: "Unit Abbreviation cannot be empty."
            },
            unitSymbol: {
                required: "Please Enter the Unit Symbol.",
                noSpace: "Unit Symbol cannot be empty."
            }
        },
        //errorPlacement: function (error, element) {

        //    switch (element.attr("name")) {
        //        case "dpProgressMonth":
        //            error.insertAfter($("#dpProgressMonth"));
        //            break;

        //        default:
        //            //nothing
        //    }
        //}
    });

}

unitAllTableRelatedFunctions = function (unitTypeGuid) {
    var unitEditing = null;

    var unitTable = $('#unitAllPopupModal table#unitsAllList').DataTable({
        //var unitTable = $('#unitsList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        //destroy: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [3],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [4],
                orderable: true
            },
            {
                targets: [5],
                orderable: false
            },
            {
                targets: [6],
                orderable: false
            },
            {
                targets: [7],
                orderable: false,
                searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewUnitBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editUnitBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteUnitBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>'
            }
        ],
        order: [[4, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Create New Unit',
                className: 'btn-success addNewUnitBtn disabled',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    //addNewUnit();

                    e.stopPropagation();
                }
            }
        ]

    });

    unitTable.on('order.dt search.dt', function () {
        unitTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    //var addUnitBtn = unitTable.button(['.addNewUnitBtn']);

    //addNewUnit = function () {

    //    if (unitEditing !== null) {
    //        /* A different row is being edited - the edit should be cancelled and this row edited */
    //        restoreUnitRowData(unitTable, unitEditing);
    //        unitEditing = null;
    //        addUnitBtn.enable();
    //    }

    //    var newUnitRow = unitTable.row.add([
    //        '',
    //        unitTypeGuid,
    //        '',
    //        '',
    //        '',
    //        '',
    //        ''
    //    ]).draw(false);

    //    var newUnitRowNode = newUnitRow.node();

    //    addNewUnitRow(unitTable, newUnitRowNode);

    //    unitEditing = newUnitRowNode;

    //    addUnitBtn.disable();

    //    $('#unitPopupModal input#unitName').focus();

    //    $(newUnitRowNode).css('color', 'red').animate({ color: 'black' });
    //};

    //$('#unitPopupModal').on('click', '#unitsList a.saveNewUnitBtn', function (e) {
    //    e.preventDefault();

    //    if ($("#unitPopupForm").valid()) {

    //        $("#preloader").show();
    //        $("#status").show();

    //        /* Get the row as a parent of the link that was clicked on */
    //        var selectedRow = $(this).parents('tr')[0];

    //        var unitTypeGuid = unitTable.row(selectedRow).data()[1];

    //        var checkExistence = checkUnitExistence(unitTypeGuid);

    //        if (checkExistence == true) {
    //            $('#status').delay(300).fadeOut();
    //            $('#preloader').delay(350).fadeOut('slow');
    //            bootbox.alert("There is already a Unit with this name in the selected UnitType.");
    //        } else if (checkExistence == false) {
    //            var unitSavedGuid = saveUnit(unitTypeGuid);

    //            if (jQuery.Guid.IsValid(unitSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(unitSavedGuid.toUpperCase())) {
    //                $('#status').delay(300).fadeOut();
    //                $('#preloader').delay(350).fadeOut('slow');

    //                bootbox.alert("Your Unit is created successfully in the selected UnitType.", function () {
    //                    if (unitEditing !== null && unitEditing == selectedRow) {
    //                        /* A different row is being edited - the edit should be cancelled and this row edited */
    //                        saveNewUnitRow(unitTable, unitEditing, unitTypeGuid, unitSavedGuid);
    //                        unitEditing = null;
    //                        addUnitBtn.enable();
    //                    }
    //                });
    //            } else {
    //                $('#status').delay(300).fadeOut();
    //                $('#preloader').delay(350).fadeOut('slow');
    //                bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
    //            }
    //        }

    //    }

    //});

    //$('#unitPopupModal').on('click', '#unitsList a.cancelNewUnitEditBtn', function (e) {
    //    e.preventDefault();

    //    /* Get the row as a parent of the link that was clicked on */
    //    var selectedRow = $(this).parents('tr')[0];

    //    if (unitEditing !== null && unitEditing == selectedRow) {
    //        /* A different row is being edited - the edit should be cancelled and this row edited */
    //        removeNewUnitRowCreatedOnCancel(unitTable, selectedRow);
    //        unitEditing = null;
    //        addUnitBtn.enable();
    //    }

    //});

    //$('#unitPopupModal').on('click', '#unitsList a.viewUnitBtn', function (e) {
    //    e.preventDefault();

    //    ///* Get the row as a parent of the link that was clicked on */
    //    //var selectedRow = $(this).parents('tr')[0];

    //    ////var selectedRowIndex = unitTable.row($(this).parents('tr')).index();

    //    //if (workItemEditing !== null && workItemEditing != selectedRow) {
    //    //    /* A different row is being edited - the edit should be cancelled and this row edited */
    //    //    restoreWorkItemRowData(unitTable, workItemEditing);
    //    //    editWorkItemRow(unitTable, selectedRow);
    //    //    workItemEditing = selectedRow;
    //    //    addWorkItemBtn.enable();
    //    //}
    //    //    //else if (nEditing == selectedRow && this.innerHTML == "Save") {
    //    //    //    /* This row is being edited and should be saved */
    //    //    //    saveRow(oTable, nEditing);
    //    //    //    nEditing = null;
    //    //    //    createBtnClicked = 0;
    //    //    //}
    //    //else {
    //    //    /* No row currently being edited */
    //    //    editWorkItemRow(unitTable, selectedRow);
    //    //    workItemEditing = selectedRow;
    //    //    addWorkItemBtn.enable();
    //    //}
    //});

    //$('#unitPopupModal').on('click', '#unitsList a.editUnitBtn', function (e) {
    //    e.preventDefault();

    //    /* Get the row as a parent of the link that was clicked on */
    //    var selectedRow = $(this).parents('tr')[0];

    //    //var selectedRowIndex = unitTable.row($(this).parents('tr')).index();

    //    if (unitEditing !== null && unitEditing != selectedRow) {
    //        /* A different row is being edited - the edit should be cancelled and this row edited */
    //        restoreUnitRowData(unitTable, unitEditing);
    //        editUnitRow(unitTable, selectedRow);
    //        unitEditing = selectedRow;
    //        addUnitBtn.enable();
    //        //$('#unitPopupModal input#unitName').focus();

    //        var el = $('#unitPopupModal input#unitName');
    //        var elemLen = el.val().length;
    //        el.focus();

    //        //el.selectRange(0, elemLen);
    //        el.setCursorPosition(elemLen);
    //    } else {
    //        /* No row currently being edited */
    //        editUnitRow(unitTable, selectedRow);
    //        unitEditing = selectedRow;
    //        addUnitBtn.enable();
    //        //$('#unitPopupModal input#unitName').focus();

    //        var el = $('#unitPopupModal input#unitName');
    //        var elemLen = el.val().length;
    //        el.focus();

    //        //el.selectRange(0, elemLen);
    //        el.setCursorPosition(elemLen);
    //    }
    //});

    //$('#unitPopupModal').on('click', '#unitsList a.updateUnitBtn', function (e) {
    //    e.preventDefault();

    //    if ($("#unitPopupForm").valid()) {

    //        $("#preloader").show();
    //        $("#status").show();

    //        /* Get the row as a parent of the link that was clicked on */
    //        var selectedRow = $(this).parents('tr')[0];

    //        var unitTypeGuid = unitTable.row(selectedRow).data()[1];

    //        var checkExistence = checkUnitExistence(unitTypeGuid);

    //        if (checkExistence == true) {
    //            $('#status').delay(300).fadeOut();
    //            $('#preloader').delay(350).fadeOut('slow');
    //            bootbox.alert("There is already a Unit with this name in the selected UnitType.");
    //        } else if (checkExistence == false) {
    //            var unitUpdated = updateUnit(unitTypeGuid, unitTable.row(selectedRow).data()[2]);

    //            if (unitUpdated == 1) {
    //                $('#status').delay(300).fadeOut();
    //                $('#preloader').delay(350).fadeOut('slow');
    //                //$('#workItemSubHeadName').val('');
    //                bootbox.alert("Your Unit is updated successfully.", function () {
    //                    if (unitEditing !== null && unitEditing == selectedRow) {
    //                        /* A different row is being edited - the edit should be cancelled and this row edited */
    //                        updateUnitRow(unitTable, unitEditing);
    //                        unitEditing = null;
    //                        addUnitBtn.enable();
    //                    }
    //                });
    //            } else if (unitUpdated == 0) {
    //                $('#status').delay(300).fadeOut();
    //                $('#preloader').delay(350).fadeOut('slow');
    //                bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
    //            }
    //        }

    //    }

    //});

    //$('#unitPopupModal').on('click', '#unitsList a.cancelUnitEditBtn', function (e) {
    //    e.preventDefault();

    //    /* Get the row as a parent of the link that was clicked on */
    //    var selectedRow = $(this).parents('tr')[0];

    //    if (unitEditing !== null && unitEditing == selectedRow) {
    //        /* A different row is being edited - the edit should be cancelled and this row edited */
    //        restoreUnitRowData(unitTable, unitEditing);
    //        unitEditing = null;
    //        addUnitBtn.enable();
    //    }

    //});

    //$('#unitPopupModal').on('click', '#unitsList a.deleteUnitBtn', function (e) {
    //    e.preventDefault();

    //    $("#preloader").show();
    //    $("#status").show();

    //    /* Get the row as a parent of the link that was clicked on */
    //    var selectedRow = $(this).parents('tr')[0];

    //    //var selectedRow1 = $(this).parents('tr');

    //    var unitDeleted = deleteUnit(unitTable.row(selectedRow).data()[1], unitTable.row(selectedRow).data()[2]);

    //    if (unitDeleted == 1) {
    //        $('#status').delay(300).fadeOut();
    //        $('#preloader').delay(350).fadeOut('slow');

    //        bootbox.alert("Your Unit is deleted successfully.", function () {
    //            unitTable
    //                .row(selectedRow)
    //                .remove()
    //                .draw();
    //        });
    //    } else if (unitDeleted == 0) {
    //        $('#status').delay(300).fadeOut();
    //        $('#preloader').delay(350).fadeOut('slow');
    //        bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
    //    }

    //});

}

//addNewUnitRow = function (unitTable, selectedRow) {
//    var selectedRowData = unitTable.row(selectedRow).data();

//    var availableTds = $('>td', selectedRow);

//    //unitTable.order([2, 'asc']).draw();

//    unitTable.column('1:visible').order('asc').draw();

//    unitTable.on('order.dt search.dt', function () {

//        var x = unitTable.order();

//        if (x[0][1] == "asc") {
//            var a = unitTable.column(0, { search: 'applied', order: 'applied' }).nodes();
//            for (i = 1; i < a.length; i++) {
//                a[i].innerHTML = i;
//            }
//        } else {
//            unitTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
//                cell.innerHTML = i + 1;
//            });
//        }
//        availableTds[0].innerHTML = '*';
//    }).draw(false);

//    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
//    availableTds[0].innerHTML = '*';
//    availableTds[1].innerHTML = '<input type="text" id="unitName" name="unitName" placeholder="Unit Name" class="form-control text-capitalize" value="' + selectedRowData[2] + '" onfocus="unitNameFocusFunction()" onblur="unitNameBlurFunction()">';//Unit Name
//    availableTds[2].innerHTML = '<input type="text" id="unitAbbreviation" name="unitAbbreviation" placeholder="Unit Abbreviation" class="form-control" value="' + selectedRowData[3] + '" onfocus="unitAbbreviationFocusFunction()" onblur="unitAbbreviationNameBlurFunction()">';//Unit Abbreviation
//    availableTds[3].innerHTML = '<input type="text" id="unitSymbol" name="unitSymbol" placeholder="Unit Symbol" class="form-control" value="' + selectedRowData[4] + '" onfocus="unitSymbolFocusFunction()" onblur="unitSymbolNameBlurFunction()">';//Unit Symbol
//    availableTds[4].innerHTML = '<a href="#" class="saveNewUnitBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelNewUnitEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
//}

//saveNewUnitRow = function (unitTable, selectedRow, unitTypeGuid, unitSavedGuid) {

//    var selectedRowData = unitTable.row(selectedRow).data();

//    var availableInputs = $('input', selectedRow);

//    selectedRowData[1] = unitTypeGuid;
//    selectedRowData[2] = unitSavedGuid;
//    selectedRowData[3] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
//    selectedRowData[4] = $.trim(availableInputs[1].value);
//    selectedRowData[5] = $.trim(availableInputs[2].value);
//    selectedRowData[6] = '<a href="#" class="viewUnitBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editUnitBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteUnitBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

//    unitTable.row(selectedRow).data(selectedRowData);

//    unitTable.on('order.dt search.dt', function () {
//        unitTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
//            cell.innerHTML = i + 1;
//        });
//    }).draw();
//}

//removeNewUnitRowCreatedOnCancel = function (unitTable, selectedRow) {

//    unitTable
//        .row(selectedRow)
//        .remove()
//        .draw();

//    unitTable.on('order.dt search.dt', function () {
//        unitTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
//            cell.innerHTML = i + 1;
//        });
//    }).draw();

//}

//editUnitRow = function (unitTable, selectedRow) {
//    var selectedRowData = unitTable.row(selectedRow).data();

//    var availableTds = $('>td', selectedRow);

//    availableTds[1].innerHTML = '<input type="text" id="unitName" name="unitName" placeholder="Unit Name" class="form-control text-capitalize" value="' + selectedRowData[3] + '" onfocus="unitNameFocusFunction()" onblur="unitNameBlurFunction()">';//Unit Name
//    availableTds[2].innerHTML = '<input type="text" id="unitAbbreviation" name="unitAbbreviation" placeholder="Unit Abbreviation" class="form-control" value="' + selectedRowData[4] + '" onfocus="unitAbbreviationFocusFunction()" onblur="unitAbbreviationNameBlurFunction()">';//Unit Abbreviation
//    availableTds[3].innerHTML = '<input type="text" id="unitSymbol" name="unitSymbol" placeholder="Unit Symbol" class="form-control" value="' + selectedRowData[5] + '" onfocus="unitSymbolFocusFunction()" onblur="unitSymbolNameBlurFunction()">';//Unit Symbol
//    availableTds[4].innerHTML = '<a href="#" class="updateUnitBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelUnitEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
//}

//updateUnitRow = function (unitTable, selectedRow) {
//    var selectedRowData = unitTable.row(selectedRow).data();

//    var availableInputs = $('input', selectedRow);

//    var availableTds = $('>td', selectedRow);

//    var selectedRowSerialNum = availableTds[0].innerHTML;

//    selectedRowData[3] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
//    selectedRowData[4] = $.trim(availableInputs[1].value);
//    selectedRowData[5] = $.trim(availableInputs[2].value);

//    unitTable.row(selectedRow).data(selectedRowData);

//    availableTds[0].innerHTML = selectedRowSerialNum;

//    //unitTable.draw();
//}

//restoreUnitRowData = function (unitTable, previousRow) {
//    var previousRowData = unitTable.row(previousRow).data();

//    var availableTds = $('>td', previousRow);

//    var previousRowSerialNum = availableTds[0].innerHTML;

//    if (previousRowSerialNum === '*') {
//        removeNewUnitRowCreatedOnCancel(unitTable, previousRow);
//    } else {
//        unitTable.row(previousRow).data(previousRowData);

//        availableTds[0].innerHTML = previousRowSerialNum;

//        //unitTable.cell(previousRowIndex, 0).innerHTML = previousRowSerialNum;

//        //unitTable.draw();
//    }

//}






//unitNameFocusFunction = function () {
//    // Focus = Changes the background color of input to yellow
//    //document.getElementById("unitTypeName").style.background = "yellow";
//    $('#unitPopupModal input#unitName').css('background-color', 'yellow');
//}

//unitNameBlurFunction = function () {
//    // No focus = Changes the background color of input to nothing
//    //document.getElementById("unitTypeName").style.background = "";
//    $('#unitPopupModal input#unitName').css('background-color', '');
//}

//unitAbbreviationFocusFunction = function () {
//    // Focus = Changes the background color of input to yellow
//    //document.getElementById("unitTypeName").style.background = "yellow";
//    $('#unitPopupModal input#unitAbbreviation').css('background-color', 'yellow');
//}

//unitAbbreviationNameBlurFunction = function () {
//    // No focus = Changes the background color of input to nothing
//    //document.getElementById("unitTypeName").style.background = "";
//    $('#unitPopupModal input#unitAbbreviation').css('background-color', '');
//}

//unitSymbolFocusFunction = function () {
//    // Focus = Changes the background color of input to yellow
//    //document.getElementById("unitTypeName").style.background = "yellow";
//    $('#unitPopupModal input#unitSymbol').css('background-color', 'yellow');
//}

//unitSymbolNameBlurFunction = function () {
//    // No focus = Changes the background color of input to nothing
//    //document.getElementById("unitTypeName").style.background = "";
//    $('#unitPopupModal input#unitSymbol').css('background-color', '');
//}






//capitalizeFirstAllWords = function (str) {
//    var pieces = str.split(" ");
//    for (var i = 0; i < pieces.length; i++) {
//        var j = pieces[i].charAt(0).toUpperCase();
//        pieces[i] = j + pieces[i].substr(1);
//    }
//    return pieces.join(" ");
//}

//checkUnitExistence = function (unitTypeGuid) {
//    var unitName = $.trim($('#unitPopupModal input#unitName').val());

//    var retValue = false;

//    $.ajax({
//        type: "Post",
//        async: false,
//        url: "DBAMethods.asmx/CheckUnitExistence",
//        data: '{"unitName":"' + unitName + '","unitTypeGuid":"' + unitTypeGuid + '"}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            retValue = response.d;
//        },
//        failure: function (msg) {
//            //$('#status').delay(300).fadeOut();
//            //$('#preloader').delay(350).fadeOut('slow');
//            bootbox.alert("Please contact your database administrator.");
//        }
//    });

//    return retValue;
//}

//saveUnit = function (unitTypeGuid) {
//    var unitTypeManager = {
//        unitTypeGuid: unitTypeGuid
//    };

//    var unitManager = {
//        unitName: $.trim($('#unitPopupModal input#unitName').val()),
//        unitAbbreviation: $.trim($('#unitPopupModal input#unitAbbreviation').val()),
//        unitSymbol: $.trim($('#unitPopupModal input#unitSymbol').val()),
//        unitTypeManager: unitTypeManager
//    };

//    var unitManagerJsonString = JSON.stringify(unitManager);

//    var retValue = jQuery.Guid.Empty();

//    $.ajax({
//        type: "Post",
//        async: false,
//        url: "DBAMethods.asmx/SaveUnit",
//        data: '{"unitManager":' + unitManagerJsonString + '}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            retValue = response.d;
//        },
//        failure: function (msg) {
//            //$('#status').delay(300).fadeOut();
//            //$('#preloader').delay(350).fadeOut('slow');
//            bootbox.alert("Please contact your database administrator.");
//        }
//    });

//    return retValue;
//}

//updateUnit = function (unitTypeGuid, unitGuid) {
//    var unitTypeManager = {
//        unitTypeGuid: unitTypeGuid
//    };

//    var unitManager = {
//        unitGuid: unitGuid,
//        unitName: $.trim($('#unitPopupModal input#unitName').val()),
//        unitAbbreviation: $.trim($('#unitPopupModal input#unitAbbreviation').val()),
//        unitSymbol: $.trim($('#unitPopupModal input#unitSymbol').val()),
//        unitTypeManager: unitTypeManager
//    };

//    var unitManagerJsonString = JSON.stringify(unitManager);

//    var retValue = 0;

//    $.ajax({
//        type: "Post",
//        async: false,
//        url: "DBAMethods.asmx/UpdateUnit",
//        data: '{"unitManager":' + unitManagerJsonString + '}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            retValue = response.d;
//        },
//        failure: function (msg) {
//            //$('#status').delay(300).fadeOut();
//            //$('#preloader').delay(350).fadeOut('slow');
//            bootbox.alert("Please contact your database administrator.");
//        }
//    });

//    return retValue;
//}

//deleteUnit = function (unitTypeGuid, unitGuid) {
//    var retValue = 0;

//    $.ajax({
//        type: "Post",
//        async: false,
//        url: "DBAMethods.asmx/DeleteUnit",
//        data: '{"unitTypeGuid":"' + unitTypeGuid + '","unitGuid":"' + unitGuid + '"}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            retValue = response.d;
//        },
//        failure: function (msg) {
//            //$('#status').delay(300).fadeOut();
//            //$('#preloader').delay(350).fadeOut('slow');
//            bootbox.alert("Please contact your database administrator.");
//        }
//    });

//    return retValue;
//}