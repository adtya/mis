﻿$(document).ready(function () {

    $('#editWorkItemPopupModal').on('click', '.modal-footer button#backFromEditWorkItemPopupModalBtn', function (e) {
        e.preventDefault();

        $('#editWorkItemPopupModal').modal('hide');

        $('#workItemPopupModal').modal('show');

        e.stopPropagation();
    });

});

createEditWorkItemPopupForm = function (selectedWorkItemGuid) {
    var arrWorkItemSubHeadsSelectedToEditWorkItem = [];

    var selectedWorkItemData = getWorkItemToEditWorkItem(selectedWorkItemGuid);

    var workItemSubHeadDataToEditWorkItem = getWorkItemSubHeadsToEditWorkItem();

    var unitTypeDataToEditWorkItem = getUnitTypesToEditWorkItem();

    var unitDataToEditWorkItem = getUnitsToEditWokItem(selectedWorkItemData.UnitManager.UnitTypeManager.UnitTypeGuid);

    $("#editWorkItemPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Edit WorkItem</h2>';

    html += '<div class="panel-body">';
    html += '<form id="editWorkItemPopupForm" name="editWorkItemPopupForm" class="form form-horizontal" role="form" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemName" id="workItemNameLabel" class="control-label pull-left">WorkItem Name</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="workItemName" name="workItemName" placeholder="WorkItem Name" class="form-control text-capitalize" value="' + selectedWorkItemData.WorkItemName + '" onfocus="workItemNameFocusFunction()" onblur="workItemNameBlurFunction()" style="width:100%;" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemSubHeadList" id="workItemSubHeadListLabel" class="control-label pull-left">WorkItem SubHead</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    //html += '<div class="btn-group-justified">';
    html += '<select class="form-control multiselect" name="workItemSubHeadList" id="workItemSubHeadList" multiple="multiple" size="5">';

    for (var i = 0; i < workItemSubHeadDataToEditWorkItem.length; i++) {
        var optionSelected = false;
        for (var j = 0; j < selectedWorkItemData.WorkItemSubHeadManager.length; j++) {
            if (workItemSubHeadDataToEditWorkItem[i].WorkItemSubHeadGuid == selectedWorkItemData.WorkItemSubHeadManager[j].WorkItemSubHeadGuid) {
                optionSelected = true;
                break;
            }
        }

        if (optionSelected === true) {
            html += '<option value="' + workItemSubHeadDataToEditWorkItem[i].WorkItemSubHeadGuid + '" selected="selected">';
            html += workItemSubHeadDataToEditWorkItem[i].WorkItemSubHeadName;
            html += '</option>';

            arrWorkItemSubHeadsSelectedToEditWorkItem.push({
                workItemSubHeadGuid: workItemSubHeadDataToEditWorkItem[i].WorkItemSubHeadGuid,
                workItemSubHeadName: workItemSubHeadDataToEditWorkItem[i].WorkItemSubHeadName
            });
        } else {
            html += '<option value="' + workItemSubHeadDataToEditWorkItem[i].WorkItemSubHeadGuid + '">';
            html += workItemSubHeadDataToEditWorkItem[i].WorkItemSubHeadName;
            html += '</option>';
        }


    }

    html += '</select>';
    //html += '</div>';
    html += '</div>';
    html += '</div>';

    //html += '<fieldset class="scheduler-border">';
    //html += '<legend class="scheduler-border">WorkItem Unit</legend>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemUnitTypeList" id="workItemUnitTypeListLabel" class="control-label pull-left">Unit Type</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control multiselect" name="workItemUnitTypeList" id="workItemUnitTypeList">';

    html += '<option value="" selected="selected">' + '--Select UnitType--' + '</option>';
    for (var i = 0; i < unitTypeDataToEditWorkItem.length; i++) {
        if (unitTypeDataToEditWorkItem[i].UnitTypeGuid == selectedWorkItemData.UnitManager.UnitTypeManager.UnitTypeGuid) {
            html += '<option value="' + unitTypeDataToEditWorkItem[i].UnitTypeGuid + '" selected="selected">';
            html += unitTypeDataToEditWorkItem[i].UnitTypeName;
            html += '</option>';
        } else {
            html += '<option value="' + unitTypeDataToEditWorkItem[i].UnitTypeGuid + '">';
            html += unitTypeDataToEditWorkItem[i].UnitTypeName;
            html += '</option>';
        }
    }

    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div id="workItemUnitDiv">';// class="hidden">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemUnitList" id="workItemUnitListLabel" class="control-label pull-left">Unit</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control multiselect" name="workItemUnitList" id="workItemUnitList">';

    html += '<option value="" selected="selected">' + '--Select Unit--' + '</option>';
    for (var i = 0; i < unitDataToEditWorkItem.length; i++) {
        if (unitDataToEditWorkItem[i].UnitGuid == selectedWorkItemData.UnitManager.UnitGuid) {
            html += '<option value="' + unitDataToEditWorkItem[i].UnitGuid + '" selected="selected">';
            html += unitDataToEditWorkItem[i].UnitName;
            html += '</option>';
        } else {
            html += '<option value="' + unitDataToEditWorkItem[i].UnitGuid + '">';
            html += unitDataToEditWorkItem[i].UnitName;
            html += '</option>';
        }
    }

    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';

    html += '<div id="workItemSubHeadListDisplayToEditWorkItem" class="panel panel-default panel-body col-xs-12 col-sm-12 col-md-12 col-lg-12 well well-sm hidden"></div>';

    html += '<div class="form-group">';
    html += '<input id="editWorkItemBtn" name="editWorkItemBtn" class="btn btn-lg btn-primary btn-block" value="Update WorkItem" type="button" />';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#editWorkItemPopupModal .modal-body").append(html);

    displayWorkItemSubHeadsListToEditWorkItem('workItemSubHeadListDisplayToEditWorkItem', arrWorkItemSubHeadsSelectedToEditWorkItem);

    $('#workItemPopupModal').modal('hide');

    //$('#editWorkItemPopupModal').modal('show');

    $('#editWorkItemPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    $('#workItemSubHeadList').multiselect({
        maxHeight: 150,
        enableCaseInsensitiveFiltering: true,
        numberDisplayed: 0,
        //includeSelectAllOption: true,
        nonSelectedText: '--Select WorkItem SubHead--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$('.multiselect').valid();
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                $('#workItemSubHeadListDisplayToEditWorkItem').removeClass('hidden');

                arrWorkItemSubHeadsSelectedToEditWorkItem.push({
                    workItemSubHeadGuid: element.val(),
                    workItemSubHeadName: element.text()
                });

                displayWorkItemSubHeadsListToEditWorkItem('workItemSubHeadListDisplayToEditWorkItem', arrWorkItemSubHeadsSelectedToEditWorkItem);
            }
            else if (checked === false) {
                if (confirm('Do you wish to deselect the element?')) {
                    for (var i = arrWorkItemSubHeadsSelectedToEditWorkItem.length - 1; i >= 0; i--) {
                        if (arrWorkItemSubHeadsSelectedToEditWorkItem[i].workItemSubHeadGuid === element.val()) {
                            arrWorkItemSubHeadsSelectedToEditWorkItem.splice(i, 1);
                        }
                    }

                    displayWorkItemSubHeadsListToEditWorkItem('workItemSubHeadListDisplayToEditWorkItem', arrWorkItemSubHeadsSelectedToEditWorkItem);
                }
                else {
                    $("#workItemSubHeadList").multiselect('select', element.val());

                    displayWorkItemSubHeadsListToEditWorkItem('workItemSubHeadListDisplayToEditWorkItem', arrWorkItemSubHeadsSelectedToEditWorkItem);
                }
            }
        },
        //onSelectAll: function (checked) {
        //    while (arrWorkItemSubHeadsSelectedForWorkItem.length > 0) {
        //        arrWorkItemSubHeadsSelectedForWorkItem.pop();
        //    }
        //    alert(checked);
        //}
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#workItemUnitTypeList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select UnitType--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$('.multiselect').valid();
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                displayUnitsToEditWorkItem(element.val());
                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#workItemUnitList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select Unit--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$('.multiselect').valid();
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#editWorkItemPopupForm').validate({ // initialize plugin
        ignore: ':hidden:not(".multiselect")',
        //ignore: ':hidden:not("#select")',
        //ignore: ":not(:visible)",
        rules: {
            workItemName: {
                required: true,
                noSpace: true
            },
            workItemSubHeadList: "required",
            workItemUnitTypeList: "required",
            workItemUnitList: "required"
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block small',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                element.closest('.form-group').append(error);
            }
        },
        submitHandler: function () {
            alert('valid form');
            return false;
        }
    });

    $('#editWorkItemBtn').on('click', function (e) {
        e.preventDefault();

        if ($("#editWorkItemPopupForm").valid()) {

            if (arrWorkItemSubHeadsSelectedToEditWorkItem.length > 0) {
                $("#preloader").show();
                $("#status").show();

                var checkExistence = checkWorkItemExistenceToAddWorkItem();

                if (checkExistence == true) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("There is already a WorkItem with this name.");
                } else if (checkExistence == false) {

                    for (var index = 0; index < arrWorkItemSubHeadsSelectedToEditWorkItem.length; index++) {
                        arrWorkItemSubHeadsSelectedToEditWorkItem[index].workItemSubHeadSequence = (index + 1);
                    }

                    var workItemSavedGuid = saveWorkItem(arrWorkItemSubHeadsSelectedToEditWorkItem);

                    if (jQuery.Guid.IsValid(workItemSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(workItemSavedGuid.toUpperCase())) {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');

                        bootbox.alert("Your WorkItem is created successfully.", function () {
                            $('#addWorkItemPopupModal').modal('hide');

                            $('#workItemListMenuBtn').click();
                        });
                    } else {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');
                        bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                    }
                }

            } else {
                bootbox.alert("Please Select WorkItem SubHeads.");
            }

        }

    });

}




workItemNameFocusFunction = function () {
    // Focus = Changes the background color of input to yellow
    //document.getElementById("workItemSubHeadName").style.background = "yellow";
    $('#addWorkItemPopupModal input#workItemName').css('background-color', 'yellow');
}

workItemNameBlurFunction = function () {
    // No focus = Changes the background color of input to nothing
    //document.getElementById("workItemSubHeadName").style.background = "";
    $('#addWorkItemPopupModal input#workItemName').css('background-color', '');
}









getWorkItemToEditWorkItem = function (workItemGuid) {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetWorkItem",
        data: '{"workItemGuid":"' + workItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

getWorkItemSubHeadsToEditWorkItem = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetWorkItemSubHeads",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

getUnitTypesToEditWorkItem = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetUnitTypes",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

getUnitsToEditWokItem = function (unitTypeGuid) {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetUnits",
        data: '{"unitTypeGuid":"' + unitTypeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

displayWorkItemSubHeadsListToEditWorkItem = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    var html = '<ul class="list-group text-left">';
    html += '<li class="nav nav-header disabled">';
    html += 'WorkItem SubHeads Selected';
    html += '</li>';
    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += (index + 1) + '. ' + arr[index].workItemSubHeadName;
        html += '</li>';
    }

    html += '</ul>';

    document.getElementById(divname).innerHTML = html;

    if (arr.length == 0) {
        div.addClass('hidden');
    }
}

displayUnitsToEditWorkItem = function (unitTypeGuid) {
    if (jQuery.Guid.IsValid(unitTypeGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(unitTypeGuid.toUpperCase())) {
        $('#workItemUnitDiv').removeClass('hidden');

        var options = [];

        var unitData = getUnitsToEditWokItem(unitTypeGuid);

        options.push({
            label: '--Select Unit--',
            value: '',
            selected: true
        });

        for (var i = 0; i < unitData.length ; i++) {
            options.push({
                label: unitData[i].UnitSymbol,
                value: unitData[i].UnitGuid,
            });

            //var options = [
            //    { label: 'A', title: '', value: '', selected: true },
            //    { label: 'Option 6', title: 'Option 6', value: '6', disabled: true }
            //];
        }

        $('#workItemUnitList').multiselect('dataprovider', options);

        $('#workItemUnitList').multiselect('refresh');

    } else if (stringIsNullOrEmpty(unitTypeGuid)) {
        $('#workItemUnitDiv').addClass('hidden');
    }
}





showWorkItemSubHeadsListOptionsToAddWorkItems = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    //option_str.options[0] = new Option('--Select Sub-Head--', '');
    //option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].WorkItemSubHeadName, arr[i].WorkItemSubHeadGuid);
    }
}

showUnitTypesListOptionsToAddWorkItem = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    //option_str.options[0] = new Option('--Select UnitType--', '');
    //option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].UnitTypeName, arr[i].UnitTypeGuid);
    }
}

showUnitsListOptionsBasedOnUnitTypeToAddWorkItems = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Unit--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].UnitSymbol, arr[i].UnitGuid);
    }
}







checkWorkItemExistenceToAddWorkItem = function () {
    var workItemName = $.trim($('#addWorkItemPopupModal input#workItemName').val());

    var retValue = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckWorkItemExistence",
        data: '{"workItemName":"' + workItemName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveWorkItem = function (arrWorkItemSubHeadsSelectedForWorkItem) {

    workItemSubHeadManager = arrWorkItemSubHeadsSelectedForWorkItem;

    var unitTypeManager = {
        unitTypeGuid: $.trim($('#addWorkItemPopupModal select#workItemUnitTypeList').val()),
    };

    var unitManager = {
        unitGuid: $.trim($('#addWorkItemPopupModal select#workItemUnitList').val()),
        //unitSymbol: $.trim($('#addWorkItemPopupModal select#workItemUnitList option:selected').text()),
        unitTypeManager: unitTypeManager
    };

    var workItemManager = {
        workItemName: $.trim($('#addWorkItemPopupModal input#workItemName').val()),
        unitManager: unitManager,
        workItemSubHeadManager: workItemSubHeadManager,
    }

    var workItemManagerJsonString = JSON.stringify(workItemManager);

    var retValue = jQuery.Guid.Empty();

    //arrWorkItemSubHeadsJSONString = JSON.stringify(getSimpleArray(arrWorkItemSubHeadsSelectedForWorkItem))

    //var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveWorkItem",
        data: '{"workItemManager":' + workItemManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}