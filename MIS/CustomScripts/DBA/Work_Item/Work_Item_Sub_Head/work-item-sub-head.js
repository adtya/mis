﻿$(document).ready(function () {

    $('#workItemSubHeadListMenuBtn').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetWorkItemSubHeads",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createWorkItemSubHeadPopupForm(response.d);
            },
            failure: function (result) {
                bootbox.alert("Error");
            }
        });
        e.stopPropagation();
    });

    $('#workItemSubHeadPopupModal').on('click', '.modal-footer button#workItemSubHeadPopupModalCloseBtn', function (e) {
        e.preventDefault();

        location.reload(true);

        e.stopPropagation();
    });

});

createWorkItemSubHeadPopupForm = function (workItemSubHeadData) {
    $("#workItemSubHeadPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">WorkItem SubHeads</h2>';

    html += '<div class="panel-body">';
    html += '<form id="workItemSubHeadPopupForm" name="workItemSubHeadPopupForm" role="form" class="form form-horizontal" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    //if (workItemSubHeadData.length > 0) {
    html += '<div class="table-responsive">';
    html += '<table id="workItemSubHeadsList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'WorkItem SubHead Guid' + '</th>';
    html += '<th>' + 'SubHead Name' + '</th>';
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    for (var i = 0; i < workItemSubHeadData.length; i++) {
        html += '<tr>';

        html += '<td>' + '' + '</td>';
        html += '<td>' + workItemSubHeadData[i].WorkItemSubHeadGuid + '</td>';
        html += '<td>' + workItemSubHeadData[i].WorkItemSubHeadName + '</td>';
        html += '<td>' + '<a href="#" class="viewWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';

        html += '</tr>';
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';
    //} else {
    //    html += '<p>There is no WorkItem SubHead available to be displayed.</p>';
    //}

    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#workItemSubHeadPopupModal .modal-body").append(html);

    //$('#workItemSubHeadPopupModal').modal('show');

    $('#workItemSubHeadPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    workItemSubHeadTableRelatedFunctions();

    $('#workItemSubHeadPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            workItemSubHeadName: {
                required: true,
                noSpace: true
            }
        },
        messages: {
            workItemSubHeadName: {
                required: "Please Enter the SubHead Name.",
                noSpace: "SubHead Name cannot be empty."
            }
        },
        //errorPlacement: function (error, element) {

        //    switch (element.attr("name")) {
        //        case "dpProgressMonth":
        //            error.insertAfter($("#dpProgressMonth"));
        //            break;

        //        default:
        //            //nothing
        //    }
        //}
    });

}

workItemSubHeadTableRelatedFunctions = function () {
    var workItemSubHeadEditing = null;

    var workItemSubHeadTable = $('#workItemSubHeadPopupModal table#workItemSubHeadsList').DataTable({
    //var workItemSubHeadTable = $('#workItemSubHeadsList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        //destroy: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false,
                searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>'
            }
        ],
        order: [[2, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Create New WorkItem SubHead',
                className: 'btn-success addNewWorkItemSubHeadBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewWorkItemSubHead();

                    e.stopPropagation();
                }
            }
        ]
    });

    workItemSubHeadTable.on('order.dt search.dt', function () {
        workItemSubHeadTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addWorkItemSubHeadBtn = workItemSubHeadTable.button(['.addNewWorkItemSubHeadBtn']);

    addNewWorkItemSubHead = function () {

        if (workItemSubHeadEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreWorkItemSubHeadRowData(workItemSubHeadTable, workItemSubHeadEditing);
            workItemSubHeadEditing = null;
            addWorkItemSubHeadBtn.enable();
        }

        var newWorkItemSubHeadRow = workItemSubHeadTable.row.add([
            '',
            '',
            '',
            ''
        ]).draw(false);

        var newWorkItemSubHeadRowNode = newWorkItemSubHeadRow.node();

        addNewWorkItemSubHeadRow(workItemSubHeadTable, newWorkItemSubHeadRowNode);

        workItemSubHeadEditing = newWorkItemSubHeadRowNode;

        addWorkItemSubHeadBtn.disable();

        $('#workItemSubHeadPopupModal input#workItemSubHeadName').focus();

        $(newWorkItemSubHeadRowNode).css('color', 'red').animate({ color: 'black' });
    };

    $('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.saveNewWorkItemSubHeadBtn', function (e) {
        e.preventDefault();

        if ($("#workItemSubHeadPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var checkExistence = checkWorkItemSubHeadExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a WorkItem SubHead with this name.");
            } else if (checkExistence == false) {
                var workItemSubHeadSavedGuid = saveWorkItemSubHead();

                if (jQuery.Guid.IsValid(workItemSubHeadSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(workItemSubHeadSavedGuid.toUpperCase())) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    
                    bootbox.alert("Your WorkItem SubHead is created successfully.", function () {
                        if (workItemSubHeadEditing !== null && workItemSubHeadEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            saveNewWorkItemSubHeadRow(workItemSubHeadTable, workItemSubHeadEditing, workItemSubHeadSavedGuid);
                            workItemSubHeadEditing = null;
                            addWorkItemSubHeadBtn.enable();
                        }
                    });
                } else {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }
            }

        }

    });

    $('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.cancelNewWorkItemSubHeadEditBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (workItemSubHeadEditing !== null && workItemSubHeadEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            removeNewWorkItemSubHeadRowCreatedOnCancel(workItemSubHeadTable, selectedRow);
            workItemSubHeadEditing = null;
            addWorkItemSubHeadBtn.enable();
        }

    });

    $('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.viewWorkItemSubHeadBtn', function (e) {
        e.preventDefault();

        ///* Get the row as a parent of the link that was clicked on */
        //var selectedRow = $(this).parents('tr')[0];

        ////var selectedRowIndex = unitTypeTable.row($(this).parents('tr')).index();

        //if (workItemEditing !== null && workItemEditing != selectedRow) {
        //    /* A different row is being edited - the edit should be cancelled and this row edited */
        //    restoreWorkItemRowData(unitTypeTable, workItemEditing);
        //    editWorkItemRow(unitTypeTable, selectedRow);
        //    workItemEditing = selectedRow;
        //    addWorkItemBtn.enable();
        //}
        //    //else if (nEditing == selectedRow && this.innerHTML == "Save") {
        //    //    /* This row is being edited and should be saved */
        //    //    saveRow(oTable, nEditing);
        //    //    nEditing = null;
        //    //    createBtnClicked = 0;
        //    //}
        //else {
        //    /* No row currently being edited */
        //    editWorkItemRow(unitTypeTable, selectedRow);
        //    workItemEditing = selectedRow;
        //    addWorkItemBtn.enable();
        //}
    });

    $('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.editWorkItemSubHeadBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        //var selectedRowIndex = workItemSubHeadTable.row($(this).parents('tr')).index();

        if (workItemSubHeadEditing !== null && workItemSubHeadEditing != selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreWorkItemSubHeadRowData(workItemSubHeadTable, workItemSubHeadEditing);
            editWorkItemSubHeadRow(workItemSubHeadTable, selectedRow);
            workItemSubHeadEditing = selectedRow;
            addWorkItemSubHeadBtn.enable();
            //$('#workItemSubHeadPopupModal input#workItemSubHeadName').focus();

            var el = $('#workItemSubHeadPopupModal input#workItemSubHeadName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);
        } else {
            /* No row currently being edited */
            editWorkItemSubHeadRow(workItemSubHeadTable, selectedRow);
            workItemSubHeadEditing = selectedRow;
            addWorkItemSubHeadBtn.enable();
            //$('#workItemSubHeadPopupModal input#workItemSubHeadName').focus();

            var el = $('#workItemSubHeadPopupModal input#workItemSubHeadName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);
        }
    });

    $('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.updateWorkItemSubHeadBtn', function (e) {
        e.preventDefault();

        if ($("#workItemSubHeadPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var checkExistence = checkWorkItemSubHeadExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a WorkItem SubHead with this name.");
            } else if (checkExistence == false) {
                var workItemSubHeadUpdated = updateWorkItemSubHead(workItemSubHeadTable.row(selectedRow).data()[1]);

                if (workItemSubHeadUpdated == 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    //$('#workItemSubHeadName').val('');
                    bootbox.alert("Your WorkItem SubHead is updated successfully.", function () {
                        if (workItemSubHeadEditing !== null && workItemSubHeadEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            updateWorkItemSubHeadRow(workItemSubHeadTable, workItemSubHeadEditing);
                            workItemSubHeadEditing = null;
                            addWorkItemSubHeadBtn.enable();
                        }
                    });
                } else if (workItemSubHeadUpdated == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
                }
            }

        }

    });

    $('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.cancelWorkItemSubHeadEditBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (workItemSubHeadEditing !== null && workItemSubHeadEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreWorkItemSubHeadRowData(workItemSubHeadTable, workItemSubHeadEditing);
            workItemSubHeadEditing = null;
            addWorkItemSubHeadBtn.enable();
        }

    });

    $('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.deleteWorkItemSubHeadBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        var workItemSubHeadDeleted = deleteWorkItemSubHead(workItemSubHeadTable.row(selectedRow).data()[1]);

        if (workItemSubHeadDeleted == 1) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Your WorkItem SubHead is deleted successfully.", function () {
                workItemSubHeadTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (workItemSubHeadDeleted == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
        }

    });

}

addNewWorkItemSubHeadRow = function (workItemSubHeadTable, selectedRow) {
    var selectedRowData = workItemSubHeadTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //workItemSubHeadTable.order([2, 'asc']).draw();

    workItemSubHeadTable.column('1:visible').order('asc').draw();

    workItemSubHeadTable.on('order.dt search.dt', function () {

        var x = workItemSubHeadTable.order();
        
        if (x[0][1] == "asc") {
            var a = workItemSubHeadTable.column(0, { search: 'applied', order: 'applied' }).nodes();
            for (i = 1; i < a.length; i++) {
                a[i].innerHTML = i;
            }
        } else {
            workItemSubHeadTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }
        availableTds[0].innerHTML = '*';
    }).draw(false);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[0].innerHTML = '*';
    availableTds[1].innerHTML = '<input type="text" id="workItemSubHeadName" name="workItemSubHeadName" placeholder="WorkItem SubHead Name" class="form-control text-capitalize" value="' + selectedRowData[2] + '" onfocus="workItemSubHeadNameFocusFunction()" onblur="workItemSubHeadNameBlurFunction()" style="width:100%;">';//WorkItem SubHead
    availableTds[2].innerHTML = '<a href="#" class="saveNewWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelNewWorkItemSubHeadEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

saveNewWorkItemSubHeadRow = function (workItemSubHeadTable, selectedRow, workItemSubHeadSavedGuid) {

    var selectedRowData = workItemSubHeadTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    selectedRowData[1] = workItemSubHeadSavedGuid;
    selectedRowData[2] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
    selectedRowData[3] = '<a href="#" class="viewWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    workItemSubHeadTable.row(selectedRow).data(selectedRowData);

    workItemSubHeadTable.on('order.dt search.dt', function () {
        workItemSubHeadTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

removeNewWorkItemSubHeadRowCreatedOnCancel = function (workItemSubHeadTable, selectedRow) {

    workItemSubHeadTable
        .row(selectedRow)
        .remove()
        .draw();

    workItemSubHeadTable.on('order.dt search.dt', function () {
        workItemSubHeadTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

editWorkItemSubHeadRow = function (workItemSubHeadTable, selectedRow) {
    var selectedRowData = workItemSubHeadTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[1].innerHTML = '<input type="text" id="workItemSubHeadName" name="workItemSubHeadName" placeholder="WorkItem SubHead Name" class="form-control text-capitalize" value="' + selectedRowData[2] + '" onfocus="workItemSubHeadNameFocusFunction()" onblur="workItemSubHeadNameBlurFunction()" style="width:100%;">';//WorkItem SubHead
    availableTds[2].innerHTML = '<a href="#" class="updateWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelWorkItemSubHeadEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

updateWorkItemSubHeadRow = function (workItemSubHeadTable, selectedRow) {
    var selectedRowData = workItemSubHeadTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[2] = capitalizeFirstAllWords($.trim(availableInputs[0].value));

    workItemSubHeadTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

    //workItemSubHeadTable.draw();
}

restoreWorkItemSubHeadRowData = function (workItemSubHeadTable, previousRow) {
    var previousRowData = workItemSubHeadTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewWorkItemSubHeadRowCreatedOnCancel(workItemSubHeadTable, previousRow);
    } else {
        workItemSubHeadTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;

        //workItemSubHeadTable.cell(previousRowIndex, 0).innerHTML = previousRowSerialNum;

        //workItemSubHeadTable.draw();
    }

}






workItemSubHeadNameFocusFunction = function () {
    // Focus = Changes the background color of input to yellow
    //document.getElementById("workItemSubHeadName").style.background = "yellow";
    $('#workItemSubHeadPopupModal input#workItemSubHeadName').css('background-color', 'yellow');
}

workItemSubHeadNameBlurFunction = function () {
    // No focus = Changes the background color of input to nothing
    //document.getElementById("workItemSubHeadName").style.background = "";
    $('#workItemSubHeadPopupModal input#workItemSubHeadName').css('background-color', '');
}






capitalizeFirstAllWords = function (str) {
    var pieces = str.split(" ");
    for (var i = 0; i < pieces.length; i++) {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
}

checkWorkItemSubHeadExistence = function () {
    var subHeadName = $.trim($('#workItemSubHeadPopupModal input#workItemSubHeadName').val());

    var retValue = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckWorkItemSubHeadExistence",
        data: '{"subHeadName":"' + subHeadName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveWorkItemSubHead = function () {
    var workItemSubHeadName = $.trim($('#workItemSubHeadPopupModal input#workItemSubHeadName').val());

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveWorkItemSubHead",
        data: '{"workItemSubHeadName":"' + workItemSubHeadName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updateWorkItemSubHead = function (workItemSubHeadGuid) {
    var workItemSubHeadName = $.trim($('#workItemSubHeadPopupModal input#workItemSubHeadName').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateWorkItemSubHead",
        data: '{"workItemSubHeadName":"' + workItemSubHeadName + '","workItemSubHeadGuid":"' + workItemSubHeadGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteWorkItemSubHead = function (workItemSubHeadGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/DeleteWorkItemSubHead",
        data: '{"workItemSubHeadGuid":"' + workItemSubHeadGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}