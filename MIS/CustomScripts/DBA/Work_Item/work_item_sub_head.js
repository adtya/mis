﻿$(document).ready(function () {

    $('#workItemSubHeadMenuBtn').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetWorkItemSubHeads",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createWorkItemSubHeadPopupForm(response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });
        
    });

    $('#addWorkItemSubHeadPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            workItemSubHeadName: {
                required: true,
                noSpace: true
            }
        }
    });

    $('#addWorkItemSubHeadBtn').on('click', function (e) {
        e.preventDefault();

        if ($("#addWorkItemSubHeadPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            var checkExistence = checkWorkItemSubHeadExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a WorkItem SubHead with this name.");
            } else if (checkExistence == false) {
                var check = saveWorkItemSubHead();

                if (check == 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    $('#workItemSubHeadName').val('');
                    bootbox.alert("Your Sub-Head is created successfully.");
                } else if (check == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }
            }

        }

    });

});

createWorkItemSubHeadPopupForm = function (workItemSubHeadData) {
    $("#workItemSubHeadPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Work Item Sub-Heads</h2>';

    html += '<div class="panel-body">';
    html += '<form id="workItemSubHeadPopupForm" name="workItemSubHeadPopupForm" role="form" class="form form-horizontal" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    if (workItemSubHeadData.length > 0) {
        html += '<div class="table-responsive">';
        html += '<table id="workItemSubHeadsList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

        html += '<thead>';
        html += '<tr>';
        html += '<th>' + '#' + '</th>';
        html += '<th>' + 'WorkItem SubHead Guid' + '</th>';
        html += '<th>' + 'SubHead Name' + '</th>';
        html += '<th>' + 'Operation' + '</th>';
        html += '</tr>';
        html += '</thead>';

        html += '<tbody>';

        for (var i = 0; i < workItemSubHeadData.length; i++) {
            html += '<tr>';

            html += '<td>' + '' + '</td>';
            html += '<td>' + workItemSubHeadData[i].WorkItemSubHeadGuid + '</td>';
            html += '<td>' + workItemSubHeadData[i].WorkItemSubHeadName + '</td>';
            html += '<td>' + '<a href="#" class="viewWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="editWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';

            html += '</tr>';
        }

        html += '</tbody>';

        html += '</table>';
        html += '</div>';
    } else {
        html += '<p>There is no WorkItem SubHead available to be displayed.</p>';
    }

    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#workItemSubHeadPopupModal .modal-body").append(html);

    $('#workItemSubHeadPopupModal').modal('show');

    tableRelatedFunctions();

    $('#workItemSubHeadPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            workItemSubHeadName: {
                required: true,
                noSpace: true
            }
        },
        messages: {
            progressMonth: {
                required: "Please Enter the Month For Progress.",
                dateITA: "Please Enter the Month For Progress in the specified format."
            }
        },
        errorPlacement: function (error, element) {

            switch (element.attr("name")) {
                case "dpProgressMonth":
                    error.insertAfter($("#dpProgressMonth"));
                    break;

                default:
                    //nothing
            }
        }
    });

}

tableRelatedFunctions = function () {
    var createBtnClicked = 0;

    var s = $.extend(true, $.fn.dataTable.defaults, {
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        //data: dataSet,
        stateSave: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: true,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false,
                searchable: false,
                //defaultContent: '<a class="viewWorkItemSubHeadBtn" onclick="editBtnClicked(this);"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="editWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-pencil" style="font-size: 22px;" data-original-title="Edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="deleteWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete"></i></a>'
            }
        ],
        order: [[2, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Create New SubHead',
                className: 'btn-success addNewWorkItemSubHeadBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();
                    
                    if (createBtnClicked == 0) {
                        createBtnClicked = createBtnClicked + 1;
                        addNewWorkItemSubHead();
                        
                    }
                    
                }
            }
        ]
    });

    var table = $('#workItemSubHeadsList').DataTable();

    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    oTable = $('#workItemSubHeadsList').dataTable();
    var nEditing = null;
    
    addNewWorkItemSubHead = function () {

        if (nEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreRowData(table, nEditing);
            nEditing = null;
        }

        var newRow = table.row.add([
            '',
            '',
            '',
            ''
        ]).draw(false);

        var newRowNode = newRow.node();

        addNewWorkItemSubHeadRow(table, newRowNode);

        nEditing = newRowNode;

        $(newRowNode).css('color', 'red').animate({ color: 'black' });
    };

    $('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.saveNewWorkItemSubHeadBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            saveNewWorkItemSubHeadRow(table, nEditing);
            nEditing = null;
            createBtnClicked = 0;
        }

    });

    $('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.cancelNewWorkItemSubHeadEditBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            removeNewRowCreatedOnCancel(table, selectedRow);
            nEditing = null;
            createBtnClicked = 0;
        }

    });

    $('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.editWorkItemSubHeadBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        var selectedRowIndex = table.row($(this).parents('tr')).index();

        if (nEditing !== null && nEditing != selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreRowData(table, nEditing);
            editWorkItemSubHeadRow(table, selectedRow);
            nEditing = selectedRow;
            createBtnClicked = 0;
        }
        else if (nEditing == selectedRow && this.innerHTML == "Save") {
            /* This row is being edited and should be saved */
            saveRow(oTable, nEditing);
            nEditing = null;
            createBtnClicked = 0;
        }
        else {
            /* No row currently being edited */
            editWorkItemSubHeadRow(table, selectedRow);
            nEditing = selectedRow;
            createBtnClicked = 0;
        }
    });




    $('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.updateWorkItemSubHeadBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            saveWorkItemSubHeadRow(table, nEditing);
            nEditing = null;
        }

    });

    $('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.cancelWorkItemSubHeadEditBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreRowData(table, nEditing);
            nEditing = null;
        }

    });

    

    //$('#workItemSubHeadPopupModal').on('click', '#workItemSubHeadsList a.addNewWorkItemSubHeadBtn', function (e) {
    //    e.preventDefault();

    //    var aiNew = oTable.fnAddData(['', '', '', '', '',
	//		'<a class="edit" href="">Edit</a>', '<a class="delete" href="">Delete</a>']);
    //    var nRow = oTable.fnGetNodes(aiNew[0]);
    //    editRow(oTable, nRow);
    //    nEditing = nRow;

    //});

    //$('#example a.delete').live('click', function (e) {
    //    e.preventDefault();

    //    var nRow = $(this).parents('tr')[0];
    //    oTable.fnDeleteRow(nRow);
    //});

    //var selectedRowData = table.row(selectedRow).data();

    //var availableInputs = $('input', selectedRow);

    //var availableTds = $('>td', selectedRow);

    //var selectedRowSerialNum = availableTds[0].innerHTML;

    //selectedRowData[2] = availableInputs[0].value;

    //table.row(selectedRow).data(selectedRowData);

    //availableTds[0].innerHTML = selectedRowSerialNum;








    //new $.fn.dataTable.Buttons(table, {
    //    buttons: [
    //        {
    //            text: 'Button 2',
    //            action: function (e, dt, node, conf) {
    //                alert('Button 2 clicked on');
    //            }
    //        },
    //        {
    //            text: 'Button 3',
    //            action: function (e, dt, node, conf) {
    //                alert('Button 3 clicked on');
    //            }
    //        }
    //    ]
    //});

    //table.buttons(1, null).container().appendTo(
    //    table.table().container()
    //);

    //$('#workItemSubHeadsList tbody td a').on('click', function () {
    //    var data = table.row($(this).parents('tr')).data();
    //    //alert(data[0] + "'s salary is: " + data[5]);

    //    alert($(this).hasClass('viewWorkItemSubHeadBtn'));

    //    alert($(this).parents('tr').index());

    //    alert(data[2]);
    //    //alert("aditya");
    //});

    //resetTable = function (value) {
    //    var myRowIndex = table.row($(value).parents('tr')).index();

    //    //table.clear().draw();
    //    //table.rows.add(dataSet);
    //    //table.draw();
    //    //table.columns.adjust().draw();
    //};

    //editBtnClicked = function (value) {
    //    var myRowIndex = table.row($(value).parents('tr')).index();

    //    var data = table.row($(value).parents('tr')).data();
    //    var workItemSubHeadGuid = data[1];
    //    var workItemSubHeadName = data[2];
    //    var workItemSubHeadBtns = data[3];

    //    //var a = createWorkItemSubHeadEditableString(workItemSubHeadGuid, workItemSubHeadName, myRowIndex);
    //    //data[2] = a;
        
    //    //table.row($(value).parents('tr')).data(data);

    //    //var requiredDataCell = table.cell(myRowIndex, 2);
    //    //requiredDataCell.data(a);//.draw();

    //    //var b = createWorkItemSubHeadEditableButtonsString();
    //    //data[3] = b;
    //    //var requiredBtnCell = table.cell(myRowIndex, 3);
    //    //requiredBtnCell.data(b);//.draw();
    //    //alert();

    //    //table.row($(value).parents('tr')).data(data).draw();
    //};

    //cancelBtnClicked = function (value) {
    //    //table.clear().draw(s);
    //};

    //$('#workItemSubHeadsList tbody').on('click', 'button', function () {
    //    var data = table.row($(this).parents('tr')).index();
    //    //alert(data[0] + "'s salary is: " + data[5]);
    //    alert("aditya");
    //});


    //$('#workItemSubHeadsList tbody').on('click', 'td', function () {
    //    //console.log(table.row(this).data());
    //    //var columnData = table.cell(this).data();
    //    var columnNode = table.cell(this).node();
    //    var cellBtns = columnNode.getElementsByTagName('button');
    //    var editBtn = columnNode.getElementsByClassName('editWorkItemSubHeadBtn');
    //    var editBtnId = editBtn[0].id;
    //    $('#' + editBtnId).on('click', function (e) {
    //        e.preventDefault();

    //        var myRowIndex = $(this).parent().parent().index();
    //        var myColIndex = table.column(':contains("Operation")').index();//$(this).parent().index();
    //        var dataColIndex = table.column(':contains("Operation")').index() - 1; //$(this).parent(-2).index();

    //        var fid1 = table.row(myRowIndex).data()[1];//WorkItem SubHead Guid
    //        var fid2 = table.row(myRowIndex).data()[2];//WorkItem SubHead Name
    //        var fid3 = table.row(myRowIndex).data()[3];//WorkItem SubHead Buttons

    //        var a = createWorkItemSubHeadEditableString(fid1, fid2, myRowIndex);

    //        //table.column(requiredColIndex).cell().data(a).draw();
    //        //cell.data(cell.data() + 1).draw();

    //        var requiredDataCell = table.cell(myRowIndex, dataColIndex);

    //        requiredDataCell.data(a);//.draw();

    //        var b = createWorkItemSubHeadEditableButtonsString();

    //        var requiredBtnCell = table.cell(myRowIndex, myColIndex);

    //        requiredBtnCell.data(b);//.draw();

    //        //$('.cancelWorkItemSubHeadBtn').on('click', function (e) {
    //        //    e.preventDefault();

    //        //    //var myRowIndex = $(this).parent().parent().index();
    //        //    //var myColIndex = $(this).parent().index();
    //        //    //var requiredColIndex = $(this).parent(-1).index();

    //        //    //var fid1 = table.row(myRowIndex).data()[1];//WorkItem SubHead Guid
    //        //    //var fid2 = table.row(myRowIndex).data()[2];//WorkItem SubHead Name
    //        //    //var fid3 = table.row(myRowIndex).data()[3];//WorkItem SubHead Buttons

    //        //    //var a = createWorkItemSubHeadEditableString(fid1, fid2, myRowIndex);

    //        //    //table.column(requiredColIndex).cell().data(a).draw();
    //        //    //cell.data(cell.data() + 1).draw();

    //        //    var requiredDataCell = table.cell(myRowIndex, dataColIndex);

    //        //    requiredDataCell.data(fid2);

    //        //    //var b = createWorkItemSubHeadEditableButtonsString();

    //        //    var requiredBtnCell = table.cell(myRowIndex, myColIndex);

    //        //    requiredBtnCell.data(fid3);

    //        //    //table.draw();

    //        //    //e.stopPropagation();
    //        //});

    //        e.stopPropagation();
    //    });
    //});



    //$('.editWorkItemSubHeadBtn').on('click', function (e) {
    //    e.preventDefault();

    //    var myRowIndex = $(this).parent().parent().index();
    //    var myColIndex = table.column(':contains("Operation")').index();//$(this).parent().index();
    //    var dataColIndex = table.column(':contains("Operation")').index() - 1; //$(this).parent(-2).index();

    //    var fid1 = table.row(myRowIndex).data()[1];//WorkItem SubHead Guid
    //    var fid2 = table.row(myRowIndex).data()[2];//WorkItem SubHead Name
    //    var fid3 = table.row(myRowIndex).data()[3];//WorkItem SubHead Buttons

    //    var a = createWorkItemSubHeadEditableString(fid1, fid2, myRowIndex);

    //    //table.column(requiredColIndex).cell().data(a).draw();
    //    //cell.data(cell.data() + 1).draw();

    //    var requiredDataCell = table.cell(myRowIndex, dataColIndex);

    //    requiredDataCell.data(a);//.draw();

    //    var b = createWorkItemSubHeadEditableButtonsString();

    //    var requiredBtnCell = table.cell(myRowIndex, myColIndex);

    //    requiredBtnCell.data(b);//.draw();

    //    $('.cancelWorkItemSubHeadBtn').on('click', function (e) {
    //        e.preventDefault();

    //        //var myRowIndex = $(this).parent().parent().index();
    //        //var myColIndex = $(this).parent().index();
    //        //var requiredColIndex = $(this).parent(-1).index();

    //        //var fid1 = table.row(myRowIndex).data()[1];//WorkItem SubHead Guid
    //        //var fid2 = table.row(myRowIndex).data()[2];//WorkItem SubHead Name
    //        //var fid3 = table.row(myRowIndex).data()[3];//WorkItem SubHead Buttons

    //        //var a = createWorkItemSubHeadEditableString(fid1, fid2, myRowIndex);

    //        //table.column(requiredColIndex).cell().data(a).draw();
    //        //cell.data(cell.data() + 1).draw();

    //        var requiredDataCell = table.cell(myRowIndex, dataColIndex);

    //        requiredDataCell.data(fid2);

    //        //var b = createWorkItemSubHeadEditableButtonsString();

    //        var requiredBtnCell = table.cell(myRowIndex, myColIndex);

    //        requiredBtnCell.data(fid3);

    //        //table.draw();

    //        //e.stopPropagation();
    //    });

    //    //e.stopPropagation();
    //});


    //$('.cancelWorkItemSubHeadBtn').on('click', function (e) {
    //    e.preventDefault();

    //    let myRowIndex = $(this).parent().parent().index();
    //    myColIndex = $(this).parent().index();
    //    let requiredColIndex = $(this).parent(-1).index();

    //    //var fid1 = table.row(myRowIndex).data()[1];//WorkItem SubHead Guid
    //    //var fid2 = table.row(myRowIndex).data()[2];//WorkItem SubHead Name
    //    //var fid3 = table.row(myRowIndex).data()[3];//WorkItem SubHead Buttons

    //    //var a = createWorkItemSubHeadEditableString(fid1, fid2, myRowIndex);

    //    //table.column(requiredColIndex).cell().data(a).draw();
    //    //cell.data(cell.data() + 1).draw();

    //    var requiredDataCell = table.cell(myRowIndex, requiredColIndex);

    //    requiredDataCell.data(fid2).draw();

    //    //var b = createWorkItemSubHeadEditableButtonsString();

    //    var requiredBtnCell = table.cell(myRowIndex, myColIndex);

    //    requiredBtnCell.data(fid3).draw();

    //    e.stopPropagation();
    //});










    //$('#deleteUserBtn').on('click', function (e) {
    //    e.preventDefault();

    //    var rowsData = table.rows({ selected: true }).data();
    //    var userGuidToBeDeleted = [];

    //    for (var i = 0; i < rowsData.length; i++) {
    //        userGuidToBeDeleted.push(rowsData[i][2]);
    //    }

    //    if ($(this).hasClass('disabled')) {

    //    } else {
    //        deleteUserConfirmation(userGuidToBeDeleted);
    //    }
    //});

    
}

addNewWorkItemSubHeadRow = function (table, selectedRow) {
    var selectedRowData = table.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    table.on('order.dt search.dt', function () {

        var x = table.order();
        
        if (x[0][1] == "asc") {
            var a = table.column(0, { search: 'applied', order: 'applied' }).nodes();
            for (i = 1; i < a.length; i++) {
                a[i].innerHTML = i;
            }
        } else {
            table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }
        availableTds[0].innerHTML = '*';
    }).draw(false);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[0].innerHTML = '*';
    availableTds[1].innerHTML = '<input type="text" id="workItemSubHeadName" name="workItemSubHeadName" placeholder="WorkItem SubHead Name" class="form-control text-capitalize" value="' + selectedRowData[2] + '">';//WorkItem SubHead
    availableTds[2].innerHTML = '<a href="#" class="saveNewWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="cancelNewWorkItemSubHeadEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>'
}

saveNewWorkItemSubHeadRow = function (table, selectedRow) {
    var selectedRowData = table.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    selectedRowData[2] = availableInputs[0].value;

    selectedRowData[3] = '<a href="#" class="viewWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="editWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    table.row(selectedRow).data(selectedRowData);

    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

removeNewRowCreatedOnCancel = function (table, selectedRow) {

    table
        .row(selectedRow)
        .remove()
        .draw();

    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

editWorkItemSubHeadRow = function (table, selectedRow) {
    var selectedRowData = table.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[1].innerHTML = '<input type="text" value="' + selectedRowData[2] + '">';//WorkItem SubHead
    availableTds[2].innerHTML = '<a href="#" class="updateWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="cancelWorkItemSubHeadEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>'
}

saveWorkItemSubHeadRow = function (table, selectedRow) {
    var selectedRowData = table.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[2] = availableInputs[0].value;

    table.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

    //table.draw();
}

restoreRowData = function (table, previousRow) {
    var previousRowData = table.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewRowCreatedOnCancel(table, previousRow);
    } else {
        table.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;

        //table.cell(previousRowIndex, 0).innerHTML = previousRowSerialNum;

        //table.draw();
    }

}













checkWorkItemSubHeadExistence = function () {
    var subHeadName = $.trim($('#workItemSubHeadName').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckWorkItemSubHeadExistence",
        data: '{"subHeadName":"' + subHeadName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveWorkItemSubHead = function () {
    var subHeadName = $.trim($('#workItemSubHeadName').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveWorkItemSubHead",
        data: '{"subHeadName":"' + subHeadName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}