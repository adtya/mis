﻿$(document).ready(function () {

    $('#workItemListMenuBtn').on('click', function (e) {
        e.preventDefault();
        //$.ajax({
        //    type: "Post",
        //    async: false,
        //    url: "DBAMethods.asmx/GetWorkItems",
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    success: function (response) {
        //        createWorkItemPopupForm(response.d);
        //    },
        //    failure: function (result) {
        //        bootbox.alert("Error");
        //    }
        //});
        //workItemData = getWorkItems();
        createWorkItemPopupForm(getWorkItems());
        e.stopPropagation();
    });

    $('#workItemPopupModal').on('click', '.modal-footer button#workItemPopupModalCloseBtn', function (e) {
        e.preventDefault();

        location.reload(true);

        e.stopPropagation();
    });

    $('#addWorkItemPopupModal').on('click', '.modal-footer button#backFromAddWorkItemPopupModalBtn', function (e) {
        e.preventDefault();

        $('#addWorkItemPopupModal').modal('hide');

        $('#workItemPopupModal').modal('show');

        e.stopPropagation();
    });

    $('#editWorkItemPopupModal').on('click', '.modal-footer button#backFromEditWorkItemPopupModalBtn', function (e) {
        e.preventDefault();

        $('#editWorkItemPopupModal').modal('hide');

        $('#workItemPopupModal').modal('show');

        e.stopPropagation();
    });

});

createWorkItemPopupForm = function (workItemData) {
    $("#workItemPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">WorkItems</h2>';

    html += '<div class="panel-body">';
    html += '<form id="workItemPopupForm" name="workItemPopupForm" role="form" class="form form-horizontal" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    //if (workItemData.length > 0) {
    html += '<div class="table-responsive">';
    html += '<table id="workItemsList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'WorkItem Guid' + '</th>';
    html += '<th>' + 'WorkItem Name' + '</th>';
    html += '<th>' + 'WorkItem SubHeads' + '</th>';
    html += '<th>' + 'Unit' + '</th>';
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    for (var i = 0; i < workItemData.length; i++) {
        html += '<tr>';

        html += '<td>' + '' + '</td>';
        html += '<td>' + workItemData[i].WorkItemGuid + '</td>';
        html += '<td>' + workItemData[i].WorkItemName + '</td>';

        html += '<td>';
        html += '<ul class="text-left">';
        for (var i1 = 0; i1 < workItemData[i].WorkItemSubHeadManager.length; i1++) {
            html += '<li>';
            html += workItemData[i].WorkItemSubHeadManager[i1].WorkItemSubHeadName;
            html += '</li>';
        }
        html += '</ul>';
        html += '</td>';

        html += '<td>' + workItemData[i].UnitManager.UnitSymbol + '</td>';
        html += '<td>' + '<a href="#" class="viewWorkItemBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';

        html += '</tr>';
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';
    //} else {
    //    html += '<p>There is no WorkItem available to be displayed.</p>';
    //}

    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#workItemPopupModal .modal-body").append(html);

    //$('#workItemPopupModal').modal('show');

    $('#workItemPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    workItemTableRelatedFunctions();

    $('#workItemPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            workItemName: {
                required: true,
                noSpace: true
            }
        },
        messages: {
            workItemName: {
                required: "Please Enter the WorkItem Name.",
                noSpace: "WorkItem Name cannot be empty."
            }
        },
        //errorPlacement: function (error, element) {

        //    switch (element.attr("name")) {
        //        case "dpProgressMonth":
        //            error.insertAfter($("#dpProgressMonth"));
        //            break;

        //        default:
        //            //nothing
        //    }
        //}
    });

}

workItemTableRelatedFunctions = function () {
    var workItemTable = $('#workItemPopupModal table#workItemsList').DataTable({
    //var workItemTable = $('#workItemsList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        //destroy: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false
            },
            {
                targets: [4],
                orderable: false
            },
            {
                targets: [5],
                orderable: false,
                searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewWorkItemBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>'
            }
        ],
        order: [[2, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Create New WorkItem',
                className: 'btn-success addNewWorkItemBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewWorkItem();

                    e.stopPropagation();
                }
            }
        ]
         
    });

    workItemTable.on('order.dt search.dt', function () {
        workItemTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    addNewWorkItem = function () {
        createAddWorkItemPopupForm(workItemTable);
    };

    $('#workItemPopupModal').on('click', '#workItemsList a.viewWorkItemBtn', function (e) {
        e.preventDefault();

        ///* Get the row as a parent of the link that was clicked on */
        //var selectedRow = $(this).parents('tr')[0];

        ////var selectedRowIndex = workItemTable.row($(this).parents('tr')).index();

        //if (workItemEditing !== null && workItemEditing != selectedRow) {
        //    /* A different row is being edited - the edit should be cancelled and this row edited */
        //    restoreWorkItemRowData(workItemTable, workItemEditing);
        //    editWorkItemRow(workItemTable, selectedRow);
        //    workItemEditing = selectedRow;
        //    addWorkItemBtn.enable();
        //}
        //    //else if (nEditing == selectedRow && this.innerHTML == "Save") {
        //    //    /* This row is being edited and should be saved */
        //    //    saveRow(oTable, nEditing);
        //    //    nEditing = null;
        //    //    createBtnClicked = 0;
        //    //}
        //else {
        //    /* No row currently being edited */
        //    editWorkItemRow(workItemTable, selectedRow);
        //    workItemEditing = selectedRow;
        //    addWorkItemBtn.enable();
        //}
    });

    $('#workItemPopupModal').on('click', '#workItemsList a.editWorkItemBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        createEditWorkItemPopupForm(workItemTable, selectedRow);

        //var selectedRowIndex = workItemTable.row($(this).parents('tr')).index();

        //if (workItemEditing !== null && workItemEditing != selectedRow) {
        //    /* A different row is being edited - the edit should be cancelled and this row edited */
        //    restoreWorkItemRowData(workItemTable, workItemEditing);
        //    editWorkItemRow(workItemTable, selectedRow);
        //    workItemEditing = selectedRow;
        //    addWorkItemBtn.enable();
        //}
        //    //else if (nEditing == selectedRow && this.innerHTML == "Save") {
        //    //    /* This row is being edited and should be saved */
        //    //    saveRow(oTable, nEditing);
        //    //    nEditing = null;
        //    //    createBtnClicked = 0;
        //    //}
        //else {
        //    /* No row currently being edited */
        //    editWorkItemRow(workItemTable, selectedRow);
        //    workItemEditing = selectedRow;
        //    addWorkItemBtn.enable();
        //}
    });

    $('#workItemPopupModal').on('click', '#workItemsList a.deleteWorkItemBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        //var selectedRow1 = $(this).parents('tr');

        var workItemDeleted = deleteWorkItem(workItemTable.row(selectedRow).data()[1]);

        if (workItemDeleted == 1) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Your WorkItem is deleted successfully.", function () {
                workItemTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (workItemDeleted == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
        }

    });

}

createAddWorkItemPopupForm = function (workItemTable) {
    var arrWorkItemSubHeadsSelected = [];

    var workItemSubHeadData = getWorkItemSubHeads();

    var unitTypeData = getUnitTypes();

    $("#addWorkItemPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Add WorkItem</h2>';

    html += '<div class="panel-body">';
    html += '<form id="addWorkItemPopupForm" name="addWorkItemPopupForm" class="form form-horizontal" role="form" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemName" id="workItemNameLabel" class="control-label pull-left">WorkItem Name</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="workItemName" name="workItemName" placeholder="WorkItem Name" class="form-control text-capitalize" value="" onfocus="workItemNameFocusFunction()" onblur="workItemNameBlurFunction()" style="width:100%;" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemSubHeadList" id="workItemSubHeadListLabel" class="control-label pull-left">WorkItem SubHead</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    //html += '<div class="btn-group-justified">';
    html += '<select class="form-control multiselect" name="workItemSubHeadList" id="workItemSubHeadList" multiple="multiple" size="5">';

    for (var i = 0; i < workItemSubHeadData.length; i++) {
        html += '<option value="' + workItemSubHeadData[i].WorkItemSubHeadGuid + '">';
        html += workItemSubHeadData[i].WorkItemSubHeadName;
        html += '</option>';
    }

    html += '</select>';
    //html += '</div>';
    html += '</div>';
    html += '</div>';

    //html += '<fieldset class="scheduler-border">';
    //html += '<legend class="scheduler-border">WorkItem Unit</legend>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemUnitTypeList" id="workItemUnitTypeListLabel" class="control-label pull-left">Unit Type</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control multiselect" name="workItemUnitTypeList" id="workItemUnitTypeList">';// onchange="getUnitsToAddWorkItem(this.value);">';

    html += '<option value="" selected="selected">' + '--Select UnitType--' + '</option>';
    for (var i = 0; i < unitTypeData.length; i++) {
        html += '<option value="' + unitTypeData[i].UnitTypeGuid + '">';
        html += unitTypeData[i].UnitTypeName;
        html += '</option>';
    }

    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div id="workItemUnitDiv" class="hidden">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemUnitList" id="workItemUnitListLabel" class="control-label pull-left">Unit</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control multiselect" name="workItemUnitList" id="workItemUnitList" size="5">';
    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';

    html += '<div id="workItemSubHeadListDisplay" class="panel panel-default panel-body col-xs-12 col-sm-12 col-md-12 col-lg-12 well well-sm hidden"></div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    //html += '<button class="btn icon-btn-save btn-success submit" type="button">';
    //html += '<span class="btn-save-label">';
    //html += '<i class="glyphicon glyphicon-floppy-disk"></i>';
    //html += '</span>';
    //html += ' Save';
    //html += '</button>';

    html += '<a href="#" id="saveNewWorkItemBtn" name="saveNewWorkItemBtn" type="button" class="btn btn-lg btn-success btn-block">';
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i>';
    html += '</span>';
    html += '  Save';
    html += '</a>';

    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    html += '<a href="#" id="cancelNewWorkItemBtn" name="cancelNewWorkItemBtn" type="button" class="btn btn-lg btn-danger btn-block">';//btn-primary
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i>';
    html += '</span>';
    html += ' Cancel';
    html += '</a>';

    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#addWorkItemPopupModal .modal-body").append(html);

    $('#workItemPopupModal').modal('hide');

    //$('#addWorkItemPopupModal').modal('show');

    $('#addWorkItemPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    $('#addWorkItemPopupModal input#workItemName').focus();

    $('#workItemSubHeadList').multiselect({
        maxHeight: 150,
        enableCaseInsensitiveFiltering: true,
        numberDisplayed: 0,
        //includeSelectAllOption: true,
        nonSelectedText: '--Select WorkItem SubHead--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$('.multiselect').valid();
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                $('#workItemSubHeadListDisplay').removeClass('hidden');

                arrWorkItemSubHeadsSelected.push({
                    workItemSubHeadGuid: element.val(),
                    workItemSubHeadName: element.text()
                });

                displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelected);
            }
            else if (checked === false) {
                if (confirm('Do you wish to deselect the element?')) {
                    for (var i = arrWorkItemSubHeadsSelected.length - 1; i >= 0; i--) {
                        if (arrWorkItemSubHeadsSelected[i].workItemSubHeadGuid === element.val()) {
                            arrWorkItemSubHeadsSelected.splice(i, 1);
                        }
                    }

                    displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelected);
                }
                else {
                    $("#workItemSubHeadList").multiselect('select', element.val());

                    displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelected);
                }
            }
        },
        //onSelectAll: function (checked) {
        //    while (arrWorkItemSubHeadsSelected.length > 0) {
        //        arrWorkItemSubHeadsSelected.pop();
        //    }
        //    alert(checked);
        //}
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#workItemUnitTypeList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select UnitType--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$('.multiselect').valid();
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                displayWorkItemUnitsList(element.val());

                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#workItemUnitList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select Unit--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$('.multiselect').valid();
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#addWorkItemPopupForm').validate({ // initialize plugin
        ignore: ':hidden:not(".multiselect")',
        //ignore: ':hidden:not("#select")',
        //ignore: ":not(:visible)",
        rules: {
            workItemName: {
                required: true,
                noSpace: true
            },
            workItemSubHeadList: "required",
            workItemUnitTypeList: "required",
            workItemUnitList: "required"
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block small',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                element.closest('.form-group').append(error);
            }
        },
        submitHandler: function () {
            alert('valid form');
            return false;
        }
    });

    $('#addWorkItemPopupModal').on('click', 'a#saveNewWorkItemBtn', function (e) {
        e.preventDefault();

        if ($("#addWorkItemPopupForm").valid()) {

            if (arrWorkItemSubHeadsSelected.length > 0) {
                $("#preloader").show();
                $("#status").show();

                var workItemName = $.trim($('#addWorkItemPopupModal input#workItemName').val());

                var checkExistence = checkWorkItemExistence(workItemName);

                if (checkExistence == true) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("There is already a WorkItem with this name.");
                } else if (checkExistence == false) {

                    for (var index = 0; index < arrWorkItemSubHeadsSelected.length; index++) {
                        arrWorkItemSubHeadsSelected[index].workItemSubHeadSequence = (index + 1);
                    }

                    var workItemSavedGuid = saveWorkItem(workItemName, arrWorkItemSubHeadsSelected);

                    if (jQuery.Guid.IsValid(workItemSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(workItemSavedGuid.toUpperCase())) {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');

                        var newWorkItemRow = workItemTable.row.add([
                            '',
                            '',
                            '',
                            '',
                            '',
                            ''
                        ]).draw();

                        var newWorkItemRowNode = newWorkItemRow.node();

                        $(newWorkItemRowNode).css('color', 'red').animate({ color: 'black' });

                        var workItemSavedUnitSymbol = $.trim($('#addWorkItemPopupModal select#workItemUnitList option:selected').text());

                        saveNewWorkItemRow(workItemTable, newWorkItemRow, workItemSavedGuid, workItemName, arrWorkItemSubHeadsSelected, workItemSavedUnitSymbol);

                        bootbox.alert("Your WorkItem is created successfully.", function () {
                            $('#addWorkItemPopupModal').modal('hide');

                            //$('#workItemListMenuBtn').click();

                            $('#workItemPopupModal').modal('show');
                        });
                    } else {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');
                        bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                    }
                }

            } else {
                bootbox.alert("Please Select WorkItem SubHeads.");
            }

        }

    });

    $('#addWorkItemPopupModal').on('click', 'a#cancelNewWorkItemBtn', function (e) {
        e.preventDefault();

        //workItemTable
        //    .row(newWorkItemRow)
        //    .remove()
        //    .draw();

        //workItemTable.on('order.dt search.dt', function () {
        //    workItemTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
        //        cell.innerHTML = i + 1;
        //    });
        //}).draw();

        $('#addWorkItemPopupModal').modal('hide');

        $('#workItemPopupModal').modal('show');

        e.stopPropagation();
    });

}

createEditWorkItemPopupForm = function (workItemTable, selectedRow) {
    var arrWorkItemSubHeadsSelected = [];

    var selectedWorkItemGuid = workItemTable.row(selectedRow).data()[1];

    var selectedWorkItemData = getWorkItemData(selectedWorkItemGuid);

    var workItemSubHeadData = getWorkItemSubHeads();

    var unitTypeData = getUnitTypes();

    var unitData = getUnitsForUnitType(selectedWorkItemData.UnitManager.UnitTypeManager.UnitTypeGuid);

    $("#editWorkItemPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Edit WorkItem</h2>';

    html += '<div class="panel-body">';
    html += '<form id="editWorkItemPopupForm" name="editWorkItemPopupForm" class="form form-horizontal" role="form" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemName" id="workItemNameLabel" class="control-label pull-left">WorkItem Name</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="workItemName" name="workItemName" placeholder="WorkItem Name" class="form-control text-capitalize" value="' + selectedWorkItemData.WorkItemName + '" onfocus="workItemNameFocusFunction()" onblur="workItemNameBlurFunction()" style="width:100%;" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemSubHeadList" id="workItemSubHeadListLabel" class="control-label pull-left">WorkItem SubHead</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    //html += '<div class="btn-group-justified">';
    html += '<select class="form-control multiselect" name="workItemSubHeadList" id="workItemSubHeadList" multiple="multiple" size="5">';

    for (var i = 0; i < workItemSubHeadData.length; i++) {
        var optionSelected = false;
        for (var j = 0; j < selectedWorkItemData.WorkItemSubHeadManager.length; j++) {
            if (workItemSubHeadData[i].WorkItemSubHeadGuid == selectedWorkItemData.WorkItemSubHeadManager[j].WorkItemSubHeadGuid) {
                optionSelected = true;
                break;
            }
        }

        if (optionSelected === true) {
            html += '<option value="' + workItemSubHeadData[i].WorkItemSubHeadGuid + '" selected="selected">';
            html += workItemSubHeadData[i].WorkItemSubHeadName;
            html += '</option>';

            arrWorkItemSubHeadsSelected.push({
                workItemSubHeadGuid: workItemSubHeadData[i].WorkItemSubHeadGuid,
                workItemSubHeadName: workItemSubHeadData[i].WorkItemSubHeadName
            });
        } else {
            html += '<option value="' + workItemSubHeadData[i].WorkItemSubHeadGuid + '">';
            html += workItemSubHeadData[i].WorkItemSubHeadName;
            html += '</option>';
        }


    }

    html += '</select>';
    //html += '</div>';
    html += '</div>';
    html += '</div>';

    //html += '<fieldset class="scheduler-border">';
    //html += '<legend class="scheduler-border">WorkItem Unit</legend>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemUnitTypeList" id="workItemUnitTypeListLabel" class="control-label pull-left">Unit Type</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control multiselect" name="workItemUnitTypeList" id="workItemUnitTypeList">';

    html += '<option value="" selected="selected">' + '--Select UnitType--' + '</option>';
    for (var i = 0; i < unitTypeData.length; i++) {
        if (unitTypeData[i].UnitTypeGuid == selectedWorkItemData.UnitManager.UnitTypeManager.UnitTypeGuid) {
            html += '<option value="' + unitTypeData[i].UnitTypeGuid + '" selected="selected">';
            html += unitTypeData[i].UnitTypeName;
            html += '</option>';
        } else {
            html += '<option value="' + unitTypeData[i].UnitTypeGuid + '">';
            html += unitTypeData[i].UnitTypeName;
            html += '</option>';
        }
    }

    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div id="workItemUnitDiv">';// class="hidden">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemUnitList" id="workItemUnitListLabel" class="control-label pull-left">Unit</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control multiselect" name="workItemUnitList" id="workItemUnitList">';

    html += '<option value="" selected="selected">' + '--Select Unit--' + '</option>';
    for (var i = 0; i < unitData.length; i++) {
        if (unitData[i].UnitGuid == selectedWorkItemData.UnitManager.UnitGuid) {
            html += '<option value="' + unitData[i].UnitGuid + '" selected="selected">';
            html += unitData[i].UnitName;
            html += '</option>';
        } else {
            html += '<option value="' + unitData[i].UnitGuid + '">';
            html += unitData[i].UnitName;
            html += '</option>';
        }
    }

    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';

    html += '<div id="workItemSubHeadListDisplay" class="panel panel-default panel-body col-xs-12 col-sm-12 col-md-12 col-lg-12 well well-sm hidden"></div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    //html += '<button class="btn icon-btn-save btn-success submit" type="button">';
    //html += '<span class="btn-save-label">';
    //html += '<i class="glyphicon glyphicon-floppy-disk"></i>';
    //html += '</span>';
    //html += ' Save';
    //html += '</button>';

    html += '<a href="#" id="updateWorkItemBtn" name="updateWorkItemBtn" type="button" class="btn btn-lg btn-success btn-block">';
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Update" title="Update"></i>';
    html += '</span>';
    html += '  Update';
    html += '</a>';

    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    html += '<a href="#" id="cancelWorkItemEditBtn" name="cancelWorkItemEditBtn" type="button" class="btn btn-lg btn-danger btn-block">';//btn-primary
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i>';
    html += '</span>';
    html += ' Cancel';
    html += '</a>';

    html += '</div>';
    html += '</div>';


    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#editWorkItemPopupModal .modal-body").append(html);

    displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelected);

    $('#workItemPopupModal').modal('hide');

    //$('#editWorkItemPopupModal').modal('show');

    $('#editWorkItemPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    //$('#editWorkItemPopupModal input#workItemName').focus();

    var el = $('#editWorkItemPopupModal input#workItemName');
    var elemLen = el.val().length;
    el.focus();

    //el.selectRange(0, elemLen);
    el.setCursorPosition(elemLen);


    $('#workItemSubHeadList').multiselect({
        maxHeight: 150,
        enableCaseInsensitiveFiltering: true,
        numberDisplayed: 0,
        //includeSelectAllOption: true,
        nonSelectedText: '--Select WorkItem SubHead--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$('.multiselect').valid();
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                $('#workItemSubHeadListDisplay').removeClass('hidden');

                arrWorkItemSubHeadsSelected.push({
                    workItemSubHeadGuid: element.val(),
                    workItemSubHeadName: element.text()
                });

                displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelected);
            }
            else if (checked === false) {
                if (confirm('Do you wish to deselect the element?')) {
                    for (var i = arrWorkItemSubHeadsSelected.length - 1; i >= 0; i--) {
                        if (arrWorkItemSubHeadsSelected[i].workItemSubHeadGuid === element.val()) {
                            arrWorkItemSubHeadsSelected.splice(i, 1);
                        }
                    }

                    displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelected);
                }
                else {
                    $("#workItemSubHeadList").multiselect('select', element.val());

                    displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelected);
                }
            }
        },
        //onSelectAll: function (checked) {
        //    while (arrWorkItemSubHeadsSelectedForWorkItem.length > 0) {
        //        arrWorkItemSubHeadsSelectedForWorkItem.pop();
        //    }
        //    alert(checked);
        //}
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#workItemUnitTypeList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select UnitType--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$('.multiselect').valid();
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                displayWorkItemUnitsList(element.val());

                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#workItemUnitList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select Unit--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$('.multiselect').valid();
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#editWorkItemPopupForm').validate({ // initialize plugin
        ignore: ':hidden:not(".multiselect")',
        //ignore: ':hidden:not("#select")',
        //ignore: ":not(:visible)",
        rules: {
            workItemName: {
                required: true,
                noSpace: true
            },
            workItemSubHeadList: "required",
            workItemUnitTypeList: "required",
            workItemUnitList: "required"
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block small',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                element.closest('.form-group').append(error);
            }
        },
        submitHandler: function () {
            alert('valid form');
            return false;
        }
    });

    $('#editWorkItemPopupModal').on('click', 'a#updateWorkItemBtn', function (e) {
        e.preventDefault();

        if ($("#editWorkItemPopupForm").valid()) {

            if (arrWorkItemSubHeadsSelected.length > 0) {
                $("#preloader").show();
                $("#status").show();

                var workItemName = $.trim($('#editWorkItemPopupModal input#workItemName').val());

                var checkExistence = checkWorkItemExistence(workItemName);

                if (checkExistence == true) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    //bootbox.alert("There is already a WorkItem with this name.");
                    bootbox.confirm({
                        title: '',
                        message: 'There is already a WorkItem with this name.<br>You can update the rest of the details.',
                        buttons: {
                            'cancel': {
                                label: 'Change',
                                className: 'btn-default pull-left'
                            },
                            'confirm': {
                                label: 'Proceed',
                                className: 'btn-success pull-right'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                //window.location = $("a[data-bb='confirm']").attr('href');
                                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                                //window.location.href = nextUrl;
                                //window.location = 'Default.aspx';
                                updateWorkItemFunction(workItemName, checkExistence);
                            }
                        }
                    });
                } else if (checkExistence == false) {
                    updateWorkItemFunction(workItemName, checkExistence);
                    //for (var index = 0; index < arrWorkItemSubHeadsSelected.length; index++) {
                    //    arrWorkItemSubHeadsSelected[index].workItemSubHeadSequence = (index + 1);
                    //}

                    //var workItemUpdated = updateWorkItem(selectedWorkItemData.WorkItemGuid, workItemName, arrWorkItemSubHeadsSelected, checkExistence);

                    //if (workItemUpdated == 1) {
                    //    $('#status').delay(300).fadeOut();
                    //    $('#preloader').delay(350).fadeOut('slow');

                    //    var workItemUpdatedUnitSymbol = $.trim($('#editWorkItemPopupModal select#workItemUnitList option:selected').text());

                    //    updateWorkItemRow(workItemTable, selectedRow, workItemName, arrWorkItemSubHeadsSelected, workItemUpdatedUnitSymbol);

                    //    bootbox.alert("Your WorkItem is updated successfully.", function () {
                    //        $('#editWorkItemPopupModal').modal('hide');

                    //        //$('#workItemListMenuBtn').click();

                    //        $('#workItemPopupModal').modal('show');
                    //    });
                    //} else {
                    //    $('#status').delay(300).fadeOut();
                    //    $('#preloader').delay(350).fadeOut('slow');
                    //    bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
                    //}
                }

            } else {
                bootbox.alert("Please Select WorkItem SubHeads.");
            }

        }

    });

    $('#editWorkItemPopupModal').on('click', 'a#cancelWorkItemEditBtn', function (e) {
        e.preventDefault();

        //workItemTable
        //    .row(newWorkItemRow)
        //    .remove()
        //    .draw();

        //workItemTable.on('order.dt search.dt', function () {
        //    workItemTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
        //        cell.innerHTML = i + 1;
        //    });
        //}).draw();

        $('#editWorkItemPopupModal').modal('hide');

        $('#workItemPopupModal').modal('show');

        e.stopPropagation();
    });

    updateWorkItemFunction = function (workItemName, checkExistence) {
        for (var index = 0; index < arrWorkItemSubHeadsSelected.length; index++) {
            arrWorkItemSubHeadsSelected[index].workItemSubHeadSequence = (index + 1);
        }

        var workItemUpdated = updateWorkItem(selectedWorkItemData.WorkItemGuid, workItemName, arrWorkItemSubHeadsSelected, checkExistence);

        if (workItemUpdated == 1) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            var workItemUpdatedUnitSymbol = $.trim($('#editWorkItemPopupModal select#workItemUnitList option:selected').text());

            updateWorkItemRow(workItemTable, selectedRow, workItemName, arrWorkItemSubHeadsSelected, workItemUpdatedUnitSymbol);

            bootbox.alert("Your WorkItem is updated successfully.", function () {
                $('#editWorkItemPopupModal').modal('hide');

                //$('#workItemListMenuBtn').click();

                $('#workItemPopupModal').modal('show');
            });
        } else {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
        }
    }

}




workItemNameFocusFunction = function () {
    // Focus = Changes the background color of input to yellow
    //document.getElementById("workItemSubHeadName").style.background = "yellow";
    $('#addWorkItemPopupModal input#workItemName').css('background-color', 'yellow');
    $('#editWorkItemPopupModal input#workItemName').css('background-color', 'yellow');
}

workItemNameBlurFunction = function () {
    // No focus = Changes the background color of input to nothing
    //document.getElementById("workItemSubHeadName").style.background = "";
    $('#addWorkItemPopupModal input#workItemName').css('background-color', '');
    $('#editWorkItemPopupModal input#workItemName').css('background-color', '');
}




getWorkItemData = function (workItemGuid) {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetWorkItem",
        data: '{"workItemGuid":"' + workItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

getWorkItemSubHeads = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetWorkItemSubHeads",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //showWorkItemSubHeadsListOptionsToAddWorkItems('workItemSubHeadList', response.d);
            retValue = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
    return retValue;
}

getUnitTypes = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetUnitTypes",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //showUnitTypesListOptionsToAddWorkItem('workItemUnitTypeList', response.d);
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

getUnitsForUnitType = function (unitTypeGuid) {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetUnits",
        data: '{"unitTypeGuid":"' + unitTypeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}




displayWorkItemSubHeadsList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    var html = '<ul class="list-group text-left">';
    html += '<li class="nav nav-header disabled">';
    html += 'WorkItem SubHeads Selected';
    html += '</li>';
    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += (index + 1) + '. ' + arr[index].workItemSubHeadName;
        html += '</li>';
    }

    html += '</ul>';

    document.getElementById(divname).innerHTML = html;

    if (arr.length == 0) {
        div.addClass('hidden');
    }
}

displayWorkItemUnitsList = function (unitTypeGuid) {
    if (jQuery.Guid.IsValid(unitTypeGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(unitTypeGuid.toUpperCase())) {
        $('#workItemUnitDiv').removeClass('hidden');

        var options = [];

        var unitData = getUnitsForUnitType(unitTypeGuid);

        options.push({
            label: '--Select Unit--',
            value: '',
            selected: true
        });

        for (var i = 0; i < unitData.length ; i++) {
            options.push({
                label: unitData[i].UnitSymbol,
                value: unitData[i].UnitGuid,
            });

            //var options = [
            //    { label: 'A', title: '', value: '', selected: true },
            //    { label: 'Option 6', title: 'Option 6', value: '6', disabled: true }
            //];
        }

        $('#workItemUnitList').multiselect('dataprovider', options);

        $('#workItemUnitList').multiselect('refresh');

    } else if (stringIsNullOrEmpty(unitTypeGuid)) {
        $('#workItemUnitDiv').addClass('hidden');
    }
}



saveNewWorkItemRow = function (workItemTable, selectedRow, workItemSavedGuid, workItemSavedName, arrWorkItemSubHeadsSelected, workItemSavedUnitSymbol) {
    var selectedRowData = workItemTable.row(selectedRow).data();

    var str = '<ul class="text-left">';
    for (var i = 0; i < arrWorkItemSubHeadsSelected.length; i++) {
        str += '<li>';
        str += arrWorkItemSubHeadsSelected[i].workItemSubHeadName;
        str += '</li>';
    }
    str += '</ul>';

    selectedRowData[1] = workItemSavedGuid;
    selectedRowData[2] = capitalizeFirstAllWords(workItemSavedName);
    selectedRowData[3] = str;
    selectedRowData[4] = workItemSavedUnitSymbol;
    selectedRowData[5] = '<a href="#" class="viewWorkItemBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    workItemTable.row(selectedRow).data(selectedRowData);

    workItemTable.on('order.dt search.dt', function () {
        workItemTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

updateWorkItemRow = function (workItemTable, selectedRow, workItemUpdatedName, arrWorkItemSubHeadsSelected, workItemUpdatedUnitSymbol) {
    var selectedRowData = workItemTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    var str = '<ul class="text-left">';
    for (var i = 0; i < arrWorkItemSubHeadsSelected.length; i++) {
        str += '<li>';
        str += arrWorkItemSubHeadsSelected[i].workItemSubHeadName;
        str += '</li>';
    }
    str += '</ul>';

    selectedRowData[2] = capitalizeFirstAllWords(workItemUpdatedName);
    selectedRowData[3] = str;
    selectedRowData[4] = workItemUpdatedUnitSymbol;

    workItemTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

    //unitTable.draw();
}




checkWorkItemExistence = function (workItemName) {
    var retValue = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckWorkItemExistence",
        data: '{"workItemName":"' + workItemName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveWorkItem = function (workItemName, arrWorkItemSubHeadsSelected) {
    var unitTypeManager = {
        unitTypeGuid: $.trim($('#addWorkItemPopupModal select#workItemUnitTypeList').val()),
    };

    var unitManager = {
        unitGuid: $.trim($('#addWorkItemPopupModal select#workItemUnitList').val()),
        //unitSymbol: $.trim($('#addWorkItemPopupModal select#workItemUnitList option:selected').text()),
        unitTypeManager: unitTypeManager
    };

    var workItemManager = {
        workItemName: workItemName,
        unitManager: unitManager,
        workItemSubHeadManager: arrWorkItemSubHeadsSelected,
    }

    var workItemManagerJsonString = JSON.stringify(workItemManager);

    var retValue = jQuery.Guid.Empty();

    //arrWorkItemSubHeadsJSONString = JSON.stringify(getSimpleArray(arrWorkItemSubHeadsSelected))

    //var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveWorkItem",
        data: '{"workItemManager":' + workItemManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updateWorkItem = function (workItemGuid, workItemName, arrWorkItemSubHeadsSelected, checkExistence) {
    var unitTypeManager = {
        unitTypeGuid: $.trim($('#editWorkItemPopupModal select#workItemUnitTypeList').val()),
    };

    var unitManager = {
        unitGuid: $.trim($('#editWorkItemPopupModal select#workItemUnitList').val()),
        //unitSymbol: $.trim($('#addWorkItemPopupModal select#workItemUnitList option:selected').text()),
        unitTypeManager: unitTypeManager
    };

    var workItemManager = {
        workItemGuid: workItemGuid,
        workItemName: workItemName,
        unitManager: unitManager,
        workItemSubHeadManager: arrWorkItemSubHeadsSelected,
    }

    var workItemManagerJsonString = JSON.stringify(workItemManager);

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateWorkItem",
        data: '{"workItemManager":' + workItemManagerJsonString + ',"editWorkItemName":"' + checkExistence + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteWorkItem = function (workItemGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/DeleteWorkItem",
        data: '{"workItemGuid":"' + workItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}