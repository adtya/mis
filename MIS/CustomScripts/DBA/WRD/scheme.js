﻿$(document).ready(function () {

    $('#schemeBtn').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetAllSchemesData",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createSchemePopupForm(response.d);
            },
            failure: function (result) {
                bootbox.alert("Error");
            }
        });

        e.stopPropagation();
    });

    $('#schemePopupModal').on('click', '.modal-footer button#schemePopupModalCloseBtn', function (e) {
        e.preventDefault();

        location.reload(true);

        e.stopPropagation();
    });

    $('#addSchemePopupModal').on('click', '.modal-footer button#backFromAddSchemePopupModalBtn', function (e) {
        e.preventDefault();

        $('#addSchemePopupModal').modal('hide');

        $('#schemePopupModal').modal('show');

        e.stopPropagation();
    });

    $('#viewSchemePopupModal').on('click', '.modal-footer button#backToSchemePopupModalBtn', function (e) {
        e.preventDefault();

        $('#viewSchemePopupModal').modal('hide');

        $('#schemePopupModal').modal('show');

        e.stopPropagation();
    });

    $('#editSchemePopupModal').on('click', '.modal-footer button#backFromEditSchemePopupModalBtn', function (e) {
        e.preventDefault();

        $('#editSchemePopupModal').modal('hide');

        $('#schemePopupModal').modal('show');

        e.stopPropagation();
    });

});

createSchemePopupForm = function (schemeData) {
    $("#schemePopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">WRD Schemes</h2>';

    html += '<div class="panel-body">';
    html += '<form id="schemePopupForm" name="schemePopupForm" role="form" class="form form-horizontal" method="post">';

    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    html += '<div class="table-responsive">';
    html += '<table id="schemesList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'Schemes Guid' + '</th>';
    html += '<th>' + 'Circle Name' + '</th>';    
    html += '<th>' + 'Division Name' + '</th>';
    html += '<th>' + 'Scheme Name' + '</th>';
    html += '<th>' + 'Scheme Code' + '</th>';
    html += '<th>' + 'Scheme Type' + '</th>';
    //html += '<th>' + 'UTM East1' + '</th>';
    //html += '<th>' + 'UTM East2' + '</th>';
    //html += '<th>' + 'UTM North1' + '</th>';
    //html += '<th>' + 'UTM North2' + '</th>';
    //html += '<th>' + 'Drawing ID' + '</th>';
    //html += '<th>' + 'Area' + '</th>';
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    for (var i = 0; i < schemeData.length; i++) {
        html += '<tr>';

        html += '<td>' + '' + '</td>';
        html += '<td>' + schemeData[i].SchemeGuid + '</td>';
        html += '<td>' + schemeData[i].CircleManager.CircleName + " / " + schemeData[i].CircleManager.CircleCode + '</td>';
        html += '<td>' + schemeData[i].DivisionManager.DivisionName + " / " + schemeData[i].DivisionManager.DivisionCode + '</td>';
        html += '<td>' + schemeData[i].SchemeName + '</td>';
        html += '<td>' + schemeData[i].SchemeCode + '</td>';
        html += '<td>' + schemeData[i].SchemeType + '</td>';
        //html += '<td>' + schemeData[i].UtmEast1 + '</td>';
        //html += '<td>' + schemeData[i].UtmEast2 + '</td>';
        //html += '<td>' + schemeData[i].UtmNorth1 + '</td>';
        //html += '<td>' + schemeData[i].UtmNorth2 + '</td>';
        //html += '<td>' + schemeData[i].DrawingId + '</td>';
        //html += '<td>' + schemeData[i].Area + '</td>';
        html += '<td>' + '<a href="#" class="viewSchemeBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editSchemeBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteSchemeBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';

        html += '</tr>';
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#schemePopupModal .modal-body").append(html);

    $('#schemePopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    schemeTableRelatedFunctions();  

}

schemeTableRelatedFunctions = function () {
    var schemeRowEditing = null;

    var schemeTable = $('#schemePopupModal table#schemesList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        //destroy: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true,
                searchable: true
            },
            {
                targets: [3],
                orderable: true,
                searchable: true
            },
            {
                targets: [4],
                orderable: true,
                searchable: true
            },
            {
                targets: [5],
                orderable: true,
                searchable: true
            },
            //{
            //    targets: [6],
            //    orderable: true,
            //    searchable: true
            //},
            //{
            //    targets: [7],
            //    orderable: true,
            //    searchable: true
            //},
            //{
            //    targets: [8],
            //    orderable: true,
            //    searchable: true
            //},
            //{
            //    targets: [9],
            //    orderable: true,
            //    searchable: true
            //},
            //{
            //    targets: [10],
            //    orderable: true,
            //    searchable: true
            //},
            //{
            //    targets: [11],
            //    orderable: true,
            //    searchable: true
            //},
            //{
            //    targets: [12],
            //    orderable: true,
            //    searchable: true
            //},            
            {
                targets: [6],
                orderable: false,
                searchable: false
                //data: null,
                //defaultContent: '<a href="#" class="viewWorkItemBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>'
            }
        ],
        order: [[2, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Add New Scheme',
                className: 'btn-success addNewSchemeBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewScheme();

                    e.stopPropagation();
                }
            }
        ]

    });

    schemeTable.on('order.dt search.dt', function () {
        schemeTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    addNewScheme = function () {
        createAddSchemePopupForm(schemeTable);
    };

    $('#schemePopupModal').on('click', '#schemesList a.viewSchemeBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        var schemeDataTable = getSchemeDataModalDetails(schemeTable.row(selectedRow).data()[1]);

        $("#viewSchemePopupModal .modal-body").append(schemeDataTable);

        $('#schemePopupModal').modal('hide');

        $('#viewSchemePopupModal').modal('show');
    });

    $('#schemePopupModal').on('click', '#schemesList a.editSchemeBtn', function (e) {

        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        createEditSchemePopupForm(schemeTable, selectedRow);

    });

    $('#schemePopupModal').on('click', '#schemesList a.deleteSchemeBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

       var schemeDeleted = deleteScheme(schemeTable.row(selectedRow).data()[1]);

        if (schemeDeleted == 1) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Scheme is deleted successfully.", function () {
                schemeTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (schemeDeleted == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
        }

    });

}

createAddSchemePopupForm = function (schemeTable) {

    var circleData = getCircles();
   
    $("#addSchemePopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Add Scheme</h2>';

    html += '<div class="panel-body">';
    html += '<form id="addSchemePopupForm" class="form form-horizontal" method="post">';
        
    html += '<div class="row">';    
    html += '<div class="col-sm-12 col-md-12">';

    html += '<div class="col-sm-6 col-md-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="circleListToAddScheme" id="circleListToAddSchemeLabel" class="control-label">Circle Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<select id="circleListToAddScheme" name="circleListToAddScheme" class="form-control">';
    html += '<option value="" selected="selected">' + 'Select Circle' + '</option>';
    for (var i = 0; i < circleData.length;) {
        html += '<option value="' + circleData[i] + '">';
        html += circleData[i + 2] + " (" + circleData[i + 1] + ")";
        html += '</option>';
        i = i + 3;
    }
    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-sm-6 col-md-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="divisionListToAddSchemeBasedOnCircleSelected" id="divisionListToAddSchemeBasedOnCircleSelectedLabel" class="control-label">Division Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<select id="divisionListToAddSchemeBasedOnCircleSelected" name="divisionListToAddSchemeBasedOnCircleSelected" class="form-control">';
    html += '<option value="">--Select Division--</option>';
    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-sm-12 col-md-12">';

    html += '<div class="col-sm-6 col-md-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="schemeName" id="schemeNameLabel" class="control-label">Scheme Name</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="schemeName" name="schemeName" placeholder="Scheme Name" class="form-control text-capitalize" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="schemeCode" id="schemeCodeLabel" class="control-label">Scheme Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="schemeCode" name="schemeCode" placeholder="Scheme Code" class="form-control text-uppercase" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-sm-12 col-md-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="schemeType" id="schemeTypeLabel" class="control-label">Scheme Type</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="schemeType" name="schemeType" placeholder="Scheme Type" class="form-control text-capitalize" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="area" id="areaLabel" class="control-label">Area</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="area" name="area" placeholder="Area" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    
    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="utmEast1" id="utmEast1Label" class="control-label">UTM East1</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="utmEast1" name="utmEast1" placeholder="UTM East1" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';    

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="utmEast2" id="utmEast2Label" class="control-label">UTM East2</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="utmEast2" name="utmEast2" placeholder="UTM East2" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="utmNorth1" id="utmNorth1Label" class="control-label">UTM North1</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="utmNorth1" name="utmNorth1" placeholder="UTM North1" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="utmNorth2" id="utmNorth2Label" class="control-label">UTM North2</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="utmNorth2" name="utmNorth2" placeholder="UTM North2" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="drawingId" id="drawingIdLabel" class="control-label">Drawing ID</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="drawingId" name="drawingId" placeholder="Drawing ID" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';  
     
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    html += '<a href="#" id="saveNewSchemeBtn" name="saveNewSchemeBtn" type="button" class="btn btn-lg btn-success btn-block">';
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i>';
    html += '</span>';
    html += '  Save';
    html += '</a>';

    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    html += '<a href="#" id="cancelNewSchemeBtn" name="cancelNewSchemeBtn" type="button" class="btn btn-lg btn-danger btn-block">';//btn-primary
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i>';
    html += '</span>';
    html += ' Cancel';
    html += '</a>';

    html += '</div>';
    html += '</div>';

    html += '</form>';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#addSchemePopupModal .modal-body").append(html);

    //showCircleListOptionsToAddScheme('circleListToAddScheme', getCircles());

    addSchemeValidationRules();

    $('#addSchemePopupModal').modal('show');

    $('#schemePopupModal').modal('hide');

    $('#circleListToAddScheme').on('change', function () {
        $('#divisionListToAddSchemeBasedOnCircleSelected').empty();
        var selectedCircleGuid = $(this).val();
        $.ajax({
            type: "POST",
            async: false,
            url: "DBAMethods.asmx/GetDivisionsBasedOnCircleGuid",
            data: '{"circleGuid":"' + selectedCircleGuid + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                divisionArr = response.d;
                showDivisionListOptionsToAddScheme('divisionListToAddSchemeBasedOnCircleSelected', divisionArr);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

    $('#addSchemePopupModal').on('click', 'a#saveNewSchemeBtn', function (e) {
        e.preventDefault();

        if ($("#addSchemePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            var schemeCode = $.trim($('#addSchemePopupModal input#schemeCode').val());
            var schemeName = $.trim($('#addSchemePopupModal input#schemeName').val());
            var schemeType = $.trim($('#addSchemePopupModal input#schemeType').val());

            var checkExistence = checkSchemeExistence(schemeCode, schemeName);

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a Scheme with this name.");
            } else if (checkExistence == false) {

                /* Get the row as a parent of the link that was clicked on */
                var selectedRow = $(this).parents('tr')[0];
                schemeSavedGuid = saveSchemeData(schemeTable, selectedRow);

                if (jQuery.Guid.IsValid(schemeSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(schemeSavedGuid.toUpperCase())) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    var newSchemeRow = schemeTable.row.add([
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        ''
                    ]).draw(false);

                    var newSchemeRowNode = newSchemeRow.node();

                    $(newSchemeRowNode).css('color', 'red').animate({ color: 'black' });

                    var selectedCircleTextSplit = $("#circleListToAddScheme :selected").text();
                    selectedCircleTextSplit = multiSplit(selectedCircleTextSplit, [' (', ')']);

                    var selectedDivisionTextSplit = $("#divisionListToAddSchemeBasedOnCircleSelected :selected").text();

                    saveNewSchemeRow(schemeTable, newSchemeRow, schemeSavedGuid, selectedCircleTextSplit[0] + "/" + selectedCircleTextSplit[1], selectedDivisionTextSplit, schemeName, schemeCode, schemeType);

                    bootbox.alert("Scheme is created successfully.", function () {
                        $('#addSchemePopupModal').modal('hide');

                        $('#schemePopupModal').modal('show');
                    });

                } else {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }
            }
        }

    });

    $('#addSchemePopupModal').on('click', 'a#cancelNewSchemeBtn', function (e) {
        e.preventDefault();

        $('#addSchemePopupModal').modal('hide');

        $('#schemePopupModal').modal('show');

        e.stopPropagation();
    });

}

createEditSchemePopupForm = function (schemeTable, selectedRow) {
    
    var selectedSchemeGuid = schemeTable.row(selectedRow).data()[1];

    var selectedSchemeData = getSchemeData(selectedSchemeGuid);

    $("#editSchemePopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Edit Scheme</h2>';

    html += '<div class="panel-body">';
    html += '<form id="editSchemePopupForm" name="editSchemePopupForm" class="form form-horizontal" role="form" method="post">';

    html += '<div class="row">';
    html += '<div class="col-sm-12 col-md-12">';

    html += '<div class="col-sm-6 col-md-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="circleNameForSchemes" id="circleNameForSchemesLabel" class="control-label pull-left">Circle Name</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input type="text" id="circleNameForSchemes" name="circleNameForSchemes" placeholder="Circle Name" class="form-control text-capitalize" value="' + selectedSchemeData.CircleManager.CircleName + '" readonly= "readonly" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';


    html += '<div class="col-sm-6 col-md-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="circleCodeForSchemes" id="circleCodeForSchemesLabel" class="control-label">Circle Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input type="text" id="circleCodeForSchemes" name="circleCodeForSchemes" placeholder="Circle Code" class="form-control text-capitalize" value="' + selectedSchemeData.CircleManager.CircleCode + '" readonly= "readonly" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-sm-12 col-md-12">';

    html += '<div class="col-sm-6 col-md-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="divisionNameForSchemes" id="divisionNameForSchemesLabel" class="control-label pull-left">Division Name</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input type="text" id="divisionNameForSchemes" name="divisionNameForSchemes" placeholder="Division Name" class="form-control text-capitalize" value="' + selectedSchemeData.DivisionManager.DivisionName + '" readonly= "readonly" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-sm-6 col-md-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="divisionCodeForSchemes" id="divisionCodeForSchemesLabel" class="control-label">Division Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input type="text" id="divisionCodeForSchemes" name="divisionCodeForSchemes" placeholder="Division Code" class="form-control text-capitalize" value="' + selectedSchemeData.DivisionManager.DivisionCode + '" readonly= "readonly" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-sm-12 col-md-12">';

    html += '<div class="col-sm-6 col-md-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedSchemeName" id="edittedSchemeNameLabel" class="control-label">Scheme Name</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedSchemeGuid" name="edittedSchemeGuid" placeholder="Scheme Guid" class="form-control text-capitalize" value="' + selectedSchemeData.SchemeGuid + '" style="display:none" style="width:100%;" />';
    html += '<input id="edittedSchemeName" name="edittedSchemeName" placeholder="Scheme Name" class="form-control text-capitalize" value="' + selectedSchemeData.SchemeName + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedSchemeCode" id="edittedSchemeCodeLabel" class="control-label">Scheme Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedSchemeCode" name="edittedSchemeCode" placeholder="Scheme Code" class="form-control text-uppercase" value="' + selectedSchemeData.SchemeCode + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-sm-12 col-md-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedSchemeType" id="edittedSchemeTypeLabel" class="control-label">Scheme Type</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedSchemeType" name="edittedSchemeType" placeholder="Scheme Type" class="form-control text-capitalize" value="' + selectedSchemeData.SchemeType + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedArea" id="edittedAreaLabel" class="control-label">Area</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedArea" name="edittedArea" placeholder="Area" class="form-control" value="' + selectedSchemeData.Area + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedUtmEast1" id="edittedUtmEast1Label" class="control-label">UTM East1</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedUtmEast1" name="edittedUtmEast1" placeholder="UTM East1" class="form-control" value="' + selectedSchemeData.UtmEast1 + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedUtmEast2" id="edittedUtmEast2Label" class="control-label">UTM East2</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedUtmEast2" name="edittedUtmEast2" placeholder="UTM East2" class="form-control" value="' + selectedSchemeData.UtmEast2 + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedUtmNorth1" id="edittedUtmNorth1Label" class="control-label">UTM North1</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedUtmNorth1" name="edittedUtmNorth1" placeholder="UTM North1" class="form-control" value="' + selectedSchemeData.UtmNorth1 + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedUtmNorth2" id="edittedUtmNorth2Label" class="control-label">UTM North2</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedUtmNorth2" name="edittedUtmNorth2" placeholder="UTM North2" class="form-control" value="' + selectedSchemeData.UtmNorth2 + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedDrawingId" id="edittedDrawingIdLabel" class="control-label">Drawing ID</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedDrawingId" name="edittedDrawingId" placeholder="Drawing ID" class="form-control" value="' + selectedSchemeData.DrawingId + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div id="schemeListDisplay" class="panel panel-default panel-body col-xs-12 col-sm-12 col-md-12 col-lg-12 well well-sm hidden"></div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    html += '<a href="#" id="updateSchemeBtn" name="updateSchemeBtn" type="button" class="btn btn-lg btn-success btn-block">';
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Update" title="Update"></i>';
    html += '</span>';
    html += '  Update';
    html += '</a>';

    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    html += '<a href="#" id="cancelSchemeEditBtn" name="cancelSchemeEditBtn" type="button" class="btn btn-lg btn-danger btn-block">';//btn-primary
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i>';
    html += '</span>';
    html += ' Cancel';
    html += '</a>';

    html += '</div>';
    html += '</div>';
    
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#editSchemePopupModal .modal-body").append(html);
    
    $('#schemePopupModal').modal('hide');

    $('#editSchemePopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    var el = $('#editSchemePopupModal input#edittedSchemeName');
    var elemLen = el.val().length;
    el.focus();

    el.setCursorPosition(elemLen);
    
    $('#editSchemePopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            edittedSchemeCode: {
                required: true,
                noSpace: true              
            },
            edittedSchemeName: {
                required: true,
                noSpace: true
            },
            edittedSchemeType: {
                required: true,
                noSpace: true
            },
            edittedUtmEast1: {
                number: true,
            },
            edittedUtmEast2: {
                number: true,
            },
            edittedUtmNorth1: {
                number: true,
            },
            edittedUtmNorth2: {
                number: true,
            },
            edittedDrawingId: {
                number: true,
            },
            edittedArea: {
                number: true,
            }

        }
    });

    $('#editSchemePopupModal').on('click', 'a#updateSchemeBtn', function (e) {
        e.preventDefault();

        if ($("#editSchemePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            var schemeGuid = $.trim($('#editSchemePopupModal input#edittedSchemeGuid').val());
            var schemeCode = $.trim($('#editSchemePopupModal input#edittedSchemeCode').val());
            var schemeName = $.trim($('#editSchemePopupModal input#edittedSchemeName').val());
            var schemeType = $.trim($('#editSchemePopupModal input#edittedSchemeType').val());

            var checkExistence = checkSchemeExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a Scheme with this name.");
            } else if (checkExistence == false) {

                var schemeUpdated = updateScheme(schemeTable.row(selectedRow).data()[1]);

                if (schemeUpdated == 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    updateSchemeRow(schemeTable, selectedRow, schemeGuid, schemeName, schemeCode, schemeType);

                    bootbox.alert("Scheme is updated successfully.", function () {
                        $('#editSchemePopupModal').modal('hide');

                        $('#schemePopupModal').modal('show');
                    });
                } else if (schemeUpdated == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Data cannot be updated.<br>Please contact your database administrator.");
                }
            }

        }

    });

    $('#editSchemePopupModal').on('click', 'a#cancelSchemeEditBtn', function (e) {
        e.preventDefault();

        $('#editSchemePopupModal').modal('hide');

        $('#schemePopupModal').modal('show');

        e.stopPropagation();
    });

}

getCircles = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetCircleNames",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
    return retValue;
}

getSchemeData = function (selectedSchemeGuid) {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/EditSchemeData",
        data: '{"schemeGuid":"' + selectedSchemeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

getSchemeDataModalDetails = function (schemeGuid) {

    //$("#preloader").show();
    //$("#status").show();
    var str;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/ViewSchemeData",
        data: '{"schemeGuid":"' + schemeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //str = str1;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return str;
}

showDivisionListOptionsToAddScheme = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Division--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 3;
    }
}

saveNewSchemeRow = function (schemeTable, selectedRow, schemeSavedGuid) {
    var selectedRowData = schemeTable.row(selectedRow).data();

    var selectedCircleTextSplit = $("#circleListToAddScheme :selected").text();
    selectedCircleTextSplit = multiSplit(selectedCircleTextSplit, [' (', ')']);

    selectedCircleName = selectedCircleTextSplit[0] + " / " + selectedCircleTextSplit[1];

    var selectedDivisionTextSplit = $("#divisionListToAddSchemeBasedOnCircleSelected :selected").text();

    var schemeCode = $.trim($('#addSchemePopupModal input#schemeCode').val());
    var schemeName = $.trim($('#addSchemePopupModal input#schemeName').val());
    var schemeType = $.trim($('#addSchemePopupModal input#schemeType').val());

    selectedRowData[1] = schemeSavedGuid;
    selectedRowData[2] = selectedCircleName;
    selectedRowData[3] = selectedDivisionTextSplit;
    selectedRowData[4] = schemeName;
    selectedRowData[5] = schemeCode;
    selectedRowData[6] = schemeType;
    selectedRowData[7] = '<a href="#" class="viewSchemeBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editSchemeBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteSchemeBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';
    schemeTable.row(selectedRow).data(selectedRowData);

    schemeTable.on('order.dt search.dt', function () {
        schemeTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

updateSchemeRow = function (schemeTable, selectedRow, schemeGuid, schemeName, schemCode, schemeType) {
    var selectedRowData = schemeTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[4] = schemeName;
    selectedRowData[5] = schemCode;
    selectedRowData[6] = schemeType;

    schemeTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

}

checkSchemeExistence = function (schemeCode, schemeName) {
    var retValue = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckSchemeExistence",
        data: '{"schemeCode":"' + schemeCode + '","schemeName":"' + schemeName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

addSchemeValidationRules = function () {
    $('#addSchemePopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            circleListToAddScheme: "required",
            divisionListToAddSchemeBasedOnCircleSelected: "required",
            schemeCode: {
                required: true,
                noSpace: true,
                minlength: 3,
                maxlength: 5
            },
            schemeName: {
                required: true,
                noSpace: true
            },
            schemeType: {
                required: true,
                noSpace: true
            },
            utmEast1: {
                number: true,
            },
            utmEast2: {
                number: true,
            },
            utmNorth1: {
                number: true,
            },
            utmNorth2: {
                number: true,
            },
            drawingId: {
                number: true,
            },
            area: {
                number: true,
            }

        }
    });
}

saveSchemeData = function () {

    var circleGuid = $('#circleListToAddScheme').val();
    var divisionGuid = $('#divisionListToAddSchemeBasedOnCircleSelected').val();
    var schemeName = $('#schemeName').val();
    var schemeCode = $('#schemeCode').val();
    var schemeType = $('#schemeType').val();
    var utmEast1 = $('#utmEast1').val();
    var utmEast2 = $('#utmEast2').val();
    var utmNorth1 = $('#utmNorth1').val();
    var utmNorth2 = $('#utmNorth2').val();
    var area = $('#area').val();
    var drawingId = $('#drawingId').val();

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveScheme",
        data: '{"schemeCode":"' + schemeCode + '","schemeType":"' + schemeType + '","schemeName":"' + schemeName +
            '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 +
            '","area":"' + area + '","circleGuid":"' + circleGuid + '","divisionGuid":"' + divisionGuid + '","drawingId":"' + drawingId + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateScheme = function (schemeGuid) {

    var schemeGuid = $("#edittedSchemeGuid").val();
    var edittedSchemeCode = $("#edittedSchemeCode").val();
    var edittedSchemeName = $("#edittedSchemeName").val();
    var edittedSchemeType = $("#edittedSchemeType").val();
    var edittedUtmEast1 = $("#edittedUtmEast1").val();
    var edittedUtmEast2 = $("#edittedUtmEast2").val();
    var edittedUtmNorth1 = $("#edittedUtmNorth1").val();
    var edittedUtmNorth2 = $("#edittedUtmNorth2").val();
    var edittedArea = $("#edittedArea").val();
    var edittedDrawingId = $("#edittedDrawingId").val();

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateScheme",
        data: '{"schemeGuid":"' + schemeGuid +
            '","edittedSchemeCode":"' + edittedSchemeCode + '","edittedSchemeType":"' + edittedSchemeType +
            '","edittedSchemeName":"' + edittedSchemeName + '","edittedUtmEast1":"' + edittedUtmEast1 + '","edittedUtmEast2":"' + edittedUtmEast2 +
            '","edittedUtmNorth1":"' + edittedUtmNorth1 + '","edittedUtmNorth2":"' + edittedUtmNorth2 +
            '","edittedArea":"' + edittedArea + '","edittedDrawingId":"' + edittedDrawingId + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteScheme = function (schemeGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/DeleteScheme",
        data: '{"schemeGuid":"' + schemeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}