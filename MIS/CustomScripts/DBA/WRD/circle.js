﻿$(document).ready(function () {

    $('#circleBtn').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetCircleNames",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createCirclePopupForm(response.d);
            },
            failure: function (result) {
                bootbox.alert("Error");
            }
        });
        e.stopPropagation();
    });

    $('#circlePopupModal').on('click', '.modal-footer button#circlePopupModalCloseBtn', function (e) {
        e.preventDefault();

        location.reload(true);

        e.stopPropagation();
    });

});

createCirclePopupForm = function (circleData) {
    $("#circlePopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">WRD Circles</h2>';

    html += '<div class="panel-body">';
    html += '<form id="circlePopupForm" name="circlePopupForm" role="form" class="form form-horizontal" method="post">';
   
    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    html += '<div class="table-responsive">';
    html += '<table id="circleList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'Circle Guid' + '</th>';
    html += '<th>' + 'Circle Name' + '</th>';
    html += '<th>' + 'Circle Code' + '</th>';    
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    for (var i = 0; i < circleData.length;) {
        html += '<tr>';

        html += '<td>' + '' + '</td>';
        html += '<td>' + circleData[i] + '</td>';
        html += '<td>' + circleData[i + 2] + '</td>';
        html += '<td>' + circleData[i + 1] + '</td>';
        html += '<td>' + '<a href="#" class="editCircleBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteCircleBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';
        //html += '<td>' + '<a href="#" class="viewCircleBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editCircleBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteCircleBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';

        html += '</tr>';
        i = i + 3;
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';
    //} else {
    //    html += '<p>There is no WorkItem SubHead available to be displayed.</p>';
    //}

    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#circlePopupModal .modal-body").append(html);

    //$('#workItemSubHeadPopupModal').modal('show');

    $('#circlePopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    circleTableRelatedFunctions();

    $('#circlePopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            circleCode: {
                required: true,
                noSpace: false
            },
            circleName: {
                required: true,
                noSpace: false
            }
        },
        messages: {
            circleCode: {
                required: "Please Enter Circle Code.",
                Circle: "Circle Code cannot be empty."
            },
            circleName: {
                required: "Please Enter Circle Name.",
                Circle: "Circle Name cannot be empty."
            }
        },
        
    });

}

circleTableRelatedFunctions = function () {
    var circleEditing = null;

    var circleTable = $('#circlePopupModal table#circleList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        //destroy: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true,
                searchable: true,
            },
            {
                targets: [3],
                orderable: true,
                searchable: true,
                //data: null,
                //defaultContent: '<a href="#" class="viewWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>'
            }
        ],
        order: [[2, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Add New Circle',
                className: 'btn-success addNewCircleBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewCircle();

                    e.stopPropagation();
                }
            }
        ]
    });

    circleTable.on('order.dt search.dt', function () {
        circleTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addNewCircleBtn = circleTable.button(['.addNewCircleBtn']);

    addNewCircle = function () {

        if (circleEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreCircleRowData(circleTable, circleEditing);
            circleEditing = null;
            circleTable.enable();
        }

        var newCircleRow = circleTable.row.add([
            '',
            '',
            '',
            '',
            ''
        ]).draw(false);

        var newCircleRowNode = newCircleRow.node();

        addNewCircleRow(circleTable, newCircleRowNode);

        circleEditing = newCircleRowNode;

        addNewCircleBtn.disable();

        $('#circlePopupModal input#circleCode').focus();
        $('#circlePopupModal input#circleName').focus();

        $(newCircleRowNode).css('color', 'red').animate({ color: 'black' });
    };

    $('#circlePopupModal').on('click', '#circleList a.saveNewCircleBtn', function (e) {
        e.preventDefault();

        if ($("#circlePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var checkExistence = checkCircleExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a Circle with this name.");
            } else if (checkExistence == false) {
                var circleSavedGuid = saveCircle();

                if (jQuery.Guid.IsValid(circleSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(circleSavedGuid.toUpperCase())) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your Circle is created successfully.", function () {
                        if (circleEditing !== null && circleEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            saveNewCircleRow(circleTable, circleEditing, circleSavedGuid);
                            circleEditing = null;
                            addNewCircleBtn.enable();
                        }
                    });
                } else {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }
            }

        }

    });

    $('#circlePopupModal').on('click', '#circleList a.cancelNewCircleBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (circleEditing !== null && circleEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            removeNewCircleRowCreatedOnCancel(circleTable, selectedRow);
            circleEditing = null;
            addNewCircleBtn.enable();
        }

    });

    $('#circlePopupModal').on('click', '#circleList a.editCircleBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];        

        if (circleEditing !== null && circleEditing != selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreCircleRowData(circleTable, circleEditing);
            editCircleRow(circleTable, selectedRow);
            circleEditing = selectedRow;
            addNewCircleBtn.enable();
         
            var el = $('#circlePopupModal input#circleName');
            var elemLen = el.val().length;
            el.focus();
            
            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);

        } else {
            /* No row currently being edited */
            editCircleRow(circleTable, selectedRow);
            circleEditing = selectedRow;
            addNewCircleBtn.enable();
            
            var el = $('#circlePopupModal input#circleName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);           
        }
    });

    $('#circlePopupModal').on('click', '#circleList a.updateCircleBtn', function (e) {
        e.preventDefault();

        if ($("#circlePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var checkExistence = checkCircleExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a Circle with this name.");
            } else if (checkExistence == false) {
                var circleUpdated = updateCircle(circleTable.row(selectedRow).data()[1]);

                if (circleUpdated == 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    //$('#workItemSubHeadName').val('');
                    bootbox.alert("Your Circle is updated successfully.", function () {
                        if (circleEditing !== null && circleEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            updateCircleRow(circleTable, circleEditing);
                            circleEditing = null;
                            addNewCircleBtn.enable();
                        }
                    });
                } else if (circleUpdated == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
                }
            }

        }

    });

    $('#circlePopupModal').on('click', '#circleList a.cancelCircleBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (circleEditing !== null && circleEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreCircleRowData(circleTable, circleEditing);
            circleEditing = null;
            addNewCircleBtn.enable();
        }

    });

    $('#circlePopupModal').on('click', '#circleList a.deleteCircleBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        var circleDeleted = deleteCircle(circleTable.row(selectedRow).data()[1]);

        if (circleDeleted == 1) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Circle is deleted successfully.", function () {
                circleTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (circleDeleted == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
        }

    });

}

addNewCircleRow = function (circleTable, selectedRow) {
    var selectedRowData = circleTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //workItemSubHeadTable.order([2, 'asc']).draw();

    circleTable.column('1:visible').order('asc').draw();

    circleTable.on('order.dt search.dt', function () {

        var x = circleTable.order();

        if (x[0][1] == "asc") {
            var a = circleTable.column(0, { search: 'applied', order: 'applied' }).nodes();
            for (i = 1; i < a.length; i++) {
                a[i].innerHTML = i;
            }
        } else {
            circleTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }
        availableTds[0].innerHTML = '*';
    }).draw(false);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[0].innerHTML = '*';
    availableTds[1].innerHTML = '<input type="text" id="circleName" name="circleName" placeholder="Circle Name" class="form-control text-capitalize" value="' + selectedRowData[2] + '" onfocus="circleNameFocusFunction()" onblur="circleNameBlurFunction()" style="width:100%;">';//Circle Name
    availableTds[2].innerHTML = '<input type="text" id="circleCode" name="circleCode" placeholder="Circle Code" class="form-control text-capitalize" value="' + selectedRowData[2] + '" onfocus="circleCodeFocusFunction()" onblur="circleCodeBlurFunction()" style="width:100%;">';//Circle Code
    availableTds[3].innerHTML = '<a href="#" class="saveNewCircleBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelNewCircleBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

saveNewCircleRow = function (circleTable, selectedRow, circleSavedGuid) {

    var selectedRowData = circleTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    selectedRowData[1] = circleSavedGuid;
    selectedRowData[2] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
    selectedRowData[3] = capitalizeFirstAllWords($.trim(availableInputs[1].value));
    selectedRowData[4] = '<a href="#" class="editCircleBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteCircleBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    circleTable.row(selectedRow).data(selectedRowData);

    circleTable.on('order.dt search.dt', function () {
        circleTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

removeNewCircleRowCreatedOnCancel = function (circleTable, selectedRow) {

    circleTable
        .row(selectedRow)
        .remove()
        .draw();

    circleTable.on('order.dt search.dt', function () {
        circleTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

editCircleRow = function (circleTable, selectedRow) {
    var selectedRowData = circleTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[1].innerHTML = '<input type="text" id="circleName" name="circleName" placeholder="Circle Name" class="form-control text-capitalize" value="' + selectedRowData[2] + '" onfocus="circleNameFocusFunction()" onblur="circleNameBlurFunction()" style="width:100%;">';//Circle Name
    availableTds[2].innerHTML = '<input type="text" id="circleCode" name="circleCode" placeholder="Circle Code" class="form-control text-capitalize" value="' + selectedRowData[3] + '" onfocus="circleCodeFocusFunction()" onblur="circleCodeBlurFunction()" style="width:100%;">';//Circle Code
    availableTds[3].innerHTML = '<a href="#" class="updateCircleBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Update" title="Update"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelCircleBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

updateCircleRow = function (circleTable, selectedRow) {
    var selectedRowData = circleTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[2] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
    selectedRowData[3] = capitalizeFirstAllWords($.trim(availableInputs[1].value));

    circleTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;
  
}

restoreCircleRowData = function (circleTable, previousRow) {
    var previousRowData = circleTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewCircleRowCreatedOnCancel(circleTable, previousRow);
    } else {
        circleTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;

    }

}






circleNameFocusFunction = function () {
    // Focus = Changes the background color of input to yellow
    //document.getElementById("workItemSubHeadName").style.background = "yellow";
    $('#circlePopupModal input#circleName').css('background-color', 'yellow');
}

circleNameBlurFunction = function () {
    // No focus = Changes the background color of input to nothing
    //document.getElementById("workItemSubHeadName").style.background = "";
    $('#circlePopupModal input#circleName').css('background-color', '');
}

circleCodeFocusFunction = function () {
    // Focus = Changes the background color of input to yellow
    //document.getElementById("workItemSubHeadName").style.background = "yellow";
    $('#circlePopupModal input#circleCode').css('background-color', 'yellow');
}

circleCodeBlurFunction = function () {
    // No focus = Changes the background color of input to nothing
    //document.getElementById("workItemSubHeadName").style.background = "";
    $('#circlePopupModal input#circleCode').css('background-color', '');
}






capitalizeFirstAllWords = function (str) {
    var pieces = str.split(" ");
    for (var i = 0; i < pieces.length; i++) {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
}

checkCircleExistence = function () {
    var circleName = $.trim($('#circlePopupModal input#circleName').val());
    var circleCode = $.trim($('#circlePopupModal input#circleCode').val());

    var retValue = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckCircleExistence",
        data: '{"circleName":"' + circleName + '","circleCode":"' + circleCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveCircle = function () {
    var circleName = $.trim($('#circlePopupModal input#circleName').val());
    var circleCode = $.trim($('#circlePopupModal input#circleCode').val());

    var retValue = jQuery.Guid.Empty();   

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveCircle",
        data: '{"circleCode":"' + circleCode + '","circleName":"' + circleName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updateCircle = function (circleGuid) {
    var circleName = $.trim($('#circlePopupModal input#circleName').val());
    var circleCode = $.trim($('#circlePopupModal input#circleCode').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateCircle",
        data: '{"circleGuid":"' + circleGuid + '","circleName":"' + circleName + '","circleCode":"' + circleCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteCircle = function (circleGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/DeleteCircle",
        data: '{"circleGuid":"' + circleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}