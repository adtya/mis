﻿$(document).ready(function () {

    $('#divisionBtn').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetDivisionsWithCircle",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createDivisionsPopupForm(response.d);
            },
            failure: function (result) {
                bootbox.alert("Error");
            }
        });      
       
        e.stopPropagation();
    });

    $('#divisionPopupModal').on('click', '.modal-footer button#divisionPopupModalCloseBtn', function (e) {
        e.preventDefault();

        location.reload(true);

        e.stopPropagation();
    });

    $('#addWorkItemPopupModal').on('click', '.modal-footer button#backFromAddDivisionPopupModalCloseBtn', function (e) {
        e.preventDefault();

        $('#addDivisionPopupModal').modal('hide');

        $('#divisionPopupModal').modal('show');

        e.stopPropagation();
    });

    $('#viewDivisionPopupModal').on('click', '.modal-footer button#backToDivisionPopupModalBtn', function (e) {
        e.preventDefault();

        $('#viewDivisionPopupModal').modal('hide');

        $('#divisionPopupModal').modal('show');

        e.stopPropagation();
    });

    $('#editDivisionPopupModal').on('click', '.modal-footer button#backFromEditDivisionPopupModalBtn', function (e) {
        e.preventDefault();

        $('#editDivisionPopupModal').modal('hide');

        $('#divisionPopupModal').modal('show');

        e.stopPropagation();
    });

});

createDivisionsPopupForm = function (divisionData) {
    $("#divisionPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">WRD Divisions</h2>';

    html += '<div class="panel-body">';
    html += '<form id="divisionPopupForm" name="divisionPopupForm" role="form" class="form form-horizontal" method="post">';
    
    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    html += '<div class="table-responsive">';
    html += '<table id="divisionsList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'Division Guid' + '</th>';
    html += '<th>' + 'Circle Name' + '</th>';
    html += '<th>' + 'Circle Code' + '</th>';
    html += '<th>' + 'Division Name' + '</th>';
    html += '<th>' + 'Division Code' + '</th>';
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    for (var i = 0; i < divisionData.length; i++) {
        html += '<tr>';

        html += '<td>' + '' + '</td>';
        html += '<td>' + divisionData[i].DivisionGuid + '</td>';        
        html += '<td>' + divisionData[i].CircleManager.CircleName + '</td>';
        html += '<td>' + divisionData[i].CircleManager.CircleCode + '</td>';
        html += '<td>' + divisionData[i].DivisionName + '</td>';
        html += '<td>' + divisionData[i].DivisionCode + '</td>';    
        html += '<td>' + '<a href="#" class="viewDivisionBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editDivisionBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteDivisionBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';

        html += '</tr>';
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';
   
    html += '</div>';
    html += '</div>';

    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#divisionPopupModal .modal-body").append(html);

    $('#divisionPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    divisionTableRelatedFunctions();

    $('#divisionPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            divisionName: {
                required: true,
                noSpace: true
            },
            divisionCode: {
                required: true,
                noSpace: true
            }
        },
        messages: {
            divisionName: {
                required: "Please Enter the Division Name.",
                noSpace: "Division Name cannot be empty."
            },
            divisionCode: {
                required: "Please Enter the Division Code.",
                noSpace: "Division Code cannot be empty."
            }
        },
      
    });

}

divisionTableRelatedFunctions = function () {
    var divisionRowEditing = null;

    var divisionTable = $('#divisionPopupModal table#divisionsList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        //destroy: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true,
                searchable: true
            },
            {
                targets: [3],
                orderable: true,
                searchable: true
            },
            {
                targets: [4],
                orderable: true,
                searchable: true
            },
            {
                targets: [5],
                orderable: true,
                searchable: true               
            },
            {
                targets: [6],
                orderable: false,
                searchable: false
                //data: null,
                //defaultContent: '<a href="#" class="viewWorkItemBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>'
            }
        ],
        order: [[2, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Add New Division',
                className: 'btn-success addNewDivisionBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewDivision();

                    e.stopPropagation();
                }
            }
        ]

    });

    divisionTable.on('order.dt search.dt', function () {
        divisionTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    addNewDivision = function () {
        createAddDivisionPopupForm(divisionTable);
    };

    $('#divisionPopupModal').on('click', '#divisionsList a.viewDivisionBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        var divisionDataTable = getDivisionDataModalDetails(divisionTable.row(selectedRow).data()[1]);

        $("#viewDivisionPopupModal .modal-body").append(divisionDataTable);

        $('#divisionPopupModal').modal('hide');

        $('#viewDivisionPopupModal').modal('show');
    });

    $('#divisionPopupModal').on('click', '#divisionsList a.editDivisionBtn', function (e) {

        /* Get the row as a parent of the link that was clicked on */
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (divisionRowEditing !== null && divisionRowEditing != selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreDivisionRowData(divisionTable, divisionRowEditing);
            editDivisionRow(divisionTable, selectedRow);
            divisionRowEditing = selectedRow;          

            var el = $('#divisionPopupModal input#divisionName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);

        } else {
            /* No row currently being edited */
            editDivisionRow(divisionTable, selectedRow);
            divisionRowEditing = selectedRow;
          
            var el = $('#divisionPopupModal input#divisionName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);
        }
        
    });

    $('#divisionPopupModal').on('click', '#divisionsList a.updateDivisionBtn', function (e) {
        e.preventDefault();

        if ($("#divisionPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var checkExistence = checkDivisionExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a Division with this name.");
            } else if (checkExistence == false) {
                var divisionUpdated = updateDivision(divisionTable.row(selectedRow).data()[1]);

                if (divisionUpdated == 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    //$('#workItemSubHeadName').val('');
                    bootbox.alert("Division is updated successfully.", function () {
                        if (divisionRowEditing !== null && divisionRowEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            updateDivisionRow(divisionTable, divisionRowEditing);
                            divisionRowEditing = null;                            
                        }
                    });
                } else if (divisionUpdated == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Data cannot be updated.<br>Please contact your database administrator.");
                }
            }

        }

    });

    $('#divisionPopupModal').on('click', '#divisionsList a.cancelDivisionBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (divisionRowEditing !== null && divisionRowEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreDivisionRowData(divisionTable, divisionRowEditing);
            divisionRowEditing = null;            
        }

    });

    $('#divisionPopupModal').on('click', '#divisionsList a.deleteDivisionBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        //var selectedRow1 = $(this).parents('tr');

        var divisionDeleted = deleteDivision(divisionTable.row(selectedRow).data()[1]);

        if (divisionDeleted == 1) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Division is deleted successfully.", function () {
                divisionTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (divisionDeleted == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
        }

    });

}

createAddDivisionPopupForm = function (divisionTable) {
    
    var circleData = getCircles();

    $("#addDivisionPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Add Division</h2>';

    html += '<div class="panel-body">';
    html += '<form id="addDivisionPopupForm" name="addDivisionPopupForm" class="form form-horizontal" role="form" method="post">';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="circleList" id="circleListLabel" class="control-label pull-left">Select Circle</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    
    html += '<select class="form-control multiselect" name="circleList" id="circleList">';
    html += '<option value="" selected="selected">' + '--Select Circle--' + '</option>';
    for (var i = 0; i < circleData.length; ) {
        html += '<option value="' + circleData[i] + '">';
        html += circleData[i + 2] + " (" + circleData[i + 1] + ")";
        html += '</option>';
        i = i + 3;
    }

    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="divisionName" id=divisionNameLabel" class="control-label pull-left">Division Name</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="divisionName" name="divisionName" placeholder="Division Name" class="form-control text-capitalize" value="" onfocus="workItemNameFocusFunction()" onblur="workItemNameBlurFunction()" style="width:100%;" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="divisionCode" id=divisionCodeLabel" class="control-label pull-left">Division Code</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="divisionCode" name="divisionCode" placeholder="Division Code" class="form-control text-capitalize" value="" onfocus="workItemNameFocusFunction()" onblur="workItemNameBlurFunction()" style="width:100%;" />';
    html += '</div>';
    html += '</div>';

    html += '<div id="workItemSubHeadListDisplay" class="panel panel-default panel-body col-xs-12 col-sm-12 col-md-12 col-lg-12 well well-sm hidden"></div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    html += '<a href="#" id="saveNewDivisionBtn" name="saveNewDivisionBtn" type="button" class="btn btn-lg btn-success btn-block">';
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i>';
    html += '</span>';
    html += '  Save';
    html += '</a>';

    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    html += '<a href="#" id="cancelNewDivisionBtn" name="cancelNewDivisionBtn" type="button" class="btn btn-lg btn-danger btn-block">';//btn-primary
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i>';
    html += '</span>';
    html += ' Cancel';
    html += '</a>';

    html += '</div>';
    html += '</div>';

    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#addDivisionPopupModal .modal-body").append(html);

    $('#divisionPopupModal').modal('hide');
    
    $('#addDivisionPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,        
    });

    $('#addDivisionPopupModal input#divisionCode').focus();
    $('#addDivisionPopupModal input#divisionName').focus();
   
    $('#addDivisionPopupForm').validate({ // initialize plugin
       rules: {
           divisionName: {
                required: true,
                noSpace: false
           },
           divisionCode: {
               required: true,
               noSpace: false
           },
           circleList: "required"           
        }
    });

    $('#addDivisionPopupModal').on('click', 'a#saveNewDivisionBtn', function (e) {
        e.preventDefault();

        if ($("#addDivisionPopupForm").valid()) {
           
                $("#preloader").show();
                $("#status").show();

                var divisionCode = $.trim($('#addDivisionPopupModal input#divisionCode').val());
                var divisionName = $.trim($('#addDivisionPopupModal input#divisionName').val());

                var checkExistence = checkDivisionExistence(divisionCode, divisionName);

                if (checkExistence == true) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("There is already a Division with this name.");
                } else if (checkExistence == false) {

                    /* Get the row as a parent of the link that was clicked on */
                    var selectedRow = $(this).parents('tr')[0];
                    divisionSavedGuid = saveDivisionData(divisionTable, selectedRow);

                    if (jQuery.Guid.IsValid(divisionSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(divisionSavedGuid.toUpperCase())) {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');

                        var newDivisionRow = divisionTable.row.add([
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            ''                            
                        ]).draw(false);

                        var newDivisionRowNode = newDivisionRow.node();

                        $(newDivisionRowNode).css('color', 'red').animate({ color: 'black' });

                        var selectedCircleTextSplit = $("#circleList :selected").text();
                        selectedCircleTextSplit = multiSplit(selectedCircleTextSplit, [' (', ')']);

                        saveNewDivisionRow(divisionTable, newDivisionRow, divisionSavedGuid, selectedCircleTextSplit[0], selectedCircleTextSplit[1], divisionName, divisionCode);

                        bootbox.alert("Division is created successfully.", function () {
                            $('#addDivisionPopupModal').modal('hide');

                            $('#divisionPopupModal').modal('show');
                        });

                    } else {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');
                        bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                    }
                }
        }

    });

    $('#addDivisionPopupModal').on('click', 'a#cancelNewDivisionBtn', function (e) {
        e.preventDefault();

        $('#addDivisionPopupModal').modal('hide');

        $('#divisionPopupModal').modal('show');

        e.stopPropagation();
    });

}

getCircles = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetCircleNames",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
    return retValue;
}

getDivisionDataModalDetails = function (divisionGuid) {

    //$("#preloader").show();
    //$("#status").show();
    var str;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/ViewDivisionData",
        data: '{"divisionGuid":"' + divisionGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //str = str1;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return str;
}

saveNewDivisionRow = function (divisionTable, selectedRow, divisionSavedGuid) {
    var selectedRowData = divisionTable.row(selectedRow).data();

    var selectedCircleTextSplit = $("#circleList :selected").text();
    selectedCircleTextSplit = multiSplit(selectedCircleTextSplit, [' (', ')']);

    selectedCircleName = selectedCircleTextSplit[0];
    selectedCircleCode = selectedCircleTextSplit[1];

    var divisionCode = $.trim($('#addDivisionPopupModal input#divisionCode').val());
    var divisionName = $.trim($('#addDivisionPopupModal input#divisionName').val());

    selectedRowData[1] = divisionSavedGuid;
    selectedRowData[2] = selectedCircleName;
    selectedRowData[3] = selectedCircleCode;
    selectedRowData[4] = divisionCode;
    selectedRowData[5] = divisionName;
    selectedRowData[6] = '<a href="#" class="viewDivisionBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editDivisionBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteDivisionBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';
    divisionTable.row(selectedRow).data(selectedRowData);

    divisionTable.on('order.dt search.dt', function () {
        divisionTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

editDivisionRow = function (divisionTable, selectedRow) {
    var selectedRowData = divisionTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[3].innerHTML = '<input type="text" id="divisionName" name="divisionName" placeholder="Division Name" class="form-control text-capitalize" value="' + selectedRowData[4] + '" onfocus="divisionNameFocusFunction()" onblur="divisionNameBlurFunction()" style="width:100%;">';//Circle Name
    availableTds[4].innerHTML = '<input type="text" id="divisionCode" name="divisionCode" placeholder="Division Code" class="form-control text-capitalize" value="' + selectedRowData[5] + '" onfocus="divisionCodeFocusFunction()" onblur="divisionCodeBlurFunction()" style="width:100%;">';//Circle Code
    availableTds[5].innerHTML = '<a href="#" class="updateDivisionBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Update" title="Update"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelDivisionBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

updateDivisionRow = function (divisionTable, selectedRow) {
    var selectedRowData = divisionTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[4] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
    selectedRowData[5] = capitalizeFirstAllWords($.trim(availableInputs[1].value));

    divisionTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

}

restoreDivisionRowData = function (divisionTable, previousRow) {
    var previousRowData = divisionTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewDivisionRowCreatedOnCancel(divisionTable, previousRow);
    } else {
        divisionTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;

    }

}

removeNewDivisionRowCreatedOnCancel = function (divisionTable, selectedRow) {

    divisionTable
        .row(selectedRow)
        .remove()
        .draw();

    divisionTable.on('order.dt search.dt', function () {
        divisionTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

checkDivisionExistence = function (divisionCode,divisionName) {
    var retValue = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckDivisionExistence",
        data: '{"divisionCode":"' + divisionCode + '","divisionName":"' + divisionName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveDivisionData = function () {
    var divisionCode = $.trim($('#addDivisionPopupModal input#divisionCode').val());
    var divisionName = $.trim($('#addDivisionPopupModal input#divisionName').val());
    var circleGuid = $("#circleList").val();

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveDivision",
        data: '{"divisionCode":"' + divisionCode + '","divisionName":"' + divisionName + '","circleGuid":"' + circleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateDivision= function (divisionGuid) {   

    var divisionName = $.trim($('#divisionPopupModal input#divisionName').val());
    var divisionCode = $.trim($('#divisionPopupModal input#divisionCode').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateDivision",
        data: '{"divisionGuid":"' + divisionGuid + '","divisionCode":"' + divisionCode + '","divisionName":"' + divisionName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteDivision = function (divisionGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/DeleteDivision",
        data: '{"divisionGuid":"' + divisionGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}


divisionNameFocusFunction = function () {
    // Focus = Changes the background color of input to yellow
    $('#divisionPopupModal input#divisionName').css('background-color', 'yellow');
}

divisionNameBlurFunction = function () {
    // No focus = Changes the background color of input to nothing
    $('#divisionPopupModal input#divisionName').css('background-color', 'yellow');
}

divisionCodeFocusFunction = function () {
    // Focus = Changes the background color of input to yellow
    $('#divisionPopupModal input#divisionCode').css('background-color', 'yellow');
}

divisionCodeBlurFunction = function () {
    // No focus = Changes the background color of input to nothing
    $('#divisionPopupModal input#divisionCode').css('background-color', 'yellow');
}