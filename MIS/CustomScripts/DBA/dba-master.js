﻿//CHECK NULL STRINGS
stringIsNullOrEmpty = function (str) {
    return (str == null || str === "");
}

//
capitalizeFirstAllWords = function (str) {
    var pieces = str.split(" ");
    for (var i = 0; i < pieces.length; i++) {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
}

//multi spli
multiSplit = function (str, delimeters) {
    var result = [str];
    if (typeof (delimeters) == 'string')
        delimeters = [delimeters];
    while (delimeters.length > 0) {
        for (var i = 0; i < result.length; i++) {
            var tempSplit = result[i].split(delimeters[0]);
            result = result.slice(0, i).concat(tempSplit).concat(result.slice(i + 1));
        }
        delimeters.shift();
    }
    return result;
}

//FOCUS FUNCTION
dbaFocusFunction = function (id) {
    $('#' + id).css('background-color', 'yellow');
}

//BLUR FUNCTION
dbaBlurFunction = function (id) {
    $('#' + id).css('background-color', '');
}


//DIVISIONS

//Gets the Divisons list from DB.
getDivisions = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetDivisions",
        //data: '{"workItemGuid":"' + workItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

//Displays Options for Divisions
showDivisionOptions = function (id, arr) {
    var option_str = document.getElementById(id);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Division--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].DivisionName + ' (' + arr[i].DivisionCode + ')', arr[i].DivisionGuid);
    }
}

//WORKITEM
getWorkItems = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetWorkItems",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

//Displays Options for Divisions
showWorkItemOptions = function (id, arr) {
    var option_str = document.getElementById(id);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select WorkItem--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].DivisionName + ' (' + arr[i].DivisionCode + ')', arr[i].DivisionGuid);
    }
}




//DISTRICTS

//Gets the Districts List from DB.
getDistricts = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetDistricts",
        //data: '{"workItemGuid":"' + workItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        error: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

//Displays Options for Districts
showDistrictOptions = function (id, arr) {
    var option_str = document.getElementById(id);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select District--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].DistrictName + ' (' + arr[i].DistrictCode + ')', arr[i].DistrictGuid);
    }
}



//Users

//Gets the Districts List from DB.
getUsersBasedOnRoleName = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetUsersBasedOnRoleName",
        //data: '{"workItemGuid":"' + workItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        error: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

//Displays Options for Districts
showUserOptions = function (id, arr) {
    var option_str = document.getElementById(id);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select User--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].DistrictName + ' (' + arr[i].DistrictCode + ')', arr[i].DistrictGuid);
    }
}


//Finanacial SubHeads

getFinancialSubHeads = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetFinancialSubHeads",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

//Schemes

//Gets the Schemes list from DB.
getSchemes = function (divisionGuid) {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetSchemes",
        data: '{"divisionGuid":"' + divisionGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

//Displays Options for Divisions
showSchemeOptions = function (id, arr) {
    var option_str = document.getElementById(id);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Division--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].DivisionName + ' (' + arr[i].DivisionCode + ')', arr[i].DivisionGuid);
    }
}

//DESIGNATIONS

//Gets the Designations List from DB.
getDesignations = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetDesignations",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        error: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

//Displays Options for Districts
showDesignationOptions = function (id, arr) {
    var option_str = document.getElementById(id);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Designation--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].DesignationName, arr[i].DesignationGuid);
    }
}