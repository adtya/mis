﻿$(document).ready(function () {

    function disableBack() {
        window.history.forward(1);
    }

    disableBack();

    window.onload = disableBack();

    window.onpageshow = function (evt) {
        if (evt.persisted)
            disableBack();
    }

    window.onunload = function () { void (0); }

    $('#popupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            userName: {
                required: true,
                //email: true
                email1: true
            }
        },
        messages: {
            userName: {
                required: "Please Enter your Email Id.",
                //email: "Please check the Email Id entered."
                email1: "Please check the Email Id entered."
            }
        },
        tooltip_options: {
            userName: {
                trigger: "focus",
                html: true
            }
        }
    });

    $('#btnLogin').on('click', function () {
        if ($("#popupForm").valid()) {
            logMeIn();
        }
    });

    $('#btnGuestLogin').on('click', function (e) {
        e.preventDefault();

        logGuestIn();
    });
});

logMeIn = function () {
    $("#preloader").show();
    $("#status").show();
    $.ajax({
        type: "Post",
        async: false,
        url: "Login.aspx/LogMeIn",
        data: '{"userName":"' + $.trim($('#userName').val()) + '","userPassword":"' + $.trim($('#userPassword').val()) + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            var str = response.d;
            if (str != "") {
                if (str == "No User") {
                    bootbox.alert({
                        title: 'Wrong Credentials!!',
                        message: 'Username and Password do not match.<br>Please enter the correct username and password.',
                        buttons: {
                            'ok': {
                                label: 'Ok',
                                className: 'btn-default pull-right'
                            }
                        },
                        callback: function () {
                            window.location='Login.aspx';
                        }
                    });
                } else {
                    window.location = str;
                }
            }
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });
}

logGuestIn = function () {
    $("#preloader").show();
    $("#status").show();

    $.ajax({
        type: "Post",
        async: false,
        url: "Login.aspx/LogGuestIn",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            var str = response.d;
            if (str != "") {
                window.location = str;
            }
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });
}