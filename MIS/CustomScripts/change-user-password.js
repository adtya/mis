﻿$(document).ready(function () {

    var prevoiusUrl = document.referrer;

    btnsave = $("#btnChangePassword");

    btnsave.click(function () {
        var check;
        if ($(".form").valid()) {
            check = checkPassword();
            if (check == true) {
                changePassword();
            }
            else {
                bootbox.alert("Current password entered is incorrect.");
            }
        }

    });

    $('.form').validate({
        rules: {
            currPassword: {
                required: true
            },
            newPassword: { required: true },
            rnewPassword: {
                required: true,
                equalTo: "#newPassword"
            }
        },
    });

    checkPassword = function () {
        var currPass = $('#currPassword').val();
        var str;

        $("#preloader").show();
        $("#status").show();
        $.ajax({
            type: "Post",
            async: false,
            url: "ChangeUserPassword.aspx/CheckCurrentPassword",
            data: '{"currentPassword":"' + currPass + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //alert("success");
                $('#status').delay(300).fadeOut(); // will first fade out the loading animation
                $('#preloader').delay(350).fadeOut('slow');
                str = response.d;
            },
            failure: function (msg) {
                $('#status').delay(300).fadeOut(); // will first fade out the loading animation
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert(msg);
            }
        });
        return str;
    }

    changePassword = function () {
        var newPass = $('#newPassword').val();
        var currPass = $('#currPassword').val();

        $("#preloader").show();
        $("#status").show();
        $.ajax({
            type: "Post",
            async: false,
            url: "ChangeUserPassword.aspx/UpdateUserPassword",
            data: '{"newPassword":"' + newPass + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                var str = response.d;
                if (str == true) {
                    bootbox.alert("Password Changed successfully.", function () {
                        //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                        //window.location.href = nextUrl;
                        window.location = 'Login.aspx';
                    });
                }
                else {
                    bootbox.alert("Cannot change the password as some problem occured.", function () {
                        //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                        //window.location.href = nextUrl;
                        window.location = 'Login.aspx';
                    });
                }
            },
            failure: function (msg) {
                $('#status').delay(300).fadeOut(); // will first fade out the loading animation
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert(msg);
            }
        });
    }

    goBack = function () {
        bootbox.alert("Are you sure you want to leave without changing the password?", function () {
            //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
            //window.location.href = nextUrl;
            window.location = prevoiusUrl;
        });
    }
});