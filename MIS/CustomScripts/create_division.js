﻿$(document).ready(function () {

    $('#popupFormDivision').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            divisionCode: {
                required: true,
                noSpace: true
            },
            divisionName: {
                required: true,
                noSpace: true
            },
            circleList: "required",
        },
    });

    $('#createDivisionBtn').on('click', function () {
        var check;

        if ($("#popupFormDivision").valid()) {
            $("#preloader").show();
            $("#status").show();
            check = saveDivision();

            if (check == 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data is saved successfully.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'DBAHome.aspx';
                });
            }
            else if (check == 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data cannot be saved.\nPlease contact your database administrator.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'DBAHome.aspx';
                });
            }
        }

    });

});

saveDivision = function () {   

        var divisionCode = $('#divisionCode').val();
        var divisionName = $('#divisionName').val();
        var circleGuid = $("#circleList").val();       
        var ret = 0;
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/SaveDivision",
            data: '{"divisionCode":"' + divisionCode + '","divisionName":"' + divisionName + '","circleGuid":"' + circleGuid + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                ret = response.d;
            },
            failure: function (msg) {
                alert(msg);
                bootbox.alert(msg);               
            }
        });

    }