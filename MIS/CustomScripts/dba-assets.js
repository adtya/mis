﻿$(document).ready(function () {
    var selectedDivision;
    var selectedDivisionText;
    var selectedSchemeText;
    var selectedAssetTypeText;

    var settings = $.extend(true, $.fn.dataTable.defaults, {
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [3],
                visible: false,
                orderable: false,
                searchable: false
            }
        ],
        //order: [[2, 'asc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Create New Asset',
                className: 'btn-success addNewAssetBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();
                    window.location = 'CreateAsset.aspx';

                }
            }
        ]
    });

    var table = $('#assetListForDBA').DataTable();    

    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $('#divisionList').on('change', function () {
        $('#schemeSelect').empty();
        selectedDivision = $(this).val();

        if (selectedDivision === "") {
            $('#schemeSelect').append("<option value=''>" + "--Select Scheme--" + "</option>");
        } else {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "DBAAssets.aspx/DisplaySchemeNameForDivisionsForAsset",
                data: '{"divisionGuid":"' + selectedDivision + '"}',
                dataType: "json",
                success: function (data) {
                    b = data.d;
                    $('#schemeSelect').append("<option value='" + 0 + "'>" + "--Select Scheme--" + "</option>");
                    for (var i = 0; i < b.length; i++) {
                        $('#schemeSelect').append('<option value="' + b[i].SchemeGuid + '">' + b[i].SchemeCode + "-" + b[i].SchemeName + '</option>');

                    }

                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        

        selectedDivisionText = $('#divisionList option:selected').text().split("-");

        var val = $.fn.dataTable.util.escapeRegex(
                           selectedDivisionText[0]
                       );

        table.column(1).search(val ? '^' + val + '$' : '', true, false).draw();
    });

    $('#schemeSelect').on('change', function (e) {
        e.preventDefault();
        selectedSchemeText = $('#schemeSelect option:selected').text().split("-");       
        var val = $.fn.dataTable.util.escapeRegex(
                           selectedSchemeText[0]
                       );

        table.column(2).search(val ? '^' + val + '$' : '', true, false).draw();

    });

    $('#assetTypeList').on('change', function (e) {
        e.preventDefault();

        selectedAssetTypeText = $('#assetTypeList option:selected').text();

        if(selectedAssetTypeText === "--Select Asset Type--"){
            table.column(6).search(val ? '^' + '' + '$' : '', true, false).draw();
        } else {
            selectedAssetTypeTextToBeSearched = selectedAssetTypeText.split("-");

            var val = $.fn.dataTable.util.escapeRegex(
                           selectedAssetTypeTextToBeSearched[0]
                       );
            table.column(6).search(val ? '^' + val + '$' : '', true, false).draw();
        }

    });

    $('#dpDateOfInitiationYear').datepicker({
        format: "dd/mm/yyyy",
    });

    $('#dpDateOfCompletionYear').datepicker({
        format: "dd/mm/yyyy",
    });

    $(document).on('click', '#assetListForDBA a.editAssetBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        setAssetSessionParameters(table.row(selectedRow).data()[3], table.row(selectedRow).data()[6]);

        window.location = 'EditAsset.aspx';

    });

    $(document).on('click', '#assetListForDBA a.viewAssetListBtn', function (e) {      
        e.preventDefault();       
       
        var selectedRow = $(this).parents('tr')[0];

        var assetData = getAssetModalDetails(table.row(selectedRow).data()[3], table.row(selectedRow).data()[6]);       

        $("#viewAssetPopupModal .modal-body").append(assetData);
        
        $('#viewAssetPopupModal').modal('show');

    });

    $('#closeBtn').on('click', function () {
        $("#viewAssetPopupModal .modal-body").empty();
    });


});

setAssetSessionParameters = function (assetGuid, assetTypeCode) {
    
    $("#preloader").show();
    $("#status").show();

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAAssets.aspx/SetAssetSessionParameters",
        data: '{"assetGuid":"' + assetGuid + '","assetTypeCode":"' + assetTypeCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //str = str1;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    
    
}

getAssetModalDetails = function (assetGuid, assetTypeCode) {

    //$("#preloader").show();
    //$("#status").show();
    var str;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAAssets.aspx/ViewAssetData",
        data: '{"assetGuid":"' + assetGuid + '","assetTypeCode":"' + assetTypeCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //str = str1;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return str;
}