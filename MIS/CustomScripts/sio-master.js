﻿$(window).load(function () {
    $('#status').delay(300).fadeOut(); // will first fade out the loading animation
    $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $('body').delay(350).css({ 'overflow': 'visible' });

    $('.marquee').marquee({
        //speed in milliseconds of the marquee
        duration: 15000,
        //gap in pixels between the tickers
        gap: 50,
        //time in milliseconds before the marquee will start animating
        delayBeforeStart: 0,
        //'left' or 'right'
        direction: 'left',
        //true or false - should the marquee be duplicated to show an effect of continues flow
        duplicated: true,
        pauseOnHover: true
    });
});

$(document).ready(function () {

    btnlogOff = $("#logOffBtn");

    btnlogOff.click(function () {
        $("#preloader").show();
        $("#status").show();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.aspx/UserLogout",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //str1 = response.d;
                $('#status').delay(300).fadeOut(); // will first fade out the loading animation
                $('#preloader').delay(350).fadeOut('slow');
                //str = str1;
                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                //window.location.href = nextUrl;
                window.location = 'Login.aspx';
            },

            failure: function (msg) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                alert("Please contact your administrator");
            }
        });
    });
});