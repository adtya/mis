﻿$(document).ready(function () {  
   
    //$('#dpFiscalYear').datepicker({
    //    format: "dd/mm/yyyy",
    //});

    //$('#dpCurrentMonitoringDate').datepicker({
    //    format: "dd/mm/yyyy",
    //});


    $('#addSystemVariablesForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            fiscalYearList: "required",
            currentMonitoringDateList: "required"
        }
    });

    $('#saveSystemVariablesDateBtn').on('click', function () {
        if ($('#addSystemVariablesForm').valid()) {          
          
            updateSystemVariables();
            //saveSystemVariables();
        }
    });

    $('#cancelSystemVariablesDateBtn').on('click', function (e) {
        e.preventDefault();
        window.location = 'DBAHome.aspx';
    });

});

//saveSystemVariables = function () { 
//    var fiscalYear = $('#fiscalYearList').val();
//    var currentMonitoringDate = $('#currentMonitoringDateList').val();    
//    currentMonitoringDate = Date.parse(currentMonitoringDate).toString('yyyy/MM/dd');
//    var quarterNumber = $('#quarterNumber').val();
//    $.ajax({
//        type: "Post",
//        async: false,
//        url: "SystemMaintain.aspx/SaveSystemVariable",
//        data: '{"fiscalYear":"' + fiscalYear + '","currentMonitoringDate":"' + currentMonitoringDate + '","quarterNumber":"' + quarterNumber + '"}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            $('#status').delay(300).fadeOut();
//            $('#preloader').delay(350).fadeOut('slow');
//            ret = response.d;
//        },
//        failure: function (msg) {
//            alert(msg);
//            bootbox.alert(msg)
//        }
//    });
    
//}

updateSystemVariables = function () {
    $("#preloader").show();
    $("#status").show();

    var systemVariablesGuid = $('#systemVariablesGuid').val();
    var fiscalYear = $('#fiscalYearList').val();
    var currentMonitoringDate = $('#currentMonitoringDateList').val();
    currentMonitoringDate = Date.parse(currentMonitoringDate).toString('yyyy/MM/dd');
    var quarterNumber = $('#quarterNumber').val();
    var circleGuid = $('#circleCodeList').val();

    
    $.ajax({
        type: "Post",
        async: false,
        url: "SystemMaintain.aspx/UpdateSystemVariable",
        data: '{"systemVariablesGuid":"' + systemVariablesGuid + '","fiscalYear":"' + fiscalYear + '","currentMonitoringDate":"' + currentMonitoringDate + '","quarterNumber":"' + quarterNumber + '","circleGuid":"' + circleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            var ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg)
        }
    });

}