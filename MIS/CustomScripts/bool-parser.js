﻿var parseBool = function (str) {
    if (str == null)
        return false;

    if (typeof str === 'boolean') {
        if (str === true)
            return true;

        return false;
    }

    if (typeof str === 'string') {
        if (str === '' || str === 'null' || str === 'undefined')
            return false;

        str = str.replace(/^\s+|\s+$/g, '');
        if (str.toLowerCase() === 'true' || str.toLowerCase() === 't' || str.toLowerCase() === 'yes' || str.toLowerCase() === 'y')
            return true;

        str = str.replace(/,/g, '.');
        str = str.replace(/^\s*\-\s*/g, '-');
    }

    // var isNum = string.match(/^[0-9]+$/) != null;
    // var isNum = /^\d+$/.test(str);
    if (!isNaN(str))
        return (parseFloat(str) !== 0);

    return false;
}