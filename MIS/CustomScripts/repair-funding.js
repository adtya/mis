﻿var repairFundingTable = '';

$(document).ready(function () {

    $('#repairFundingBtn').on('click', function (e) {
        e.preventDefault();        
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetRepairFundingData",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createRepairFundingPopupForm(response.d);
            },
            failure: function (result) {
                bootbox.alert("Error");
            }
        });
        e.stopPropagation();
    });

    $('#repairFundingPopupModal').on('click', '.modal-footer button#repairFundingPopupModalCloseBtn', function (e) {
        e.preventDefault();

        location.reload(true);

        e.stopPropagation();
    });

});

createRepairFundingPopupForm = function (repairFundingData) {
    $("#repairFundingPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Repair Funds</h2>';

    html += '<div class="panel-body">';
    html += '<form id="repairFundingPopupForm" name="repairFundingPopupForm" role="form" class="form form-horizontal" method="post">';

    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    html += '<div class="table-responsive">';
    html += '<table id="repairFundingList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'Repair Funding Guid' + '</th>';
    html += '<th>' + 'Fiscal Year' + '</th>';
    html += '<th>' + 'Available Funds' + '</th>';
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    for (var i = 0; i < repairFundingData.length; i++) {
        html += '<tr>';
        html += '<td>' + '' + '</td>';
        html += '<td>' + repairFundingData[i].RepairFundingGuid + '</td>';
        html += '<td>' + repairFundingData[i].FiscalYear + '</td>';
        html += '<td>' + repairFundingData[i].AvailableFunds.Amount + '</td>';
        html += '<td>' + '<a href="#" class="editRepairFundingBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteRepairFundingBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';

        html += '</tr>';
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#repairFundingPopupModal .modal-body").append(html);

    $('#repairFundingPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    repairFundingTableRelatedFunctions();

    $('#repairFundingPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            fiscalYear: "required",
            availableFunds: {
                number: true,
            }
        },
        messages: {
            fiscalYear: {
                required: "Please Enter Fiscal Year.",
                noSpace: "Fiscal Year cannot be empty."
            }
        },
    });
}

repairFundingTableRelatedFunctions = function () {
    var repairFundingEditing = null;

    repairFundingTable = $('#repairFundingPopupModal table#repairFundingList').DataTable({
        responsive: true,
        paging: false,
        info: false,
        searching: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [4],
                orderable: false,
                searchable: false
            },

        ],
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',       
        buttons: [
            {
                text: '+ Add Repair Funds',
                className: 'btn-success addNewRepairFundsBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewRepairFunds();

                    e.stopPropagation();
                }
            }
        ]
    });

    repairFundingTable.on('order.dt search.dt', function () {
        repairFundingTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addNewRepairFundsBtn = repairFundingTable.button(['.addNewRepairFundsBtn']);

    addNewRepairFunds = function () {

        if (repairFundingEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreRepairFundingRowData(repairFundingTable, repairFundingEditing);
            repairFundingEditing = null;
            addNewRepairFundsBtn.enable();
        }

        var newRepairFundsRow = repairFundingTable.row.add([
            '',
            '',
            '',
            '',
            ''
        ]).draw(false);

        var newRepairFundsRowNode = newRepairFundsRow.node();

        addNewRepairFundingRow(repairFundingTable, newRepairFundsRowNode);

        repairFundingEditing = newRepairFundsRowNode;

        addNewRepairFundsBtn.disable();

        //$('#workItemSubHeadPopupModal input#workItemSubHeadName').focus();

       // $(newWorkItemSubHeadRowNode).css('color', 'red').animate({ color: 'black' });
    };

    $('#repairFundingPopupModal').on('click', '#repairFundingList a.saveNewRepairFundingBtn', function (e) {
        e.preventDefault();

        if ($("#repairFundingPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var checkExistence = checkFiscalYearExistence(repairFundingTable, selectedRow);

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already funds for this fiscal year.");
            } else if (checkExistence == false) {
                var repairFundingSavedGuid = saveRepairFundingData(repairFundingTable, selectedRow);

                if (jQuery.Guid.IsValid(repairFundingSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(repairFundingSavedGuid.toUpperCase())) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Funds are saved successfully.", function () {
                        if (repairFundingEditing !== null && repairFundingEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            saveNewRepairFundingRow(repairFundingTable, repairFundingEditing, repairFundingSavedGuid);
                            repairFundingEditing = null;
                            addNewRepairFundsBtn.enable();
                        }
                    });
                } else {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }
            }
        }

    });

    $('#repairFundingPopupModal').on('click', '#repairFundingList a.cancelNewRepairFundingEditBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (repairFundingEditing !== null && repairFundingEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            removeNewRepairFundingRowCreatedOnCancel(repairFundingTable, selectedRow);
            repairFundingEditing = null;
            addNewRepairFundsBtn.enable();
        }

    });    

    $('#repairFundingPopupModal').on('click', '#repairFundingList a.editRepairFundingBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (repairFundingEditing !== null && repairFundingEditing != selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreRepairFundingRowData(repairFundingTable, repairFundingEditing);
            editRepairFundingRow(repairFundingTable, selectedRow);
            repairFundingEditing = selectedRow;
            addNewRepairFundsBtn.enable();           

            var el = $('#repairFundingPopupModal input#availableFunds');
            var elemLen = el.val().length;
            el.focus();
           
            el.setCursorPosition(elemLen);
        } else {

            /* No row currently being edited */
            editRepairFundingRow(repairFundingTable, selectedRow);
            repairFundingEditing = selectedRow;
            addNewRepairFundsBtn.enable();
           
            var el = $('#repairFundingPopupModal input#availableFunds');
            var elemLen = el.val().length;
            el.focus();

            el.setCursorPosition(elemLen);
        }
    });

    $('#repairFundingPopupModal').on('click', '#repairFundingList a.updateRepairFundingBtn', function (e) {
        e.preventDefault();

        if ($("#repairFundingPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var repairFundingUpdated = updateRepairFundingData(repairFundingTable.row(selectedRow).data()[1]);
            if (repairFundingUpdated == 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("Data is updated successfully.", function () {
                    if (repairFundingEditing !== null && repairFundingEditing == selectedRow) {
                        /* A different row is being edited - the edit should be cancelled and this row edited */
                        updateRepairFundingRow(repairFundingTable, repairFundingEditing);
                        repairFundingEditing = null;
                        addNewRepairFundsBtn.enable();
                    }
                });
            } else if (repairFundingUpdated == 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
            }
        }
    });

    $('#repairFundingPopupModal').on('click', '#repairFundingList a.cancelRepairFundingBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (repairFundingEditing !== null && repairFundingEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreRepairFundingRowData(repairFundingTable, repairFundingEditing);
            repairFundingEditing = null;
            addNewRepairFundsBtn.enable();
        }

    });

     $('#repairFundingPopupModal').on('click', '#repairFundingList a.deleteRepairFundingBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        var repairFundingDeleted = deleteRepairFundingData(repairFundingTable.row(selectedRow).data()[1]);

        if (repairFundingDeleted == 1) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Data is deleted successfully.", function () {
                repairFundingTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (repairFundingDeleted == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Data cannot be deleted.<br>Please contact your database administrator.");
        }

    });

}

addNewRepairFundingRow = function (repairFundingTable, selectedRow) {
    var selectedRowData = repairFundingTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    repairFundingTable.column('1:visible').order('asc').draw();

    repairFundingTable.on('order.dt search.dt', function () {

        var x = repairFundingTable.order();

        if (x[0][1] == "asc") {
            var a = repairFundingTable.column(0, { search: 'applied', order: 'applied' }).nodes();
            for (i = 1; i < a.length; i++) {
                a[i].innerHTML = i;
            }
        } else {
            repairFundingTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }
        availableTds[0].innerHTML = '*';
    }).draw(false);

    var currentSystemDate = new Date();
    var currentYear = currentSystemDate.getFullYear();
    var backYear = currentSystemDate.getFullYear() - 8;  

    var html = '<select name=fiscalYearSelect class=form-control fiscalYearSelect>';
    html += '<option selected="selected" value="">'+ "--Select Fiscal Year--" + '</option>';    
    for (var numberOfFiscalYear = 1; numberOfFiscalYear <= 15; numberOfFiscalYear++)
    {
        var nextYear = backYear + 1;
        html += '<option value="' + backYear + "/" + nextYear + '">' + backYear + "/" + nextYear + '</option>';
        backYear = backYear + 1;
    }    
    html += '</select>';

    //Only 4 tds are there, but there will be 5 data. The invisible td will not be counted in tds.
    availableTds[0].innerHTML = '*';
    availableTds[1].innerHTML = html;
    availableTds[2].innerHTML = '<input type="text" id="availableFunds" name="availableFunds" placeholder="Available Funds" class="form-control text-capitalize" value="' + selectedRowData[3] + '" onfocus="availableFundsFocusFunction()" onblur="availableFundsBlurFunction()" style="width:100%;">';//avaiable Funds
    availableTds[3].innerHTML = '<a href="#" class="saveNewRepairFundingBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelNewRepairFundingEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

saveNewRepairFundingRow = function (repairFundingTable, selectedRow, repairFundingSavedGuid) {

    var selectedRowData = repairFundingTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableSelects = $('select', selectedRow)[0].value;
 
    var fiscalYear = availableSelects;
    selectedRowData[1] = repairFundingSavedGuid;
    selectedRowData[2] = fiscalYear;
    selectedRowData[3] = availableInputs[0].value;
    selectedRowData[4] = '<a href="#" class="editRepairFundingBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteRepairFundingBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    repairFundingTable.row(selectedRow).data(selectedRowData);

    repairFundingTable.on('order.dt search.dt', function () {
        repairFundingTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

removeNewRepairFundingRowCreatedOnCancel = function (repairFundingTable, selectedRow) {

    repairFundingTable
        .row(selectedRow)
        .remove()
        .draw();

    repairFundingTable.on('order.dt search.dt', function () {
        repairFundingTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

editRepairFundingRow = function (repairFundingTable, selectedRow) {
    var selectedRowData = repairFundingTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //Only 4 tds are there, but there will be 5 data. The invisible td will not be counted in tds.

    availableTds[2].innerHTML = '<input type="text" id="availableFunds" name="availableFunds" placeholder="Available Funds" class="form-control text-capitalize" value="' + selectedRowData[3] + '" onfocus="availableFundsFocusFunction()" onblur="availableFundsBlurFunction()" style="width:100%;">';//WorkItem SubHead
    availableTds[3].innerHTML = '<a href="#" class="updateRepairFundingBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelRepairFundingBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

updateRepairFundingRow = function (repairFundingTable, selectedRow) {
    var selectedRowData = repairFundingTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[3] = availableInputs[0].value;

    repairFundingTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

}

restoreRepairFundingRowData = function (repairFundingTable, previousRow) {
    var previousRowData = repairFundingTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewRepairFundingRowCreatedOnCancel(repairFundingTable, previousRow);
    } else {
        repairFundingTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;       
    }

}


availableFundsFocusFunction = function () {
    // Focus = Changes the background color of input to yellow
    //document.getElementById("workItemSubHeadName").style.background = "yellow";
    $('#repairFundingPopupModal input#availableFunds').css('background-color', 'yellow');
}

availableFundsBlurFunction = function () {
    // No focus = Changes the background color of input to nothing
    //document.getElementById("workItemSubHeadName").style.background = "";
    $('#repairFundingPopupModal input#availableFunds').css('background-color', '');
}


capitalizeFirstAllWords = function (str) {
    var pieces = str.split(" ");
    for (var i = 0; i < pieces.length; i++) {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
}

checkFiscalYearExistence = function (repairFundingTable, selectedRow) {

    var selectedRowData = repairFundingTable.row(selectedRow).data();
    var availableSelects = $('select', selectedRow)[0].value;
    var fiscalYear = availableSelects; 

    var retValue = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckFiscalYearExistence",
        data: '{"fiscalYear":"' + fiscalYear + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveRepairFundingData = function (repairFundingTable, selectedRow) {

    var availableSelects = $('select', selectedRow)[0].value;
    var fiscalYear = availableSelects;

    var availableFunds = $.trim($('#repairFundingPopupModal input#availableFunds').val());
    var newchar = '';
    availableFunds = availableFunds.split(',').join(newchar);
    availableFunds = stringIsNullOrEmpty($.trim(availableFunds)) ? parseFloat(0) : parseFloat(availableFunds);

    var repairFundingManager = {
        fiscalYear: fiscalYear,
        availableFundsInput: availableFunds
    }

    var repairFundingManagerJsonString = JSON.stringify(repairFundingManager);

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveRepairFundingData",
        data: '{"repairFundingManager":' + repairFundingManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updateRepairFundingData = function (repairFundingGuid) {

    var repairFundingManager = {
        repairFundingGuid: repairFundingGuid,
        availableFundsInput: $.trim($('#repairFundingPopupModal input#availableFunds').val())
    }

    var repairFundingManagerJsonString = JSON.stringify(repairFundingManager);  

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateRepairFundingData",
        data: '{"repairFundingManager":' + repairFundingManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteRepairFundingData = function (repairFundingGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/DeleteRepairFundingData",
        data: '{"repairFundingGuid":"' + repairFundingGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}