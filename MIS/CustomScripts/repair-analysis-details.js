﻿var repairAnalysisTable = '';
var assetCodeList = "";
var assetRiskRateNumber;
var schemeRiskRateNumber;
var probabiltyFailureList = "";
var currentAssetFunctionalityList = "";
var repairAnalysisStatusList = "";
var repairAnalysisData = "";
var assetRiskRatingNumber;
var schemeRiskRatingNumber;
$(document).ready(function (e) {
    getAssetCodeList();
    createRepairAnalysisTable(assetCodeList);

});

createRepairAnalysisTable = function (assetCodeList) {

    getRepairAnalysisData();
    getProbabilityFailureList();
    getCurrentAssetFunctionalityList();
    getRepairAnalysisStatusList();

    $("#repairAnalysisListPanel").empty();

    var html = '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<div class="form-group">';

    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="assetCodeList" id="assetCodeListLabel" class="control-label pull-left">Asset Code</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control multiselect" name="assetCodeList" id="assetCodeList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">';
    html += '<option value="" selected="selected">--Select Asset Code--</option>';

    if (repairAnalysisData.length === 0) {
        for (var i = 0; i < assetCodeList.length; i++) {
            html += '<option value="' + assetCodeList[i].AssetGuid + '">';
            html += assetCodeList[i].AssetCode;
            html += '</option>';
        }
    }
    else {
        var a = [];

        for (var i = 0; i < repairAnalysisData.length; i++) {
            a.push(repairAnalysisData[i].AssetManager.AssetGuid);
        }
        for (var i = 0; i < assetCodeList.length; i++) {

            if ($.inArray(assetCodeList[i].AssetGuid, a) === -1) {
                html += '<option value="' + assetCodeList[i].AssetGuid + '">';
                html += assetCodeList[i].AssetCode;
                html += '</option>';
            }
        }
    }
    html += '</select>';
    html += '</div>';

    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div id="table-container" style="padding:1%;">';

    html += '<div class="table-responsive">';
    html += '<table id="repairAnalysisList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'Repair Analysis Guid' + '</th>';
    html += '<th>' + 'Asset Guid' + '</th>';
    html += '<th>' + 'Asset Code' + '</th>';
    html += '<th>' + 'Asset Risk Rating' + '</th>';
    html += '<th>' + 'Scheme Risk Rating' + '</th>';
    html += '<th>' + 'Probability Failure' + '</th>';
    html += '<th>' + 'Current Asset Functionality' + '</th>';
    html += '<th>' + 'Intensity Factor' + '</th>';
    html += '<th>' + 'Total' + '</th>';
    html += '<th>' + 'Repair Cost' + '</th>';
    html += '<th>' + 'Submission Date' + '</th>';
    html += '<th>' + 'Status' + '</th>';
    html += '<th>' + 'Probability Failure Guid' + '</th>';
    html += '<th>' + 'Current Asset Functionality Guid' + '</th>';
    html += '<th>' + 'Status Guid' + '</th>';
    html += '<th>' + 'Operations' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    for (var i = 0; i < repairAnalysisData.length; i++) {
        html += '<tr>';
        html += '<td>' + '' + '</td>';
        html += '<td>' + repairAnalysisData[i].RepairAnalysisGuid + '</td>';
        html += '<td>' + repairAnalysisData[i].AssetManager.AssetGuid + '</td>';
        html += '<td>' + repairAnalysisData[i].AssetManager.AssetCode + '</td>';

        if (repairAnalysisData[i].AssetCodeAndAssetRiskRatingRelationManager.AssetRiskRatingManager.AssetRiskRatingNumber === 0)
        {
            html += '<td>' + ""+ '</td>';
        }
        else
        {
            html += '<td>' + repairAnalysisData[i].AssetCodeAndAssetRiskRatingRelationManager.AssetRiskRatingManager.AssetRiskRatingNumber + '</td>';
        }
        
        if (repairAnalysisData[i].SchemeCodeAndSchemeRiskRatingRelationManager.SchemeRiskRatingManager.SchemeRiskRatingNumber === 0) {
            html += '<td>' + "" + '</td>';
        }
        else {
            html += '<td>' + repairAnalysisData[i].SchemeCodeAndSchemeRiskRatingRelationManager.SchemeRiskRatingManager.SchemeRiskRatingNumber + '</td>';
        }      

        for (var j = 0; j < probabiltyFailureList.length; j++)
        {
            if(repairAnalysisData[i].ProbabilityFailureManager.ProbabilityFailureGuid == probabiltyFailureList[j].ProbabilityFailureGuid)
            {
                var probabilityFailureGuid = repairAnalysisData[i].ProbabilityFailureManager.ProbabilityFailureGuid;
                html += '<td>' + repairAnalysisData[i].ProbabilityFailureManager.ProbabilityFailureNumber + ' . ' + repairAnalysisData[i].ProbabilityFailureManager.ProbabilityFailure + '</td>';
                break;
            }
            else {
                
            }
        }

        for (var k = 0; k < currentAssetFunctionalityList.length; k++) {
            if (repairAnalysisData[i].CurrentAssetFunctionalityManager.CurrentAssetFunctionalityGuid == currentAssetFunctionalityList[k].CurrentAssetFunctionalityGuid) {
                var currentAssetFunctionalityGuid = repairAnalysisData[i].CurrentAssetFunctionalityManager.CurrentAssetFunctionalityGuid;
                html += '<td>' + repairAnalysisData[i].CurrentAssetFunctionalityManager.CurrentAssetFunctionalityNumber + ' . ' + repairAnalysisData[i].CurrentAssetFunctionalityManager.CurrentAssetFunctionality + '</td>';
                break;
            }
            else {
                
            }
        }

        html += '<td>' + repairAnalysisData[i].RepairIntensityFactor + '</td>';

        if (repairAnalysisData[i].Total === 0) {
            html += '<td>' + "" + '</td>';
        }
        else {
            html += '<td>' + repairAnalysisData[i].Total + '</td>';
        }

        html += '<td>' + repairAnalysisData[i].RepairCost.Amount + '</td>';

        var submissionDate = "";
        for (var submissionDateLength = 0; submissionDateLength < repairAnalysisData[i].SubmissionDate.length; submissionDateLength++) {
            if (repairAnalysisData[i].SubmissionDate.charAt(submissionDateLength) >= '0' && repairAnalysisData[i].SubmissionDate.charAt(submissionDateLength) <= '9') {
                submissionDate += repairAnalysisData[i].SubmissionDate.charAt(submissionDateLength);
            }
        }
        var modifiedSubmissionDate = new Date(parseInt(submissionDate));

        html += '<td>' + modifiedSubmissionDate.toString("dd/MM/yyyy") + '</td>';

        for (var repairAnalysisStatus = 0; repairAnalysisStatus < repairAnalysisStatusList.length; repairAnalysisStatus++) {
            if (repairAnalysisData[i].RepairAnalysisStatusManger.RepairAnalysisStatusGuid == repairAnalysisStatusList[repairAnalysisStatus].RepairAnalysisStatusGuid) {
                var repairAnalysisStatusGuid = repairAnalysisData[i].RepairAnalysisStatusManger.RepairAnalysisStatusGuid;
                html += '<td>' + repairAnalysisData[i].RepairAnalysisStatusManger.RepairAnalysisStatus + '</td>';
                break;
            }
            else {
                
            }
        }

        html += '<td>' + probabilityFailureGuid + '</td>';
        html += '<td>' + currentAssetFunctionalityGuid + '</td>';
        html += '<td>' + repairAnalysisStatusGuid + '</td>';

        html += '<td>' + '<a href="#" class="editRepairAnalysisBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteRepairAnalysisBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';

        html += '</tr>';
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';

    html += '</div>';
    //html += '</div>'; 

    $("#repairAnalysisListPanel").append(html);

    repairAnalysisTableRelatedFunctions();

    $('#assetCodeList').multiselect({
        maxHeight: 150,
        numberDisplayed: 1,
        nonSelectedText: '--Select Asset Code--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',

        onChange: function (element, checked) {

            $(element).closest('.multiselect').valid();

            if (checked === true) {
                repairAnalysisTable.button(['.addNewRepairAnalysisTableBtn']).enable();
                $(document).click();
            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });   

}

repairAnalysisTableRelatedFunctions = function () {
    var repairAnalysisEditing = null;

    repairAnalysisTable = $('#repairAnalysisListPanel table#repairAnalysisList').DataTable({
        paging: false,
        info: false,
        searching: false,        
        columnDefs: [
            {                
                targets: [0],
                orderable: false,
                searchable: false               
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
                
            },
            {
                className: 'never',//className:'hidden',
                targets: [2],
                visible: false,
                orderable: false,
                searchable: false                
            },
            {
                className: 'never',//className:'hidden',
                targets: [13],
                visible: false,
                orderable: false,
                searchable: false,
                
            },
            {
                className: 'never',//className:'hidden',
                targets: [14],
                visible: false,
                orderable: false,
                searchable: false,                
            },
            {
                className: 'never',//className:'hidden',
                targets: [15],
                visible: false,
                orderable: false,
                searchable: false,
            },
           {
                targets: [16],                
                orderable: false,
                searchable: false
            }
        ],
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        buttons: [
            {
                text: '+ Add Repair Analysis Details',
                className: 'btn-success addNewRepairAnalysisTableBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewRepairAnalysisDetails();

                    e.stopPropagation();
                },
                enabled: false
            }
        ]
    });

    repairAnalysisTable.on('order.dt search.dt', function () {
        repairAnalysisTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addNewRepairAnalysisTableBtn = repairAnalysisTable.button(['.addNewRepairAnalysisTableBtn']);

    addNewRepairAnalysisDetails = function () {

        $('#descriptionFieldset').show();

        var assetCodeSelectGuid = $('#assetCodeList').val();
        var assetCodeSelect = $('#assetCodeList option:selected').text();
        getAssetRiskRatingNumber(assetCodeSelectGuid);      

        if (assetRiskRateNumber.AssetRiskRatingManager.AssetRiskRatingNumber == 0)
        {
            assetRiskRatingNumber = null;
        }
        else
        {
            assetRiskRatingNumber = assetRiskRateNumber.AssetRiskRatingManager.AssetRiskRatingNumber;
        }

        getSchemeRiskRatingNumber(assetCodeSelectGuid);

        if (schemeRiskRateNumber.SchemeRiskRatingManager.SchemeRiskRatingNumber == 0) {
            schemeRiskRatingNumber = null;
        }
        else {
            schemeRiskRatingNumber = schemeRiskRateNumber.SchemeRiskRatingManager.SchemeRiskRatingNumber;
        }

        if (repairAnalysisEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            //restoreRepairFundingRowData(repairAnalysisTable, repairFundingEditing);
            repairAnalysisEditing = null;
            addNewRepairAnalysisTableBtn.enable();
        }

        var newRepairAnalysisDetailsRow = repairAnalysisTable.row.add([
            '',
            '',
            assetCodeSelectGuid,
            assetCodeSelect,
            assetRiskRatingNumber,
            schemeRiskRatingNumber,
           '<select id="probFailureSelect"  name="probFailureSelect" class="form-control probFailureSelect" style="width:60px"></select>',
           '<select id="currentAssetFunctionalitySelect"  name="currentAssetFunctionalitySelect" class="form-control currentAssetFunctionalitySelect" style="width:60px"></select>',
            '<input id="intensityFactor" name="intensityFactor" class="form-control intensityFactor" type="text" value= "' + 1 + '" style="width:40px"/>',
            '<input id="total" name="total" class="form-control total" type="text" readonly="readonly" style="width:40px"/>',
            '<input id="repairCost" name="repairCost" class="form-control repairCost" type="text" style="width:90px"/>',
            '<div id=\"dpSubmissionDate\" class=\"input-group date\"><input type=\"text\" name=\"submissionDate\" class=\"form-control\" id=\"submissionDate\" placeholder=\"Submission Date (dd/mm/yyyy)\" style="width:60px" /><span class=\"input-group-addon form-control-static\"><i class=\"fa fa-calendar fa-fw\"></i></span></div>',
            '<select id="repairAnalysisSelect"  name="repairAnalysisSelect" class="form-control repairAnalysisSelect" style="width:110px"></select>',
            '',
            '',
            '',
            '<a href="#" class="saveRepairAnalysisBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelRepairAnalysisBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>'
        ]).draw(false);

        getProbabilityFailureList();
        getCurrentAssetFunctionalityList();
        getRepairAnalysisStatusList();

        $('#dpSubmissionDate').datepicker({
            format: "dd/mm/yyyy",
        });

        var newRepairAnalysisDetailsRowNode = newRepairAnalysisDetailsRow.node();

        //addNewRepairFundingRow(repairAnalysisTable, newRepairAnalysisDetailsRowNode);

        repairAnalysisEditing = newRepairAnalysisDetailsRowNode;

        addNewRepairAnalysisTableBtn.disable();

        $('option[value="' + assetCodeSelectGuid + '"]', $('#assetCodeList')).remove();

        $('#assetCodeList').multiselect('rebuild');
        
    };

    $('#repairAnalysisForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            probFailureSelect: "required",
            currentAssetFunctionalitySelect: "required",
            repairAnalysisSelect: "required",
            probabiltyFailureSelectEditRow: "required",
            currentAssetFunctionalitySelectEditRow: "required",
            repairAnalysisStatusSelectEditRow: "required",
            repairCost: {
                number: true
            },
            intensityFactor: {
                number: true
            },
            dpSubmissionDate: {
                required: true,
                dateITA: true
            },
            repairCostEditRow: {
                number: true
            },
            intensityFactorEditRow: {
                number: true
            },
            dpSubmissionDateEditRow: {
                required: true,
                dateITA: true
            },
        },
    });

    $('#repairAnalysisListPanel').on('click', '#repairAnalysisList a.saveRepairAnalysisBtn', function (e) {
        e.preventDefault();

        if ($('#repairAnalysisForm').valid()) {

            var selectedRow = $(this).parents('tr')[0];

            assetCodeSelectGuid = repairAnalysisTable.row(selectedRow).data()[2];

            assetCodeSelect = repairAnalysisTable.row(selectedRow).data()[3];

            var availableInputs = $('input', selectedRow);

            var availableSelects = $('select', selectedRow);

            //intensityFactor = availableInputs[0].value;
            //total = availableInputs[1].value;
            //repairCost = availableInputs[2].value;

            $('#assetCodeList').multiselect('rebuild');

            var repairAnalysisSavedGuid = saveRepairAnalysisData(repairAnalysisTable, selectedRow);


            if (jQuery.Guid.IsValid(repairAnalysisSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(repairAnalysisSavedGuid.toUpperCase())) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Data is saved successfully.", function () {
                    if (repairAnalysisEditing !== null && repairAnalysisEditing == selectedRow) {
                        /* A different row is being edited - the edit should be cancelled and this row edited */
                        saveNewRepairAnalysisRow(repairAnalysisTable, repairAnalysisEditing, repairAnalysisSavedGuid);
                        repairAnalysisEditing = null;
                        addNewRepairAnalysisTableBtn.enable();
                    }
                });
            } else {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("Data cannot be saved.<br>Please contact your database administrator.");
            }
        }

    });

    $('#repairAnalysisListPanel').on('click', '#repairAnalysisList a.editRepairAnalysisBtn', function (e) {
        e.preventDefault();

        $('#descriptionFieldset').show();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (repairAnalysisEditing !== null && repairAnalysisEditing != selectedRow) {

            /* A different row is being edited - the edit should be cancelled and this row edited */

            restoreRepairAnalysisRowData(repairAnalysisTable, repairAnalysisEditing);
            editRepairAnalysisRow(repairAnalysisTable, selectedRow);
            repairAnalysisEditing = selectedRow;

            var el = $('#fieldSurveyCostFieldsetPanel input#intensityFactor');
            var elemLen = el.val().length;
            el.focus();


            el.setCursorPosition(elemLen);
          
        }
        else {
            /* No row currently being edited */
            editRepairAnalysisRow(repairAnalysisTable, selectedRow);
            repairAnalysisEditing = selectedRow;
           
            var el = $('#repairAnalysisListPanel input#intensityFactor');
            var elemLen = el.val().length;
            el.focus();

            el.setCursorPosition(elemLen);
        }

        
    });

    $('#repairAnalysisListPanel').on('click', '#repairAnalysisList a.updateRepairAnalysisEditRowBtn', function (e) {
        e.preventDefault();

        if ($("#repairAnalysisForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];
            var repairAnalysisUpdated = updateRepairAnalysisData(repairAnalysisTable.row(selectedRow).data()[1]);
            if (repairAnalysisUpdated == 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("Data is updated successfully.", function () {
                    if (repairAnalysisEditing !== null && repairAnalysisEditing == selectedRow) {
                        /* A different row is being edited - the edit should be cancelled and this row edited */
                        updateRepairAnalysisRow(repairAnalysisTable, repairAnalysisEditing);
                        repairAnalysisEditing = null;
                    }
                });
            } else if (repairAnalysisUpdated == 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("Data cannot be updated.<br>Please contact your database administrator.");
            }
        }
    });

    $('#repairAnalysisListPanel').on('click', '#repairAnalysisList a.cancelRepairAnalysisBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        var assetSelectGuid = repairAnalysisTable.row(selectedRow).data()[2];
        var assetSelect = repairAnalysisTable.row(selectedRow).data()[3];

        repairAnalysisTable.row(selectedRow).remove().draw();

        $('#assetCodeList')
            .append('<option value="' + assetSelectGuid + '">' + assetSelect + '</option>')
        ;

        $('#assetCodeList').multiselect('rebuild');

        addNewRepairAnalysisTableBtn.enable();

    });

    $('#repairAnalysisListPanel').on('click', '#repairAnalysisList a.cancelRepairAnalysisEditRowBtn', function (e) {
        e.preventDefault();

        $('#descriptionFieldset').hide();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (repairAnalysisEditing !== null && repairAnalysisEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreRepairAnalysisRowData(repairAnalysisTable, repairAnalysisEditing);
            repairAnalysisEditing = null;
            addNewRepairAnalysisTableBtn.enable();
        }

    });

    $('#repairAnalysisListPanel').on('click', '#repairAnalysisList a.deleteRepairAnalysisBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        var repairAnalysisGuid = repairAnalysisTable.row(selectedRow).data()[1];

        var assetCodeGuid = repairAnalysisTable.row(selectedRow).data()[2];

        var assetCode = repairAnalysisTable.row(selectedRow).data()[3];

        deleteRepairAnalysisBtn(repairAnalysisGuid);

        repairAnalysisTable.row(selectedRow).remove().draw();

        $('#intensityFactorJustificationForRoutineAnalysis').val("");
        $('#failureModeDescription').val("");
        $('#repairDescription').val("");
        $('#damageDescription').val("");

        $('#assetCodeList')
            .append('<option value="' + assetCodeGuid + '">' + assetCode + '</option>');

        $('#assetCodeList').multiselect('rebuild');

    });

    $('#repairAnalysisListPanel table#repairAnalysisList').on('click', 'input.total', function (e) {
        var selectedRow = $(this).parents('tr')[0];

        var availableSelects = $('select', selectedRow);

        var availableInputs = $('input', selectedRow);

        var selectedProbFailureTextSplit = $("#probFailureSelect :selected").text();
        selectedProbFailureTextSplit = selectedProbFailureTextSplit.split(".");

        var selectedCurrentAssetFunctionalityTextSplit = $("#currentAssetFunctionalitySelect :selected").text();
        selectedCurrentAssetFunctionalityTextSplit = selectedCurrentAssetFunctionalityTextSplit.split(".");

        var intensityFactor = $("#intensityFactor").val();

        if (assetRiskRatingNumber == null)
        {
            $('#total').val();
        }
        else if(schemeRiskRatingNumber ==  null)
        {
            $('#total').val();
        }
        else if($("#probFailureSelect :selected").text() == "---Select Probablity Failure---")
        {
            $('#total').val();
        }
        else if($("#currentAssetFunctionalitySelect :selected").text() == "---Select Current Asset Functionality---")
        {
            $('#total').val();
        }
        else {  

            total = assetRiskRatingNumber * schemeRiskRatingNumber * selectedProbFailureTextSplit[0] * selectedCurrentAssetFunctionalityTextSplit[0] * intensityFactor;

            $('#total').val(total);
        }            
        
    });

    $('#repairAnalysisListPanel table#repairAnalysisList').on('click', 'input.totalEditRow', function (e) {
        var selectedRow = $(this).parents('tr')[0];

        var availableSelects = $('select', selectedRow);

        var availableInputs = $('input', selectedRow);

        var selectedProbFailureTextSplit = $("#probabiltyFailureSelectEditRow :selected").text();
        selectedProbFailureTextSplit = selectedProbFailureTextSplit.split(".");

        var selectedCurrentAssetFunctionalityTextSplit = $("#currentAssetFunctionalitySelectEditRow :selected").text();
        selectedCurrentAssetFunctionalityTextSplit = selectedCurrentAssetFunctionalityTextSplit.split(".");

        var intensityFactor = $("#intensityFactorEditRow").val();

        if (assetRiskRatingNumber == null) {
            $('#totalEditRow').val();
        }
        else if (schemeRiskRatingNumber == null) {
            $('#totalEditRow').val();
        }
        else if ($("#probabiltyFailureSelectEditRow :selected").text() == "---Select Probablity Failure---") {
            $('#totalEditRow').val();
        }
        else if ($("#currentAssetFunctionalitySelectEditRow :selected").text() == "---Select Current Asset Functionality---") {
            $('#totalEditRow').val();
        }
        else {

            total = assetRiskRatingNumber * schemeRiskRatingNumber * selectedProbFailureTextSplit[0] * selectedCurrentAssetFunctionalityTextSplit[0] * intensityFactor;

            $('#totalEditRow').val(total);
        }

    });

}

editRepairAnalysisRow = function (repairAnalysisTable, selectedRow) {
    var selectedRowData = repairAnalysisTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    var probFailureGuid = repairAnalysisTable.row(selectedRow).data()[13];
    var currentAssetFunctionalityGuid = repairAnalysisTable.row(selectedRow).data()[14];
    var repairAnalysisStatusGuid = repairAnalysisTable.row(selectedRow).data()[15];
   
    var htmlProbablityFailure = '<select id=probabiltyFailureSelectEditRow name=probabiltyFailureSelectEditRow class=form-control probabiltyFailureSelectEditRow style="width:60px">';

    for (var i = 0; i < probabiltyFailureList.length; i++) {
        if (probabiltyFailureList[i].ProbabilityFailureGuid == probFailureGuid) {
            htmlProbablityFailure += '<option selected="selected" value="' + probabiltyFailureList[i].ProbabilityFailureGuid + '">' + probabiltyFailureList[i].ProbabilityFailureNumber + '. ' + probabiltyFailureList[i].ProbabilityFailure + '</option>';
        }
        else {
            htmlProbablityFailure += '<option value="' + probabiltyFailureList[i].ProbabilityFailureGuid + '">' + probabiltyFailureList[i].ProbabilityFailureNumber + '. ' + probabiltyFailureList[i].ProbabilityFailure + '</option>';
        }

    }

    htmlProbablityFailure += '</select>';

    var htmlCurrentAssetFunctionality = '<select id=currentAssetFunctionalitySelectEditRow name=currentAssetFunctionalitySelectEditRow class=form-control currentAssetFunctionalitySelectEditRow style="width:60px">';

    for (var i = 0; i < currentAssetFunctionalityList.length; i++) {

        if (currentAssetFunctionalityList[i].CurrentAssetFunctionalityGuid == currentAssetFunctionalityGuid) {
            htmlCurrentAssetFunctionality += '<option selected="selected" value="' + currentAssetFunctionalityList[i].CurrentAssetFunctionalityGuid + '">' + currentAssetFunctionalityList[i].CurrentAssetFunctionalityNumber + '. ' + currentAssetFunctionalityList[i].CurrentAssetFunctionality + '</option>';
        }
        else {
            htmlCurrentAssetFunctionality += '<option value="' + currentAssetFunctionalityList[i].CurrentAssetFunctionalityGuid + '">' + currentAssetFunctionalityList[i].CurrentAssetFunctionalityNumber + '. ' + currentAssetFunctionalityList[i].CurrentAssetFunctionality + '</option>';
        }

    }

    htmlCurrentAssetFunctionality += '</select>';

    var htmlRepairAnalysisStatus = '<select id=repairAnalysisStatusSelectEditRow name=repairAnalysisStatusSelectEditRow class=form-control repairAnalysisStatusSelectEditRow style="width:110px">';

    for (var i = 0; i < repairAnalysisStatusList.length; i++) {

        if (repairAnalysisStatusList[i].RepairAnalysisStatusGuid == repairAnalysisStatusGuid) {
            htmlRepairAnalysisStatus += '<option selected="selected" value="' + repairAnalysisStatusList[i].RepairAnalysisStatusGuid + '">' + repairAnalysisStatusList[i].RepairAnalysisStatus + '</option>';
        }
        else {
            htmlRepairAnalysisStatus += '<option value="' + repairAnalysisStatusList[i].RepairAnalysisStatusGuid + '">' + repairAnalysisStatusList[i].RepairAnalysisStatus + '</option>';
        }

    }

    htmlRepairAnalysisStatus += '</select>';

    availableTds[4].innerHTML = htmlProbablityFailure;
    availableTds[5].innerHTML = htmlCurrentAssetFunctionality;
    availableTds[6].innerHTML = '<input id="intensityFactorEditRow" name="intensityFactorEditRow" class="form-control intensityFactorEditRow" style="width:40px" type="text" value= "' + repairAnalysisTable.row(selectedRow).data()[8] + '"/>',
    availableTds[7].innerHTML = '<input id="totalEditRow" name="totalEditRow" class="form-control totalEditRow" style="width:40px" type="text" readonly="readonly" value= "' + repairAnalysisTable.row(selectedRow).data()[9] + '"/>',
    availableTds[8].innerHTML = '<input id="repairCostEditRow" name="repairCostEditRow" class="form-control repairCostEditRow" style="width:90px" type="text" value= "' + repairAnalysisTable.row(selectedRow).data()[10] + '"/>',
    availableTds[9].innerHTML = '<div id=\"dpSubmissionDateEditRow\" class=\"input-group date\"><input type=\"text\" name=\"submissionDateEditRow\" style="width:60px" class=\"form-control\" id=\"submissionDateEditRow\" placeholder=\"Submission Date (dd/mm/yyyy)\" value= "' + repairAnalysisTable.row(selectedRow).data()[11].toString("dd/MM/yyyy") + '" /><span class=\"input-group-addon form-control-static\"><i class=\"fa fa-calendar fa-fw\"></i></span></div>',
    availableTds[10].innerHTML = htmlRepairAnalysisStatus;
    availableTds[11].innerHTML = '<a href="#" class="updateRepairAnalysisEditRowBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Update" title="Update"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelRepairAnalysisEditRowBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';

    getRepairAnalysisData();

    for (var i = 0; i < repairAnalysisData.length; i++) {
        if(repairAnalysisData[i].RepairAnalysisGuid == repairAnalysisTable.row(selectedRow).data()[1])
        {
            var intensityFactorJustification = $('#intensityFactorJustificationForRoutineAnalysis').val(repairAnalysisData[i].IntensityFactorJustification);
            var failureModeDescription = $('#failureModeDescription').val(repairAnalysisData[i].FailureModeDescription);
            var repairDescription = $('#repairDescription').val(repairAnalysisData[i].RepairDescription);
            var damageDescription = $('#damageDescription').val(repairAnalysisData[i].DamageDescription);
        }        
    }

    $('#dpSubmissionDateEditRow').datepicker({
        format: "dd/mm/yyyy",
    });
    
}

restoreRepairAnalysisRowData = function (repairAnalysisTable, previousRow) {
    var previousRowData = repairAnalysisTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        //removeNewWorkItemRateRowCreatedOnCancel(routineWorkItemRateTable, previousRow);
    } else {
        repairAnalysisTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;
    }

}

saveNewRepairAnalysisRow = function (repairAnalysisTable, selectedRow, repairAnalysisSavedGuid) {

    var selectedRowData = repairAnalysisTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);
    var availableSelects = $('select', selectedRow);  

    selectedRowData[1] = repairAnalysisSavedGuid;
    selectedRowData[6] = availableSelects[0].options[availableSelects[0].selectedIndex].text;
    selectedRowData[7] = availableSelects[1].options[availableSelects[1].selectedIndex].text;
    selectedRowData[8] = availableInputs[0].value;
    selectedRowData[9] = availableInputs[1].value;
    selectedRowData[10] = availableInputs[2].value;
    selectedRowData[11] = availableInputs[3].value;
    selectedRowData[12] = availableSelects[2].options[availableSelects[2].selectedIndex].text;
    selectedRowData[13] = availableSelects[0].value;
    selectedRowData[14] = availableSelects[1].value;
    selectedRowData[15] = availableSelects[2].value;
    selectedRowData[16] = '<a href="#" class="editRepairAnalysisBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteRepairAnalysisBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    repairAnalysisTable.row(selectedRow).data(selectedRowData);

    repairAnalysisTable.on('order.dt search.dt', function () {
        repairAnalysisTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

updateRepairAnalysisRow = function (repairAnalysisTable, selectedRow) {
    var selectedRowData = repairAnalysisTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);
    var availableSelects = $('select', selectedRow);

    selectedRowData[1] = repairAnalysisTable.row(selectedRow).data()[1]
    selectedRowData[6] = availableSelects[0].options[availableSelects[0].selectedIndex].text;
    selectedRowData[7] = availableSelects[1].options[availableSelects[1].selectedIndex].text;
    selectedRowData[8] = availableInputs[0].value;
    selectedRowData[9] = availableInputs[1].value;
    selectedRowData[10] = availableInputs[2].value;
    selectedRowData[11] = availableInputs[3].value;
    selectedRowData[12] = availableSelects[2].options[availableSelects[2].selectedIndex].text;
    selectedRowData[13] = availableSelects[0].value;
    selectedRowData[14] = availableSelects[1].value;
    selectedRowData[15] = availableSelects[2].value;
    selectedRowData[16] = '<a href="#" class="editRepairAnalysisBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteRepairAnalysisBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    repairAnalysisTable.row(selectedRow).data(selectedRowData);

    repairAnalysisTable.on('order.dt search.dt', function () {
        repairAnalysisTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

getRepairAnalysisData = function () {

   $.ajax({
        type: "POST",
        async: false,
        url: "RepairAnalysisDetails.aspx/GetRepairAnalysisData",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            repairAnalysisData = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });

}

getAssetCodeList = function () {   
    $.ajax({
        type: "Post",
        async: false,
        url: "RepairAnalysisDetails.aspx/DisplayAssetCodeList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            assetCodeList = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
}

getAssetRiskRatingNumber = function (assetCodeSelectGuid) {
   
    $.ajax({
        type: "POST",
        async: false,
        url: "RepairAnalysisDetails.aspx/GetAssetRiskRatingNumber",
        data: '{"assetGuid":"' + assetCodeSelectGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            assetRiskRateNumber = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getSchemeRiskRatingNumber = function (assetCodeSelectGuid) {

    $.ajax({
        type: "POST",
        async: false,
        url: "RepairAnalysisDetails.aspx/GetSchemeRiskRatingNumber",
        data: '{"assetGuid":"' + assetCodeSelectGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            schemeRiskRateNumber = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getProbabilityFailureList = function () {
    $.ajax({
        type: "Post",
        async: false,
        url: "RepairAnalysisDetails.aspx/GetProbabilityFailure",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            probabiltyFailureList = response.d;
            showProbalityFailure('probFailureSelect', probabiltyFailureList);
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
}

getCurrentAssetFunctionalityList = function () {
    $.ajax({
        type: "Post",
        async: false,
        url: "RepairAnalysisDetails.aspx/GetCurrentAssetFunctionality",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            currentAssetFunctionalityList = response.d;
            showCurrentAssetFunctionality('currentAssetFunctionalitySelect', currentAssetFunctionalityList);
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
}

getRepairAnalysisStatusList = function () {
    $.ajax({
        type: "Post",
        async: false,
        url: "RepairAnalysisDetails.aspx/GetRepairAnalysisStatus",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            repairAnalysisStatusList = response.d;
            showRepairAnalysisStatus('repairAnalysisSelect',repairAnalysisStatusList)
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
}

showProbalityFailure = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('---Select Probablity Failure---', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].ProbabilityFailureNumber + '.' + arr[i].ProbabilityFailure, arr[i].ProbabilityFailureGuid);  
    }
}

showCurrentAssetFunctionality = function (selectId, arr) {
    var option_str_gate = document.getElementById(selectId);
    option_str_gate.length = 0;
    option_str_gate.options[0] = new Option('---Select Current Asset Functionality---', '');
    option_str_gate.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_gate.options[option_str_gate.length] = new Option(arr[i].CurrentAssetFunctionalityNumber + '.' + arr[i].CurrentAssetFunctionality, arr[i].CurrentAssetFunctionalityGuid);
    }
}

showRepairAnalysisStatus = function (selectId, arr) {
    var option_str_status = document.getElementById(selectId);
    option_str_status.length = 0;
    option_str_status.options[0] = new Option('--Select Status--', '');
    option_str_status.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_status.options[option_str_status.length] = new Option(arr[i].RepairAnalysisStatus, arr[i].RepairAnalysisStatusGuid);        //i += 3;
    }
}

saveRepairAnalysisData = function (repairAnalysisTable, selectedRow) {

    var assetManager = {
        assetGuid: repairAnalysisTable.row(selectedRow).data()[2]
    }

    var probabilityFailureManager = {
        probabilityFailureGuid: $('#probFailureSelect').val()
    }

    var currentAssetFunctionalityManager = {
        currentAssetFunctionalityGuid: $('#currentAssetFunctionalitySelect').val()
    }

    var repairAnalysisStatusManger = {
        repairAnalysisStatusGuid: $('#repairAnalysisSelect').val()
    }

   var systemVariablesManager = {
        fiscalYear: $('#fiscalYearValue').text()    

   }
    
   var intensityFactorJustification = $('#intensityFactorJustificationForRoutineAnalysis').val();
   var failureModeDescription = $('#failureModeDescription').val();
   var repairDescription = $('#repairDescription').val();
   var damageDescription = $('#damageDescription').val();

    var availableInputs = $('input', selectedRow);   

    var repairCost = $("#repairCost").val();
    var newchar = '';
    repairCost = repairCost.split(',').join(newchar);
    repairCost = stringIsNullOrEmpty($.trim(repairCost)) ? parseFloat(0) : parseFloat(repairCost);
   
    var submissionDate = $('#submissionDate').val();
    submissionDate = Date.parseExact(submissionDate, 'dd/MM/yyyy').toString('yyyy/MM/dd')

    var repairAnalysisManager = {
        assetManager: assetManager,
        probabilityFailureManager: probabilityFailureManager,        
        currentAssetFunctionalityManager: currentAssetFunctionalityManager,
        repairIntensityFactor: stringIsNullOrEmpty($.trim($('#intensityFactor').val())) ? 0 : parseInt($.trim($('#intensityFactor').val())),
        total: stringIsNullOrEmpty($.trim($('#total').val())) ? 0 : parseInt($.trim($('#total').val())),
        repairCostInput: repairCost,
        submissionDate: submissionDate,
        repairAnalysisStatusManger: repairAnalysisStatusManger,
        intensityFactorJustification: intensityFactorJustification,
        failureModeDescription: failureModeDescription,
        repairDescription: repairDescription,
        damageDescription: damageDescription,
        systemVariablesManager: systemVariablesManager,
    }

    var repairAnalysisManagerJsonString = JSON.stringify(repairAnalysisManager);

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "RepairAnalysisDetails.aspx/SaveRepairAnalysisData",
        data: '{"repairAnalysisManager":' + repairAnalysisManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateRepairAnalysisData = function (repairAnalysisGuid) {

    var probabilityFailureManager = {
        probabilityFailureGuid: $('#probabiltyFailureSelectEditRow').val()
    }

    var currentAssetFunctionalityManager = {
        currentAssetFunctionalityGuid: $('#currentAssetFunctionalitySelectEditRow').val()
    }

    var repairAnalysisStatusManger = {
        repairAnalysisStatusGuid: $('#repairAnalysisStatusSelectEditRow').val()
    }

    var intensityFactorJustification = $('#intensityFactorJustificationForRoutineAnalysis').val();
    var failureModeDescription = $('#failureModeDescription').val();
    var repairDescription = $('#repairDescription').val();
    var damageDescription = $('#damageDescription').val();

    //var availableInputs = $('input', selectedRow);

    var repairCost = $("#repairCostEditRow").val();
    var newchar = '';
    repairCost = repairCost.split(',').join(newchar);
    repairCost = stringIsNullOrEmpty($.trim(repairCost)) ? parseFloat(0) : parseFloat(repairCost);

    var submissionDate = $('#submissionDateEditRow').val();
    submissionDate = Date.parseExact(submissionDate, 'dd/MM/yyyy').toString('yyyy/MM/dd')

    var repairAnalysisManager = {
        probabilityFailureManager: probabilityFailureManager,
        currentAssetFunctionalityManager: currentAssetFunctionalityManager,
        repairIntensityFactor: stringIsNullOrEmpty($.trim($('#intensityFactorEditRow').val())) ? 0 : parseInt($.trim($('#intensityFactorEditRow').val())),
        total: stringIsNullOrEmpty($.trim($('#totalEditRow').val())) ? 0 : parseInt($.trim($('#totalEditRow').val())),
        repairCostInput: repairCost,
        submissionDate: submissionDate,
        repairAnalysisStatusManger: repairAnalysisStatusManger,
        intensityFactorJustification: intensityFactorJustification,
        failureModeDescription: failureModeDescription,
        repairDescription: repairDescription,
        damageDescription: damageDescription
    }

    var repairAnalysisManagerJsonString = JSON.stringify(repairAnalysisManager);

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "RepairAnalysisDetails.aspx/UpdateRepairAnalysisData",
        data: '{"repairAnalysisGuid":"' + repairAnalysisGuid + '","repairAnalysisManager":' + repairAnalysisManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
            $('#descriptionFieldset').hide();
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteRepairAnalysisBtn = function (repairAnalysisGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "RepairAnalysisDetails.aspx/DeleteRepairAnalysisData",
        data: '{"repairAnalysisGuid":"' + repairAnalysisGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}