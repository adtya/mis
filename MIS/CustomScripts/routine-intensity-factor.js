﻿$(document).ready(function () {

    $('#routineIntensityFactorBtn').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetRoutineIntensityFactorData",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createRoutineIntensityFactorForm(response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });

        e.stopPropagation();
    });

});

createRoutineIntensityFactorForm = function (routineIntensityFactorData) {
    $('#routineIntensityFactorPopupModal .modal-body').empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/view-data.png" class="login" height="70" />';
    html += '<h2 class="text-center">Routine Intensity Factor</h2>';

    html += '<div class="panel-body">';
    html += '<form id="routineIntensityFactorPopupForm" name="routineIntensityFactorPopupForm" role="form" class="form form-horizontal" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    if (routineIntensityFactorData.length > 0) {
        html += '<div class="table-responsive">';
        html += '<table id="routineIntensityFactorDataList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

        html += '<thead>';
        html += '<tr>';
        html += '<th>' + '#' + '</th>';       
        html += '<th>' + 'Routine Intensity Factor Guid' + '</th>';
        html += '<th>' + 'Asset Code' + '</th>';
        html += '<th>' + 'Asset Name' + '</th>';
        html += '<th>' + 'Work Item Code' + '</th>';
        html += '<th>' + 'Work Item Description' + '</th>';
        html += '<th>' + 'Unit' + '</th>';
        html += '<th>' + 'Intensity Factor' + '</th>';
        html += '<th>' + 'Intensity Factor Justification' + '</th>';
        html += '<th>' + 'Operation' + '</th>';
        html += '</tr>';
        html += '</thead>';

        html += '<tbody>';

        for (var i = 0; i < routineIntensityFactorData.length; i++) {
            html += '<tr>';

            html += '<td>' + '' + '</td>';           
            html += '<td id="routineIntensityFactorGuid">' + routineIntensityFactorData[i].RoutineIntensityFactorGuid + '</td>';
            
            html += '<td>' + routineIntensityFactorData[i].AssetManager.AssetCode + '</td>';
            html += '<td>' + routineIntensityFactorData[i].AssetManager.AssetName + '</td>';
            html += '<td>' + routineIntensityFactorData[i].RoutineWorkItemManager.RoutineWorkItemTypeCode + '</td>';
            html += '<td>' + routineIntensityFactorData[i].RoutineWorkItemManager.ShortDescription + '</td>';
            html += '<td>' + routineIntensityFactorData[i].RoutineWorkItemManager.AssetQuantityParameterManager.UnitManager.UnitName + '</td>';
            html += '<td>' + routineIntensityFactorData[i].RoutineIntensityFactor + '</td>';
            html += '<td>' + routineIntensityFactorData[i].RoutineIntensityFactorJustification + '</td>';            
            html += '<td>' + '<a href="#" class="editRoutineIntensityFactorRowBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '</td>';
            html += '</tr>';
        }

        html += '</tbody>';

        html += '</table>';
        html += '</div>';
    } else {
        html += '<p>There is no data to be displayed.</p>';
    }

    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $('#routineIntensityFactorPopupModal .modal-body').append(html);

    $('#routineIntensityFactorPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });
    
    routineIntensityFactorTableRelatedFunctions();

    $('#routineIntensityFactorPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            routineIntensityFactor: {
                required: true
            }
        },
        messages: {
            routineIntensityFactor: {
                required: "Please Enter Routine Intensity Factor."                
            }
        },
      
    });

 }

routineIntensityFactorTableRelatedFunctions = function () {
    var routineIntensityFactorRowEditing = null;

    var routineIntensityFactorTable = $('#routineIntensityFactorDataList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },            
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                searchable: false               
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false,
                searchable: false
           }
        ]
    });

    routineIntensityFactorTable.on('order.dt search.dt', function () {
        routineIntensityFactorTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
   }).draw();

   $('#routineIntensityFactorPopupModal').on('click', '#routineIntensityFactorDataList a.editRoutineIntensityFactorRowBtn', function (e) {
       e.preventDefault();

       /* Get the row as a parent of the link that was clicked on */
       var selectedRow = $(this).parents('tr')[0];     

       if (routineIntensityFactorRowEditing !== null && routineIntensityFactorRowEditing != selectedRow) {

           /* A different row is being edited - the edit should be cancelled and this row edited */

           restoreRoutineIntensityFactorRowData(routineIntensityFactorTable, routineIntensityFactorRowEditing);
           editRoutineIntensityFactorRow(routineIntensityFactorTable, selectedRow);
           routineIntensityFactorRowEditing = selectedRow;
           
           var el = $('#routineIntensityFactorPopupModal input#routineIntensityFactor');
           var elemLen = el.val().length;
           el.focus();

          
           el.setCursorPosition(elemLen);
       }
       else {
           /* No row currently being edited */
           editRoutineIntensityFactorRow(routineIntensityFactorTable, selectedRow);
           routineIntensityFactorRowEditing = selectedRow;             

           var el = $('#routineIntensityFactorPopupModal input#routineIntensityFactor');
           var elemLen = el.val().length;
           el.focus();
                     
           el.setCursorPosition(elemLen);
       }
   });

   $('#routineIntensityFactorPopupModal').on('click', '#routineIntensityFactorDataList a.updateRoutineIntensityFactorRowBtn', function (e) {
       e.preventDefault();

       if ($("#routineIntensityFactorPopupForm").valid()) {

           $("#preloader").show();
           $("#status").show();

           /* Get the row as a parent of the link that was clicked on */
           var selectedRow = $(this).parents('tr')[0];

           var routineIntensityFactorUpdated = updateRoutineIntensityFactorData(routineIntensityFactorTable.row(selectedRow).data()[1]);

           if (routineIntensityFactorUpdated == 1) {

               $('#status').delay(300).fadeOut();
               $('#preloader').delay(350).fadeOut('slow');
           
               bootbox.alert("Your Data is updated successfully.", function () {
                   if (routineIntensityFactorRowEditing !== null && routineIntensityFactorRowEditing == selectedRow) {

                       /* A different row is being edited - the edit should be cancelled and this row edited */

                       updateRoutineIntensityFactorRow(routineIntensityFactorTable, routineIntensityFactorRowEditing);
                       routineIntensityFactorRowEditing = null;                      
                   }
               });
           } else if (routineIntensityFactorUpdated == 0) {
               $('#status').delay(300).fadeOut();
               $('#preloader').delay(350).fadeOut('slow');
               bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
           }
       }    

   });

   $('#routineIntensityFactorPopupModal').on('click', '#routineIntensityFactorDataList a.cancelRoutineIntensityFactorRowBtn', function (e) {
       e.preventDefault();

       /* Get the row as a parent of the link that was clicked on */
       var selectedRow = $(this).parents('tr')[0];

       if (routineIntensityFactorRowEditing !== null && routineIntensityFactorRowEditing == selectedRow) {
           /* A different row is being edited - the edit should be cancelled and this row edited */

           restoreRoutineIntensityFactorRowData(routineIntensityFactorTable, routineIntensityFactorRowEditing);
           routineIntensityFactorRowEditing = null;          
       }

   });

}

editRoutineIntensityFactorRow = function (routineIntensityFactorTable, selectedRow) {
    var selectedRowData = routineIntensityFactorTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[6].innerHTML = '<input type="text" id="routineIntensityFactor" name="routineIntensityFactor" placeholder="Routine Intensity Factor" class="form-control text-capitalize" value="' + selectedRowData[7] + '">';
    availableTds[7].innerHTML = '<input type="text" id="routineIntensityFactorJustification" name="routineIntensityFactorJustification" placeholder="Routine Intensity Factor Justification" class="form-control text-capitalize" value="' + selectedRowData[8] + '">';
    availableTds[8].innerHTML = '<a href="#" class="updateRoutineIntensityFactorRowBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelRoutineIntensityFactorRowBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

updateRoutineIntensityFactorRow = function (routineIntensityFactorTable, selectedRow) {
    var selectedRowData = routineIntensityFactorTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[7] = capitalizeFirstAllWords($.trim(availableInputs[0].value));

    selectedRowData[8] = capitalizeFirstAllWords($.trim(availableInputs[1].value));

    routineIntensityFactorTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

}

restoreRoutineIntensityFactorRowData = function (routineIntensityFactorTable, previousRow) {
    var previousRowData = routineIntensityFactorTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewRoutineIntensityFactorRowCreatedOnCancel(routineIntensityFactorTable, previousRow);
    } else {
        routineIntensityFactorTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;

        //workItemSubHeadTable.cell(previousRowIndex, 0).innerHTML = previousRowSerialNum;

        //workItemSubHeadTable.draw();
    }

}

updateRoutineIntensityFactorData = function (routineIntensityFactorGuid) {
    var routineIntensityFactor = $.trim($('#routineIntensityFactorPopupModal input#routineIntensityFactor').val());

    var routineIntensityFactorJustification = $.trim($('#routineIntensityFactorPopupModal input#routineIntensityFactorJustification').val());

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateRoutineIntensityFactorData",
        data: '{"routineIntensityFactorGuid":"' + routineIntensityFactorGuid + '","routineIntensityFactor":"' + routineIntensityFactor + '","routineIntensityFactorJustification":"' + routineIntensityFactorJustification + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {          
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}