﻿$(document).ready(function () {

    var table = $('#dbaProjectsList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                searchable: false
            }
        ],
        order: [[2, 'asc']]//,
        //select: {
        //    style: 'single',
        //    blurable: true
        //}
    });

    //table1 = $('#registeredUserList').DataTable({
    //    retrieve: true,
    //    searchHighlight: true,
    //    paging: false
    //});

    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    table.on('select', function (e, dt, type, indexes) {
        var rows = table.rows({ selected: true }).count();
        if (rows > 0) {
            $('#deleteUserBtn').removeClass('disabled');
        }
        //var rowData = table.rows(indexes).data().toArray();
        //userGuidToBeDeleted.push(rowData[0][2]);
        //if (userGuidToBeDeleted.length > 0) {
        // $('#deleteUserBtn').removeClass('disabled');
        //}
    });

    table.on('deselect', function (e, dt, type, indexes) {
        var rows = table.rows({ selected: true }).count();
        if (rows > 0) {
            $('#deleteUserBtn').removeClass('disabled');
        } else {
            $('#deleteUserBtn').addClass('disabled');
        }
        //var rowData = table.rows(indexes).data().toArray();
        //var index = userGuidToBeDeleted.indexOf(rowData[0][2]);
        //if (index > -1) {
        //    userGuidToBeDeleted.splice(index, 1);
        //}

        //if (userGuidToBeDeleted.length > 0) {
        //    $('#deleteUserBtn').removeClass('disabled');
        //} else {
        //    $('#deleteUserBtn').addClass('disabled');
        //}
    });

    //$('#registeredUserList tbody').on('click', 'tr', function () {
    //    if ($(this).hasClass('selected')) {
    //        //$(this).removeClass('selected');
    //    }
    //    else {
    //        table.$('tr.selected').removeClass('selected');
    //        $(this).addClass('selected');
    //    }

    //    rowIndex = table1.row(this).index();

    //    fid0 = table.fnGetData(this, 0);//for user_id
    //    var fid1 = table.fnGetData(this, 1);//user name
    //    userName = fid1;
    //    var fid2 = table.fnGetData(this, 2);//designation
    //    var fid3 = table.fnGetData(this, 3);//divison
    //    var fid4 = table.fnGetData(this, 4);//department
    //    var fid5 = table.fnGetData(this, 5);//emailId
    //    var fid6 = table.fnGetData(this, 6);//mobile
    //    var fid7 = table.fnGetData(this, 7);//superUser
    //    var fid8 = table.fnGetData(this, 8);//admin
    //    var fid9 = table.fnGetData(this, 9);//superAdmin
    //    var fid10 = table.fnGetData(this, 10);//selfApproval

    //    $("#popupModal .modal-title").empty();
    //    var title = fid1;
    //    $("#popupModal .modal-title").append(title);

    //    $("#popupModal .modal-body").empty();
    //    var html = "<div class=\"desc\">";
    //    html += "<table class=\"table table-striped\" style=\"width:100%\">";
    //    html += "<tr>";
    //    html += "<td>Designation:</td>";
    //    html += "<td>" + fid2 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Divison:</td>";
    //    html += "<td>" + fid3 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Department:</td>";
    //    html += "<td>" + fid4 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>EmailId:</td>";
    //    html += "<td>" + fid5 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Mobile:</td>";
    //    html += "<td>" + fid6 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Self Approval:</td>";
    //    html += "<td>" + fid10 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>SuperUser:</td>";
    //    html += "<td>" + fid7 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Admin:</td>";
    //    html += "<td>" + fid8 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>SuperAdmin:</td>";
    //    html += "<td>" + fid9 + "</td>";
    //    html += "</tr>";
    //    html += "</table>";
    //    html += "</div>";
    //    $("#popupModal .modal-body").append(html);//.html('<div class=\"descp\"></div>');
    //    $('#popupModal').modal('show');

    //    $('.selfApproval').on('click', function () {
    //        if ($("#selfApproval" + rowIndex).is(":checked")) {
    //            $("#superUserList" + rowIndex).prop('disabled', 'disabled');
    //            $("#adminList" + rowIndex).prop('disabled', 'disabled');
    //            $("#superAdminList" + rowIndex).prop('disabled', 'disabled');
    //        }
    //        else {
    //            $("#superUserList" + rowIndex).prop('disabled', false);
    //            $("#adminList" + rowIndex).prop('disabled', false);
    //            $("#superAdminList" + rowIndex).prop('disabled', false);
    //        }
    //    });
    //});



    //$('.deleteUserCheck').on('click', function (e) {
    //    myRowIndex = $(this).parent().parent().index();
    //    //myColIndex = $(this).index();
    //    //myColIndex = $(this).parent().index()

    //    //var fid2 = table.fnGetData(myRowIndex, 2);//SubChainage Guid

    //    //alert(myRowIndex);

    //    //var count = table.rows({ selected: true });

    //    //alert(count);

    //    e.stopPropagation();
    //});

    $('#deleteUserBtn').on('click', function (e) {
        e.preventDefault();

        var rowsData = table.rows({ selected: true }).data();
        var userGuidToBeDeleted = [];

        for (var i = 0; i < rowsData.length; i++) {
            userGuidToBeDeleted.push(rowsData[i][2]);
        }

        if ($(this).hasClass('disabled')) {

        } else {
            deleteUserConfirmation(userGuidToBeDeleted);
        }
    });

});