﻿$(document).ready(function () {
    var current = 1;

    widget = $(".step");
    btnnext = $(".next");
    btnback = $(".back");
    btnsubmit = $(".submit");

    // Init buttons and UI
    widget.not(':eq(0)').hide();
    hideButtons(current);
    setProgress(current);

    // Next button click action
    btnnext.click(function () {
        //Step 1
        //if (current == 1) {
            if (current < widget.length) {
                // Check validation
                //if ($(".form").valid()) {
                    widget.show();
                    widget.not(':eq(' + (current++) + ')').hide();
                    setProgress(current);
                //}
            }
       // }
        //else if (current == 2) {
        //    if (current < widget.length) {
        //        // Check validation
        //        if ($(".form").valid()) {
        //            widget.show();
        //            widget.not(':eq(' + (current++) + ')').hide();
        //            setProgress(current);
        //        }
        //    }
        //}
        //else {

        //}
        
        hideButtons(current);
    });

    // Submit button click
    btnsubmit.click(function () {
        
    });

    // Back button click action
    btnback.click(function () {
        if (current > 1) {
            current = current - 2;
            if (current < widget.length) {
                widget.show();
                widget.not(':eq(' + (current++) + ')').hide();
                setProgress(current);
            }
        }
        hideButtons(current);
    });

    

});

// Change progress bar action
setProgress = function (currstep) {
    var percent = parseFloat(100 / widget.length) * currstep;
    percent = percent.toFixed();
    $(".progress-bar").css("width", percent + "%").html(percent + "%");
    $("#step-display").html("Step " + currstep + " of " + widget.length);
}

// Hide buttons according to the current step
hideButtons = function (current) {
    var limit = parseInt(widget.length);

    $(".action").hide();

    if (current < limit) btnnext.show();
    if (current > 1) btnback.show();
    if (current == limit) {
        // Show entered values
        $(".display label.lbl").each(function () {
            $(this).html($("#" + $(this).data("id")).val());
        });
        btnnext.hide();
        btnsubmit.show();
    }
}



//(function (i, s, o, g, r, a, m) { i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () { (i[r].q = i[r].q || []).push(arguments) }, i[r].l = 1 * new Date(); a = s.createElement(o), m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m) })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga'); ga('create', 'UA-19096935-1', 'auto'); ga('send', 'pageview');

//(function (i, s, o, g, r, a, m) {
//    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
//        (i[r].q = i[r].q || []).push(arguments)
//    }, i[r].l = 1 * new Date(); a = s.createElement(o),
//    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
//})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

//ga('create', 'UA-43091346-1', 'devzone.co.in');
//ga('send', 'pageview');