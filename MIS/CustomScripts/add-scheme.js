﻿$(document).ready(function () {  

    $("#addSchemeMenuBtn").on('click', function () {
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetCircleNames",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createAddSchemeForm(response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

});

createAddSchemeForm = function (circleArr) {
    $("#addSchemePopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Add Scheme</h2>';

    html += '<div class="panel-body">';
    html += '<form id="addSchemePopupForm" class="form form-horizontal" method="post">';
    html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="circleListToAddScheme" id="circleListToAddSchemeLabel" class="control-label">Circle Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<select id="circleListToAddScheme" name="circleListToAddScheme" class="form-control">';
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="divisionListToAddSchemeBasedOnCircleSelected" id="divisionListToAddSchemeBasedOnCircleSelectedLabel" class="control-label">Division Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<select id="divisionListToAddSchemeBasedOnCircleSelected" name="divisionListToAddSchemeBasedOnCircleSelected" class="form-control">';
    html += '<option value="">--Select Division--</option>';
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="schemeName" id="schemeNameLabel" class="control-label">Scheme Name</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="schemeName" name="schemeName" placeholder="Scheme Name" class="form-control text-capitalize" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="schemeCode" id="schemeCodeLabel" class="control-label">Scheme Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="schemeCode" name="schemeCode" placeholder="Scheme Code" class="form-control text-uppercase" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="schemeType" id="schemeTypeLabel" class="control-label">Scheme Type</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="schemeType" name="schemeType" placeholder="Scheme Type" class="form-control text-capitalize" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="utmEast1" id="utmEast1Label" class="control-label">UTM East1</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="utmEast1" name="utmEast1" placeholder="UTM East1" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="utmEast2" id="utmEast2Label" class="control-label">UTM East2</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="utmEast2" name="utmEast2" placeholder="UTM East2" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="utmNorth1" id="utmNorth1Label" class="control-label">UTM North1</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="utmNorth1" name="utmNorth1" placeholder="UTM North1" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="utmNorth2" id="utmNorth2Label" class="control-label">UTM North2</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="utmNorth2" name="utmNorth2" placeholder="UTM North2" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="drawingId" id="drawingIdLabel" class="control-label">Drawing ID</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="drawingId" name="drawingId" placeholder="Drawing ID" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="area" id="areaLabel" class="control-label">Area</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="area" name="area" placeholder="Area" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-xs-offset-4 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">';
    html += '<input id="addSchemeBtn" class="btn btn-lg btn-primary btn-block" value="Add Scheme" type="submit" />';
    html += '</div>';
    html += '</div>';

    html += '</fieldset>';
    html += '</form>';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    
    $("#addSchemePopupModal .modal-body").append(html);

    showCircleListOptionsToAddScheme('circleListToAddScheme', circleArr);

    addSchemeValidationRules();

    $('#addSchemePopupModal').modal('show');

    $('#circleListToAddScheme').on('change', function () {
        $('#divisionListToAddSchemeBasedOnCircleSelected').empty();
        var selectedCircleGuid = $(this).val();
        $.ajax({
            type: "POST",
            async: false,
            url: "DBAMethods.asmx/GetDivisionsBasedOnCircleGuid",
            data: '{"circleGuid":"' + selectedCircleGuid + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                divisionArr = response.d;
                showDivisionListOptionsToAddScheme('divisionListToAddSchemeBasedOnCircleSelected', divisionArr);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

    $('#addSchemeBtn').on('click', function () {

        if ($("#addSchemePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            var check = saveScheme();

            if (check == 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data is saved successfully.", function () {
                    window.location = 'DBAHome.aspx';
                });
            }
            else if (check == 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data cannot be saved.\nPlease contact your database administrator.", function () {
                    window.location = 'DBAHome.aspx';
                });
            }
        }

    });

}

showCircleListOptionsToAddScheme = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Circle--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 3;
    }
}

showDivisionListOptionsToAddScheme = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Division--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 3;
    }
}

addSchemeValidationRules = function () {
    $('#addSchemePopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            circleListToAddScheme: "required",
            divisionListToAddSchemeBasedOnCircleSelected: "required",
            schemeCode: {
                required: true,
                noSpace: true,
                minlength: 3,
                maxlength: 5
            },
            schemeName: {
                required: true,
                noSpace: true
            },
            schemeType: {
                required: true,
                noSpace: true
            },
            utmEast1: {
                number: true,
            },
            utmEast2: {
                number:true,
            },
            utmNorth1: {
                number: true,
            },
            utmNorth2: {
                number: true,
            },
            drawingId: {
                number: true,
            },
            area: {
                number: true,
            }

        }
    });
}

saveScheme = function () {

    var circleGuid = $('#circleListToAddScheme').val();
    var divisionGuid = $('#divisionListToAddSchemeBasedOnCircleSelected').val();
    var schemeName = $('#schemeName').val();
    var schemeCode = $('#schemeCode').val();
    var schemeType = $('#schemeType').val();
    var utmEast1 = $('#utmEast1').val();
    var utmEast2 = $('#utmEast2').val();
    var utmNorth1 = $('#utmNorth1').val();
    var utmNorth2 = $('#utmNorth2').val();
    var area = $('#area').val();
    var drawingId = $('#drawingId').val();

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveScheme",
        data: '{"schemeCode":"' + schemeCode + '","schemeType":"' + schemeType + '","schemeName":"' + schemeName +
            '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 +
            '","area":"' + area + '","circleGuid":"' + circleGuid + '","divisionGuid":"' + divisionGuid + '","drawingId":"' + drawingId + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);          
        }
    });

    return retValue;
}