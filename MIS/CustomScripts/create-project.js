﻿$(document).ready(function () {
    var arrOfficersEngaged = [];
    var arrPmcSiteEngineers = [];
    var arrSubChainages = [];

    $('#addOfficersEngagedBtn').on('click', function (e) {
        
        $('#officersEngagedListDisplay').removeClass('hidden');

        var data = $('#officersEngaged').val();
        arrOfficersEngaged.push(data);
                
        displayEngagedOfficersList('officersEngagedListDisplay', arrOfficersEngaged);
    });

    displayEngagedOfficersList = function (divname, arr) {

        var div = $('#' + divname);
        div.empty();

        var newDiv = document.createElement('div');
        newDiv.className = "panel panel-default";

        var html = '<div class="panel-heading">Panel heading</div>';
        html += '<div class="panel-body">';

        html += '<ul class = "list-group">';
        for (var index = 0; index < arr.length; index++) {
            html += '<li class="list-group-item">';
            html += arr[index];
            html += '<button type="button" class="btn btn-primary" onclick="removeOfficersEngaged(' + index + ');">Remove</button>';
            html += '</li>';
        }
        
        html += '</ul>';

        html += '</div>';

        newDiv.innerHTML = html;

        document.getElementById(divname).appendChild(newDiv);

    }

    removeOfficersEngaged = function (index) {
        //alert("aditya");
        //alert(index);
        arrOfficersEngaged.splice(index, 1);

        displayEngagedOfficersList('officersEngagedListDisplay', arrOfficersEngaged);

    }



    $('#addPmcSiteEngineersBtn').on('click', function (e) {

        $('#pmcSiteEngineersListDisplay').removeClass('hidden');

        var data = $('#pmcSiteEngineers').val();
        arrPmcSiteEngineers.push(data);

        displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);
    });

    displayPmcSiteEngineersList = function (divname, arr) {

        var div = $('#' + divname);
        div.empty();

        var newDiv = document.createElement('div');
        newDiv.className = "panel panel-default";

        var html = '<div class="panel-heading">Panel heading</div>';
        html += '<div class="panel-body">';

        html += '<ul class = "list-group">';
        for (var index = 0; index < arr.length; index++) {
            html += '<li class="list-group-item">';
            html += arr[index];
            html += '<button type="button" class="btn btn-primary" onclick="removePmcSiteEngineers(' + index + ');">Remove</button>';
            html += '</li>';
        }

        html += '</ul>';

        html += '</div>';

        newDiv.innerHTML = html;

        document.getElementById(divname).appendChild(newDiv);

    }

    removePmcSiteEngineers = function (index) {
        //alert("aditya");
        //alert(index);
        arrPmcSiteEngineers.splice(index, 1);

        displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);

    }




    $('#addSubChainagesBtn').on('click', function (e) {

        $('#subChainagesListDisplay').removeClass('hidden');

        var subChainageName = $('#subChainages').val();
        var associatedSIOName = $('#associatedSIO').val();

        arrSubChainages.push({ chainageName: subChainageName, sioName: associatedSIOName });

        displaySubChainagesList('subChainagesListDisplay', arrSubChainages);
    });

    displaySubChainagesList = function (divname, arr) {

        var div = $('#' + divname);
        div.empty();

        var newDiv = document.createElement('div');
        newDiv.className = "panel panel-default";

        var html = '<div class="panel-heading">Panel heading</div>';
        html += '<div class="panel-body">';

        html += '<ul class = "list-group">';
        for (var index = 0; index < arr.length; index++) {
            html += '<li class="list-group-item">';
            html += "Sub Chainage: " + arr[index].chainageName + " Associated SIO: " + arr[index].sioName;
            html += '<button type="button" class="btn btn-primary" onclick="removeSubChainages(' + index + ');">Remove</button>';
            html += '</li>';
        }

        html += '</ul>';

        html += '</div>';

        newDiv.innerHTML = html;

        document.getElementById(divname).appendChild(newDiv);

    }

    removeSubChainages = function (index) {
        //alert("aditya");
        //alert(index);
        arrSubChainages.splice(index, 1);

        displaySubChainagesList('subChainagesListDisplay', arrSubChainages);

    }




    createProject = function () {
        var districtGuid = $("#districtList").val();
        var divisionGuid = $("#divisionList").val();
        var projectName = $("#projectName").val();
        var projectChainage = $("#chainageOfProject").val();
        var dateOfStart = $("#dateOfStart").val();
        var expectedDateOfCompletion = $("#expectedDateOfCompletion").val();
        var actualDateOfCompletion = $("#actualDateOfCompletion").val();
        var projectValue = $("#projectValue").val();
        
        var arrOfficersEngagedJsonString = JSON.stringify(arrOfficersEngaged);
        var arrPmcSiteEngineersJsonString = JSON.stringify(arrPmcSiteEngineers);
        var arrSubChainagesJsonString = JSON.stringify(arrSubChainages);

        $.ajax({
            type: "Post",
            async: false,
            url: "CreateProject.aspx/CreateNewProject",
            data: '{"districtGuid":"' + districtGuid + '","divisionGuid":"' + divisionGuid + '","projectName":"' + projectName + '","projectChainage":"' + projectChainage +
                '","dateOfStart":"' + dateOfStart + '","expectedDateOfCompletion":"' + expectedDateOfCompletion + '","actualDateOfCompletion":"' + actualDateOfCompletion +
                '","projectValue":"' + projectValue + '","arrOfficersEngaged":' + arrOfficersEngagedJsonString + ',"arrPmcSiteEngineers":' + arrPmcSiteEngineersJsonString + ',"arrSubChainages":' + arrSubChainagesJsonString + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //$('#status').delay(300).fadeOut();
                //$('#preloader').delay(350).fadeOut('slow');

                alert("Success");

                //var str = response.d;
                //if (str == true) {
                //    bootbox.confirm({
                //        title: '!!Attention!!',
                //        message: 'There is already a person with this designation in this division in our database. To change click Change otherwise click Exit.',
                //        buttons: {
                //            'cancel': {
                //                label: 'Change',
                //                className: 'btn-default pull-left'
                //            },
                //            'confirm': {
                //                label: 'Exit',
                //                className: 'btn-danger pull-right'
                //            }
                //        },
                //        callback: function (result) {
                //            if (result) {
                //                //window.location = $("a[data-bb='confirm']").attr('href');
                //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                //                //window.location.href = nextUrl;
                //                window.location = 'Default.aspx';
                //            }
                //        }
                //    });
                //}
                //ret = str;
            },
            failure: function (msg) {
                //$('#status').delay(300).fadeOut();
                //$('#preloader').delay(350).fadeOut('slow');
                //bootbox.alert("Please contact your administrator.");
            }
        });


        //var parameters = {
        //    benefs1: [
        //        {
        //            Nome: $('#txtBenefNome').val(),
        //            DataNasc: $('#txtBenefDataNasc').val(),
        //            GrauParent: $('#txtBenefGrauParent').val()
        //        }
        //    ]
        //};


        

        ////var identedText = JSON.stringify(arrSubChainages, null, 4);
        //var bcd = '{"arrSubChainages":' + myJsonString1 + '}';

        ////document.write("<h1>Idented 4 spaces</h1> <pre>" + bcd + "</pre>");

        //var jsonData = JSON.stringify({
        //    'id': "1",
        //    objPerson: {
        //        'name': "adi",
        //        'email': "asa"
        //    }
        //});


        ////document.write("<h1>Idented 4 spaces</h1> <pre>" + jsonData + "</pre>");

        //$.ajax({
        //    type: "Post",
        //    async: false,
        //    url: "CreateProject.aspx/CreateNewProject1",
        //    //data: '{"arrOfficersEngaged":["value1", "value2", "value3", "value4", "value5"], "arrPmcSiteEngineers":["Bill", "Scott", "Brad"]}',//'{"arrOfficersEngaged":"' + arrOfficersEngaged + '","arrPmcSiteEngineers":"' + arrPmcSiteEngineers + '"}',
        //    data: bcd,//'{"arrOfficersEngaged":' + arrOfficersEngaged + ',"arrPmcSiteEngineers":' + arrPmcSiteEngineers + '}',
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    success: function (response) {
        //        //$('#status').delay(300).fadeOut();
        //        //$('#preloader').delay(350).fadeOut('slow');

        //        alert("Success1");

        //        //var str = response.d;
        //        //if (str == true) {
        //        //    bootbox.confirm({
        //        //        title: '!!Attention!!',
        //        //        message: 'There is already a person with this designation in this division in our database. To change click Change otherwise click Exit.',
        //        //        buttons: {
        //        //            'cancel': {
        //        //                label: 'Change',
        //        //                className: 'btn-default pull-left'
        //        //            },
        //        //            'confirm': {
        //        //                label: 'Exit',
        //        //                className: 'btn-danger pull-right'
        //        //            }
        //        //        },
        //        //        callback: function (result) {
        //        //            if (result) {
        //        //                //window.location = $("a[data-bb='confirm']").attr('href');
        //        //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
        //        //                //window.location.href = nextUrl;
        //        //                window.location = 'Default.aspx';
        //        //            }
        //        //        }
        //        //    });
        //        //}
        //        //ret = str;
        //    },
        //    failure: function (msg) {
        //        //$('#status').delay(300).fadeOut();
        //        //$('#preloader').delay(350).fadeOut('slow');
        //        //bootbox.alert("Please contact your administrator.");
        //    }
        //});
    }
    

    //$('#popupForm').validate({ // initialize plugin
    //    ignore: ":not(:visible)",
    //    rules: {
    //        emailInput: {
    //            required: true,
    //            //email: true
    //            email1: true
    //        }
    //    },
    //    messages: {
    //        emailInput: {
    //            required: "Please Enter your Email Id.",
    //            //email: "Please check the Email Id entered."
    //            email1: "Please check the Email Id entered."
    //        }
    //    },
    //    tooltip_options: {
    //        emailInput: {
    //            trigger: "focus",
    //            html: true
    //        }
    //    }
    //});

    //forgotPassword = function () {

    //    emailId = $.trim($("#emailInput").val());

    //    if ($("#popupForm").valid()) {

    //        $("#preloader").show();
    //        $("#status").show();

    //        $.ajax({
    //            type: "Post",
    //            async: false,
    //            url: "Default.aspx/RetrievePassword",
    //            data: '{"emailId":"' + emailId + '"}',
    //            contentType: "application/json; charset=utf-8",
    //            dataType: "json",
    //            success: function (response) {
    //                $('#status').delay(300).fadeOut();
    //                $('#preloader').delay(350).fadeOut('slow');

    //                var str = response.d;

    //                bootbox.alert(str);
    //            },

    //            failure: function (msg) {
    //                //$('#output').text(msg);
    //                $('#status').delay(300).fadeOut();
    //                $('#preloader').delay(350).fadeOut('slow');
    //                bootbox.alert("Please contact your administrator.");
    //                //return ret;
    //            }
    //        });
    //    }
   // }
});