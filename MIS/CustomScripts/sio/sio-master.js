﻿$(window).load(function () {

    $('.marquee').marquee({
        //speed in milliseconds of the marquee
        duration: 15000,
        //gap in pixels between the tickers
        gap: 50,
        //time in milliseconds before the marquee will start animating
        delayBeforeStart: 0,
        //'left' or 'right'
        direction: 'left',
        pauseOnHover: true
    });
});

//FOCUS FUNCTION
sioFocusFunction = function (id) {
    $('#' + id).css('background-color', 'yellow');
}

//BLUR FUNCTION
sioBlurFunction = function (id) {
    $('#' + id).css('background-color', '');
}

//CHECK NULL STRINGS
stringIsNullOrEmpty = function (str) {
    return (str == null || str === "");
}

//
capitalizeFirstAllWords = function (str) {
    var pieces = str.split(" ");
    for (var i = 0; i < pieces.length; i++) {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
}

//multi spli
multiSplit = function (str, delimeters) {
    var result = [str];
    if (typeof (delimeters) == 'string')
        delimeters = [delimeters];
    while (delimeters.length > 0) {
        for (var i = 0; i < result.length; i++) {
            var tempSplit = result[i].split(delimeters[0]);
            result = result.slice(0, i).concat(tempSplit).concat(result.slice(i + 1));
        }
        delimeters.shift();
    }
    return result;
}

//DIVISIONS

//Gets the Divisons list from DB.
getDivisions = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetDivisions",
        //data: '{"workItemGuid":"' + workItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

//Displays Options for Divisions
showDivisionOptions = function (id, arr) {
    var option_str = document.getElementById(id);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Division--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].DivisionName + ' (' + arr[i].DivisionCode + ')', arr[i].DivisionGuid);
    }
}

//Schemes

//Gets the Schemes list from DB.
getSchemes = function (divisionGuid) {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetSchemes",
        data: '{"divisionGuid":"' + divisionGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

//Displays Options for Divisions
showSchemeOptions = function (id, arr) {
    var option_str = document.getElementById(id);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Scheme--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].SchemeName + ' (' + arr[i].SchemeCode + ')', arr[i].SchemeGuid);
    }
}