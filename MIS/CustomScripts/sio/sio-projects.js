﻿var heading = "";
var subHeading = "";
var progressChart = null;

$(document).ready(function () {

    var btnClicked;

    var sioProjectsTable = $('table#sioSubChainageList').DataTable({
        //responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        //stateSave: true,
        //destroy: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [4],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [5],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [6],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [7],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                //width: '30%',
                targets: [8],
                orderable: false,
                searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewDbaProjectBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editDbaProjectBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteDbaProjectBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>'
            }
        ],
        order: [[2, 'asc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        //dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
    });

    sioProjectsTable.on('order.dt search.dt', function () {
        sioProjectsTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $(document).on('click', '#sioSubChainageList a.addSubChainageProgressBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];
        
        //myRowIndex = $(this).parent().parent().index();
        //myColIndex = $(this).parent().index();

        heading = 'Add Progress';
        subHeading = 'Pick the Month For which you want to Add the Progress';

        createProgressMonthPickerPopupForm(sioProjectsTable, selectedRow);

        e.stopPropagation();
    });

    $(document).on('click', '#sioSubChainageList a.viewSubChainageProgressBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        //myRowIndex = $(this).parent().parent().index();
        //myColIndex = $(this).parent().index();

        heading = 'View Progress';
        subHeading = 'Pick the Month For which you want to View the Progress';

        createProgressMonthPickerPopupForm(sioProjectsTable, selectedRow);

        e.stopPropagation();
    });

    $(document).on('click', '#sioSubChainageList a.editSubChainageProgressBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        //myRowIndex = $(this).parent().parent().index();
        //myColIndex = $(this).parent().index();

        heading = 'Edit Progress';
        subHeading = 'Pick the Month For which you want to Edit the Progress';

        createProgressMonthPickerPopupForm(sioProjectsTable, selectedRow);

        e.stopPropagation();
    });

    $(document).on('click', '#sioSubChainageList a.viewSubChainageProgressChartBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        //myRowIndex = $(this).parent().parent().index();
        //myColIndex = $(this).parent().index();

        //heading = 'Edit Progress';
        //subHeading = 'Pick the Month For which you want to Edit the Progress';

        createProgressChartPopupForm(sioProjectsTable, selectedRow);

        e.stopPropagation();
    });




    //$('#addMonthlyProgressPopupModal').on('click', '.modal-footer button#backFromAddMonthlyProgressPopupModalBtn', function (e) {
    //    e.preventDefault();

    //    $('#addMonthlyProgressPopupModal').modal('hide');

    //    $('#progressMonthPickerPopupModal').modal('show');

    //    e.stopPropagation();
    //});

    //$('#viewMonthlyProgressPopupModal').on('click', '.modal-footer button#backFromViewMonthlyProgressPopupModalBtn', function (e) {
    //    e.preventDefault();

    //    $('#viewMonthlyProgressPopupModal').modal('hide');

    //    $('#progressMonthPickerPopupModal').modal('show');

    //    e.stopPropagation();
    //});

    $('#monthlyProgressPopupModal').on('click', '.modal-footer button#backFromMonthlyProgressPopupModalBtn', function (e) {
        e.preventDefault();

        $('#monthlyProgressPopupModal').modal('hide');

        $('#progressMonthPickerPopupModal').modal('show');

        e.stopPropagation();
    });

});

createProgressMonthPickerPopupForm = function (sioProjectsTable, selectedRow) {

    var selectedSubChainageGuid = sioProjectsTable.row(selectedRow).data()[1];//SubChainage Guid
    var selectedSubChainageName = sioProjectsTable.row(selectedRow).data()[2];//SubChainage Name

    var selectedSubChainageProjectName = sioProjectsTable.row(selectedRow).data()[3];//Project Name

    var selectedSubChainageStartingDate = sioProjectsTable.row(selectedRow).data()[4];//SubChainage Starting Date
    var selectedSubChainageCompletionDate = sioProjectsTable.row(selectedRow).data()[5];//SubChainage End Date

    var selectedSubChainageUsedDates = sioProjectsTable.row(selectedRow).data()[6];//SubChainage Used Dates
    var serverCurrentDateString = sioProjectsTable.row(selectedRow).data()[7];//Server Current Date

    $("#progressMonthPickerPopupModal .modal-header").empty();

    var html = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
    html += '<h1 class="text-center">' + heading + '</h1>';
    html += '<h4 class="text-center">SubChainage: ' + selectedSubChainageName + '</h4>';

    $("#progressMonthPickerPopupModal .modal-header").append(html);

    $("#progressMonthPickerPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/calender2.png" class="login" height="70" />';
    html += '<h3 class="text-center">' + subHeading + '</h3>';

    html += '<div class="panel-body">';
    html += '<form id="progressMonthPickerPopupForm" name="progressMonthPickerPopupForm" role="form" class="form form-horizontal" method="post">';

    html += '<div class="form-group">';

    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="progressMonth" id="progressMonthLabel" class="control-label pull-right">Progress Month</label>';
    html += '</div>';

    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<div id="dpProgressMonth" class="input-group date">';
    html += '<input type="text" name="progressMonth" class="form-control" id="progressMonth" placeholder="Month" readonly="readonly" />';
    html += '<span class="input-group-addon form-control-static">';
    html += '<i class="fa fa-calendar fa-fw"></i>';
    html += '</span>';
    html += '</div>';
    html += '</div>';

    html += '</div>';

    html += '</form>';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#progressMonthPickerPopupModal .modal-body").append(html);

    progressMonthPickerPopupModalDatePickerRelatedFunctions(selectedSubChainageStartingDate, selectedSubChainageCompletionDate, selectedSubChainageUsedDates, serverCurrentDateString);

    //$('#progressMonthPickerPopupModal').modal('show');

    $('#progressMonthPickerPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    $('#progressMonthPickerPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            progressMonth: {
                required: true,
                dateInSpecifiedFormat: "MMMM, yyyy"//[name = 'remainingBudgetLbl']
            }
        },
        messages: {
            progressMonth: {
                required: "Please Enter the Month For Progress.",
                dateITA: "Please Enter the Month For Progress in the specified format."
            }
        },
        errorClass: 'help-block small has-error',
        validClass: 'help-block has-success',
        errorElement: 'span',
        //success: function (label, element) {
        //    //$(element).closest('.form-group').addClass('help-block has-success');
        //    //label.empty();
        //    //label.append('<i class="fa fa-check-square-o" aria-hidden="true">OK!</i>');
        //    //label.text("Ok!");
        //},
        highlight: function (element, errorClass, validClass) {
            //$(element).closest('.form-group').addClass('has-error');
            $(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            //$(element).closest('.form-group').removeClass('has-error');
            $(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('multiselect')) {
                //error.insertAfter($('#' + element.attr('id')));
                //error.insertBefore(element);
                error.insertAfter(element.parent().find('.btn-group'));
                //$(this).parents("fieldset").find("*:not('legend')").toggle();
            } else {
                //element.closest('.form-group').append(error);
                error.insertAfter(element);
            }

            //switch (element.attr("name")) {
            //    case "dpProgressMonth":
            //        error.insertAfter($("#dpProgressMonth"));
            //        break;

            //    default:
            //        //nothing
            //}

        },
        submitHandler: function () {
            //alert('valid form');
            return false;
        }
    });

    $("#progressMonthPickerPopupModal").on('hide.bs.modal', function () {
        $('#progressMonthPickerPopupModal input#progressMonth').val('');
        $('#progressMonthPickerPopupModal button#proceedBtn').addClass('disabled');
    });

    $("#progressMonthPickerPopupModal").on('click', 'button#proceedBtn', function (e) {
        e.preventDefault();

        if ($(this).hasClass('disabled')) {

        } else {
            if ($("#progressMonthPickerPopupForm").valid()) {

                $("#preloader").show();
                $("#status").show();

                var selectedDate = Date.parseExact($('#dpProgressMonth').datepicker('getDate').toString('dd/MM/yyyy'), 'dd/MM/yyyy').toString('yyyy/MM/dd');

                $('#progressMonthPickerPopupModal').modal('hide');

                if (heading === 'Add Progress') {
                    var subChainageMonthlyProgressExists = checkSubChainageMonthlyProgressExistence(selectedSubChainageGuid, selectedDate);

                    if (Date.compare(new Date(selectedDate), new Date(Date.parseExact(selectedSubChainageStartingDate, 'dd/MM/yyyy').setDate(1))) === 0) {

                        if (subChainageMonthlyProgressExists === false) {
                            var addMonthlyProgressModalBodyStringReceived = getAddMonthlyProgressPopupModalBodyString(selectedSubChainageGuid, selectedDate);

                            $('#status').delay(300).fadeOut();
                            $('#preloader').delay(350).fadeOut('slow');

                            createAddMonthlyProgressPopupForm(selectedSubChainageName, addMonthlyProgressModalBodyStringReceived, selectedSubChainageGuid, selectedDate);
                        } else {
                            $('#status').delay(300).fadeOut();
                            $('#preloader').delay(350).fadeOut('slow');

                            bootbox.alert("The Progress of the selected SubChainage already exists for the selected month.", function () {
                                $('#progressMonthPickerPopupModal').modal('show');
                            });
                        }

                    } else if (Date.compare(new Date(selectedDate), new Date(Date.parseExact(selectedSubChainageStartingDate, 'dd/MM/yyyy').setDate(1))) === 1) {

                        var selectedLastMonthDate = Date.parseExact($('#dpProgressMonth').datepicker('getDate').toString('dd/MM/yyyy'), 'dd/MM/yyyy').addMonths(-1).toString('yyyy/MM/dd');

                        var subChainageLastMonthProgressExists = checkSubChainageMonthlyProgressExistence(selectedSubChainageGuid, selectedLastMonthDate);

                        if (subChainageLastMonthProgressExists === true) {

                            if (subChainageMonthlyProgressExists === false) {
                                var addMonthlyProgressModalBodyStringReceived = getAddMonthlyProgressPopupModalBodyString(selectedSubChainageGuid, selectedDate);

                                $('#status').delay(300).fadeOut();
                                $('#preloader').delay(350).fadeOut('slow');

                                createAddMonthlyProgressPopupForm(selectedSubChainageName, addMonthlyProgressModalBodyStringReceived, selectedSubChainageGuid, selectedDate);
                            } else {
                                $('#status').delay(300).fadeOut();
                                $('#preloader').delay(350).fadeOut('slow');

                                bootbox.alert("The Progress of the selected SubChainage already exists for the selected month.", function () {
                                    $('#progressMonthPickerPopupModal').modal('show');
                                });
                            }

                        } else {
                            $('#status').delay(300).fadeOut();
                            $('#preloader').delay(350).fadeOut('slow');

                            bootbox.alert("The Progress of the selected SubChainage does not exist for the last month.", function () {
                                $('#progressMonthPickerPopupModal').modal('show');
                            });
                        }

                    }

                } else if (heading === 'View Progress') {
                    var subChainageMonthlyProgressExists = checkSubChainageMonthlyProgressExistence(selectedSubChainageGuid, selectedDate);

                    if (subChainageMonthlyProgressExists === false) {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');

                        bootbox.alert("The Progress of the selected SubChainage does not exist for the selected month.", function () {
                            $('#progressMonthPickerPopupModal').modal('show');
                        });
                    } else {
                        var viewMonthlyProgressModalBodyStringReceived = getViewMonthlyProgressPopupModalBodyString(selectedSubChainageGuid, selectedDate);

                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');

                        createViewMonthlyProgressPopupForm(selectedSubChainageName, viewMonthlyProgressModalBodyStringReceived);
                    }
                } else if (heading === 'Edit Progress') {
                    var subChainageMonthlyProgressExists = checkSubChainageMonthlyProgressExistence(selectedSubChainageGuid, selectedDate);

                    if (subChainageMonthlyProgressExists === false) {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');

                        bootbox.alert("The Progress of the selected SubChainage does not exist for the selected month.", function () {
                            $('#progressMonthPickerPopupModal').modal('show');
                        });
                    } else {
                        var editMonthlyProgressModalBodyStringReceived = getEditMonthlyProgressPopupModalBodyString(selectedSubChainageGuid, selectedDate);

                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');

                        createEditMonthlyProgressPopupForm(selectedSubChainageName, editMonthlyProgressModalBodyStringReceived, selectedSubChainageGuid, selectedDate);
                    }
                }
            } else {
                alert("Please select the Progress Month to proceed.");
            }
        }

    });

};

progressMonthPickerPopupModalDatePickerRelatedFunctions = function (startDate, endDate, usedDates, currentDateString) {
    var startDateFull = new Date(Date.parseExact(startDate, 'dd/MM/yyyy').setDate(1));
    var endDateFull = new Date(Date.parseExact(endDate, 'dd/MM/yyyy').setDate(1));

    //var selectedDate = Date.parseExact($('#dpProgressMonth').datepicker('getDate').toString('dd/MM/yyyy'), 'dd/MM/yyyy').toString('yyyy/MM/dd');
    //


    //currentDate = Date.parse(currentDateString);

    var usedDatesArray = usedDates.split(',');

    $('#dpProgressMonth').datepicker({
        //format: "MM, yyyy",
        format: {
            toDisplay: function (date, format, language) {
                var d = new Date(date);
                return d.toString('MMMM, yyyy');
            },
            toValue: function (date, format, language) {
                var d = new Date(date);
                return new Date(d);
            }
        },
        startView: 1,
        minViewMode: "months",
        maxViewMode: 2,//2-years
        startDate: startDateFull,
        endDate: endDateFull,
        beforeShowMonth: function (date) {
            if (date.between(startDateFull, endDateFull)) {

                if (heading === 'Add Progress') {
                    var formattedDate = date.toString('dd/MM/yyyy');//$.fn.datepicker.DPGlobal.formatDate(date, 'MM, yyyy', 'en-IN');
                    if ($.inArray(formattedDate.toString(), usedDatesArray) !== -1) {
                        return {
                            enabled: false,
                            classes: "disabled-period",
                            tooltip: "Progress Already Entered For " + date.toString('MMMM, yyyy') + "."
                        };
                    }
                    else if (Date.compare(date, Date.today().set({ day: 1 })) === 0) {
                        return {
                            enabled: false,
                            classes: "disabled-period",
                            tooltip: "Progress Cannot be Entered For the Current Month " + date.toString('MMMM, yyyy') + "."
                        };
                    }
                    else if (Date.compare(date, Date.today().set({ day: 1 })) === 1) {
                        return {
                            enabled: false,
                            classes: "disabled-period",
                            tooltip: "Progress Cannot be Entered For the Coming Month " + date.toString('MMMM, yyyy') + "."
                        };
                    }
                    else {
                        return {
                            enabled: true,
                            classes: "selected-period",
                            tooltip: "Enter Progress For " + date.toString('MMMM, yyyy') + "."
                        };
                    }
                    
                }
                else if (heading === 'View Progress') {
                    var formattedDate = date.toString('dd/MM/yyyy');//$.fn.datepicker.DPGlobal.formatDate(date, 'MM, yyyy', 'en-IN');
                    if ($.inArray(formattedDate.toString(), usedDatesArray) !== -1) {
                        return {
                            enabled: true,
                            classes: "selected-period",
                            tooltip: "View Progress for " + date.toString('MMMM, yyyy') + "."
                        };
                    }
                    return {
                        enabled: false,
                        classes: "disabled-period",
                        tooltip: "No Progress Entered."
                    };
                }
                else if (heading === 'Edit Progress') {
                    var formattedDate = date.toString('dd/MM/yyyy');//$.fn.datepicker.DPGlobal.formatDate(date, 'MM, yyyy', 'en-IN');
                    if ($.inArray(formattedDate.toString(), usedDatesArray) !== -1) {
                        return {
                            enabled: true,
                            classes: "selected-period",
                            tooltip: "Edit Progress for " + date.toString('MMMM, yyyy') + "."
                        };
                    }
                    return {
                        enabled: false,
                        classes: "disabled-period",
                        tooltip: "No Progress Entered."
                    };
                }



                //var formattedDate = date.toString('dd/MM/yyyy');//$.fn.datepicker.DPGlobal.formatDate(date, 'MM, yyyy', 'en-IN');
                //if ($.inArray(formattedDate.toString(), disabledDatesArray) != -1) {
                //    return {
                //        enabled: false,
                //        classes: "selected-period",
                //        tooltip: "Hello\nAditya"
                //    };
                //}
                //return {
                //    enabled: true,
                //    classes: "selected-period",
                //    tooltip: "Hello\nAditya"
                //};
                //if ($.inArray(formattedDate, disabledDatesArray) != -1) {
                //    return false;
                //} else {
                //    return true;
                //}
            }


        },
        beforeShowYear: function (date) {
            var formattedDate = date.toString('MMMM, yyyy');//$.fn.datepicker.DPGlobal.formatDate(date, 'MM, yyyy', 'en-IN');
            if ($.inArray(formattedDate.toString(), usedDatesArray) !== -1) {
                return {
                    enabled: false,
                    classes: "selected-period",
                    tooltip: "Hello\nAditya"
                };
            }
            return {
                enabled: true,
                classes: "selected-period",
                tooltip: "Hello\nAditya"
            };
            //if ($.inArray(formattedDate, disabledDatesArray) != -1) {
            //    return false;
            //} else {
            //    return true;
            //}
        },
        forceParse: false
    }).on('changeMonth', function (e) {
        var dp = $(e.currentTarget).data('datepicker');
        dp.date = e.date;
        dp.setValue();
        dp.hide();

        $('#proceedBtn').removeClass('disabled');
        //}).on('hide', function () {
        //    if (!this.firstHide) {
        //        if (!$(this).is(":focus")) {
        //            this.firstHide = true;
        //            // this will inadvertently call show (we're trying to hide!)
        //            this.focus();
        //        }
        //    } else {
        //        this.firstHide = false;
        //    }
        //}).on('show', function () {
        //    if (this.firstHide) {
        //        // careful, we have an infinite loop!
        //        $(this).datepicker('hide');
        //    }
    });
};

checkSubChainageMonthlyProgressExistence = function (subChainageGuid, selectedDate) {

    var retValue = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "SIOProjects.aspx/CheckSubChainageProgressExistenceForGivenMonth",
        data: '{"subChainageGuid":"' + subChainageGuid + '","selectedDate":"' + selectedDate + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your administrator");
        }
    });

    return retValue;
};

getAddMonthlyProgressPopupModalBodyString = function (subChainageGuid, selectedDate) {

    var retValue;

    $.ajax({
        type: "Post",
        async: false,
        url: "SIOProjects.aspx/CreateAddMonthlyProgressPopupModalBodyString",
        data: '{"subChainageGuid":"' + subChainageGuid + '","selectedDate":"' + selectedDate + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your administrator");
        }
    });

    return retValue;
};

createAddMonthlyProgressPopupForm = function (selectedSubChainageName, addMonthlyProgressModalBodyString, subChainageGuid, selectedDate) {
    $("#monthlyProgressPopupModal .modal-header").empty();

    //var html1 = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
    var addMonthlyProgressModalHeaderHtml = '<h1 class="text-center">' + heading + '</h1>';
    addMonthlyProgressModalHeaderHtml += '<h4 class="text-center">SubChainage: ' + selectedSubChainageName + '</h4>';

    $("#monthlyProgressPopupModal .modal-header").append(addMonthlyProgressModalHeaderHtml);

    $("#monthlyProgressPopupModal .modal-body").empty();

    $("#monthlyProgressPopupModal .modal-body").append(addMonthlyProgressModalBodyString);

    //$("#monthlyProgressPopupModal .modal-footer").empty();

    //var modalFooterHtml = '<div class="col-md-12">';
    //modalFooterHtml += '<button id="backFromonthlyProgressPopupModalBtn" name="backFromEditMonthlyProgressPopupModalBtn" class="btn" aria-hidden="true">Back</button>';
    //modalFooterHtml += '</div>';

    //$("#monthlyProgressPopupModal .modal-footer").append(modalFooterHtml);

    //$('#addMonthlyProgressPopupModal').modal('show');

    $('#monthlyProgressPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    $('#addMonthlyProgressPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        //ignore: ':hidden:not(".multiselect")',
        //rules: {
        //    districtList: "required",
        //    divisionList: "required"            
        //},
        //messages: {
        //    districtList: {
        //        required: "Please Enter Project District."
        //    },
        //    divisionList: {
        //        required: "Please Enter Project Division."
        //    }
        //},
        errorClass: 'help-block small has-error',
        validClass: 'help-block has-success',
        errorElement: 'span',
        //success: function (label, element) {
        //    //$(element).closest('.form-group').addClass('help-block has-success');
        //    //label.empty();
        //    //label.append('<i class="fa fa-check-square-o" aria-hidden="true">OK!</i>');
        //    //label.text("Ok!");
        //},
        highlight: function (element, errorClass, validClass) {
            //$(element).closest('.form-group').addClass('has-error');
            $(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            //$(element).closest('.form-group').removeClass('has-error');
            $(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('multiselect')) {
                //error.insertAfter($('#' + element.attr('id')));
                //error.insertBefore(element);
                error.insertAfter(element.parent().find('.btn-group'));
                //$(this).parents("fieldset").find("*:not('legend')").toggle();
            } else {
                //element.closest('.form-group').append(error);
                error.insertAfter(element);
            }
        },
        submitHandler: function () {
            //alert('valid form');
            return false;
        }
    });

    var workItems0Element = document.getElementsByClassName("workItems0");
    var workItemsElement = document.getElementsByClassName("workItems");

    for (var i = 0; i < workItemsElement.length; i++) {
        var workItemGuid = $('#' + workItems0Element[i].getElementsByTagName("input")[0].getAttribute("id")).val();
        var workItemBalance = workItems0Element[i].getElementsByTagName("input")[3].getAttribute("name");

        var workItemName = $('#' + workItemsElement[i].getElementsByTagName("input")[0].getAttribute("id") + 'Label').text();

        $('input[name="' + workItemsElement[i].getElementsByTagName("input")[0].getAttribute("name") + '"]').rules('add', {
            required: true,
            number: true,
            smallerThanEqualTo: [name = workItemBalance],
            messages: {
                required: 'This field is required.',
                number: 'Please Enter a Valid value for ' + workItemName + '.',
                smallerThanEqualTo: 'Value should be less than ' + $('#' + workItemBalance).val() + '.'
                //minlength: jQuery.validator.format("Please, at least {0} characters are necessary")
            }
        });

    }

    //Submit button click action
    $('#monthlyProgressPopupModal').on('click', 'a#saveNewMonthlyProgressBtn', function (e) {
        e.preventDefault();

        if ($('#addMonthlyProgressPopupForm').valid()) {
            $("#preloader").show();
            $("#status").show();

            var progressSaved = saveProjectPhysicalProgress(subChainageGuid, selectedDate);

            if (progressSaved === 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data is saved successfully.", function () {
                    window.location = 'SIOProjects.aspx';
                });
            } else {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data cannot be saved.\nPlease contact your administrator.", function () {
                    window.location = 'SIOProjects.aspx';
                });
            }
        }

    });

    $('#monthlyProgressPopupModal').on('click', 'a#cancelNewMonthlyProgressBtn', function (e) {
        e.preventDefault();

        $('#monthlyProgressPopupModal').modal('hide');

        $('#progressMonthPickerPopupModal').modal('show');

        e.stopPropagation();
    });

};

saveProjectPhysicalProgress = function (subChainageGuid, selectedDate) {
    var retValue = 0;

    var fremaaOfficerProgressManager = [];

    var fremaaOfficersElement = document.getElementsByClassName("fremaaOfficers");

    for (var i = 0; i < fremaaOfficersElement.length; i++) {
        var x1 = fremaaOfficersElement[i].getElementsByTagName("input")[0].getAttribute("id");
        var x2 = fremaaOfficersElement[i].getElementsByTagName("input")[1].getAttribute("name");

        var projectAndFremaaOfficersRelationManager = {
            fremaaOfficerProjectRelationshipGuid: $.trim($('#' + x1).val())
        };

        fremaaOfficerProgressManager.push({
            projectAndFremaaOfficersRelationManager: projectAndFremaaOfficersRelationManager,
            fremaaOfficerArose: parseBool($('input[name="' + x2 + '"]:checked', '#addMonthlyProgressPopupForm').val())
        });

    }

    var pmcSiteEngineerProgressManager = [];

    var pmcSiteEngineersElement = document.getElementsByClassName("pmcSiteEngineers");

    for (var i = 0; i < pmcSiteEngineersElement.length; i++) {
        var x1 = pmcSiteEngineersElement[i].getElementsByTagName("input")[0].getAttribute("id");
        var x2 = pmcSiteEngineersElement[i].getElementsByTagName("input")[1].getAttribute("name");

        var projectAndPmcSiteEngineersRelationManager = {
            pmcSiteEngineerProjectRelationshipGuid: $.trim($('#' + x1).val())
        };

        pmcSiteEngineerProgressManager.push({
            projectAndPmcSiteEngineersRelationManager: projectAndPmcSiteEngineersRelationManager,
            pmcSiteEngineerArose: parseBool($('input[name="' + x2 + '"]:checked', '#addMonthlyProgressPopupForm').val())
        });

    }

    var workItemProgressManager = [];

    var workItems0Element = document.getElementsByClassName("workItems0");
    var workItemsElement = document.getElementsByClassName("workItems");

    for (var i = 0; i < workItemsElement.length; i++) {
        var workItemGuid = $('#' + workItems0Element[i].getElementsByTagName("input")[0].getAttribute("id")).val();
        var workItemTotalValue = $('#' + workItems0Element[i].getElementsByTagName("input")[1].getAttribute("id")).val();
        var workItemCumulativeValue = $('#' + workItems0Element[i].getElementsByTagName("input")[2].getAttribute("id")).val();
        var workItemBalanceValue = $('#' + workItems0Element[i].getElementsByTagName("input")[3].getAttribute("id")).val();

        var workItemProgressValue = $('#' + workItemsElement[i].getElementsByTagName("input")[0].getAttribute("id")).val();

        var workItemNewCumulativeValue = numbers.basic.sum([parseFloat(workItemCumulativeValue), parseFloat(workItemProgressValue)]);
        var workItemNewBalanceValue = numbers.basic.subtraction([parseFloat(workItemTotalValue), workItemNewCumulativeValue]);

        var workItemManager = {
            workItemGuid: workItemGuid,
            workItemTotalValue: workItemTotalValue,
            workItemCumulativeValue: workItemNewCumulativeValue,
            workItemBalanceValue: workItemNewBalanceValue
        };

        workItemProgressManager.push({
            workItemProgressValue: workItemProgressValue,
            workItemCumulativeValue: workItemNewCumulativeValue,
            workItemBalanceValue: workItemNewBalanceValue,
            workItemManager: workItemManager
        });

    }

    var subChainageManager = {
        subChainageGuid: subChainageGuid
    };

    var subChainagePhysicalProgressManager = {
        physicalProgressDate: selectedDate,
        subChainageManager: subChainageManager,
        fremaaOfficerProgressManager: fremaaOfficerProgressManager,
        pmcSiteEngineerProgressManager: pmcSiteEngineerProgressManager,
        workItemProgressManager: workItemProgressManager
    };

    var subChainagePhysicalProgressManagerJsonString = JSON.stringify(subChainagePhysicalProgressManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "SIOProjects.aspx/AddProjectPhysicalProgress",
        data: '{"subChainagePhysicalProgressManager":' + subChainagePhysicalProgressManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });

    return retValue;
};

getViewMonthlyProgressPopupModalBodyString = function (subChainageGuid, selectedDate) {

    var retValue;

    $.ajax({
        type: "Post",
        async: false,
        url: "SIOProjects.aspx/CreateViewMonthlyProgressPopupModalBodyString",
        data: '{"subChainageGuid":"' + subChainageGuid + '","selectedDate":"' + selectedDate + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your administrator");
        }
    });

    return retValue;
};

createViewMonthlyProgressPopupForm = function (selectedSubChainageName, viewMonthlyProgressModalBodyString) {
    $("#monthlyProgressPopupModal .modal-header").empty();

    //var html1 = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
    var viewMonthlyProgressModalHeaderHtml = '<h1 class="text-center">' + heading + '</h1>';
    viewMonthlyProgressModalHeaderHtml += '<h4 class="text-center">SubChainage: ' + selectedSubChainageName + '</h4>';

    $("#monthlyProgressPopupModal .modal-header").append(viewMonthlyProgressModalHeaderHtml);

    $("#monthlyProgressPopupModal .modal-body").empty();

    $("#monthlyProgressPopupModal .modal-body").append(viewMonthlyProgressModalBodyString);

    //$('#viewMonthlyProgressPopupModal').modal('show');

    //var progressChart = createProgressChart();

    $('#monthlyProgressPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    $('#monthlyProgressPopupModal').on('show.bs.modal', function () {
        $('#progressChartContainer').css('visibility', 'hidden');
    });
    $('#monthlyProgressPopupModal').on('shown.bs.modal', function () {
        $('#progressChartContainer').css('visibility', 'initial');
        //progressChart.reflow();
    });

};

getEditMonthlyProgressPopupModalBodyString = function (subChainageGuid, selectedDate) {

    var retValue;

    $.ajax({
        type: "Post",
        async: false,
        url: "SIOProjects.aspx/CreateEditMonthlyProgressPopupModalBodyString",
        data: '{"subChainageGuid":"' + subChainageGuid + '","selectedDate":"' + selectedDate + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your administrator");
        }
    });

    return retValue;
};

createEditMonthlyProgressPopupForm = function (selectedSubChainageName, editMonthlyProgressModalBodyString, subChainageGuid, selectedDate) {
    $("#monthlyProgressPopupModal .modal-header").empty();

    //var html1 = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
    var editMonthlyProgressModalHeaderHtml = '<h1 class="text-center">' + heading + '</h1>';
    editMonthlyProgressModalHeaderHtml += '<h4 class="text-center">SubChainage: ' + selectedSubChainageName + '</h4>';

    $("#monthlyProgressPopupModal .modal-header").append(editMonthlyProgressModalHeaderHtml);

    $("#monthlyProgressPopupModal .modal-body").empty();

    $("#monthlyProgressPopupModal .modal-body").append(editMonthlyProgressModalBodyString);

    //$('#addMonthlyProgressPopupModal').modal('show');

    $('#monthlyProgressPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    $('#editMonthlyProgressPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        //ignore: ':hidden:not(".multiselect")',
        //rules: {
        //    districtList: "required",
        //    divisionList: "required"            
        //},
        //messages: {
        //    districtList: {
        //        required: "Please Enter Project District."
        //    },
        //    divisionList: {
        //        required: "Please Enter Project Division."
        //    }
        //},
        errorClass: 'help-block small has-error',
        validClass: 'help-block has-success',
        errorElement: 'span',
        //success: function (label, element) {
        //    //$(element).closest('.form-group').addClass('help-block has-success');
        //    //label.empty();
        //    //label.append('<i class="fa fa-check-square-o" aria-hidden="true">OK!</i>');
        //    //label.text("Ok!");
        //},
        highlight: function (element, errorClass, validClass) {
            //$(element).closest('.form-group').addClass('has-error');
            $(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            //$(element).closest('.form-group').removeClass('has-error');
            $(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('multiselect')) {
                //error.insertAfter($('#' + element.attr('id')));
                //error.insertBefore(element);
                error.insertAfter(element.parent().find('.btn-group'));
                //$(this).parents("fieldset").find("*:not('legend')").toggle();
            } else {
                //element.closest('.form-group').append(error);
                error.insertAfter(element);
            }
        },
        submitHandler: function () {
            //alert('valid form');
            return false;
        }
    });

    //var fremaaOfficersElement = document.getElementsByClassName("fremaaOfficers");

    //for (var i = 0; i < fremaaOfficersElement.length; i++) {
    //    var x = fremaaOfficersElement[i].getElementsByTagName("input")[1].getAttribute("name");

    //    $('input[name="' + x + '"]').rules('add', {
    //        required: true
    //    });
    //}

    //var pmcSiteEngineersElement = document.getElementsByClassName("pmcSiteEngineers");

    //for (var i = 0; i < pmcSiteEngineersElement.length; i++) {
    //    var x = pmcSiteEngineersElement[i].getElementsByTagName("input")[1].getAttribute("name");

    //    $('input[name="' + x + '"]').rules('add', {
    //        required: true
    //    });
    //}

    var workItemsElement = document.getElementsByClassName("workItems");

    for (var i = 0; i < workItemsElement.length; i++) {
        var x1 = workItemsElement[i].getElementsByTagName("input")[0].getAttribute("id");
        var x2 = workItemsElement[i].getElementsByTagName("input")[1].getAttribute("id");

        var workItemGuid = $('#' + x1).val();
        var workItemName = $('#' + x2 + 'Label').text();

        $('input[name="' + x2 + '"]').rules('add', {
            required: true,
            number: true,
            messages: {
                required: 'This field is required.',
                number: 'Please Enter a Valid value for ' + workItemName + '.'
                //minlength: jQuery.validator.format("Please, at least {0} characters are necessary")
            }
        });

    }

    //$('#addMonthlyProgressPopupModal').on('hide.bs.modal', function () {
    //    //$('#saveProgressBtn').addClass('disabled');
    //    $('#saveNewMonthlyProgressBtn').attr('disabled', 'disabled');
    //});

    //var $input = $('input:text'),
    //saveProgress = $('#saveNewMonthlyProgressBtn');
    //saveProgress.attr('disabled', true);

    //$input.keyup(function () {
    //    var trigger = false;
    //    $input.each(function () {
    //        if (!$(this).val()) {
    //            trigger = true;
    //        }
    //    });
    //    trigger ? saveProgress.attr('disabled', true) : saveProgress.removeAttr('disabled');
    //});

    //Submit button click action
    $('#monthlyProgressPopupModal').on('click', 'a#updateMonthlyProgressBtn', function (e) {
        e.preventDefault();

        if ($('#editMonthlyProgressPopupForm').valid()) {
            $("#preloader").show();
            $("#status").show();

            var progressSaved = saveProjectPhysicalProgress(subChainageGuid, selectedDate);

            if (progressSaved === 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data is saved successfully.", function () {
                    window.location = 'SIOProjects.aspx';
                });
            } else {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data cannot be saved.\nPlease contact your administrator.", function () {
                    window.location = 'SIOProjects.aspx';
                });
            }
        }

    });

    $('#monthlyProgressPopupModal').on('click', 'a#cancelUpdateMonthlyProgressBtn', function (e) {
        e.preventDefault();

        $('#monthlyProgressPopupModal').modal('hide');

        $('#progressMonthPickerPopupModal').modal('show');

        e.stopPropagation();
    });

};


getSubChainageWorkItemMonthlyProgress = function (workItemGuid, subChainageGuid) {

    var retValue;

    $.ajax({
        type: "Post",
        async: false,
        url: "SIOProjects.aspx/GetSubChainageWorkItemMonthlyProgress",
        data: '{"workItemGuid":"' + workItemGuid + '","subChainageGuid":"' + subChainageGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your administrator");
        }
    });

    return retValue;
};


createProgressChartPopupForm = function (sioProjectsTable, selectedRow) {

    var selectedSubChainageGuid = sioProjectsTable.row(selectedRow).data()[1];//SubChainage Guid
    var selectedSubChainageName = sioProjectsTable.row(selectedRow).data()[2];//SubChainage Name

    var selectedSubChainageProjectName = sioProjectsTable.row(selectedRow).data()[3];//Project Name

    var selectedSubChainageStartingDate = sioProjectsTable.row(selectedRow).data()[4];//SubChainage Starting Date
    var selectedSubChainageCompletionDate = sioProjectsTable.row(selectedRow).data()[5];//SubChainage End Date

    var selectedSubChainageUsedDates = sioProjectsTable.row(selectedRow).data()[6];//SubChainage Used Dates
    var serverCurrentDateString = sioProjectsTable.row(selectedRow).data()[7];//Server Current Date

    $("#progressChartPopupModal .modal-header").empty();

    var html = '<button type="button" class="close" data-dismiss="modal">';
    html += '<span aria-hidden="true">&times;</span>';
    html += '<span class="sr-only">Close</span>';
    html += '</button>';
    html += '<h4 class="modal-title text-center" id="myModalLabel">SubChainage: ' + selectedSubChainageName + '</h4>';


    //html += '<h1 class="text-center">' + heading + '</h1>';
    //html += '<h4 class="text-center">SubChainage: ' + selectedSubChainageName + '</h4>';

    $("#progressChartPopupModal .modal-header").append(html);

    
    var chartXAxisCategories = [];
    //var chartYAxisText = "";
    //var selectedWorkItemName = "";
    //var workItemProgressDataSeries = "";

    for (var start = Date.parseExact(selectedSubChainageStartingDate, 'dd/MM/yyyy'), end = Date.parseExact(selectedSubChainageCompletionDate, 'dd/MM/yyyy') ; start < end;) {
        chartXAxisCategories.push(new Date(start).toString('MMMM, yyyy'));

        //var newDate = start.setDate(start.getMonth() + 1);
        var newDate = start.addMonths(1);
        start = new Date(newDate);
    };

    var workItemData = getWorkItemsBasedOnSubChainage(selectedSubChainageGuid);

    showWorkItemOptions('subChainageWorkItemList', workItemData);

    $('#progressChartPopupModal').off().on('change', '#subChainageWorkItemList', function (e) {
        e.preventDefault();

        //if(progressChart){
        //    progressChart.destroy();
        //};

        var selectedWorkItemGuid = $(this).val();

        if (jQuery.Guid.IsValid(selectedWorkItemGuid) && !jQuery.Guid.IsEmpty(selectedWorkItemGuid)) {
            var chartYAxisText = "";

            var selectedWorkItemName = $('#subChainageWorkItemList option:selected').text();

            var workItemProgressDataSeries = getSubChainageWorkItemMonthlyProgress(selectedWorkItemGuid.toLowerCase(), selectedSubChainageGuid);

            for (var i = 0; i < workItemData.length; i++) {
                if (workItemData[i].WorkItemGuid === selectedWorkItemGuid.toLowerCase()) {
                    chartYAxisText = workItemData[i].UnitManager.UnitName + ' (' + workItemData[i].UnitManager.UnitAbbreviation + ')';
                    break;
                }
            }

            $('#progressChartPopupModal #progressChartContainer').removeClass('hidden');

            progressChart = createProgressChart(chartXAxisCategories, chartYAxisText, selectedWorkItemName, workItemProgressDataSeries);

            progressChart.reflow();

        } else {
            $('#progressChartPopupModal #progressChartContainer').addClass('hidden');
        }

    });

    $('#progressChartPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    //$('#progressChartPopupModal').on('show.bs.modal', function () {
    //    $('#progressChartContainer').css('visibility', 'hidden');
    //});
    //$('#progressChartPopupModal').on('shown.bs.modal', function () {
    //    $('#progressChartContainer').css('visibility', 'initial');
    //    progressChart.reflow();
    //});

    $('#progressChartPopupModal').on('hide.bs.modal', function () {
        //progressChart.destroy();
        //progressChart.destroy();
        //progressChart = null;
        //$('#progressChartPopupModal #progressChartContainer').innerHTML = '';
    });
    $('#progressChartPopupModal').on('hidden.bs.modal', function () {
        $('#progressChartPopupModal #progressChartContainer').addClass('hidden');
        //progressChart.destroy();
    });

};

//WORKITEM
getWorkItemsBasedOnSubChainage = function (subChainageGuid) {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "SIOProjects.aspx/GetWorkItemsBasedOnSubChainage",
        data: '{"subChainageGuid":"' + subChainageGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

//Displays Options for WORKITEM
showWorkItemOptions = function (id, arr) {
    var option_str = document.getElementById(id);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select WorkItem--', jQuery.Guid.Empty());
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].WorkItemName, arr[i].WorkItemGuid.toUpperCase());
    }
}


createProgressChart = function (xAxisCategories, yAxisText, workItemName, seriesString) {

    $('#progressChartContainer').highcharts({
        //chart: {
        //    type: 'column'
        //},
        title: {
            text: 'Monthly Progress',
            x: -20 //center
        },
        subtitle: {
            text: workItemName,
            //text: 'Source: WorldClimate.com',
            x: -20
        },
        xAxis: {
            //categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            //  'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            categories: xAxisCategories
        },
        yAxis: {
            min: 0,
            title: {
                //text: 'Temperature (°C)'
                text: yAxisText
            },
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> <br/>',
            shared: true
        },
        //plotOptions: {
        //    column: {
        //        stacking: 'percent'
        //    }
        //},
        series: JSON.parse(seriesString)
        //series: [{
        //    name: 'Tokyo',
        //    data: [7.0, 6.9, 9.5]
        //}]
    });

    var chart = $('#progressChartContainer').highcharts();
    return chart;
}