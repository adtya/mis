﻿var viewSelectedRow = null;
var sioAssetListTable = null;

$(document).ready(function () {
    var selectedDivision;
    var selectedDivisionText;
    var selectedSchemeText;
    var selectedAssetTypeText;

    showDivisionOptions('divisionList', getDivisions());

    //showSchemeOptions('schemeList', getSchemes());

    sioAssetListTable = $('table#assetListForSIO').DataTable({
        //responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: true
            },
            {
                className: 'never',//className:'hidden',
                targets: [4],
                visible: false,
                orderable: true
            },
            {
                targets: [5],
                orderable: true
            },
            {
                className: 'never',//className:'hidden',
                targets: [6],
                visible: false,
                orderable: true
            },
            {
                targets: [7],
                orderable: true
            },
            {
                className: 'never',//className:'hidden',
                targets: [8],
                visible: false,
                orderable: true
            },
            {               
                targets: [9],                
                orderable: true,
            },
            {
                className: 'never',//className:'hidden',
                targets: [10],
                visible: false,
                orderable: true,
                
            },
            {
                className: 'never',//className:'hidden',
                targets: [11],
                visible: false,
                orderable: true,
            },
            {
                className: 'never',//className:'hidden',
                targets: [12],
                visible: false,
                orderable: true,
            },
            {
                className: 'never',//className:'hidden',
                targets: [13],
                visible: false,
                orderable: true,
            },
            {
                className: 'never',//className:'hidden',
                targets: [14],
                visible: false,
                orderable: true,
            },
            {
                className: 'operations',
                targets: [15],
                orderable: false
            }
        ],
        order: [[2, 'desc']],
        language: {
           lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',       
        buttons: [
            {
                text: '+ Create New Asset',
                className: 'btn btn-success addNewAssetBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();
                    window.location = 'CreateSIOAsset.aspx';
                    e.stopPropagation();
                }
            },
            {
                extend: 'print',
                key: {
                    //altKey: true,
                    key: 'p'
                },
                className: 'btn btn-default',
                exportOptions: {
                    columns: [0, 2, 3, 5, 7, 9]
                }
            }
        ]
    });

    sioAssetListTable.on('order.dt search.dt', function () {
        sioAssetListTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
            sioAssetListTable.cell(cell).invalidate('dom');
        });
    }).draw();

    //Division List
    $('#divisionList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Division--',
        minimumResultsForSearch: -1,
        //placeholder: {
        //    id: '', // the value of the option
        //    //text: ''
        //},
        allowClear: true
    }).on('change', function (e) {
        $(this).valid();

        //$("#schemeList").select2("destroy");
        //$('#schemeList').val('');

        $('#schemeList').select2('close');
        $('#assetTypeList').select2('close');

        var selectedDivisionGuid = $(this).val();
        var selectedDivisionText = $('option:selected', this).text();

        if (selectedDivisionGuid === '') {
            showSchemeOptions('schemeList', []);

            sioAssetListTable.column(4).search('').draw();
        } else {
            showSchemeOptions('schemeList', getSchemes(selectedDivisionGuid));

            var selectedDivisionTextSplit = multiSplit(selectedDivisionText, [' (', ')']);

            var val = $.fn.dataTable.util.escapeRegex(selectedDivisionTextSplit[1]);

            sioAssetListTable.column(4).search(val ? '^' + val + '$' : '', true, false).draw();
        }

        $('#schemeList').trigger('change');
        //$('#schemeList').trigger('change.select2');

        //$('#schemeList').select2({
        //    theme: 'bootstrap',
        //    placeholder: '--Select Scheme--',
        //    minimumResultsForSearch: -1,
        //    //placeholder: {
        //    //    id: '', // the value of the option
        //    //    //text: ''
        //    //},
        //    allowClear: true
        //});

    });

    //Scheme List
    $('#schemeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Scheme--',
        minimumResultsForSearch: -1,
        //placeholder: {
        //    id: '', // the value of the option
        //    //text: ''
        //},
        allowClear: true
    }).on('change', function (e) {
        $(this).valid();

        $('#divisionList').select2('close');
        $('#assetTypeList').select2('close');

        var selectedSchemeGuid = $(this).val();
        var selectedSchemeText = $('option:selected', this).text();

        if (selectedSchemeGuid === '') {
            sioAssetListTable.column(6).search('').draw();
        } else {
            var selectedSchemeTextSplit = multiSplit(selectedSchemeText, [' (', ')']);

            var val = $.fn.dataTable.util.escapeRegex(selectedSchemeTextSplit[1]);

            sioAssetListTable.column(6).search(val ? '^' + val + '$' : '', true, false).draw();
        }

    });

    //AssetType List
    $('#assetTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select AssetType--',
        minimumResultsForSearch: -1,
        //placeholder: {
        //    id: '', // the value of the option
        //    //text: ''
        //},
        allowClear: true
    }).on('change', function (e) {
        $(this).valid();

        $('#divisionList').select2('close');
        $('#schemeList').select2('close');

        var selectedAssetTypeGuid = $(this).val();
        var selectedAssetTypeText = $('option:selected', this).text();

        if (selectedAssetTypeGuid === '') {
            sioAssetListTable.column(8).search('').draw();
        } else {
            var selectedAssetTypeTextSplit = multiSplit(selectedAssetTypeText, [' (', ')']);

            var val = $.fn.dataTable.util.escapeRegex(selectedAssetTypeTextSplit[1]);

            sioAssetListTable.column(8).search(val ? '^' + val + '$' : '', true, false).draw();
        }

    });

    $('#dpDateOfInitiationYear').datepicker({
        format: "yyyy",
        minView: 2,
        startView: 2,
        minViewMode: "years",
        maxViewMode: 2,//2-years
    }).on('changeDate', function (ev) {

        var searchInitiationYear = new Date(ev.date.valueOf()).toString('yyyy');

        var val = $.fn.dataTable.util.escapeRegex(searchInitiationYear);

        sioAssetListTable.column(10).search(val ? '^' + val + '$' : '', true, false).draw();

        $(this).blur();
        $(this).datepicker('hide');
    });

    $('#dpDateOfCompletionYear').datepicker({
        format: "yyyy",
        minView: 2,
        startView: 2,
        minViewMode: "years",
        maxViewMode: 2,//2-years
    }).on('changeDate', function (ev) {

        var searchCompletionYear = new Date(ev.date.valueOf()).toString('yyyy');

        var val = $.fn.dataTable.util.escapeRegex(searchCompletionYear);

        sioAssetListTable.column(11).search(val ? '^' + val + '$' : '', true, false).draw();

        $(this).blur();
        $(this).datepicker('hide');
    });

    $('#clearSearchBtn').on('click', function (e) {
        e.preventDefault();

        sioAssetListTable.column(4).search('').draw();
        sioAssetListTable.column(6).search('').draw();
        sioAssetListTable.column(8).search('').draw();
        sioAssetListTable.column(10).search('').draw();
        sioAssetListTable.column(11).search('').draw();

        $('#divisionList').val('').trigger('change');
        $('#assetTypeList').val('').trigger('change');

        $('#dateOfInitiationYear').val('');
        $('#dateOfCompletionYear').val('');

    });
    
    $(document).on('click', '#assetListForSIO a.viewAssetListBtn', function (e) {
        e.preventDefault();

        viewSelectedRow = $(this).parents('tr')[0];

        var assetData = getSIOAssetModalDetails(sioAssetListTable.row(viewSelectedRow).data()[1], sioAssetListTable.row(viewSelectedRow).data()[8]);

        $("#viewSIOAssetPopupModal .modal-body").empty();

        $("#viewSIOAssetPopupModal .modal-body").append(assetData);

        $('#viewSIOAssetPopupModal').modal({
            backdrop: 'static',
            keyboard: false//,
            //show: true
        });

    });

    $(document).on('click', '#assetListForSIO a.editAssetBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        var sessionCreated = setSIOAssetSessionParameters(sioAssetListTable.row(selectedRow).data()[1], sioAssetListTable.row(selectedRow).data()[14], sioAssetListTable.row(selectedRow).data()[12], sioAssetListTable.row(selectedRow).data()[13], sioAssetListTable.row(selectedRow).data()[8]);

        if (sessionCreated === true) {
            window.location = 'EditSIOAsset.aspx';
        }

    });

    $(document).on('click', '#assetListForSIO a.deleteAssetBtn', function (e) {
        e.preventDefault();
    });

    

    $('#printSIOAssetListBtn').on('click', function (e) {
        var assetData = getSIOAssetModalDetails(sioAssetListTable.row(viewSelectedRow).data()[1], sioAssetListTable.row(viewSelectedRow).data()[8]);

        var myWindow = window.open("");

        myWindow.document.write(assetData);

        myWindow.print();
        myWindow.close();

    });

    $('#closeSIOAssetViewBtn').on('click', function (e) {
        e.preventDefault();
        $("#viewSIOAssetPopupModal .modal-body").empty();
    });

});

function setSIOAssetSessionParameters (assetGuid, assetTypeGuid, divisionGuid, schemeGuid, assetTypeCode) {

    $("#preloader").show();
    $("#status").show();

    var result = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "SIOAssets.aspx/SetAssetSessionParameters",
        data: '{"assetGuid":"' + assetGuid + '","assetTypeGuid":"' + assetTypeGuid + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeCode":"' + assetTypeCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            result = true;
            str = response.d;
           
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });

    return result;
}

function getSIOAssetModalDetails (assetGuid, assetTypeCode) {

    var str;
    $.ajax({
        type: "Post",
        async: false,
        url: "SIOAssets.aspx/ViewAssetData",
        data: '{"assetGuid":"' + assetGuid + '","assetTypeCode":"' + assetTypeCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //str = str1;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return str;
}