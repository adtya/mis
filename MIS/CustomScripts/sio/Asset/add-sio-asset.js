﻿var drawingUrl = '';
var regulatorGateTable = null;
var sluiceGateTable = null;
var turnoutGateTable = null;

$(document).ready(function () {
    $('.assetType').hide();

    var codeArray = [];

    var cfgCulture = 'en-IN';
    $.preferCulture(cfgCulture);
    //$.preferCulture("en-IN");

    $('#assetCost').maskMoney();

    showDivisionOptions('divisionList', getDivisions());

    //Division List
    $('#divisionList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Division--',
        minimumResultsForSearch: -1,
        allowClear: true
    }).on('change', function (e) {
        //$(this).valid();

        $('#schemeList').select2('close');
        $('#assetTypeList').select2('close');
        $('#assetCodeNumberSelect').select2('close');

        var selectedDivisionGuid = $(this).val();
        var selectedDivisionText = $('option:selected', this).text();

        codeArray[0] = '';

        if (selectedDivisionGuid === '') {
            showSchemeOptions('schemeList', []);

            $('#select2-divisionList-container').attr("title", '');

            $('#schemeList').prop('disabled', true);
            //$('#assetTypeList').prop('disabled', true);
            //$('#assetCodeNumberSelect').prop('disabled', true);
        } else {
            showSchemeOptions('schemeList', getSchemes(selectedDivisionGuid));

            var selectedDivisionTextSplit = multiSplit(selectedDivisionText, [' (', ')']);

            codeArray[0] = selectedDivisionTextSplit[1];

            $('#select2-divisionList-container').attr("title", selectedDivisionText);

            $('#schemeList').prop('disabled', false);
        }

        //displayAssetCode(codeArray);

        $('#schemeList').trigger('change');

    });

    //Scheme List
    $('#schemeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Scheme--',
        minimumResultsForSearch: -1,
        disabled: true,
        allowClear: true
    }).on('change', function (e) {
        //$(this).valid();

        $('#divisionList').select2('close');
        $('#assetTypeList').select2('close');
        $('#assetCodeNumberSelect').select2('close');

        var selectedSchemeGuid = $(this).val();
        var selectedSchemeText = $('option:selected', this).text();

        codeArray[1] = '';

        if (selectedSchemeGuid === '') {
            //$('#assetTypeList').val('');
            $('#select2-schemeList-container').attr("title", '');

            $('#assetTypeList').prop('disabled', true);
        } else {
            var selectedSchemeTextSplit = multiSplit(selectedSchemeText, [' (', ')']);

            codeArray[1] = selectedSchemeTextSplit[1];

            $('#select2-schemeList-container').attr("title", selectedSchemeText);

            $('#assetTypeList').prop('disabled', false);
        }

        $('#assetTypeList').val('').trigger('change');

        //displayAssetCode(codeArray);

    });

    //AssetType List
    $('#assetTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select AssetType--',
        minimumResultsForSearch: -1,
        disabled: true,
        allowClear: true
    }).on('change', function (e) {
        //$(this).valid();

        $('#divisionList').select2('close');
        $('#schemeList').select2('close');
        $('#assetCodeNumberSelect').select2('close');

        var selectedAssetTypeGuid = $(this).val();
        var selectedAssetTypeText = $('option:selected', this).text();

        codeArray[2] = '';

        if (selectedAssetTypeGuid === '') {
            showAssetNumberOptions('assetCodeNumberSelect', [], false);

            showKeyAssets('keyAssetSelect', []);

            $('#showKeyAssetSelect').hide();

            $('#select2-assetTypeList-container').attr("title", '');

            $('#assetCodeNumberSelect').prop('disabled', true);

            $('.assetType').hide();
        } else {
            showAssetNumberOptions('assetCodeNumberSelect', getAssetNumberOptions(selectedAssetTypeGuid, $('#schemeList').val()), true);

            var selectedAssetTypeTextSplit = multiSplit(selectedAssetTypeText, [' (', ')']);

            codeArray[2] = selectedAssetTypeTextSplit[1];

            var arr = getKeyAssets(selectedAssetTypeGuid, $('#divisionList').val(), $('#schemeList').val());

            $('#select2-assetTypeList-container').attr("title", selectedAssetTypeText);

            $('#assetCodeNumberSelect').prop('disabled', false);

            if (arr.length == 0) {
                showKeyAssets('keyAssetSelect', []);

                $('#showKeyAssetSelect').hide();
            } else {
                showKeyAssets('keyAssetSelect', getKeyAssets(selectedAssetTypeGuid, $('#divisionList').val(), $('#schemeList').val()));

                $('#showKeyAssetSelect').show();
            }

            displayAssetTechnicalSpecification(selectedAssetTypeTextSplit[1]);

        }

        $('#assetCodeNumberSelect').trigger('change');

        $('#keyAssetSelect').trigger('change');

        //displayAssetCode(codeArray);

    });

    //Asset Number List
    $('#assetCodeNumberSelect').select2({
        theme: 'bootstrap',
        placeholder: '--Select Asset Number--',
        minimumResultsForSearch: -1,
        disabled: true,
        allowClear: true
    }).on('change', function (e) {
        //$(this).valid();

        var selectedAssetNumberValue = $(this).val();
        var selectedAssetNumberText = $('option:selected', this).text();

        codeArray[3] = selectedAssetNumberValue;

        if (selectedAssetNumberValue === '') {
            $('#select2-assetCodeNumberSelect-container').attr("title", '');
        } else {
            $('#select2-assetCodeNumberSelect-container').attr("title", selectedAssetNumberValue);
        }

        displayAssetCode(codeArray);

    });

    //Key Asset List
    $('#keyAssetSelect').select2({
        theme: 'bootstrap',
        placeholder: '--Select Key Asset--',
        minimumResultsForSearch: -1,
        //disabled: true,
        allowClear: true
    }).on('change', function (e) {
        //$(this).valid();

        var selectedKeyAssetGuid = $(this).val();
        var selectedKeyAssetText = $('option:selected', this).text();

    });

    $('#uploadDrawing').click(function (e) {
        e.preventDefault();
        $('#drawingCode1').click();
    });

    $('#drawingCode1').change(function (e) {
        e.preventDefault();
        var filePath = $('#drawingCode1').val();
        var fileName = filePath.split('\\');
        $('#drawingCode').val(fileName[fileName.length - 1]);
    });

    $('#dpDateOfInitiationOfAsset').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var dateOfCompletionOfAssetStartDate = new Date(ev.date.valueOf());
        dateOfCompletionOfAssetStartDate = dateOfCompletionOfAssetStartDate.add(1).days();
        $('#dpDateOfCompletionOfAsset').datepicker('setStartDate', dateOfCompletionOfAssetStartDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpDateOfCompletionOfAsset').datepicker('setStartDate', null);
    });

    $('#dpDateOfCompletionOfAsset').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var dateOfInitiationOfAssetEndDate = new Date(ev.date.valueOf());
        dateOfInitiationOfAssetEndDate = dateOfInitiationOfAssetEndDate.add(-1).days();
        $('#dpDateOfInitiationOfAsset').datepicker('setEndDate', dateOfInitiationOfAssetEndDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpDateOfInitiationOfAsset').datepicker('setEndDate', null);
    });

    //Bridge
    getBridgeType();

    $('#bridgeTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Bridge Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    //Canal
    getCanalLiningType();

    $('#canalLiningTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Lining Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    //Culvert
    getCulvertType();

    $('#culvertTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Culvert Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    //Embankment
    getEmbankmentCrestType();
    getEmbankmentFillType();

    $('#embankmentCrestTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Crest Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    $('#embankmentFillTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Fill Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    //Gauge
    getGaugeType();
    getGaugeSeasonType();
    getGaugeFrequencyType();

    $('#gaugeTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Gauge Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    $('#gaugeSeasonTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Season Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    $('#gaugeFrequencyTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Frequency Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    $('#dpGaugeStartDate').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var gaugeEndDateStartDate = new Date(ev.date.valueOf());
        gaugeEndDateStartDate = gaugeEndDateStartDate.add(1).days();
        $('#dpGaugeEndDate').datepicker('setStartDate', gaugeEndDateStartDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpGaugeEndDate').datepicker('setStartDate', null);
    });

    $('#dpGaugeEndDate').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var gaugeStartDateEndDate = new Date(ev.date.valueOf());
        gaugeStartDateEndDate = gaugeStartDateEndDate.add(-1).days();
        $('#dpGaugeStartDate').datepicker('setEndDate', gaugeStartDateEndDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpGaugeStartDate').datepicker('setEndDate', null);
    });

    //Porcupine
    getPorcupineType();
    getPorcupineMaterialType();

    $('#porcupineTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Porcupine Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    $('#porcupineMaterialTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Material Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    //Regulator
    getRegulatorType();
    getRegulatorGateType();

    $('#regulatorTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Regulator Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    $('#regulatorGateTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Gate Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    //Revetment
    getRevetmentType();
    getRevetmentRiverProtectionType();
    getRevetmentWaveProtectionType();

    $('#revetmentTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Revetment Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    $('#revetmentRiverProtectionTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select River Protection Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    $('#revetmentWaveProtectionTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Wave Protection Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    //Sluice
    getSluiceType();

    $('#sluiceTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Sluice Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    //Spur
    getSpurShapeType();
    getSpurConstructionType();
    getSpurRevetType();

    $('#spurShapeTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Shape Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    $('#spurConstructionTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Construction Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    $('#spurRevetTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Revet Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    //Turnout
    getTurnoutType();
    getTurnoutGateType();

    $('#turnoutTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Turnout Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    $('#turnoutGateTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Gate Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    //Weir
    getWeirType();

    $('#weirTypeList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Weir Type--',
        minimumResultsForSearch: -1,
        allowClear: true
    });

    $('#saveSIOAssetBtn').on('click', function (e) {
        e.preventDefault();

        if ($('#addSIOAssetForm').valid()) {
            $("#preloader").show();
            $("#status").show();

            var fileUploaded = uploadAssetDrawing(codeArray);

            if (fileUploaded[0]) {
                var assetSaved = saveAsset(codeArray[2]);

                if (assetSaved === 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    //bootbox.alert("Asset is saved successfully.", function () {
                    //    window.location = 'SIOAssets.aspx';
                    //});

                    bootbox.confirm({
                        title: '',
                        message: 'Asset is saved successfully. Do you want to Add Another Asset?',
                        buttons: {
                            'cancel': {
                                label: 'No',
                                className: 'btn-default pull-left'
                            },
                            'confirm': {
                                label: 'Yes',
                                className: 'btn-default pull-right'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                $('#addSIOAssetForm').trigger('reset');
                                $('.select2').trigger('change');
                            } else {
                                window.location = 'SIOAssets.aspx';
                            }
                        }
                    });
                } else {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Data cannot be Saved.");
                }
            }
            else {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert(fileUploaded[1]);
            }
        }

    });

    $('#cancelSIOAssetBtn').click(function (e) {
        e.preventDefault();
        window.location = 'SIOAssets.aspx';
    });

    $('#addSIOAssetForm').validate({ // initialize plugin
        //ignore: ":not(:visible)",
        ignore: ':hidden:not(".multiselect")',
        rules: {
            divisionList: "required",
            schemeList: "required",
            assetTypeList: "required",
            assetCodeNumberSelect: "required",
            //assetCode: "required",
            //keyAssetSelect: "required",
            //drawingCode: "required",
            amtd: {
                number: true
            },
            assetName: {
                required: true,
                noSpace: true
            },
            //assetCost: "required",
            chainageStart: {
                number: true
            },
            chainageEnd: {
                number: true
            },
            eastingUS: {
                number: true
            },
            eastingDS: {
                number: true
            },
            northingUS: {
                number: true
            },
            northingDS: {
                number: true
            },
            dateOfInitiationOfAsset: {
                required: true,
                dateITA: true
            },
            dateOfCompletionOfAsset: {
                required: true,
                dateITA: true
            },

            //Bridge
            bridgeTypeList: "required",
            bridgeLength: {
                number: true
            },
            bridgeNumberOfPiers: {
                number: true
            },
            bridgeRoadWidth: {
                number: true
            },

            //Canal
            canalUsCrestElevation: {
                number: true
            },
            canalDsCrestElevation: {
                number: true
            },
            canalLength: {
                number: true
            },
            canalBedWidth: {
                number: true
            },
            canalSlipeSlope1: {
                number: true
            },
            canalSlipeSlope2: {
                number: true
            },
            canalAverageDepth: {
                number: true
            },
            canalLiningTypeList: "required",

            //Culvert
            culvertTypeList: "required",
            culvertNumberOfVents: {
                number: true
            },
            culvertVentHeight: {
                number: true
            },
            culvertVentWidth: {
                number: true
            },
            culvertLength: {
                number: true
            },

            //Drainage
            drainageUsBedElevation: {
                number: true
            },
            drainageDsBedElevation: {
                number: true
            },
            drainageLength: {
                number: true
            },
            drainageBedWidth: {
                number: true
            },
            drainageSlope1: {
                number: true
            },
            drainageSlope2: {
                number: true
            },
            drainageAverageDepth: {
                number: true
            },

            //Drop Structure
            dropStructureUsInvertLevel: {
                number: true
            },
            dropStructureDsInvertLevel: {
                number: true
            },
            dropStructureStillingBasinLength: {
                number: true
            },
            dropStructureStillingBasinWidth: {
                number: true
            },

            //Embankment
            embankmentUsCrestElevation: {
                number: true
            },
            embankmentDsCrestElevation: {
                number: true
            },
            embankmentLength: {
                number: true
            },
            embankmentCrestWidth: {
                number: true
            },
            embankmentCsSlope1: {
                number: true
            },
            embankmentCsSlope2: {
                number: true
            },
            embankmentRsSlope1: {
                number: true
            },
            embankmentRsSlope2: {
                number: true
            },
            embankmentBermSlope1: {
                number: true
            },
            embankmentBermSlope2: {
                number: true
            },
            embankmentPlatformWidth: {
                number: true
            },
            embankmentCrestTypeList: "required",
            embankmentFillTypeList: "required",
            embankmentAverageHeight: {
                number: true
            },

            //Gauge
            gaugeTypeList: "required",
            gaugeSeasonTypeList: "required",
            gaugeFrequencyTypeList: "required",
            gaugeZeroDatum: {
                number: true
            },
            gaugeStartDate: {
                //required: true,
                dateITA: true
            },
            gaugeEndDate: {
                //required: true,
                dateITA: true
            },
            //gaugeActive: "required",
            //gaugeLwlPresent: "required",
            //gaugeHwlPresent: "required",
            //gaugeLevelGeo: "required",

            //Porcupine
            porcupineTypeList: "required",
            porcupineMaterialTypeList: "required",
            porcupineScreenLength: {
                number: true
            },
            porcupineSpacingAlongScreen: {
                number: true
            },
            porcupineScreenRows: {
                number: true
            },
            porcupineNumberOfLayers: {
                number: true
            },
            porcupineMamberLength: {
                number: true
            },
            porcupineLength: {
                number: true
            },

            //Regulator
            regulatorNumberOfVents: {
                number: true
            },
            regulatorVentHeight: {
                number: true
            },
            regulatorVentWidth: {
                number: true
            },
            regulatorTypeList: "required",
            regulatorGateTypeList: "required",

            //Revetment
            revetmentTypeList: "required",
            revetmentRiverProtectionTypeList: "required",
            revetmentWaveProtectionTypeList: "required",
            revetmentSlope: {
                number: true
            },
            revetmentLength: {
                number: true
            },
            revetmentPlainWidth: {
                number: true
            },

            //Sluice
            sluiceTypeList: "required",
            sluiceNumberOfVents: {
                number: true
            },
            sluiceVentDiameter: {
                number: true
            },
            sluiceVentWidth: {
                number: true
            },
            sluiceVentHeight: {
                number: true
            },

            //Spur
            spurOrientation: {
                number: true
            },
            spurLength: {
                number: true
            },
            spurWidth: {
                number: true
            },
            spurShapeTypeList: "required",
            spurConstructionTypeList: "required",
            spurRevetTypeList: "required",

            //Turnout
            turnoutInletBoxWidth: {
                number: true
            },
            turnoutInletBoxLength: {
                number: true
            },
            turnoutOutletNumber: {
                number: true
            },
            turnoutOutletWidth: {
                number: true
            },
            turnoutOutletHeight: {
                number: true
            },
            turnoutTypeList: "required",
            turnoutGateTypeList: "required",

            //Weir
            weirInvertLevel: {
                number: true
            },
            weirCrestTopLevel: {
                number: true
            },
            weirCrestTopWidth: {
                number: true
            },
            weirTypeList: "required",

            //Regulator Gate
            regulatorGatesGateTypeList: "required",
            regulatorGatesGateConstructionMaterialTypeList: "required",
            regulatorGatesGateNumbers: {
                number: true
            },
            regulatorGatesGateHeight: {
                number: true
            },
            regulatorGatesGateWidth: {
                number: true
            },
            regulatorGatesGateDiameter: {
                number: true
            },
            regulatorGatesGateInstallationDate: {
                //required: true,
                dateITA: true
            },

            //Sluice Gate
            sluiceGatesGateTypeList: "required",
            sluiceGatesGateConstructionMaterialTypeList: "required",
            sluiceGatesGateNumbers: {
                number: true
            },
            sluiceGatesGateHeight: {
                number: true
            },
            sluiceGatesGateWidth: {
                number: true
            },
            sluiceGatesGateDiameter: {
                number: true
            },
            sluiceGatesGateInstallationDate: {
                //required: true,
                dateITA: true
            },

            //Turnout Gate
            turnoutGatesGateTypeList: "required",
            turnoutGatesGateConstructionMaterialTypeList: "required",
            turnoutGatesGateNumbers: {
                number: true
            },
            turnoutGatesGateHeight: {
                number: true
            },
            turnoutGatesGateWidth: {
                number: true
            },
            turnoutGatesGateDiameter: {
                number: true
            },
            turnoutGatesGateInstallationDate: {
                //required: true,
                dateITA: true
            },
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block small',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('multiselect')) {
                //error.insertAfter($('#' + element.attr('id')));
                //error.insertBefore(element);
                error.insertAfter(element.parent().find('.btn-group'));
                //$(this).parents("fieldset").find("*:not('legend')").toggle();
            } else if (element.hasClass('select2')) {
                //error.insertAfter($('#' + element.attr('id')));
                //error.insertBefore(element);
                error.insertAfter(element.parent().find('.select2-container'));
                //$(this).parents("fieldset").find("*:not('legend')").toggle();
            } else {
                //element.closest('.form-group').append(error);
                error.insertAfter(element);
            }
        },
        submitHandler: function () {
            //alert('valid form');
            return false;
        }
    });

});

function getKeyAssets(assetTypeGuid, divisionGuid, schemeGuid) {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "CreateSIOAsset.aspx/DisplayKeyAssets",
        data: '{"assetTypeGuid":"' + assetTypeGuid + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

function showKeyAssets(id, arr) {
    var option_str = document.getElementById(id);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Key Asset--', jQuery.Guid.Empty());
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].AssetCode, arr[i].AssetGuid);
    }
}

function getAssetNumberOptions(assetTypeGuid, schemeGuid) {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/GetAssetNumbers",
        data: '{"assetTypeGuid":"' + assetTypeGuid + '","schemeGuid":"' + schemeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
};

function showAssetNumberOptions(id, arr, valid) {
    var option_str = document.getElementById(id);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select  Asset Number--', '');
    option_str.selectedIndex = 0;
    if (valid) {
        for (var i = 1; i <= 100 + arr.length; i++) {
            if (arr.indexOf(i) > -1) {

            } else {
                option_str.options[option_str.length] = new Option(i, i);
            }
        }
    }
};

function displayAssetCode(assetCodeArray) {
    var code = '';

    for (var i = 0; i < assetCodeArray.length; i++) {
        if (assetCodeArray[i] !== '') {
            code += assetCodeArray[i];
            if (i < 3) {
                code += '-';
            }
        }
    }
    



    //if (assetCodeArray.length === 4) {
    //    for (var i = 0; i < assetCodeArray.length - 1; i++) {
    //        code += assetCodeArray[i] + '-';
    //    }
    //    code += assetCodeArray[assetCodeArray.length - 1];
    //} else {
    //    for (var i = 0; i < assetCodeArray.length; i++) {
    //        if (assetCodeArray[i] !== '') {
    //            code += assetCodeArray[i] + '-';
    //        }
    //    }
    //}

    //$('#assetCode').val(assetCodeArray.join('-'));

    $('#assetCode').val(code);

}

function displayAssetTechnicalSpecification(assetTypeCode) {

    $('.assetType').hide();

    if (regulatorGateTable != null) {
        regulatorGateTable.clear().draw();
    }

    if (sluiceGateTable != null) {
        sluiceGateTable.clear().draw();
    }

    if (turnoutGateTable != null) {
        turnoutGateTable.clear().draw();
    }

    if (assetTypeCode === 'BRG') {
        $('#bridgeAssetTypeDiv').show();
    } else if (assetTypeCode === 'CAN') {
        $('#canalAssetTypeDiv').show();
    } else if (assetTypeCode === 'CUL') {
        $('#culvertAssetTypeDiv').show();
    } else if (assetTypeCode === 'DRN') {
        $('#drainageAssetTypeDiv').show();
    } else if (assetTypeCode === 'DRP') {
        $('#dropStructureAssetTypeDiv').show();
    } else if (assetTypeCode === 'EMB') {
        $('#embankmentAssetTypeDiv').show();
    } else if (assetTypeCode === 'GAU') {
        $('#gaugeAssetTypeDiv').show();
    } else if (assetTypeCode === 'POR') {
        $('#porcupineAssetTypeDiv').show();
    } else if (assetTypeCode === 'REG') {
        $('#regulatorAssetTypeDiv').show();
        regulatorGatesTableRelatedFunctions();
    } else if (assetTypeCode === 'REV') {
        $('#revetmentAssetTypeDiv').show();
    } else if (assetTypeCode === 'SLU') {
        $('#sluiceAssetTypeDiv').show();
        sluiceGatesTableRelatedFunctions();
    } else if (assetTypeCode === 'SPU') {
        $('#spurAssetTypeDiv').show();
    } else if (assetTypeCode === 'TRN') {
        $('#turnoutAssetTypeDiv').show();
        turnoutGatesTableRelatedFunctions();
    } else if (assetTypeCode === 'WEI') {
        $('#weirAssetTypeDiv').show();
    } else {
        $('.assetType').hide();
    }
}

//gatesBasedOnAssetTypeTableFunctions = function (id) {
//    gateTable = $('#' + id).DataTable({
//        responsive: true,
//        paging: false,
//        info: false,       
//        searching: false,
//        columnDefs: [
//            {
//                targets: [0],
//                orderable: false,
//                searchable: false
//            },
//            {
//                className: 'never',//className:'hidden',
//                targets: [1],
//                visible: false,
//                orderable: false,
//                searchable: false
//            },
//            {
//                className: 'never',//className:'hidden',
//                targets: [2],
//                visible: false,
//                orderable: false,
//                searchable: false
//            }
//        ],
//        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
//        buttons: [
//            {
//                text: '+ Add New Gate Type',
//                className: 'btn-success addNewGateTypeBtn',               
//                action: function (e, dt, node, config) {
//                    e.preventDefault();
//                    //var gateTable = $('#gatesBasedOnSluiceList').DataTable();                 
                                      
//                    gateTable.row.add([
//                          '',
//                          '<input id="gateTypeGuid" name="gateTypeGuid" class="form-control gateTypeGuid" type="text"/>',
//                          '<input id="gateConstructionMaterialGuid" name="gateConstructionMaterialGuid" class="form-control gateConstructionMaterialGuid" type="text"/>',
//                          '<select id="gateTypeSelect"  name="gateTypeSelect" class="form-control gateTypeSelect"></select>',
//                          '<select id="gateConstructionMaterialSelect"  name="gateConstructionMaterialSelect" class="form-control gateConstructionMaterialSelect"></select>',
//                          '<input id="numberOfGates" name="numberOfGates" class="form-control numberOfGates" type="text" size="3"/>',
//                          '<input id="height" name="height" class="form-control height" type="text" size="3"/>',
//                          '<input id="width" name="width" class="form-control width" type="text" size="3"/>',
//                          '<input id="diameter" name="diameter" class="form-control diameter" type="text" size="3"/>',
//                          '<div id=\"dpInstallDate\" class=\"input-group date\"><input type=\"text\" name=\"installDate\" class=\"form-control\" id=\"installDate\" placeholder=\"Install Date (dd/mm/yyyy)\" /><span class=\"input-group-addon form-control-static\"><i class=\"fa fa-calendar fa-fw\"></i></span></div>',
//                          '<a href="#" class="saveGateBasedOnSluiceBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelGateBasedOnSluiceBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>'
//                    ]).draw(false);

//                    getGateType();
//                    getGateConstructionMaterial();

//                    $('#dpInstallDate').datepicker({
//                        format: "dd/mm/yyyy",
//                    });

//                    gateTable.button(['.addNewGateTypeBtn']).disable();

//                }
//            }
//        ]
//    });


//    gateTable.on('order.dt search.dt', function () {
//        gateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
//            cell.innerHTML = i + 1;
//        });
//    }).draw();

//    $('#assetTypePanel table#gatesBasedOnAssetTypeList').on('click', 'a.saveGateBasedOnSluiceBtn', function (e) {
//        e.preventDefault();
//        var gateTable = $('#gatesBasedOnAssetTypeList').DataTable();

//        var selectedRow = $(this).parents('tr')[0];

//        saveNewGateRow(gateTable,selectedRow);

//        gateTable.button(['.addNewGateTypeBtn']).enable();

//    });

//    $('#assetTypePanel table#gatesBasedOnAssetTypeList').on('click', 'a.deleteGateTypeBtn', function (e) {
//        e.preventDefault();

//        var selectedRow = $(this).parents('tr')[0];

//        gateTable.row(selectedRow).remove().draw(false);

//        gateTable.button(['.addNewGateTypeBtn']).enable();

//    });

//    $('#assetTypePanel table#gatesBasedOnAssetTypeList').on('click', 'a.cancelGateBasedOnSluiceBtn', function (e) {
//        e.preventDefault();        

//        var selectedRow = $(this).parents('tr')[0];       

//        gateTable.row(selectedRow).remove().draw(false);

//        gateTable.button(['.addNewGateTypeBtn']).enable();

//    });

//}

//saveNewGateRow = function (gateTable, selectedRow) {

//    var selectedRowData = gateTable.row(selectedRow).data();

//    var availableSelects = $('select', selectedRow);
//    var availableInputs = $('input', selectedRow);
   
//    selectedRowData[1] = availableSelects[0].value
//    selectedRowData[2] = availableSelects[1].value
//    selectedRowData[3] = availableSelects[0].options[availableSelects[0].selectedIndex].text
//    selectedRowData[4] = availableSelects[1].options[availableSelects[1].selectedIndex].text
//    selectedRowData[5] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
//    selectedRowData[6] = capitalizeFirstAllWords($.trim(availableInputs[1].value));
//    selectedRowData[7] = capitalizeFirstAllWords($.trim(availableInputs[2].value));
//    selectedRowData[8] = capitalizeFirstAllWords($.trim(availableInputs[3].value));
//    selectedRowData[9] = capitalizeFirstAllWords($.trim(availableInputs[4].value));
//    selectedRowData[10] = '<a href="#" class="deleteGateTypeBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

//    gateTable.row(selectedRow).data(selectedRowData);

//    gateTable.on('order.dt search.dt', function () {
//        gateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
//            cell.innerHTML = i + 1;
//        });
//    }).draw();
//}


//Bridge

function getBridgeType () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetBridgeType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showBridgeType('bridgeTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showBridgeType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Bridge Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].BridgeTypeName, arr[i].BridgeTypeGuid);
    }
}


//Canal

function getCanalLiningType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetCanalLiningType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showCanalLiningType('canalLiningTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showCanalLiningType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Lining Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].LiningTypeName, arr[i].LiningTypeGuid);
    }
}


//Culvert

function getCulvertType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetCulvertType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showCulvertType('culvertTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showCulvertType(selectId, arr) {
    var option_str_culvert = document.getElementById(selectId);
    option_str_culvert.length = 0;
    option_str_culvert.options[0] = new Option('--Select Culvert Type--', '');
    option_str_culvert.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_culvert.options[option_str_culvert.length] = new Option(arr[i].CulvertTypeName, arr[i].CulvertTypeGuid);
    }
}


//Embankment

function getEmbankmentCrestType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetEmbankmentCrestType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showEmbankmentCrestType('embankmentCrestTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showEmbankmentCrestType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Crest Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].CrestTypeName, arr[i].CrestTypeGuid);
    }
}

function getEmbankmentFillType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetEmbankmentFillType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showEmbankmentFillType('embankmentFillTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showEmbankmentFillType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Fill Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].FillTypeName, arr[i].FillTypeGuid);
    }
}


//Gauge

function getGaugeType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetGaugeType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showGaugeType('gaugeTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showGaugeType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Gauge Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].GaugeTypeName, arr[i].GaugeTypeGuid);
    }
}

function getGaugeSeasonType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetGaugeSeasonType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showGaugeSeasonType('gaugeSeasonTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showGaugeSeasonType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Season Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].SeasonTypeName, arr[i].SeasonTypeGuid);
    }
}

function getGaugeFrequencyType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetGaugeFrequencyType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showGaugeFrequencyType('gaugeFrequencyTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showGaugeFrequencyType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Frequency Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].FrequencyTypeName, arr[i].FrequencyTypeGuid);
    }
}


//Porcupine

function getPorcupineType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetPorcupineType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showPorcupineType('porcupineTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showPorcupineType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Porcupine Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].PorcupineTypeName, arr[i].PorcupineTypeGuid);
    }
}

function getPorcupineMaterialType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetPorcupineMaterialType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showPorcupineMaterialType('porcupineMaterialTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showPorcupineMaterialType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Material Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].PorcupineMaterialTypeName, arr[i].PorcupineMaterialTypeGuid);
    }
}


//Regulator

function getRegulatorType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetRegulatorType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showRegulatorType('regulatorTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showRegulatorType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Regulator Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].RegulatorTypeName, arr[i].RegulatorTypeGuid);
    }
}

function getRegulatorGateType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetRegulatorGateType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showRegulatorGateType('regulatorGateTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showRegulatorGateType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Gate Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].RegulatorGateTypeName, arr[i].RegulatorGateTypeGuid);
    }
}


//Revetment

function getRevetmentType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetRevetmentType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showRevetmentType('revetmentTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showRevetmentType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Revetment Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].RevetTypeName, arr[i].RevetTypeGuid);
    }
}

function getRevetmentRiverProtectionType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetRevetmentRiverProtectionType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showRevetmentRiverProtectionType('revetmentRiverProtectionTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showRevetmentRiverProtectionType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select River Protection Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].RiverprotTypeName, arr[i].RiverprotTypeGuid);
    }
}

function getRevetmentWaveProtectionType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetRevetmentWaveProtectionType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showRevetmentWaveProtectionType('revetmentWaveProtectionTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showRevetmentWaveProtectionType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Wave Protection Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].WaveprotTypeName, arr[i].WaveprotTypeGuid);
    }
}

//Sluice

function getSluiceType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetSluiceType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showSluiceType('sluiceTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showSluiceType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Sluice Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].SluiceTypeName, arr[i].SluiceTypeGuid);
    }
}


//Spur

function getSpurShapeType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetSpurShapeType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showSpurShapeType('spurShapeTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showSpurShapeType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Shape Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].ShapeTypeName, arr[i].ShapeTypeGuid);
    }
}

function getSpurConstructionType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetSpurConstructionType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showSpurConstructionType('spurConstructionTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showSpurConstructionType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Construction Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].ConstructionTypeName, arr[i].ConstructionTypeGuid);
    }
}

function getSpurRevetType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetSpurRevetType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showSpurRevetType('spurRevetTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showSpurRevetType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Revet Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].SpurRevetTypeName, arr[i].SpurRevetTypeGuid);
    }
}


//Turnout

function getTurnoutType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetTurnoutType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showTurnoutType('turnoutTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showTurnoutType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Turnout Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].TurnoutTypeName, arr[i].TurnoutTypeGuid);
    }
}

function getTurnoutGateType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetTurnoutGateType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showTurnoutGateType('turnoutGateTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showTurnoutGateType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Gate Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].TurnoutGateTypeName, arr[i].TurnoutGateTypeGuid);
    }
}


//Weir

function getWeirType() {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetWeirType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showWeirType('weirTypeList', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showWeirType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Weir Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].WeirTypeName, arr[i].WeirTypeGuid);
    }
}


//Gate

function regulatorGatesTableRelatedFunctions() {
    //var gateEditing = null;

    var tableId = $('#regulatorAssetTypeDiv table#regulatorGatesTable');

    regulatorGateTable = tableId.DataTable({
        destroy: true,
        paging: false,
        info: false,
        searching: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [2],
                visible: false,
                orderable: false,
                searchable: false
            }
        ],
        order: [[3, 'asc']],
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        buttons: [
            {
                text: '+ Add New Gate',
                className: 'btn-success addNewRegulatorGateBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewRegulatorGate();

                    e.stopPropagation();
                }
            }
        ]
    });


    regulatorGateTable.on('order.dt search.dt', function () {
        regulatorGateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addRegulatorGateBtn = regulatorGateTable.button(['.addNewRegulatorGateBtn']);

    function addNewRegulatorGate() {

        //if (gateEditing !== null) {
        //    /* A different row is being edited - the edit should be cancelled and this row edited */
        //    restoreGateRowData(gateTable, gateEditing);
        //    gateEditing = null;
        //    addGateBtn.enable();
        //}

        var newGateRow = regulatorGateTable.row.add([
            '',
            '',
            '',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><select class="form-control select1" name="regulatorGatesGateTypeList" id="regulatorGatesGateTypeList" style="width:100%;"></select></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><select class="form-control select1" name="regulatorGatesGateConstructionMaterialTypeList" id="regulatorGatesGateConstructionMaterialTypeList" style="width:100%;"></select></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="regulatorGatesGateNumbers" name="regulatorGatesGateNumbers" placeholder="Number of Gates" class="form-control numberOfGates" style="width:100%;" /></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="regulatorGatesGateHeight" name="regulatorGatesGateHeight" placeholder="Height" class="form-control height"  style="width:100%;" /></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="regulatorGatesGateWidth" name="regulatorGatesGateWidth" placeholder="Width" class="form-control width"  style="width:100%;" /></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="regulatorGatesGateDiameter" name="regulatorGatesGateDiameter" placeholder="Diameter" class="form-control diameter" style="width:100%;" /></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div id="dpRegulatorGatesGateInstallationDate" class="input-group date"><input type="text" name="regulatorGatesGateInstallationDate" id="regulatorGatesGateInstallationDate" class="form-control" placeholder="Installation Date (dd/mm/yyyy)" /><span class="input-group-addon form-control-static"><i class="fa fa-calendar fa-fw"></i></span></div></div></div>',
            '<a href="#" class="saveRegulatorGateBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelRegulatorGateSaveBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>'
        ]).draw(false);

        getGateType('regulatorGatesGateTypeList');
        getGateConstructionMaterialType('regulatorGatesGateConstructionMaterialTypeList');

        $('#dpRegulatorGatesGateInstallationDate').datepicker({
            format: "dd/mm/yyyy",
            clearBtn: true,
            autoclose: true,
            todayHighlight: true
        });

        addRegulatorGateBtn.disable();
    }

    tableId.on('click', 'a.saveRegulatorGateBtn', function (e) {
        e.preventDefault();

        if ($('select[name="regulatorGatesGateTypeList"], select[name="regulatorGatesGateConstructionMaterialTypeList"], input[name="regulatorGatesGateNumbers"], input[name="regulatorGatesGateHeight"], input[name="regulatorGatesGateWidth"], input[name="regulatorGatesGateDiameter"], input[name="regulatorGatesGateInstallationDate"]').valid()) {
            var selectedRow = $(this).parents('tr')[0];

            saveNewGateRow(regulatorGateTable, selectedRow, 'deleteRegulatorGateBtn');

            regulatorGateTable.button(['.addNewGateBtn']).enable();
        }

    });

    tableId.on('click', 'a.deleteRegulatorGateBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        regulatorGateTable.row(selectedRow).remove().draw(false);

        addRegulatorGateBtn.enable();

    });

    tableId.on('click', 'a.cancelRegulatorGateSaveBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        regulatorGateTable.row(selectedRow).remove().draw(false);

        addRegulatorGateBtn.enable();

    });

}

function sluiceGatesTableRelatedFunctions() {
    //var gateEditing = null;

    var tableId = $('#sluiceAssetTypeDiv table#sluiceGatesTable');

    sluiceGateTable = tableId.DataTable({
        destroy: true,
        paging: false,
        info: false,
        searching: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [2],
                visible: false,
                orderable: false,
                searchable: false
            }
        ],
        order: [[3, 'asc']],
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        buttons: [
            {
                text: '+ Add New Gate',
                className: 'btn-success addNewSluiceGateBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewSluiceGate();

                    e.stopPropagation();
                }
            }
        ]
    });


    sluiceGateTable.on('order.dt search.dt', function () {
        sluiceGateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addSluiceGateBtn = sluiceGateTable.button(['.addNewSluiceGateBtn']);

    function addNewSluiceGate() {

        //if (gateEditing !== null) {
        //    /* A different row is being edited - the edit should be cancelled and this row edited */
        //    restoreGateRowData(gateTable, gateEditing);
        //    gateEditing = null;
        //    addGateBtn.enable();
        //}

        var newGateRow = sluiceGateTable.row.add([
            '',
            '',
            '',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><select class="form-control select1" name="sluiceGatesGateTypeList" id="sluiceGatesGateTypeList" style="width:100%;"></select></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><select class="form-control select1" name="sluiceGatesGateConstructionMaterialTypeList" id="sluiceGatesGateConstructionMaterialTypeList" style="width:100%;"></select></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="sluiceGatesGateNumbers" name="sluiceGatesGateNumbers" placeholder="Number of Gates" class="form-control numberOfGates" style="width:100%;" /></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="sluiceGatesGateHeight" name="sluiceGatesGateHeight" placeholder="Height" class="form-control height"  style="width:100%;" /></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="sluiceGatesGateWidth" name="sluiceGatesGateWidth" placeholder="Width" class="form-control width"  style="width:100%;" /></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="sluiceGatesGateDiameter" name="sluiceGatesGateDiameter" placeholder="Diameter" class="form-control diameter" style="width:100%;" /></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div id="dpSluiceGatesGateInstallationDate" class="input-group date"><input type="text" name="sluiceGatesGateInstallationDate" id="sluiceGatesGateInstallationDate" class="form-control" placeholder="Installation Date (dd/mm/yyyy)" /><span class="input-group-addon form-control-static"><i class="fa fa-calendar fa-fw"></i></span></div></div></div>',
            '<a href="#" class="saveSluiceGateBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelSluiceGateSaveBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>'
        ]).draw(false);

        getGateType('sluiceGatesGateTypeList');
        getGateConstructionMaterialType('sluiceGatesGateConstructionMaterialTypeList');

        $('#dpSluiceGatesGateInstallationDate').datepicker({
            format: "dd/mm/yyyy",
            clearBtn: true,
            autoclose: true,
            todayHighlight: true
        });

        addSluiceGateBtn.disable();
    }

    tableId.on('click', 'a.saveSluiceGateBtn', function (e) {
        e.preventDefault();

        if ($('select[name="sluiceGatesGateTypeList"], select[name="sluiceGatesGateConstructionMaterialTypeList"], input[name="sluiceGatesGateNumbers"], input[name="sluiceGatesGateHeight"], input[name="sluiceGatesGateWidth"], input[name="sluiceGatesGateDiameter"], input[name="sluiceGatesGateInstallationDate"]').valid()) {
            var selectedRow = $(this).parents('tr')[0];

            saveNewGateRow(sluiceGateTable, selectedRow, 'deleteSluiceGateBtn');

            sluiceGateTable.button(['.addNewGateBtn']).enable();
        }

    });

    tableId.on('click', 'a.deleteSluiceGateBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        sluiceGateTable.row(selectedRow).remove().draw(false);

        addSluiceGateBtn.enable();

    });

    tableId.on('click', 'a.cancelSluiceGateSaveBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        sluiceGateTable.row(selectedRow).remove().draw(false);

        addSluiceGateBtn.enable();

    });

}

function turnoutGatesTableRelatedFunctions() {
    //var gateEditing = null;

    var tableId = $('#turnoutAssetTypeDiv table#turnoutGatesTable');

    turnoutGateTable = tableId.DataTable({
        destroy: true,
        paging: false,
        info: false,
        searching: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',
                targets: [2],
                visible: false,
                orderable: false,
                searchable: false
            }
        ],
        order: [[3, 'asc']],
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        buttons: [
            {
                text: '+ Add New Gate',
                className: 'btn-success addNewTurnoutGateBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewTurnoutGate();

                    e.stopPropagation();
                }
            }
        ]
    });


    turnoutGateTable.on('order.dt search.dt', function () {
        turnoutGateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addTurnoutGateBtn = turnoutGateTable.button(['.addNewTurnoutGateBtn']);

    function addNewTurnoutGate() {

        //if (gateEditing !== null) {
        //    /* A different row is being edited - the edit should be cancelled and this row edited */
        //    restoreGateRowData(gateTable, gateEditing);
        //    gateEditing = null;
        //    addGateBtn.enable();
        //}

        var newGateRow = turnoutGateTable.row.add([
            '',
            '',
            '',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><select class="form-control select1" name="turnoutGatesGateTypeList" id="turnoutGatesGateTypeList" style="width:100%;"></select></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><select class="form-control select1" name="turnoutGatesGateConstructionMaterialTypeList" id="turnoutGatesGateConstructionMaterialTypeList" style="width:100%;"></select></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="turnoutGatesGateNumbers" name="turnoutGatesGateNumbers" placeholder="Number of Gates" class="form-control numberOfGates" style="width:100%;" /></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="turnoutGatesGateHeight" name="turnoutGatesGateHeight" placeholder="Height" class="form-control height"  style="width:100%;" /></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="turnoutGatesGateWidth" name="turnoutGatesGateWidth" placeholder="Width" class="form-control width"  style="width:100%;" /></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><input type="text" id="turnoutGatesGateDiameter" name="turnoutGatesGateDiameter" placeholder="Diameter" class="form-control diameter" style="width:100%;" /></div></div>',
            '<div class="form-group"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div id="dpTurnoutGatesGateInstallationDate" class="input-group date"><input type="text" name="turnoutGatesGateInstallationDate" id="turnoutGatesGateInstallationDate" class="form-control" placeholder="Installation Date (dd/mm/yyyy)" /><span class="input-group-addon form-control-static"><i class="fa fa-calendar fa-fw"></i></span></div></div></div>',
            '<a href="#" class="saveTurnoutGateBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelTurnoutGateSaveBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>'
        ]).draw(false);

        getGateType('turnoutGatesGateTypeList');
        getGateConstructionMaterialType('turnoutGatesGateConstructionMaterialTypeList');

        $('#dpTurnoutGatesGateInstallationDate').datepicker({
            format: "dd/mm/yyyy",
            clearBtn: true,
            autoclose: true,
            todayHighlight: true
        });

        addTurnoutGateBtn.disable();
    }

    tableId.on('click', 'a.saveTurnoutGateBtn', function (e) {
        e.preventDefault();

        if ($('select[name="turnoutGatesGateTypeList"], select[name="turnoutGatesGateConstructionMaterialTypeList"], input[name="turnoutGatesGateNumbers"], input[name="turnoutGatesGateHeight"], input[name="turnoutGatesGateWidth"], input[name="turnoutGatesGateDiameter"], input[name="turnoutGatesGateInstallationDate"]').valid()) {
            var selectedRow = $(this).parents('tr')[0];

            saveNewGateRow(turnoutGateTable, selectedRow, 'deleteTurnoutGateBtn');

            turnoutGateTable.button(['.addNewGateBtn']).enable();
        }

    });

    tableId.on('click', 'a.deleteTurnoutGateBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        turnoutGateTable.row(selectedRow).remove().draw(false);

        addTurnoutGateBtn.enable();

    });

    tableId.on('click', 'a.cancelTurnoutGateSaveBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        turnoutGateTable.row(selectedRow).remove().draw(false);

        addTurnoutGateBtn.enable();

    });

}

function getGateType(id) {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetGateType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showGateType(id, response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showGateType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Gate Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].GateTypeName, arr[i].GateTypeGuid);
    }
}

function getGateConstructionMaterialType(id) {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateSIOAsset.aspx/GetGateConstructionMaterialType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showGateConstructionMaterialType(id, response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

function showGateConstructionMaterialType(selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Construction Material Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].GateConstructionMaterialName, arr[i].GateConstructionMaterialGuid);
    }
}

function saveNewGateRow(gateTable, selectedRow, deleteBtnClass) {

    var selectedRowData = gateTable.row(selectedRow).data();

    var availableSelects = $('select', selectedRow);
    var availableInputs = $('input', selectedRow);

    selectedRowData[1] = availableSelects[0].value
    selectedRowData[2] = availableSelects[1].value
    selectedRowData[3] = availableSelects[0].options[availableSelects[0].selectedIndex].text
    selectedRowData[4] = availableSelects[1].options[availableSelects[1].selectedIndex].text
    selectedRowData[5] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
    selectedRowData[6] = capitalizeFirstAllWords($.trim(availableInputs[1].value));
    selectedRowData[7] = capitalizeFirstAllWords($.trim(availableInputs[2].value));
    selectedRowData[8] = capitalizeFirstAllWords($.trim(availableInputs[3].value));
    selectedRowData[9] = capitalizeFirstAllWords($.trim(availableInputs[4].value));
    selectedRowData[10] = '<a href="#" class="' + deleteBtnClass + '"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    gateTable.row(selectedRow).data(selectedRowData);

    gateTable.on('order.dt search.dt', function () {
        gateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}


//Upload Drawing
function uploadAssetDrawing(assetCode) {
    var retValue = [];

    retValue[0] = false;

    var fileUpload = $("#drawingCode1").get(0);
    var files = fileUpload.files;

    var maxSize = 10000000;//$("#drawingCode1").data('max-size');//10MB

    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        var fileSize = files[i].size; // in bytes
        if (fileSize > maxSize) {
            retValue[1] = 'File size is more than 10MB.';
        } else {
            data.append(files[i].name, files[i]);
        }

    }

    if (retValue.length === 1) {
        data.append("assetType", assetCode[2]);
        data.append("assetCode", assetCode);

        var options = {};
        options.url = "Handler1.ashx";
        options.type = "POST";
        options.data = data;
        options.contentType = false;
        options.processData = false;
        options.async = false;
        options.success = function (result) {
            retValue[0] = true;
            drawingUrl = result;
        };
        options.error = function (err) {
            retValue[1] = err.statusText;
        };

        $.ajax(options);
    }

    return retValue;
}


//Save Asset Functions

function saveAsset(assetTypeCode) {
    var returnValue = 0;

    var divisionManager = {
        divisionGuid: $('#divisionList').val()
    }

    var schemeManager = {
        schemeGuid: $('#schemeList').val()
    }

    var assetTypeManager = {
        assetTypeGuid: $('#assetTypeList').val(),
        assetTypeCode: assetTypeCode
    }

    var assetDrawingManager = {
        drawingFileName: $('#drawingCode').val(),
        drawingFileUrl: drawingUrl
    }

    var initiationDate = $('#dateOfInitiationOfAsset').val();
    initiationDate = Date.parseExact(initiationDate, 'dd/MM/yyyy').toString('yyyy/MM/dd');

    var completionDate = $('#dateOfCompletionOfAsset').val();
    completionDate = Date.parseExact(completionDate, 'dd/MM/yyyy').toString('yyyy/MM/dd');

    var keyAssetGuid = $('#keyAssetSelect').val();    

    var assetCost = $('#assetCost').val();
    var newchar = '';
    assetCost = assetCost.split(',').join(newchar);
    assetCost = stringIsNullOrEmpty($.trim(assetCost)) ? parseFloat(0) : parseFloat(assetCost);
    //assetCost = parseFloat(assetCost);

    var assetManager = {
        assetCode: $('#assetCode').val(),
        drawingCode: $('#drawingCode').val(),
        amtd: $('#amtd').val(),
        assetName: $('#assetName').val(),
        startChainage: stringIsNullOrEmpty($.trim($('#chainageStart').val())) ? 0 : parseInt($.trim($('#chainageStart').val())),
        endChainage: stringIsNullOrEmpty($.trim($('#chainageEnd').val())) ? 0 : parseInt($.trim($('#chainageEnd').val())),
        utmEast1: stringIsNullOrEmpty($.trim($('#eastingUS').val())) ? 0 : parseInt($.trim($('#eastingUS').val())),
        utmEast2: stringIsNullOrEmpty($.trim($('#eastingDS').val())) ? 0 : parseInt($.trim($('#eastingDS').val())),
        utmNorth1: stringIsNullOrEmpty($.trim($('#northingUS').val())) ? 0 : parseInt($.trim($('#northingUS').val())),
        utmNorth2: stringIsNullOrEmpty($.trim($('#northingDS').val())) ? 0 : parseInt($.trim($('#northingDS').val())),
        assetNumber: $('#assetCodeNumberSelect').val(),
        initiationDate: initiationDate,
        completionDate: completionDate,
        assetCostInput: assetCost,
        divisionManager: divisionManager,
        schemeManager: schemeManager,
        assetTypeManager: assetTypeManager,
        keyAssetGuid: keyAssetGuid,
        assetDrawingManager: assetDrawingManager
    }

    if (assetTypeCode === 'BRG') {
        var bridgeTypeManager = {
            bridgeTypeGuid: $('#bridgeTypeList').val(),
        }

        var bridgeManager = {
            length: stringIsNullOrEmpty($.trim($('#bridgeLength').val())) ? 0 : parseFloat($.trim($('#bridgeLength').val())),
            pierNumber: stringIsNullOrEmpty($.trim($('#bridgeNumberOfPiers').val())) ? 0 : parseInt($.trim($('#bridgeNumberOfPiers').val())),
            roadWidth: stringIsNullOrEmpty($.trim($('#bridgeRoadWidth').val())) ? 0 : parseFloat($.trim($('#bridgeRoadWidth').val())),
            bridgeTypeManager: bridgeTypeManager
        }

        assetManager.bridgeManager = bridgeManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'CAN') {
        var canalLiningTypeManager = {
            liningTypeGuid: $('#canalLiningTypeList').val(),
        }

        var canalManager = {
            usElevation: stringIsNullOrEmpty($.trim($('#canalUsCrestElevation').val())) ? 0 : parseFloat($.trim($('#canalUsCrestElevation').val())),
            dsElevation: stringIsNullOrEmpty($.trim($('#canalDsCrestElevation').val())) ? 0 : parseFloat($.trim($('#canalDsCrestElevation').val())),
            length: stringIsNullOrEmpty($.trim($('#canalLength').val())) ? 0 : parseFloat($.trim($('#canalLength').val())),
            bedWidth: stringIsNullOrEmpty($.trim($('#canalBedWidth').val())) ? 0 : parseFloat($.trim($('#canalBedWidth').val())),
            slope1: stringIsNullOrEmpty($.trim($('#canalSlipeSlope1').val())) ? 0 : parseFloat($.trim($('#canalSlipeSlope1').val())),
            slope2: stringIsNullOrEmpty($.trim($('#canalSlipeSlope2').val())) ? 0 : parseFloat($.trim($('#canalSlipeSlope2').val())),
            averageDepth: stringIsNullOrEmpty($.trim($('#canalAverageDepth').val())) ? 0 : parseFloat($.trim($('#canalAverageDepth').val())),
            canalLiningTypeManager: canalLiningTypeManager
        }

        assetManager.canalManager = canalManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'CUL') {
        var culvertTypeManager = {
            culvertTypeGuid: $('#culvertTypeList').val(),
        }

        var culvertManager = {
            ventNumber: stringIsNullOrEmpty($.trim($('#culvertNumberOfVents').val())) ? 0 : parseInt($.trim($('#culvertNumberOfVents').val())),
            ventHeight: stringIsNullOrEmpty($.trim($('#culvertVentHeight').val())) ? 0 : parseFloat($.trim($('#culvertVentHeight').val())),
            ventWidth: stringIsNullOrEmpty($.trim($('#culvertVentWidth').val())) ? 0 : parseFloat($.trim($('#culvertVentWidth').val())),
            lengthCulvert: stringIsNullOrEmpty($.trim($('#culvertLength').val())) ? 0 : parseFloat($.trim($('#culvertLength').val())),
            culvertTypeManager: culvertTypeManager
        }

        assetManager.culvertManager = culvertManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'DRN') {
        var drainageManager = {
            usElevationDrainage: stringIsNullOrEmpty($.trim($('#drainageUsBedElevation').val())) ? 0 : parseFloat($.trim($('#drainageUsBedElevation').val())),
            dsElevationDrainage: stringIsNullOrEmpty($.trim($('#drainageDsBedElevation').val())) ? 0 : parseFloat($.trim($('#drainageDsBedElevation').val())),
            lengthDrainage: stringIsNullOrEmpty($.trim($('#drainageLength').val())) ? 0 : parseFloat($.trim($('#drainageLength').val())),
            bedWidthDrainage: stringIsNullOrEmpty($.trim($('#drainageBedWidth').val())) ? 0 : parseFloat($.trim($('#drainageBedWidth').val())),
            slope1Drainage: stringIsNullOrEmpty($.trim($('#drainageSlope1').val())) ? 0 : parseFloat($.trim($('#drainageSlope1').val())),
            slope2Drainage: stringIsNullOrEmpty($.trim($('#drainageSlope2').val())) ? 0 : parseFloat($.trim($('#drainageSlope2').val())),
            averageDepthDrainage: stringIsNullOrEmpty($.trim($('#drainageAverageDepth').val())) ? 0 : parseFloat($.trim($('#drainageAverageDepth').val()))
        }

        assetManager.drainageManager = drainageManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'DRP') {
        var dropStructureManager = {
            usInvertLevel: stringIsNullOrEmpty($.trim($('#dropStructureUsInvertLevel').val())) ? 0 : parseFloat($.trim($('#dropStructureUsInvertLevel').val())),
            dsInvertLevel: stringIsNullOrEmpty($.trim($('#dropStructureDsInvertLevel').val())) ? 0 : parseFloat($.trim($('#dropStructureDsInvertLevel').val())),
            stillingBasinLength: stringIsNullOrEmpty($.trim($('#dropStructureStillingBasinLength').val())) ? 0 : parseFloat($.trim($('#dropStructureStillingBasinLength').val())),
            stillingBasinWidth: stringIsNullOrEmpty($.trim($('#dropStructureStillingBasinWidth').val())) ? 0 : parseFloat($.trim($('#dropStructureStillingBasinWidth').val()))
        }

        assetManager.dropStructureManager = dropStructureManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'EMB') {
        var embankmentCrestTypeManager = {
            crestTypeGuid: $('#embankmentCrestTypeList').val(),
        }

        var embankmentFillTypeManager = {
            fillTypeGuid: $('#embankmentFillTypeList').val(),
        }

        var embankmentManager = {
            usElevation: stringIsNullOrEmpty($.trim($('#embankmentUsCrestElevation').val())) ? 0 : parseFloat($.trim($('#embankmentUsCrestElevation').val())),
            dsElevation: stringIsNullOrEmpty($.trim($('#embankmentDsCrestElevation').val())) ? 0 : parseFloat($.trim($('#embankmentDsCrestElevation').val())),
            length: stringIsNullOrEmpty($.trim($('#embankmentLength').val())) ? 0 : parseFloat($.trim($('#embankmentLength').val())),
            crestWidth: stringIsNullOrEmpty($.trim($('#embankmentCrestWidth').val())) ? 0 : parseFloat($.trim($('#embankmentCrestWidth').val())),
            csSlope1: stringIsNullOrEmpty($.trim($('#embankmentCsSlope1').val())) ? 0 : parseFloat($.trim($('#embankmentCsSlope1').val())),
            csSlope2: stringIsNullOrEmpty($.trim($('#embankmentCsSlope2').val())) ? 0 : parseFloat($.trim($('#embankmentCsSlope2').val())),
            rsSlope1: stringIsNullOrEmpty($.trim($('#embankmentRsSlope1').val())) ? 0 : parseFloat($.trim($('#embankmentRsSlope1').val())),
            rsSlope2: stringIsNullOrEmpty($.trim($('#embankmentRsSlope2').val())) ? 0 : parseFloat($.trim($('#embankmentRsSlope2').val())),
            bermSlope1: stringIsNullOrEmpty($.trim($('#embankmentBermSlope1').val())) ? 0 : parseFloat($.trim($('#embankmentBermSlope1').val())),
            bermSlope2: stringIsNullOrEmpty($.trim($('#embankmentBermSlope2').val())) ? 0 : parseFloat($.trim($('#embankmentBermSlope2').val())),
            platformWidth: stringIsNullOrEmpty($.trim($('#embankmentPlatformWidth').val())) ? 0 : parseFloat($.trim($('#embankmentPlatformWidth').val())),
            averageHeight: stringIsNullOrEmpty($.trim($('#embankmentAverageHeight').val())) ? 0 : parseFloat($.trim($('#embankmentAverageHeight').val())),
            embankmentCrestTypeManager: embankmentCrestTypeManager,
            embankmentFillTypeManager: embankmentFillTypeManager
        }

        assetManager.embankmentManager = embankmentManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'GAU') {
        var gaugeTypeManager = {
            gaugeTypeGuid: $('#gaugeTypeList').val(),
        }

        var gaugeSeasonTypeManager = {
            seasonTypeGuid: $('#gaugeSeasonTypeList').val(),
        }

        var gaugeFrequencyTypeManager = {
            frequencyTypeGuid: $('#gaugeFrequencyTypeList').val(),
        }

        var startDate = $('#gaugeStartDate').val();
        startDate = stringIsNullOrEmpty($.trim($('#gaugeStartDate').val())) ? "" : Date.parseExact(startDate, 'dd/MM/yyyy').toString('yyyy/MM/dd');

        var endDate = $('#gaugeEndDate').val();
        endDate = stringIsNullOrEmpty($.trim($('#gaugeEndDate').val())) ? "" : Date.parseExact(endDate, 'dd/MM/yyyy').toString('yyyy/MM/dd');

        var gaugeManager = {
            startDate: startDate,
            endDate: endDate,
            active: $('#gaugeActive').is(':checked'),
            lwl: $('#gaugeLwlPresent').is(':checked'),
            hwl: $('#gaugeHwlPresent').is(':checked'),
            levelGeo: $('#gaugeLevelGeo').is(':checked'),
            zeroDatum: stringIsNullOrEmpty($.trim($('#gaugeZeroDatum').val())) ? 0 : parseFloat($.trim($('#gaugeZeroDatum').val())),
            gaugeTypeManager: gaugeTypeManager,
            gaugeFrequencyTypeManager: gaugeFrequencyTypeManager,
            gaugeSeasonTypeManager: gaugeSeasonTypeManager
        }

        assetManager.gaugeManager = gaugeManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'POR') {
        var porcupineTypeManager = {
            porcupineTypeGuid: $('#porcupineTypeList').val(),
        }

        var porcupineMaterialTypeManager = {
            porcupineMaterialTypeGuid: $('#porcupineMaterialTypeList').val(),
        }

        var porcupineManager = {
            screenLength: stringIsNullOrEmpty($.trim($('#porcupineScreenLength').val())) ? 0 : parseFloat($.trim($('#porcupineScreenLength').val())),
            spacingAlongScreen: stringIsNullOrEmpty($.trim($('#porcupineSpacingAlongScreen').val())) ? 0 : parseFloat($.trim($('#porcupineSpacingAlongScreen').val())),
            screenRows: stringIsNullOrEmpty($.trim($('#porcupineScreenRows').val())) ? 0 : parseInt($.trim($('#porcupineScreenRows').val())),
            numberOfLayers: stringIsNullOrEmpty($.trim($('#porcupineNumberOfLayers').val())) ? 0 : parseInt($.trim($('#porcupineNumberOfLayers').val())),
            memberLength: stringIsNullOrEmpty($.trim($('#porcupineMamberLength').val())) ? 0 : parseFloat($.trim($('#porcupineMamberLength').val())),
            lengthPorcupine: stringIsNullOrEmpty($.trim($('#porcupineLength').val())) ? 0 : parseFloat($.trim($('#porcupineLength').val())),
            porcupineTypeManager: porcupineTypeManager,
            porcupineMaterialTypeManager: porcupineMaterialTypeManager,
        }

        assetManager.porcupineManager = porcupineManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'REG') {
        var output = regulatorGateTable.buttons.exportData();

        var regulatorTypeManager = {
            regulatorTypeGuid: $('#regulatorTypeList').val(),
        }

        var regulatorGateTypeManager = {
            regulatorGateTypeGuid: $('#regulatorGateTypeList').val(),
        }

        var gateManager = [];

        for (var i = 0; i < output.body.length; i++) {
            var gateTypeManager = {
                gateTypeGuid: output.body[i][1],
            };

            var gateConstructionMaterialManager = {
                gateConstructionMaterialGuid: output.body[i][2],
            }

            gateManager.push({
                gateTypeManager: gateTypeManager,
                gateConstructionMaterialManager: gateConstructionMaterialManager,
                numberOfGates: output.body[i][5],
                height: stringIsNullOrEmpty(output.body[i][6]) ? 0 : parseFloat(output.body[i][6]),
                width: stringIsNullOrEmpty(output.body[i][7]) ? 0 : parseFloat(output.body[i][7]),
                diameter: stringIsNullOrEmpty(output.body[i][8]) ? 0 : parseFloat(output.body[i][8]),
                installDate: Date.parseExact(output.body[i][9], 'dd/MM/yyyy').toString('yyyy/MM/dd'),
            });
        }

        var regulatorManager = {
            ventNumberRegulator: stringIsNullOrEmpty($.trim($('#regulatorNumberOfVents').val())) ? 0 : parseInt($.trim($('#regulatorNumberOfVents').val())),
            ventHeightRegulator: stringIsNullOrEmpty($.trim($('#regulatorVentHeight').val())) ? 0 : parseFloat($.trim($('#regulatorVentHeight').val())),
            ventWidthRegulator: stringIsNullOrEmpty($.trim($('#regulatorVentWidth').val())) ? 0 : parseFloat($.trim($('#regulatorVentWidth').val())),
            regulatorTypeManager: regulatorTypeManager,
            regulatorGateTypeManager: regulatorGateTypeManager,
            gateManagerList: gateManager,
        }

        assetManager.regulatorManager = regulatorManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'REV') {
        var revetmentTypeManager = {
            revetTypeGuid: $('#revetmentTypeList').val(),
        }

        var revetmentRiverprotTypeManager = {
            riverprotTypeGuid: $('#revetmentRiverProtectionTypeList').val(),
        }

        var revetmentWaveprotTypeManager = {
            waveprotTypeGuid: $('#revetmentWaveProtectionTypeList').val(),
        }

        var revetmentManager = {
            lengthRevetment: stringIsNullOrEmpty($.trim($('#revetmentLength').val())) ? 0 : parseFloat($.trim($('#revetmentLength').val())),
            plainWidth: stringIsNullOrEmpty($.trim($('#revetmentPlainWidth').val())) ? 0 : parseFloat($.trim($('#revetmentPlainWidth').val())),
            slopeRevetment: stringIsNullOrEmpty($.trim($('#revetmentSlope').val())) ? 0 : parseInt($.trim($('#revetmentSlope').val())),
            revetmentTypeManager: revetmentTypeManager,
            revetmentRiverprotTypeManager: revetmentRiverprotTypeManager,
            revetmentWaveprotTypeManager: revetmentWaveprotTypeManager
        }

        assetManager.revetmentManager = revetmentManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'SLU') {
        var output = sluiceGateTable.buttons.exportData();

        var sluiceTypeManager = {
            sluiceTypeGuid: $('#sluiceTypeList').val(),
        };

        var gateManager = [];

        for (var i = 0; i < output.body.length; i++) {
            var gateTypeManager = {
                gateTypeGuid: output.body[i][1],
            };

            var gateConstructionMaterialManager = {
                gateConstructionMaterialGuid: output.body[i][2],
            }

            gateManager.push({
                gateTypeManager: gateTypeManager,
                gateConstructionMaterialManager: gateConstructionMaterialManager,
                numberOfGates: output.body[i][5],
                height: stringIsNullOrEmpty(output.body[i][6]) ? 0 : parseFloat(output.body[i][6]),
                width: stringIsNullOrEmpty(output.body[i][7]) ? 0 : parseFloat(output.body[i][7]),
                diameter: stringIsNullOrEmpty(output.body[i][8]) ? 0 : parseFloat(output.body[i][8]),
                installDate: Date.parseExact(output.body[i][9], 'dd/MM/yyyy').toString('yyyy/MM/dd'),
            });
        }

        var sluiceManager = {
            ventNumberSluice: stringIsNullOrEmpty($.trim($('#sluiceNumberOfVents').val())) ? 0 : parseInt($.trim($('#sluiceNumberOfVents').val())),
            ventDiameterSluice: stringIsNullOrEmpty($.trim($('#sluiceVentDiameter').val())) ? 0 : parseFloat($.trim($('#sluiceVentDiameter').val())),
            ventHeightSluice: stringIsNullOrEmpty($.trim($('#sluiceVentHeight').val())) ? 0 : parseFloat($.trim($('#sluiceVentHeight').val())),
            ventWidthSluice: stringIsNullOrEmpty($.trim($('#sluiceVentWidth').val())) ? 0 : parseFloat($.trim($('#sluiceVentWidth').val())),
            sluiceTypeManager: sluiceTypeManager,
            gateManagerList: gateManager
        };

        assetManager.sluiceManager = sluiceManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'SPU') {
        var spurConstructionTypeManager = {
            constructionTypeGuid: $('#spurConstructionTypeList').val(),
        }

        var spurRevetTypeManager = {
            spurRevetTypeGuid: $('#spurRevetTypeList').val(),
        }

        var spurShapeTypeManager = {
            shapeTypeGuid: $('#spurShapeTypeList').val(),
        }

        var spurManager = {
            orientation: stringIsNullOrEmpty($.trim($('#spurOrientation').val())) ? 0 : parseFloat($.trim($('#spurOrientation').val())),
            lengthSpur: stringIsNullOrEmpty($.trim($('#spurLength').val())) ? 0 : parseFloat($.trim($('#spurLength').val())),
            widthSpur: stringIsNullOrEmpty($.trim($('#spurWidth').val())) ? 0 : parseFloat($.trim($('#spurWidth').val())),
            spurConstructionTypeManager: spurConstructionTypeManager,
            spurRevetTypeManager: spurRevetTypeManager,
            spurShapeTypeManager: spurShapeTypeManager
        }

        assetManager.spurManager = spurManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'TRN') {
        var output = turnoutGateTable.buttons.exportData();

        var turnoutTypeManager = {
            turnoutTypeGuid: $('#turnoutTypeList').val(),
        }

        var turnoutGateTypeManager = {
            turnoutGateTypeGuid: $('#turnoutGateTypeList').val(),
        }

        var gateManager = [];

        for (var i = 0; i < output.body.length; i++) {
            var gateTypeManager = {
                gateTypeGuid: output.body[i][1],
            };

            var gateConstructionMaterialManager = {
                gateConstructionMaterialGuid: output.body[i][2],
            }

            gateManager.push({
                gateTypeManager: gateTypeManager,
                gateConstructionMaterialManager: gateConstructionMaterialManager,
                numberOfGates: output.body[i][5],
                height: stringIsNullOrEmpty(output.body[i][6]) ? 0 : parseFloat(output.body[i][6]),
                width: stringIsNullOrEmpty(output.body[i][7]) ? 0 : parseFloat(output.body[i][7]),
                diameter: stringIsNullOrEmpty(output.body[i][8]) ? 0 : parseFloat(output.body[i][8]),
                installDate: Date.parseExact(output.body[i][9], 'dd/MM/yyyy').toString('yyyy/MM/dd'),
            });
        }

        var turnoutManager = {
            inletboxWidth: stringIsNullOrEmpty($.trim($('#turnoutInletBoxWidth').val())) ? 0 : parseFloat($.trim($('#turnoutInletBoxWidth').val())),
            inletboxLength: stringIsNullOrEmpty($.trim($('#turnoutInletBoxLength').val())) ? 0 : parseFloat($.trim($('#turnoutInletBoxLength').val())),
            outletNumber: stringIsNullOrEmpty($.trim($('#turnoutOutletNumber').val())) ? 0 : parseInt($.trim($('#turnoutOutletNumber').val())),
            outletWidth: stringIsNullOrEmpty($.trim($('#turnoutOutletWidth').val())) ? 0 : parseFloat($.trim($('#turnoutOutletWidth').val())),
            outletHeight: stringIsNullOrEmpty($.trim($('#turnoutOutletHeight').val())) ? 0 : parseFloat($.trim($('#turnoutOutletHeight').val())),
            turnoutTypeManager: turnoutTypeManager,
            turnoutGateTypeManager: turnoutGateTypeManager,
            gateManagerList: gateManager
        }

        assetManager.turnoutManager = turnoutManager;

        returnValue = saveAssetData(assetManager);
    }
    else if (assetTypeCode === 'WEI') {
        weirTypeManager = {
            weirTypeGuid: $('#weirTypeList').val(),
        }

        weirManager = {
            invertLevel: stringIsNullOrEmpty($.trim($('#weirInvertLevel').val())) ? 0 : parseFloat($.trim($('#weirInvertLevel').val())),
            crestTopLevel: stringIsNullOrEmpty($.trim($('#weirCrestTopLevel').val())) ? 0 : parseFloat($.trim($('#weirCrestTopLevel').val())),
            crestTopWidth: stringIsNullOrEmpty($.trim($('#weirCrestTopWidth').val())) ? 0 : parseFloat($.trim($('#weirCrestTopWidth').val())),
            weirTypeManager: weirTypeManager
        }

        assetManager.weirManager = weirManager;

        returnValue = saveAssetData(assetManager);
    }
    else {
        returnValue = 0;
    }

    return returnValue;
}

function saveAssetData(assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateSIOAsset.aspx/SaveAsset",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}