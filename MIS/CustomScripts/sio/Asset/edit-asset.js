﻿var gateTypeData = [];
var gateConstructionMaterialData = [];

$(document).ready(function () {


    gatesBasedOnEditAssetTypeTableFunctions();

    //showKeyAssetOptions('keyAssetList', getKeyAssets());

    var cfgCulture = 'en-IN';
    $.preferCulture(cfgCulture);

    $('#assetCost').maskMoney();

    //$(document.body).on('click', 'button#updateAssetBtn', function (e) {
    $('#updateAssetBtn').on('click', function (e) {
        e.preventDefault();

        if ($('#editAssetForm').valid()) {
            $("#preloader").show();
            $("#status").show();

            var assetUpdated = updateAsset();

            if (assetUpdated === 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Asset is updated successfully.", function () {
                    window.location = 'SIOAssets.aspx';
                    //location.reload(true);
                });
            } else {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Data cannot be updated.");
            }

        }

    });

    $('#cancelAssetEditBtn').on('click', function (e) {
        e.preventDefault();

        window.location = 'SIOAssets.aspx';
    });

    $('#keyAssetList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select Key Asset--',
        disableIfEmpty: false,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                var selectedKeyAssetGuid = $(element).val();
                var selectedKeyAssetText = $(element).text();

                //if (selectedKeyAssetGuid === '') {
                //    //codeArray[2] = '';

                //    //displayAssetCode(codeArray);

                //    //showAssetNumberOptionsToAddDbaAssets('assetCodeNumberSelect', [], false);

                //    //showKeyAssets('keyAssetSelect', getKeyAssets(selectedAssetTypeGuid, [], []), false);
                //} else {
                    
                //    //showAssetNumberOptionsToAddDbaAssets('assetCodeNumberSelect', getAssetNumberOptionsToAddDbaAssets(selectedAssetTypeGuid, $('#schemeList').val()), true);

                //    //var selectedAssetTypeTextSplit = multiSplit(selectedAssetTypeText, [' (', ')']);

                //    //codeArray[2] = selectedAssetTypeTextSplit[1];

                //    //displayAssetCode(codeArray);

                //    //showKeyAssets('keyAssetSelect', getKeyAssets(selectedAssetTypeGuid, $('#divisionList').val(), $('#schemeList').val()), true);

                //    ////var start = new Date().getTime();

                //    ////displayAssetTechnicalSpecification1(selectedAssetTypeTextSplit[1]);

                //    ////var end = new Date().getTime();
                //    ////var time = end - start;
                //    ////alert('Execution time1: ' + time);

                //    //displayAssetTechnicalSpecification(selectedAssetTypeTextSplit[1]);
                //}

                $(document).click();

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#dpDateOfInitiationOfAsset').datepicker({
        format: "dd/mm/yyyy",
    });

    $('#dpStartDate').datepicker({
        format: "dd/mm/yyyy",
    });

    //$('#dpEndDate').datepicker({
    //    format: "dd/mm/yyyy",
    //});

    var fullDate = new Date();

    var twoDigitMonth = fullDate.getMonth() + 1 + '';

    if (twoDigitMonth.length == 1) {
        twoDigitMonth = "0" + twoDigitMonth;
    }

    var twoDigitDate = fullDate.getDate() + '';

    if (twoDigitDate.length == 1) {
        twoDigitDate = "0" + twoDigitDate;
    }

    var currentDate = twoDigitDate + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

    $('#dpDateOfInitiationOfAsset').datepicker({
        format: "dd/mm/yyyy",
        startDate: currentDate.toString(),
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var initiationAssetDate = new Date(ev.date.valueOf());
        initiationAssetDate = initiationAssetDate.add(1).days();
        $('#dpDateOfCompletionOfAsset').datepicker('setStartDate', initiationAssetDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpDateOfCompletionOfAsset').datepicker('setStartDate', null);
    });

    $('#dpDateOfCompletionOfAsset').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var assetStartDateCompletionDate = new Date(ev.date.valueOf());
        assetStartDateCompletionDate = assetStartDateCompletionDate.add(-1).days();
        $('#dpDateOfInitiationOfAsset').datepicker('setEndDate', assetStartDateCompletionDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpDateOfInitiationOfAsset').datepicker('setEndDate', null);
    });

    $('#dpStartDate').datepicker({
        format: "dd/mm/yyyy",
        startDate: currentDate.toString(),
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var projectExpectedCompletionDateStartDate = new Date(ev.date.valueOf());
        projectExpectedCompletionDateStartDate = projectExpectedCompletionDateStartDate.add(1).days();
        $('#dpEndDate').datepicker('setStartDate', projectExpectedCompletionDateStartDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpEndDate').datepicker('setStartDate', null);
    });

    $('#dpEndDate').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var projectStartDateEndDate = new Date(ev.date.valueOf());
        projectStartDateEndDate = projectStartDateEndDate.add(-1).days();
        $('#dpStartDate').datepicker('setEndDate', projectStartDateEndDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpStartDate').datepicker('setEndDate', null);
    });

    $('#editAssetForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            bridgeTypeSelect: "required",
            liningTypeSelect: "required",
            culvertTypeSelect: "required",
            crestTypeSelect: "required",
            fillTypeSelect: "required",
            gaugeTypeSelect: "required",
            seasonTypeSelect: "required",
            frequencyTypeSelect: "required",
            porcupineTypeSelect: "required",
            materialTypeSelect: "required",
            regulatorTypeSelect: "required",
            regulatorGateTypeSelect: "required",
            revetmentTypeSelect: "required",
            riverProtectionTypeSelect: "required",
            waveProtectionTypeSelect: "required",
            sluiceTypeSelect: "required",
            shapeTypeSelect: "required",
            constructionTypeSelect: "required",
            spurRevetTypeSelect: "required",
            turnoutTypeSelect: "required",
            turnoutGateTypeSelect: "required",
            weirTypeSelect: "required",
            assetName: {
                required: true,
                noSpace: true
            },
            chainageStart: {
                number: true
            },
            chainageEnd: {
                number: true
            },
            eastingUS: {
                number: true
            },
            eastingDS: {
                number: true
            },
            northingUS: {
                number: true
            },
            northingDS: {
                number: true
            },
            dateOfCompletionOfAsset: {
                required: true,
                dateITA: true
            },
            lengthBridge: {
                number: true
            },
            numberPeirs: {
                number: true
            },
            roadWidth: {
                number: true
            },
            uSCrestElevationCanal: {
                number: true
            },
            dSCrestElevationCanal: {
                number: true
            },
            lengthCanal: {
                number: true
            },
            bedWidthCanal: {
                number: true
            },
            slipeSlope1Canal: {
                number: true
            },
            slipeSlope2Canal: {
                number: true
            },
            averageDepthCanal: {
                number: true
            },
            numberOfVents: {
                number: true
            },
            ventHeight: {
                number: true
            },
            ventWidth: {
                number: true
            },
            lengthCulvert: {
                number: true
            },
            usCrestElevationDrainage: {
                number: true
            },
            dsCrestElevationDrainage: {
                number: true
            },
            lengthDrainage: {
                number: true
            },
            BedWidthDrainage: {
                number: true
            },
            slope1Drainage: {
                number: true
            },
            slope2Drainage: {
                number: true
            },
            averageDepthDrainage: {
                number: true
            },
            uSCrestElevation: {
                number: true
            },
            dSCrestElevation: {
                number: true
            },
            length: {
                number: true
            },
            crestWidth: {
                number: true
            },
            csSlope1: {
                number: true
            },
            csSlope2: {
                number: true
            },
            rsSlope1: {
                number: true
            },
            rsSlope2: {
                number: true
            },
            bermSlope1: {
                number: true
            },
            bermSlope2: {
                number: true
            },
            platformWidth: {
                number: true
            },
            averageHeight: {
                number: true
            },
            zeroDatum: {
                number: true
            },
            startDate: {
                //required: true,
                dateITA: true
            },
            endDate: {
                //required: true,
                dateITA: true
            },
            usInvertLevel: {
                number: true
            },
            dsInvertLevel: {
                number: true
            },
            stillingBasinLength: {
                number: true
            },
            stillingBasinWidth: {
                number: true
            },
            screenLength: {
                number: true
            },
            spacingAlongScreen: {
                number: true
            },
            spacingAlongScreen: {
                number: true
            },
            screenRows: {
                number: true
            },
            noOfLayers: {
                number: true
            },
            mamberLength: {
                number: true
            },
            lengthPorcupine: {
                number: true
            },
            numberOfVentsRegulator: {
                number: true
            },
            ventHeightRegulator: {
                number: true
            },
            ventWidthRegulator: {
                number: true
            },
            slopeRevetment: {
                number: true
            },
            lengthRevetment: {
                number: true
            },
            plainWidth: {
                number: true
            },
            NoOfVentsSluice: {
                number: true
            },
            ventDiameterSluice: {
                number: true
            },
            ventHeightSluice: {
                number: true
            },
            ventWidthSluice: {
                number: true
            },
            orientation: {
                number: true
            },
            lengthSpur: {
                number: true
            },
            widthSpur: {
                number: true
            },
            inletBoxWidth: {
                number: true
            },
            inletBoxLength: {
                number: true
            },
            outletNo: {
                number: true
            },
            outletWidth: {
                number: true
            },
            outletHeight: {
                number: true
            },
            invertLevel: {
                number: true
            },
            crestTopLevel: {
                number: true
            },
            crestTopWidth: {
                number: true
            },
            dpInstallDateEditRow: {
                required: true,
                dateITA: true
            }
        }
    });

});

gatesBasedOnEditAssetTypeTableFunctions = function () {
    var gateTypeRowEditing = null;
    gateTable = $('#gatesBasedOnEditAssetTypeList').DataTable({
        responsive: true,
        paging: false,
        info: false,
        searching: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [2],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [3],
                visible: false,
                orderable: false,
                searchable: false
            }           
        ],
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        buttons: [
            {
                text: '+ Add New Gate Type',
                className: 'btn-success addNewGateTypeBtnEditRow',
                action: function (e, dt, node, config) {
                    e.preventDefault();                        

                    gateTable.row.add([
                          '',
                          '<input id="gateGuidEditRow" name="gateGuidEditRow" class="form-control gateGuidEditRow" type="text"/>',
                          '<input id="gateTypeGuidEditRow" name="gateTypeGuidEditRow" class="form-control gateTypeGuidEditRow" type="text"/>',
                          '<input id="gateConstructionMaterialGuidEditRow" name="gateConstructionMaterialGuidEditRow" class="form-control gateConstructionMaterialGuidEditRow" type="text"/>',
                          '<select id="gateTypeSelectEditRow"  name="gateTypeSelectEditRow" class="form-control gateTypeSelectEditRow"></select>',
                          '<select id="gateConstructionMaterialSelectEditRow"  name="gateConstructionMaterialSelectEditRow" class="form-control gateConstructionMaterialSelectEditRow"></select>',
                          '<input id="numberOfGatesEditRow" name="numberOfGatesEditRow" class="form-control numberOfGatesEditRow" type="text" size="3"/>',
                          '<input id="heightEditRow" name="heightEditRow" class="form-control heightEditRow" type="text" size="3"/>',
                          '<input id="widthEditRow" name="widthEditRow" class="form-control widthEditRow" type="text" size="3"/>',
                          '<input id="diameteEditRowr" name="diameterEditRow" class="form-control diameterEditRow" type="text" size="3"/>',
                          '<div id=\"dpInstallDateEditRow\" class=\"input-group date\"><input type=\"text\" name=\"installDateEditRow\" class=\"form-control\" id=\"installDateEditRow\" placeholder=\"Install Date (dd/mm/yyyy)\" /><span class=\"input-group-addon form-control-static\"><i class=\"fa fa-calendar fa-fw\"></i></span></div>',
                          '<a href="#" class="saveGateTypeNewRowBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelGateTypeNewRowBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>'
                    ]).draw(false);

                    getGateType();
                    getGateConstructionMaterial();

                    $('#dpInstallDateEditRow').datepicker({
                        format: "dd/mm/yyyy",
                    });

                    gateTable.button(['.addNewGateTypeBtnEditRow']).disable();

                }
            }
        ]
    });


    gateTable.on('order.dt search.dt', function () {
        gateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $('#assetTypePanel table#gatesBasedOnEditAssetTypeList').on('click', 'a.editGateTypeBtn', function (e) {
        e.preventDefault();

        gateTable.button(['.addNewGateTypeBtnEditRow']).disable();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        getGateType();
        getGateConstructionMaterial();

        if (gateTypeRowEditing !== null && gateTypeRowEditing != selectedRow) {

            /* A different row is being edited - the edit should be cancelled and this row edited */

            restoreEditGateData(gateTable, gateTypeRowEditing);
            editGateTableRow(gateTable, selectedRow);
            gateTypeRowEditing = selectedRow;

            //var el = $('#assetTypePanel table#gatesBasedOnEditAssetTypeList');
            //var elemLen = el.val().length;
            //el.focus();


            //el.setCursorPosition(elemLen);
        }
        else {
            /* No row currently being edited */
            editGateTableRow(gateTable, selectedRow);
            gateTypeRowEditing = selectedRow;

            //var el = $('#assetTypePanel table#gatesBasedOnEditAssetTypeList');
            //var elemLen = el.val().length;
            //el.focus();

            //el.setCursorPosition(elemLen);
        }
    });

    $('#assetTypePanel table#gatesBasedOnEditAssetTypeList').on('click', 'a.deleteGateTypeBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        var gateTableRowDeleted = deleteGateTableRow(gateTable.row(selectedRow).data()[1]);

        if (gateTableRowDeleted == 1) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Your Gate is deleted successfully.", function () {
                gateTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (gateTableRowDeleted == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
        }

    });

    $('#assetTypePanel table#gatesBasedOnEditAssetTypeList').on('click', 'a.saveGateTypeNewRowBtn', function (e) {
        e.preventDefault();
        var gateTable = $('#gatesBasedOnEditAssetTypeList').DataTable();

        var selectedRow = $(this).parents('tr')[0];

        saveNewGateRow(gateTable, selectedRow);

        gateTable.button(['.addNewGateTypeBtnEditRow']).enable();

    });

    $('#assetTypePanel table#gatesBasedOnEditAssetTypeList').on('click', 'a.updateGateTypeEditRowBtn', function (e) {
        e.preventDefault();
        var gateTable = $('#gatesBasedOnEditAssetTypeList').DataTable();

        var selectedRow = $(this).parents('tr')[0];

        updateEditGateRow(gateTable, selectedRow);

        gateTable.button(['.addNewGateTypeBtnEditRow']).enable();

    });

    $('#assetTypePanel table#gatesBasedOnEditAssetTypeList').on('click', 'a.cancelGateTypeNewRowBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        gateTable.row(selectedRow).remove().draw(false);

        gateTable.button(['.addNewGateTypeBtnEditRow']).enable();

    });

    $('#assetTypePanel table#gatesBasedOnEditAssetTypeList').on('click', 'a.cancelGateTypeEditRowBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        var selectedRowData = gateTable.row(selectedRow).data();

        var availableTds = $('>td', selectedRow);

        var selectedRowSerialNum = availableTds[0].innerHTML;

        var selectedGateTypeGuidRow = gateTable.row(selectedRow).data()[1];
        var selectedGateConstructionMaterialGuidRow = gateTable.row(selectedRow).data()[2];

        var selectedGateType = gateTable.row(selectedRow).data()[3];
        var selectedGateConstructionMaterialRow = gateTable.row(selectedRow).data()[4];

        selectedRowData[1] = selectedGateTypeGuidRow;
        selectedRowData[2] = selectedGateConstructionMaterialGuidRow;
        selectedRowData[3] = selectedGateType;
        selectedRowData[4] = selectedGateConstructionMaterialRow;

        gateTable.row(selectedRow).data(selectedRowData);

        availableTds[0].innerHTML = selectedRowSerialNum;

        gateTable.button(['.addNewGateTypeBtnEditRow']).enable();
    });

}

getGateType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetGateType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {            
            gateTypeData = response.d;            
            showGateType('gateTypeSelectEditRow', gateTypeData);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getGateConstructionMaterial = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetGateConstructionMaterial",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {           
            gateConstructionMaterialData = response.d;

            showGateConstructionMaterial('gateConstructionMaterialSelectEditRow', gateConstructionMaterialData);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

showGateType = function (selectId, arr) {
    var option_str_gate = document.getElementById(selectId);
    option_str_gate.length = 0;
    //option_str_gate.options[0] = new Option('--Select Gate Type--', '');
    option_str_gate.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_gate.options[option_str_gate.length] = new Option(arr[i].GateTypeName, arr[i].GateTypeGuid);
        //i += 3;
    }
}

showGateConstructionMaterial = function (selectId, arr) {
    var option_str_gate_construction_material = document.getElementById(selectId);
    option_str_gate_construction_material.length = 0;
    //option_str_gate_construction_material.options[0] = new Option('--Select Construction Material--', '');
    option_str_gate_construction_material.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_gate_construction_material.options[option_str_gate_construction_material.length] = new Option(arr[i].GateConstructionMaterialName, arr[i].GateConstructionMaterialGuid);
        //i += 3;
    }
}

editGateTableRow = function (gateTable, selectedRow) {
   
    var selectedRowData = gateTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    var gateTypeGuid = gateTable.row(selectedRow).data()[2];
    var gateConstructionMaterialGuid = gateTable.row(selectedRow).data()[3];

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.  
                          
    var htmlGateType = '<select name=gateTypeSelectEditRow class=form-control gateTypeSelectEditRow>';

    for (var i = 0; i < gateTypeData.length; i++) {
        if (gateTypeData[i].GateTypeGuid == gateTypeGuid) {
            htmlGateType += '<option selected="selected" value="' + gateTypeData[i].GateTypeGuid + '">' + gateTypeData[i].GateTypeName + '</option>';
        }
        else {
            htmlGateType += '<option value="' + gateTypeData[i].GateTypeGuid + '">' + gateTypeData[i].GateTypeName + '</option>';
        }

    }

    htmlGateType += '</select>';
    
    var htmlGateConstructionMaterial = '<select name=gateConstructionMaterialSelectEditRow class=form-control gateConstructionMaterialSelectEditRow>';

    for (var i = 0; i < gateConstructionMaterialData.length; i++) {

        if (gateConstructionMaterialData[i].GateConstructionMaterialGuid == gateConstructionMaterialGuid) {
            htmlGateConstructionMaterial += '<option selected="selected" value="' + gateConstructionMaterialData[i].GateConstructionMaterialGuid + '">' + gateConstructionMaterialData[i].GateConstructionMaterialName + '</option>';
        }
        else {
            htmlGateConstructionMaterial += '<option value="' + gateConstructionMaterialData[i].GateConstructionMaterialGuid + '">' + gateConstructionMaterialData[i].GateConstructionMaterialName + '</option>';
        }

    }

    htmlGateConstructionMaterial += '</select>';

    availableTds[1].innerHTML = htmlGateType;

    availableTds[2].innerHTML = htmlGateConstructionMaterial;

    availableTds[3].innerHTML = '<input id="numberOfGatesEditRow" name="numberOfGatesEditRow" class="form-control numberOfGatesEditRow" type="text" size="3" value= "' + gateTable.row(selectedRow).data()[6] + '"/>',
    availableTds[4].innerHTML = '<input id="heightEditRow" name="heightEditRow" class="form-control heightEditRow" type="text" size="3" value= "' + gateTable.row(selectedRow).data()[7] + '"/>',
    availableTds[5].innerHTML = '<input id="widthEditRow" name="widthEditRow" class="form-control widthEditRow" type="text" size="3" value= "' + gateTable.row(selectedRow).data()[8] + '"/>',
    availableTds[6].innerHTML = '<input id="diameteEditRowr" name="diameterEditRow" class="form-control diameterEditRow" type="text" size="3" value= "' + gateTable.row(selectedRow).data()[9] + '"/>',
    availableTds[7].innerHTML = '<div id=\"dpInstallDateEditRow\" class=\"input-group date\"><input type=\"text\" name=\"installDateEditRow\" class=\"form-control\" id=\"installDateEditRow\" placeholder=\"Install Date (dd/mm/yyyy)\" value= "' + gateTable.row(selectedRow).data()[10].toString("dd/MM/yyyy") + '" /><span class=\"input-group-addon form-control-static\"><i class=\"fa fa-calendar fa-fw\"></i></span></div>',

    availableTds[8].innerHTML = '<a href="#" class="updateGateTypeEditRowBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Update" title="Update"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelGateTypeEditRowBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';

    $('#dpInstallDateEditRow').datepicker({
        format: "dd/mm/yyyy",
    });
}

deleteGateTableRow = function (gateGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/DeleteGateData",
        data: '{"gateGuid":"' + gateGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {          
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveNewGateRow = function (gateTable, selectedRow) {

    var selectedRowData = gateTable.row(selectedRow).data();

    var availableSelects = $('select', selectedRow);
    var availableInputs = $('input', selectedRow);

    selectedRowData[1] = "00000000-0000-0000-0000-000000000000"
    selectedRowData[2] = availableSelects[0].value
    selectedRowData[3] = availableSelects[1].value
    selectedRowData[4] = availableSelects[0].options[availableSelects[0].selectedIndex].text
    selectedRowData[5] = availableSelects[1].options[availableSelects[1].selectedIndex].text
    selectedRowData[6] = availableInputs[0].value
    selectedRowData[7] = availableInputs[1].value
    selectedRowData[8] = availableInputs[2].value
    selectedRowData[9] = availableInputs[3].value
    selectedRowData[10] = availableInputs[4].value
    selectedRowData[11] = '<a href="#" class="editGateTypeBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteGateTypeBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;"data-original-title="Delete" title="Delete"></i></a>'

    gateTable.row(selectedRow).data(selectedRowData);

    gateTable.on('order.dt search.dt', function () {
        gateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

restoreEditGateData = function (gateTable, previousRow) {
    var previousRowData = gateTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
       
    } else {
        gateTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;
    }

}

updateEditGateRow = function (gateTable, selectedRow) {

    var selectedRowData = gateTable.row(selectedRow).data();

    var availableSelects = $('select', selectedRow);
    var availableInputs = $('input', selectedRow);

    selectedRowData[1] = gateTable.row(selectedRow).data()[1]
    selectedRowData[2] = availableSelects[0].value
    selectedRowData[3] = availableSelects[1].value    
    selectedRowData[4] = availableSelects[0].options[availableSelects[0].selectedIndex].text
    selectedRowData[5] = availableSelects[1].options[availableSelects[1].selectedIndex].text
    selectedRowData[6] = availableInputs[0].value
    selectedRowData[7] = availableInputs[1].value
    selectedRowData[8] = availableInputs[2].value    
    selectedRowData[9] = availableInputs[3].value
    selectedRowData[10] = availableInputs[4].value
    selectedRowData[11] = '<a href="#" class="editGateTypeBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteGateTypeBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;"data-original-title="Delete" title="Delete"></i></a>'
    gateTable.row(selectedRow).data(selectedRowData);

    gateTable.on('order.dt search.dt', function () {
        gateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

updateAsset = function () {
    var returnValue = 0;

    var assetTypeManager = {
        assetTypeCode: $('#assetTypeCode').val()
    }

    var initiationDate = $('#dateOfInitiationOfAsset').val();
    initiationDate = Date.parseExact(initiationDate, 'dd/MM/yyyy').toString('yyyy/MM/dd')

    var completionDate = $('#dateOfCompletionOfAsset').val();
    completionDate = Date.parseExact(completionDate, 'dd/MM/yyyy').toString('yyyy/MM/dd')

    var keyAssetGuid = stringIsNullOrEmpty($("#keyAssetList").val()) ? '00000000-0000-0000-0000-000000000000' : $("#keyAssetList").val()

    var assetCost = $("#assetCost").val();
    var newchar = '';
    assetCost = assetCost.split(',').join(newchar);
    assetCost = stringIsNullOrEmpty($.trim(assetCost)) ? parseFloat(0) : parseFloat(assetCost);

    var assetManager = {
        assetCode: $('#assetCode').val(),
        drawingCode: $('#drawingCode').val(),
        amtd: $('#amtd').val(),
        assetName: $('#assetName').val(),
        startChainage: stringIsNullOrEmpty($.trim($('#chainageStart').val())) ? 0 : parseInt($.trim($('#chainageStart').val())),
        endChainage: stringIsNullOrEmpty($.trim($('#chainageEnd').val())) ? 0 : parseInt($.trim($('#chainageEnd').val())),
        utmEast1: stringIsNullOrEmpty($.trim($('#eastingUS').val())) ? 0 : parseInt($.trim($('#eastingUS').val())),
        utmEast2: stringIsNullOrEmpty($.trim($('#eastingDS').val())) ? 0 : parseInt($.trim($('#eastingDS').val())),
        utmNorth1: stringIsNullOrEmpty($.trim($('#northingUS').val())) ? 0 : parseInt($.trim($('#northingUS').val())),
        utmNorth2: stringIsNullOrEmpty($.trim($('#northingDS').val())) ? 0 : parseInt($.trim($('#northingDS').val())),
        initiationDate: initiationDate,
        completionDate: completionDate,
        assetCostInput: assetCost,
        //divisionManager: divisionManager,
        //schemeManager: schemeManager,
        assetTypeManager: assetTypeManager,
        keyAssetGuid: keyAssetGuid
    }

    //var assetTypeCode = $("#assetTypeList option:selected").text().split("-");

    switch (assetTypeManager.assetTypeCode) {
        case 'BRG':
            var bridgeTypeManager = {
                bridgeTypeGuid: $('#bridgeTypeSelect').val(),
            }

            var bridgeManager = {
                bridgeGuid: $('#bridgeGuidHidden').val(),
                length: stringIsNullOrEmpty($.trim($('#lengthBridge').val())) ? 0 : parseFloat($.trim($('#lengthBridge').val())),
                pierNumber: stringIsNullOrEmpty($.trim($('#numberPeirs').val())) ? 0 : parseInt($.trim($('#numberPeirs').val())),
                roadWidth: stringIsNullOrEmpty($.trim($('#roadWidth').val())) ? 0 : parseFloat($.trim($('#roadWidth').val())),
                bridgeTypeManager: bridgeTypeManager
            }

            assetManager.bridgeManager = bridgeManager;

            returnValue = updateAssetWithBridge(assetManager);
            break;
        case 'CAN':
            var canalLiningTypeManager = {
                liningTypeGuid: $('#liningTypeSelect').val(),
            }

            var canalManager = {
                canalGuid: $('#canalGuidHidden').val(),
                usElevation: stringIsNullOrEmpty($.trim($('#uSCrestElevationCanal').val())) ? 0 : parseFloat($.trim($('#uSCrestElevationCanal').val())),
                dsElevation: stringIsNullOrEmpty($.trim($('#dSCrestElevationCanal').val())) ? 0 : parseFloat($.trim($('#dSCrestElevationCanal').val())),
                length: stringIsNullOrEmpty($.trim($('#lengthCanal').val())) ? 0 : parseFloat($.trim($('#lengthCanal').val())),
                bedWidth: stringIsNullOrEmpty($.trim($('#bedWidthCanal').val())) ? 0 : parseFloat($.trim($('#bedWidthCanal').val())),
                slope1: stringIsNullOrEmpty($.trim($('#slipeSlope1Canal').val())) ? 0 : parseInt($.trim($('#slipeSlope1Canal').val())),
                slope2: stringIsNullOrEmpty($.trim($('#slipeSlope2Canal').val())) ? 0 : parseInt($.trim($('#slipeSlope2Canal').val())),
                averageDepth: stringIsNullOrEmpty($.trim($('#averageDepthCanal').val())) ? 0 : parseFloat($.trim($('#averageDepthCanal').val())),
                canalLiningTypeManager: canalLiningTypeManager
            }

            assetManager.canalManager = canalManager;

            returnValue = updateAssetWithCanal(assetManager);
            break;
        case 'CUL':
            var culvertTypeManager = {
                culvertTypeGuid: $('#culvertTypeSelect').val(),
            }

            var culvertManager = {
                culvertGuid: $('#culvertGuidHidden').val(),
                ventHeight: stringIsNullOrEmpty($.trim($('#ventHeight').val())) ? 0 : parseFloat($.trim($('#ventHeight').val())),
                ventWidth: stringIsNullOrEmpty($.trim($('#ventWidth').val())) ? 0 : parseFloat($.trim($('#ventWidth').val())),
                lengthCulvert: stringIsNullOrEmpty($.trim($('#lengthCulvert').val())) ? 0 : parseFloat($.trim($('#lengthCulvert').val())),
                culvertTypeManager: culvertTypeManager
            }

            assetManager.culvertManager = culvertManager;

            returnValue = updateAssetWithCulvert(assetManager);
            break;
        case 'DRN':
            var drainageManager = {
                drainageGuid: $('#drainageGuidHidden').val(),
                usElevationDrainage: stringIsNullOrEmpty($.trim($('#usBedElevationDrainage').val())) ? 0 : parseFloat($.trim($('#usBedElevationDrainage').val())),
                dsElevationDrainage: stringIsNullOrEmpty($.trim($('#dsBedElevationDrainage').val())) ? 0 : parseFloat($.trim($('#dsBedElevationDrainage').val())),
                lengthDrainage: stringIsNullOrEmpty($.trim($('#lengthDrainage').val())) ? 0 : parseFloat($.trim($('#lengthDrainage').val())),
                bedWidthDrainage: stringIsNullOrEmpty($.trim($('#bedWidthDrainage').val())) ? 0 : parseFloat($.trim($('#bedWidthDrainage').val())),
                slope1Drainage: stringIsNullOrEmpty($.trim($('#slope1Drainage').val())) ? 0 : parseInt($.trim($('#slope1Drainage').val())),
                slope2Drainage: stringIsNullOrEmpty($.trim($('#slope2Drainage').val())) ? 0 : parseInt($.trim($('#slope2Drainage').val())),
                averageDepthDrainage: stringIsNullOrEmpty($.trim($('#averageDepthDrainage').val())) ? 0 : parseFloat($.trim($('#averageDepthDrainage').val()))
            }

            assetManager.drainageManager = drainageManager;

            returnValue = updateAssetWithDrainage(assetManager);
            break;
        case 'DRP':
            var dropStructureManager = {
                dropStructureGuid: $('#dropStructureGuidHidden').val(),
                usInvertLevel: stringIsNullOrEmpty($.trim($('#usInvertLevel').val())) ? 0 : parseFloat($.trim($('#usInvertLevel').val())),
                dsInvertLevel: stringIsNullOrEmpty($.trim($('#dsInvertLevel').val())) ? 0 : parseFloat($.trim($('#dsInvertLevel').val())),
                stillingBasinLength: stringIsNullOrEmpty($.trim($('#stillingBasinLength').val())) ? 0 : parseFloat($.trim($('#stillingBasinLength').val())),
                stillingBasinWidth: stringIsNullOrEmpty($.trim($('#stillingBasinWidth').val())) ? 0 : parseFloat($.trim($('#stillingBasinWidth').val()))
            }

            assetManager.dropStructureManager = dropStructureManager;

            returnValue = updateAssetWithDropStructure(assetManager);
            break;
        case 'EMB':
            var embankmentCrestTypeManager = {
                crestTypeGuid: $('#crestTypeSelect').val(),
            }

            var embankmentFillTypeManager = {
                fillTypeGuid: $('#fillTypeSelect').val(),
            }

            var embankmentManager = {
                embankmentGuid: $('#embankmentGuidHidden').val(),
                usElevation: stringIsNullOrEmpty($.trim($('#uSCrestElevation').val())) ? 0 : parseFloat($.trim($('#uSCrestElevation').val())),
                dsElevation: stringIsNullOrEmpty($.trim($('#dSCrestElevation').val())) ? 0 : parseFloat($.trim($('#dSCrestElevation').val())),
                length: stringIsNullOrEmpty($.trim($('#length').val())) ? 0 : parseFloat($.trim($('#length').val())),
                crestWidth: stringIsNullOrEmpty($.trim($('#crestWidth').val())) ? 0 : parseFloat($.trim($('#crestWidth').val())),
                csSlope1: stringIsNullOrEmpty($.trim($('#csSlope1').val())) ? 0 : parseFloat($.trim($('#csSlope1').val())),
                csSlope2: stringIsNullOrEmpty($.trim($('#csSlope2').val())) ? 0 : parseFloat($.trim($('#csSlope2').val())),
                rsSlope1: stringIsNullOrEmpty($.trim($('#rsSlope1').val())) ? 0 : parseFloat($.trim($('#rsSlope1').val())),
                rsSlope2: stringIsNullOrEmpty($.trim($('#rsSlope2').val())) ? 0 : parseFloat($.trim($('#rsSlope2').val())),
                bermSlope1: stringIsNullOrEmpty($.trim($('#bermSlope1').val())) ? 0 : parseFloat($.trim($('#bermSlope1').val())),
                bermSlope2: stringIsNullOrEmpty($.trim($('#bermSlope2').val())) ? 0 : parseFloat($.trim($('#bermSlope2').val())),
                platformWidth: stringIsNullOrEmpty($.trim($('#platformWidth').val())) ? 0 : parseFloat($.trim($('#platformWidth').val())),
                averageHeight: stringIsNullOrEmpty($.trim($('#averageHeight').val())) ? 0 : parseFloat($.trim($('#averageHeight').val())),
                embankmentCrestTypeManager: embankmentCrestTypeManager,
                embankmentFillTypeManager: embankmentFillTypeManager
            }

            assetManager.embankmentManager = embankmentManager;

            returnValue = updateAssetWithEmbankment(assetManager);
            break;
        case 'GAU':
            var gaugeTypeManager = {
                gaugeTypeGuid: $('#gaugeTypeSelect').val(),
            }

            var gaugeSeasonTypeManager = {
                seasonTypeGuid: $('#seasonTypeSelect').val(),
            }

            var gaugeFrequencyTypeManager = {
                frequencyTypeGuid: $('#frequencyTypeSelect').val(),
            }

            var startDate = $('#startDate').val();
            startDate = stringIsNullOrEmpty($.trim($('#startDate').val())) ? "" : Date.parseExact(startDate, 'dd/MM/yyyy').toString('yyyy/MM/dd');

            var endDate = $('#endDate').val();
            endDate = stringIsNullOrEmpty($.trim($('#endDate').val())) ? "" : Date.parseExact(endDate, 'dd/MM/yyyy').toString('yyyy/MM/dd');

            var gaugeManager = {
                gaugeGuid: $('#gaugeGuidHidden').val(),
                startDate: startDate,
                endDate: endDate,
                active: $('#active').is(':checked'),
                lwl: $('#lwlPresent').is(':checked'),
                hwl: $('#hwlPresent').is(':checked'),
                levelGeo: $('#levelGeo').is(':checked'),
                zeroDatum: stringIsNullOrEmpty($.trim($('#zeroDatum').val())) ? 0 : parseFloat($.trim($('#zeroDatum').val())),
                gaugeTypeManager: gaugeTypeManager,
                gaugeFrequencyTypeManager: gaugeFrequencyTypeManager,
                gaugeSeasonTypeManager: gaugeSeasonTypeManager
            }

            assetManager.gaugeManager = gaugeManager;

            returnValue = updateAssetWithGauge(assetManager);
            break;
        case 'POR':
            var porcupineTypeManager = {
                porcupineTypeGuid: $('#porcupineTypeSelect').val(),
            }

            var porcupineMaterialTypeManager = {
                porcupineMaterialTypeGuid: $('#materialTypeSelect').val(),
            }

            var porcupineManager = {
                porcupineGuid: $('#porcupineGuidHidden').val(),
                screenLength: stringIsNullOrEmpty($.trim($('#screenLength').val())) ? 0 : parseFloat($.trim($('#screenLength').val())),
                spacingAlongScreen: stringIsNullOrEmpty($.trim($('#spacingAlongScreen').val())) ? 0 : parseFloat($.trim($('#spacingAlongScreen').val())),
                screenRows: stringIsNullOrEmpty($.trim($('#screenRows').val())) ? 0 : parseInt($.trim($('#screenRows').val())),
                numberOfLayers: stringIsNullOrEmpty($.trim($('#noOfLayers').val())) ? 0 : parseInt($.trim($('#noOfLayers').val())),
                memberLength: stringIsNullOrEmpty($.trim($('#mamberLength').val())) ? 0 : parseFloat($.trim($('#mamberLength').val())),
                lengthPorcupine: stringIsNullOrEmpty($.trim($('#lengthPorcupine').val())) ? 0 : parseFloat($.trim($('#lengthPorcupine').val())),
                porcupineTypeManager: porcupineTypeManager,
                porcupineMaterialTypeManager: porcupineMaterialTypeManager,
            }

            assetManager.porcupineManager = porcupineManager;

            returnValue = updateAssetWithPorcupine(assetManager);
            break;
        case 'REG':
            var output = gateTable.buttons.exportData();

            var regulatorTypeManager = {
                regulatorTypeGuid: $('#regulatorTypeSelect').val(),
            }

            var regulatorGateTypeManager = {
                regulatorGateTypeGuid: $('#regulatorGateTypeSelect').val(),
            }

            var gateManager = [];

            for (var i = 0; i < output.body.length; i++) {
                var gateTypeManager = {
                    gateTypeGuid: output.body[i][2],
                };

                var gateConstructionMaterialManager = {
                    gateConstructionMaterialGuid: output.body[i][3],
                }

                gateManager.push({
                    gateGuid: output.body[i][1],
                    gateTypeManager: gateTypeManager,
                    gateConstructionMaterialManager: gateConstructionMaterialManager,
                    numberOfGates: output.body[i][6],
                    height: stringIsNullOrEmpty(output.body[i][7]) ? 0 : parseFloat(output.body[i][7]),
                    width: stringIsNullOrEmpty(output.body[i][8]) ? 0 : parseFloat(output.body[i][8]),
                    diameter: stringIsNullOrEmpty(output.body[i][9]) ? 0 : parseFloat(output.body[i][9]),
                    installDate: Date.parseExact(output.body[i][10], 'dd/MM/yyyy').toString('yyyy/MM/dd'),//output.body[i][9].toString("yyyy/MM/dd"),
                });
            }

            var regulatorManager = {
                regulatorGuid: $('#regulatorGuidHidden').val(),
                ventNumberRegulator: stringIsNullOrEmpty($.trim($('#numberOfVentsRegulator').val())) ? 0 : parseFloat($.trim($('#numberOfVentsRegulator').val())),
                ventHeightRegulator: stringIsNullOrEmpty($.trim($('#ventHeightRegulator').val())) ? 0 : parseFloat($.trim($('#ventHeightRegulator').val())),
                ventWidthRegulator: stringIsNullOrEmpty($.trim($('#ventWidthRegulator').val())) ? 0 : parseFloat($.trim($('#ventWidthRegulator').val())),
                regulatorTypeManager: regulatorTypeManager,
                regulatorGateTypeManager: regulatorGateTypeManager,
                gateManagerList: gateManager
            }

            assetManager.regulatorManager = regulatorManager;

            returnValue = updateAssetWithRegulator(assetManager);
            break;
        case 'REV':
            var revetmentTypeManager = {
                revetTypeGuid: $('#revetmentTypeSelect').val(),
            }

            var revetmentRiverprotTypeManager = {
                riverprotTypeGuid: $('#riverProtectionTypeSelect').val(),
            }

            var revetmentWaveprotTypeManager = {
                waveprotTypeGuid: $('#waveProtectionTypeSelect').val(),
            }

            var revetmentManager = {
                revetmentGuid: $('#revetmentGuidHidden').val(),
                lengthRevetment: stringIsNullOrEmpty($.trim($('#lengthRevetment').val())) ? 0 : parseFloat($.trim($('#lengthRevetment').val())),
                plainWidth: stringIsNullOrEmpty($.trim($('#plainWidth').val())) ? 0 : parseFloat($.trim($('#plainWidth').val())),
                slopeRevetment: stringIsNullOrEmpty($.trim($('#slopeRevetment').val())) ? 0 : parseFloat($.trim($('#slopeRevetment').val())),
                revetmentTypeManager: revetmentTypeManager,
                revetmentRiverprotTypeManager: revetmentRiverprotTypeManager,
                revetmentWaveprotTypeManager: revetmentWaveprotTypeManager
            }

            assetManager.revetmentManager = revetmentManager;

            returnValue = updateAssetWithRevetment(assetManager);
            break;
        case 'SLU':
            var output = gateTable.buttons.exportData();

            var sluiceTypeManager = {
                sluiceTypeGuid: $('#sluiceTypeSelect').val(),
            }

            var gateManager = [];

            for (var i = 0; i < output.body.length; i++) {
                var gateTypeManager = {
                    gateTypeGuid: output.body[i][2],
                };

                var gateConstructionMaterialManager = {
                    gateConstructionMaterialGuid: output.body[i][3],
                }

                gateManager.push({
                    gateGuid: output.body[i][1],
                    gateTypeManager: gateTypeManager,
                    gateConstructionMaterialManager: gateConstructionMaterialManager,
                    numberOfGates: output.body[i][6],
                    height: stringIsNullOrEmpty(output.body[i][7]) ? 0 : parseFloat(output.body[i][7]),
                    width: stringIsNullOrEmpty(output.body[i][8]) ? 0 : parseFloat(output.body[i][8]),
                    diameter: stringIsNullOrEmpty(output.body[i][9]) ? 0 : parseFloat(output.body[i][9]),
                    installDate: Date.parseExact(output.body[i][10], 'dd/MM/yyyy').toString('yyyy/MM/dd'),//output.body[i][9].toString("yyyy/MM/dd"),
                });
            }

            var sluiceManager = {
                sluiceGuid: $('#sluiceGuidHidden').val(),
                ventNumberSluice: stringIsNullOrEmpty($.trim($('#NoOfVentsSluice').val())) ? 0 : parseInt($.trim($('#NoOfVentsSluice').val())),
                ventDiameterSluice: stringIsNullOrEmpty($.trim($('#ventDiameterSluice').val())) ? 0 : parseFloat($.trim($('#ventDiameterSluice').val())),
                ventHeightSluice: stringIsNullOrEmpty($.trim($('#ventHeightSluice').val())) ? 0 : parseFloat($.trim($('#ventHeightSluice').val())),
                ventWidthSluice: stringIsNullOrEmpty($.trim($('#ventWidthSluice').val())) ? 0 : parseFloat($.trim($('#ventWidthSluice').val())),
                sluiceTypeManager: sluiceTypeManager,
                gateManagerList: gateManager
            }

            assetManager.sluiceManager = sluiceManager;

            returnValue = updateAssetWithSluice(assetManager);
            break;
        case 'SPU':
            var spurConstructionTypeManager = {
                constructionTypeGuid: $('#constructionTypeSelect').val(),
            }

            var spurRevetTypeManager = {
                spurRevetTypeGuid: $('#spurRevetTypeSelect').val(),
            }

            var spurShapeTypeManager = {
                shapeTypeGuid: $('#shapeTypeSelect').val(),
            }

            var spurManager = {
                spurGuid: $('#spurGuidHidden').val(),
                orientation: stringIsNullOrEmpty($.trim($('#orientation').val())) ? 0 : parseFloat($.trim($('#orientation').val())),
                lengthSpur: stringIsNullOrEmpty($.trim($('#lengthSpur').val())) ? 0 : parseFloat($.trim($('#lengthSpur').val())),
                widthSpur: stringIsNullOrEmpty($.trim($('#widthSpur').val())) ? 0 : parseFloat($.trim($('#widthSpur').val())),
                spurConstructionTypeManager: spurConstructionTypeManager,
                spurRevetTypeManager: spurRevetTypeManager,
                spurShapeTypeManager: spurShapeTypeManager
            }

            assetManager.spurManager = spurManager;

            returnValue = updateAssetWithSpur(assetManager);
            break;
        case 'TRN':
            var output = gateTable.buttons.exportData();

            var turnoutTypeManager = {
                turnoutTypeGuid: $('#turnoutTypeSelect').val(),
            }

            var turnoutGateTypeManager = {
                turnoutGateTypeGuid: $('#turnoutGateTypeSelect').val(),
            }

            var gateManager = [];

            for (var i = 0; i < output.body.length; i++) {
                var gateTypeManager = {
                    gateTypeGuid: output.body[i][2],
                };

                var gateConstructionMaterialManager = {
                    gateConstructionMaterialGuid: output.body[i][3],
                }

                gateManager.push({
                    gateGuid: output.body[i][1],
                    gateTypeManager: gateTypeManager,
                    gateConstructionMaterialManager: gateConstructionMaterialManager,
                    numberOfGates: output.body[i][6],
                    height: stringIsNullOrEmpty(output.body[i][7]) ? 0 : parseFloat(output.body[i][7]),
                    width: stringIsNullOrEmpty(output.body[i][8]) ? 0 : parseFloat(output.body[i][8]),
                    diameter: stringIsNullOrEmpty(output.body[i][9]) ? 0 : parseFloat(output.body[i][9]),
                    installDate: Date.parseExact(output.body[i][10], 'dd/MM/yyyy').toString('yyyy/MM/dd'),//output.body[i][9].toString("yyyy/MM/dd"),
                });
            }

            var turnoutManager = {
                turnoutGuid: $('#turnoutGuidHidden').val(),
                inletboxWidth: stringIsNullOrEmpty($.trim($('#inletBoxWidth').val())) ? 0 : parseFloat($.trim($('#inletBoxWidth').val())),
                inletboxLength: stringIsNullOrEmpty($.trim($('#inletBoxLength').val())) ? 0 : parseFloat($.trim($('#inletBoxLength').val())),
                outletNumber: stringIsNullOrEmpty($.trim($('#outletNo').val())) ? 0 : parseInt($.trim($('#outletNo').val())),
                outletWidth: stringIsNullOrEmpty($.trim($('#outletWidth').val())) ? 0 : parseFloat($.trim($('#outletWidth').val())),
                outletHeight: stringIsNullOrEmpty($.trim($('#outletHeight').val())) ? 0 : parseFloat($.trim($('#outletHeight').val())),
                turnoutTypeManager: turnoutTypeManager,
                turnoutGateTypeManager: turnoutGateTypeManager,
                gateManagerList: gateManager
            }

            assetManager.turnoutManager = turnoutManager;

            returnValue = updateAssetWithTurnout(assetManager);
            break;
        case 'WEI':
            weirTypeManager = {
                weirTypeGuid: $('#weirTypeSelect').val(),
            }

            weirManager = {
                weirGuid: $('#weirGuidHidden').val(),
                invertLevel: stringIsNullOrEmpty($.trim($('#invertLevel').val())) ? 0 : parseFloat($.trim($('#invertLevel').val())),
                crestTopLevel: stringIsNullOrEmpty($.trim($('#crestTopLevel').val())) ? 0 : parseFloat($.trim($('#crestTopLevel').val())),
                crestTopWidth: stringIsNullOrEmpty($.trim($('#crestTopWidth').val())) ? 0 : parseFloat($.trim($('#crestTopWidth').val())),
                weirTypeManager: weirTypeManager
            }

            assetManager.weirManager = weirManager;

            returnValue = updateAssetWithWeir(assetManager);
            break;
        default: document.write("Unknown grade<br />");
    }

    return returnValue;

}

updateAssetWithBridge = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithBridge",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateAssetWithCanal = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithCanal",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateAssetWithCulvert = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithCulvert",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateAssetWithDrainage = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithDrainage",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateAssetWithDropStructure = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithDropStructure",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateAssetWithEmbankment = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithEmbankment",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;       
}

updateAssetWithGauge = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithGauge",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateAssetWithPorcupine = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithPorcupine",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateAssetWithRegulator = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithRegulator",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateAssetWithRevetment = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithRevetment",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateAssetWithSluice = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithSluice",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateAssetWithSpur = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithSpur",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateAssetWithTurnout = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithTurnout",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateAssetWithWeir = function (assetManager) {
    var retValue = 0;

    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithWeir",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert(msg);
        }
    });

    return retValue;
}