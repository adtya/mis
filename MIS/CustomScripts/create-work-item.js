﻿$(document).ready(function () {
        
    $('#createWorkItemBtn').on('click', function (e) {
        var workItemName = $("#workItemName").val();
        var workItemUnitGuid = $("#workItemUnitList").val();
        var arrWorkItemSubHeadTitles = [];
        //var arrWorkTypeUnits = [];

        //arrWorkTypeSubHeadTitle.push($('#estimateQtyDrop').val());
        arrWorkItemSubHeadTitles.push($('#estimatedQtyName').val());
        arrWorkItemSubHeadTitles.push($('#reportingMonth').val());
        arrWorkItemSubHeadTitles.push($('#cumulativeValue').val());
        arrWorkItemSubHeadTitles.push($('#balance').val());

        var arrWorkItemSubHeadTitlesJSONString = JSON.stringify(arrWorkItemSubHeadTitles);

        //arrWorkTypeUnits.push($('#UnitsDrop').val());
        //arrWorkTypeUnits.push($('#UnitsReportsDrop').val());
        //arrWorkTypeUnits.push($('#unitsCumulativeDrop').val());
        //arrWorkTypeUnits.push($('#unitsBalanceSelect').val());

        //var workTypeUnitsJSONString = JSON.stringify(arrWorkTypeUnits);

        $.ajax({
            type: "Post",
            async: false,
            url: "CreateWorkItem.aspx/CreateNewWorkItem",
            data: '{"workItemName":"' + workItemName + '","workItemUnitGuid":"' + workItemUnitGuid + '","arrWorkItemSubHeadTitles":' + arrWorkItemSubHeadTitlesJSONString + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //$('#status').delay(300).fadeOut();
                //$('#preloader').delay(350).fadeOut('slow');

                alert("Success");
            }
        });     
    });

});