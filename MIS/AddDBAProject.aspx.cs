﻿using MIS.App_Code;
using NodaMoney;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class AddDBAProject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "DBA")
                {
                    if (!Page.IsPostBack)
                    {
                        //DisplayDistrictList();
                        //DisplayDivisionList();
                        //DisplaySioList();
                        //DisplayWorkItemsList();
                        //DisplayFinancialSubHeads();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                    //Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("DBALogin.aspx");
            }
        }

        private DateTime CalculateDateTime(string date)
        {
            char[] delimiter = new char[] { '/' };

            string[] dateParts = date.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

            string day = dateParts[0];
            string month = dateParts[1];
            string year = dateParts[2];

            DateTime dt = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));

            return dt;
        }

        [WebMethod]
        public static int AddProject(ProjectManager projectManager)
        {
            return new ProjectDB().AddProject(projectManager);
        }

        [WebMethod]
        public static string SaveProjectStep1(ProjectManager projectManager)
        {
            projectManager.ProjectGuid = Guid.NewGuid();

            int result = new ProjectDB().SaveProjectBasicDetails(projectManager);

            if (result == 1)
            {
                return projectManager.ProjectGuid.ToString().ToUpper();
            }
            else
            {
                return new Guid().ToString();
            }
        }

        [WebMethod]
        public static int UpdateProjectStep1(ProjectManager projectManager)
        {
            return new ProjectDB().UpdateProjectBasicDetails(projectManager);
        }

        [WebMethod]
        public static string SaveProjectStep2FremaaOfficer(ProjectAndFremaaOfficersRelationManager projectAndFremaaOfficersRelationManager)
        {
            projectAndFremaaOfficersRelationManager.FremaaOfficerProjectRelationshipGuid = Guid.NewGuid();

            int result = new ProjectAndFremaaOfficersRelationDB().AddProjectAndFremaaOfficersRelation1(projectAndFremaaOfficersRelationManager);

            if (result == 1)
            {
                return projectAndFremaaOfficersRelationManager.FremaaOfficerProjectRelationshipGuid.ToString().ToUpper();
            }
            else
            {
                return new Guid().ToString();
            }
        }

        [WebMethod]
        public static int UpdateProjectStep2FremaaOfficer(ProjectAndFremaaOfficersRelationManager projectAndFremaaOfficersRelationManager)
        {
            return new ProjectAndFremaaOfficersRelationDB().UpdateProjectAndFremaaOfficersRelation1(projectAndFremaaOfficersRelationManager);
        }

        [WebMethod]
        public static int DeleteProjectStep2FremaaOfficer(Guid projectGuid, Guid fremaaOfficerProjectRelationshipGuid)
        {
            return new ProjectAndFremaaOfficersRelationDB().DeleteProjectAndFremaaOfficersRelation1(projectGuid, fremaaOfficerProjectRelationshipGuid);
        }

        [WebMethod]
        public static string SaveProjectStep3PmcSiteEngineer(ProjectAndPmcSiteEngineersRelationManager projectAndPmcSiteEngineersRelationManager)
        {
            projectAndPmcSiteEngineersRelationManager.PmcSiteEngineerProjectRelationshipGuid = Guid.NewGuid();

            int result = new ProjectAndPmcSiteEngineersRelationDB().AddProjectAndPmcSiteEngineersRelation1(projectAndPmcSiteEngineersRelationManager);

            if (result == 1)
            {
                return projectAndPmcSiteEngineersRelationManager.PmcSiteEngineerProjectRelationshipGuid.ToString().ToUpper();
            }
            else
            {
                return new Guid().ToString();
            }
        }

        [WebMethod]
        public static int UpdateProjectStep3PmcSiteEngineer(ProjectAndPmcSiteEngineersRelationManager projectAndPmcSiteEngineersRelationManager)
        {
            return new ProjectAndPmcSiteEngineersRelationDB().UpdateProjectAndPmcSiteEngineersRelation1(projectAndPmcSiteEngineersRelationManager);
        }

        [WebMethod]
        public static int DeleteProjectStep3PmcSiteEngineer(Guid projectGuid, Guid pmcSiteEngineerProjectRelationshipGuid)
        {
            return new ProjectAndPmcSiteEngineersRelationDB().DeleteProjectAndPmcSiteEngineersRelation1(projectGuid, pmcSiteEngineerProjectRelationshipGuid);
        }

        //[WebMethod]
        //public static bool SaveProjectStep2(string projectGuid, string[] arrOfficersEngaged)
        //{
        //    int result = new ProjectDB().SaveProjectOtherOfficersEngagedDetails(new Guid(projectGuid), arrOfficersEngaged);
        //    if (result == 1)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //[WebMethod]
        //public static bool SaveProjectStep3(string projectGuid, string[] arrPmcSiteEngineers)
        //{
        //    int result = new ProjectDB().SaveProjectPmcSiteEngineersDetails(new Guid(projectGuid), arrPmcSiteEngineers);
        //    if (result == 1)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //[WebMethod]
        //public static void SaveProjectStep4(string projectGuid, List<SubChainageManager> arrSubChainages)
        //{
        //    int result = 0;

        //    if (arrSubChainages.Count > 0)
        //    {
        //        foreach (SubChainageManager subChainageObj in arrSubChainages)
        //        {
        //            Guid subChainageGuid = Guid.NewGuid();
        //            subChainageObj.SubChainageGuid = subChainageGuid;
        //            subChainageObj.ProjectManager.ProjectGuid = new Guid(projectGuid);
        //        }
        //    }

        //    //result = new SubChainageDB().CreateNewSubChainage(arrSubChainages);

        //    //if (result >= 1)
        //    //{
        //    //    return true;
        //    //}
        //    //else
        //    //{
        //    //    return false;
        //    //}
        //}

        //[WebMethod]
        //public static bool SaveProjectStep5(string projectGuid, string includeLandAcquisition)
        //{
        //    int result = new ProjectDB().SaveLandAcquisitionDetails(new Guid(projectGuid), Convert.ToBoolean(includeLandAcquisition));
        //    if (result == 1)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //[WebMethod]
        //public static void SaveProjectStep6(string projectGuid, List<ProjectValueDistributionHeadManager> arrHeadBudgets)
        //{
        //    int result = 0;

        //    List<Guid> headGuidList = new List<Guid>();

        //    if (arrHeadBudgets.Count > 0)
        //    {
        //        foreach (ProjectValueDistributionHeadManager headObj in arrHeadBudgets)
        //        {
        //            Guid headGuid = Guid.NewGuid();
        //            headObj.HeadGuid = headGuid;
        //            headObj.ProjectManager.ProjectGuid = new Guid(projectGuid);
        //        }
        //    }

        //    //result = new ProjectValueDistributionHeadDB().SaveHeadBudget(arrHeadBudgets);

        //    //if (result >= 1)
        //    //{
        //    //    return true;
        //    //}
        //    //else
        //    //{
        //    //    return false;
        //    //}
        //}

        //[WebMethod]
        //public static bool SaveProjectStep7(string projectGuid)
        //{
        //    int result = new ProjectDB().SubmitProject(new Guid(projectGuid), (int)ProjectStatusEnum.Submitted);
        //    if (result == 1)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}     

    }
}