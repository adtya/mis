﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class CreateSubChainage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayProjectsList();
            DisplayWorkTypeItemsList();
        }

        private void DisplayProjectsList()
        {
            List<ProjectManager> projectList = new List<ProjectManager>();
            projectList = new ProjectDB().GetProjectNameWithStatus();

            StringBuilder htmlProjectList = new StringBuilder();

            htmlProjectList.Append("<select class=\"form-control\" name=\"projectList\" id=\"projectList\">");
            if (projectList.Count > 0)
            {
                foreach (ProjectManager projectObj in projectList)
                {
                    htmlProjectList.Append("<option value=\"" + projectObj.ProjectGuid.ToString() + "\">" + projectObj.ProjectName.ToString() + "</option>");
                }
            }
            htmlProjectList.Append("</select>");

            ltProjectList.Text = htmlProjectList.ToString();



            //List<Pr> divisionListArray = new List<string>();

            //foreach (DivisionManager divisionData in projectList)
            //{
            //    int index = projectList.IndexOf(divisionData);

            //    divisionListArray.Add(divisionData.DivisionGuid.ToString());
            //    divisionListArray.Add(divisionData.DivisionName.ToString());
            //}

            //return divisionListArray.ToArray();
        }

        private void DisplayWorkTypeItemsList()
        {
            List<WorkItemManager> workTypeItemList = new List<WorkItemManager>();
            //workTypeItemList = new WorkItemDB().GetWorkItemNames();

            StringBuilder htmlWorkTypeItemList = new StringBuilder();

            htmlWorkTypeItemList.Append("<select class=\"form-control\" name=\"workTypeItemList\" id=\"workTypeItemList\">");
            if (workTypeItemList.Count > 0)
            {
                foreach (WorkItemManager workTypeItemObj in workTypeItemList)
                {
                    htmlWorkTypeItemList.Append("<option value=\"" + workTypeItemObj.WorkItemGuid.ToString() + "\">" + workTypeItemObj.WorkItemName.ToString() + "</option>");
                }
            }
            htmlWorkTypeItemList.Append("</select>");

            ltProjectList.Text = htmlWorkTypeItemList.ToString();



            //List<Pr> divisionListArray = new List<string>();

            //foreach (DivisionManager divisionData in projectList)
            //{
            //    int index = projectList.IndexOf(divisionData);

            //    divisionListArray.Add(divisionData.DivisionGuid.ToString());
            //    divisionListArray.Add(divisionData.DivisionName.ToString());
            //}

            //return divisionListArray.ToArray();
        }

        [WebMethod]
        public static void CreateNewSubChainage(string projectGuid, string subChainageName)
        {

        }

    }
}