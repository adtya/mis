﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class RoutineWorkItem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dtRoutineWorkItem = new DataTable();
            DataRow drRoutineWorkItem = null;

            dtRoutineWorkItem.Columns.Add(new DataColumn("Code", typeof(string)));
            dtRoutineWorkItem.Columns.Add(new DataColumn("Class", typeof(string)));
            dtRoutineWorkItem.Columns.Add(new DataColumn("Short Description", typeof(string)));
            dtRoutineWorkItem.Columns.Add(new DataColumn("Asset Quality Paramter", typeof(string)));            
            dtRoutineWorkItem.Columns.Add(new DataColumn("Units", typeof(string)));
            dtRoutineWorkItem.Columns.Add(new DataColumn("Full Description", typeof(string)));
           
            drRoutineWorkItem = dtRoutineWorkItem.NewRow();

            drRoutineWorkItem["Code"] = "";
            drRoutineWorkItem["Class"] = "";
            drRoutineWorkItem["Short Description"] = "";
            drRoutineWorkItem["Asset Quality Paramter"] = "";
            drRoutineWorkItem["Units"] = "";
            drRoutineWorkItem["Full Description"] = "";
         
            dtRoutineWorkItem.Rows.Add(drRoutineWorkItem);

            GridView1.DataSource = dtRoutineWorkItem;
            GridView1.DataBind();
        }
    }
}