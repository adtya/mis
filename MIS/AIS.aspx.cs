﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class AMS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //FieldSetHeading.Text = "Address - Embankment";

            DataTable dtEmb = new DataTable();
            DataRow drEmb = null;

            dtEmb.Columns.Add(new DataColumn("Code", typeof(string)));
            dtEmb.Columns.Add(new DataColumn("Asset Name", typeof(string)));
            dtEmb.Columns.Add(new DataColumn("Cost (Rs Lakh)", typeof(string)));

            drEmb = dtEmb.NewRow();

            drEmb["Code"] = "";
            drEmb["Asset Name"] = "";
            drEmb["Cost (Rs Lakh)"] = "";

            dtEmb.Rows.Add(drEmb);      
            
            //ViewState["CurrentTable"] = dt;
            GridView1.DataSource = dtEmb;
            GridView1.DataBind();


            DataTable dtCan = new DataTable();
            DataRow drCan = null;

            dtCan.Columns.Add(new DataColumn("Code", typeof(string)));
            dtCan.Columns.Add(new DataColumn("Asset Name", typeof(string)));
            dtCan.Columns.Add(new DataColumn("Cost (Rs Lakh)", typeof(string)));

            drCan = dtCan.NewRow();

            drCan["Code"] = "";
            drCan["Asset Name"] = "";
            drCan["Cost (Rs Lakh)"] = "";

            dtCan.Rows.Add(drCan);

            //ViewState["CurrentTable"] = dt;
            GridView2.DataSource = dtCan;
            GridView2.DataBind();

            DataTable dtDrainage = new DataTable();
            DataRow drDrainage = null;

            dtDrainage.Columns.Add(new DataColumn("Code", typeof(string)));
            dtDrainage.Columns.Add(new DataColumn("Asset Name", typeof(string)));
            dtDrainage.Columns.Add(new DataColumn("Cost (Rs Lakh)", typeof(string)));

            drDrainage = dtDrainage.NewRow();

            drDrainage["Code"] = "";
            drDrainage["Asset Name"] = "";
            drDrainage["Cost (Rs Lakh)"] = "";

            dtDrainage.Rows.Add(drDrainage);

            //ViewState["CurrentTable"] = dt;
            GridView3.DataSource = dtDrainage;
            GridView3.DataBind();

            DataTable dtSluice = new DataTable();
            DataRow drSluice = null;

            dtSluice.Columns.Add(new DataColumn("Gate Type", typeof(string)));
            dtSluice.Columns.Add(new DataColumn("Construction Material", typeof(string)));
            dtSluice.Columns.Add(new DataColumn("Number of Gates", typeof(string)));
            dtSluice.Columns.Add(new DataColumn("Height", typeof(string)));
            dtSluice.Columns.Add(new DataColumn("Width", typeof(string)));
            dtSluice.Columns.Add(new DataColumn("Diameter", typeof(string)));

            drSluice = dtSluice.NewRow();

            drSluice["Gate Type"] = "";
            drSluice["Construction Material"] = "";
            drSluice["Number of Gates"] = "";
            drSluice["Height"] = "";
            drSluice["Width"] = "";
            drSluice["Diameter"] = "";

            dtSluice.Rows.Add(drSluice);

            //ViewState["CurrentTable"] = dt;
            GridView4.DataSource = dtSluice;
            GridView4.DataBind();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           //This line of code creates a html file for the data export.
            
            string tempPath = Server.MapPath("/Temp");
            tempPath = tempPath + "\\AssetCoreForm.html";       
            
            StreamWriter file = new StreamWriter(tempPath);
            try
            {
                string sLine = "<html><h1><center>Asset Core </h1></center><style>table, tr, td { border:0px}</style><body bgcolor=" + "\"" + "lightblue" + "\"" + ">";
                file.WriteLine(sLine);
                file.WriteLine("<table>");
                file.WriteLine("<tr>" + "<td width =\"250\">" + "WRD division" + "</td>" + "<td>" + WRDDivision.Text + "</td>" + "<td width =\"300\">" + "</td>" + "<td>" + "</td>" + "<td colspan =\"2\" width=\"200\" >" + "<center>" + "UTM(m)" + "</td>" + "<td>" + "</td>" + "<td width =\"100\" align = \"center\" >" + "Chainage" + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Scheme" + "</td>" + "<td>" + Scheme.Text + "</td>" + "<td width =\"200\">" + "</td>" + "<td>" + "</td>" + "<td>" + "<center>" + "Easting" + "</td>" + "<td>" + "<center>" + "Northing" + "</td>" + "<td>" + "</td>" + "<td>" + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "</td>" + "<td>" + "</td>" + "<td width =\"200\">" + "</td>" + "<td>" + "Upstream" + "</td>" + "<td align = \"right\">" + EastingUS.Text + "</td>" + "<td align = \"right\">" + NorthingUS.Text + "</td>" + "<td>" + "</td>" + "<td align = \"right\">" + ChainageStart.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "</td>" + "<td>" + "</td>" + "<td width =\"200\">" + "</td>" + "<td>" + "Downstream" + "</td>" + "<td align = \"right\">" + EastingDS.Text + "</td>" + "<td align = \"right\">" + NorthingDS.Text + "</td>" + "<td>" + "</td>" + "<td align = \"right\">" + ChainageEnd.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "<h4>" + "Key Asset" + "</h4>" + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Drawing Code" + "</td>" + "<td  align=\"right\">" + DrawingCode.Text + "</td>" + "<td width =\"200\">" + "</td>" + "<td>" + "AMTD" + "</td>" + "<td align = \"right\">" + AMTD.Text + "</td>" + "<td>" + "</td>" + "<td>" + "</td>" + "<td>" + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Asset type" + "</td>" + "<td>" + AssetType.Text + "</td>" + "<td width =\"200\">" + "</td>" + "<td>" + "Assset cost (Rs)" + "</td>" + "<td align = \"right\">" + AssetCost.Text + "</td>" + "<td>" + "</td>" + "<td>" + "</td>" + "<td>" + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Asset quantity" + "</td>" + "<td colspan= \"3\">" + AssetName.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Asset name" + "</td>" + "<td  colspan= \"3\">" + RecordList.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Asset code" + "</td>" + "<td   colspan= \"2\">" + AssetCode.Text + "</td>" + "</tr>");
                file.WriteLine("</table>");

                file.WriteLine("<h2>" + "Embankment" + "<br>" + "</h2>");
                file.WriteLine("<table>");
                file.WriteLine("<tr>" + "<td width =\"250\">" + "U/S Crest elevation (m)" + "</td>" + "<td align=\"right\">" + USCrestElevation.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "D/S Crest elevation (m)" + "</td>" + "<td align=\"right\">" + DSCrestElevation.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Embankment length (m)" + "</td>" + "<td align=\"right\">" + Length.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Crest width (m)" + "</td>" + "<td align=\"right\">" + CrestWidth.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "CS slope" + "</td>" + "<td align=\"right\">" + CSSlope.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "RS slope" + "</td>" + "<td align=\"right\">" + RSSlope.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Berm slope" + "</td>" + "<td align=\"right\">" + BermSlope.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Platform width (m)" + "</td>" + "<td align=\"right\">" + PlatformWidth.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Crest type" + "</td>" + "<td>" + CrestType.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Fill type" + "</td>" + "<td>" + FillType.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Average height(m)" + "</td>" + "<td align=\"right\">" + AverageHeight.Text + "</td>" + "</tr>"); ;
                file.WriteLine("</table>");
                file.WriteLine("<h4>" + "Appurtenant Assets" + "</h4>");
                sLine = "<br><center><table cellspacing=\"0\" cellpadding=\"5\" style=" + "\"" + "border:1px solid black" + "\"" + " " + "align=" + "\"" + "center" + "\"" + " bgcolor=" + "\"" + "white" + "\"" + " width= " + "\"" + "100%" + "\"" + " border-collapse=" + "\"" + "collapse" + "\"" + "><tr>";
                file.WriteLine(sLine);
                //This for loop places the column headers into the first row of the HTML table.
                file.WriteLine("<tr>" + "<td width =\"250\">" + "No" + "</td>" + "<td>" + "Type" + "</td>" + "<td width =\"300\">" + "Item"+ "</td>" + "<td>" + "Item Performance Description" + "</td>" + "<td>" + "Item Performance Status" + "</td>" + "</tr>");
                //This for loop loops through each row in the DataGridView.
                for (int r = 0; r <= GridView1.Rows.Count - 1; r++)
                {
                    sLine= sLine + "<tr>";
                    file.WriteLine(sLine);
                    sLine = "";
                    //This for loop loops through each column, and the row number
                    //is passed from the for loop above.
                    for (int c = 0; c <= GridView1.Columns.Count - 1; c++)
                    {
                        if (GridView1.Rows[r].Cells[c].Text == null)
                        {
                            sLine = string.Empty;
                        }
                        else
                        {
                            sLine= GridView1.Rows[r].Cells[c].Text.ToString();
                        }
                        sLine = "<td style=" + "\"" + "border:1px solid black" + "\"" + ">" + GridView1.Rows[r].Cells[c].Text + "</td>";
                        file.WriteLine(sLine);
                    }
                    //The exported text is written to the html file, one line at a time.
                    sLine= "</tr>";
                    file.WriteLine(sLine);
                    sLine ="";
                }
                sLine  ="</table></body><br></html>";
                file.WriteLine(sLine);


                file.Close();
                System.Diagnostics.Process.Start(tempPath);
            }

            catch (System.Exception err)
            {
                file.Close();
            }

        }
       
    }
}