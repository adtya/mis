﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using ISO3166;
using MIS.App_Code;

namespace MIS
{
    public partial class Countries : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //List<string> a1 = new List<string>();
            //List<string> a2 = new List<string>();
            //List<string> a3 = new List<string>();
            //List<string> a4 = new List<string>();

            //foreach (Country cv in ISO3166.Country.List)
            //{
            //    a1.Add(cv.Name.ToString());
            //    a2.Add(cv.NumericCode.ToString());
            //    a3.Add(cv.ThreeLetterCode.ToString());
            //    a4.Add(cv.TwoLetterCode.ToString());
            //}

            
            CountryDB a = new CountryDB();


            //for (int cnt = 0; cnt < a1.Count; cnt++) // Loop with for.
            //{
            //    System.Diagnostics.Debug.WriteLine(a1[cnt] + "  " + a2[cnt] + "  " + a3[cnt] + "  " + a4[cnt]);
            //    //Console.WriteLine(list[i]);
            //    //a.CreateCountry(a4[cnt], a3[cnt], a2[cnt], a1[cnt]);
            //}

            //new PISDB().CreateCountry()


            //foreach (List<Country> xcv in a)
            //{
            //    System.Diagnostics.Debug.WriteLine(xcv);
            //}

            //Console.WriteLine(a);   

            List<Country> countriesList = new List<Country>();

            countriesList = a.GetCountries();

            string abc = "<div id=" + "\"table-container\"" + "style=" + "\"padding:1%;\">";
            abc += "<table id=\"trainingList\" class=\"table table-striped table-bordered table-hover table-responsive\" cellspacing=\"0\" width=\"100%\">";

            abc += "<thead>";
            abc += "<tr>";
            abc += "<th>";
            abc += "Country Name";
            abc += "</th>";
            abc += "<th>";
            abc += "Code 2";
            abc += "</th>";
            abc += "</tr>";
            abc += "</thead>";

            abc += "<tfoot>";
            abc += "<tr>";
            abc += "<th>";
            abc += "Country Name";
            abc += "</th>";
            abc += "<th>";
            abc += "Code 2";
            abc += "</th>";
            abc += "</tr>";
            abc += "</tfoot>";

            abc += "<tbody>";
            foreach (Country countryData in countriesList)
            {
                abc += "<tr>";
                abc += "<td>" + countryData.CountryName + "</td>";
                abc += "<td>" + countryData.CountryISOCode2 + "</td>";
                abc += "</tr>";
            }
            
            abc += "</tbody>";

            countriesListLiteral.Text = abc;
        }
    }
}