﻿using MIS.App_Code;
using NodaMoney;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class CreateSIOAsset : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "SIO")
                {
                    if (!Page.IsPostBack)
                    {
                        DisplayAssetTypeList();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                    //Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("DBALogin.aspx");
            }     

        }

        private void DisplayAssetTypeList()
        {
            List<AssetTypeManager> assetTypeList = new List<AssetTypeManager>();

            assetTypeList = new AssetTypeDB().GetAssetTypes();

            StringBuilder htmlAssetTypeList = new StringBuilder();

            htmlAssetTypeList.Append("<select class=\"form-control select2\" name=\"assetTypeList\" id=\"assetTypeList\" style=\"width:100%;\">");
            htmlAssetTypeList.Append("<option value=\"\">" + "--Select AssetType--" + "</option>");
            if (assetTypeList.Count > 0)
            {
                foreach (AssetTypeManager assetTypeObj in assetTypeList)
                {
                    htmlAssetTypeList.Append("<option value=\"" + assetTypeObj.AssetTypeGuid + "\">" + assetTypeObj.AssetTypeName + " (" + assetTypeObj.AssetTypeCode + ")" + "</option>");
                }
            }
            htmlAssetTypeList.Append("</select>");
            ltAssetTypeList.Text = htmlAssetTypeList.ToString();
        }

        [WebMethod]
        public static Array GetAssetNumbers(Guid assetTypeGuid, Guid schemeGuid)
        {
            return new AssetDB().GetAssetNumbers(assetTypeGuid, schemeGuid);            
        }

        [WebMethod]
        public static List<SchemeManager> DisplaySchemeNameForDivisions(Guid divisionGuid)
        {
            List<SchemeManager> schemeList = new List<SchemeManager>();
            schemeList = new SchemeDB().GetSchemesBasedOnDivisions(divisionGuid);         

            return schemeList;
        }

        [WebMethod]
        public static List<AssetManager> DisplayKeyAssets(Guid assetTypeGuid, Guid divisionGuid, Guid schemeGuid)
        {
            List<AssetManager> keyAssetList = new List<AssetManager>();
            keyAssetList = new AssetDB().GetKeyAssets(assetTypeGuid, divisionGuid, schemeGuid);

            return keyAssetList;
        }

        [WebMethod]
        public static List<BridgeTypeManager> GetBridgeType()
        {
            List<BridgeTypeManager> bridgeTypeManagerList = new List<BridgeTypeManager>();
            bridgeTypeManagerList = new BridgeTypeDB().GetBridgeTypeList();
            return bridgeTypeManagerList;
        }

        [WebMethod]
        public static List<CanalLiningTypeManager> GetCanalLiningType()
        {
            List<CanalLiningTypeManager> canalLiningTypeManagerList = new List<CanalLiningTypeManager>();
            canalLiningTypeManagerList = new CanalLiningTypeDB().GetCanalLiningTypeList();
            return canalLiningTypeManagerList;
        }

        [WebMethod]
        public static List<CulvertTypeManager> GetCulvertType()
        {
            List<CulvertTypeManager> culvertTypeManagerList = new List<CulvertTypeManager>();
            culvertTypeManagerList = new CulvertTypeDB().GetCulvertTypeList();
            return culvertTypeManagerList;
        }

        [WebMethod]
        public static List<EmbankmentCrestTypeManager> GetEmbankmentCrestType()
        {
            List<EmbankmentCrestTypeManager> embankmentCrestTypeManagerList = new List<EmbankmentCrestTypeManager>();
            embankmentCrestTypeManagerList = new EmbankmentCrestTypeDB().GetEmbankmentCrestTypeList();       
            return embankmentCrestTypeManagerList;
        }

        [WebMethod]
        public static List<EmbankmentFillTypeManager> GetEmbankmentFillType()
        {
            List<EmbankmentFillTypeManager> embankmentFillTypeManagerList = new List<EmbankmentFillTypeManager>();
            embankmentFillTypeManagerList = new EmbankmentFillTypeDB().GetEmbankmentFillTypeList();
            return embankmentFillTypeManagerList;
        }

        [WebMethod]
        public static List<GaugeTypeManager> GetGaugeType()
        {
            List<GaugeTypeManager> gaugeTypeManagerList = new List<GaugeTypeManager>();
            gaugeTypeManagerList = new GaugeTypeDB().GetGaugeTypeList();
            return gaugeTypeManagerList;
        }

        [WebMethod]
        public static List<GaugeFrequencyTypeManager> GetGaugeFrequencyType()
        {
            List<GaugeFrequencyTypeManager> gaugeFrequencyTypeManagerList = new List<GaugeFrequencyTypeManager>();
            gaugeFrequencyTypeManagerList = new GaugeFrequencyTypeDB().GetGaugeFrequencyTypeList();
            return gaugeFrequencyTypeManagerList;
        }

        [WebMethod]
        public static List<GaugeSeasonTypeManager> GetGaugeSeasonType()
        {
            List<GaugeSeasonTypeManager> gaugeSeasonTypeManagerList = new List<GaugeSeasonTypeManager>();
            gaugeSeasonTypeManagerList = new GaugeSeasonTypeDB().GetGaugeSeasonTypeList();
            return gaugeSeasonTypeManagerList;
        }

        [WebMethod]
        public static List<PorcupineTypeManager> GetPorcupineType()
        {
            List<PorcupineTypeManager> porcupineTypeManagerList = new List<PorcupineTypeManager>();
            porcupineTypeManagerList = new PorcupineTypeDB().GetPorcupineTypeList();
            return porcupineTypeManagerList;
        }

        [WebMethod]
        public static List<PorcupineMaterialTypeManager> GetPorcupineMaterialType()
        {
            List<PorcupineMaterialTypeManager> porcupineMaterialTypeManagerList = new List<PorcupineMaterialTypeManager>();
            porcupineMaterialTypeManagerList = new PorcupineMaterialTypeDB().GetPorMaterialTypeList();
            return porcupineMaterialTypeManagerList;
        }

        [WebMethod]
        public static List<RegulatorTypeManager> GetRegulatorType()
        {
            List<RegulatorTypeManager> regulatorTypeManagerList = new List<RegulatorTypeManager>();
            regulatorTypeManagerList = new RegulatorTypeDB().GetRegulatorTypeList();
            return regulatorTypeManagerList;
        }

        [WebMethod]
        public static List<RegulatorGateTypeManager> GetRegulatorGateType()
        {
            List<RegulatorGateTypeManager> regGateTypeManagerList = new List<RegulatorGateTypeManager>();
            regGateTypeManagerList = new RegulatorGateTypeDB().GetRegGateTypeList();
            return regGateTypeManagerList;
        }

        [WebMethod]
        public static List<RevetmentTypeManager> GetRevetmentType()
        {
            List<RevetmentTypeManager> revetmentTypeManagerList = new List<RevetmentTypeManager>();
            revetmentTypeManagerList = new RevetmentTypeDB().GetRevetmentTypeList();
            return revetmentTypeManagerList;
        }

        [WebMethod]
        public static List<RevetmentRiverprotTypeManager> GetRevetmentRiverProtectionType()
        {
            List<RevetmentRiverprotTypeManager> revRiverprotTypeManagerList = new List<RevetmentRiverprotTypeManager>();
            revRiverprotTypeManagerList = new RevetmentRiverprotTypeDB().GetRevRiverprotTypeList();
            return revRiverprotTypeManagerList;
        }

        [WebMethod]
        public static List<RevetmentWaveprotTypeManager> GetRevetmentWaveProtectionType()
        {
            List<RevetmentWaveprotTypeManager> revWaveprotTypeManagerList = new List<RevetmentWaveprotTypeManager>();
            revWaveprotTypeManagerList = new RevetmentWaveprotTypeDB().GetRevWaveprotTypeList();
            return revWaveprotTypeManagerList;
        }

        [WebMethod]
        public static List<SluiceTypeManager> GetSluiceType()
        {
            List<SluiceTypeManager> sluiceTypeManagerList = new List<SluiceTypeManager>();
            sluiceTypeManagerList = new SluiceTypeDB().GetSluiceTypeList();
            return sluiceTypeManagerList;
        }

        [WebMethod]
        public static List<SpurShapeTypeManager> GetSpurShapeType()
        {
            List<SpurShapeTypeManager> spurShapeTypeManagerList = new List<SpurShapeTypeManager>();
            spurShapeTypeManagerList = new SpurShapeTypeDB().GetSpurShapeTypeList();
            return spurShapeTypeManagerList;
        }

        [WebMethod]
        public static List<SpurConstructionTypeManager> GetSpurConstructionType()
        {
            List<SpurConstructionTypeManager> spurConstructionTypeManagerList = new List<SpurConstructionTypeManager>();
            spurConstructionTypeManagerList = new SpurConstructionTypeDB().GetSpurConstructionTypeList();
            return spurConstructionTypeManagerList;
        }

        [WebMethod]
        public static List<SpurRevetTypeManager> GetSpurRevetType()
        {
            List<SpurRevetTypeManager> spurRevetTypeManagerList = new List<SpurRevetTypeManager>();
            spurRevetTypeManagerList = new SpurRevetTypeDB().GetSpurRevetTypeList();
            return spurRevetTypeManagerList;
        }

        [WebMethod]
        public static List<TurnoutTypeManager> GetTurnoutType()
        {
            List<TurnoutTypeManager> turnoutTypeManagerList = new List<TurnoutTypeManager>();
            turnoutTypeManagerList = new TurnoutTypeDB().GetTurnoutTypeList();
            return turnoutTypeManagerList;
        }

        [WebMethod]
        public static List<TurnoutGateTypeManager> GetTurnoutGateType()
        {
            List<TurnoutGateTypeManager> trnGateTypeManagerList = new List<TurnoutGateTypeManager>();
            trnGateTypeManagerList = new TurnoutGateTypeDB().GetTrnGateTypeList();
            return trnGateTypeManagerList;
        }

        [WebMethod]
        public static List<WeirTypeManager> GetWeirType()
        {
            List<WeirTypeManager> weirTypeManagerList = new List<WeirTypeManager>();
            weirTypeManagerList = new WeirTypeDB().GetWeirTypeList();
            return weirTypeManagerList;
        }

        [WebMethod]
        public static List<GateTypeManager> GetGateType()
        {
            List<GateTypeManager> gateTypeManagerList = new List<GateTypeManager>();
            gateTypeManagerList = new GateTypeDB().GetGateTypeList();
            return gateTypeManagerList;
        }

        [WebMethod]
        public static List<GateConstructionMaterialManager> GetGateConstructionMaterialType()
        {
            List<GateConstructionMaterialManager> gateConstructionMaterialManagerList = new List<GateConstructionMaterialManager>();
            gateConstructionMaterialManagerList = new GateConstructionMaterialDB().GetGateConstructionMaterialList();
            return gateConstructionMaterialManagerList;
        }

        [WebMethod]
        public static int SaveAsset(AssetManager assetManager)
        {
            assetManager.AssetGuid = Guid.NewGuid();
            int rowsAffected = new AssetDB().AddAsset(assetManager);
            if (rowsAffected == 1)
            {
                int assetTypeSaved = 0;
                switch (assetManager.AssetTypeManager.AssetTypeCode)
                {
                    case "BRG":
                        assetTypeSaved = new BridgeDB().AddBridge(assetManager);
                        break;
                    case "CAN":
                        assetTypeSaved = new CanalDB().AddCanalData(assetManager);
                        break;
                    case "CUL":
                        assetTypeSaved = new CulvertDB().AddCulvertData(assetManager);
                        break;
                    case "DRN":
                        assetTypeSaved = new DrainageDB().AddDrainageData(assetManager);
                        break;
                    case "DRP":
                        assetTypeSaved = new DropStructureDB().AddDropStructureData(assetManager);
                        break;
                    case "EMB":
                        assetTypeSaved = new EmbankmentDB().AddEmbankmentData(assetManager);
                        break;
                    case "GAU":
                        assetTypeSaved = new GaugeDB().AddGaugeData(assetManager);
                        break;
                    case "POR":
                        assetTypeSaved = new PorcupineDB().AddPorcupineData(assetManager);
                        break;
                    case "REG":
                        assetTypeSaved = new RegulatoreDB().AddRegulatorData(assetManager);
                        break;
                    case "REV":
                        assetTypeSaved = new RevetmentDB().AddRevetmentData(assetManager);
                        break;
                    case "SLU":
                        assetTypeSaved = new SluiceDB().AddSluiceData(assetManager);
                        break;
                    case "SPU":
                        assetTypeSaved = new SpurDB().AddSpurData(assetManager);
                        break;
                    case "TRN":
                        assetTypeSaved = new TurnoutDB().AddTurnOutData(assetManager);
                        break;
                    case "WEI":
                        assetTypeSaved = new WeirDB().AddWeirData(assetManager);
                        break;
                    default:
                        assetTypeSaved = 0;
                        break;
                }
                if (assetTypeSaved == 0)
                {
                    new AssetDB().DeleteAsset(assetManager.AssetGuid);
                    rowsAffected = 0;
                }
                else
                {
                    if (assetManager.AssetDrawingManager.DrawingFileName != "")
                    {
                        new AssetDrawingDB().AddDrawing(assetManager);
                    }
                }
            }
            return rowsAffected;
        }

    }
}