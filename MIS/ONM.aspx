﻿<%@ Page Title="O&M" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ONM.aspx.cs" Inherits="MIS.ONM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="CustomStyles/onm.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>Performance Monitoring Details</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <asp:Label ID="RecordListLabel" CssClass="control-label col-sm-2" AssociatedControlID="RecordList" runat="server" Text="Go to Record"></asp:Label>
                                            <div class="col-sm-10">
                                                <asp:DropDownList ID="RecordList" runat="server" CssClass="form-control">
                                                    <asp:ListItem Selected="True" Text="GUWE-P-G-E-EMB02"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="CurrentMonitoringDateLabel" CssClass="control-label col-sm-6" AssociatedControlID="CurrentMonitoringDate" runat="server" Text="Current Monitoring Date"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="CurrentMonitoringDate" runat="server" CssClass="form-control" ReadOnly="true" Text="2015-09-30"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="AssetTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="AssetType" runat="server" Text="Asset Type"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="AssetType" runat="server" CssClass="form-control" ReadOnly="true" Text="EMB"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="AssetCodeLabel" CssClass="control-label col-sm-6" AssociatedControlID="AssetCode" runat="server" Text="Asset Code"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="AssetCode" runat="server" CssClass="form-control" ReadOnly="true" Text="GUWE-P-G-E-EMB02"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="AssetNameLabel" CssClass="control-label col-sm-6" AssociatedControlID="AssetName" runat="server" Text="AssetName"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="AssetName" runat="server" CssClass="form-control" ReadOnly="true" Text="B/dyke from PGP"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="InspectionDateLabel" CssClass="control-label col-sm-6" AssociatedControlID="InspectionDate" runat="server" Text="Inspection Date"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="InspectionDate" runat="server" CssClass="form-control" ReadOnly="true" Text="2013-08-2015"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="InspectorNameLabel" CssClass="control-label col-sm-6" AssociatedControlID="InspectorName" runat="server" Text="Inspector Name"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="InspectorName" runat="server" CssClass="form-control" ReadOnly="true" Text=""></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="OverallStatusLabel" CssClass="control-label col-sm-6" AssociatedControlID="OverallStatus" runat="server" Text="Overall Status"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="OverallStatus" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="Not Applicable" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Very Good"></asp:ListItem>
                                                    <asp:ListItem Text="Good"></asp:ListItem>
                                                    <asp:ListItem Text="Satisfactory"></asp:ListItem>
                                                    <asp:ListItem Text="Poor"></asp:ListItem>
                                                    <asp:ListItem Text="Very Poor"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="InspectorPositionLabel" CssClass="control-label col-sm-6" AssociatedControlID="InspectorPosition" runat="server" Text="Inspector Position"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="InspectorPosition" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="Sectional Officer"></asp:ListItem>
                                                    <asp:ListItem Text="Assistant Engineer"></asp:ListItem>
                                                    <asp:ListItem Text="Executive Engineer"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <asp:Label ID="GeneralCommentsLabel" CssClass="control-label col-sm-2" AssociatedControlID="GeneralComments" runat="server" Text="General Comments"></asp:Label>
                                            <div class="col-sm-10">
                                                <asp:TextBox ID="GeneralComments" runat="server" CssClass="form-control" ReadOnly="true" Text="" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border">Performance CheckList</legend>

                                        <asp:GridView ID="GridView1" runat="server" Width="870px">
                                            <%--<Columns>
                                                <asp:BoundField HeaderText="No." />
                                                <asp:BoundField HeaderText="Type" />
                                                <asp:BoundField HeaderText="Item" />
                                                <asp:BoundField HeaderText="Item Performance Description" />
                                                <asp:TemplateField HeaderText="Item Performance Status"></asp:TemplateField>
                                            </Columns>--%>
                                        </asp:GridView>

                                    </fieldset>
                                </div>
                               <asp:Button ID="btnPrint" runat="server" Text="Print" OnClick="btnPrint_Click" />
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
</asp:Content>
