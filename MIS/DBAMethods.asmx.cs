﻿using MIS.App_Code;
using NodaMoney;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace MIS
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DBAMethods : System.Web.Services.WebService
    {
        [WebMethod]
        public string[] GetRoleNames()
        {
            List<RoleManager> roleManagerList = new RoleDB().GetRoles();

            List<String> roleNameList = new List<string>();

            if (roleManagerList.Count > 0)
            {
                foreach (RoleManager roleObj in roleManagerList)
                {
                    roleNameList.Add(roleObj.RoleGuid.ToString());
                    roleNameList.Add(roleObj.RoleName.ToString());
                }
            }

            return roleNameList.ToArray();
        }

        [WebMethod]
        public int SaveUserData(string userFirstName, string userMiddleName, string userLastName, string userEmailId, string userRoleGuid)
        {
            int rowUpdateStatus;
            Guid userGuid = Guid.NewGuid();
            string userPassword = new PasswordGenerator().CreateRandomPassword();
            try
            {
                if (new UserDB().CheckUserExistence(userEmailId))
                {
                    rowUpdateStatus = 2;
                }
                else
                {
                    rowUpdateStatus = new UserDB().AddUser(userGuid, userFirstName, userMiddleName, userLastName, userEmailId, new Guid(userRoleGuid), userPassword);
                    if (rowUpdateStatus == 1)
                    {
                        string subject = "Successful Registration at MIS";
                        string body = "Hello" + " " + "\n";
                        body += "You have registered on MIS Successfully.\n";
                        body += "UserName: " + userEmailId + "\n";
                        body += "Password: " + userPassword + "\n";
                        body += "\n\n";
                        body += "Thanks n Regards\n";
                        body += "FREMAA\n";
                        body += "This is a system generated mail. Please do not reply to this.";
                        new MailManager().SendMailToUser(userEmailId, subject, body);
                    }
                }               

                return rowUpdateStatus;
            }
            catch
            {
                return 0;
            }
        }

        [WebMethod]
        public List<UserManager> GetUsers()
        {
            List<UserManager> userList = new List<UserManager>();
            userList = new UserDB().GetUserData();

            List<String> userNameList = new List<string>();

            if (userList.Count > 0)
            {
                foreach (UserManager userObj in userList)
                {
                    userNameList.Add(userObj.UserGuid.ToString());
                    userNameList.Add(userObj.FirstName.ToString());
                    userNameList.Add(userObj.Email);
                    userNameList.Add(userObj.RoleManager.RoleName);
                }
            }

            //return userNameList;
            return userList;

        }

        [WebMethod]
        public bool DeleteUserData(string[] userGuidListToDelete)
        {
            List<Guid> userGuidList = new List<Guid>();

            foreach (string userGuidString in userGuidListToDelete)
            {
                userGuidList.Add(new Guid(userGuidString));
            }

            return new UserDB().DeleteUserData(userGuidList);
        }

        //[WebMethod]
        //public int SaveCircle(string circleCode, string circleName)
        //{
        //    return new CircleDB().AddCircle(Guid.NewGuid(), circleCode, circleName);
        //}

        [WebMethod]
        public string[] GetCircleNames()
        {
            List<CircleManager> circleList = new List<CircleManager>();
            circleList = new CircleDB().GetCircles();

            List<String> circleNameList = new List<string>();

            if (circleList.Count > 0)
            {
                foreach (CircleManager circleObj in circleList)
                {
                    circleNameList.Add(circleObj.CircleGuid.ToString());
                    circleNameList.Add(circleObj.CircleCode.ToString());
                    circleNameList.Add(circleObj.CircleName.ToString());
                }
            }

            return circleNameList.ToArray();
        }

        [WebMethod]
        public bool CheckCircleExistence(string circleName, string circleCode)
        {
            return new CircleDB().CheckCircleExistence(circleName, circleCode);
        }

        [WebMethod]
        public Guid SaveCircle(string circleCode, string circleName)
        {
            Guid circleGuid = Guid.NewGuid();
            int returnValue = new CircleDB().AddCircle(circleGuid, circleCode, circleName);

            if (returnValue == 1)
            {
                return circleGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public bool UpdateCircle(string circleGuid, string circleName, string circleCode)
        {
            return new CircleDB().UpdateCircle(new Guid(circleGuid), circleName, circleCode);
        }

        [WebMethod]
        public int DeleteCircle(Guid circleGuid)
        {
            return new CircleDB().DeleteCircle(circleGuid);
        }        

        [WebMethod]
        public List<DivisionManager> GetDivisionsWithCircle()
        {
            List<DivisionManager> divisionList = new List<DivisionManager>();
            divisionList = new DivisionDB().GetDivisionsWithCircle();

            return divisionList;
        }

        [WebMethod]
        public bool CheckDivisionExistence(string divisionCode, string divisionName)
        {
            return new DivisionDB().CheckDivisionExistence(divisionCode,divisionName);
        }

        [WebMethod]
        public Guid SaveDivision(string divisionCode, string divisionName, string circleGuid)
        {
            Guid divisionGuid = Guid.NewGuid();
            int returnValue = new DivisionDB().AddDivision(divisionGuid, divisionCode, divisionName, new Guid(circleGuid));
            if (returnValue == 1)
            {
                return divisionGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public string ViewDivisionData(Guid divisionGuid)
        {
            DivisionManager divisionDataObj = new DivisionManager();
            divisionDataObj = new DivisionDB().GetDivisionDataWithCircle(divisionGuid);

            StringBuilder htmlDivisionDetailsBody = new StringBuilder();
            htmlDivisionDetailsBody.Append("<div class=\"container-fluid col-md-12\">");
            htmlDivisionDetailsBody.Append("<div class=\"row\">");
            htmlDivisionDetailsBody.Append("<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">");
            htmlDivisionDetailsBody.Append("<div class=\"panel panel-default\">");
            htmlDivisionDetailsBody.Append("<div class=\"panel-body\">");
            htmlDivisionDetailsBody.Append("<div class=\"text-center\">");

            htmlDivisionDetailsBody.Append("<img src=\"Images/view-data.png\" class=\"login\" height=\"70\">");

            htmlDivisionDetailsBody.Append("<h3 class=\"text-center\">" + "Division" + ":" + divisionDataObj.DivisionName + " / "  + divisionDataObj.DivisionCode + "</h3>");
        
            htmlDivisionDetailsBody.Append("<div class=\"panel-body\">");
            htmlDivisionDetailsBody.Append("<form id=\"viewDivisionDataPopupForm\" name=\"viewDivisionDataPopupForm\" role=\"form\" class=\"form form-horizontal\" method=\"post\">");

            htmlDivisionDetailsBody.Append("<div class=\"form-group\">");
            htmlDivisionDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

            htmlDivisionDetailsBody.Append("<div class=\"table-responsive\">");
            htmlDivisionDetailsBody.Append("<table id=\"divisionDataDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

            htmlDivisionDetailsBody.Append("<thead>");
            htmlDivisionDetailsBody.Append("</thead>");

            htmlDivisionDetailsBody.Append("<tbody>");
            htmlDivisionDetailsBody.Append("<tr>");
            htmlDivisionDetailsBody.Append("<td>" + "Circle Name" + "</td>");
            htmlDivisionDetailsBody.Append("<td>" + ":" + "</td>");
            htmlDivisionDetailsBody.Append("<td>" + divisionDataObj.CircleManager.CircleName.ToString() + "</td>");
            htmlDivisionDetailsBody.Append("</tr>");

            htmlDivisionDetailsBody.Append("<tr>");
            htmlDivisionDetailsBody.Append("<td>" + "Circle Code" + "</td>");
            htmlDivisionDetailsBody.Append("<td>" + ":" + "</td>");
            htmlDivisionDetailsBody.Append("<td>" + divisionDataObj.CircleManager.CircleCode.ToString() + "</td>");
            htmlDivisionDetailsBody.Append("</tr>");

            htmlDivisionDetailsBody.Append("<tr>");
            htmlDivisionDetailsBody.Append("<td>" + "Division Name" + "</td>");
            htmlDivisionDetailsBody.Append("<td>" + ":" + "</td>");
            htmlDivisionDetailsBody.Append("<td>" + divisionDataObj.DivisionName.ToString() + "</td>");
            htmlDivisionDetailsBody.Append("</tr>");

            htmlDivisionDetailsBody.Append("<tr>");
            htmlDivisionDetailsBody.Append("<td>" + "Division Code" + "</td>");
            htmlDivisionDetailsBody.Append("<td>" + ":" + "</td>");
            htmlDivisionDetailsBody.Append("<td>" + divisionDataObj.DivisionCode.ToString() + "</td>");
            htmlDivisionDetailsBody.Append("</tr>");

            return htmlDivisionDetailsBody.ToString();

        }

        [WebMethod]
        public bool UpdateDivision(string divisionGuid, string divisionCode, string divisionName)
        {
            return new DivisionDB().UpdateDivision(new Guid(divisionGuid), divisionCode, divisionName);
        }

        [WebMethod]
        public int DeleteDivision(Guid divisionGuid)
        {
            return new DivisionDB().DeleteDivision(divisionGuid);
        }

        [WebMethod]
        public List<SchemeManager> GetAllSchemesData()
        {
            List<SchemeManager> schemeList = new List<SchemeManager>();
            schemeList = new SchemeDB().GetAllSchemesData();

            return schemeList;
        }

        [WebMethod]
        public string[] GetDivisionsBasedOnCircleGuid(Guid circleGuid)
        {
            List<DivisionManager> divisionList = new List<DivisionManager>();
            divisionList = new DivisionDB().GetDivisionsBasedOnCircleGuid(circleGuid);

            List<String> divisionNameList = new List<string>();

            if (divisionList.Count > 0)
            {
                foreach (DivisionManager divisionObj in divisionList)
                {
                    divisionNameList.Add(divisionObj.DivisionGuid.ToString());
                    divisionNameList.Add(divisionObj.DivisionCode.ToString());
                    divisionNameList.Add(divisionObj.DivisionName.ToString());

                }
            }

            return divisionNameList.ToArray();
        }

        [WebMethod]
        public string ViewSchemeData(Guid schemeGuid)
        {
            SchemeManager schemeDataObj = new SchemeManager();
            schemeDataObj = new SchemeDB().GetSchemeDataBySchemeGuid(schemeGuid);

            StringBuilder htmlSchemeDetailsBody = new StringBuilder();
            htmlSchemeDetailsBody.Append("<div class=\"container-fluid col-md-12\">");
            htmlSchemeDetailsBody.Append("<div class=\"row\">");
            htmlSchemeDetailsBody.Append("<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">");
            htmlSchemeDetailsBody.Append("<div class=\"panel panel-default\">");
            htmlSchemeDetailsBody.Append("<div class=\"panel-body\">");
            htmlSchemeDetailsBody.Append("<div class=\"text-center\">");

            htmlSchemeDetailsBody.Append("<img src=\"Images/view-data.png\" class=\"login\" height=\"70\">");

            htmlSchemeDetailsBody.Append("<h3 class=\"text-center\">" + "Scheme" + ":" + schemeDataObj.SchemeName + " / " + schemeDataObj.SchemeCode + "</h3>");

            htmlSchemeDetailsBody.Append("<div class=\"panel-body\">");
            htmlSchemeDetailsBody.Append("<form id=\"viewSchemeDataPopupForm\" name=\"viewSchemeDataPopupForm\" role=\"form\" class=\"form form-horizontal\" method=\"post\">");

            htmlSchemeDetailsBody.Append("<div class=\"form-group\">");
            htmlSchemeDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

            htmlSchemeDetailsBody.Append("<div class=\"table-responsive\">");
            htmlSchemeDetailsBody.Append("<table id=\"schemeDataDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

            htmlSchemeDetailsBody.Append("<thead>");
            htmlSchemeDetailsBody.Append("</thead>");

            htmlSchemeDetailsBody.Append("<tbody>");
            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "Circle Name" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.CircleManager.CircleName.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "Circle Code" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.CircleManager.CircleCode.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "Division Name" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.DivisionManager.DivisionName.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "Division Code" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.DivisionManager.DivisionCode.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            htmlSchemeDetailsBody.Append("<tbody>");
            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "Scheme Name" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.SchemeName.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "Scheme Code" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.SchemeCode.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "Scheme Type" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.SchemeType.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "Utm East1" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.UtmEast1.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "Utm East2" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.UtmEast2.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "UTM North1" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.UtmNorth1.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "UTM North2" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.UtmNorth2.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "Drawing ID" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.DrawingId.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            htmlSchemeDetailsBody.Append("<tr>");
            htmlSchemeDetailsBody.Append("<td>" + "Area" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + ":" + "</td>");
            htmlSchemeDetailsBody.Append("<td>" + schemeDataObj.Area.ToString() + "</td>");
            htmlSchemeDetailsBody.Append("</tr>");

            return htmlSchemeDetailsBody.ToString();
        }            

        [WebMethod]
        public bool CheckSchemeExistence(string schemeCode, string schemeName)
        {
            return new SchemeDB().CheckSchemeExistence(schemeCode, schemeName);
        }        

        [WebMethod]
        public Guid SaveScheme(string schemeCode, string schemeType, string schemeName, string utmEast1, string utmEast2, string utmNorth1, string utmNorth2, string area, string circleGuid, string divisionGuid, string drawingId)
        {
            Guid schemeGuid = Guid.NewGuid();
            int returnValue = new SchemeDB().AddScheme(schemeGuid, schemeCode, schemeType, schemeName, (string.IsNullOrEmpty(utmEast1) ? 0 : int.Parse(utmEast1)), (string.IsNullOrEmpty(utmEast2) ? 0 : int.Parse(utmEast2)), (string.IsNullOrEmpty(utmNorth1) ? 0 : int.Parse(utmNorth1)), (string.IsNullOrEmpty(utmNorth2) ? 0 : int.Parse(utmNorth2)), (string.IsNullOrEmpty(area) ? 0 : int.Parse(area)), new Guid(circleGuid), new Guid(divisionGuid), drawingId);
            if (returnValue == 1)
            {
                return schemeGuid;
            }
            else
            {
                return new Guid();
            } 
        }

        [WebMethod]
        public int DeleteScheme(Guid schemeGuid)
        {
            return new SchemeDB().DeleteScheme(schemeGuid);
        }

        [WebMethod]
        public SchemeManager EditSchemeData(Guid schemeGuid)
        {
            SchemeManager schemeDataObj = new SchemeManager();
            schemeDataObj = new SchemeDB().GetSchemeDataBySchemeGuid(schemeGuid);
            return schemeDataObj;
        }

        [WebMethod]
        public string[] GetSchemeNames(string circleGuid, string divisionGuid)
        {
            List<SchemeManager> schemeList = new List<SchemeManager>();
            schemeList = new SchemeDB().GetSchemesBasedOnCircleAndDivision(new Guid(circleGuid), new Guid(divisionGuid));

            List<String> schemeNameList = new List<string>();

            if (schemeList.Count > 0)
            {
                foreach (SchemeManager schemeObj in schemeList)
                {
                    schemeNameList.Add(schemeObj.SchemeGuid.ToString());
                    schemeNameList.Add(schemeObj.SchemeCode.ToString());                    
                }
            }
            return schemeNameList.ToArray();
        }        

        [WebMethod]
        public bool UpdateScheme(string schemeGuid, string edittedSchemeCode, string edittedSchemeType, string edittedSchemeName, string edittedUtmEast1, string edittedUtmEast2, string edittedUtmNorth1, string edittedUtmNorth2, string edittedArea, string edittedDrawingId)
        {
            return new SchemeDB().UpdateSchemeData(new Guid (schemeGuid), edittedSchemeCode, edittedSchemeType, edittedSchemeName, (string.IsNullOrEmpty(edittedUtmEast1) ? 0 : int.Parse(edittedUtmEast1)), (string.IsNullOrEmpty(edittedUtmEast2) ? 0 : int.Parse(edittedUtmEast2)), (string.IsNullOrEmpty(edittedUtmNorth1) ? 0 : int.Parse(edittedUtmNorth1)), (string.IsNullOrEmpty(edittedUtmNorth2) ? 0 : int.Parse(edittedUtmNorth2)), (string.IsNullOrEmpty(edittedArea) ? 0 : int.Parse(edittedArea)), edittedDrawingId);                
        }

        [WebMethod]
        public string GetAssetCodeDataWithAssetRisk()
        {
            List<AssetCodeAndAssetRiskRatingRelationManager> assetCodeAndAssetRiskRatingRelationManagerList = new List<AssetCodeAndAssetRiskRatingRelationManager>();
            assetCodeAndAssetRiskRatingRelationManagerList = new AssetCodeAndAssetRiskRatingRelationDB().GetAssetsWithAssetRiskRatingData();

            //List<AssetRiskRatingManager> assetRiskRatingManagerList = new List<AssetRiskRatingManager>();
            //assetRiskRatingManagerList = new AssetRiskRatingDB().GetAssetRiskRating();

            string assetManagerListTableBody;

            StringBuilder htmlTable = new StringBuilder();
            htmlTable.Append("<div class=\"container-fluid col-md-12\">");
            htmlTable.Append("<div class=\"row\">");
            htmlTable.Append("<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">");
            htmlTable.Append("<div class=\"panel panel-default\">");
            htmlTable.Append("<div class=\"panel-body\">");
            htmlTable.Append("<div class=\"text-center\">");
            htmlTable.Append("<img src=\"Images/view-data.png\" class=\"login\" height=\"70\" />");
            htmlTable.Append("<h2 class=\"text-center\">Asset Risk Rating</h2>");
            htmlTable.Append("<div class=\"panel-body\">");
            htmlTable.Append("<form id=\"assetRiskRatingPopupForm\" name=\"assetRiskRatingPopupForm\" role=\"form\" class=\"form form-horizontal\" method=\"post\">");
            htmlTable.Append("<div class=\"form-group\">");
            htmlTable.Append("<div id=" + "\"table-container\"" + " style=" + "\"padding:1%;\">");

            htmlTable.Append("<div class=\"table-responsive\">");
            htmlTable.Append("<table id=\"assetRiskRatingDataList\" name=\"assetRiskRatingDataList\" class=\"table table-striped table-bordered table-hover table-responsive\" cellspacing=\"0\" width=\"100%\">");

            htmlTable.Append("<thead>");
            htmlTable.Append("<tr>");
            htmlTable.Append("<th>#</th>");
            htmlTable.Append("<th>Asset Guid</th>");
            htmlTable.Append("<th>Asset Code</th>");
            htmlTable.Append("<th>Asset Name</th>");
            htmlTable.Append("<th>Asset Risk Rating Relation Guid</th>");
            htmlTable.Append("<th>Asset Risk Rating Guid</th>");
            htmlTable.Append("<th>Asset Risk Rating</th>");
            htmlTable.Append("<th>Operations</th>");
            htmlTable.Append("</tr>");
            htmlTable.Append("</thead>");

            //htmlTable.Append("<tfoot>");
            //htmlTable.Append("<tr>");
            //htmlTable.Append("<th>#</th>");
            //htmlTable.Append("<th>Asset Guid</th>");
            //htmlTable.Append("<th>Asset Name</th>");
            //htmlTable.Append("<th>Asset Code</th>");
            //htmlTable.Append("<th>Division Code</th>");
            //htmlTable.Append("<th>Division Name</th>");
            //htmlTable.Append("<th>Scheme Code</th>");
            //htmlTable.Append("<th>Scheme Name</th>");
            //htmlTable.Append("<th>AssetType Code</th>");
            //htmlTable.Append("<th>AssetType Name</th>");
            //htmlTable.Append("<th>Operations</th>");
            //htmlTable.Append("</tr>");
            //htmlTable.Append("</tfoot>");

            htmlTable.Append("<tbody>");

            foreach (AssetCodeAndAssetRiskRatingRelationManager assetCodeAndAssetRiskRatingRelationManagerObj in assetCodeAndAssetRiskRatingRelationManagerList)
            {
                htmlTable.Append("<tr>");
                htmlTable.Append("<td></td>");
                htmlTable.Append("<td>" + assetCodeAndAssetRiskRatingRelationManagerObj.AssetManager.AssetGuid + "</td>");
                htmlTable.Append("<td>" + assetCodeAndAssetRiskRatingRelationManagerObj.AssetManager.AssetCode + "</td>");
                htmlTable.Append("<td>" + assetCodeAndAssetRiskRatingRelationManagerObj.AssetManager.AssetName + "</td>");
                htmlTable.Append("<td>" + assetCodeAndAssetRiskRatingRelationManagerObj.AssetCodeAssetRiskRatingRelationGuid + "</td>");
                htmlTable.Append("<td>" + assetCodeAndAssetRiskRatingRelationManagerObj.AssetRiskRatingManager.AssetRiskRatingGuid + "</td>");
                htmlTable.Append("<td>" + assetCodeAndAssetRiskRatingRelationManagerObj.AssetRiskRatingManager.AssetRiskRating + "</td>");
                
                htmlTable.Append("<td>" + "<a href=\"#\" class=\"editAssetRiskRatingRowBtn\"><i class=\"ui-tooltip fa fa-pencil-square-o\" style=\"font-size: 22px;\" data-original-title=\"Edit\" title=\"Edit\"></i></a>" + "</td>");
                htmlTable.Append("</tr>");
            }

            htmlTable.Append("</tbody>");
            htmlTable.Append("</table>");
            htmlTable.Append("</div>");

            htmlTable.Append("</div>");
            htmlTable.Append("</div>");

            htmlTable.Append("</form>");
            htmlTable.Append("</div>");

            htmlTable.Append("</div>");
            htmlTable.Append("</div>");
            htmlTable.Append("</div>");
            htmlTable.Append("</div>");
            htmlTable.Append("</div>");
            htmlTable.Append("</div>");

            assetManagerListTableBody = htmlTable.ToString();
            return assetManagerListTableBody;
        }


        [WebMethod]
        public List<AssetRiskRatingManager> GetAssetRiskRating()
        {
            List<AssetRiskRatingManager> assetRiskRatingManagerList = new List<AssetRiskRatingManager>();
            assetRiskRatingManagerList = new AssetRiskRatingDB().GetAssetRiskRating();
            return assetRiskRatingManagerList;
        }

        [WebMethod]
        public Guid SaveAssetCodeAndAssetRiskRatingRelation(AssetCodeAndAssetRiskRatingRelationManager assetCodeAndAssetRiskRatingRelationManager)
        {
            assetCodeAndAssetRiskRatingRelationManager.AssetCodeAssetRiskRatingRelationGuid = Guid.NewGuid();
            int rowsAffected = new AssetCodeAndAssetRiskRatingRelationDB().AddAssetCodeAndAssetRiskRatingRelation(assetCodeAndAssetRiskRatingRelationManager);
            if (rowsAffected == 1)
            {
                return assetCodeAndAssetRiskRatingRelationManager.AssetCodeAssetRiskRatingRelationGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public int UpdateAssetCodeAndAssetRiskRatingRelationData(string assetCodeAssetRiskRatingRelationGuid, string assetRiskRatingGuid)
        {
            return new AssetCodeAndAssetRiskRatingRelationDB().UpdateAssetCodeAndAssetRiskRatingRelationData(new Guid(assetCodeAssetRiskRatingRelationGuid), new Guid(assetRiskRatingGuid));
        }

        [WebMethod]
        public string GetSchemeCodeDataWithSchemeRisk()
        {
            List<SchemeCodeAndSchemeRiskRatingRelationManager> schemeCodeAndSchemeRiskRatingRelationManagerList = new List<SchemeCodeAndSchemeRiskRatingRelationManager>();
            schemeCodeAndSchemeRiskRatingRelationManagerList = new SchemeCodeAndSchemeRiskRatingRelationDB().GetSchemesWithSchemeRiskRatingData();

            List<SchemeRiskRatingManager> schemeRiskRatingManagerList = new List<SchemeRiskRatingManager>();
            schemeRiskRatingManagerList = new SchemeRiskRatingDB().GetSchemeRiskRating();

            string schemeManagerListTableBody;

            StringBuilder htmlTable = new StringBuilder();
            htmlTable.Append("<div class=\"container-fluid col-md-12\">");
            htmlTable.Append("<div class=\"row\">");
            htmlTable.Append("<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">");
            htmlTable.Append("<div class=\"panel panel-default\">");
            htmlTable.Append("<div class=\"panel-body\">");
            htmlTable.Append("<div class=\"text-center\">");
            htmlTable.Append("<img src=\"Images/view-data.png\" class=\"login\" height=\"70\" />");
            htmlTable.Append("<h2 class=\"text-center\">Scheme Risk Rating</h2>");
            htmlTable.Append("<div class=\"panel-body\">");
            htmlTable.Append("<form id=\"schemeRiskRatingPopupForm\" name=\"schemeRiskRatingPopupForm\" role=\"form\" class=\"form form-horizontal\" method=\"post\">");
            htmlTable.Append("<div class=\"form-group\">");
            htmlTable.Append("<div id=" + "\"table-container\"" + " style=" + "\"padding:1%;\">");

            htmlTable.Append("<div class=\"table-responsive\">");
            htmlTable.Append("<table id=\"schemeRiskRatingDataList\" name=\"schemeRiskRatingDataList\" class=\"table table-striped table-bordered table-hover table-responsive\" cellspacing=\"0\" width=\"100%\">");

            htmlTable.Append("<thead>");
            htmlTable.Append("<tr>");
            htmlTable.Append("<th>#</th>");
            htmlTable.Append("<th>Scheme Guid</th>");
            htmlTable.Append("<th>Scheme Code</th>");
            htmlTable.Append("<th>Scheme Name</th>");
            htmlTable.Append("<th>Scheme Risk Rating Relation Guid</th>");
            htmlTable.Append("<th>Scheme Risk Rating Guid</th>");
            htmlTable.Append("<th>Scheme Risk Rating</th>");
            htmlTable.Append("<th>Operations</th>");
            htmlTable.Append("</tr>");
            htmlTable.Append("</thead>");

            //htmlTable.Append("<tfoot>");
            //htmlTable.Append("<tr>");
            //htmlTable.Append("<th>#</th>");
            //htmlTable.Append("<th>Asset Guid</th>");
            //htmlTable.Append("<th>Asset Name</th>");
            //htmlTable.Append("<th>Asset Code</th>");
            //htmlTable.Append("<th>Division Code</th>");
            //htmlTable.Append("<th>Division Name</th>");
            //htmlTable.Append("<th>Scheme Code</th>");
            //htmlTable.Append("<th>Scheme Name</th>");
            //htmlTable.Append("<th>AssetType Code</th>");
            //htmlTable.Append("<th>AssetType Name</th>");
            //htmlTable.Append("<th>Operations</th>");
            //htmlTable.Append("</tr>");
            //htmlTable.Append("</tfoot>");

            htmlTable.Append("<tbody>");

            foreach (SchemeCodeAndSchemeRiskRatingRelationManager schemeCodeAndSchemeRiskRatingRelationManagerObj in schemeCodeAndSchemeRiskRatingRelationManagerList)
            {
                htmlTable.Append("<tr>");
                htmlTable.Append("<td></td>");
                htmlTable.Append("<td>" + schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeManager.SchemeGuid + "</td>");
                htmlTable.Append("<td>" + schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeManager.SchemeCode + "</td>");
                htmlTable.Append("<td>" + schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeManager.SchemeName + "</td>");
                htmlTable.Append("<td>" + schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeCodeSchemeRiskRatingRelationGuid + "</td>");
                htmlTable.Append("<td>" + schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeRiskRatingManager.SchemeRiskRatingGuid + "</td>");
                htmlTable.Append("<td>" + schemeCodeAndSchemeRiskRatingRelationManagerObj.SchemeRiskRatingManager.SchemeRiskRating + "</td>");
                htmlTable.Append("<td>" + "<a href=\"#\" class=\"editSchemeRiskRatingRowBtn\"><i class=\"ui-tooltip fa fa-pencil-square-o\" style=\"font-size: 22px;\" data-original-title=\"Save\" title=\"Save\"></i></a>");
                htmlTable.Append("</tr>");
            }         
            htmlTable.Append("</tbody>");
            htmlTable.Append("</table>");
            htmlTable.Append("</div>");

            htmlTable.Append("</div>");
            htmlTable.Append("</div>");

            htmlTable.Append("</form>");
            htmlTable.Append("</div>");

            htmlTable.Append("</div>");
            htmlTable.Append("</div>");
            htmlTable.Append("</div>");
            htmlTable.Append("</div>");
            htmlTable.Append("</div>");
            htmlTable.Append("</div>");

            schemeManagerListTableBody = htmlTable.ToString();
            return schemeManagerListTableBody;
        }

        [WebMethod]
        public List<SchemeRiskRatingManager> GetSchemeRiskRating()
        {
            List<SchemeRiskRatingManager> schemeRiskRatingManagerList = new List<SchemeRiskRatingManager>();
            schemeRiskRatingManagerList = new SchemeRiskRatingDB().GetSchemeRiskRating();
            return schemeRiskRatingManagerList;      
        }

        [WebMethod]
        public Guid SaveSchemeCodeAndSchemeRiskRatingRelation(SchemeCodeAndSchemeRiskRatingRelationManager schemeCodeAndSchemeRiskRatingRelationManager)
        {
            schemeCodeAndSchemeRiskRatingRelationManager.SchemeCodeSchemeRiskRatingRelationGuid = Guid.NewGuid();
            int rowsAffected = new SchemeCodeAndSchemeRiskRatingRelationDB().AddSchemeCodeAndSchemeRiskRatingRelation(schemeCodeAndSchemeRiskRatingRelationManager);
            if (rowsAffected == 1)
            {
                return schemeCodeAndSchemeRiskRatingRelationManager.SchemeCodeSchemeRiskRatingRelationGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public int UpdateSchemeCodeAndSchemeRiskRatingRelationData(string schemeCodeSchemeRiskRatingRelationGuid, string schemeRiskRatingGuid)
        {
            return new SchemeCodeAndSchemeRiskRatingRelationDB().UpdateSchemeCodeAndSchemeRiskRatingRelationData(new Guid(schemeCodeSchemeRiskRatingRelationGuid), new Guid(schemeRiskRatingGuid));
        }
        
        [WebMethod]
        public List<RoutineIntensityFactorManager> GetRoutineIntensityFactorData()
        {
            List<RoutineIntensityFactorManager> routineIntensityFactorManagerList = new List<RoutineIntensityFactorManager>();
            routineIntensityFactorManagerList = new RoutineIntensityFactorDB().GetRoutineIntensityFactorData();
            return routineIntensityFactorManagerList;
        }

        [WebMethod]
        public int UpdateRoutineIntensityFactorData(string routineIntensityFactorGuid, string routineIntensityFactor, string routineIntensityFactorJustification)
        {
            return new RoutineIntensityFactorDB().UpdateRoutineIntensityFactorData(new Guid(routineIntensityFactorGuid), Convert.ToInt16(routineIntensityFactor), routineIntensityFactorJustification);
        }        

        [WebMethod]
        public List<RoutineServiceLevelManager> GetRoutineServiceLevelData()
        {
            List<RoutineServiceLevelManager> routineServiceLevelManagerList = new List<RoutineServiceLevelManager>();
            routineServiceLevelManagerList = new RoutineServiceLevelDB().GetRoutineServiceLevelData();
            return routineServiceLevelManagerList;
        }

        [WebMethod]
        public int UpdateRoutineServiceLevelData(string routineServiceLevelGuid, string routineServiceLevel)
        {
            return new RoutineServiceLevelDB().UpdateRoutineServiceLevelData(new Guid(routineServiceLevelGuid), Convert.ToInt16(routineServiceLevel));
        }

        [WebMethod]
        public List<RoutineWorkItemClassManager> GetWorkItemClass()
        {
            List<RoutineWorkItemClassManager> routineWorkItemManagerList = new List<RoutineWorkItemClassManager>();
            routineWorkItemManagerList = new RoutineWorkItemClassDB().GetWorkItemClass();
            return routineWorkItemManagerList;
        }

        [WebMethod]
        public List<RoutineWorkItemTypeManager> GetWorkItemType(string routineWorkItemClassGuid)
        {
            List<RoutineWorkItemTypeManager> routineWorkItemTypeManagerList = new List<RoutineWorkItemTypeManager>();
            routineWorkItemTypeManagerList = new RoutineWorkItemTypeDB().GetWorkItemType(new Guid(routineWorkItemClassGuid));
            return routineWorkItemTypeManagerList;
        }

        [WebMethod]
        public RoutineWorkItemTypeManager GetWorkItemNumber(string routineWorkItemTypeGuid)
        {
            RoutineWorkItemTypeManager routineWorkItemTypeManagerObj = new RoutineWorkItemTypeManager();
            routineWorkItemTypeManagerObj = new RoutineWorkItemTypeDB().GetWorkItemNumber(new Guid(routineWorkItemTypeGuid));
            return routineWorkItemTypeManagerObj;
        }

        [WebMethod]
        public List<AssetQuantityParameterManager> GetAssetQuantityParameter()
        {
            List<AssetQuantityParameterManager> assetQuantityParameterManagerList = new List<AssetQuantityParameterManager>();
            assetQuantityParameterManagerList = new AssetQuantityParameterDB().GetAssetQuantityParameter();
            return assetQuantityParameterManagerList;
        }

        [WebMethod]
        public AssetQuantityParameterManager GetUnitForAssetQuantityParameter(string assetQuantityParameterGuid)
        {
            AssetQuantityParameterManager assetQuantityParameterManagerObj = new AssetQuantityParameterManager();
            assetQuantityParameterManagerObj = new AssetQuantityParameterDB().GetUnitForAssetQuantityParameter(new Guid(assetQuantityParameterGuid));
            return assetQuantityParameterManagerObj;
        }

        [WebMethod]
        public List<RoutineWorkItemManager> GetRoutineWorkItemsData()
        {
            List<RoutineWorkItemManager> routineWorkItemManagerList = new List<RoutineWorkItemManager>();
            routineWorkItemManagerList = new RoutineWorkItemDB().GetRoutineWorkItemsData();
            return routineWorkItemManagerList;
        }

        [WebMethod]
        public Guid SaveRoutineWorkItemData(RoutineWorkItemManager routineWorkItemManager)
        {
            return routineWorkItemManager.RoutineWorkItemGuid = Guid.NewGuid();
        }

        [WebMethod]
        public Guid UpdateRoutineWorkItemTypeNumber(RoutineWorkItemManager routineWorkItemManager)
        {
            routineWorkItemManager.RoutineWorkItemGuid = Guid.NewGuid();
            int rowsAffected = new RoutineWorkItemDB().AddRoutineWorkItem(routineWorkItemManager);
            if (rowsAffected == 1)
            {                
               new RoutineWorkItemTypeDB().UpdateWorkItemTypeNumber(routineWorkItemManager);
               return routineWorkItemManager.RoutineWorkItemGuid;
            }
            else
            {
                return new Guid();
            }          
        }

        [WebMethod]
        public string ViewRoutineWorkItemData(Guid routineWorkItemGuid)
        {
            RoutineWorkItemManager routineWorkItemDataObj = new RoutineWorkItemManager();
            routineWorkItemDataObj = new RoutineWorkItemDB().GetRoutineWorkItemDataByRoutineWorkItemUuid(routineWorkItemGuid);

            StringBuilder htmlRoutineWorkItemDetailsBody = new StringBuilder();
            htmlRoutineWorkItemDetailsBody.Append("<div class=\"container-fluid col-md-12\">");
            htmlRoutineWorkItemDetailsBody.Append("<div class=\"row\">");
            htmlRoutineWorkItemDetailsBody.Append("<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">");
            htmlRoutineWorkItemDetailsBody.Append("<div class=\"panel panel-default\">");
            htmlRoutineWorkItemDetailsBody.Append("<div class=\"panel-body\">");
            htmlRoutineWorkItemDetailsBody.Append("<div class=\"text-center\">");

            htmlRoutineWorkItemDetailsBody.Append("<img src=\"Images/view-data.png\" class=\"login\" height=\"70\">");
            htmlRoutineWorkItemDetailsBody.Append("<h3 class=\"text-center\">" + "Class Name" + ":" + routineWorkItemDataObj.RoutineWorkItemClassManager.RoutineWorkItemClassName + "</h3>");
            htmlRoutineWorkItemDetailsBody.Append("<h3 class=\"text-center\">" + "Asset Name" + ":" + routineWorkItemDataObj.RoutineWorkItemTypeCode + "</h3>");

            htmlRoutineWorkItemDetailsBody.Append("<div class=\"panel-body\">");
            htmlRoutineWorkItemDetailsBody.Append("<form id=\"viewRoutineWorkItemDataPopupForm\" name=\"viewRoutineWorkItemDataPopupForm\" role=\"form\" class=\"form form-horizontal\" method=\"post\">");

            htmlRoutineWorkItemDetailsBody.Append("<div class=\"form-group\">");
            htmlRoutineWorkItemDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

            htmlRoutineWorkItemDetailsBody.Append("<div class=\"table-responsive\">");
            htmlRoutineWorkItemDetailsBody.Append("<table id=\"routineWorkItemDataDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

            htmlRoutineWorkItemDetailsBody.Append("<thead>");
            htmlRoutineWorkItemDetailsBody.Append("</thead>");

            htmlRoutineWorkItemDetailsBody.Append("<tbody>");
            htmlRoutineWorkItemDetailsBody.Append("<tr>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + "Class Name" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + ":" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + routineWorkItemDataObj.RoutineWorkItemClassManager.RoutineWorkItemClassName.ToString() + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("</tr>");

            htmlRoutineWorkItemDetailsBody.Append("<tr>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + "Work Item Type" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + ":" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + routineWorkItemDataObj.RoutineWorkItemTypeManager.RoutineWorkItemTypeName.ToString() + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("</tr>");

            htmlRoutineWorkItemDetailsBody.Append("<tr>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + "Work Item Type Code" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + ":" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + routineWorkItemDataObj.RoutineWorkItemTypeCode.ToString() + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("</tr>");

            htmlRoutineWorkItemDetailsBody.Append("<tr>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + "Asset Quantity Parameter" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + ":" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + routineWorkItemDataObj.AssetQuantityParameterManager.AssetQuantityParameterName.ToString() + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("</tr>");

            htmlRoutineWorkItemDetailsBody.Append("<tr>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + "Unit" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + ":" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + routineWorkItemDataObj.AssetQuantityParameterManager.UnitManager.UnitName.ToString() + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("</tr>");

            htmlRoutineWorkItemDetailsBody.Append("<tr>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + "Short Description" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + ":" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + routineWorkItemDataObj.ShortDescription.ToString() + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("</tr>");

            htmlRoutineWorkItemDetailsBody.Append("<tr>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + "Full Description" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + ":" + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("<td>" + routineWorkItemDataObj.LongDescription.ToString() + "</td>");
            htmlRoutineWorkItemDetailsBody.Append("</tr>");

            return htmlRoutineWorkItemDetailsBody.ToString();

        }

        [WebMethod]
        public RoutineWorkItemManager EditRoutineWorkItemData(Guid routineWorkItemGuid)
        {
            RoutineWorkItemManager routineWorkItemDataObj = new RoutineWorkItemManager();
            routineWorkItemDataObj = new RoutineWorkItemDB().GetRoutineWorkItemDataByRoutineWorkItemUuid(routineWorkItemGuid);
            return routineWorkItemDataObj;
        }

        [WebMethod]
        public int UpdateRoutineWorkItemData(RoutineWorkItemManager routineWorkItemManager)
        {
            return new RoutineWorkItemDB().UpdateRoutineWorkItemData(new Guid(routineWorkItemManager.RoutineWorkItemGuid.ToString()), routineWorkItemManager);
        }

        [WebMethod]
        public int DeleteRoutineWorkItemData(Guid routineWorkItemGuid)
        {
            return new RoutineWorkItemDB().DeleteRoutineWorkItemData(routineWorkItemGuid);           
        }

        [WebMethod]
        public SystemVariablesManager GetSystemVariablesGuid()
        {
            SystemVariablesManager systemVariablesManagerObj = new SystemVariablesManager();
            systemVariablesManagerObj = new SystemVariablesDB().GetSystemVariablesGuid();
            return systemVariablesManagerObj;
        }

        [WebMethod]
        public List<RoutineWorkItemManager> GetRoutineWorkItemRateData()
        {
            List<RoutineWorkItemManager> routineWorkItemRateManagerList = new List<RoutineWorkItemManager>();
            routineWorkItemRateManagerList = new RoutineWorkItemRateDB().GetRoutineWorkItemRateData();

            return routineWorkItemRateManagerList;
        }

        [WebMethod]
        public Guid SaveRoutineWorkItemRateData(RoutineWorkItemRateManager routineWorkItemRateManager)
        {
            routineWorkItemRateManager.RoutineWorkItemRateGuid = Guid.NewGuid();
            int result = new RoutineWorkItemRateDB().AddRoutineWorkItemRateData(routineWorkItemRateManager);
            if (result == 1)
            {
                return routineWorkItemRateManager.RoutineWorkItemRateGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public int UpdateRoutineWorkItemRateData(string routineWorkItemRateGuid, string routineWorkItemRate)
        {
            return new RoutineWorkItemRateDB().UpdateRoutineWorkItemRateData(new Guid(routineWorkItemRateGuid), Money.Parse(routineWorkItemRate));
        }

        [WebMethod]
        public List<RepairFundingManager> GetRepairFundingData()
        {
            List<RepairFundingManager> repairFundingManagerList = new List<RepairFundingManager>();
            repairFundingManagerList = new RepairFundingDB().GetRepairFundingData();

            return repairFundingManagerList;
        }

        [WebMethod]
        public bool CheckFiscalYearExistence(string fiscalYear)
        {
            return new RepairFundingDB().CheckFiscalYearExistence(fiscalYear);
        }

        [WebMethod]
        public Guid SaveRepairFundingData(RepairFundingManager repairFundingManager)
        {
            repairFundingManager.RepairFundingGuid = Guid.NewGuid();
            int returnValue = new RepairFundingDB().SaveRepairFundingData(repairFundingManager);

            if (returnValue == 1)
            {
                return repairFundingManager.RepairFundingGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public int UpdateRepairFundingData(RepairFundingManager repairFundingManager)
        {
            return new RepairFundingDB().UpdateRepairFundingData(new Guid(repairFundingManager.RepairFundingGuid.ToString()), repairFundingManager);
        }

        [WebMethod]
        public int DeleteRepairFundingData(Guid repairFundingGuid)
        {
            return new RepairFundingDB().DeleteRepairFundingData(repairFundingGuid);
        }

        #region DISTRICT

        [WebMethod]
        public List<DistrictManager> GetDistricts()
        {
            List<DistrictManager> districtManagerList = new List<DistrictManager>();
            districtManagerList = new DistrictDB().GetDistricts();

            return districtManagerList;
        }

        #endregion




        #region WORKITEM

        [WebMethod]
        public List<WorkItemManager> GetWorkItems()
        {
            List<WorkItemManager> workItemList = new List<WorkItemManager>();
            workItemList = new WorkItemDB().GetWorkItemsWithUnitAndWorkItemSubHeads();

            //Testing
            //WorkItemManager a = new WorkItemDB().GetWorkItem(new Guid());
            //WorkItemManager b = new WorkItemDB().GetWorkItemWithUnit(new Guid());
            //WorkItemManager c = new WorkItemDB().GetWorkItemWithWorkItemSubHeads(new Guid());
            //WorkItemManager d = new WorkItemDB().GetWorkItemWithUnitAndWorkItemSubHeads(new Guid());

            return workItemList;
        }

        [WebMethod]
        public WorkItemManager GetWorkItem(Guid workItemGuid)
        {
            WorkItemManager workItemManagerObj = new WorkItemManager();
            workItemManagerObj = new WorkItemDB().GetWorkItemWithUnitAndWorkItemSubHeads(workItemGuid);

            return workItemManagerObj;
        }

        [WebMethod]
        public bool CheckWorkItemExistence(string workItemName)
        {
            return new WorkItemDB().CheckWorkItemExistence(workItemName);
        }

        [WebMethod]
        public Guid SaveWorkItem(WorkItemManager workItemManager)
        {
            workItemManager.WorkItemGuid = Guid.NewGuid();
            int returnValue = new WorkItemDB().AddWorkItem(workItemManager);

            if (returnValue == 1)
            {
                return workItemManager.WorkItemGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public int UpdateWorkItem(WorkItemManager workItemManager, bool editWorkItemName)
        {
            if (editWorkItemName == false)
            {
                return new WorkItemDB().UpdateWorkItem(workItemManager);
            }
            else
            {
                return new WorkItemDB().UpdateWorkItemWithoutWorkItemName(workItemManager);
            }
        }

        [WebMethod]
        public int DeleteWorkItem(Guid workItemGuid)
        {
            return new WorkItemDB().DeleteWorkItem(workItemGuid);
        }

        #endregion




        #region WORKITEM SUBHEAD

        [WebMethod]
        public List<WorkItemSubHeadManager> GetWorkItemSubHeads()
        {
            List<WorkItemSubHeadManager> workItemSubHeadManagerList = new List<WorkItemSubHeadManager>();
            workItemSubHeadManagerList = new WorkItemSubHeadDB().GetWorkItemSubHeads();

            return workItemSubHeadManagerList;
        }

        [WebMethod]
        public bool CheckWorkItemSubHeadExistence(string subHeadName)
        {
            return new WorkItemSubHeadDB().CheckWorkItemSubHeadExistence(subHeadName);
        }

        [WebMethod]
        public Guid SaveWorkItemSubHead(string workItemSubHeadName)
        {
            Guid workItemSubHeadGuid = Guid.NewGuid();
            int returnValue = new WorkItemSubHeadDB().AddWorkItemSubHead(workItemSubHeadGuid, workItemSubHeadName);

            if (returnValue == 1)
            {
                return workItemSubHeadGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public int UpdateWorkItemSubHead(string workItemSubHeadName, Guid workItemSubHeadGuid)
        {
            return new WorkItemSubHeadDB().UpdateWorkItemSubHead(workItemSubHeadGuid, workItemSubHeadName);
        }

        [WebMethod]
        public int DeleteWorkItemSubHead(Guid workItemSubHeadGuid)
        {
            return new WorkItemSubHeadDB().DeleteWorkItemSubHead(workItemSubHeadGuid);
        }

        #endregion




        #region FINANCIAL SUBHEAD

        [WebMethod]
        public List<FinancialSubHeadManager> GetFinancialSubHeads()
        {
            List<FinancialSubHeadManager> financialSubHeadManagerList = new List<FinancialSubHeadManager>();
            financialSubHeadManagerList = new FinancialSubHeadDB().GetFinancialSubHeads();

            return financialSubHeadManagerList;

        }

        [WebMethod]
        public bool CheckFinancialSubHeadExistence(string financialSubHeadName)
        {
            return new FinancialSubHeadDB().CheckFinancialSubHeadExistence(financialSubHeadName);
        }

        [WebMethod]
        public Guid SaveFinancialSubHead(string financialSubHeadName)
        {
            Guid financialSubHeadGuid = Guid.NewGuid();
            int returnValue = new FinancialSubHeadDB().AddFinancialSubHead(financialSubHeadGuid, financialSubHeadName);

            if (returnValue == 1)
            {
                return financialSubHeadGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public int UpdateFinancialSubHead(string financialSubHeadName, Guid financialSubHeadGuid)
        {
            return new FinancialSubHeadDB().UpdateFinancialSubHead(financialSubHeadGuid, financialSubHeadName);
        }

        [WebMethod]
        public int DeleteFinancialSubHead(Guid financialSubHeadGuid)
        {
            return new FinancialSubHeadDB().DeleteFinancialSubHead(financialSubHeadGuid);
        }

        #endregion




        #region DIVISION

        [WebMethod]
        public List<DivisionManager> GetDivisions()
        {
            List<DivisionManager> divisionManagerList = new List<DivisionManager>();
            divisionManagerList = new DivisionDB().GetDivisions();

            return divisionManagerList;
        }

        //[WebMethod]
        //public WorkItemManager GetWorkItem(Guid workItemGuid)
        //{
        //    WorkItemManager workItemManagerObj = new WorkItemManager();
        //    workItemManagerObj = new WorkItemDB().GetWorkItemWithUnitAndWorkItemSubHeads(workItemGuid);

        //    return workItemManagerObj;
        //}

        //[WebMethod]
        //public bool CheckWorkItemExistence(string workItemName)
        //{
        //    return new WorkItemDB().CheckWorkItemExistence(workItemName);
        //}

        //[WebMethod]
        //public Guid SaveWorkItem(WorkItemManager workItemManager)
        //{
        //    workItemManager.WorkItemGuid = Guid.NewGuid();
        //    int returnValue = new WorkItemDB().AddWorkItem(workItemManager);

        //    if (returnValue == 1)
        //    {
        //        return workItemManager.WorkItemGuid;
        //    }
        //    else
        //    {
        //        return new Guid();
        //    }
        //}

        //[WebMethod]
        //public int UpdateWorkItem(WorkItemManager workItemManager, bool editWorkItemName)
        //{
        //    if (editWorkItemName == false)
        //    {
        //        return new WorkItemDB().UpdateWorkItem(workItemManager);
        //    }
        //    else
        //    {
        //        return new WorkItemDB().UpdateWorkItemWithoutWorkItemName(workItemManager);
        //    }
        //}

        //[WebMethod]
        //public int DeleteWorkItem(Guid workItemGuid)
        //{
        //    return new WorkItemDB().DeleteWorkItem(workItemGuid);
        //}

        #endregion




        #region SCHEME

        [WebMethod]
        public List<SchemeManager> GetSchemes(Guid divisionGuid)
        {
            List<SchemeManager> schemeManagerList = new List<SchemeManager>();
            schemeManagerList = new SchemeDB().GetSchemesBasedOnDivisions(divisionGuid);

            return schemeManagerList;
        }

        //[WebMethod]
        //public WorkItemManager GetWorkItem(Guid workItemGuid)
        //{
        //    WorkItemManager workItemManagerObj = new WorkItemManager();
        //    workItemManagerObj = new WorkItemDB().GetWorkItemWithUnitAndWorkItemSubHeads(workItemGuid);

        //    return workItemManagerObj;
        //}

        //[WebMethod]
        //public bool CheckWorkItemExistence(string workItemName)
        //{
        //    return new WorkItemDB().CheckWorkItemExistence(workItemName);
        //}

        //[WebMethod]
        //public Guid SaveWorkItem(WorkItemManager workItemManager)
        //{
        //    workItemManager.WorkItemGuid = Guid.NewGuid();
        //    int returnValue = new WorkItemDB().AddWorkItem(workItemManager);

        //    if (returnValue == 1)
        //    {
        //        return workItemManager.WorkItemGuid;
        //    }
        //    else
        //    {
        //        return new Guid();
        //    }
        //}

        //[WebMethod]
        //public int UpdateWorkItem(WorkItemManager workItemManager, bool editWorkItemName)
        //{
        //    if (editWorkItemName == false)
        //    {
        //        return new WorkItemDB().UpdateWorkItem(workItemManager);
        //    }
        //    else
        //    {
        //        return new WorkItemDB().UpdateWorkItemWithoutWorkItemName(workItemManager);
        //    }
        //}

        //[WebMethod]
        //public int DeleteWorkItem(Guid workItemGuid)
        //{
        //    return new WorkItemDB().DeleteWorkItem(workItemGuid);
        //}

        #endregion




        #region UNIT TYPES

        [WebMethod]
        public List<UnitTypeManager> GetUnitTypes()
        {
            List<UnitTypeManager> unitTypeManagerList = new List<UnitTypeManager>();
            unitTypeManagerList = new UnitTypeDB().GetUnitTypes();

            return unitTypeManagerList;
        }

        [WebMethod]
        public bool CheckUnitTypeExistence(string unitTypeName)
        {
            return new UnitTypeDB().CheckUnitTypeExistence(unitTypeName);
        }

        [WebMethod]
        public Guid SaveUnitType(string unitTypeName)
        {
            Guid unitTypeGuid = Guid.NewGuid();
            int returnValue = new UnitTypeDB().AddUnitType(unitTypeGuid, unitTypeName);

            if (returnValue == 1)
            {
                return unitTypeGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public int UpdateUnitType(string unitTypeName, Guid unitTypeGuid)
        {
            return new UnitTypeDB().UpdateUnitType(unitTypeGuid, unitTypeName);
        }

        [WebMethod]
        public int DeleteUnitType(Guid unitTypeGuid)
        {
            return new UnitTypeDB().DeleteUnitType(unitTypeGuid);
        }

        #endregion




        #region UNITS

        [WebMethod]
        public List<UnitManager> GetUnits(Guid unitTypeGuid)
        {
            List<UnitManager> unitManagerList = new List<UnitManager>();
            unitManagerList = new UnitDB().GetUnitsBasedOnUnitType(unitTypeGuid);

            return unitManagerList;
        }

        [WebMethod]
        public bool CheckUnitExistence(string unitName, Guid unitTypeGuid)
        {
            return new UnitDB().CheckUnitExistence(unitName, unitTypeGuid);
        }

        [WebMethod]
        public Guid SaveUnit(UnitManager unitManager)
        {
            unitManager.UnitGuid = Guid.NewGuid();
            int returnValue = new UnitDB().AddUnit(unitManager);

            if (returnValue == 1)
            {
                return unitManager.UnitGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public int UpdateUnit(UnitManager unitManager)
        {
            return new UnitDB().UpdateUnit(unitManager);
        }

        [WebMethod]
        public int DeleteUnit(Guid unitTypeGuid, Guid unitGuid)
        {
            return new UnitDB().DeleteUnit(unitTypeGuid, unitGuid);
        }

        #endregion




        #region USERS

        [WebMethod]
        public List<UserManager> GetUsersBasedOnRoleName()
        {
            List<UserManager> userManagerList = new List<UserManager>();
            userManagerList = new UserDB().GetUsersBasedOnRoleName("SIO");

            return userManagerList;
        }

        //[WebMethod]
        //public bool CheckUnitExistence(string unitName, Guid unitTypeGuid)
        //{
        //    return new UnitDB().CheckUnitExistence(unitName, unitTypeGuid);
        //}

        //[WebMethod]
        //public Guid SaveUnit(UnitManager unitManager)
        //{
        //    unitManager.UnitGuid = Guid.NewGuid();
        //    int returnValue = new UnitDB().AddUnit(unitManager);

        //    if (returnValue == 1)
        //    {
        //        return unitManager.UnitGuid;
        //    }
        //    else
        //    {
        //        return new Guid();
        //    }
        //}

        //[WebMethod]
        //public int UpdateUnit(UnitManager unitManager)
        //{
        //    return new UnitDB().UpdateUnit(unitManager);
        //}

        //[WebMethod]
        //public int DeleteUnit(Guid unitTypeGuid, Guid unitGuid)
        //{
        //    return new UnitDB().DeleteUnit(unitTypeGuid, unitGuid);
        //}

        #endregion




        #region DESIGNATIONS

        [WebMethod]
        public List<DesignationManager> GetDesignations()
        {
            List<DesignationManager> designationManagerList = new List<DesignationManager>();
            designationManagerList = new DesignationDB().GetDesignations();

            return designationManagerList;
        }

        //[WebMethod]
        //public bool CheckUnitExistence(string unitName, Guid unitTypeGuid)
        //{
        //    return new UnitDB().CheckUnitExistence(unitName, unitTypeGuid);
        //}

        //[WebMethod]
        //public Guid SaveUnit(UnitManager unitManager)
        //{
        //    unitManager.UnitGuid = Guid.NewGuid();
        //    int returnValue = new UnitDB().AddUnit(unitManager);

        //    if (returnValue == 1)
        //    {
        //        return unitManager.UnitGuid;
        //    }
        //    else
        //    {
        //        return new Guid();
        //    }
        //}

        //[WebMethod]
        //public int UpdateUnit(UnitManager unitManager)
        //{
        //    return new UnitDB().UpdateUnit(unitManager);
        //}

        //[WebMethod]
        //public int DeleteUnit(Guid unitTypeGuid, Guid unitGuid)
        //{
        //    return new UnitDB().DeleteUnit(unitTypeGuid, unitGuid);
        //}

        #endregion




        [WebMethod(EnableSession = true)]
        public void UserLogout()
        {
            System.Web.HttpContext.Current.Session.Clear();
            System.Web.HttpContext.Current.Session.Abandon();
        }

    }
}
