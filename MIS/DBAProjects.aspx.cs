﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class DBAProjects : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "DBA")
                {
                    if (!Page.IsPostBack)
                    {
                        DrawProjectList();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                    //Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("DBALogin.aspx");
            }
        }

        private void DrawProjectList()
        {
            List<ProjectManager> projectManagerList = new ProjectDB().GetProjects();

            StringBuilder htmlProjectList = new StringBuilder();
            htmlProjectList.Append("<div id=\"table-container\" style=\"padding:1%;\">");

            //if (projectManagerList.Count > 0)
            //{
                htmlProjectList.Append("<div class=\"table-responsive\">");
                htmlProjectList.Append("<table id=\"dbaProjectsList\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

                htmlProjectList.Append("<thead>");
                htmlProjectList.Append("<tr>");
                htmlProjectList.Append("<th>" + "#" + "</th>");
                htmlProjectList.Append("<th>" + "Project Guid" + "</th>");
                htmlProjectList.Append("<th>" + "Project Name" + "</th>");
                htmlProjectList.Append("<th>" + "Status Code" + "</th>");
                htmlProjectList.Append("<th>" + "Status" + "</th>");
                htmlProjectList.Append("<th>" + "Project Creation Date" + "</th>");
                htmlProjectList.Append("<th>" + "Operations" + "</th>");
                htmlProjectList.Append("</tr>");
                htmlProjectList.Append("</thead>");

                htmlProjectList.Append("<tbody>");

                foreach (ProjectManager projectManagerObj in projectManagerList)
                {
                    htmlProjectList.Append("<tr>");

                    htmlProjectList.Append("<td>" + "" + "</td>");
                    htmlProjectList.Append("<td>" + projectManagerObj.ProjectGuid + "</td>");
                    htmlProjectList.Append("<td>" + projectManagerObj.ProjectName + "</td>");
                    htmlProjectList.Append("<td>" + projectManagerObj.ProjectStatus + "</td>");
                    htmlProjectList.Append("<td>" + (ProjectStatusEnum)Enum.Parse(typeof(ProjectStatusEnum), projectManagerObj.ProjectStatus.ToString()) + "</td>");
                    htmlProjectList.Append("<td>" + projectManagerObj.ProjectCreationDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                    htmlProjectList.Append("<td>" + "<a href=\"#\" class=\"viewDbaProjectBtn\"><i class=\"ui-tooltip fa fa-file-text-o\" style=\"font-size: 22px;\" data-original-title=\"View\" title=\"View\"></i></a>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "<a href=\"#\" class=\"editDbaProjectBtn\"><i class=\"ui-tooltip fa fa-pencil-square-o\" style=\"font-size: 22px;\" data-original-title=\"Edit\" title=\"Edit\"></i></a>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "<a href=\"#\" class=\"deleteDbaProjectBtn\"><i class=\"ui-tooltip fa fa-trash-o\" style=\"font-size: 22px;\" data-original-title=\"Delete\" title=\"Delete\"></i></a>" + "</td>");

                    htmlProjectList.Append("</tr>");
                }

                htmlProjectList.Append("</tbody>");

                htmlProjectList.Append("</table>");
                htmlProjectList.Append("</div>");
            //}
            //else
            //{
            //    htmlProjectList.Append("<p>" + "There is no data to be displayed." + "</p>");
            //}

            htmlProjectList.Append("</div>");

            projectsList.Text = htmlProjectList.ToString();
        }

        [WebMethod]
        public static string ViewProject(Guid projectGuid)
        {
            ProjectManager projectManagerObj = new ProjectDB().GetProject(projectGuid);

            StringBuilder htmlAssetDetailsBody = new StringBuilder();
            htmlAssetDetailsBody.Append("<div class=\"container-fluid col-md-12\">");
            htmlAssetDetailsBody.Append("<div class=\"row\">");
            htmlAssetDetailsBody.Append("<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">");
            htmlAssetDetailsBody.Append("<div class=\"panel panel-default\">");
            htmlAssetDetailsBody.Append("<div class=\"panel-body\">");
            htmlAssetDetailsBody.Append("<div class=\"text-center\">");

            htmlAssetDetailsBody.Append("<img src=\"Images/view-data.png\" class=\"login\" height=\"70\">");
            //htmlAssetDetailsBody.Append("<h3 class=\"text-center\">" + "Project Code" + ":" + projectManagerObj.ProjectCode + "</h3>");
            htmlAssetDetailsBody.Append("<h3 class=\"text-center\">" + "Project Name" + ":" + projectManagerObj.ProjectName + "</h3>");

            htmlAssetDetailsBody.Append("<div class=\"panel-body\">");
            htmlAssetDetailsBody.Append("<form id=\"viewProjectPopupForm\" name=\"viewProjectPopupForm\" role=\"form\" class=\"form form-horizontal\" method=\"post\">");

            htmlAssetDetailsBody.Append("<fieldset class=\"scheduler-border\">");
            htmlAssetDetailsBody.Append("<legend class=\"scheduler-border\">");
            htmlAssetDetailsBody.Append("<a data-toggle=\"collapse\" href=\"#demo\">1. Project Basic Details</a>");
            htmlAssetDetailsBody.Append("</legend>");
            //htmlAssetDetailsBody.Append("<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12>");
            //htmlAssetDetailsBody.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6>");
            //htmlAssetDetailsBody.Append("</div>");
            //htmlAssetDetailsBody.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6>");
            //htmlAssetDetailsBody.Append("</div>");
            //htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("<div class=\"collapse in\" id=\"demo\">");
            htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

            htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
            htmlAssetDetailsBody.Append("<table id=\"assetDetails\" class=\"table table-no-border\" cellspacing=\"0\" width=\"100%\">");

            htmlAssetDetailsBody.Append("<thead>");
            htmlAssetDetailsBody.Append("</thead>");

            htmlAssetDetailsBody.Append("<tbody>");

            htmlAssetDetailsBody.Append("<tr>");
            //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "Project Name" + "</td>");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left\">" + projectManagerObj.ProjectName + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "SubProject Name" + "</td>");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left\">" + projectManagerObj.SubProjectName + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "District Name" + "</td>");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left\">" + projectManagerObj.DistrictManager.DistrictName + "(" + projectManagerObj.DistrictManager.DistrictCode + ")" + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "Division Name" + "</td>");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left\">" + projectManagerObj.DivisionManager.DivisionName + "(" + projectManagerObj.DivisionManager.DivisionCode + ")" + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "Project Chainage" + "</td>");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left\">" + projectManagerObj.ProjectChainage + "(" + projectManagerObj.DivisionManager.DivisionCode + ")" + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left\">" + "Project Start Date" + "</td>");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left\">" + projectManagerObj.ProjectStartDate + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left\">" + "Project Completion Date(Expected)" + "</td>");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left\">" + projectManagerObj.ProjectExpectedCompletionDate + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left\">" + "Project Value" + "</td>");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left\">" + projectManagerObj.ProjectValue + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("</tbody>");

            htmlAssetDetailsBody.Append("</table>");
            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</div>");
            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</fieldset>");

            //Fremaa Officers

            htmlAssetDetailsBody.Append("<fieldset class=\"scheduler-border\">");
            htmlAssetDetailsBody.Append("<legend class=\"scheduler-border\">");
            htmlAssetDetailsBody.Append("<a data-toggle=\"collapse\" href=\"#fremaaOfficersListDiv\">2. Fremaa Officers List</a>");
            htmlAssetDetailsBody.Append("</legend>");
            
            htmlAssetDetailsBody.Append("<div class=\"collapse in\" id=\"fremaaOfficersListDiv\">");

            htmlAssetDetailsBody.Append("<div class=\"row\">");

            htmlAssetDetailsBody.Append("<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
            foreach (ProjectAndFremaaOfficersRelationManager fremaaOfficersList in projectManagerObj.OfficersEngaged)
            {
                htmlAssetDetailsBody.Append(fremaaOfficersList.FremaaOfficerName);
            }
            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</div>");
            
            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</fieldset>");


            //PMC Officers

            htmlAssetDetailsBody.Append("<fieldset class=\"scheduler-border\">");
            htmlAssetDetailsBody.Append("<legend class=\"scheduler-border\">");
            htmlAssetDetailsBody.Append("<a data-toggle=\"collapse\" href=\"#PMCOfficersListDiv\">3. PMC Officers List</a>");
            htmlAssetDetailsBody.Append("</legend>");

            htmlAssetDetailsBody.Append("<div class=\"collapse in\" id=\"PMCOfficersListDiv\">");

            htmlAssetDetailsBody.Append("<div class=\"row\">");

            htmlAssetDetailsBody.Append("<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
            foreach (ProjectAndPmcSiteEngineersRelationManager PmcOfficersList in projectManagerObj.PmcSiteEngineers)
            {
                htmlAssetDetailsBody.Append(PmcOfficersList.SiteEngineerName);
            }
            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</fieldset>");

            //Subchainges

            htmlAssetDetailsBody.Append("<fieldset class=\"scheduler-border\">");
            htmlAssetDetailsBody.Append("<legend class=\"scheduler-border\">");
            htmlAssetDetailsBody.Append("<a data-toggle=\"collapse\" href=\"#SubchainagesListDiv\">4. Sub-Chainages List</a>");
            htmlAssetDetailsBody.Append("</legend>");

            htmlAssetDetailsBody.Append("<div class=\"collapse in\" id=\"SubchainagesListDiv\">");

            htmlAssetDetailsBody.Append("<div class=\"row\">");

            htmlAssetDetailsBody.Append("<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
            foreach (SubChainageManager SubchainagesList in projectManagerObj.SubChainageManagerList)
            {
                htmlAssetDetailsBody.Append("<fieldset class=\"scheduler-border\">");
                htmlAssetDetailsBody.Append("<legend class=\"scheduler-border\">");
                htmlAssetDetailsBody.Append("<a data-toggle=\"collapse\" href=\"#" + SubchainagesList.SubChainageName + "\">" + SubchainagesList.SubChainageName + "</a>");
                htmlAssetDetailsBody.Append("</legend>");

                htmlAssetDetailsBody.Append("<div class=\"collapse in\" id=\"" + SubchainagesList.SubChainageName + "\">");

                htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

                htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
                htmlAssetDetailsBody.Append("<table id=\"assetDetails\" class=\"table table-no-border\" cellspacing=\"0\" width=\"100%\">");

                htmlAssetDetailsBody.Append("<thead>");
                htmlAssetDetailsBody.Append("</thead>");

                htmlAssetDetailsBody.Append("<tbody>");

                htmlAssetDetailsBody.Append("<tr>");
                //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "SIO Name" + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left\">" + SubchainagesList.UserManager.FirstName + " " +SubchainagesList.UserManager.MiddleName + " "+ SubchainagesList.UserManager.LastName + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "SubChainage Value" + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left\">" + SubchainagesList.SubChainageValue + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "Administrative Approval Reference" + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left\">" + SubchainagesList.AdministrativeApprovalReference + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "Technical Sanction Reference" + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left\">" + SubchainagesList.TechnicalSanctionReference + "(" + projectManagerObj.DistrictManager.DistrictCode + ")" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "Contractor Name" + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left\">" + SubchainagesList.ContractorName + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "Work Order Reference" + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left\">" + SubchainagesList.WorkOrderReference + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left\">" + "SubChainage Start Date" + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left\">" + SubchainagesList.SubChainageStartingDate + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left\">" + "SubChainage Completion Date(Expected)" + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left\">" + SubchainagesList.SubChainageExpectedCompletionDate + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left\">" + "Type Of Works" + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left\">" + SubchainagesList.TypeOfWorks + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");

                htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

                htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
                htmlAssetDetailsBody.Append("<table id=\"workItemsDetails\" class=\"table table-bordered table-striped\" cellspacing=\"0\" width=\"100%\">");

                htmlAssetDetailsBody.Append("<tbody>");

                htmlAssetDetailsBody.Append("<tr colspan=\"3\">WorkItems</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left\">" + "#" + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "WorkItem Name" + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right\">" + "WorkItem Total Value" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                foreach(WorkItemManager workItemList in SubchainagesList.WorkItemManager)
                {
                    int index = SubchainagesList.WorkItemManager.IndexOf(workItemList);

                    htmlAssetDetailsBody.Append("<tr>");
                    //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                    htmlAssetDetailsBody.Append("<td class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left\">" + (++index) + "</td>");
                    htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + workItemList.WorkItemName + "</td>");
                    htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right\">" + Double.Parse(workItemList.WorkItemTotalValue) + "</td>");
                    htmlAssetDetailsBody.Append("</tr>");
                }
                
                htmlAssetDetailsBody.Append("</tbody>");

                htmlAssetDetailsBody.Append("</table>");
                htmlAssetDetailsBody.Append("</div>");

                htmlAssetDetailsBody.Append("</div>");

                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("</tbody>");

                htmlAssetDetailsBody.Append("</table>");
                htmlAssetDetailsBody.Append("</div>");               

                htmlAssetDetailsBody.Append("</fieldset>");
            }
            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</fieldset>");

            //Financial Value Distribution

            htmlAssetDetailsBody.Append("<fieldset class=\"scheduler-border\">");
            htmlAssetDetailsBody.Append("<legend class=\"scheduler-border\">");
            htmlAssetDetailsBody.Append("<a data-toggle=\"collapse\" href=\"#projectValueDistributionDetailsDiv\">5. Project Value Distribution Details</a>");
            htmlAssetDetailsBody.Append("</legend>");
            //htmlAssetDetailsBody.Append("<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12>");
            //htmlAssetDetailsBody.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6>");
            //htmlAssetDetailsBody.Append("</div>");
            //htmlAssetDetailsBody.Append("<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6>");
            //htmlAssetDetailsBody.Append("</div>");
            //htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("<div class=\"collapse in\" id=\"projectValueDistributionDetailsDiv\">");
            htmlAssetDetailsBody.Append("<div id=\"projectValueDistributionDetails-table-container\" style=\"padding:1%;\">");

            htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
            htmlAssetDetailsBody.Append("<table id=\"projectValueDistributionDetailsTable\" class=\"table table-bordered table-striped\" cellspacing=\"0\" width=\"100%\">");

            htmlAssetDetailsBody.Append("<thead>");
            htmlAssetDetailsBody.Append("</thead>");

            htmlAssetDetailsBody.Append("<tbody>");

            htmlAssetDetailsBody.Append("<tr>");
            //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left\">" + "#" + "</td>");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + "Finacial Head" + "</td>");
            htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right\">" + "Value Assigned" + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            foreach (ProjectValueDistributionManager projectValueDistributionManagerObj in projectManagerObj.ProjectValueDistributionManagerList)
            {
                int index = projectManagerObj.ProjectValueDistributionManagerList.IndexOf(projectValueDistributionManagerObj);

                htmlAssetDetailsBody.Append("<tr>");
                //htmlAssetDetailsBody.Append("<tr class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left\">" + (++index) + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left\">" + projectValueDistributionManagerObj.FinancialSubHeadManager.FinancialSubHeadName + "</td>");
                htmlAssetDetailsBody.Append("<td class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right\">" + projectValueDistributionManagerObj.FinancialSubHeadBudget + "</td>");
                htmlAssetDetailsBody.Append("</tr>");
            }

            htmlAssetDetailsBody.Append("</tbody>");

            htmlAssetDetailsBody.Append("</table>");
            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</div>");
            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</fieldset>");










            htmlAssetDetailsBody.Append("</form>");
            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</div>");
            htmlAssetDetailsBody.Append("</div>");
            htmlAssetDetailsBody.Append("</div>");
            htmlAssetDetailsBody.Append("</div>");
            htmlAssetDetailsBody.Append("</div>");
            htmlAssetDetailsBody.Append("</div>");


            return htmlAssetDetailsBody.ToString();

        }
    }
}