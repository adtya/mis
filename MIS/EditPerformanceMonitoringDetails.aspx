﻿<%@ Page Language="C#" MasterPageFile="~/DBA.Master" Title="Edit Performance Montioring Details" AutoEventWireup="true" CodeBehind="EditPerformanceMonitoringDetails.aspx.cs" Inherits="MIS.EditPerformanceMonitoringDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">   
    <link href="CustomStyles/ams.css" rel="stylesheet" type="text/css" />
    <link href="Content/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">    
    <script src="Scripts/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="Scripts/Datejs/build/date.js" type="text/javascript"></script>
    <script src="Scripts/date.format.js" type="text/javascript"></script>

    <script src="CustomScripts/edit-performance-monitoring.js" type="text/javascript"></script> 
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Performance Monitoring Details</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server" id="editPerformanceMonitoringDetailsForm" name="addPerformanceMonitoringDetailsForm">
                            <fieldset>
                                <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12">                                        
                                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                 <label for="assetCode" id="assetCodeLabel" class="control-label">Asset Code</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8"> 
                                                                <asp:Literal ID="ltAssetCode" runat="server"></asp:Literal>
                                                            </div>
                                                         </div>
                                                     </div>                                                    
                                             </div>
                                    </div>
                                                         
                                <div class="panel panel-default">
                                    <div class="panel-body">                                                                   
                                
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12">                                      
                                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                 <label for="assetTypeCode" id="assetTypeCodeLabel" class="control-label">Asset Type</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8"> 
                                                                 <input id="assetTypeCode" name="assetTypeCode" readonly="readonly" class="form-control" />
                                                                <input id="assetTypeGuid" name="assetTypeGuid" style="display:none" class="form-control" />
                                                            </div>
                                                         </div>
                                                   </div>

                                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                                          <div class="form-group">
                                                              <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                 <label for="assetCodeText" id="assetCodeTextLabel" class="control-label">Asset Code</label>
                                                              </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8"> 
                                                                <input id="assetCode" name="assetCode" readonly="readonly" class="form-control" />
                                                            </div>                                                
                                                          </div>
                                                    </div>
                                                </div>
                                            </div>        
                                
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12">                                                
                                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="assetName" id="assetNameLabel" class="control-label">Asset Name</label>
                                                            </div>
                                                           <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8"> 
                                                                <input id="assetName" name="assetName" readonly="readonly" class="form-control" />                                                                                                        
                                                            </div>
                                                         </div>
                                                     </div>

                                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="monitoringDate" id="monitoringDateLabel"class="control-label">Monitoring Date</label>
                                                             </div>
                                                             <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                 <asp:Literal ID="ltMonitoringDate" runat="server"></asp:Literal>                                                        
                                                                     <%--<input type="text" name="monitoringDate" class="form-control" id="monitoringDate" readonly="readonly" value="2012/5/15"/>--%>
                                                                         <%--<span class="input-group-addon form-control-static">--%>
                                                                             <%--<i class="fa fa-calendar fa-fw"></i></span></div>--%>                                                                                                                                                                                    
                                                             </div>
                                                        </div>
                                                   </div>
                                            </div>                          
                                        </div>

                                      </div>
                                </div>

                                <div class="panel panel-default">
                                      <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12">                                                
                                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                             <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <label for="inspectionDate" id="inspectionDateLabel"class="control-label">Inspection Date</label>
                                                                  <input id="performanceMonitoringGuid" name="performanceMonitoringGuid" placeholder="performanceMonitoringGuid" class="form-control" style="display:none"  />
                                                             </div>
                                                             <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                 <div id="dpInspectionDate" class="input-group date">                                                                    
                                                                     <input type="text" name="inspectionDate" class="form-control" id="inspectionDate" placeholder="Inspection Date (dd/mm/yyyy)" />
                                                                     <span class="input-group-addon form-control-static">
                                                                         <i class="fa fa-calendar fa-fw"></i>
                                                                     </span>
                                                                </div>                                                                                                                                                                                         
                                                             </div>
                                                        </div>
                                                    </div>                                   
                                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <label for="inspectorName" id="inspectorNameLabel"class="control-label">Inspector Name</label>
                                                             </div>
                                                             <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">                                                              
                                                                 <input id="inspectorName" name="inspectorName" placeholder="Inspector Name" class="form-control" />
                                                             </div>                                       
                                                       </div>
                                                   </div>
                                            </div>
                                        </div>                                

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12">                                   
                                                
                                                     <div class="col-xs-12 col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                             <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <label for="inspectorPosition" id="inspectorPositionLabel"class="control-label">Inspector Position</label>
                                                             </div>
                                                             <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">                                                                
                                                                 <select class="form-control multiselect" name="inspectorPositionSelect" id="inspectorPositionSelect" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">
                                                                     <option value="">--Select Inspector Position --</option>
                                                                 </select> 
                                                             </div>    
                                                         </div>
                                                    </div>                                                                               
                                                     <div class="col-xs-12 col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <label for="overallStatus" id="overallStatusLabel" class="control-label">Overall Status</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                               <select class="form-control multiselect" name="overallStatusSelect" id="overallStatusSelect" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">
                                                                    <option value="">--Select Overall Status --</option>
                                                               </select>                                                               
                                                            </div>
                                                         </div>
                                                </div>
                                            </div>
                                        </div>                              

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <div class="col-xs-12 col-sm-3 col-md-3">
                                                    <div class="form-group">
                                                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                          <label for="generalComments" id="generalCommentsLabel"class="control-label">General Comments</label>
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-9 col-md-9">
                                                    <div class="form-group">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <textarea id="generalComments" rows="2" cols="6" name="generalComments" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>               
                                        </div>

                                      </div>
                                </div>                                                       

                                <fieldset id="performanceCheckList" class="scheduler-border hidden">
                                    <legend class="scheduler-border"><h4><u>Performance Checklist</u></h4></legend>

                                    <div class="form-group" id="performanceChecklistPanel">                                        
                                    </div>

                                </fieldset>
                                       
                                
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary" aria-hidden="true" id="updatePerformanceDetailsBtn">Update</button>
                                            <button class="btn btn-primary" aria-hidden="true" id="cancelPerformanceDetailsBtn">Cancel</button>
                                        </div>
                                    </div>                     
                                </div>                              
                                        
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>  
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
</asp:Content>
