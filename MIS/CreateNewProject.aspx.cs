﻿using MIS.App_Code;
using NodaMoney;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class CreateNewProject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "DBA")
                {
                    if (!Page.IsPostBack)
                    {
                        DisplayDistrictList();
                        DisplayDivisionList();
                        DisplaySioList();
                        DisplayWorkItemsList();
                        DisplayFinancialSubHeads();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                    //Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("DBALogin.aspx");
            }
        }

        private void DisplayDistrictList()
        {
            List<DistrictManager> districtsList = new List<DistrictManager>();

            districtsList = new DistrictDB().GetDistricts();

            StringBuilder htmlDistrictList = new StringBuilder();

            htmlDistrictList.Append("<select class=\"form-control\" name=\"districtList\" id=\"districtList\">");
            htmlDistrictList.Append("<option value=\"\">" + "  --Select District--  " + "</option>");
            if (districtsList.Count > 0)
            {
                foreach (DistrictManager districtObj in districtsList)
                {
                    htmlDistrictList.Append("<option value=\"" + districtObj.DistrictGuid.ToString() + "\">" + districtObj.DistrictName.ToString() + "</option>");
                }
                htmlDistrictList.Append("</select>");
            }

            ltDistrictList.Text = htmlDistrictList.ToString();
        }

        private void DisplayDivisionList()
        {
            List<DivisionManager> divisionList = new List<DivisionManager>();

            divisionList = new DivisionDB().GetDivisions();

            StringBuilder htmlDivisionList = new StringBuilder();

            htmlDivisionList.Append("<select class=\"form-control\" name=\"divisionList\" id=\"divisionList\">");
            htmlDivisionList.Append("<option value=\"\">" + "  --Select Division--  " + "</option>");
            if (divisionList.Count > 0)
            {
                foreach (DivisionManager divisionObj in divisionList)
                {
                    htmlDivisionList.Append("<option value=\"" + divisionObj.DivisionGuid.ToString() + "\">" + divisionObj.DivisionName.ToString() + "</option>");
                }
            }
            htmlDivisionList.Append("</select>");

            ltDivisionList.Text = htmlDivisionList.ToString();
        }

        private void DisplaySioList()
        {
            List<UserManager> sioUsersList = new List<UserManager>();
            sioUsersList = new UserDB().GetSioUsers();

            StringBuilder htmlSioUsersList = new StringBuilder();

            htmlSioUsersList.Append("<select class=\"form-control\" name=\"sioUsersList\" id=\"sioUsersList\">");
            htmlSioUsersList.Append("<option value=\"\">" + "  --Select SIO--  " + "</option>");
            if (sioUsersList.Count > 0)
            {
                foreach (UserManager sioUsersObj in sioUsersList)
                {
                    htmlSioUsersList.Append("<option value=\"" + sioUsersObj.UserGuid.ToString() + "\">" + sioUsersObj.FirstName.ToString() + "</option>");
                }
            }
            htmlSioUsersList.Append("</select>");

            ltSioUserList.Text = htmlSioUsersList.ToString();
        }

        private void DisplayWorkItemsList()
        {
            List<WorkItemManager> workItemList = new List<WorkItemManager>();
            //workItemList = new WorkItemDB().GetWorkItemNames();

            StringBuilder htmlWorkTypeItemList = new StringBuilder();

            htmlWorkTypeItemList.Append("<select class=\"form-control\" name=\"workTypeItemList\" id=\"workTypeItemList\">");
            htmlWorkTypeItemList.Append("<option value=\"\">" + "  --Select Work Item--  " + "</option>");
            if (workItemList.Count > 0)
            {
                foreach (WorkItemManager workTypeItemObj in workItemList)
                {
                    htmlWorkTypeItemList.Append("<option value=\"" + workTypeItemObj.WorkItemGuid.ToString() + "\">" + workTypeItemObj.WorkItemName.ToString() + "</option>");
                }
            }
            htmlWorkTypeItemList.Append("</select>");

            //ltWorkTypeItems.Text = htmlWorkTypeItemList.ToString();



            //List<Pr> divisionListArray = new List<string>();

            //foreach (DivisionManager divisionData in projectList)
            //{
            //    int index = projectList.IndexOf(divisionData);

            //    divisionListArray.Add(divisionData.DivisionGuid.ToString());
            //    divisionListArray.Add(divisionData.DivisionName.ToString());
            //}

            //return divisionListArray.ToArray();
        }

        private void DisplayFinancialSubHeads()
        {
            List<FinancialSubHeadManager> financialSubHeadsList = new List<FinancialSubHeadManager>();
            financialSubHeadsList = new FinancialSubHeadDB().GetFinancialSubHeads();

            StringBuilder htmlFinancialSubHeadNameList = new StringBuilder();

            htmlFinancialSubHeadNameList.Append("<select class=\"form-control\" name=\"financialSubHeadList\" id=\"financialSubHeadList\">");
            htmlFinancialSubHeadNameList.Append("<option value=\"\">" + "  --Select Financial Head--  " + "</option>");
            if (financialSubHeadsList.Count > 0)
            {
                foreach (FinancialSubHeadManager financialSubHeadObj in financialSubHeadsList)
                {
                    htmlFinancialSubHeadNameList.Append("<option value=\"" + financialSubHeadObj.FinancialSubHeadGuid.ToString() + "\">" + financialSubHeadObj.FinancialSubHeadName.ToString() + "</option>");
                }
            }
            htmlFinancialSubHeadNameList.Append("</select>");

            //ltWorkTypeItems.Text = htmlWorkTypeItemList.ToString();



            //List<Pr> divisionListArray = new List<string>();

            //foreach (DivisionManager divisionData in projectList)
            //{
            //    int index = projectList.IndexOf(divisionData);

            //    divisionListArray.Add(divisionData.DivisionGuid.ToString());
            //    divisionListArray.Add(divisionData.DivisionName.ToString());
            //}

            //return divisionListArray.ToArray();
        }

        //private void CalculateRemainingProjectValue(Guid projectGuid)
        //{
        //    Money remainingValue;

        //    List<ProjectValueDistributionHeadManager> headList = new List<ProjectValueDistributionHeadManager>();
        //    headList = new ProjectValueDistributionHeadDB().GetHeadBudgets(projectGuid);

        //    foreach (ProjectValueDistributionHeadManager headObj in headList)
        //    {
        //        //headObj.HeadBudget
        //    }
        //}

        private DateTime CalculateDateTime(string date)
        {
            char[] delimiter = new char[] { '/' };

            string[] dateParts = date.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

            string day = dateParts[0];
            string month = dateParts[1];
            string year = dateParts[2];

            DateTime dt = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));

            return dt;
        }

        [WebMethod]
        public static bool SaveProject(string districtGuid, string divisionGuid, string projectName, string subProjectName, string chainageOfProject, DateTime dateOfStartOfProject, DateTime expectedDateOfCompletionOfProject, decimal projectValue,
            List<FremaaOfficerProjectRelationshipManager> arrOfficersEngaged, List<PmcSiteEngineerProjectRelationshipManager> arrPmcSiteEngineers,
            List<SubChainageManager> arrSubChainages, string includeLandAcquisition, List<ProjectValueDistributionManager> arrFinancialHeadBudgets)
        {
            Guid projectGuid = Guid.NewGuid();

            int resultProjectBasicDetails = new ProjectDB().SaveProjectBasicDetails(projectGuid, new Guid(districtGuid), new Guid(divisionGuid), projectName, subProjectName, chainageOfProject, dateOfStartOfProject, expectedDateOfCompletionOfProject, projectValue, (int)ProjectStatusEnum.Created);

            if (resultProjectBasicDetails == 1)
            {
                int resultFremaaOfficerProjectRelation;
                int resultPmcSiteEngineerProjectRelation;
                bool subChainageResult = false;
                bool headBudgetResult = false;

                if (arrOfficersEngaged.Count > 0)
                {
                    foreach (FremaaOfficerProjectRelationshipManager fremaaOfficerProjectRelationshipObj in arrOfficersEngaged)
                    {
                        fremaaOfficerProjectRelationshipObj.FremaaOfficerProjectRelationshipGuid = Guid.NewGuid();
                        fremaaOfficerProjectRelationshipObj.ProjectManager.ProjectGuid = projectGuid;
                    }

                    resultFremaaOfficerProjectRelation = new FremaaOfficerProjectRelationshipDB().SaveFremaaOfficerProjectRelationship(arrOfficersEngaged);
                }

                if (arrPmcSiteEngineers.Count > 0)
                {
                    foreach (PmcSiteEngineerProjectRelationshipManager pmcSiteEngineerProjectRelationshipObj in arrPmcSiteEngineers)
                    {
                        pmcSiteEngineerProjectRelationshipObj.PmcSiteEngineerProjectRelationshipGuid = Guid.NewGuid();
                        pmcSiteEngineerProjectRelationshipObj.ProjectManager.ProjectGuid = projectGuid;
                    }

                    resultPmcSiteEngineerProjectRelation = new PmcSiteEngineerProjectRelationshipDB().SavePmcSiteEngineerRelationship(arrPmcSiteEngineers);
                }

                if (arrSubChainages.Count > 0)
                {
                    foreach (SubChainageManager subChainageObj in arrSubChainages)
                    {
                        subChainageObj.SubChainageGuid = Guid.NewGuid();
                        subChainageObj.ProjectManager.ProjectGuid = projectGuid;
                    }

                    subChainageResult = new SubChainageDB().CreateNewSubChainage(arrSubChainages);
                }

                new ProjectDB().SaveLandAcquisitionDetails(projectGuid, BoolParser.GetValue(includeLandAcquisition));

                if (arrFinancialHeadBudgets.Count > 0)
                {
                    foreach (ProjectValueDistributionManager projectValueDistributionObj in arrFinancialHeadBudgets)
                    {
                        projectValueDistributionObj.ProjectValueDistributionGuid = Guid.NewGuid();
                        projectValueDistributionObj.ProjectManager.ProjectGuid = projectGuid;
                    }

                    headBudgetResult = new ProjectValueDistributionDB().SaveProjectValueDistribution(arrFinancialHeadBudgets);
                }

                return true;
            }
            else
            {
                return false;
            }

            //bool result = new ProjectDB().CreateProject(Guid.NewGuid(), new Guid(districtGuid), new Guid(divisionGuid), projectName, subProjectName, chainageOfProject, dateOfStartOfProject, expectedDateOfCompletionOfProject, projectValue, (int)ProjectStatusEnum.Created, arrOfficersEngaged, arrPmcSiteEngineers, arrSubChainages, BoolParser.GetValue(includeLandAcquisition), arrFinancialHeadBudgets);

            //return (resultProjectBasicDetails == 1);
        }

    }
}