﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class EditSIOAsset : System.Web.UI.Page
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "SIO")
                {
                    if (!Page.IsPostBack)
                    {
                        DisplayAssetTypeDetailsList();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                    //Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("DBALogin.aspx");
            }            
        }

        private void DisplayAssetTypeDetailsList()
        {
            AssetManager assetDataObj = new AssetManager();
            assetDataObj = new AssetDB().GetAssetDataByAssetUuid(new Guid(System.Web.HttpContext.Current.Session["sessionAssetGuid"].ToString()), System.Web.HttpContext.Current.Session["sessionAssetTypeCode"].ToString());

            List<AssetManager> keyAssetList = new List<AssetManager>();
            keyAssetList = new AssetDB().GetKeyAssets(new Guid(System.Web.HttpContext.Current.Session["sessionAssetTypeGuid"].ToString()), new Guid(System.Web.HttpContext.Current.Session["sessionDivisionGuid"].ToString()), new Guid(System.Web.HttpContext.Current.Session["sessionSchemeGuid"].ToString()));

            List<AssetManager> appurtenantAssetManagerList = new List<AssetManager>();
            appurtenantAssetManagerList = new AssetDB().GetKeyAssetData(new Guid(System.Web.HttpContext.Current.Session["sessionAssetGuid"].ToString()));

            List<GateManager> gateManagerList = new List<GateManager>();
            gateManagerList = new GateDB().GetGateDataListByAssetUuid(new Guid(System.Web.HttpContext.Current.Session["sessionAssetGuid"].ToString()));

            if (assetDataObj != null)
            {
                ltDivisionCode.Text = assetDataObj.DivisionManager.DivisionCode;

                ltSchemeCode.Text = assetDataObj.SchemeManager.SchemeCode;

                ltAssetTypeCode.Text = assetDataObj.AssetTypeManager.AssetTypeCode;

                ltAssetCode.Text = assetDataObj.AssetCode;

                StringBuilder htmlKeyAssetSelect = new StringBuilder();
                htmlKeyAssetSelect.Append("<option value=\"\">" + "--Select Key Asset--" + "</option>");
                if (keyAssetList.Count > 0)
                {                  
                    foreach (AssetManager keyAssetObj in keyAssetList)
                    {
                        if (keyAssetObj.AssetGuid == assetDataObj.KeyAssetGuid)
                        {
                            htmlKeyAssetSelect.Append("<option selected=\"selected\" value=\"" + keyAssetObj.AssetGuid + "\">" + keyAssetObj.AssetCode + "</option>");
                        }
                        else
                        {
                            htmlKeyAssetSelect.Append("<option value=\"" + keyAssetObj.AssetGuid + "\">" + keyAssetObj.AssetCode + "</option>");
                        }                        
                    }
                }
                ltKeyAsset.Text = htmlKeyAssetSelect.ToString();

                ltDrawingCode.Text = assetDataObj.DrawingCode;

                ltAmtd.Text = assetDataObj.Amtd;

                ltAssetName.Text = assetDataObj.AssetName;

                ltAssetCost.Text = assetDataObj.AssetCost.Amount.ToString();

                ltChainageStart.Text = assetDataObj.StartChainage.ToString();

                ltChainageEnd.Text = assetDataObj.EndChainage.ToString();

                ltEastingUS.Text = assetDataObj.UtmEast1.ToString();

                ltEastingDS.Text = assetDataObj.UtmEast2.ToString();

                ltNorthingUS.Text = assetDataObj.UtmNorth1.ToString();

                ltNorthingDS.Text = assetDataObj.UtmNorth2.ToString();

                ltInitiationDateOfAsset.Text = assetDataObj.InitiationDate.ToString(@"dd\/MM\/yyyy");

                ltCompletionDateOfAsset.Text = assetDataObj.CompletionDate.ToString(@"dd\/MM\/yyyy");

                StringBuilder htmlAssetTypeDetailsBody = new StringBuilder();

                if (assetDataObj.AssetTypeManager.AssetTypeCode == "BRG")
                {
                    List<BridgeTypeManager> bridgeTypeManagerList = new List<BridgeTypeManager>();
                    bridgeTypeManagerList = new BridgeTypeDB().GetBridgeTypeList();

                    ltBridgeGuidHidden.Text = assetDataObj.BridgeManager.BridgeGuid.ToString();

                    StringBuilder htmlBridgeTypeList = new StringBuilder();
                    htmlBridgeTypeList.Append("<option value=\"\">" + "--Select Bridge Type--" + "</option>");
                    if (bridgeTypeManagerList.Count > 0)
                    {
                        foreach (BridgeTypeManager bridgeTypeManagerObj in bridgeTypeManagerList)
                        {
                            if (bridgeTypeManagerObj.BridgeTypeName == assetDataObj.BridgeManager.BridgeTypeManager.BridgeTypeName)
                            {
                                htmlBridgeTypeList.Append("<option selected=\"selected\" value=\"" + bridgeTypeManagerObj.BridgeTypeGuid + "\">" + bridgeTypeManagerObj.BridgeTypeName + "</option>");
                            }
                            else
                            {
                                htmlBridgeTypeList.Append("<option value=\"" + bridgeTypeManagerObj.BridgeTypeGuid + "\">" + bridgeTypeManagerObj.BridgeTypeName + "</option>");
                            }
                        }
                    }
                    ltBridgeTypeList.Text = htmlBridgeTypeList.ToString();

                    ltBridgeLength.Text = assetDataObj.BridgeManager.Length.ToString();

                    ltBridgeNumberOfPiers.Text = assetDataObj.BridgeManager.PierNumber.ToString();

                    ltBridgeRoadWidth.Text = assetDataObj.BridgeManager.RoadWidth.ToString();
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode == "CAN")
                {
                    List<CanalLiningTypeManager> canalLiningTypeManagerList = new List<CanalLiningTypeManager>();
                    canalLiningTypeManagerList = new CanalLiningTypeDB().GetCanalLiningTypeList();

                    ltCanalGuidHidden.Text = assetDataObj.CanalManager.CanalGuid.ToString();

                    ltCanalUsCrestElevation.Text = assetDataObj.CanalManager.UsElevation.ToString();

                    ltCanalDsCrestElevation.Text = assetDataObj.CanalManager.DSElevation.ToString();

                    ltCanalLength.Text = assetDataObj.CanalManager.Length.ToString();

                    ltCanalBedWidth.Text = assetDataObj.CanalManager.BedWidth.ToString();

                    ltCanalSlipeSlope1.Text = assetDataObj.CanalManager.Slope1.ToString();

                    ltCanalSlipeSlope2.Text = assetDataObj.CanalManager.Slope2.ToString();

                    ltCanalAverageDepth.Text = assetDataObj.CanalManager.AverageDepth.ToString();

                    StringBuilder htmlCanalLiningTypeList = new StringBuilder();
                    htmlCanalLiningTypeList.Append("<option value=\"\">" + "--Select Lining Type--" + "</option>");
                    if (canalLiningTypeManagerList.Count > 0)
                    {
                        foreach (CanalLiningTypeManager canalLiningTypeManagerObj in canalLiningTypeManagerList)
                        {
                            if (canalLiningTypeManagerObj.LiningTypeName == assetDataObj.CanalManager.CanalLiningTypeManager.LiningTypeName)
                            {
                                htmlCanalLiningTypeList.Append("<option selected=\"selected\" value=\"" + canalLiningTypeManagerObj.LiningTypeGuid + "\">" + canalLiningTypeManagerObj.LiningTypeName + "</option>");
                            }
                            else
                            {
                                htmlCanalLiningTypeList.Append("<option value=\"" + canalLiningTypeManagerObj.LiningTypeGuid + "\">" + canalLiningTypeManagerObj.LiningTypeName + "</option>");
                            }
                        }
                    }
                    ltCanalLiningTypeList.Text = htmlCanalLiningTypeList.ToString();

                    StringBuilder htmlAppurtenantAssetsTable = new StringBuilder();
                    if (appurtenantAssetManagerList.Count > 0)
                    {
                        for (var i = 0; i < appurtenantAssetManagerList.Count; i++)
                        {
                            htmlAppurtenantAssetsTable.Append("<tr>");
                            htmlAppurtenantAssetsTable.Append("<td>" + i + 1 + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].AssetGuid + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].AssetCode + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].AssetName + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].InitiationDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].CompletionDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].AssetCost.Amount + "</td>");

                            htmlAppurtenantAssetsTable.Append("</tr>");
                        }
                    }
                    ltCanalAppurtenantAssetsTable.Text = htmlAppurtenantAssetsTable.ToString();
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode == "CUL")
                {
                    List<CulvertTypeManager> culvertTypeManagerList = new List<CulvertTypeManager>();
                    culvertTypeManagerList = new CulvertTypeDB().GetCulvertTypeList();

                    ltCulvertGuidHidden.Text = assetDataObj.CulvertManager.CulvertGuid.ToString();

                    StringBuilder htmlCulvertTypeList = new StringBuilder();
                    if (culvertTypeManagerList.Count > 0)
                    {
                        foreach (var culvertTypeManagerObj in culvertTypeManagerList)
                        {
                            if (culvertTypeManagerObj.CulvertTypeName == assetDataObj.CulvertManager.CulvertTypeManager.CulvertTypeName)
                            {
                                htmlCulvertTypeList.Append("<option selected=\"selected\" value=\"" + culvertTypeManagerObj.CulvertTypeGuid + "\">" + culvertTypeManagerObj.CulvertTypeName + "</option>");
                            }
                            else
                            {
                                htmlCulvertTypeList.Append("<option value=\"" + culvertTypeManagerObj.CulvertTypeGuid + "\">" + culvertTypeManagerObj.CulvertTypeName + "</option>");
                            }
                        }
                    }
                    ltCulvertTypeList.Text = htmlCulvertTypeList.ToString();

                    ltCulvertNumberOfVents.Text = assetDataObj.CulvertManager.VentNumber.ToString();

                    ltCulvertVentHeight.Text = assetDataObj.CulvertManager.VentHeight.ToString();

                    ltCulvertVentWidth.Text = assetDataObj.CulvertManager.VentWidth.ToString();

                    ltCulvertLength.Text = assetDataObj.CulvertManager.LengthCulvert.ToString();
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode == "DRN")
                {
                    ltDrainageGuidHidden.Text = assetDataObj.DrainageManager.DrainageGuid.ToString();

                    ltDrainageUsBedElevation.Text = assetDataObj.DrainageManager.UsElevationDrainage.ToString();

                    ltDrainageDsBedElevation.Text = assetDataObj.DrainageManager.DsElevationDrainage.ToString();

                    ltDrainageLength.Text = assetDataObj.DrainageManager.LengthDrainage.ToString();

                    ltDrainageBedWidth.Text = assetDataObj.DrainageManager.BedWidthDrainage.ToString();

                    ltDrainageSlope1.Text = assetDataObj.DrainageManager.Slope1Drainage.ToString();

                    ltDrainageSlope2.Text = assetDataObj.DrainageManager.Slope2Drainage.ToString();

                    ltDrainageAverageDepth.Text = assetDataObj.DrainageManager.AverageDepthDrainage.ToString();

                    StringBuilder htmlAppurtenantAssetsTable = new StringBuilder();
                    if (appurtenantAssetManagerList.Count > 0)
                    {
                        for (var i = 0; i < appurtenantAssetManagerList.Count; i++)
                        {
                            htmlAppurtenantAssetsTable.Append("<tr>");
                            htmlAppurtenantAssetsTable.Append("<td>" + i + 1 + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].AssetGuid + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].AssetCode + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].AssetName + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].InitiationDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].CompletionDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].AssetCost.Amount + "</td>");

                            htmlAppurtenantAssetsTable.Append("</tr>");
                        }
                    }
                    ltDrainageAppurtenantAssetsTable.Text = htmlAppurtenantAssetsTable.ToString();
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode == "DRP")
                {
                    ltDropStructureGuidHidden.Text = assetDataObj.DropStructureManager.DropStructureGuid.ToString();

                    ltDropStructureUsInvertLevel.Text = assetDataObj.DropStructureManager.UsInvertLevel.ToString();

                    ltDropStructureDsInvertLevel.Text = assetDataObj.DropStructureManager.DsInvertLevel.ToString();

                    ltDropStructureStillingBasinLength.Text = assetDataObj.DropStructureManager.StillingBasinLength.ToString();

                    ltDropStructureStillingBasinWidth.Text = assetDataObj.DropStructureManager.StillingBasinWidth.ToString();
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode == "EMB")
                {
                    List<EmbankmentCrestTypeManager> embankmentCrestTypeManagerList = new List<EmbankmentCrestTypeManager>();
                    embankmentCrestTypeManagerList = new EmbankmentCrestTypeDB().GetEmbankmentCrestTypeList();

                    List<EmbankmentFillTypeManager> embankmentFillTypeManagerList = new List<EmbankmentFillTypeManager>();
                    embankmentFillTypeManagerList = new EmbankmentFillTypeDB().GetEmbankmentFillTypeList();

                    ltEmbankmentGuidHidden.Text = assetDataObj.EmbankmentManager.EmbankmentGuid.ToString();

                    ltEmbankmentUsCrestElevation.Text = assetDataObj.EmbankmentManager.UsElevation.ToString();

                    ltEmbankmentDsCrestElevation.Text = assetDataObj.EmbankmentManager.DsElevation.ToString();

                    ltEmbankmentLength.Text = assetDataObj.EmbankmentManager.Length.ToString();

                    ltEmbankmentCrestWidth.Text = assetDataObj.EmbankmentManager.CrestWidth.ToString();

                    ltEmbankmentCsSlope1.Text = assetDataObj.EmbankmentManager.CsSlope1.ToString();

                    ltEmbankmentCsSlope2.Text = assetDataObj.EmbankmentManager.CsSlope2.ToString();

                    ltEmbankmentRsSlope1.Text = assetDataObj.EmbankmentManager.RsSlope1.ToString();

                    ltEmbankmentRsSlope2.Text = assetDataObj.EmbankmentManager.RsSlope2.ToString();

                    ltEmbankmentBermSlope1.Text = assetDataObj.EmbankmentManager.BermSlope1.ToString();

                    ltEmbankmentBermSlope2.Text = assetDataObj.EmbankmentManager.BermSlope2.ToString();

                    ltEmbankmentPlatformWidth.Text = assetDataObj.EmbankmentManager.PlatformWidth.ToString();

                    StringBuilder htmlEmbankmentCrestTypeManagerList = new StringBuilder();
                    if (embankmentCrestTypeManagerList.Count > 0)
                    {
                        foreach (EmbankmentCrestTypeManager embankmentCrestTypeManagerObj in embankmentCrestTypeManagerList)
                        {
                            if (embankmentCrestTypeManagerObj.CrestTypeName == assetDataObj.EmbankmentManager.EmbankmentCrestTypeManager.CrestTypeName)
                            {
                                htmlEmbankmentCrestTypeManagerList.Append("<option selected=\"selected\" value=\"" + embankmentCrestTypeManagerObj.CrestTypeGuid + "\">" + embankmentCrestTypeManagerObj.CrestTypeName + "</option>");
                            }
                            else
                            {
                                htmlEmbankmentCrestTypeManagerList.Append("<option value=\"" + embankmentCrestTypeManagerObj.CrestTypeGuid + "\">" + embankmentCrestTypeManagerObj.CrestTypeName + "</option>");
                            }
                        }
                    }
                    ltEmbankmentCrestTypeList.Text = htmlEmbankmentCrestTypeManagerList.ToString();

                    StringBuilder htmlEmbankmentFillTypeManagerList = new StringBuilder();
                    if (embankmentFillTypeManagerList.Count > 0)
                    {
                        foreach (EmbankmentFillTypeManager embankmentFillTypeManagerObj in embankmentFillTypeManagerList)
                        {
                            if (embankmentFillTypeManagerObj.FillTypeName == assetDataObj.EmbankmentManager.EmbankmentFillTypeManager.FillTypeName)
                            {
                                htmlEmbankmentFillTypeManagerList.Append("<option selected=\"selected\" value=\"" + embankmentFillTypeManagerObj.FillTypeGuid + "\">" + embankmentFillTypeManagerObj.FillTypeName + "</option>");
                            }
                            else
                            {
                                htmlEmbankmentFillTypeManagerList.Append("<option value=\"" + embankmentFillTypeManagerObj.FillTypeGuid + "\">" + embankmentFillTypeManagerObj.FillTypeName + "</option>");
                            }
                        }
                    }
                    ltEmbankmentFillTypeList.Text = htmlEmbankmentFillTypeManagerList.ToString();

                    ltEmbankmentAverageHeight.Text = assetDataObj.EmbankmentManager.AverageHeight.ToString();

                    StringBuilder htmlAppurtenantAssetsTable = new StringBuilder();
                    if (appurtenantAssetManagerList.Count > 0)
                    {
                        for (var i = 0; i < appurtenantAssetManagerList.Count; i++)
                        {
                            htmlAppurtenantAssetsTable.Append("<tr>");
                            htmlAppurtenantAssetsTable.Append("<td>" + i + 1 + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].AssetGuid + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].AssetCode + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].AssetName + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].InitiationDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].CompletionDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                            htmlAppurtenantAssetsTable.Append("<td>" + appurtenantAssetManagerList[i].AssetCost.Amount + "</td>");

                            htmlAppurtenantAssetsTable.Append("</tr>");
                        }
                    }
                    ltEmbankmentAppurtenantAssetsTable.Text = htmlAppurtenantAssetsTable.ToString();
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode == "GAU")
                {
                    List<GaugeTypeManager> gaugeTypeManagerList = new List<GaugeTypeManager>();
                    gaugeTypeManagerList = new GaugeTypeDB().GetGaugeTypeList();

                    List<GaugeFrequencyTypeManager> gaugeFrequencyTypeManagerList = new List<GaugeFrequencyTypeManager>();
                    gaugeFrequencyTypeManagerList = new GaugeFrequencyTypeDB().GetGaugeFrequencyTypeList();

                    List<GaugeSeasonTypeManager> gaugeSeasonTypeManagerList = new List<GaugeSeasonTypeManager>();
                    gaugeSeasonTypeManagerList = new GaugeSeasonTypeDB().GetGaugeSeasonTypeList();

                    ltGaugeGuidHidden.Text = assetDataObj.GaugeManager.GaugeGuid.ToString();

                    StringBuilder htmlGaugeTypeManagerList = new StringBuilder();
                    if (gaugeTypeManagerList.Count > 0)
                    {
                        foreach (GaugeTypeManager gaugeTypeManagerObj in gaugeTypeManagerList)
                        {
                            if (gaugeTypeManagerObj.GaugeTypeName == assetDataObj.GaugeManager.GaugeTypeManager.GaugeTypeName)
                            {
                                htmlGaugeTypeManagerList.Append("<option selected=\"selected\" value=\"" + gaugeTypeManagerObj.GaugeTypeGuid + "\">" + gaugeTypeManagerObj.GaugeTypeName + "</option>");
                            }
                            else
                            {
                                htmlGaugeTypeManagerList.Append("<option value=\"" + gaugeTypeManagerObj.GaugeTypeGuid + "\">" + gaugeTypeManagerObj.GaugeTypeName + "</option>");
                            }
                        }
                    }
                    ltGaugeTypeList.Text = htmlGaugeTypeManagerList.ToString();

                    StringBuilder htmlGaugeSeasonTypeManagerList = new StringBuilder();
                    if (gaugeSeasonTypeManagerList.Count > 0)
                    {
                        foreach (GaugeSeasonTypeManager gaugeSeasonTypeManagerObj in gaugeSeasonTypeManagerList)
                        {
                            if (gaugeSeasonTypeManagerObj.SeasonTypeName == assetDataObj.GaugeManager.GaugeSeasonTypeManager.SeasonTypeName)
                            {
                                htmlGaugeSeasonTypeManagerList.Append("<option selected=\"selected\" value=\"" + gaugeSeasonTypeManagerObj.SeasonTypeGuid + "\">" + gaugeSeasonTypeManagerObj.SeasonTypeName + "</option>");
                            }
                            else
                            {
                                htmlGaugeSeasonTypeManagerList.Append("<option value=\"" + gaugeSeasonTypeManagerObj.SeasonTypeGuid + "\">" + gaugeSeasonTypeManagerObj.SeasonTypeName + "</option>");
                            }

                        }
                    }
                    ltGaugeSeasonTypeList.Text = htmlGaugeSeasonTypeManagerList.ToString();

                    StringBuilder htmlGaugeFrequencyTypeManagerList = new StringBuilder();
                    if (gaugeFrequencyTypeManagerList.Count > 0)
                    {
                        foreach (GaugeFrequencyTypeManager gaugeFrequencyTypeManagerObj in gaugeFrequencyTypeManagerList)
                        {
                            if (gaugeFrequencyTypeManagerObj.FrequencyTypeName == assetDataObj.GaugeManager.GaugeFrequencyTypeManager.FrequencyTypeName)
                            {
                                htmlGaugeFrequencyTypeManagerList.Append("<option selected=\"selected\" value=\"" + gaugeFrequencyTypeManagerObj.FrequencyTypeGuid + "\">" + gaugeFrequencyTypeManagerObj.FrequencyTypeName + "</option>");
                            }
                            else
                            {
                                htmlGaugeFrequencyTypeManagerList.Append("<option value=\"" + gaugeFrequencyTypeManagerObj.FrequencyTypeGuid + "\">" + gaugeFrequencyTypeManagerObj.FrequencyTypeName + "</option>");
                            }
                        }
                    }
                    ltGaugeFrequencyTypeList.Text = htmlGaugeFrequencyTypeManagerList.ToString();

                    ltGaugeZeroDatum.Text = assetDataObj.GaugeManager.ZeroDatum.ToString();

                    if (assetDataObj.GaugeManager.StartDate.ToString(@"dd\/MM\/yyyy") == "01/01/0001")
                    {
                        ltGaugeStartDate.Text = "";
                    }
                    else
                    {
                        ltGaugeStartDate.Text = assetDataObj.GaugeManager.StartDate.ToString(@"dd\/MM\/yyyy");
                    }

                    if (assetDataObj.GaugeManager.EndDate.ToString(@"dd\/MM\/yyyy") == "01/01/0001")
                    {
                        ltGaugeEndDate.Text = "";
                    }
                    else
                    {
                        ltGaugeEndDate.Text = assetDataObj.GaugeManager.EndDate.ToString(@"dd\/MM\/yyyy");
                    }

                    if (assetDataObj.GaugeManager.Active == true)
                    {
                        ltGaugeActive.Text = "checked=\"true\"";
                    }
                    else
                    {
                        ltGaugeActive.Text = "";
                    }

                    if (assetDataObj.GaugeManager.Lwl == true)
                    {
                        ltGaugeLwlPresent.Text = "checked=\"true\"";
                    }
                    else
                    {
                        ltGaugeLwlPresent.Text = "";
                    }

                    if (assetDataObj.GaugeManager.Hwl == true)
                    {
                        ltGaugeHwlPresent.Text = "checked=\"true\"";
                    }
                    else
                    {
                        ltGaugeHwlPresent.Text = "";
                    }

                    if (assetDataObj.GaugeManager.LevelGeo == true)
                    {
                        ltGaugeLevelGeo.Text = "checked=\"true\"";
                    }
                    else
                    {
                        ltGaugeLevelGeo.Text = "";
                    }
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode == "POR")
                {
                    List<PorcupineTypeManager> porcupineTypeManagerList = new List<PorcupineTypeManager>();
                    porcupineTypeManagerList = new PorcupineTypeDB().GetPorcupineTypeList();

                    List<PorcupineMaterialTypeManager> porcupineMaterialTypeManagerList = new List<PorcupineMaterialTypeManager>();
                    porcupineMaterialTypeManagerList = new PorcupineMaterialTypeDB().GetPorMaterialTypeList();

                    ltPorcupineGuidHidden.Text = assetDataObj.PorcupineManager.PorcupineGuid.ToString();

                    StringBuilder htmlPorcupineTypeManagerList = new StringBuilder();
                    if (porcupineTypeManagerList.Count > 0)
                    {
                        foreach (PorcupineTypeManager porcupineTypeManagerObj in porcupineTypeManagerList)
                        {
                            if (porcupineTypeManagerObj.PorcupineTypeName == assetDataObj.PorcupineManager.PorcupineTypeManager.PorcupineTypeName)
                            {
                                htmlPorcupineTypeManagerList.Append("<option selected=\"selected\" value=\"" + porcupineTypeManagerObj.PorcupineTypeGuid + "\">" + porcupineTypeManagerObj.PorcupineTypeName + "</option>");
                            }
                            else
                            {
                                htmlPorcupineTypeManagerList.Append("<option value=\"" + porcupineTypeManagerObj.PorcupineTypeGuid + "\">" + porcupineTypeManagerObj.PorcupineTypeName + "</option>");
                            }
                        }
                    }
                    ltPorcupineTypeList.Text = htmlPorcupineTypeManagerList.ToString();

                    StringBuilder htmlPorcupineMaterialTypeManagerList = new StringBuilder();
                    if (porcupineMaterialTypeManagerList.Count > 0)
                    {
                        foreach (PorcupineMaterialTypeManager porcupineMaterialTypeManagerObj in porcupineMaterialTypeManagerList)
                        {
                            if (porcupineMaterialTypeManagerObj.PorcupineMaterialTypeName == assetDataObj.PorcupineManager.PorcupineMaterialTypeManager.PorcupineMaterialTypeName)
                            {
                                htmlPorcupineMaterialTypeManagerList.Append("<option selected=\"selected\" value=\"" + porcupineMaterialTypeManagerObj.PorcupineMaterialTypeGuid + "\">" + porcupineMaterialTypeManagerObj.PorcupineMaterialTypeName + "</option>");
                            }
                            else
                            {
                                htmlPorcupineMaterialTypeManagerList.Append("<option value=\"" + porcupineMaterialTypeManagerObj.PorcupineMaterialTypeGuid + "\">" + porcupineMaterialTypeManagerObj.PorcupineMaterialTypeName + "</option>");
                            }
                        }
                    }
                    ltPorcupineMaterialTypeList.Text = htmlPorcupineMaterialTypeManagerList.ToString();

                    ltPorcupineScreenLength.Text = assetDataObj.PorcupineManager.ScreenLength.ToString();

                    ltPorcupineSpacingAlongScreen.Text = assetDataObj.PorcupineManager.SpacingAlongScreen.ToString();

                    ltPorcupineScreenRows.Text = assetDataObj.PorcupineManager.ScreenRows.ToString();

                    ltPorcupineNumberOfLayers.Text = assetDataObj.PorcupineManager.NumberOfLayers.ToString();

                    ltPorcupineMamberLength.Text = assetDataObj.PorcupineManager.MemberLength.ToString();

                    ltPorcupineLength.Text = assetDataObj.PorcupineManager.LengthPorcupine.ToString();
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode == "REG")
                {
                    List<RegulatorTypeManager> regulatorTypeManagerList = new List<RegulatorTypeManager>();
                    regulatorTypeManagerList = new RegulatorTypeDB().GetRegulatorTypeList();

                    List<RegulatorGateTypeManager> regulatorGateTypeManagerList = new List<RegulatorGateTypeManager>();
                    regulatorGateTypeManagerList = new RegulatorGateTypeDB().GetRegGateTypeList();

                    ltRegulatorGuidHidden.Text = assetDataObj.RegulatorManager.RegulatorGuid.ToString();

                    ltRegulatorNumberOfVents.Text = assetDataObj.RegulatorManager.VentNumberRegulator.ToString();

                    ltRegulatorVentHeight.Text = assetDataObj.RegulatorManager.VentHeightRegulator.ToString();

                    ltRegulatorVentWidth.Text = assetDataObj.RegulatorManager.VentWidthRegulator.ToString();

                    StringBuilder htmlRegulatorTypeManagerList = new StringBuilder();
                    if (regulatorTypeManagerList.Count > 0)
                    {
                        foreach (RegulatorTypeManager regulatorTypeManagerObj in regulatorTypeManagerList)
                        {
                            if (regulatorTypeManagerObj.RegulatorTypeName == assetDataObj.RegulatorManager.RegulatorTypeManager.RegulatorTypeName)
                            {
                                htmlRegulatorTypeManagerList.Append("<option selected=\"selected\" value=\"" + regulatorTypeManagerObj.RegulatorTypeGuid + "\">" + regulatorTypeManagerObj.RegulatorTypeName + "</option>");
                            }
                            else
                            {
                                htmlRegulatorTypeManagerList.Append("<option value=\"" + regulatorTypeManagerObj.RegulatorTypeGuid + "\">" + regulatorTypeManagerObj.RegulatorTypeName + "</option>");
                            }
                        }
                    }
                    ltRegulatorTypeList.Text = htmlRegulatorTypeManagerList.ToString();

                    StringBuilder htmlRegulatorGateTypeManagerList = new StringBuilder();
                    if (regulatorGateTypeManagerList.Count > 0)
                    {
                        foreach (RegulatorGateTypeManager regulatorGateTypeManagerObj in regulatorGateTypeManagerList)
                        {
                            if (regulatorGateTypeManagerObj.RegulatorGateTypeName == assetDataObj.RegulatorManager.RegulatorGateTypeManager.RegulatorGateTypeName)
                            {
                                htmlRegulatorGateTypeManagerList.Append("<option selected=\"selected\" value=\"" + regulatorGateTypeManagerObj.RegulatorGateTypeGuid + "\">" + regulatorGateTypeManagerObj.RegulatorGateTypeName + "</option>");
                            }
                            else
                            {
                                htmlRegulatorGateTypeManagerList.Append("<option value=\"" + regulatorGateTypeManagerObj.RegulatorGateTypeGuid + "\">" + regulatorGateTypeManagerObj.RegulatorGateTypeName + "</option>");
                            }

                        }
                    }
                    ltRegulatorGateTypeList.Text = htmlRegulatorGateTypeManagerList.ToString();

                    StringBuilder htmlRegulatorGatesTableBody = new StringBuilder();
                    if (gateManagerList.Count > 0)
                    {
                        for (var i = 0; i < gateManagerList.Count; i++)
                        {
                            htmlRegulatorGatesTableBody.Append("<tr>");

                            htmlRegulatorGatesTableBody.Append("<td>" + i + "</td>");
                            htmlRegulatorGatesTableBody.Append("<td>" + gateManagerList[i].GateGuid + "</td>");
                            htmlRegulatorGatesTableBody.Append("<td>" + gateManagerList[i].GateTypeManager.GateTypeGuid + "</td>");
                            htmlRegulatorGatesTableBody.Append("<td>" + gateManagerList[i].GateConstructionMaterialManager.GateConstructionMaterialGuid + "</td>");
                            htmlRegulatorGatesTableBody.Append("<td>" + gateManagerList[i].GateTypeManager.GateTypeName + "</td>");
                            htmlRegulatorGatesTableBody.Append("<td>" + gateManagerList[i].GateConstructionMaterialManager.GateConstructionMaterialName + "</td>");
                            htmlRegulatorGatesTableBody.Append("<td>" + gateManagerList[i].NumberOfGates + "</td>");
                            htmlRegulatorGatesTableBody.Append("<td>" + gateManagerList[i].Height + "</td>");
                            htmlRegulatorGatesTableBody.Append("<td>" + gateManagerList[i].Width + "</td>");
                            htmlRegulatorGatesTableBody.Append("<td>" + gateManagerList[i].Diameter + "</td>");
                            htmlRegulatorGatesTableBody.Append("<td>" + gateManagerList[i].InstallDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                            htmlRegulatorGatesTableBody.Append("<td>" + "<a href=\"#\" class=\"editRegulatorGateBtn\"><i class=\"ui-tooltip fa fa-pencil-square-o\" style=\"font-size: 22px;\" data-original-title=\"Edit\" title=\"Edit\"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"#\" class=\"deleteRegulatorGateBtn\"><i class=\"ui-tooltip fa fa-trash-o\" style=\"font-size: 22px;\" data-original-title=\"Delete\" title=\"Delete\"></i></a>" + "</td>");

                            htmlRegulatorGatesTableBody.Append("</tr>");
                        }
                    }
                    ltRegulatorGatesTable.Text = htmlRegulatorGatesTableBody.ToString();
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode == "REV")
                {
                    List<RevetmentTypeManager> revetmentTypeManagerList = new List<RevetmentTypeManager>();
                    revetmentTypeManagerList = new RevetmentTypeDB().GetRevetmentTypeList();

                    List<RevetmentRiverprotTypeManager> revetmentRiverprotTypeManagerList = new List<RevetmentRiverprotTypeManager>();
                    revetmentRiverprotTypeManagerList = new RevetmentRiverprotTypeDB().GetRevRiverprotTypeList();

                    List<RevetmentWaveprotTypeManager> revetmentWaveprotTypeManagerList = new List<RevetmentWaveprotTypeManager>();
                    revetmentWaveprotTypeManagerList = new RevetmentWaveprotTypeDB().GetRevWaveprotTypeList();

                    ltRevetmentGuidHidden.Text = assetDataObj.RevetmentManager.RevetmentGuid.ToString();

                    StringBuilder htmlRevetmentTypeManagerList = new StringBuilder();
                    if (revetmentTypeManagerList.Count > 0)
                    {
                        foreach (RevetmentTypeManager revetmentTypeManagerObj in revetmentTypeManagerList)
                        {
                            if (revetmentTypeManagerObj.RevetTypeName == assetDataObj.RevetmentManager.RevetmentTypeManager.RevetTypeName)
                            {
                                htmlRevetmentTypeManagerList.Append("<option selected=\"selected\" value=\"" + revetmentTypeManagerObj.RevetTypeGuid + "\">" + revetmentTypeManagerObj.RevetTypeName + "</option>");
                            }
                            else
                            {
                                htmlRevetmentTypeManagerList.Append("<option value=\"" + revetmentTypeManagerObj.RevetTypeGuid + "\">" + revetmentTypeManagerObj.RevetTypeName + "</option>");
                            }
                        }
                    }
                    ltRevetmentTypeList.Text = htmlRevetmentTypeManagerList.ToString();

                    StringBuilder htmlRevetmentRiverprotTypeManagerList = new StringBuilder();
                    if (revetmentRiverprotTypeManagerList.Count > 0)
                    {
                        foreach (var riverProtectionType in revetmentRiverprotTypeManagerList)
                        {
                            if (riverProtectionType.RiverprotTypeName == assetDataObj.RevetmentManager.RevetmentRiverprotTypeManager.RiverprotTypeName)
                            {
                                htmlRevetmentRiverprotTypeManagerList.Append("<option selected=\"selected\" value=\"" + riverProtectionType.RiverprotTypeGuid + "\">" + riverProtectionType.RiverprotTypeName + "</option>");
                            }
                            else
                            {
                                htmlRevetmentRiverprotTypeManagerList.Append("<option value=\"" + riverProtectionType.RiverprotTypeGuid + "\">" + riverProtectionType.RiverprotTypeName + "</option>");
                            }
                        }
                    }
                    ltRevetmentRiverProtectionTypeList.Text = htmlRevetmentRiverprotTypeManagerList.ToString();

                    StringBuilder htmlRevetmentWaveprotTypeManagerList = new StringBuilder();
                    if (revetmentWaveprotTypeManagerList.Count > 0)
                    {
                        foreach (var waveProtectionType in revetmentWaveprotTypeManagerList)
                        {
                            if (waveProtectionType.WaveprotTypeName == assetDataObj.RevetmentManager.RevetmentWaveprotTypeManager.WaveprotTypeName)
                            {
                                htmlRevetmentWaveprotTypeManagerList.Append("<option selected=\"selected\" value=\"" + waveProtectionType.WaveprotTypeGuid + "\">" + waveProtectionType.WaveprotTypeName + "</option>");
                            }
                            else
                            {
                                htmlRevetmentWaveprotTypeManagerList.Append("<option value=\"" + waveProtectionType.WaveprotTypeGuid + "\">" + waveProtectionType.WaveprotTypeName + "</option>");
                            }
                        }
                    }
                    ltRevetmentWaveProtectionTypeList.Text = htmlRevetmentWaveprotTypeManagerList.ToString();

                    ltRevetmentSlope.Text = assetDataObj.RevetmentManager.SlopeRevetment.ToString();

                    ltRevetmentLength.Text = assetDataObj.RevetmentManager.LengthRevetment.ToString();

                    ltRevetmentPlainWidth.Text = assetDataObj.RevetmentManager.PlainWidth.ToString();
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode == "SLU")
                {
                    List<SluiceTypeManager> sluiceTypeManagerList = new List<SluiceTypeManager>();
                    sluiceTypeManagerList = new SluiceTypeDB().GetSluiceTypeList();

                    ltSluiceGuidHidden.Text = assetDataObj.SluiceManager.SluiceGuid.ToString();

                    StringBuilder htmlSluiceTypeManagerList = new StringBuilder();
                    if (sluiceTypeManagerList.Count > 0)
                    {
                        foreach (SluiceTypeManager sluiceTypeManagerObj in sluiceTypeManagerList)
                        {
                            if (sluiceTypeManagerObj.SluiceTypeName == assetDataObj.SluiceManager.SluiceTypeManager.SluiceTypeName)
                            {
                                htmlSluiceTypeManagerList.Append("<option selected=\"selected\" value=\"" + sluiceTypeManagerObj.SluiceTypeGuid + "\">" + sluiceTypeManagerObj.SluiceTypeName + "</option>");
                            }
                            else
                            {
                                htmlSluiceTypeManagerList.Append("<option value=\"" + sluiceTypeManagerObj.SluiceTypeGuid + "\">" + sluiceTypeManagerObj.SluiceTypeName + "</option>");
                            }

                        }
                    }
                    ltSluiceTypeList.Text = htmlSluiceTypeManagerList.ToString();

                    ltSluiceNumberOfVents.Text = assetDataObj.SluiceManager.VentNumberSluice.ToString();

                    ltSluiceVentDiameter.Text = assetDataObj.SluiceManager.VentDiameterSluice.ToString();

                    ltSluiceVentWidth.Text = assetDataObj.SluiceManager.VentWidthSluice.ToString();

                    ltSluiceVentHeight.Text = assetDataObj.SluiceManager.VentHeightSluice.ToString();

                    StringBuilder htmlSluiceGatesTableBody = new StringBuilder();
                    if (gateManagerList.Count > 0)
                    {
                        for (var i = 0; i < gateManagerList.Count; i++)
                        {
                            htmlSluiceGatesTableBody.Append("<tr>");

                            htmlSluiceGatesTableBody.Append("<td>" + i + "</td>");
                            htmlSluiceGatesTableBody.Append("<td>" + gateManagerList[i].GateGuid + "</td>");
                            htmlSluiceGatesTableBody.Append("<td>" + gateManagerList[i].GateTypeManager.GateTypeGuid + "</td>");
                            htmlSluiceGatesTableBody.Append("<td>" + gateManagerList[i].GateConstructionMaterialManager.GateConstructionMaterialGuid + "</td>");
                            htmlSluiceGatesTableBody.Append("<td>" + gateManagerList[i].GateTypeManager.GateTypeName + "</td>");
                            htmlSluiceGatesTableBody.Append("<td>" + gateManagerList[i].GateConstructionMaterialManager.GateConstructionMaterialName + "</td>");
                            htmlSluiceGatesTableBody.Append("<td>" + gateManagerList[i].NumberOfGates + "</td>");
                            htmlSluiceGatesTableBody.Append("<td>" + gateManagerList[i].Height + "</td>");
                            htmlSluiceGatesTableBody.Append("<td>" + gateManagerList[i].Width + "</td>");
                            htmlSluiceGatesTableBody.Append("<td>" + gateManagerList[i].Diameter + "</td>");
                            htmlSluiceGatesTableBody.Append("<td>" + gateManagerList[i].InstallDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                            htmlSluiceGatesTableBody.Append("<td>" + "<a href=\"#\" class=\"editSluiceGateBtn\"><i class=\"ui-tooltip fa fa-pencil-square-o\" style=\"font-size: 22px;\" data-original-title=\"Edit\" title=\"Edit\"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"#\" class=\"deleteSluiceGateBtn\"><i class=\"ui-tooltip fa fa-trash-o\" style=\"font-size: 22px;\" data-original-title=\"Delete\" title=\"Delete\"></i></a>" + "</td>");

                            htmlSluiceGatesTableBody.Append("</tr>");
                        }
                    }
                    ltSluiceGatesTableBody.Text = htmlSluiceGatesTableBody.ToString();
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode == "SPU")
                {
                    List<SpurConstructionTypeManager> spurConstructionTypeManagerList = new List<SpurConstructionTypeManager>();
                    spurConstructionTypeManagerList = new SpurConstructionTypeDB().GetSpurConstructionTypeList();

                    List<SpurRevetTypeManager> spurRevetTypeManagerList = new List<SpurRevetTypeManager>();
                    spurRevetTypeManagerList = new SpurRevetTypeDB().GetSpurRevetTypeList();

                    List<SpurShapeTypeManager> spurShapeTypeManagerList = new List<SpurShapeTypeManager>();
                    spurShapeTypeManagerList = new SpurShapeTypeDB().GetSpurShapeTypeList();

                    ltSpurGuidHidden.Text = assetDataObj.SpurManager.SpurGuid.ToString();

                    ltSpurOrientation.Text = assetDataObj.SpurManager.Orientation.ToString();

                    ltSpurLength.Text = assetDataObj.SpurManager.LengthSpur.ToString();

                    ltSpurWidth.Text = assetDataObj.SpurManager.WidthSpur.ToString();

                    StringBuilder htmlSpurShapeTypeManagerList = new StringBuilder();
                    if (spurShapeTypeManagerList.Count > 0)
                    {
                        foreach (SpurShapeTypeManager spurShapeTypeManagerObj in spurShapeTypeManagerList)
                        {
                            if (spurShapeTypeManagerObj.ShapeTypeName == assetDataObj.SpurManager.SpurShapeTypeManager.ShapeTypeName)
                            {
                                htmlSpurShapeTypeManagerList.Append("<option selected=\"selected\" value=\"" + spurShapeTypeManagerObj.ShapeTypeGuid + "\">" + spurShapeTypeManagerObj.ShapeTypeName + "</option>");
                            }
                            else
                            {
                                htmlSpurShapeTypeManagerList.Append("<option value=\"" + spurShapeTypeManagerObj.ShapeTypeGuid + "\">" + spurShapeTypeManagerObj.ShapeTypeName + "</option>");
                            }
                        }
                    }
                    ltSpurShapeTypeList.Text = htmlSpurShapeTypeManagerList.ToString();

                    StringBuilder htmlSpurConstructionTypeManagerList = new StringBuilder();
                    if (spurConstructionTypeManagerList.Count > 0)
                    {
                        foreach (SpurConstructionTypeManager spurConstructionTypeManagerObj in spurConstructionTypeManagerList)
                        {
                            if (spurConstructionTypeManagerObj.ConstructionTypeName == assetDataObj.SpurManager.SpurConstructionTypeManager.ConstructionTypeName)
                            {
                                htmlSpurConstructionTypeManagerList.Append("<option selected=\"selected\" value=\"" + spurConstructionTypeManagerObj.ConstructionTypeGuid + "\">" + spurConstructionTypeManagerObj.ConstructionTypeName + "</option>");
                            }
                            else
                            {
                                htmlSpurConstructionTypeManagerList.Append("<option value=\"" + spurConstructionTypeManagerObj.ConstructionTypeGuid + "\">" + spurConstructionTypeManagerObj.ConstructionTypeName + "</option>");
                            }
                        }
                    }
                    ltSpurConstructionTypeList.Text = htmlSpurConstructionTypeManagerList.ToString();

                    StringBuilder htmlSpurRevetTypeManagerList = new StringBuilder();
                    if (spurRevetTypeManagerList.Count > 0)
                    {
                        foreach (SpurRevetTypeManager spurRevetTypeManagerObj in spurRevetTypeManagerList)
                        {
                            if (spurRevetTypeManagerObj.SpurRevetTypeName == assetDataObj.SpurManager.SpurRevetTypeManager.SpurRevetTypeName)
                            {
                                htmlSpurRevetTypeManagerList.Append("<option selected=\"selected\" value=\"" + spurRevetTypeManagerObj.SpurRevetTypeGuid + "\">" + spurRevetTypeManagerObj.SpurRevetTypeName + "</option>");
                            }
                            else
                            {
                                htmlSpurRevetTypeManagerList.Append("<option value=\"" + spurRevetTypeManagerObj.SpurRevetTypeGuid + "\">" + spurRevetTypeManagerObj.SpurRevetTypeName + "</option>");
                            }
                        }
                    }
                    ltSpurRevetTypeList.Text = htmlSpurRevetTypeManagerList.ToString();
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode.ToString() == "TRN")
                {
                    List<TurnoutTypeManager> turnoutTypeManagerList = new List<TurnoutTypeManager>();
                    turnoutTypeManagerList = new TurnoutTypeDB().GetTurnoutTypeList();

                    List<TurnoutGateTypeManager> turnoutGateTypeManagerList = new List<TurnoutGateTypeManager>();
                    turnoutGateTypeManagerList = new TurnoutGateTypeDB().GetTrnGateTypeList();

                    ltTurnoutGuidHidden.Text = assetDataObj.TurnoutManager.TurnoutGuid.ToString();

                    ltTurnoutInletBoxWidth.Text = assetDataObj.TurnoutManager.InletboxWidth.ToString();

                    ltTurnoutInletBoxLength.Text = assetDataObj.TurnoutManager.InletboxLength.ToString();

                    ltTurnoutOutletNumber.Text = assetDataObj.TurnoutManager.OutletNumber.ToString();

                    ltTurnoutOutletWidth.Text = assetDataObj.TurnoutManager.OutletWidth.ToString();

                    ltTurnoutOutletHeight.Text = assetDataObj.TurnoutManager.OutletHeight.ToString();

                    StringBuilder htmlTurnoutTypeManagerList = new StringBuilder();
                    if (turnoutTypeManagerList.Count > 0)
                    {
                        foreach (TurnoutTypeManager turnoutTypeManagerObj in turnoutTypeManagerList)
                        {
                            if (turnoutTypeManagerObj.TurnoutTypeName == assetDataObj.TurnoutManager.TurnoutTypeManager.TurnoutTypeName)
                            {
                                htmlTurnoutTypeManagerList.Append("<option selected=\"selected\" value=\"" + turnoutTypeManagerObj.TurnoutTypeGuid + "\">" + turnoutTypeManagerObj.TurnoutTypeName + "</option>");
                            }
                            else
                            {
                                htmlTurnoutTypeManagerList.Append("<option value=\"" + turnoutTypeManagerObj.TurnoutTypeGuid + "\">" + turnoutTypeManagerObj.TurnoutTypeName + "</option>");
                            }
                        }
                    }
                    ltTurnoutTypeList.Text = htmlTurnoutTypeManagerList.ToString();

                    StringBuilder htmlTurnoutGateTypeManagerList = new StringBuilder();
                    if (turnoutGateTypeManagerList.Count > 0)
                    {
                        foreach (TurnoutGateTypeManager turnoutGateTypeManagerObj in turnoutGateTypeManagerList)
                        {
                            if (turnoutGateTypeManagerObj.TurnoutGateTypeName == assetDataObj.TurnoutManager.TurnoutGateTypeManager.TurnoutGateTypeName)
                            {
                                htmlTurnoutGateTypeManagerList.Append("<option selected=\"selected\" value=\"" + turnoutGateTypeManagerObj.TurnoutGateTypeGuid + "\">" + turnoutGateTypeManagerObj.TurnoutGateTypeName + "</option>");
                            }
                            else
                            {
                                htmlTurnoutGateTypeManagerList.Append("<option value=\"" + turnoutGateTypeManagerObj.TurnoutGateTypeGuid + "\">" + turnoutGateTypeManagerObj.TurnoutGateTypeName + "</option>");
                            }
                        }
                    }
                    ltTurnoutGateTypeList.Text = htmlTurnoutGateTypeManagerList.ToString();

                    StringBuilder htmlTurnoutGatesTableBody = new StringBuilder();
                    if (gateManagerList.Count > 0)
                    {
                        for (var i = 0; i < gateManagerList.Count; i++)
                        {
                            htmlTurnoutGatesTableBody.Append("<tr>");

                            htmlTurnoutGatesTableBody.Append("<td>" + i + "</td>");
                            htmlTurnoutGatesTableBody.Append("<td>" + gateManagerList[i].GateGuid + "</td>");
                            htmlTurnoutGatesTableBody.Append("<td>" + gateManagerList[i].GateTypeManager.GateTypeGuid + "</td>");
                            htmlTurnoutGatesTableBody.Append("<td>" + gateManagerList[i].GateConstructionMaterialManager.GateConstructionMaterialGuid + "</td>");
                            htmlTurnoutGatesTableBody.Append("<td>" + gateManagerList[i].GateTypeManager.GateTypeName + "</td>");
                            htmlTurnoutGatesTableBody.Append("<td>" + gateManagerList[i].GateConstructionMaterialManager.GateConstructionMaterialName + "</td>");
                            htmlTurnoutGatesTableBody.Append("<td>" + gateManagerList[i].NumberOfGates + "</td>");
                            htmlTurnoutGatesTableBody.Append("<td>" + gateManagerList[i].Height + "</td>");
                            htmlTurnoutGatesTableBody.Append("<td>" + gateManagerList[i].Width + "</td>");
                            htmlTurnoutGatesTableBody.Append("<td>" + gateManagerList[i].Diameter + "</td>");
                            htmlTurnoutGatesTableBody.Append("<td>" + gateManagerList[i].InstallDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                            htmlTurnoutGatesTableBody.Append("<td>" + "<a href=\"#\" class=\"editTurnoutGateBtn\"><i class=\"ui-tooltip fa fa-pencil-square-o\" style=\"font-size: 22px;\" data-original-title=\"Edit\" title=\"Edit\"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"#\" class=\"deleteTurnoutGateBtn\"><i class=\"ui-tooltip fa fa-trash-o\" style=\"font-size: 22px;\" data-original-title=\"Delete\" title=\"Delete\"></i></a>" + "</td>");

                            htmlTurnoutGatesTableBody.Append("</tr>");
                        }
                    }
                    ltTurnoutGatesTableBody.Text = htmlTurnoutGatesTableBody.ToString();
                }
                else if (assetDataObj.AssetTypeManager.AssetTypeCode == "WEI")
                {
                    List<WeirTypeManager> weirTypeManagerList = new List<WeirTypeManager>();
                    weirTypeManagerList = new WeirTypeDB().GetWeirTypeList();

                    ltWeirGuidHidden.Text = assetDataObj.WeirManager.WeirGuid.ToString();

                    ltWeirInvertLevel.Text = assetDataObj.WeirManager.InvertLevel.ToString();

                    ltWeirCrestTopLevel.Text = assetDataObj.WeirManager.CrestTopLevel.ToString();

                    ltWeirCrestTopWidth.Text = assetDataObj.WeirManager.CrestTopWidth.ToString();

                    StringBuilder htmlWeirTypeManagerList = new StringBuilder();
                    if (weirTypeManagerList.Count > 0)
                    {
                        foreach (WeirTypeManager weirTypeManagerObj in weirTypeManagerList)
                        {
                            if (weirTypeManagerObj.WeirTypeName == assetDataObj.WeirManager.WeirTypeManager.WeirTypeName)
                            {
                                htmlWeirTypeManagerList.Append("<option selected=\"selected\" value=\"" + weirTypeManagerObj.WeirTypeGuid + "\">" + weirTypeManagerObj.WeirTypeName + "</option>");
                            }
                            else
                            {
                                htmlWeirTypeManagerList.Append("<option value=\"" + weirTypeManagerObj.WeirTypeGuid + "\">" + weirTypeManagerObj.WeirTypeName + "</option>");
                            }
                        }
                    }
                    ltWeirTypeList.Text = htmlWeirTypeManagerList.ToString();
                }

            }

        }

        [WebMethod]
        public static int UpdateAsset(AssetManager assetManager)
        {
            assetManager.AssetGuid = new Guid(System.Web.HttpContext.Current.Session["sessionAssetGuid"].ToString());

            int rowsAffected = new AssetDB().UpdateAssetData(assetManager);

            if (rowsAffected == 1)
            {
                int assetTypeUpdated = 0;
                switch (assetManager.AssetTypeManager.AssetTypeCode)
                {
                    case "BRG":
                        assetTypeUpdated = new BridgeDB().UpdateBridge(assetManager);
                        break;
                    case "CAN":
                        assetTypeUpdated = new CanalDB().UpdateCanalData(assetManager);
                        break;
                    case "CUL":
                        assetTypeUpdated = new CulvertDB().UpdateCulvertData(assetManager);
                        break;
                    case "DRN":
                        assetTypeUpdated = new DrainageDB().UpdateDrainageData(assetManager);
                        break;
                    case "DRP":
                        assetTypeUpdated = new DropStructureDB().UpdateDropStructureData(assetManager);
                        break;
                    case "EMB":
                        assetTypeUpdated = new EmbankmentDB().UpdateEmbankmentData(assetManager);
                        break;
                    case "GAU":
                        assetTypeUpdated = new GaugeDB().UpdateGaugeData(assetManager);
                        break;
                    case "POR":
                        assetTypeUpdated = new PorcupineDB().UpdatePorcupineData(assetManager);
                        break;
                    case "REG":
                        assetTypeUpdated = new RegulatoreDB().UpdateRegulatorData(assetManager);
                        break;
                    case "REV":
                        assetTypeUpdated = new RevetmentDB().UpdateRevetmentData(assetManager);
                        break;
                    case "SLU":
                        assetTypeUpdated = new SluiceDB().UpdateSluiceData(assetManager);
                        break;
                    case "SPU":
                        assetTypeUpdated = new SpurDB().UpdateSpurData(assetManager);
                        break;
                    case "TRN":
                        assetTypeUpdated = new TurnoutDB().UpdateTurnoutData(assetManager);
                        break;
                    case "WEI":
                        assetTypeUpdated = new WeirDB().UpdateWeirData(assetManager);
                        break;
                    default:
                        assetTypeUpdated = 0;
                        break;
                }
                //if (assetTypeUpdated == 0)
                //{
                //    new AssetDB().DeleteAsset(assetManager.AssetGuid);
                //    rowsAffected = 0;
                //}
                //else
                //{
                //    if (assetManager.AssetDrawingManager.DrawingFileName != "")
                //    {
                //        new AssetDrawingDB().AddDrawing(assetManager);
                //    }
                //}
            }
            return rowsAffected;
        }

        [WebMethod]
        public static int DeleteGateData(Guid gateGuid)
        {
            return new GateDB().DeleteGateData(gateGuid);
        }

    }
}


