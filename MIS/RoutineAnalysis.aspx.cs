﻿using MIS.App_Code;
using NodaMoney;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class RoutineAnalysis : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "DBA")
                {
                    if (!Page.IsPostBack)
                    {
                        DisplayRoutineWorkItemsList();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
            }
        }

        private void DisplayRoutineWorkItemsList()
        {
            List<RoutineWorkItemManager> routineWorkItemsManagerList = new List<RoutineWorkItemManager>();

            routineWorkItemsManagerList = new RoutineWorkItemDB().GetRoutineWorkItemsForRoutineAnalysis();
            StringBuilder htmlRoutineWorkItemList = new StringBuilder();
            
            htmlRoutineWorkItemList.Append("<select class=\"form-control multiselect\" name=\"routineWorkItemList\" id=\"routineWorkItemList\" onfocus=\"dbaFocusFunction(this.id)\" onblur=\"dbaBlurFunction(this.id)\" >");
            htmlRoutineWorkItemList.Append("<option value=\"\">" + "Select Routine Work Item" + "</option>");
            if (routineWorkItemsManagerList.Count > 0)
            {
                foreach (RoutineWorkItemManager routineWorkItemsManagerObj in routineWorkItemsManagerList)
                {
                    htmlRoutineWorkItemList.Append("<option value=\"" + routineWorkItemsManagerObj.RoutineWorkItemGuid.ToString() + "\">" + routineWorkItemsManagerObj.RoutineWorkItemTypeManager.RoutineWorkItemTypeFullName + " (" +  routineWorkItemsManagerObj.RoutineWorkItemTypeCode.ToString() + ")" + "</option>");
                }
            }
            htmlRoutineWorkItemList.Append("</select>");
            ltRoutineWorkItemList.Text = htmlRoutineWorkItemList.ToString();
        }

        [WebMethod]
        public static List<RoutineWorkItemManager> GetRoutineWorkItemDataBasedOnWorkItem(string routineWorkItemGuid)
        {
            List<RoutineWorkItemManager> routineWorkItemDataList = new List<RoutineWorkItemManager>();
            routineWorkItemDataList = new RoutineWorkItemDB().GetRoutineWorkItemDataForRoutineAnalysis(new Guid(routineWorkItemGuid));

            return routineWorkItemDataList;
        }

        [WebMethod]
        public static List<AssetTypeAndAssetQuantityParameterRelationManager> GetAssetTypeBasedOnAssetQuantityParameterForRoutineAnalysis(string assetQuantityParameterGuid)
        {
            List<AssetTypeAndAssetQuantityParameterRelationManager> assetTypeAndAssetQuantityParameterRelationManagerList = new List<AssetTypeAndAssetQuantityParameterRelationManager>();
            assetTypeAndAssetQuantityParameterRelationManagerList = new AssetTypeAndAssetQuantityParameterRelationDB().GetAssetTypeBasedOnAssetQuantityParameterForRoutineAnalysis(new Guid(assetQuantityParameterGuid));

            return assetTypeAndAssetQuantityParameterRelationManagerList;
        }

        [WebMethod]
        public static List<AssetManager> GetAssetCodeBasedOnAssetTypeForRoutineAnalysis(string assetTypeGuid)
        {
            List<AssetManager> assetCodeManagerList = new List<AssetManager>();
            assetCodeManagerList = new AssetDB().GetAssetCodeBasedOnAssetTypeForRoutineAnalysis(new Guid(assetTypeGuid));

            return assetCodeManagerList;
        }

        [WebMethod]
        public static AssetManager GetAssetCodeDataForRoutineAnalysis(Guid assetGuid)
        {
            AssetManager assetCodeDataObj = new AssetManager();
            assetCodeDataObj = new AssetDB().GetAssetCodeDataForRoutineAnalysis(assetGuid);
            
            return assetCodeDataObj;
        }

        [WebMethod]
        public static AssetManager GetAssetTypeDataForRoutineAnalysis(string assetGuid, string assetTypeCode)
        {
            AssetManager assetTypeDataObj = new AssetManager();
            assetTypeDataObj = new AssetDB().GetAssetTypeDataForRoutineAnalysis(new Guid(assetGuid), assetTypeCode);
            return assetTypeDataObj;
        }

        [WebMethod]
        public static List<GateManager> GetGateDataForAssetQuantityForRoutineAnalysis(string assetGuid)
        {
            List<GateManager> gateDataList = new List<GateManager>();
            gateDataList = new GateDB().GetGateDataListByAssetUuid(new Guid(assetGuid));

            return gateDataList;
        }

        [WebMethod]
        public static AssetCodeAndAssetRiskRatingRelationManager GetAssetRiskRatingForAssetCodeForRoutineAnalysis(string assetGuid)
        {
            AssetCodeAndAssetRiskRatingRelationManager assetRiskRatingRelationForAssetCodeManagerList = new AssetCodeAndAssetRiskRatingRelationManager();
            assetRiskRatingRelationForAssetCodeManagerList = new AssetCodeAndAssetRiskRatingRelationDB().GetAssetRiskRatingForAssetCode(new Guid(assetGuid));
            return assetRiskRatingRelationForAssetCodeManagerList;
        }

        [WebMethod]
        public static RoutineServiceLevelManager GetRoutineServiceLevelForRoutineAnalysis(string routineWorkItemGuid, string assetTypeGuid)
        {
            RoutineServiceLevelManager routineServiceLevelManagerObj = new RoutineServiceLevelManager();
            routineServiceLevelManagerObj = new RoutineServiceLevelDB().GetRoutineServiceLevelForRoutineAnalysis(new Guid(routineWorkItemGuid), new Guid(assetTypeGuid));
            return routineServiceLevelManagerObj;
        }

        [WebMethod]
        public static List<RoutineServiceLevelManager> GetRoutineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis(string routineWorkItemGuid)
        {
            List<RoutineServiceLevelManager> routineServiceLevelManagerList = new List<RoutineServiceLevelManager>();
            routineServiceLevelManagerList = new RoutineServiceLevelDB().GetRoutineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis(new Guid(routineWorkItemGuid));
            return routineServiceLevelManagerList;
        }

        [WebMethod]
        public static RoutineIntensityFactorManager GetRoutineIntensityFactorDataForRoutineAnalysis(string routineWorkItemGuid, string assetGuid)
        {
            RoutineIntensityFactorManager routineIntensityFactorManagerObj = new RoutineIntensityFactorManager();
            routineIntensityFactorManagerObj = new RoutineIntensityFactorDB().GetRoutineIntensityFactorDataForRoutineAnalysis(new Guid(routineWorkItemGuid), new Guid(assetGuid));
            return routineIntensityFactorManagerObj;
        }

        [WebMethod]
        public static RoutineWorkItemRateManager GetRoutineWorkItemRateForRoutineAnalysis(string routineWorkItemGuid)
        {
            RoutineWorkItemRateManager routineWorkItemRateManagerObj = new RoutineWorkItemRateManager();
            routineWorkItemRateManagerObj = new RoutineWorkItemRateDB().GetRoutineWorkItemRateForRoutineAnalysis(new Guid(routineWorkItemGuid));
            return routineWorkItemRateManagerObj;
        }

        [WebMethod]
        public static RoutineAnalysisDetailsManager GetRoutineAnalysisGuid(string routineWorkItemGuid, string assetGuid)
        {
            RoutineAnalysisDetailsManager routineAnalysisDetailsManager = new RoutineAnalysisDetailsManager();
            routineAnalysisDetailsManager = new RoutineAnalysisDetailsDB().GetRoutineAnalysisGuid(new Guid(routineWorkItemGuid), new Guid(assetGuid));
            return routineAnalysisDetailsManager;
        }

        [WebMethod]
        public static SystemVariablesManager GetSystemVariablesGuid()
        {
            SystemVariablesManager systemVariablesManagerObj = new SystemVariablesManager();
            systemVariablesManagerObj = new SystemVariablesDB().GetSystemVariablesGuid();
            return systemVariablesManagerObj;
        }

        [WebMethod]
        public static int UpdateRoutineWorkItemData(RoutineWorkItemManager routineWorkItemManager)
        {
            return new RoutineWorkItemDB().UpdateRoutineWorkItemData(new Guid(routineWorkItemManager.RoutineWorkItemGuid.ToString()), routineWorkItemManager);
        }

        [WebMethod]
        public static Guid SaveRoutineServiceLevelData(Guid routineWorkItemGuid, Guid assetTypeGuid, int routineServiceLevel, string routineServiceLevelCode)
        {
            Guid routineServiceLevelGuid = Guid.NewGuid();
            int returnValue= new RoutineServiceLevelDB().SaveRoutineServiceLevelData(routineServiceLevelGuid, routineWorkItemGuid, assetTypeGuid, routineServiceLevel, routineServiceLevelCode);
            if (returnValue == 1)
            {
                return routineServiceLevelGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public static int UpdateRoutineServiceLevelData(string routineServiceLevelGuid, string routineServiceLevel)
        {
            return new RoutineServiceLevelDB().UpdateRoutineServiceLevelData(new Guid(routineServiceLevelGuid), Convert.ToInt16(routineServiceLevel));
        }

        [WebMethod]
        public static int DeleteRoutineServiceLevelData(Guid routineWorkItemGuid, Guid assetTypeGuid)
        {
            return new RoutineServiceLevelDB().DeleteRoutineServiceLevelData(routineWorkItemGuid, assetTypeGuid);
        }

        [WebMethod]
        public static Guid SaveRoutineIntensityFactorData(Guid routineWorkItemGuid, Guid assetGuid, int routineIntensityFactor, string routineIntensityFactorJustification)
        {
           Guid routineIntensityFactorGuid  = Guid.NewGuid();
           int result = new RoutineIntensityFactorDB().SaveRoutineIntensityFactorData(routineIntensityFactorGuid, routineWorkItemGuid, assetGuid, routineIntensityFactor, routineIntensityFactorJustification);
           if (result == 1)
           {
                return routineIntensityFactorGuid;
           }
           else
           {
               return new Guid();
           }
        }

        [WebMethod]
        public static int UpdateRoutineIntensityFactorDataForRoutineAnalysis(string routineIntensityFactorGuid, string routineIntensityFactor, string routineIntensityFactorJustification)
        {
            return new RoutineIntensityFactorDB().UpdateRoutineIntensityFactorData(new Guid(routineIntensityFactorGuid), Convert.ToInt16(routineIntensityFactor), routineIntensityFactorJustification);
        }

        [WebMethod]
        public static int DeleteRoutineIntensityFactorData(Guid routineWorkItemGuid, Guid assetGuid)
        {
            return new RoutineIntensityFactorDB().DeleteRoutineIntensityFactorData(routineWorkItemGuid, assetGuid);
        }

        [WebMethod]
        public static Guid SaveRoutineWorkItemRateData(RoutineWorkItemRateManager routineWorkItemRateManager)
        {
            routineWorkItemRateManager.RoutineWorkItemRateGuid = Guid.NewGuid();
            int result = new RoutineWorkItemRateDB().AddRoutineWorkItemRateData(routineWorkItemRateManager);
            if (result == 1)
            {
                return routineWorkItemRateManager.RoutineWorkItemRateGuid;
            }
            else
            {
                return new Guid();
            }
        }                

        [WebMethod]
        public static int UpdateRoutineWorkItemRateDataForRoutineAnalysis(string routineWorkItemRateGuid, string routineWorkItemRate)
        {
            return new RoutineWorkItemRateDB().UpdateRoutineWorkItemRateData(new Guid(routineWorkItemRateGuid), Money.Parse(routineWorkItemRate));
        }

        [WebMethod]
        public static Guid SaveRoutineAnalysisDetailsData(RoutineAnalysisDetailsManager routineAnalysisDetailsManager)
        {
            routineAnalysisDetailsManager.RoutineAnalysisDetailsGuid = Guid.NewGuid();
            int result = new RoutineAnalysisDetailsDB().SaveRoutineAnalysisDetailsData(routineAnalysisDetailsManager);
            if (result == 1)
            {
                return routineAnalysisDetailsManager.RoutineAnalysisDetailsGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public static int UpdateRoutineAnalysisDetailsData(RoutineAnalysisDetailsManager routineAnalysisDetailsManager)
        {
            return new RoutineAnalysisDetailsDB().UpdateRoutineAnalysisDetailsData(new Guid(routineAnalysisDetailsManager.RoutineAnalysisDetailsGuid.ToString()), routineAnalysisDetailsManager);
        }

        [WebMethod]
        public static int DeleteRoutineAnalysisDetailsData(Guid routineWorkItemGuid, Guid assetGuid)
        {
            return new RoutineAnalysisDetailsDB().DeleteRoutineAnalysisDetailsData(routineWorkItemGuid, assetGuid);
        }


    }
}



                  