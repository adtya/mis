﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/SIO.Master" AutoEventWireup="true" CodeBehind="ChangeUserPassword.aspx.cs" Inherits="MIS.ChangeUserPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    <link href="CustomStyles/change-user-password.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div class="container" style="margin-top:2%;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-th"></span>
                            Change password
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 separator social-login-box"> <br />
                                <img alt="" class="img-thumbnail img-responsive" src="images/image_change_password.jpg" />
                            </div>
                            <div style="margin-top:80px;" class="col-xs-6 col-sm-6 col-md-6 login-box">
                                <form class="form" id="passForm" name="passForm">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-lock"></span>
                                            </div>
                                            <input class="form-control" type="password" name="currPassword" id="currPassword" placeholder="Current Password" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-log-in"></span>
                                            </div>
                                            <input class="form-control" type="password" name="newPassword" id="newPassword" placeholder="New Password" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-log-in"></span>
                                            </div>
                                            <input class="form-control" type="password" name="rnewPassword" id="rnewPassword" placeholder="Retype New Password" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <button type="button" class="btn btn-primary" id="btnGoBack" onclick="goBack();">Go Back</button>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <button type="button" class="action btn-hot text-capitalize submit btn icon-btn-save btn-success" id="btnChangePassword">
                                <!--<button class="btn icon-btn-save btn-success submit" type="button">-->
                                    <span class="btn-save-label">
                                        <i class="glyphicon glyphicon-floppy-disk"></i>
                                    </span>
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
    <script src="CustomScripts/change-user-password.js" type="text/javascript"></script>
</asp:Content>