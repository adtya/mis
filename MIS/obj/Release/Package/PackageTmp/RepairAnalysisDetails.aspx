﻿<%@ Page Language="C#" Title="Repair Analysis" MasterPageFile="~/DBA.Master" AutoEventWireup="true" CodeBehind="RepairAnalysisDetails.aspx.cs" Inherits="MIS.RepairAnalysisDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">   
    <link href="CustomStyles/ams.css" rel="stylesheet" type="text/css" />
    <link href="CustomStyles/OM/repair-analyis-datatable.css" rel="stylesheet" type="text/css" />
    <link href="Content/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">    
    <script src="Scripts/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="Scripts/Datejs/build/date.js" type="text/javascript"></script>
    <script src="Scripts/date.format.js" type="text/javascript"></script>

    <script src="CustomScripts/repair-analysis-details.js" type="text/javascript"></script> 
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">               
                        <form role="form" name="repairAnalysisForm" id="repairAnalysisForm" class="form form-horizontal" action="#" method="post" runat="server">
                            <fieldset>
                                <div class="row">
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <div class="form-group">
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                    <label for="fiscalYear" id="fiscalYearLabel" class="control-label">Fiscal Year</label>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                    <asp:Literal ID="ltFiscalYear" runat="server"></asp:Literal>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <div class="form-group">
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                    <asp:Literal ID="ltAvailableFunds" runat="server"></asp:Literal>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                    <asp:Literal ID="ltAvailableFundsValue" runat="server"></asp:Literal>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                    </div>
                               
                                <%--<div class="row">
                                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <div class="form-group">
                                                 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                     <label for="assetCode" id="assetCodeLabel" class="control-label">Asset Code</label>
                                                 </div>
                                                 <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                    <asp:Literal ID="ltAssetCodeForRepairAnalysisDetails" runat="server"></asp:Literal>
                                                 </div>
                                              </div>
                                        </div>
                                     </div>
                                </div>--%>
                                                            
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">                                     
                                           <div class="form-group" id="repairAnalysisListPanel"></div>                                                
                                            <%--<asp:Literal runat="server" ID="repairAnalysisList" />--%>
                                            </div>                                    
                                </div>

                                <fieldset class="scheduler-border" id="descriptionFieldset" name="descriptionFieldset" hidden="hidden">
                                           <legend class="scheduler-border">Description</legend>

                                           <div class="col-sm-12 col-md-12">                                
                                                                                          
                                               <div class="row">
                                                       <div class="col-sm-12 col-md-12">
                                                           <div class="col-sm-3 col-md-3">
                                                               <div class="form-group">
                                                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                       <label for="damageDescription" id="damageDescriptionLabel" class="control-label text-left">Damage Description</label>
                                                                   </div>
                                                               </div>
                                                            </div>
                                                           <div class="col-sm-9 col-md-9">
                                                               <div class="form-group">
                                                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <textarea id="damageDescription" rows="1" cols="2" name="damageDescription" class="form-control"></textarea>                                                                  
                                                                   </div>
                                                               </div>
                                                           </div>                                                                                                        
                                                               
                                                      </div>
                                                   </div>

                                               <div class="row">
                                                       <div class="col-sm-12 col-md-12">
                                                           <div class="col-sm-3 col-md-3">
                                                               <div class="form-group">
                                                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                       <label for="failureModeDescription" id="failureModeDescriptionlabel" class="control-label text-left">Failure Mode Description</label>
                                                                   </div>
                                                               </div>
                                                            </div>
                                                           <div class="col-sm-9 col-md-9">
                                                               <div class="form-group">
                                                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <textarea id="failureModeDescription" rows="1" cols="2" name="failureModeDescription" class="form-control"></textarea>                                                                  
                                                                   </div>
                                                               </div>
                                                           </div>                                                                                                        
                                                               
                                                      </div>
                                                   </div>  

                                               <div class="row">
                                                       <div class="col-sm-12 col-md-12">
                                                           <div class="col-sm-3 col-md-3">
                                                               <div class="form-group">
                                                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                       <label for="repairDescription" id="repairDescriptionLabel" class="control-label text-left">Repair Description</label>
                                                                   </div>
                                                               </div>
                                                            </div>
                                                           <div class="col-sm-9 col-md-9">
                                                               <div class="form-group">
                                                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <textarea id="repairDescription" rows="1" cols="2" name="repairDescription" class="form-control"></textarea>                                                                  
                                                                   </div>
                                                               </div>
                                                           </div>                                                                                                        
                                                               
                                                      </div>
                                                   </div>  
                                               
                                               <div class="row" style="display:none">
                                                       <div class="col-sm-12 col-md-12">
                                                           <div class="col-sm-3 col-md-3">
                                                               <div class="form-group">
                                                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                       <label for="intensityFactorJustificationForRoutineAnalysis" id="intensityFactorJustificationForRoutineAnalysisLabel" class="control-label text-left">Intensity Factor Justification</label>
                                                                   </div>
                                                               </div>
                                                            </div>
                                                           <div class="col-sm-9 col-md-9">
                                                               <div class="form-group">
                                                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <textarea id="intensityFactorJustificationForRoutineAnalysis" rows="1" cols="2" name="intensityFactorJustificationForRoutineAnalysis" class="form-control"></textarea>                                                                  
                                                                   </div>
                                                               </div>
                                                           </div> 
                                                                                                                  
                                                               
                                                      </div>
                                                   </div>                                  
                                                                                                  
                                          </div>
                                    </fieldset>                                    

                             </fieldset>                           
                        </form>                 
            </div>
        </div>

    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
</asp:Content>
