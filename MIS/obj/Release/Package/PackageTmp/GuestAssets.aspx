﻿<%@ Page Title="Assets" Language="C#" AutoEventWireup="true" MasterPageFile="~/Guest.Master" CodeBehind="GuestAssets.aspx.cs" Inherits="MIS.GuestAssets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    <link href="Content/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
    <script src="Scripts/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="Scripts/Datejs/build/date.js" type="text/javascript"></script>
    <script src="Scripts/date.format.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">

    <div class="container-fluid"><%-- style="margin-top:5%;">--%>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <%--<div class="panel panel-default">--%>

                    <%--<div class="panel-body">--%>
                        <form role="form" name="guestAssetsForm" id="guestAssetsForm" class="form form-horizontal" action="#" method="post" runat="server">
                            <fieldset>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="box clearfix">

                                            <%--<div class="panel panel-default">--%>
                                                <%--<div class="panel-body">--%>

                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="divisionList" id="divisionListLabel" class="control-label">Division</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <%--<asp:Literal ID="ltDivisionList" runat="server"></asp:Literal>--%>
                                                                    <select class="form-control multiselect" name="divisionList" id="divisionList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="schemeList" id="schemeListLabel" class="control-label">Scheme</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <select class="form-control multiselect" id="schemeList" name="schemeList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="assetTypeList" id="assetTypeLabel" class="control-label">Asset Type</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <asp:Literal ID="ltAssetTypeList" runat="server"></asp:Literal>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="initiationYear" id="initiationYearLabel" class="control-label">Initiation Year</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <div id="dpDateOfInitiationYear" class="input-group date">
                                                                        <input type="text" name="dateOfInitiationYear" class="form-control" id="dateOfInitiationYear" placeholder="Initiation Year (dd/mm/yyyy)" />
                                                                        <span class="input-group-addon form-control-static">
                                                                            <i class="fa fa-calendar fa-fw"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="completionYear" id="completionYearLabel" class="control-label">Completion Year</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <div id="dpDateOfCompletionYear" class="input-group date">
                                                                        <input type="text" name="dateOfCompletionYear" class="form-control" id="dateOfCompletionYear" placeholder="Completion Year (dd/mm/yyyy)" />
                                                                        <span class="input-group-addon form-control-static">
                                                                            <i class="fa fa-calendar fa-fw"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                <%--</div>--%>
                                            <%--</div>--%>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">

                                            <asp:Literal runat="server" ID="assetsList" />

                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    <%--</div>--%>

                <%--</div>--%>
            </div>
        </div>

    </div>
   
    <div id="viewAssetPopupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" style="overflow-y:initial !important;">
                <div class="modal-content">

                    <div class="modal-header" style="display:none"></div>
                    <%--<div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h1 class="text-center">What's My Password?</h1>
                        </div>--%>

                    <div class="modal-body" style="max-height:calc(100vh - 200px); overflow-y: auto;">                      
                    </div>

                    <div class="modal-footer">
                        <div class="col-md-12">
                            <button class="btn" data-dismiss="modal" aria-hidden="true" id="closeBtn">Close</button>
                        </div>
                    </div>

                </div>
            </div>
     </div>   

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
    <script src="CustomScripts/Guest/Asset/guest-assets.js" type="text/javascript"></script>
</asp:Content>
