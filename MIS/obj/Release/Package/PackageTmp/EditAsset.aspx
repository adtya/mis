﻿<%@ Page Title="Edit Asset" Language ="C#" MasterPageFile="~/DBA.Master" AutoEventWireup="true" CodeBehind="EditAsset.aspx.cs" Inherits="MIS.EditAsset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    <link href="Content/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
    <script src="Scripts/jquery.glob.js" type="text/javascript"></script>
    <script src="Scripts/globinfo/jQuery.glob.en-IN.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.GlobalMoneyInput2.js" type="text/javascript"></script>

    <script src="Scripts/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="Scripts/Datejs/build/date.js" type="text/javascript"></script>
    <script src="Scripts/date.format.js" type="text/javascript"></script>
    <script src="Scripts/jquery.Guid.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Asset Details</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server" id="editAssetForm" name="editAssetForm">
                            <fieldset>                               
                                <div class="panel panel-default">
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                        
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                               <label for="divisionCode" id="divisionCodeLabel" class="control-label">Division</label>
                                                             </div>
                                                             <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                 <asp:Literal ID="ltDivisionCode" runat="server"></asp:Literal> 
                                                               <%--<input id="divisionCode" name="divisionCode" class="form-control" readonly="readonly" />  --%>                                             
                                                             </div>
                                                         </div>
                                                     </div>
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                               <label for="schemeCode" id="schemeCodeLabel" class="control-label">Scheme</label>
                                                           </div>
                                                           <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                               <asp:Literal ID="ltSchemeCode" runat="server"></asp:Literal> 
                                                                <%--<input id="schemeCode" name="schemeCode" class="form-control" readonly="readonly" />--%>
                                                           </div>
                                                        </div>
                                                   </div>
                                             </div>
                                        </div>                               
                                
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                      
                                                    <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                 <label for="assetTypeCode" id="assetTypeCodeLabel" class="control-label">Asset Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <asp:Literal ID="ltAssetTypeCode" runat="server"></asp:Literal> 
                                                                <%--<input id="assetTypeCode" name="assetTypeCode" class="form-control" readonly="readonly" />--%>
                                                            </div>
                                                     </div>
                                                   </div>

                                                    <div class="col-sm-6 col-md-6">
                                                          <div class="form-group">
                                                              <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                 <label for="assetCode" id="assetCodeLabel" class="control-label">Asset Code</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <asp:Literal ID="ltAssetCode" runat="server"></asp:Literal>
                                                                <%--<input id="assetCode" name="assetCode" class="form-control" readonly="readonly" />--%>
                                                            </div>                                                  
                                                          </div>
                                                    </div>
                                                </div>
                                            </div>
                                     
                                    </div>
                                   </div>
                                

                                <div class="panel panel-default" id="showKeyAssetPanel"> <%--style="display:none">--%>
                                    <div class="panel-body">
                                        <div class="row">                                         
                                                <div class="col-sm-12 col-md-12" >
                                                     <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="keyAsset" id="keyAssetLabel" class="control-label">Select Key Asset</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <%-- <select class="form-control multiselect" name="keyAssetList" id="keyAssetList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">

                                                                  </select>--%>
                                                                <asp:Literal ID="ltKeyAsset" runat="server"></asp:Literal>
                                                            </div>
                                                        </div>
                                                    </div>
                                                 </div>
                                            </div>
                                      </div>
                                </div>                             
                                                      
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                                
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="drawingCode" id="drawingCodeLabel" class="control-label">Drawing Code</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <asp:Literal ID="ltDrawingCode" runat="server"></asp:Literal>
                                                               <%-- <input id="drawingCode" name="drawingCode" placeholder="Drawing Code" class="form-control" /> --%>                                                 
                                                            </div>
                                                         </div>
                                                     </div>

                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                 <label for="amtd" id="amtdLabel"class="control-label">AMTD</label>
                                                           </div>
                                                           <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                               <asp:Literal ID="ltAmtd" runat="server"></asp:Literal>
                                                               <%-- <input id="amtd" name="amtd" placeholder="AMTD" class="form-control" />--%>                                         
                                                           </div>
                                                        </div>
                                                   </div>
                                            </div>                               
                                    
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                                
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="assetName" id="assetNameLabel" class="control-label">Asset Name</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <asp:Literal ID="ltAssetName" runat="server"></asp:Literal>
                                                                <%--<input id="assetName" name="assetName" placeholder="Asset Name" class="form-control" />--%>                                                   
                                                            </div>
                                                        </div>
                                                    </div>                                   
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="assetCost" id="assetCostLabel"class="control-label">Asset Cost (Rs.)</label>
                                                           </div>
                                                           <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                               <asp:Literal ID="ltAssetCost" runat="server"></asp:Literal>
                                                               <%-- <input id="assetCost" name="assetCost" placeholder="Asset Cost (Rs.)" class="form-control" />--%>                                                    
                                                           </div>                                            
                                                       </div>
                                                   </div>
                                            </div>
                                        </div>                                

                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                   
                                                
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                             <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="chainageStart" id="chainageStartLabel"class="control-label">Chainage Start</label>
                                                             </div>
                                                             <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                 <asp:Literal ID="ltChainageStart" runat="server"></asp:Literal>
                                                                <%--<input id="chainageStart" name="chainageStart" placeholder="Chainage Start" class="form-control" />--%>                                                   
                                                             </div>
                                                         </div>
                                                    </div>                                                                               
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="chainageEnd" id="chainageEndLabel" class="control-label">Chainage End</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <asp:Literal ID="ltChainageEnd" runat="server"></asp:Literal>
                                                                <%--<input id="chainageEnd" name="chainageEnd" placeholder="Chainage End" class="form-control" />--%>                                                    
                                                            </div>
                                                         </div>
                                                </div>
                                            </div>
                                        </div>                              

                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                   
                                                
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                                 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                        <label for="eastingUS" id="eastingUSLabel"class="control-label">Easting US</label>
                                                                 </div>
                                                                 <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                     <asp:Literal ID="ltEastingUS" runat="server"></asp:Literal>
                                                                    <%--<input id="eastingUS" name="eastingUS" placeholder="Easting US" class="form-control" />--%>                                                    
                                                                </div>
                                                            </div>
                                                         </div>                                                                            
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                    <label for="eastingDS" id="eastingDSLabel"class="control-label">Easting DS</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                    <asp:Literal ID="ltEastingDS" runat="server"></asp:Literal>
                                                                    <%--<input id="eastingDS" name="eastingDS" placeholder="Easting DS" class="form-control" />--%>                                                   
                                                                </div>
                                                           </div>
                                                    </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                             <div class="col-sm-12 col-md-12">                                   
                                                
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                             <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">                                            
                                                                    <label for="northingUS" id="northingUSLabel"class="control-label">Northing US</label>
                                                             </div>
                                                             <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                 <asp:Literal ID="ltNorthingUS" runat="server"></asp:Literal>
                                                                    <%--<input id="northingUS" name="northingUS" placeholder="Northing US" class="form-control" />--%>                                                        
                                                             </div>
                                                         </div>
                                                     </div>
                                                                                                                               
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="northingDS" id="northingDSLabel"class="control-label">Northing DS</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <asp:Literal ID="ltNorthingDS" runat="server"></asp:Literal>
                                                                <%--<input id="northingDS" name="northingDS" placeholder="Northing DS" class="form-control" />--%>                                                   
                                                            </div>
                                                         </div>
                                                      </div>
                                            </div>
                                        </div>                   

                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                   
                                                
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                             <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="initiationDate" id="initiationDateLabel"class="control-label">Initiation Date</label>
                                                             </div>
                                                             <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">                                                                
                                                                     <asp:Literal ID="ltInitiationDateOfAsset" runat="server"></asp:Literal>                                                                                                                                                                                       
                                                             </div>
                                                        </div>
                                                    </div>
                                                                                                                                    
                                                     <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                              <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="completionDate" id="completionDateLabel"class="control-label">Completion Date</label>
                                                             </div>
                                                             <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                 <%--<div id="dpCompletionDateOfAsset" class="input-group date">--%>
                                                                     <asp:Literal ID="ltCompletionDateOfAsset" runat="server"></asp:Literal>
                                                                        <%--<input type="text" name="completionDateOfAsset" class="form-control" id="completionDateOfAsset" placeholder="Date Of Completion (dd/mm/yyyy)" />
                                                                        <span class="input-group-addon form-control-static">
                                                                            <i class="fa fa-calendar fa-fw"></i>
                                                                        </span>--%>
                                                                  <%--</div>--%>                                                                                                                   
                                                             </div>
                                                         </div>
                                                </div>
                                            </div>                                    
                                        </div>                                       
                                     </div>
                                </div>
                                
                                <div class="col-xs-12 col-sm-12 col-md-12">                                   
                                      <div class="form-group" id="assetTypePanel">
                                          <asp:Literal ID="ltAssetTechnicalDetails" runat="server"></asp:Literal>
                                      </div>
                                 </div>
                                       
                                
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary" aria-hidden="true" id="updateAssetBtn">Update</button>
                                            <button class="btn btn-primary" aria-hidden="true" id="cancelAssetEditBtn">Cancel</button>
                                        </div>
                                    </div>                       
                                </div>                              
                                        
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
     
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
    <script src="CustomScripts/DBA/Asset/edit-asset.js" type="text/javascript"></script> 
</asp:Content>
