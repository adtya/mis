﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.Master" CodeBehind="FundingRepair.aspx.cs" Inherits="MIS.FundingRepair" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="CustomStyles/onm.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>Funding Repair</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server">
                            <fieldset>                             
                                <div class="col-sm-12 col-md-12">   
                                    <div class="form-group">                              
                                        <asp:GridView ID="GridView1" runat="server" Width="870px">
                                        </asp:GridView>
                                   </div>  
                                </div>
                                <div class="col-sm-12 col-md-12">   
                                    <div class="form-group">
                                           <asp:Button ID="btnAdd" runat="server" Text="Add" />
                                           <asp:Button ID="btnDelete" runat="server" Text="Delete" />
                                           <asp:Button ID="btnSave" runat="server" Text="Save" />
                                           <asp:Button ID="btnClose" runat="server" Text="Close" />
                                        </div>
                                  </div>
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
</asp:Content>