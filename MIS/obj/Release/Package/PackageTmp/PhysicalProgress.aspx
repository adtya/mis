﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PhysicalProgress.aspx.cs" Inherits="MIS.PhysicalProgress" MasterPageFile="~/Main.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="Content/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="CustomStyles/register.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
    <script src="Scripts/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="Scripts/additional-methods.min.js" type="text/javascript"></script>
    <script src="CustomScripts/my-validation-methods.js" type="text/javascript"></script>
    <script src="CustomScripts/physical-progress.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container" style="margin-top:5%;">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Progress Report</strong>
                    </div>
                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" action="#" method="post" runat="server">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="box">
                                            <!--Step 1-->
                                            <div class="step">
                                                <div class="panel panel-heading">
                                                    <h3 align="center">
                                                        <strong>General Information</strong>
                                                    </h3>
                                                </div>
                                                <div class="panel panel-body panel-default">
                                                    <div class="form-group">
                                                        <label for="districtName" class="col-sm-2 pull-left">Name of District</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="districtName" class="form-control" id="districtName" placeholder="Name of District" readonly="readonly"  />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="divisionName" class="col-sm-2 pull-left">Name of Division</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="divisionName" class="form-control" id="divisionName" placeholder="Name of Division" readonly="readonly" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="subProjectName" class="col-sm-2 pull-left">Name of SubProject</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="subProjectName" class="form-control" id="subProjectName" placeholder="Name of SubProject" readonly="readonly" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="sioName" class="col-sm-2 pull-center">Name of SIO</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" id="sioName" name="sioName">
                                                                <option value="1">Guwahati - West,WR Division</option>
                                                                <option value="2">Guwahati - East,WR Division</option>
                                                                <option value="3">Dibugarh - East,WR Division</option>
                                                                <option value="4">Dibugarh - West,WR Division</option>                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="sioJoiningDate" class="col-sm-2 pull-left">Joining Date of SIO</label>
                                                        <div class="col-sm-10">
                                                            <div class="input-group date" id="dpSIODoj">
                                                            <input type="text" name="sioJoiningDate" class="form-control" id="sioJoiningDate" placeholder="Date of Joining of SIO" readonly ="readonly" />
                                                            <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                           </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nameFremaa" class="col-sm-12 pull-left">Name and designation of the other officer engaged for FREMAA works</label>
                                                    </div>
                                                    <div id ="includeName">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-10">
                                                                <input type="text" name="nameFremaa" class="form-control" id="nameFremaa1" placeholder="Name and designation of the other officer engaged for FREMAA works" readonly="readonly"/>
                                                                <input type="text" name="nameFremaa" class="form-control" id="nameFremaa2" placeholder="Name and designation of the other officer engaged for FREMAA works" readonly="readonly"/>
                                                                <input type="text" name="nameFremaa" class="form-control" id="nameFremaa3" placeholder="Name and designation of the other officer engaged for FREMAA works" readonly="readonly"/>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a><span class="glyphicon glyphicon-plus" onclick="addPersonField();"></span></a>
                                                            </div>
                                                        </div>
                                                        <div id="includeAnotherName"></div>
                                                     </div></div>
                                                     <div class="form-group">
                                                        <label for="namePmcSiteEngineer" class="col-sm-12 pull-left">Name and designation of the PMC Site Engineer</label>
                                                     
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <input type="text" name="namePmcSiteEngineer" class="form-control" id="namePmcSiteEngineer" placeholder="Name and designation of the the PMC Site Engineer" readonly="readonly" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Step 1 ended-->

                                            <!--Step 2-->
                                            <div class="step">
                                                <div class="panel panel-heading">
                                                    <h3 align="center">
                                                        <strong>Physical Progress</strong>
                                                    </h3>
                                                </div>
                                                <div class="panel panel-body panel-default">
                                                    <div class="form-group">
                                                        <label for="packageName" class="col-sm-2 pull-left">Name of Packages</label>
                                                        <div class="col-sm-10">
                                                           <input type="text" name="packageName" class="form-control" id="packageName" placeholder="Name of Packages" readonly="readonly" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="projectValue" class="col-sm-2 pull-left">Project Value</label>
                                                        <div class="col-sm-10">
                                                             <input type="text" name="projectValue" class="form-control" id="projectValue" placeholder="Project Value" readonly="readonly" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="RefAA" class="col-sm-2 pull-left">Ref of AA</label>
                                                        <div class="col-sm-10">
                                                           <input type="text" name="RefAA" class="form-control" id="RefAA" placeholder="Ref of AA" readonly="readonly" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="RefTS" class="col-sm-2 pull-left">Ref of TS</label>
                                                        <div class="col-sm-10">
                                                             <input type="text" name="RefTS" class="form-control" id="RefTS" placeholder="Ref of TS" readonly="readonly"/>
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        <label for="contractorName" class="col-sm-2 pull-left">Name of Contractor</label>
                                                        <div class="col-sm-10">
                                                           <input type="text" name="contractorName" class="form-control" id="contractorName" placeholder="Name of Contractor" readonly="readonly" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="RefWorkOrder" class="col-sm-2 pull-left">Ref of Work Order</label>
                                                        <div class="col-sm-10">
                                                             <input type="text" name="RefWorkOrder" class="form-control" id="RefWorkOrder" placeholder="Ref of Work Order" readonly="readonly" />
                                                        </div>
                                                    </div>                                                
                                                    <div class="form-group">
                                                        <label for="startingDate" class="col-sm-2 pull-left">Date of Starting</label>
                                                        <div class="col-sm-10">
                                                            <div class="input-group date" id="dpStartingDate">
                                                                <input type="text" name="startingDate" class="form-control" id="startingDate" placeholder="Date of Starting(dd/mm/yyyy)" readonly="readonly"/>
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <div class="form-group">
                                                        <label for="completionDate" class="col-sm-2 pull-left">Completion Date</label>
                                                        <div class="col-sm-10">
                                                            <div class="input-group date" id="dpcompletionDate">
                                                                <input type="text" name="completionDate" class="form-control" id="completionDate" placeholder="Completion Date(dd/mm/yyyy)" readonly="readonly" />
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="workType" class="col-sm-2 pull-left">Type of Works</label>
                                                        <div class="col-sm-10">
                                                             <input type="text" name="workType" class="form-control" id="workType" placeholder="Type of Works" readonly="readonly" />
                                                        </div>
                                                    </div>
                                                   <div class="form-group">
                                                        <label for="workItemProgress" class="col-sm-12 pull-left">Progress of work item wise</label>                                                       
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="geoBagDumping" class="col-sm-12 pull-left">A) Geo Bag Dumping</label>                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="geoEsttQty" class="col-sm-2 pull-left">i) Estt quantity/Target</label>
                                                        <div class="col-sm-10">                                                           
                                                                <input type="text" name="geoEsttQty" class="form-control" id="geoEsttQty" placeholder="Estt qty/Target" readonly="readonly" />                                                               
                                                        </div>                                                     
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="gepDumpingReportingMonth" class="col-sm-2 pull-left">ii) Dumping during reporting month</label>
                                                        <div class="col-sm-10">                                                           
                                                                <input type="text" name="gepDumpingReportingMonth" class="form-control" id="gepDumpingReportingMonth" placeholder="Dumping during reporting month" readonly="readonly" />
                                                        </div>                                                     
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="geocumulativeDumping" class="col-sm-2 pull-left">iii) Cumulative Dumping</label>
                                                        <div class="col-sm-10">                                                           
                                                                <input type="text" name="geocumulativeDumping" class="form-control" id="Cumulative Dumping" placeholder="Cumulative Dumping" readonly="readonly" />                                                                
                                                        </div>                                                     
                                                    </div>
                                                     <div class="form-group">
                                                        <label for="geoBalance" class="col-sm-2 pull-left">iv) Balance</label>
                                                        <div class="col-sm-10">                                                           
                                                                <input type="text" name="geoBalance" class="form-control" id="geoBalance" placeholder="Balance" readonly="readonly" />                                                               
                                                        </div>                                                     
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="supplyBoulder" class="col-sm-12 pull-left">B) Supply of boulder</label>                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="supplyEsttQty" class="col-sm-2 pull-left">i) Estt quantity/Target</label>
                                                        <div class="col-sm-10">                                                           
                                                                <input type="text" name="supplyEsttQty" class="form-control" id="supplyEsttQty" placeholder="Estt qty/Target" readonly="readonly" />
                                                              
                                                        </div>                                                     
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="supplyReportingMonth" class="col-sm-2 pull-left">ii)Supply during reporting month</label>
                                                        <div class="col-sm-10">                                                           
                                                                <input type="text" name="supplyReportingMonth" class="form-control" id="supplyReportingMonth" placeholder="Supply during reporting month" readonly="readonly" />
                                                               
                                                        </div>                                                     
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="cumulativeSupply" class="col-sm-2 pull-left">iii) Cumulative Supply</label>
                                                        <div class="col-sm-10">                                                           
                                                                <input type="text" name="cumulativeSupply" class="form-control" id="cumulativeSupply Dumping" placeholder="Cumulative Supply" readonly="readonly" />                                                               
                                                        </div>                                                     
                                                    </div>
                                                     <div class="form-group">
                                                        <label for="supplyBalance" class="col-sm-2 pull-left">iv) Balance</label>
                                                        <div class="col-sm-10">                                                           
                                                                <input type="text" name="supplyBalance" class="form-control" id="supplyBalance" placeholder="Balance" readonly="readonly" />                                                             
                                                        </div>                                                     
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="pitchingBoulder" class="col-sm-12 pull-left">C) Pitching of boulder in revetment</label>                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="pitchEsttQty" class="col-sm-2 pull-left">i) Estt quantity/Target</label>
                                                        <div class="col-sm-10">                                                           
                                                                <input type="text" name="pitchEsttQty" class="form-control" id="pitchEsttQty" placeholder="Estt qty/Target" readonly="readonly" />
                                                              
                                                        </div>                                                     
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="quantityReportingMonth" class="col-sm-2 pull-left">ii)Quantity of boulder used for revetment in reporting month</label>
                                                        <div class="col-sm-10">                                                           
                                                                <input type="text" name="quantityReportingMonth" class="form-control" id="quantityReportingMonth" placeholder="Quantity of boulder used for revetment in reporting month" readonly="readonly" />                                                               
                                                        </div>                                                     
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="cumulativeqty" class="col-sm-2 pull-left">iii)Cumulative Quantity</label>
                                                        <div class="col-sm-10">                                                           
                                                                <input type="text" name="cumulativeqty" class="form-control" id="cumulativeqty" placeholder="Cumulative Qty" readonly="readonly" />                                                               
                                                        </div>                                                     
                                                    </div>
                                                     <div class="form-group">
                                                        <label for="pitchBalance" class="col-sm-2 pull-left">iv)Balance</label>
                                                        <div class="col-sm-10">                                                           
                                                                <input type="text" name="pitchBalance" class="form-control" id="pitchBalance" placeholder="Balance" readonly="readonly" />                                                             
                                                        </div>                                                     
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <!--Step 2 ended-->

                                            <!--Step 3-->
                                            <div class="step">
                                                <div class="panel panel-heading">
                                                    <h3 align="center">
                                                        <strong>Land Acquistion</strong>
                                                    </h3>
                                                </div>
                                                <div class="panel panel-body panel-default">
                                                    <div class="form-group">
                                                        <label for="totalArea" class="col-sm-6 pull-left">Total area to be acquisitoned</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="totalArea" class="form-control" id="totalArea" placeholder="Total area to be acquisitoned" readonly="readonly"></input>
                                                        </div>                                                      
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="totalFamilies" class="col-sm-6 pull-left">Total number of families to be effected</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="totalFamilies" class="form-control" id="totalFamilies" placeholder="Total number of families to be effected" readonly="readonly" />
                                                        </div>                                                      
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="compensationLA" class="col-sm-6 pull-left">Whether LA compensation has been approved</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="compensationLA" class="form-control" id="compensationLA" placeholder="Whether LA compensation has been approved" readonly="readonly"></input>
                                                        </div>                                                       
                                                    </div>                                                    
                                                    <div class="form-group">
                                                        <label for="noticeLA" class="col-sm-6 pull-left">Total number of notices to be served for LA payment</label>                                                       
                                                        <div class="col-sm-6">
                                                            <input type="text" name="noticeLA" class="form-control" id="noticeLA" placeholder="Total number of notices to be served for LA payment" readonly="readonly" />
                                                        </div>
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="noticesServed" class="col-sm-6 pull-left">Total of notices served</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="noticesServed" class="form-control" id="noticesServed" placeholder="Total of notices served" readonly="readonly"></input>
                                                        </div>                                                       
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="totalPayment" class="col-sm-2 pull-left">Total Payment</label>
                                                        <div class="col-sm-4">
                                                            <label for="madeToBeneficiary">a)Made to the beneficiary</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="madeToBeneficiary" class="form-control" id="madeToBeneficiary" placeholder="Made to the beneficiary" readonly="readonly" />
                                                        </div>                                                 
                                                   </div>
                                                    <div class="form-group">
                                                        <label for="totalPayment" class="col-sm-2 pull-left"></label>
                                                        <div class="col-sm-4">
                                                            <label for="toBeMadeToBeneficiary">b)To be made to the beneficiary</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="toBeMadeToBeneficiary" class="form-control" id="toBeMadeToBeneficiary" placeholder="To be made to the beneficiary" readonly="readonly" />
                                                        </div>                                                 
                                                   </div>                                                    
                                                    <div class="form-group">
                                                        <label for="landToSIO" class="col-sm-6 pull-left">Total area of land handed over to SIO</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="landToSIO" class="form-control" id="landToSIO" placeholder="Total area of land handed over to SIO" readonly="readonly"/>
                                                        </div>                                                      
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="expectingDate" class="col-sm-6 pull-left">Expected Date of handing over of land</label>
                                                        <div class="col-sm-6">
                                                            <div class="input-group date" id="dpExpectingDate">
                                                            <input type="text" name="expectingDate" class="form-control" id="expectingDate" placeholder="Expected Date of handing over of land" readonly="readonly" />
                                                                <span class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>                                                                                                   
                                                    </div>                                                
                                            </div>
                                            <!--Step 3 ended-->

                                            <!--Step 4-->
                                            <div class="step">
                                                <div class="panel panel-heading">
                                                    <h3 align="center">
                                                        <strong>Financial Progress</strong>
                                                    </h3>
                                                </div>
                                              <div class="panel panel-body panel-default">
                                                  <div class="form-group">
                                                      <div id="table-container" style="padding:1%;">
                                                          <table id="trainingList" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                                                              <thead>
                                                                  <tr>
                                                                      <th>No.</th>
                                                                      <th>Name of Package</th>
                                                                      <th>Total Value of Package</th>
                                                                      <th>Payment made during month</th>
                                                                      <th>Cumulative Payment</th>
                                                                      <th>Balance</th>
                                                                    </tr>
                                                                  </thead>
                                                              <tbody>
                                                                  <tr>
                                                                      <td></td>
                                                                      <td></td>
                                                                      <td></td>
                                                                      <td></td>
                                                                      <td></td>
                                                                      <td></td>
                                                                  </tr>
                                                              </tbody>
                                                            </table>
                                                        </div>
                                                  </div>
                                                    <div class="form-group">
                                                        <label for="totalContigencyFund" class="col-sm-6 pull-left">Total contigency fund received</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="totalContigencyFund" class="form-control" id="totalContigencyFund" placeholder="Total contigency fund received" readonly="readonly"></input>
                                                        </div>                                                      
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="fundUtilized" class="col-sm-6 pull-left">Fund Utilized</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="fundUtilized" class="form-control" id="fundUtilized" placeholder="Fund Utilized" readonly="readonly" />
                                                        </div>                                                      
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="balance" class="col-sm-6 pull-left">Balance</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="balance" class="form-control" id="balance" placeholder="Balance" readonly="readonly"></input>
                                                        </div> 
                                                   </div>
                                                   <div class="form-group">
                                                        <label for="billsWithSIO" class="col-sm-6 pull-left">Nos & Amount of Bills pending with SIO</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="billsWithSIO" class="form-control" id="billsWithSIO" placeholder="Nos & amount of Bills pending with SIO" readonly="readonly"></input>
                                                        </div> 
                                                   </div>
                                                   <div class="form-group">
                                                        <label for="pendingBillsFremaa" class="col-sm-6 pull-left">Nos & Amount of Bills pending in FREMAA office</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="pendingBillsFremaa" class="form-control" id="pendingBillsFremaa" placeholder="Nos & amount of Bills pending in FREMAA office" readonly="readonly"></input>
                                                        </div> 
                                                        </div>
                                                </div>
                                            </div>
                                            <!--Step 4 ended-->

                                            <!--Step 5-->
                                            <div class="step">
                                                <div class="panel panel-heading">
                                                    <h3 align="center">
                                                        <strong>Quality Control</strong>
                                                    </h3>
                                                </div>
                                              <div class="panel panel-body panel-default">                                                 
                                                    <div class="form-group">
                                                        <label for="visitDateWRD" class="col-sm-6 pull-left">Date of visit by Quality Control team of WRD</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="visitDateWRD" class="form-control" id="visitDateWRD" placeholder="Date of visit by Quality Control team of WRD" readonly="readonly"></input>
                                                        </div>                                                      
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="nameQualityControlTeam" class="col-sm-6 pull-left">Name & Designation of visiting officers of Quality Control team of WRD</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="nameQualityControlTeam" class="form-control" id="nameQualityControlTeam" placeholder="Name & Designation of visiting officers of Quality Control tean of WRD" readonly="readonly"/>
                                                        </div>                                                      
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="visitFremaa" class="col-sm-12 pull-left">Visit/ Inspection by FREMAA, PMC, ADB team and any other officers</label>
                                                        <div class="col-sm-12">
                                                           <div id="qualityControlTableContainer" style="padding:1%;">
                                                          <table id="qualityControl" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                                                              <thead>
                                                                  <tr>
                                                                      <th>Sr No.</th>
                                                                      <th>Date of Visit</th>
                                                                      <th>Name & Designation of Inspecting/Visiting Officer</th>
                                                                      <th>Details of Inspection/Visit, Action Taken and Direction Given</th>
                                                                  </tr>
                                                              </thead>
                                                              <tbody>
                                                                  <tr>
                                                                      <td></td>
                                                                      <td></td>
                                                                      <td></td>
                                                                      <td></td>
                                                                      
                                                                  </tr>
                                                              </tbody>
                                                            </table>
                                                        </div>
                                                        </div> 
                                                   </div>
                                                   <div class="form-group">
                                                        <label for="recordDetails" class="col-sm-12 pull-left">Details of Records maintained:</label>                                                       
                                                   </div>
                                                       <div class="form-group">
                                                             <label for="" class="col-sm-2 pull-left"></label>
                                                            <label for="whetherWeeklyMeeting" class="col-sm-8 pull-left">a) Whether weekly meeting held(Yes/No):</label>
                                                            <div class="col-sm-2">
                                                                <input type="text" name="whetherWeeklyMeeting" class="form-control" id="whetherWeeklyMeeting" placeholder="Yes/No" readonly="readonly"/>
                                                            </div> 
                                                      </div>
                                                         <div class="form-group">
                                                             <label for="" class="col-sm-2 pull-left"></label>
                                                            <label for="whetherminuteMeeting" class="col-sm-8 pull-left">b) Whether minutes of the meeting is maintained(Yes/No):</label>
                                                            <div class="col-sm-2">
                                                                <input type="text" name="whetherminuteMeeting" class="form-control" id="whetherminuteMeeting" placeholder="Yes/No" readonly="readonly" />
                                                            </div> 
                                                      </div>
                                                         <div class="form-group">
                                                             <label for="" class="col-sm-2 pull-left"></label>
                                                            <label for="whetherSiteRegister" class="col-sm-8 pull-left">c) Whether Site Register is being signed regularly by the offciers of SIO, PMC & Contractor(Yes/No):</label>
                                                            <div class="col-sm-2">
                                                                <input type="text" name="whetherSiteRegister" class="form-control" id="whetherSiteRegister" placeholder="Yes/No" readonly="readonly" />
                                                            </div> 
                                                      </div>
                                                      <div class="form-group">
                                                             <label for="" class="col-sm-2 pull-left"></label>
                                                            <label for="whetherDiverRegister" class="col-sm-8 pull-left">d) Whether Diver's register maintained(Yes/No):</label>
                                                            <div class="col-sm-2">
                                                                <input type="text" name="whetherDiverRegister" class="form-control" id="whetherDiverRegister" placeholder="Yes/No" readonly="readonly" />
                                                            </div> 
                                                      </div>
                                                         <div class="form-group">
                                                             <label for="" class="col-sm-2 pull-left"></label>
                                                            <label for="whetherLabTest" class="col-sm-8 pull-left">e) Whether Lab test is being carried out(Yes/No):</label>
                                                            <div class="col-sm-2">
                                                                <input type="text" name="whetherLabTest" class="form-control" id="whetherLabTest" placeholder="Yes/No" readonly="readonly" />
                                                            </div> 
                                                        </div>
                                                        <div class="form-group">
                                                             <label for="" class="col-sm-2 pull-left"></label>                                                          
                                                            <div class="col-sm-10">
                                                                <input type="text" name="whetherLabTest" class="form-control" id="whetherLabTestDetails" placeholder="Give Details of the test done of ongoing works" hidden="hidden"></input>
                                                            </div> 
                                                          </div>
                                                         <div class="form-group">
                                                             <label for="" class="col-sm-2 pull-left"></label>
                                                            <label for="whethervideoRecordig" class="col-sm-8 pull-left">f) Whether video recording is being done (Yes/No):</label>
                                                            <div class="col-sm-2">
                                                                <input type="text" name="whethervideoRecordig" class="form-control" id="whethervideoRecordig" placeholder="Yes/No" readonly="readonly" />
                                                            </div> 
                                                      </div>
                                                    
                                                         <div class="form-group">
                                                             <label for="" class="col-sm-2 pull-left"></label>
                                                            <label for="whetherWorkPhotos" class="col-sm-8 pull-left">g) Whether Photographs of work are taken regularly (Yes/No):</label>
                                                            <div class="col-sm-2">
                                                                <input type="text" name="whetherWorkPhotos" class="form-control" id="whetherWorkPhotos" placeholder="Yes/No" readonly="readonly" />
                                                            </div> 
                                                      </div>
                                                     
                                                      <div class="form-group">
                                                        <label for="" class="col-sm-6 pull-left">Upload Quality Control Report</label>
                                                        <div class="col-sm-4">
                                                              <input type="text" name="uploadReport" class="form-control" id="uploadReport" readonly="readonly" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                                 <button type="button">Upload</button>
                                                        </div>                                                      
                                                    </div> 
                                                     
                                                </div>
                                            </div>
                                            <!--Step 5 ended-->

                                            <!--Buttons-->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="pull-right">
                                                        <button type="button" class="action btn-sky text-capitalize back btn">Back</button>
                                                        <button type="button" class="action btn-sky text-capitalize next btn">Next</button>    
                                                        <button type="button" class="action btn-hot text-capitalize submit btn">Save</button>                                                    
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Buttons ended-->

                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <div id="step-display" class="panel-footer"></div>
                </div>
            </div>
        </div>

        <%--<div class="modal fade" id="userExistModal" tabindex="-1" role="dialog" aria-labelledby="userExistModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="userExistModalLabel">Warning</h4>
                    </div>
                    <div class="modal-body">
                        <p>You have entered the email id1 as:</p>
                        <p id="userEmailId"></p>
                        <p>The user with the above email id already exists in our database</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                        
                    </div>
                </div>
            </div>
        </div>--%>

    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
    <script src="CustomScripts/register.js" type="text/javascript"></script>

    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) { i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () { (i[r].q = i[r].q || []).push(arguments) }, i[r].l = 1 * new Date(); a = s.createElement(o), m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m) })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga'); ga('create', 'UA-19096935-1', 'auto'); ga('send', 'pageview');
    </script>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-43091346-1', 'devzone.co.in');
        ga('send', 'pageview');
    </script>
</asp:Content>

