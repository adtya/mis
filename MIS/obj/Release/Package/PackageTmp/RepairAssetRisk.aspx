﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup ="true" CodeBehind="RepairAssetRisk.aspx.cs" Inherits="MIS.RepairAssetRisk" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="CustomStyles/onm.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>Repair Asset Risk</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server">
                            <fieldset>                             
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <asp:GridView ID="GridView1" runat="server" Width="870px">
                                          
                                        </asp:GridView>

                                    </div>                       
                                </div>
                                <div class="col-sm-12 col-md-12">   
                                    <div class="form-group">
                                        <asp:Button ID="btnPrint" runat="server" Text="Print" />
                                         <asp:Button ID="btnSave" runat="server" Text="Save" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" />
                                    </div>
                                </div>
                                
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
</asp:Content>