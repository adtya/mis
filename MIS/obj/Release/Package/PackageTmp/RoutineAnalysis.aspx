﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RoutineAnalysis.aspx.cs" Inherits="MIS.RoutineAnalysis" MasterPageFile="~/DBA.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
    <script src="Scripts/jquery.glob.js" type="text/javascript"></script>
    <script src="Scripts/globinfo/jQuery.glob.en-IN.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.GlobalMoneyInput2.js" type="text/javascript"></script>
        
    <script src="Scripts/jquery.Guid.js" type="text/javascript"></script>

    <script src="CustomScripts/routine-analysis-details.js" type="text/javascript"></script> 

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>Routine Analysis Details</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server" id="routineAnalysisDetailsForm" name="routineAnalysisDetailsForm">
                           <fieldset>
                               <div class="col-sm-12 col-md-12">
                                   <div>
                                       <fieldset class="scheduler-border">
                                           <legend class="scheduler-border"></legend>

                                           <div class="row">
                                               <div class="col-sm-12 col-md-12">
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                <label for="routineWorkItemList" id="routineWorkItemListLabel" class="control-label">Routine Work Item</label>
                                                            </div>                                                            
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="col-sm-6 col-md-6">
                                                      <div class="form-group">
                                                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Literal ID="ltRoutineWorkItemList" runat="server"></asp:Literal>
                                                           </div>
                                                      </div>
                                                  </div>  
                                               </div>
                                           </div>  

                                           <div class="row">
                                               <div class="col-sm-12 col-md-12">                                                                               
                                                  <div class="col-sm-6 col-md-6">
                                                   <div class="form-group">
                                                       <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">                                                      
                                                          <label for="workItemShortDescription" id="workItemShortDescriptionLabel" class="control-label">Short & Detailed Work Item Description</label>
                                                       </div>                                     
                                                   </div>

                                                  </div>

                                                  <div class="col-sm-6 col-md-6">
                                                      <div class="form-group">
                                                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                              <input id="workItemShortDescription" name="workItemShortDescription" placeholder="Short Description" class="form-control" readonly="readonly" />

                                                          </div>
                                                      </div>
                                                  </div>
                                               </div>
                                           </div>
                                                                                      
                                           <div class="row">
                                               <div class="col-sm-12 col-md-12">
                                                   <div class="col-sm-12 col-md-12">                                                  
                                                       <div class="form-group">
                                                           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                               <textarea id="workItemLongDescription" name="workItemLongDescription" placeholder="Long Description" class="form-control" readonly="readonly"></textarea>
                                                           </div>
                                                       </div>
                                                   </div>                                                 
                                               </div>
                                           </div> 

                                           <div class="row">
                                               <div class="col-sm-12 col-md-12">
                                                   <div class="col-sm-6 col-md-6">
                                                       <div class="form-group">
                                                           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                               <label for="assetQuantityParameterListForRoutineAnalysis" id="assetQuantityParameterListForRoutineAnalysisLabel" class="control-label">Asset Quantity Parameter</label>
                                                           </div>                                                           
                                                       </div>
                                                   </div>

                                                   <div class="col-sm-6 col-md-6">
                                                       <div class="form-group">
                                                           <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                               <select class="form-control multiselect" name="assetQuantityParameterListForRoutineAnalysis" id="assetQuantityParameterListForRoutineAnalysis" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">
                                                                    <option value="">Select Asset Quantity Parameter</option>
                                                               </select>
                                                           </div>                                                           
                                                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                               <input id="unitForRoutineAnalysis" name="unitForRoutineAnalysis" placeholder="Unit" class="form-control" readonly="readonly" />
                                                           </div>
                                                       </div>
                                                   </div>

                                                </div>

                                               </div>                                         

                                       </fieldset>
                                                                         
                                       <fieldset class="scheduler-border">
                                           <legend class="scheduler-border">Unit Rate, by FY & Circle</legend>

                                           <div class="row">
                                               <div class="col-sm-12 col-md-12">
                                                    <div class="col-sm-6 col-md-6">
                                                       <div class="form-group">
                                                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                               <label for="circleNameForRoutineAnalysis" id="circleNameForRoutineAnalysisLabel" class="control-label">Circle :</label>
                                                           </div>
                                                           <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                               <input id="circleNameGuidForRoutineAnalysis" name="circleNameGuidForRoutineAnalysis" placeholder="Circle Name Guid" class="form-control" style="display:none" />
                                                               <label for="circleNameForRoutineAnalysis" id="circleNameForRoutineAnalysis" class="control-label"></label>
                                                           </div>
                                                       </div>
                                                   </div>   
                                               </div>
                                           </div>
                                               <div class="row">
                                               <div class="col-sm-12 col-md-12">
                                                  <div class="col-sm-6 col-md-6">
                                                   <div class="form-group">
                                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                           <label for="fiscalYearForRoutineAnalysis" id="fiscalYearForRoutineAnalysis" class="control-label">Fiscal Year :</label>
                                                       </div>
                                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                           <label for="fiscalYearForRoutineAnalysis" id="fiscalYearForRoutineAnalysisText" class="control-label"></label>
                                                       </div>
                                                   </div>
                                                  </div> 
                                               </div>
                                           </div>    
                                           <div class="row">
                                               <div class="col-sm-12 col-md-12">
                                                    <div class="col-sm-6 col-md-6">
                                                       <div class="form-group">
                                                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                               <label for="workItemRateForRoutineAnalysis" id="workItemRateForRoutineAnalysisLabel" class="control-label">Rate (Rs) :</label>
                                                           </div>
                                                           <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                               <input id="workItemRateGuidForRoutineAnalysis" name="workItemRateGuidForRoutineAnalysis" placeholder="Unit Rate Guid" class="form-control" style="display:none" />
                                                               <input id="workItemRateForRoutineAnalysis" name="workItemRateForRoutineAnalysis" placeholder="Unit Rate" class="form-control" />
                                                           </div>
                                                       </div>
                                                   </div>   
                                               </div>
                                         </div>                                 

                                       </fieldset>
                                   
                                       <fieldset class="scheduler-border" id="serviceLevelByAssetTypeFieldset" name="serviceLevelByAssetTypeFieldset" hidden="hidden">
                                           <legend class="scheduler-border" >Service Level, by Asset Type</legend>                                 
                                                <div class="form-group" id="assetTypeBasedOnRoutineWorkItemsPanel"></div>                                                                               
                                       </fieldset>                             
                                                             
                                       <fieldset class="scheduler-border" id="routineOMQuantitiesFieldset" name="routineOMQuantitiesFieldset" hidden="hidden">
                                           <legend class="scheduler-border">Routine O&M Quantitites</legend>

                                           <div class="col-sm-12 col-md-12">

                                               <div class="row">
                                                   <div class="col-sm-12 col-md-12">
                                                       <div class="col-sm-6 col-md-6">
                                                           <div class="form-group">
                                                               <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                   <label for="assetCodeListForRoutineWorkItem" id="assetCodeListForRoutineWorkItemLabel" class="control-label">Asset Code</label>
                                                               </div>
                                                               <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                   <select class="form-control multiselect" name="assetCodeListForRoutineWorkItem" id="assetCodeListForRoutineWorkItem" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">
                                                                     <option value="">-- Select Asset Code --</option>
                                                                   </select>
                                                               </div>                                                               
                                                         </div>
                                                       </div>
                                                       
                                                   </div>
                                               </div>

                                               <div class="row">
                                                   <div class="col-sm-12 col-md-12">
                                                       <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                               <label for="assetTypeForRoutineAnalysis" id="assetTypeForRoutineAnalysisLabel" class="control-label">Asset Type</label>
                                                           </div>                                                           
                                                           <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                               <input id="assetTypeForRoutineAnalysis" name="assetTypeForRoutineAnalysis" placeholder="Asset Type" class="form-control" readonly="readonly" />
                                                           </div>
                                                         </div>
                                                     </div>

                                                       <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                               <label for="assetCodeForRoutineAnalysis" id="assetCodeForRoutineAnalysisLabel" class="control-label">Asset Code</label>
                                                           </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                               <input id="assetCodeForRoutineAnalysis" name="assetCodeForRoutineAnalysis" placeholder="Asset Code" class="form-control" readonly="readonly" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                                                                              
                                                   </div>
                                               </div>

                                               <div class="row">
                                                   <div class="col-sm-12 col-md-12">
                                                       <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="assetNameForRoutineAnalysis" id="assetNameForRoutineAnalysisLabel" class="control-label">Asset Name</label>
                                                           </div>                                                           
                                                           <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                               <input id="assetNameForRoutineAnalysis" name="assetNameForRoutineAnalysis" placeholder="Asset Name" class="form-control" readonly="readonly" />
                                                           </div>
                                                         </div>
                                                     </div>

                                                       <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                               <label for="assetRiskRatingForRoutineAnalysis" id="assetRiskRatingForRoutineAnalysisLabel" class="control-label">Asset Risk Rating</label>
                                                           </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                               <input id="assetRiskRatingForRoutineAnalysis" name="assetRiskRatingForRoutineAnalysis" placeholder="Asset Risk Rating" class="form-control" readonly="readonly" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                   </div>

                                               </div>

                                               <fieldset class="scheduler-border">
                                                   <legend class="scheduler-border">Work Item Calculations</legend>

                                                   <div class="row">
                                                       <div class="col-sm-12 col-md-12">
                                                           <div class="col-sm-6 col-md-6">
                                                               <div class="form-group">
                                                                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                       <label for="serviceLevelForRoutineAnalysis" id="serviceLevelForRoutineAnalysisLabel" class="control-label">Service Level</label>
                                                                   </div>                                                               
                                                                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                       <input id="serviceLevelForRoutineAnalysis" name="serviceLevelForRoutineAnalysis" placeholder="Service Level" class="form-control" readonly="readonly" />
                                                                   </div>
                                                               </div>
                                                     </div>

                                                           <div class="col-sm-6 col-md-6">
                                                               <div class="form-group">
                                                                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                       <label for="assetQuantityForRoutineAnalysis" id="assetQuantityForRoutineAnalysisLabel" class="control-label">Asset Quantity</label>

                                                                   </div>
                                                                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                       <input id="routineAnalysisGuid" name="routineAnalysisGuid" placeholder="Guid" class="form-control" style="display:none" />
                                                                       <input id="assetQuantityForRoutineAnalysis" name="assetQuantityForRoutineAnalysis" placeholder="Asset Quantity" class="form-control" readonly="readonly" />
                                                                   </div>
                                                                </div>
                                                            </div>                                                           
                                                       </div>
                                                   </div>
                                                   
                                                   <div class="row">
                                                       <div class="col-sm-12 col-md-12">
                                                           <div class="col-sm-6 col-md-6">
                                                               <div class="form-group">
                                                                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                       <label for="intensityFactorForRoutineAnalysis" id="intensityFactorForRoutineAnalysisLabel" class="control-label">Intensiy Factor</label>
                                                                   </div>
                                                                                                 
                                                                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                       <input id="intensityFactorGuidForRoutineAnalysis" name="intensityFactorGuidForRoutineAnalysis" placeholder="Intensiy Factor" class="form-control" style="display:none" />
                                                                       <input id="intensityFactorForRoutineAnalysis" name="intensityFactorForRoutineAnalysis" placeholder="Intensiy Factor" class="form-control" />
                                                                   </div>
                                                               </div>
                                                           </div>

                                                           <div class="col-sm-6 col-md-6">
                                                               <div class="form-group">
                                                                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                       <label for="unitRateForRoutineAnalysis" id="unitRateForRoutineAnalysisLabel" class="control-label">Unit Rate</label>
                                                                   </div>
                                                                                                 
                                                                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                       <input id="unitRateForRoutineAnalysis" name="unitRateForRoutineAnalysis" placeholder="Unit Rate" class="form-control" readonly="readonly"/>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                      </div>
                                                   </div>

                                                   <div class="row">
                                                       <div class="col-sm-12 col-md-12">
                                                           <div class="col-sm-6 col-md-6">
                                                               <div class="form-group">
                                                                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                       <label for="omRoutineCost" id="omRoutineCostLabel" class="control-label">O & M Routine Cost</label>
                                                                   </div>
                                                                                                 
                                                                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                       <input id="omRoutineCost" name="omRoutineCost" placeholder="O & M Routine Cost" class="form-control" readonly="readonly" />
                                                                   </div>
                                                               </div>
                                                           </div>
                                                      </div>
                                               </div>
                                               
                                                   <div class="row">
                                                       <div class="col-sm-12 col-md-12">
                                                           <div class="col-sm-3 col-md-3">
                                                               <div class="form-group">
                                                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                       <label for="intensityFactorJustificationForRoutineAnalysis" id="intensityFactorJustificationForRoutineAnalysisLabel" class="control-label text-left">Intensity Factor Justification</label>
                                                                   </div>
                                                               </div>
                                                            </div>
                                                           <div class="col-sm-9 col-md-9">
                                                               <div class="form-group">
                                                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <textarea id="intensityFactorJustificationForRoutineAnalysis" rows="1" cols="2" name="intensityFactorJustificationForRoutineAnalysis" class="form-control"></textarea>                                                                  
                                                                   </div>
                                                               </div>
                                                           </div> 
                                                                                                                  
                                                               
                                                      </div>
                                                   </div>                                             
                                                                                                  
                                               </fieldset>

                                               <fieldset class="scheduler-border">
                                                   <legend class="scheduler-border">Funding Details</legend>
                                                   <div class="col-sm-12 col-md-12">

                                                       <div class="row">
                                                           <div class="col-sm-12 col-md-12">
                                                               <div class="col-sm-6 col-md-6">
                                                                   <div class="form-group">
                                                                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                           <label for="fundingSourceList" id="fundingSourceListLabel" class="control-label">Funding Source</label>
                                                                       </div>
                                                                   
                                                                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                           <select class="form-control multiselect" name="fundingSourceList" id="fundingSourceList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)"></select>
                                                                       </div>
                                                                   </div>
                                                               </div>

                                                               <div class="col-sm-6 col-md-6">
                                                                   <div class="form-group">
                                                                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                           <label for="officerIncharge" id="officerInchargeLabel" class="control-label">Officer Incharge</label>
                                                                       </div>
                                                                   
                                                                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                           <input id="officerIncharge" name="officerIncharge" placeholder="Officer Incharge" class="form-control" />
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                       </div>                                                     

                                                       <div class="row">
                                                           <div class="col-sm-12 col-md-12">
                                                               <div class="col-sm-6 col-md-6">
                                                                   <div class="form-group">
                                                                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                           <label for="workStatusList" id="workStatusListLabel" class="control-label">Work Status</label>
                                                                       </div>
                                                                   
                                                                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                           <select class="form-control multiselect" name="workStatusList" id="workStatusList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)"></select>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                       </div>

                                                   </div>
                                               </fieldset>

                                           </div>

                                       </fieldset>
                                   </div>
                               </div>

                               <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary" aria-hidden="true" id="saveRoutineAnalysisBtn">Save</button>
                                            <button class="btn btn-primary" id="cancelRoutineAnalysisBtn" aria-hidden="true">Cancel</button>

                                        </div>                                                               
                                    </div>
                                </div>
                                          
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

   
          
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
</asp:Content>
