﻿<%@ Page Title="Projects" Language="C#" MasterPageFile="~/SIO.Master" AutoEventWireup="true" CodeBehind="SIOProjects.aspx.cs" Inherits="MIS.SIOProjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    <link href="Content/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="CustomStyles/SIO/sio-projects.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
    <script src="Scripts/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="Scripts/Datejs/build/date.js" type="text/javascript"></script>
    <script src="Scripts/date.format.js" type="text/javascript"></script>
    <script src="CustomScripts/my-bootstrap-datepicker-methods.js" type="text/javascript"></script>

    <script src="CustomScripts/bool-parser.js" type="text/javascript"></script>

    <script src="Scripts/numbers.min.js" type="text/javascript"></script>

    <script src="Scripts/highcharts/4.2.0/highcharts.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <asp:Literal runat="server" ID="subChainagesList" />

    <div id="progressMonthPickerPopupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="overflow-y:initial !important; max-width:calc(100vw - 100px); min-width:calc(100vw - 100px);">
            <div class="modal-content">

                <%--<div class="modal-header" style="display:none"></div>--%>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h1 class="text-center">Pick Month</h1>
                </div>

                <div class="modal-body" style="max-height:calc(100vh - 200px); overflow-y: auto;">

                </div>

                <div class="modal-footer">
                    <div class="col-md-12">
                        <button class="btn disabled" aria-hidden="true" id="proceedBtn">Proceed</button>
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>

                        <%--<button id="backToUnitTypeBtn" name="backToUnitTypeBtn" class="btn" aria-hidden="true">Back</button>--%>
                        <%--<button id="unitModalCloseBtn" name="unitModalCloseBtn" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>--%>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="monthlyProgressPopupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="overflow-y:initial !important; max-width:calc(100vw - 100px); min-width:calc(100vw - 100px);">
            <div class="modal-content">

                <div class="modal-header">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--%>
                    <%--<h1 class="text-center">Add Progress</h1>--%>
                </div>

                <div class="modal-body" style="max-height:calc(100vh - 250px); overflow-y: auto;">
                    
                </div>

                <div class="modal-footer">
                    <div class="col-md-12">
                        <button id="backFromMonthlyProgressPopupModalBtn" name="backFromMonthlyProgressPopupModalBtn" class="btn btn-default" aria-hidden="true">Back</button>
                        <%--<button id="backFromEditMonthlyProgressPopupModalBtn" name="backFromEditMonthlyProgressPopupModalBtn" class="btn" aria-hidden="true">Back</button>--%>
                        <%--<button class="submit btn btn-success" aria-hidden="true" id="saveProgressBtn" disabled="disabled">Add Progress</button>--%>
                        <%--<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>--%>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <%--<div id="addMonthlyProgressPopupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="overflow-y:initial !important; max-width:calc(100vw - 100px); min-width:calc(100vw - 100px);">
            <div class="modal-content">

                <div class="modal-header">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--%
                    <%--<h1 class="text-center">Add Progress</h1>--%
                </div>

                <div class="modal-body" style="max-height:calc(100vh - 200px); overflow-y: auto;">
                    
                </div>

                <div class="modal-footer">
                    <div class="col-md-12">
                        <button id="backFromAddMonthlyProgressPopupModalBtn" name="backFromAddMonthlyProgressPopupModalBtn" class="btn" aria-hidden="true">Back</button>
                        <%--<button class="submit btn btn-success" aria-hidden="true" id="saveProgressBtn" disabled="disabled">Add Progress</button>--%
                        <%--<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>--%
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="viewMonthlyProgressPopupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="overflow-y:initial !important; max-width:calc(100vw - 100px); min-width:calc(100vw - 100px);">
            <div class="modal-content">

                <div class="modal-header">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--%
                    <%--<h1 class="text-center">View Progress</h1>--%
                </div>

                <div class="modal-body" style="max-height:calc(100vh - 200px); overflow-y: auto;">
                    
                </div>

                <div class="modal-footer">
                    <div class="col-md-12">
                        <button id="backFromViewMonthlyProgressPopupModalBtn" name="backFromViewMonthlyProgressPopupModalBtn" class="btn" aria-hidden="true">Back</button>
                        <%--<button class="submit btn btn-success" aria-hidden="true" id="saveProgressBtn" disabled="disabled">Save Progress</button>--%
                        <%--<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>--%
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="editMonthlyProgressPopupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="overflow-y:initial !important; max-width:calc(100vw - 100px); min-width:calc(100vw - 100px);">
            <div class="modal-content">

                <div class="modal-header">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--%
                    <%--<h1 class="text-center">Add Progress</h1>--%
                </div>

                <div class="modal-body" style="max-height:calc(100vh - 200px); overflow-y: auto;">
                    
                </div>

                <div class="modal-footer">
                    <div class="col-md-12">
                        <button id="backFromEditMonthlyProgressPopupModalBtn" name="backFromEditMonthlyProgressPopupModalBtn" class="btn" aria-hidden="true">Back</button>
                        <%--<button class="submit btn btn-success" aria-hidden="true" id="saveProgressBtn" disabled="disabled">Add Progress</button>--%
                        <%--<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>--%
                    </div>
                </div>

            </div>
        </div>
    </div>--%>

    <div id="progressChartPopupModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="overflow-y:initial !important; max-width:calc(100vw - 100px); min-width:calc(100vw - 100px);">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Assortment Analysis</h4>
                </div>

                <div class="modal-body" style="max-height:calc(100vh - 250px); overflow-y: auto;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <%--<div id="container"></div>--%>
                            <select class="form-control multiselect" name="subChainageWorkItemList" id="subChainageWorkItemList" onfocus="sioFocusFunction(this.id)" onblur="sioBlurFunction(this.id)">

                            </select>
                            <div id="progressChartContainer" class="hidden" style="width:100%; height:100%; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
   <script src="CustomScripts/SIO/sio-projects.js" type="text/javascript"></script>
</asp:Content>