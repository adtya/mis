﻿$.fn.datepicker.Constructor.prototype.fillMonths = function () {
    var localDate = this._utc_to_local(this.viewDate);
    var html = '',
    i = 0;
    while (i < 12) {
        //var focused = localDate && localDate.getMonth() === i ? ' focused' : '';
        var focused = localDate && localDate.getMonth() === i ? '' : '';
        html += '<span class="month' + focused + '">' + $.fn.datepicker.dates[this.o.language].monthsShort[i++] + '</span>';
    }
    this.picker.find('.datepicker-months td').html(html);
};