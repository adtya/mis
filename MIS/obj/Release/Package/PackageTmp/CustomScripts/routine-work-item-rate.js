﻿var routineWorkItemGuid;
var fiscalYear;
var circleGuid;
var circleCode;
var systemVariableData;

$(document).ready(function () {

    $("#addRoutineWorkItemRateBtn").on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetRoutineWorkItemRateData",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                getSystemVariablesData();
                createRoutineWorkItemRateForm(response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });        
        e.stopPropagation();
    });

});

createRoutineWorkItemRateForm = function (routineWorkItemRateDataForWorkItemRate) {    

    $("#routineWorkItemRatePopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/view-data.png" class="login" height="70" />';
    html += '<h2 class="text-center">Routine Work Item Rate</h2>';   
           
     html += '<h3 class="text-center">' + "Fiscal Year" + ":" + systemVariableData.FiscalYear + '</h3>';
     html += '<h3 class="text-center">' + "Circle" + ":" + systemVariableData.CircleManager.CircleCode + '</h3>';

     html += '<input id="systemFiscalYear" name="systemFiscalYear" value = "' + systemVariableData.FiscalYear + '" hidden="hidden"/>';
    html += '<input id="circleGuid" name="circleGuid" value = "' + systemVariableData.CircleManager.CircleGuid + '" hidden="hidden"/>';
   
    html += '<div class="panel-body">';
    html += '<form id="routineWorkItemRatePopupForm" name="routineWorkItemRatePopupForm" role="form" class="form form-horizontal" method="post">';
      
    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    if (routineWorkItemRateDataForWorkItemRate.length > 0) {
        
        html += '<div class="table-responsive">';
        html += '<table id="routineWorkItemRateDataList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';
        html += '<thead>';
        html += '<tr>';
        html += '<th>' + '#' + '</th>';
        html += '<th>' + 'Routine Work Item Guid' + '</th>';
        html += '<th>' + 'Work Type Code' + '</th>';
        html += '<th>' + 'Work Item Class' + '</th>';
        html += '<th>' + 'Work Item Description' + '</th>';
        html += '<th>' + 'Unit' + '</th>';
        html += '<th>' + 'Routine Work Item Rate Guid' + '</th>';
        html += '<th>' + 'Work Item Rate' + '</th>';
        html += '<th>' + 'Operation' + '</th>';
        html += '</tr>';
        html += '</thead>';

        html += '<tbody>';
        for (var i = 0; i < routineWorkItemRateDataForWorkItemRate.length; i++) {                 
            
            html += '<tr>';
            html += '<td>' + '' + '</td>';
            html += '<td id="routineWorkItemRowGuid">' + routineWorkItemRateDataForWorkItemRate[i].RoutineWorkItemGuid + '</td>';
            html += '<td>' + routineWorkItemRateDataForWorkItemRate[i].RoutineWorkItemTypeCode + '</td>';
            html += '<td>' + routineWorkItemRateDataForWorkItemRate[i].RoutineWorkItemClassManager.RoutineWorkItemClassName + '</td>';
            html += '<td>' + routineWorkItemRateDataForWorkItemRate[i].ShortDescription + '</td>';
            html += '<td>' + routineWorkItemRateDataForWorkItemRate[i].AssetQuantityParameterManager.UnitManager.UnitName + '</td>';
            html += '<td id="routineWorkItemRateRowGuid">' + routineWorkItemRateDataForWorkItemRate[i].RoutineWorkItemRateManagerList[0].RoutineWorkItemRateGuid + '</td>';
            if (routineWorkItemRateDataForWorkItemRate[i].RoutineWorkItemRateManagerList[0].RoutineWorkItemRate.Amount === 0)
            {
                html += '<td>' + '' + '</td>';
            }
            else
            {
                html += '<td>' + routineWorkItemRateDataForWorkItemRate[i].RoutineWorkItemRateManagerList[0].RoutineWorkItemRate.Amount + '</td>';
            }
            
            html += '<td>' + '<a href="#" class="editRoutineWorkItemRateRowBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '</td>';
            html += '</tr>';
        }

    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';
    
    html += '</div>';
    html += '</div>';

    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#routineWorkItemRatePopupModal .modal-body").append(html);

    $('#routineWorkItemRatePopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });
    routineWorkItemRateTableRelatedFunctions();

    $('#routineWorkItemRatePopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            routineWorkItemRate: {
                required: true
            }
        },
        messages: {
            routineWorkItemRate: {
                required: "Please Enter Work Item Rate."
            }
        },

    });
}

routineWorkItemRateTableRelatedFunctions = function () {
    var routineWorkItemRateRowEditing = null;

    var routineWorkItemRateTable = $('#routineWorkItemRateDataList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [6],
                visible: false,
                searchable: false
            }
        ]
    });

    routineWorkItemRateTable.on('order.dt search.dt', function () {
        routineWorkItemRateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $('#routineWorkItemRatePopupModal').on('click', '#routineWorkItemRateDataList a.editRoutineWorkItemRateRowBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (routineWorkItemRateRowEditing !== null && routineWorkItemRateRowEditing != selectedRow) {

            /* A different row is being edited - the edit should be cancelled and this row edited */

            restoreRoutineWorkItemRateRowData(routineWorkItemRateTable, routineWorkItemRateRowEditing);
            editRoutineWorkItemRateRow(routineWorkItemRateTable, selectedRow);
            routineWorkItemRateRowEditing = selectedRow;

            var el = $('#routineWorkItemRatePopupModal input#routineWorkItemRate');
            var elemLen = el.val().length;
            el.focus();


            el.setCursorPosition(elemLen);
        }
        else {
            /* No row currently being edited */
            editRoutineWorkItemRateRow(routineWorkItemRateTable, selectedRow);
            routineWorkItemRateRowEditing = selectedRow;

            var el = $('#routineWorkItemRatePopupModal input#routineWorkItemRate');
            var elemLen = el.val().length;
            el.focus();

            el.setCursorPosition(elemLen);
        }
    });

    $('#routineWorkItemRatePopupModal').on('click', '#routineWorkItemRateDataList a.updateRoutineWorkItemRateRowBtn', function (e) {
        e.preventDefault();

        if ($("#routineWorkItemRatePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            var guid = routineWorkItemRateTable.row(selectedRow).data()[6];
            if (guid == "00000000-0000-0000-0000-000000000000")
            {
                routineWorkItemGuid = routineWorkItemRateTable.row(selectedRow).data()[1];

                var routineWorkItemRateSavedGuid = saveRoutineWorkItemRateData();

                if (jQuery.Guid.IsValid(routineWorkItemRateSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(routineWorkItemRateSavedGuid.toUpperCase())) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your Data is saved successfully.", function () {
                        if (routineWorkItemRateRowEditing !== null && routineWorkItemRateRowEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            saveNewRoutineWorkItemRateRow(routineWorkItemRateTable, routineWorkItemRateRowEditing, routineWorkItemRateSavedGuid);
                            routineWorkItemRateRowEditing = null;
                        }
                    });
                }
                else {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }
           
            }
            else
            {
                var routineWorkItemRateUpdated = updateRoutineWorkItemRateData(routineWorkItemRateTable.row(selectedRow).data()[6]);

                if (routineWorkItemRateUpdated == 1) {

                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your Data is updated successfully.", function () {
                        if (routineWorkItemRateRowEditing !== null && routineWorkItemRateRowEditing == selectedRow) {

                            /* A different row is being edited - the edit should be cancelled and this row edited */

                            updateRoutineWorkItemRateRow(routineWorkItemRateTable, routineWorkItemRateRowEditing);
                            routineWorkItemRateRowEditing = null;
                        }
                    });
                } else if (routineWorkItemRateUpdated == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
                }
            }           
        }

    });

    $('#routineWorkItemRatePopupModal').on('click', '#routineWorkItemRateDataList a.cancelRoutineWorkItemRateRowBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (routineWorkItemRateRowEditing !== null && routineWorkItemRateRowEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */

            restoreRoutineWorkItemRateRowData(routineWorkItemRateTable, routineWorkItemRateRowEditing);
            routineWorkItemRateRowEditing = null;
        }

    });
}

editRoutineWorkItemRateRow = function (routineWorkItemRateTable, selectedRow) {
    var selectedRowData = routineWorkItemRateTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.

    availableTds[5].innerHTML = '<input type="text" id="routineWorkItemRate" name="routineWorkItemRate" placeholder="Routine Work Item Rate" class="form-control text-capitalize" value="' + selectedRowData[7] + '">';
    availableTds[6].innerHTML = '<a href="#" class="updateRoutineWorkItemRateRowBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelRoutineWorkItemRateRowBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

saveNewRoutineWorkItemRateRow = function (routineWorkItemRateTable, selectedRow, routineWorkItemRateSavedGuid) {

    var selectedRowData = routineWorkItemRateTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    selectedRowData[6] = routineWorkItemRateSavedGuid;
    selectedRowData[7] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
   
    routineWorkItemRateTable.row(selectedRow).data(selectedRowData);

    routineWorkItemRateTable.on('order.dt search.dt', function () {
        routineWorkItemRateTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

updateRoutineWorkItemRateRow = function (routineWorkItemRateTable, selectedRow) {
    var selectedRowData = routineWorkItemRateTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[7] = capitalizeFirstAllWords($.trim(availableInputs[0].value));

    routineWorkItemRateTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

}

restoreRoutineWorkItemRateRowData = function (routineWorkItemRateTable, previousRow) {
    var previousRowData = routineWorkItemRateTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        //removeNewWorkItemRateRowCreatedOnCancel(routineWorkItemRateTable, previousRow);
    } else {
        routineWorkItemRateTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;
    }

}

capitalizeFirstAllWords = function (str) {
    var pieces = str.split(" ");
    for (var i = 0; i < pieces.length; i++) {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
}

updateRoutineWorkItemRateData = function (routineWorkItemRateGuid) {
    var routineWorkItemRate = $.trim($('#routineWorkItemRatePopupModal input#routineWorkItemRate').val());

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateRoutineWorkItemRateData",
        data: '{"routineWorkItemRateGuid":"' + routineWorkItemRateGuid + '","routineWorkItemRate":"' + routineWorkItemRate + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

getSystemVariablesData = function () {

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetSystemVariablesGuid",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            systemVariableData = response.d;
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return systemVariableData;
}

saveRoutineWorkItemRateData = function () {
    var routineWorkItemManager = {
        routineWorkItemGuid: routineWorkItemGuid
    }    

    var routineWorkItemRate = $.trim($('#routineWorkItemRatePopupModal input#routineWorkItemRate').val());

    var fiscalYear = $('#systemFiscalYear').val();

    var circleManager = {
        circleGuid: $('#circleGuid').val()
    }
    

    var systemVariablesManager = {
        fiscalYear: fiscalYear,
        circleManager: circleManager

    }

    var routineWorkItemRateManager = {
        routineWorkItemManager: routineWorkItemManager,
        routineWorkItemRateInput: routineWorkItemRate,
        systemVariablesManager: systemVariablesManager
    }

    var routineWorkItemRateManagerJsonString = JSON.stringify(routineWorkItemRateManager);

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveRoutineWorkItemRateData",
        data: '{"routineWorkItemRateManager":' + routineWorkItemRateManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}
