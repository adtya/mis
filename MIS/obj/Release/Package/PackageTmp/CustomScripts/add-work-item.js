﻿$(document).ready(function () {
    var arrWorkItemSubHeadsSelectedForWorkItem = [];

    $('#addWorkItemMenuBtn').on('click', function (e) {
        e.preventDefault();
        //$('#addWorkItemPopupModal').modal('show');
        
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetUnitSymbols",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                showUnitListOptionsToAddWorkItems('workItemUnitList', response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });

        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetWorkItemSubHeadNames",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                showWorkItemSubHeadListOptionsToAddWorkItems('workItemSubHeadList', response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });

        $('#workItemSubHeadList').multiselect({
            maxHeight: 150,
            enableCaseInsensitiveFiltering: true,
            numberDisplayed: 0,
            onChange: function (element, checked) {
                if (checked === true) {
                    $('#workItemSubHeadListDisplay').removeClass('hidden');

                    arrWorkItemSubHeadsSelectedForWorkItem.push({
                        workItemSubHeadGuid: element.val(),
                        workItemSubHeadName: element.text()
                    });

                    displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelectedForWorkItem);
                }
                else if (checked === false) {
                    if (confirm('Do you wish to deselect the element?')) {
                        for (var i = arrWorkItemSubHeadsSelectedForWorkItem.length - 1; i >= 0; i--) {
                            if (arrWorkItemSubHeadsSelectedForWorkItem[i].workItemSubHeadGuid === element.val()) {
                                arrWorkItemSubHeadsSelectedForWorkItem.splice(i, 1);
                            }
                        }

                        displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelectedForWorkItem);
                    }
                    else {
                        $("#workItemSubHeadList").multiselect('select', element.val());

                        displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelectedForWorkItem);
                    }
                }
            }
        });

        $('#addWorkItemPopupModal').modal('show');

    });

    $('#addWorkItemPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            workItemName: {
                required: true,
                noSpace: true
            },
            workItemUnitList: "required",
            workItemSubHeadList: "required"
        },
    });

    $('#addWorkItemBtn').on('click', function (e) {
        e.preventDefault();

        if ($("#addWorkItemPopupForm").valid()) {

            if (arrWorkItemSubHeadsSelectedForWorkItem.length > 0) {
                $("#preloader").show();
                $("#status").show();

                var check = saveWorkItem(JSON.stringify(getSimpleArray(arrWorkItemSubHeadsSelectedForWorkItem)));

                if (check == 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data is saved successfully.");
                } else if (check == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.\nPlease contact your database administrator.");
                }
            } else {
                bootbox.alert("Please Select Work Item Sub Heads.");
            }

        }

    });

});

showUnitListOptionsToAddWorkItems = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Unit--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 2;
    }
}

showWorkItemSubHeadListOptionsToAddWorkItems = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    //option_str.options[0] = new Option('--Select Sub-Head--', '');
    //option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 2;
    }
}

displayWorkItemSubHeadsList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    var html = '<ul class = "list-group">';
    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += arr[index].workItemSubHeadName;
        html += '</li>';
    }

    html += '</ul>';

    document.getElementById(divname).innerHTML = html;

    if (arr.length == 0) {
        div.addClass('hidden');
    }
}

getSimpleArray = function (arr) {

    var result = new Array();

    for (var x = 0; x < arr.length; x++) {
        result.push(arr[x].workItemSubHeadGuid);
    }

    return result;
}

saveWorkItem = function (arrWorkItemSubHeadsJSONString) {

    var workItemName = $.trim($('#workItemName').val());
    var workItemUnitGuid = $('#workItemUnitList').val();

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveWorkItem",
        data: '{"workItemName":"' + workItemName + '","workItemUnitGuid":"' + workItemUnitGuid + '","arrWorkItemSubHeadsJSONString":' + arrWorkItemSubHeadsJSONString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });

    return retValue;
}