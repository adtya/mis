﻿var routineWorkItemData;
var assetQuanityParameterData = [];
var assetTypesBasedRoutineOnWorkItem = "";
var assetCodeBasedOnAssetTypeForRoutineAnalysis = "";
var gateDataForAssetQuantity = "";
var assetCodeDataForRoutineAnalysis;
var assetTypeDataForRoutineAnalysis
var assetRiskRateForRoutineAnalysis;
var routineServiceLevelForRoutineAnalysis;
var routineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis ="";
var routineIntensityFactorForRoutineAnalysis;
var routineWorkItemRateForRoutineAnalysis;
var systemVariablesForRoutineAnalysis;
var routineAnalysisGuidVal;
var assetCodeList = [];
var selectedAssetTypeGuid;
var selectedAssetTypeText;
var routineServiceLevel;

var assetTypeSelectGuid;

$(document).ready(function () {

    $('#routineWorkItemList').multiselect({
        maxHeight: 150,
        numberDisplayed: 1,
        nonSelectedText: '-- Select Routine Work Item --',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',

        onChange: function (element, checked) {

            $(element).closest('.multiselect').valid();

            $(element).closest('.multiselect').valid();

            if (checked === true) {

                assetCodeList = [];
                options = [];
                $('#assetQuantityParameterListForRoutineAnalysis').html("");
                $('#assetCodeListForRoutineWorkItem').html("");

                var selectedRoutineWorkItemGuid = $(element).val();
                var selectedRoutineWorkItemText = $(element).text();

                if (selectedRoutineWorkItemGuid === '') {

                    $('#workItemShortDescription').val("");
                    $('#workItemLongDescription').val("");
                    $('#unitForRoutineAnalysis').val("");
                    $('#assetQuantityParameterListForRoutineAnalysis').append('<option value="" selected="selected">' + 'Select Asset Quantity Parameter' + '</option>');
                    $('#assetQuantityParameterListForRoutineAnalysis').multiselect('rebuild');
                    $('#unitRateForRoutineAnalysis').val("");
                    $('#circleNameForRoutineAnalysis').text("");
                    $('#fiscalYearForRoutineAnalysisText').text("");
                    $('#serviceLevelByAssetTypeFieldset').hide();
                    $('#assetCodeListForRoutineWorkItem').append('<option value="" selected="selected">' + '--Select Asset Code--' + '</option>');
                    $('#assetCodeListForRoutineWorkItem').multiselect('rebuild');
                    $('#routineOMQuantitiesFieldset').hide();


                } else {
                    
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "RoutineAnalysis.aspx/GetRoutineWorkItemDataBasedOnWorkItem",
                        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '"}',
                        dataType: "json",
                        success: function (data) {
                            routineWorkItemData = data.d;

                            for (var i = 0; i < routineWorkItemData.length; i++) {
                                var assetQuantityParametersData = getAssetTypeParameters();
                                $('#workItemShortDescription').val(routineWorkItemData[i].ShortDescription);
                                $('#workItemLongDescription').val(routineWorkItemData[i].LongDescription);

                                $('#assetQuantityParameterListForRoutineAnalysis').append('<option value="">' + 'Select Asset Quantity Parameter' + '</option>');
                                for (var j = 0; j < assetQuantityParametersData.length; j++) {
                                    if (assetQuantityParametersData[j].AssetQuantityParameterGuid == routineWorkItemData[i].AssetQuantityParameterManager.AssetQuantityParameterGuid) {

                                        $('#assetQuantityParameterListForRoutineAnalysis').append('<option value="' + assetQuantityParametersData[j].AssetQuantityParameterGuid + '" selected="selected">' + assetQuantityParametersData[j].AssetQuantityParameterName + '</option>');
                                        $('#unitForRoutineAnalysis').val(routineWorkItemData[i].AssetQuantityParameterManager.UnitManager.UnitName);

                                    } else {
                                        $('#assetQuantityParameterListForRoutineAnalysis').append('<option value="' + assetQuantityParametersData[j].AssetQuantityParameterGuid + '">' + assetQuantityParametersData[j].AssetQuantityParameterName + '</option>');

                                    }
                                }

                                $('#assetQuantityParameterListForRoutineAnalysis').multiselect('rebuild');

                                getSystemVariablesForRoutineAnalysis();

                                getAssetTypeDataBasedOnAsserQuantityParameters();

                                getRoutineWorkItemRateForRoutineAnalysis();

                                getRoutineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis();                               

                                $('#circleNameGuidForRoutineAnalysis').val(systemVariablesForRoutineAnalysis.CircleManager.CircleGuid);

                                $('#circleNameForRoutineAnalysis').append(systemVariablesForRoutineAnalysis.CircleManager.CircleCode);

                                $('#fiscalYearForRoutineAnalysisText').append(systemVariablesForRoutineAnalysis.FiscalYear);

                                $('#workItemRateForRoutineAnalysis').val(routineWorkItemRateForRoutineAnalysis.RoutineWorkItemRate.Amount);

                                $('#workItemRateGuidForRoutineAnalysis').val(routineWorkItemRateForRoutineAnalysis.RoutineWorkItemRateGuid);

                                $('#unitRateForRoutineAnalysis').val(routineWorkItemRateForRoutineAnalysis.RoutineWorkItemRate.Amount);

                                createAssetTypeBasedOnRoutineWorkItemTable(assetTypesBasedRoutineOnWorkItem);                               
                                

                             }
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });

                }
                $(document).click();

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#assetQuantityParameterListForRoutineAnalysis').multiselect({
        maxHeight: 150,
        numberDisplayed: 1,
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',

        onChange: function (element, checked) {

            $(element).closest('.multiselect').valid();            

            if (checked === true) {

                var selectedAssetQuantityParameterGuid = $(element).val();
                var selectedAssetQuantityParameterText = $(element).text();

                if (selectedAssetQuantityParameterGuid === '') {
                    $('#unitForRoutineAnalysis').val("");
                    $('#serviceLevelByAssetTypeFieldset').hide();
                    $('#routineOMQuantitiesFieldset').hide();                   
                }
                else {                    
                    getAssetTypeDataBasedOnAsserQuantityParameters();
                    $('#unitForRoutineAnalysis').val(assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.UnitManager.UnitName);
                    getRoutineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis();                    
                    createAssetTypeBasedOnRoutineWorkItemTable(assetTypesBasedRoutineOnWorkItem);
                }               
                $(document).click();          
                
            }
        },
        
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#assetCodeListForRoutineWorkItem').multiselect({
        maxHeight: 150,
        numberDisplayed: 1,
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',

        onChange: function (element, checked) {

            $(element).closest('.multiselect').valid();

            if (checked === true) {
                var selectedAssetGuid = $(element).val();
                var selectedAssetText = $(element).text();

                if (selectedAssetGuid === '') {
                    $('#assetTypeForRoutineAnalysis').val("");
                    $('#assetCodeForRoutineAnalysis').val("");
                    $('#assetNameForRoutineAnalysis').val("");
                    $('#assetRiskRatingForRoutineAnalysis').val("");
                    $('#intensityFactorForRoutineAnalysis').val("");
                    $('#intensityFactorJustificationForRoutineAnalysis').val("");
                } else {
                    getAssetCodeDataForRoutineAnalysis();
                    getAssetRiskRatingForAssetCodeForRoutineAnalysis(selectedAssetGuid);
                    getRoutineIntensityFactorDataForRoutineAnalysis();
                    getRoutineServiceLevelForRoutineAnalysis($('#routineWorkItemList').val(), assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeGuid);
                    getRoutineAnalysisGuid($('#routineWorkItemList').val(), selectedAssetGuid);

                    $('#assetTypeForRoutineAnalysis').val(assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode);
                    $('#assetCodeForRoutineAnalysis').val(assetCodeDataForRoutineAnalysis.AssetCode);
                    $('#assetNameForRoutineAnalysis').val(assetCodeDataForRoutineAnalysis.AssetName);
                    $('#assetRiskRatingForRoutineAnalysis').val(assetRiskRateForRoutineAnalysis.AssetRiskRatingManager.AssetRiskRating);                   
                    $('#serviceLevelForRoutineAnalysis').val(routineServiceLevelForRoutineAnalysis.RoutineServiceLevel);
                    $('#intensityFactorGuidForRoutineAnalysis').val(routineIntensityFactorForRoutineAnalysis.RoutineIntensityFactorGuid);
                    $('#intensityFactorForRoutineAnalysis').val(routineIntensityFactorForRoutineAnalysis.RoutineIntensityFactor);
                    $('#intensityFactorJustificationForRoutineAnalysis').val(routineIntensityFactorForRoutineAnalysis.RoutineIntensityFactorJustification);
                    $('#routineAnalysisGuid').val(routineAnalysisGuidVal.RoutineAnalysisDetailsGuid);

                    calculateAssetQuantity();

                    var omRoutineCostValue = $('#serviceLevelForRoutineAnalysis').val() * $('#assetQuantityForRoutineAnalysis').val() * $('#intensityFactorForRoutineAnalysis').val() * $('#unitRateForRoutineAnalysis').val();
                    $('#omRoutineCost').val(omRoutineCostValue);
                }                
                $(document).click();

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });    

    $('#workItemRateForRoutineAnalysis').on('change', function (e) {
        e.preventDefault();

        $('#unitRateForRoutineAnalysis').val($('#workItemRateForRoutineAnalysis').val());
    });

    $('#intensityFactorForRoutineAnalysis').on('change', function (e) {
        e.preventDefault();

        calculateAssetQuantity();

        var omRoutineCostValue = $('#serviceLevelForRoutineAnalysis').val() * $('#assetQuantityForRoutineAnalysis').val() * $('#intensityFactorForRoutineAnalysis').val() * $('#unitRateForRoutineAnalysis').val();
        $('#omRoutineCost').val(omRoutineCostValue);

    });

    $('#saveRoutineAnalysisBtn').on('click', function () {

        if ($('#workItemRateGuidForRoutineAnalysis').val() === '00000000-0000-0000-0000-000000000000') {
            saveRoutineWorkItemRateData();
        }
        else
        {           
            updateRoutineWorkItemRateDataForRoutineAnalysis($('#workItemRateGuidForRoutineAnalysis').val());
        }

        if ($('#intensityFactorGuidForRoutineAnalysis').val() === '00000000-0000-0000-0000-000000000000') {
            saveRoutineIntensityFactorData();
        }
        else {
            updateRoutineIntensityFactorDataForRoutineAnalysis($('#intensityFactorGuidForRoutineAnalysis').val());
        }
        if ($('#routineAnalysisGuid').val() === '00000000-0000-0000-0000-000000000000')
        {
            saveRoutineAnalysisDetailsData();
        }
        else {
            updateRoutineAnalysisDetailsData();
        }      

    });


});

createAssetTypeBasedOnRoutineWorkItemTable = function (assetTypesBasedRoutineOnWorkItem) {

    $("#assetTypeBasedOnRoutineWorkItemsPanel").html("");

    var html ='<div class="row">';
    html += '<div class="col-sm-12 col-md-12">';

    html += '<div class="col-sm-6 col-md-6">';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="assetTypeList" id="assetTypeListLabel" class="control-label pull-left">Asset Type</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control multiselect" name="assetTypeList" id="assetTypeList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">';
    html += '<option value="" selected="selected">--Select Asset Type--</option>';

    if (routineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis.length === 0) {
        for (var i = 0; i < assetTypesBasedRoutineOnWorkItem.length; i++) {
            html += '<option value="' + assetTypesBasedRoutineOnWorkItem[i].AssetManager.AssetTypeManager.AssetTypeGuid + '">';
            html += assetTypesBasedRoutineOnWorkItem[i].AssetManager.AssetTypeManager.AssetTypeCode;
            html += '</option>';
        }

    }
    else {
        var a = [];

        for (var i = 0; i < routineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis.length; i++) {
            a.push(routineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis[i].AssetTypeManager.AssetTypeGuid);
        }

        for (var i = 0; i < assetTypesBasedRoutineOnWorkItem.length; i++) {

            if ($.inArray(assetTypesBasedRoutineOnWorkItem[i].AssetManager.AssetTypeManager.AssetTypeGuid, a) === -1) {
                html += '<option value="' + assetTypesBasedRoutineOnWorkItem[i].AssetManager.AssetTypeManager.AssetTypeGuid + '">';
                html += assetTypesBasedRoutineOnWorkItem[i].AssetManager.AssetTypeManager.AssetTypeCode;
                html += '</option>';
            }                       
        }

    }    

    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    
    html += '<div id="table-container" style="padding:1%;">';
   
    html += '<div class="table-responsive">';
    html += '<table id="assetTypeBasedOnRoutineWorkItemForRoutineAnalysisDataList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'Service Level Guid' + '</th>';
    html += '<th>' + 'Asset Type Guid' + '</th>';
    html += '<th>' + 'Asset Type Code' + '</th>';
    html += '<th>' + 'Times/ Year' + '</th>';
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    $('#assetCodeListForRoutineWorkItem').append('<option value="" selected="selected">' + '--Select Asset Code--' + '</option>');

    if (routineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis.length !== 0)
    {
        for (var i = 0; i < routineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis.length; i++) {
            html += '<tr>';
            html += '<td>' + '' + '</td>';
            html += '<td>' + routineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis[i].RoutineServiceLevelGuid + '</td>';
            html += '<td>' + routineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis[i].AssetTypeManager.AssetTypeGuid + '</td>';
            html += '<td>' + routineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis[i].AssetTypeManager.AssetTypeCode + '</td>';
            html += '<td>' + routineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis[i].RoutineServiceLevel + '</td>';
            html += '<td>' + '<a href="#" class="editAssetTypeBasedOnWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteAssetTypeBasedOnWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';
            html += '</tr>';

            getAssetCodeBasedOnAssetTypeForRoutineAnalysis(routineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis[i].AssetTypeManager.AssetTypeGuid);
                   
            for (var j = 0; j < assetCodeBasedOnAssetTypeForRoutineAnalysis.length; j++) {
                $('#assetCodeListForRoutineWorkItem').append('<option value="' + assetCodeBasedOnAssetTypeForRoutineAnalysis[j].AssetGuid + '">' + assetCodeBasedOnAssetTypeForRoutineAnalysis[j].AssetCode + '</option>');
            }

            $('#assetCodeListForRoutineWorkItem').multiselect('rebuild');
            
        }
        
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#assetTypeBasedOnRoutineWorkItemsPanel").append(html);

    $('#routineOMQuantitiesFieldset').show();

    $('#serviceLevelByAssetTypeFieldset').show();

    $('#assetTypeList').multiselect({
        maxHeight: 150,    
        numberDisplayed: 1,
        nonSelectedText: '--Select Asset Type--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        
        onChange: function (element, checked) {
           
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                //var selectedAssetTypeGuid = $(element).val();
                //var selectedAssetTypeText = $(element).text();

                //if (selectedAssetTypeGuid === '') {
                //}
                //else {
                //    getRoutineServiceLevelForRoutineAnalysis();

                //    $('#serviceLevelForRoutineAnalysis').val(routineServiceLevelForRoutineAnalysis.RoutineServiceLevel);
                //}             

                $(document).click();

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    assetTypesBasedOnRoutineWorkItemTableRelatedFunctions();


}

assetTypesBasedOnRoutineWorkItemTableRelatedFunctions = function () {
    var assetTypeRowEditing = null;

    var table = $('#assetTypeBasedOnRoutineWorkItemForRoutineAnalysisDataList').DataTable({
        responsive: true,
        paging: false,
        info: false,
        searching: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [2],
                visible: false,
                searchable: false
            }
        ],
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        buttons: [
            {               
                text: '+ Add New Asset Type',
                className: 'btn-success addNewAssetTypeBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    var assetTypeSelectGuid = $('#assetTypeList').val();
                    var assetTypeSelect = $('#assetTypeList option:selected').text();             
                    if (assetTypeSelectGuid == '') {

                    }
                    else {
                        getRoutineServiceLevelForRoutineAnalysis($('#routineWorkItemList').val(), assetTypeSelectGuid);
                        var newAssetTypeRow = table.row.add([
                          '',
                          '',
                          assetTypeSelectGuid,
                          assetTypeSelect,
                          '<input id="times" name="serviceLevelTimes" class="form-control serviceLevelTimes" type="text" value="' + routineServiceLevelForRoutineAnalysis.RoutineServiceLevel + '"/>',
                          '<a href="#" class="saveAssetTypeBasedOnWorkItemBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelAssetTypeBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>'
                        ]).draw();

                        var newAssetTypeRowNode = newAssetTypeRow.node();

                        assetTypeRowEditing = newAssetTypeRowNode;                        

                        table.button(['.addNewAssetTypeBtn']).disable();                       

                        getAssetCodeBasedOnAssetTypeForRoutineAnalysis(assetTypeSelectGuid);

                        for (var j = 0; j < assetCodeBasedOnAssetTypeForRoutineAnalysis.length; j++) {
                            $('#assetCodeListForRoutineWorkItem').append('<option value="' + assetCodeBasedOnAssetTypeForRoutineAnalysis[j].AssetGuid + '">' + assetCodeBasedOnAssetTypeForRoutineAnalysis[j].AssetCode + '</option>');
                        }

                        $('#serviceLevelForRoutineAnalysis').val(routineServiceLevelForRoutineAnalysis.RoutineServiceLevel);

                        $('option[value="' + assetTypeSelectGuid + '"]', $('#assetTypeList')).remove();                  

                        $('#assetTypeList').multiselect('rebuild');
                        

                    }                                      
                }
            }
        ]
    });

    
    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $('#assetTypeBasedOnRoutineWorkItemsPanel table#assetTypeBasedOnRoutineWorkItemForRoutineAnalysisDataList').on('click', 'a.saveAssetTypeBasedOnWorkItemBtn', function (e) {
        e.preventDefault();
        var assetTypeTable = $('#assetTypeBasedOnRoutineWorkItemForRoutineAnalysisDataList').DataTable();

        var selectedRow = $(this).parents('tr')[0];

        selectedAssetTypeGuid = assetTypeTable.row(selectedRow).data()[2];

        selectedAssetTypeText = assetTypeTable.row(selectedRow).data()[3];

        var availableInputs = $('input', selectedRow);

        routineServiceLevel = availableInputs[0].value; //assetTypeTable.row(selectedRow).data()[4];       

        $('#assetCodeListForRoutineWorkItem').multiselect('rebuild');

        var routineServiceLevelSavedGuid = saveRoutineServiceLevelData();

        if (jQuery.Guid.IsValid(routineServiceLevelSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(routineServiceLevelSavedGuid.toUpperCase())) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Data is saved successfully.", function () {
                if (assetTypeRowEditing !== null && assetTypeRowEditing == selectedRow) {
                    /* A different row is being edited - the edit should be cancelled and this row edited */
                    saveNewAssetTypeRow(assetTypeTable, assetTypeRowEditing, routineServiceLevelSavedGuid);
                    assetTypeRowEditing = null;
                    assetTypeTable.button(['.addNewAssetTypeBtn']).enable();
                }
            });
        } else {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Data cannot be saved.<br>Please contact your database administrator.");
        }       

        $('#serviceLevelForRoutineAnalysis').val(routineServiceLevel);     
       

    });

    $('#assetTypeBasedOnRoutineWorkItemsPanel table#assetTypeBasedOnRoutineWorkItemForRoutineAnalysisDataList').on('click', 'a.editAssetTypeBasedOnWorkItemBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (assetTypeRowEditing !== null && assetTypeRowEditing != selectedRow) {

            /* A different row is being edited - the edit should be cancelled and this row edited */

            restoreAssetTypeRowData(table, assetTypeRowEditing);
            editAssetTypeRow(table, selectedRow);
            assetTypeRowEditing = selectedRow;

            var el = $('#assetTypeBasedOnRoutineWorkItemsPanel input#routineServiceLevelCell');
            var elemLen = el.val().length;
            el.focus();


            el.setCursorPosition(elemLen);
        }
        else {
            /* No row currently being edited */
            editAssetTypeRow(table, selectedRow);
            assetTypeRowEditing = selectedRow;

            var el = $('#assetTypeBasedOnRoutineWorkItemsPanel input#routineServiceLevelCell');
            var elemLen = el.val().length;
            el.focus();

            el.setCursorPosition(elemLen);
        }
    });

    $('#assetTypeBasedOnRoutineWorkItemsPanel table#assetTypeBasedOnRoutineWorkItemForRoutineAnalysisDataList').on('click', 'a.updateAssetTypeRowBtn', function (e) {
            e.preventDefault();

                $("#preloader").show();
                $("#status").show();

                /* Get the row as a parent of the link that was clicked on */
                var selectedRow = $(this).parents('tr')[0];

                var routineServiceLevelUpdated = updateRoutineServiceLevelData(table.row(selectedRow).data()[1]);

                if (routineServiceLevelUpdated == 1) {

                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your Data is updated successfully.", function () {
                        if (assetTypeRowEditing !== null && assetTypeRowEditing == selectedRow) {

                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            updateRoutineServiceLevelRow(table, assetTypeRowEditing);
                            assetTypeRowEditing = null;
                            $('#serviceLevelForRoutineAnalysis').val(table.row(selectedRow).data()[4]);

                            calculateAssetQuantity();

                            var omRoutineCostValue = $('#serviceLevelForRoutineAnalysis').val() * $('#assetQuantityForRoutineAnalysis').val() * $('#intensityFactorForRoutineAnalysis').val() * $('#unitRateForRoutineAnalysis').val();
                            $('#omRoutineCost').val(omRoutineCostValue);

                        }
                    });
                } else if (routineServiceLevelUpdated == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
                }         
            

    });

    $('#assetTypeBasedOnRoutineWorkItemsPanel table#assetTypeBasedOnRoutineWorkItemForRoutineAnalysisDataList').on('click', 'a.deleteAssetTypeBasedOnWorkItemBtn', function (e) {
        e.preventDefault();
        var assetTypeTable = $('#assetTypeBasedOnRoutineWorkItemForRoutineAnalysisDataList').DataTable();

        var selectedRow = $(this).parents('tr')[0];        

        assetTypeSelectGuid = assetTypeTable.row(selectedRow).data()[2];
        var assetTypeSelect = assetTypeTable.row(selectedRow).data()[3];       

        var selectedRoutineWorkItemGuid = $('#routineWorkItemList').val();

        deleteRoutineServiceLevelData();

        assetTypeTable.row(selectedRow).remove().draw();        

        for (var j = 0; j < assetCodeList.length; j++)
        {            
            for (var k=0;k< assetCodeList[j].length;k++)
            {
                if(assetCodeList[j][k].AssetTypeManager.AssetTypeGuid == assetTypeSelectGuid)
                {
                    $('option[value="' + assetCodeList[j][k].AssetGuid + '"]', $('#assetCodeListForRoutineWorkItem')).remove();
                    deleteRoutineIntensityFactorData(selectedRoutineWorkItemGuid, assetCodeList[j][k].AssetGuid);
                    deleteRoutineAnalysisDetailsData(selectedRoutineWorkItemGuid, assetCodeList[j][k].AssetGuid);                    
                }
                $('#assetCodeListForRoutineWorkItem').multiselect('rebuild');
                $('#assetTypeForRoutineAnalysis').val("");
                $('#assetCodeForRoutineAnalysis').val("");
                $('#assetNameForRoutineAnalysis').val("");
                $('#assetRiskRatingForRoutineAnalysis').val("");
                $('#serviceLevelForRoutineAnalysis').val("");
                $('#assetQuantityForRoutineAnalysis').val("");
                $('#intensityFactorForRoutineAnalysis').val("");
                $('#omRoutineCost').val("");
                $('#intensityFactorJustificationForRoutineAnalysis').val("");           

            }            
        }

        $('#assetTypeList')
            .append('<option value="' + assetTypeSelectGuid + '">' + assetTypeSelect + '</option>');

        $('#assetTypeList').multiselect('rebuild');
        
    });

    $('#assetTypeBasedOnRoutineWorkItemsPanel table#assetTypeBasedOnRoutineWorkItemForRoutineAnalysisDataList').on('click', 'a.cancelAssetTypeBtn', function (e) {
        e.preventDefault();
        var assetTypeTable = $('#assetTypeBasedOnRoutineWorkItemForRoutineAnalysisDataList').DataTable();

        var selectedRow = $(this).parents('tr')[0];

        var assetTypeSelectGuid = assetTypeTable.row(selectedRow).data()[2];
        var assetTypeSelect = assetTypeTable.row(selectedRow).data()[3];

        assetTypeTable.row(selectedRow).remove().draw();

        for (var j = 0; j < assetCodeList.length; j++) {
            for (var k = 0; k < assetCodeList[j].length; k++) {
                if (assetCodeList[j][k].AssetTypeManager.AssetTypeGuid == assetTypeSelectGuid) {
                    $('option[value="' + assetCodeList[j][k].AssetGuid + '"]', $('#assetCodeListForRoutineWorkItem')).remove();
                }
                $('#assetCodeListForRoutineWorkItem').multiselect('rebuild');
                $('#serviceLevelForRoutineAnalysis').val("");

            }
        }

        $('#assetTypeList')
            .append('<option value="' + assetTypeSelectGuid + '">' + assetTypeSelect + '</option>')
        ;

        $('#assetTypeList').multiselect('rebuild');

        assetTypeTable.button(['.addNewAssetTypeBtn']).enable();

    });

    $('#assetTypeBasedOnRoutineWorkItemsPanel table#assetTypeBasedOnRoutineWorkItemForRoutineAnalysisDataList').on('click', 'a.cancelAssetTypeRowBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (assetTypeRowEditing !== null && assetTypeRowEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */

            restoreAssetTypeRowData(table, assetTypeRowEditing);
            assetTypeRowEditing = null;
        }

    });

}

calculateAssetQuantity = function () {
    getAssetCodeDataForRoutineAnalysis();
    getAssetTypeDataForRoutineAnalysis();

    if (assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.AssetQuantityParameterName == "Bed Surface Area") {
        if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "CAN") {
            if (assetTypeDataForRoutineAnalysis.CanalManager.Length == null || assetTypeDataForRoutineAnalysis.CanalManager.BedWidth == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                var quantityValue = (assetTypeDataForRoutineAnalysis.CanalManager.Length) * (assetTypeDataForRoutineAnalysis.CanalManager.BedWidth);
                $('#assetQuantityForRoutineAnalysis').val(quantityValue);
            }

        }
        else if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "DRN") {
            if (assetTypeDataForRoutineAnalysis.DrainageManager.LengthDrainage == null || assetTypeDataForRoutineAnalysis.DrainageManager.BedWidthDrainage == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                var quantityValue = (assetTypeDataForRoutineAnalysis.DrainageManager.LengthDrainage) * (assetTypeDataForRoutineAnalysis.DrainageManager.BedWidthDrainage);
                $('#assetQuantityForRoutineAnalysis').val(quantityValue);
            }
        }
    }

    else if (assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.AssetQuantityParameterName == "Crest Surface Area") {
        if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "EMB") {
            if (assetTypeDataForRoutineAnalysis.EmbankmentManager.Length == null || assetTypeDataForRoutineAnalysis.EmbankmentManager.CrestWidth == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                var quantityValue = (assetTypeDataForRoutineAnalysis.EmbankmentManager.Length) * (assetTypeDataForRoutineAnalysis.EmbankmentManager.CrestWidth);
                $('#assetQuantityForRoutineAnalysis').val(quantityValue);
            }
        }
    }
    else if (assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.AssetQuantityParameterName == "Gate Surface Area") {
        getGateDataForAssetQuantityForRoutineAnalysis();
        var sumGateSurfaceAreaValue = 0;
        for (var i = 0; i < gateDataForAssetQuantity.length; i++) {
            if (gateDataForAssetQuantity[i].Diameter == null || gateDataForAssetQuantity[i].Diameter === 0) {
                if (gateDataForAssetQuantity[i].Height == null || gateDataForAssetQuantity[i].Width == null) {
                    sumGateSurfaceAreaValue = sumGateSurfaceAreaValue + gateDataForAssetQuantity[i].NumberOfGates * gateDataForAssetQuantity[i].Height * gateDataForAssetQuantity[i].Width;
                }
            }
            else {
                sumGateSurfaceAreaValue = sumGateSurfaceAreaValue + gateDataForAssetQuantity[i].NumberOfGates * Math.PI * Math.pow((gateDataForAssetQuantity[i].Diameter * 0.5), 2);
            }
        }
        $('#assetQuantityForRoutineAnalysis').val(sumGateSurfaceAreaValue.toFixed(2));
    }


    else if (assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.AssetQuantityParameterName == "Gate Circumference") {
        getGateDataForAssetQuantityForRoutineAnalysis();
        var sumGateCircumferenceValue = 0;
        for (var i = 0; i < gateDataForAssetQuantity.length; i++) {
            if (gateDataForAssetQuantity[i].Diameter == null || gateDataForAssetQuantity[i].Diameter === 0) {
                if (gateDataForAssetQuantity[i].Height == null || gateDataForAssetQuantity[i].Width == null) {
                    sumGateCircumferenceValue = sumGateCircumferenceValue + gateDataForAssetQuantity[i].NumberOfGates * 2 * (gateDataForAssetQuantity[i].Height + gateDataForAssetQuantity[i].Width);
                }
            }
            else {
                sumGateCircumferenceValue = sumGateCircumferenceValue + gateDataForAssetQuantity[i].NumberOfGates * Math.PI * gateDataForAssetQuantity[i].Diameter

            }
        }
        $('#assetQuantityForRoutineAnalysis').val(sumGateCircumferenceValue.toFixed(2));
    }

    else if (assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.AssetQuantityParameterName == "Length") {
        if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "CAN") {
            if (assetTypeDataForRoutineAnalysis.CanalManager.Length == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                $('#assetQuantityForRoutineAnalysis').val(assetTypeDataForRoutineAnalysis.CanalManager.Length);
            }

        }
        else if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "DRN") {
            if (assetTypeDataForRoutineAnalysis.DrainageManager.LengthDrainage == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                $('#assetQuantityForRoutineAnalysis').val(assetTypeDataForRoutineAnalysis.DrainageManager.LengthDrainage);
            }
        }
        else if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "EMB") {
            if (assetTypeDataForRoutineAnalysis.EmbankmentManager.Length == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                $('#assetQuantityForRoutineAnalysis').val(assetTypeDataForRoutineAnalysis.EmbankmentManager.Length);
            }

        }
        else if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "POR") {
            if (assetTypeDataForRoutineAnalysis.PorcupineManager.LengthPorcupine == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                $('#assetQuantityForRoutineAnalysis').val(assetTypeDataForRoutineAnalysis.PorcupineManager.LengthPorcupine);
            }

        }
        else if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "REV") {
            if (assetTypeDataForRoutineAnalysis.RevetmentManager.LengthRevetment == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                $('#assetQuantityForRoutineAnalysis').val(assetTypeDataForRoutineAnalysis.RevetmentManager.LengthRevetment);
            }

        }
    }

    else if (assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.AssetQuantityParameterName == "Number") {
        $('#assetQuantityForRoutineAnalysis').val(1);
    }

    else if (assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.AssetQuantityParameterName == "Slope Surface Area") {
        if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "CAN") {
            if (assetTypeDataForRoutineAnalysis.CanalManager.Length == null || assetTypeDataForRoutineAnalysis.CanalManager.AverageDepth == null || assetTypeDataForRoutineAnalysis.CanalManager.Slope1 == null || assetTypeDataForRoutineAnalysis.CanalManager.Slope2 == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                var slope = assetTypeDataForRoutineAnalysis.CanalManager.Slope1 / assetTypeDataForRoutineAnalysis.CanalManager.Slope2;
                var quantityValue = (assetTypeDataForRoutineAnalysis.CanalManager.Length) * (2 * (assetTypeDataForRoutineAnalysis.CanalManager.AverageDepth) * Math.pow(1 + Math.pow(slope, 2), 0.5));
                $('#assetQuantityForRoutineAnalysis').val(quantityValue);
            }

        }
        else if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "DRN") {
            if (assetTypeDataForRoutineAnalysis.DrainageManager.LengthDrainage == null || assetTypeDataForRoutineAnalysis.DrainageManager.AverageDepthDrainage == null || assetTypeDataForRoutineAnalysis.DrainageManager.Slope1Drainage == null || assetTypeDataForRoutineAnalysis.DrainageManager.Slope2Drainage == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                var slope = assetTypeDataForRoutineAnalysis.DrainageManager.Slope1Drainage / assetTypeDataForRoutineAnalysis.DrainageManager.Slope2Drainage;

                var quantityValue = (assetTypeDataForRoutineAnalysis.DrainageManager.LengthDrainage) * (2 * (assetTypeDataForRoutineAnalysis.DrainageManager.AverageDepthDrainage) * Math.pow(1 + Math.pow(slope, 2), 0.5));
                $('#assetQuantityForRoutineAnalysis').val(quantityValue);
            }
        }
        else if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "EMB") {
            var CsSlope;
            var RsSlope;

            if (assetTypeDataForRoutineAnalysis.EmbankmentManager.CsSlope1 == null || assetTypeDataForRoutineAnalysis.EmbankmentManager.CsSlope2 == null) {
                CsSlope = 0;
            }
            else {
                CsSlope = (assetTypeDataForRoutineAnalysis.EmbankmentManager.CsSlope1) / (assetTypeDataForRoutineAnalysis.EmbankmentManager.CsSlope2);

            }
            if (assetTypeDataForRoutineAnalysis.EmbankmentManager.RsSlope1 == null || assetTypeDataForRoutineAnalysis.EmbankmentManager.RsSlope2 == null) {
                RsSlope = 0;
            }
            else {
                RsSlope = (assetTypeDataForRoutineAnalysis.EmbankmentManager.RsSlope1) / (assetTypeDataForRoutineAnalysis.EmbankmentManager.RsSlope2);

            }
            if (assetTypeDataForRoutineAnalysis.EmbankmentManager.Length == null || assetTypeDataForRoutineAnalysis.EmbankmentManager.AverageHeight == null || CsSlope == null || RsSlope == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                var quantityValue = (assetTypeDataForRoutineAnalysis.EmbankmentManager.Length) * ((assetTypeDataForRoutineAnalysis.EmbankmentManager.AverageHeight) * (Math.pow(1 + Math.pow(CsSlope, 2), 0.5)) + (Math.pow(1 + Math.pow(RsSlope, 2), 0.5)));
                $('#assetQuantityForRoutineAnalysis').val(quantityValue);
            }
        }

    }

    else if (assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.AssetQuantityParameterName == "Top Bank Surface Area") {
        if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "CAN") {

            if (assetTypeDataForRoutineAnalysis.CanalManager.Length == null || assetTypeDataForRoutineAnalysis.CanalManager.BedWidth == null || assetTypeDataForRoutineAnalysis.CanalManager.AverageDepth == null || assetTypeDataForRoutineAnalysis.CanalManager.Slope1 == null || assetTypeDataForRoutineAnalysis.CanalManager.Slope2 == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                var slope = assetTypeDataForRoutineAnalysis.CanalManager.Slope1 / assetTypeDataForRoutineAnalysis.CanalManager.Slope2;

                var quantityValue = (assetTypeDataForRoutineAnalysis.CanalManager.Length) * (assetTypeDataForRoutineAnalysis.CanalManager.BedWidth + 2 * (assetTypeDataForRoutineAnalysis.CanalManager.AverageDepth) * slope);
                $('#assetQuantityForRoutineAnalysis').val(quantityValue);
            }

        }
        else if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "DRN") {
            if (assetTypeDataForRoutineAnalysis.DrainageManager.LengthDrainage == null || assetTypeDataForRoutineAnalysis.DrainageManager.BedWidthDrainage == null || assetTypeDataForRoutineAnalysis.DrainageManager.AverageDepthDrainage == null || assetTypeDataForRoutineAnalysis.DrainageManager.Slope1Drainage == null || assetTypeDataForRoutineAnalysis.DrainageManager.Slope2Drainage == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                var slope = assetTypeDataForRoutineAnalysis.DrainageManager.Slope1Drainage / assetTypeDataForRoutineAnalysis.DrainageManager.Slope2Drainage;
                var quantityValue = (assetTypeDataForRoutineAnalysis.DrainageManager.LengthDrainage) * (assetTypeDataForRoutineAnalysis.DrainageManager.BedWidthDrainage + 2 * (assetTypeDataForRoutineAnalysis.DrainageManager.AverageDepthDrainage) * slope);
                $('#assetQuantityForRoutineAnalysis').val(quantityValue);
            }
        }

    }
    else if (assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.AssetQuantityParameterName == "Total Surface Area") {
        if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "CAN") {

            if (assetTypeDataForRoutineAnalysis.CanalManager.Length == null || assetTypeDataForRoutineAnalysis.CanalManager.BedWidth == null || assetTypeDataForRoutineAnalysis.CanalManager.AverageDepth == null || assetTypeDataForRoutineAnalysis.CanalManager.Slope1 == null || assetTypeDataForRoutineAnalysis.CanalManager.Slope2 == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                var slope = assetTypeDataForRoutineAnalysis.CanalManager.Slope1 / assetTypeDataForRoutineAnalysis.CanalManager.Slope2;
                var quantityValue = (assetTypeDataForRoutineAnalysis.CanalManager.Length) * ((assetTypeDataForRoutineAnalysis.CanalManager.BedWidth + 2) * (assetTypeDataForRoutineAnalysis.CanalManager.AverageDepth) * Math.pow(Math.pow(slope, 2), 0.5));
                $('#assetQuantityForRoutineAnalysis').val(quantityValue);
            }

        }
        else if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "DRN") {
            if (assetTypeDataForRoutineAnalysis.DrainageManager.LengthDrainage == null || assetTypeDataForRoutineAnalysis.DrainageManager.BedWidthDrainage == null || assetTypeDataForRoutineAnalysis.DrainageManager.AverageDepthDrainage == null || assetTypeDataForRoutineAnalysis.DrainageManager.Slope1Drainage == null || assetTypeDataForRoutineAnalysis.DrainageManager.Slope2Drainage == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {
                var slope = assetTypeDataForRoutineAnalysis.DrainageManager.Slope1Drainage / assetTypeDataForRoutineAnalysis.DrainageManager.Slope2Drainage;
                var quantityValue = (assetTypeDataForRoutineAnalysis.DrainageManager.LengthDrainage) * ((assetTypeDataForRoutineAnalysis.DrainageManager.BedWidthDrainage + 2) * (assetTypeDataForRoutineAnalysis.DrainageManager.AverageDepthDrainage) * Math.pow(Math.pow(slope, 2), 0.5));
                $('#assetQuantityForRoutineAnalysis').val(quantityValue);
            }
        }
        else if (assetCodeDataForRoutineAnalysis.AssetTypeManager.AssetTypeCode == "EMB") {
            var CsSlope;
            var RsSlope;

            if (assetTypeDataForRoutineAnalysis.EmbankmentManager.CsSlope1 == null || assetTypeDataForRoutineAnalysis.EmbankmentManager.CsSlope2 == null) {
                CsSlope = 0;
            }
            else {
                CsSlope = (assetTypeDataForRoutineAnalysis.EmbankmentManager.CsSlope1) / (assetTypeDataForRoutineAnalysis.EmbankmentManager.CsSlope2);

            }
            if (assetTypeDataForRoutineAnalysis.EmbankmentManager.RsSlope1 == null || assetTypeDataForRoutineAnalysis.EmbankmentManager.RsSlope2 == null) {
                RsSlope = 0;
            }
            else {
                RsSlope = (assetTypeDataForRoutineAnalysis.EmbankmentManager.RsSlope1) / (assetTypeDataForRoutineAnalysis.EmbankmentManager.RsSlope2);

            }
            if (assetTypeDataForRoutineAnalysis.EmbankmentManager.Length == null || assetTypeDataForRoutineAnalysis.EmbankmentManager.CrestWidth == null || assetTypeDataForRoutineAnalysis.EmbankmentManager.AverageHeight == null || CsSlope == null || RsSlope == null) {
                $('#assetQuantityForRoutineAnalysis').val(0);
            }
            else {

                var quantityValue = (assetTypeDataForRoutineAnalysis.EmbankmentManager.Length) * (assetTypeDataForRoutineAnalysis.EmbankmentManager.CrestWidth + assetTypeDataForRoutineAnalysis.EmbankmentManager.AverageHeight * ((Math.pow(1 + Math.pow(CsSlope, 2), 0.5)) + (Math.pow(1 + Math.pow(RsSlope, 2), 0.5))));
                $('#assetQuantityForRoutineAnalysis').val(quantityValue);
            }
        }

    }
    else if (assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.AssetQuantityParameterName == "Volume Cut") {
        $('#assetQuantityForRoutineAnalysis').val(0);

    }
    else if (assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.AssetQuantityParameterName == "Volume Fill") {
        $('#assetQuantityForRoutineAnalysis').val(0);
    }
}

editAssetTypeRow = function (table, selectedRow) {
    var selectedRowData = table.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.

    availableTds[2].innerHTML = '<input type="text" id="routineServiceLevelCell" name="routineServiceLevelCell" placeholder="RoutineService Level" class="form-control text-capitalize" value="' + selectedRowData[4] + '">';
    availableTds[3].innerHTML = '<a href="#" class="updateAssetTypeRowBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelAssetTypeRowBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

saveNewAssetTypeRow = function (assetTypeTable, selectedRow, routineServiceLevelSavedGuid) {

    var selectedRowData = assetTypeTable.row(selectedRow).data();
   
    var availableInputs = $('input', selectedRow);

    selectedRowData[1] = routineServiceLevelSavedGuid;

    selectedRowData[4] = availableInputs[0].value;    
    selectedRowData[5] = '<a href="#" class="editAssetTypeBasedOnWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteAssetTypeBasedOnWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    assetTypeTable.row(selectedRow).data(selectedRowData);

    assetTypeTable.on('order.dt search.dt', function () {
        assetTypeTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

restoreAssetTypeRowData = function (table, previousRow) {
    var previousRowData = table.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        //removeNewWorkItemRateRowCreatedOnCancel(routineWorkItemRateTable, previousRow);
    } else {
        table.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;
    }

}

updateRoutineServiceLevelRow = function (table, selectedRow) {
    var selectedRowData = table.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML; 

    selectedRowData[4] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
    
    table.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

}

getAssetQuantityParameters = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "DBAMethods.asmx/GetAssetQuantityParameter",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {           
            assetQuanityParameterData = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });    
}

getAssetTypeDataBasedOnAsserQuantityParameters = function () {
    var selectedAssetQuantityParameterGuid = $('#assetQuantityParameterListForRoutineAnalysis').val();
    $.ajax({
        type: "POST",
        async: false,
        url: "RoutineAnalysis.aspx/GetAssetTypeBasedOnAssetQuantityParameterForRoutineAnalysis",
        data: '{"assetQuantityParameterGuid":"' + selectedAssetQuantityParameterGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            assetTypesBasedRoutineOnWorkItem = response.d;
            $('#unitForRoutineAnalysis').val(assetTypesBasedRoutineOnWorkItem[0].AssetQuantityParameterManager.UnitManager.UnitName);
        },
        failure: function (result) {
            alert("Error");
        }
    });
   
}

getAssetCodeBasedOnAssetTypeForRoutineAnalysis = function (selectedAssetTypeGuid) {
   
    $.ajax({
        type: "POST",
        async: false,
        url: "RoutineAnalysis.aspx/GetAssetCodeBasedOnAssetTypeForRoutineAnalysis",
        data: '{"assetTypeGuid":"' + selectedAssetTypeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {           
            assetCodeBasedOnAssetTypeForRoutineAnalysis = response.d;
            assetCodeList.push(assetCodeBasedOnAssetTypeForRoutineAnalysis);
        },
        failure: function (result) {
            alert("Error");
        }
    });   
}

getAssetCodeDataForRoutineAnalysis = function () {
    var selectedAssetGuid = $('#assetCodeListForRoutineWorkItem').val();

    //var selectedAssetTypeCode = var selectedAssetTypeGuid = $('#assetTypeList').val();

   
    $.ajax({
        type: "POST",
        async: false,
        url: "RoutineAnalysis.aspx/GetAssetCodeDataForRoutineAnalysis",
        data: '{"assetGuid":"' + selectedAssetGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            
            assetCodeDataForRoutineAnalysis = response.d;           
        },
        failure: function (result) {
            alert("Error");
        }
    });
   
}

getAssetTypeDataForRoutineAnalysis = function () {
    var selectedAssetGuid = $('#assetCodeListForRoutineWorkItem').val();

    var selectedAssetTypeCode = $('#assetTypeForRoutineAnalysis').val();

    $.ajax({
        type: "POST",
        async: false,
        url: "RoutineAnalysis.aspx/GetAssetTypeDataForRoutineAnalysis",
        data: '{"assetGuid":"' + selectedAssetGuid + '","assetTypeCode":"' + selectedAssetTypeCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {            
            assetTypeDataForRoutineAnalysis = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getGateDataForAssetQuantityForRoutineAnalysis = function () {
    var selectedAssetGuid = $('#assetCodeListForRoutineWorkItem').val();

    $.ajax({
        type: "POST",
        async: false,
        url: "RoutineAnalysis.aspx/GetGateDataForAssetQuantityForRoutineAnalysis",
        data: '{"assetGuid":"' + selectedAssetGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {            
            gateDataForAssetQuantity = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getAssetRiskRatingForAssetCodeForRoutineAnalysis = function (selectedAssetGuid) {
   // var selectedAssetGuid = $('#assetCodeListForRoutineWorkItem').val();

    $.ajax({
        type: "POST",
        async: false,
        url: "RoutineAnalysis.aspx/GetAssetRiskRatingForAssetCodeForRoutineAnalysis",
        data: '{"assetGuid":"' + selectedAssetGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {      
            assetRiskRateForRoutineAnalysis = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getRoutineServiceLevelForRoutineAnalysis = function (selectedRoutineWorkItemGuid, selectedAssetTypeGuid) {
    
    $.ajax({
        type: "POST",
        async: false,
        url: "RoutineAnalysis.aspx/GetRoutineServiceLevelForRoutineAnalysis",
        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '","assetTypeGuid":"' + selectedAssetTypeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {           
            routineServiceLevelForRoutineAnalysis = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getRoutineAnalysisGuid = function (selectedRoutineWorkItemGuid, selectedAssetGuid) {

    $.ajax({
        type: "POST",
        async: false,
        url: "RoutineAnalysis.aspx/GetRoutineAnalysisGuid",
        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '","assetGuid":"' + selectedAssetGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            routineAnalysisGuidVal = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getRoutineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis = function () {

    var selectedRoutineWorkItemGuid = $('#routineWorkItemList').val();

    $.ajax({
        type: "POST",
        async: false,
        url: "RoutineAnalysis.aspx/GetRoutineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis",
        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {            
            routineServiceLevelDataBasedOnRoutineWorkItemForRoutineAnalysis = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getRoutineIntensityFactorDataForRoutineAnalysis = function () {

    var selectedRoutineWorkItemGuid = $('#routineWorkItemList').val();
    var selectedAssetGuid = $('#assetCodeListForRoutineWorkItem').val();

    $.ajax({
        type: "POST",
        async: false,
        url: "RoutineAnalysis.aspx/GetRoutineIntensityFactorDataForRoutineAnalysis",
        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '","assetGuid":"' + selectedAssetGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
              routineIntensityFactorForRoutineAnalysis = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getRoutineWorkItemRateForRoutineAnalysis = function () {

    var selectedRoutineWorkItemGuid = $('#routineWorkItemList').val();

    $.ajax({
        type: "POST",
        async: false,
        url: "RoutineAnalysis.aspx/GetRoutineWorkItemRateForRoutineAnalysis",
        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {            
            routineWorkItemRateForRoutineAnalysis = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getSystemVariablesForRoutineAnalysis = function () {

   $.ajax({
        type: "POST",
        async: false,
        url: "RoutineAnalysis.aspx/GetSystemVariablesGuid",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {         
            systemVariablesForRoutineAnalysis = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

updateRoutineWorkItemData = function (routineWorkItemManager) {
    var assetQuantityParameterManager = {
        assetQuantityParameterGuid: $.trim($('#assetQuantityParameterListForRoutineAnalysis').val()),
    };

    var routineWorkItemManager = {
        routineWorkItemGuid: $('#routineWorkItemList').val(),
        shortDescription: $('#workItemShortDescription').val(),
        longDescription: $('#workItemLongDescription').val(),        
        assetQuantityParameterManager: assetQuantityParameterManager
    }

    var routineWorkItemManagerJsonString = JSON.stringify(routineWorkItemManager);

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "RoutineAnalysis.aspx/UpdateRoutineWorkItemData",
        data: '{"routineWorkItemManager":' + routineWorkItemManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveRoutineServiceLevelData = function () {

    var selectedRoutineWorkItemGuid = $('#routineWorkItemList').val();    

    var selectedRoutineWorkItemText = $('#routineWorkItemList option:selected').text();

    var routineServiceLevelCode = selectedRoutineWorkItemText + "/" + selectedAssetTypeText;
    
    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "RoutineAnalysis.aspx/SaveRoutineServiceLevelData",
        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '","assetTypeGuid":"' + selectedAssetTypeGuid +
            '","routineServiceLevel":"' + routineServiceLevel + '","routineServiceLevelCode":"' + routineServiceLevelCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateRoutineServiceLevelData = function (routineServiceLevelGuid) {
    var routineServiceLevel = $.trim($('#assetTypeBasedOnRoutineWorkItemsPanel input#routineServiceLevelCell').val());   

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "RoutineAnalysis.aspx/UpdateRoutineServiceLevelData",
        data: '{"routineServiceLevelGuid":"' + routineServiceLevelGuid + '","routineServiceLevel":"' + routineServiceLevel + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

deleteRoutineServiceLevelData = function () {   
   var selectedRoutineWorkItemGuid = $('#routineWorkItemList').val();

   var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "RoutineAnalysis.aspx/DeleteRoutineServiceLevelData",
        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '","assetTypeGuid":"' + assetTypeSelectGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveRoutineWorkItemRateData = function () {
    var routineWorkItemManager = {
        routineWorkItemGuid: $('#routineWorkItemList').val()
    }

    var routineWorkItemRate = $('#unitRateForRoutineAnalysis').val();

    var fiscalYear = $('#fiscalYearForRoutineAnalysisText').text();

    var circleManager = {
        circleGuid : $('#circleNameGuidForRoutineAnalysis').val()
    }

    var systemVariablesManager = {
        fiscalYear: fiscalYear,
        circleManager: circleManager
    }

    var routineWorkItemRateManager = {
        routineWorkItemManager: routineWorkItemManager,
        routineWorkItemRateInput: routineWorkItemRate,
        systemVariablesManager: systemVariablesManager
    }

    var routineWorkItemRateManagerJsonString = JSON.stringify(routineWorkItemRateManager);

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "RoutineAnalysis.aspx/SaveRoutineWorkItemRateData",
        data: '{"routineWorkItemRateManager":' + routineWorkItemRateManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updateRoutineWorkItemRateDataForRoutineAnalysis = function (routineWorkItemRateGuid) {

    var routineWorkItemRate = $('#workItemRateForRoutineAnalysis').val();

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "RoutineAnalysis.aspx/UpdateRoutineWorkItemRateDataForRoutineAnalysis",
        data: '{"routineWorkItemRateGuid":"' + routineWorkItemRateGuid + '","routineWorkItemRate":"' + routineWorkItemRate + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

saveRoutineIntensityFactorData = function () {

    var selectedRoutineWorkItemGuid = $('#routineWorkItemList').val();    

    var selectedAssetGuid = $('#assetCodeListForRoutineWorkItem').val();

    var routineIntensityFactor = $('#intensityFactorForRoutineAnalysis').val();   

    var routineIntensityFactorJustification = $('#intensityFactorJustificationForRoutineAnalysis').val();

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "RoutineAnalysis.aspx/SaveRoutineIntensityFactorData",
        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '","assetGuid":"' + selectedAssetGuid +
            '","routineIntensityFactor":"' + routineIntensityFactor + '","routineIntensityFactorJustification":"' + routineIntensityFactorJustification + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateRoutineIntensityFactorDataForRoutineAnalysis = function (routineIntensityFactorGuid) {

    var routineIntensityFactor = $.trim($('#intensityFactorForRoutineAnalysis').val());

    var routineIntensityFactorJustification = $.trim($('#intensityFactorJustificationForRoutineAnalysis').val());

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "RoutineAnalysis.aspx/UpdateRoutineIntensityFactorDataForRoutineAnalysis",
        data: '{"routineIntensityFactorGuid":"' + routineIntensityFactorGuid + '","routineIntensityFactor":"' + routineIntensityFactor + '","routineIntensityFactorJustification":"' + routineIntensityFactorJustification + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

deleteRoutineIntensityFactorData = function (selectedRoutineWorkItemGuid, assetSelectGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "RoutineAnalysis.aspx/DeleteRoutineIntensityFactorData",
        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '","assetGuid":"' + assetSelectGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {           
            bootbox.alert("Please contact your database administrator.");
        }
    });
    return retValue;
}

saveRoutineAnalysisDetailsData = function () {

    var routineWorkItemManager = {
        routineWorkItemGuid: $('#routineWorkItemList').val()
    }

    var assetQuantity = $('#assetQuantityForRoutineAnalysis').val();

    var routineWorkItemCost = $('#omRoutineCost').val();

    var assetManager = {
        assetGuid: $('#assetCodeListForRoutineWorkItem').val()
    }

    var routineAnalysisDetailsManager = {
        routineWorkItemManager: routineWorkItemManager,        
        assetManager: assetManager,
        routineWorkItemCostInput: routineWorkItemCost,
        assetQuantity: assetQuantity
    }

    var routineAnalysisDetailsManagerJsonString = JSON.stringify(routineAnalysisDetailsManager);

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "RoutineAnalysis.aspx/SaveRoutineAnalysisDetailsData",
        data: '{"routineAnalysisDetailsManager":' + routineAnalysisDetailsManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updateRoutineAnalysisDetailsData = function (routineAnalysisDetailsManager) {
  
    var routineAnalysisDetailsManager = {
        routineAnalysisDetailsGuid: $('#routineAnalysisGuid').val(),
        assetQuantity: $('#assetQuantityForRoutineAnalysis').val(),
        routineWorkItemCostInput: $('#omRoutineCost').val()
    }

    var routineAnalysisDetailsManagerJsonString = JSON.stringify(routineAnalysisDetailsManager);

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "RoutineAnalysis.aspx/UpdateRoutineAnalysisDetailsData",
        data: '{"routineAnalysisDetailsManager":' + routineAnalysisDetailsManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteRoutineAnalysisDetailsData = function (selectedRoutineWorkItemGuid, assetSelectGuid) {    
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "RoutineAnalysis.aspx/DeleteRoutineAnalysisDetailsData",
        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '","assetGuid":"' + assetSelectGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}
