﻿var overallStatusArr =[];
var inspectorPositionArr = [];
var itemPerformanceData =[];
var monitoringItemTableDataArr = [];
var monitoringItemTableDataArr1 = [];


$(document).ready(function () { 

    $('#dpInspectionDate').datepicker({
        format: "dd/mm/yyyy",
    });

    //$(function () {
    //    $('.a1').click(function () {
    //        $(this).parent().find('.content').slideToggle("slow");
    //    });
    //});

    //$("fieldset legend").click(function () {

    //    if ($(this).closest('fieldset').find('.contents').length == 0) return;
    //    if ($(this).parent().children().length == 2) $(this).siblings().slideToggle("slow");
    //    else {
    //        $(this).parent().wrapInner("<div>");
    //        $(this).appendTo($(this).parent().parent());
    //        $(this).parent().find("div").toggle();
    //    }
    //});

    $('#assetCodeList').on('change', function () {
        // monitoringItemTableDataArr1 = [];
        $('#overallStatusSelect').html("");
        $('#inspectorPositionSelect').html("");
        var selectedAssetCodeGuid = $(this).val();
        var currentMonitoringDate = $('#monitoringDate').val();
        currentMonitoringDate = Date.parseExact(currentMonitoringDate, 'dd/MM/yyyy').toString('yyyy/MM/dd')

        if (selectedAssetCodeGuid === '') {
            $('#assetTypeCode').val('');
            $('#assetCode').val('');
            $('#assetName').val('');
            $('#assetTypeGuid').val('');
            $('#assetTypeCode').val('');
            $('#performanceMonitoringGuid').val('');
            $('#inspectionDate').val('');
            $('#inspectorName').val('');
            $('#generalComments').val('');
            $('#overallStatusSelect').append('<option value="" selected="selected">' + '-- Select Overall Status --' + '</option>');
            $('#overallStatusSelect').multiselect('rebuild');
            $('#inspectorPositionSelect').append('<option value="" selected="selected">' + '-- Select Inspector Position --' + '</option>');
            $('#inspectorPositionSelect').multiselect('rebuild');
            
        } else {
            $.ajax({
                type: "POST",
                async: false,
                url: "EditPerformanceMonitoringDetails.aspx/GetPerformanceMonitoringData",
                data: '{"assetGuid":"' + selectedAssetCodeGuid + '","currentMonitoringDate":"' + currentMonitoringDate + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    performanceMonitoringDataArr = response.d;

                    getOverallStatus();
                    getInspectorPosition();

                    var inspectionDate = "";
                    for (var i = 0; i < performanceMonitoringDataArr.InspectionDate.length; i++) {
                        if (performanceMonitoringDataArr.InspectionDate.charAt(i) >= '0' && performanceMonitoringDataArr.InspectionDate.charAt(i) <= '9') {
                            inspectionDate += performanceMonitoringDataArr.InspectionDate.charAt(i);
                        }
                    }
                    var modifiedInspectionDate = new Date(parseInt(inspectionDate));
                
                    $('#assetCode').val(performanceMonitoringDataArr.AssetManager.AssetCode);
                    $('#assetName').val(performanceMonitoringDataArr.AssetManager.AssetName);
                    $('#assetTypeGuid').val(performanceMonitoringDataArr.AssetManager.AssetTypeManager.AssetTypeGuid);
                    $('#assetTypeCode').val(performanceMonitoringDataArr.AssetManager.AssetTypeManager.AssetTypeCode);
                    $('#performanceMonitoringGuid').val(performanceMonitoringDataArr.PerformanceMonitoringGuid);
                    $('#inspectionDate').val(modifiedInspectionDate.toString("dd/MM/yyyy"));
                    $('#inspectorName').val(performanceMonitoringDataArr.InspectorName);
                    $('#generalComments').val(performanceMonitoringDataArr.GeneralComments);

                    $('#overallStatusSelect').append('<option value="">' + 'Select Overall Status' + '</option>');
                    for (var j = 0; j < overallStatusArr.length;) {
                        if (overallStatusArr[j] == performanceMonitoringDataArr.OverallStatusManager.OverallStatusGuid) {

                            $('#overallStatusSelect').append('<option value="' + overallStatusArr[j] + '" selected="selected">' + overallStatusArr[j + 1] + '</option>');
                            

                        } else {
                            $('#overallStatusSelect').append('<option value="' + overallStatusArr[j] + '">' + overallStatusArr[j + 1] + '</option>');

                        }
                        j = j + 2;
                    }
                    $('#overallStatusSelect').multiselect('rebuild');

                    $('#inspectorPositionSelect').append('<option value="">' + 'Select Inspector Position' + '</option>');
                    for (var j = 0; j < inspectorPositionArr.length;) {
                        if (inspectorPositionArr[j] == performanceMonitoringDataArr.DesignationManager.DesignationGuid) {

                            $('#inspectorPositionSelect').append('<option value="' + inspectorPositionArr[j] + '" selected="selected">' + inspectorPositionArr[j + 1] + '</option>');


                        } else {
                            $('#inspectorPositionSelect').append('<option value="' + inspectorPositionArr[j] + '">' + inspectorPositionArr[j + 1] + '</option>');

                        }
                        j = j + 2;
                    }
                    $('#inspectorPositionSelect').multiselect('rebuild');
                    
                },
                failure: function (result) {
                    alert("Error");
                }
            });

            var selectedAssetTypeGuid = $('#assetTypeGuid').val();

            var performanceMonitoringGuid = $('#performanceMonitoringGuid').val();

            $.ajax({
                type: "POST",
                async: false,
                url: "EditPerformanceMonitoringDetails.aspx/GetMonitoringItemPerformanceData",
                data: '{"assetGuid":"' + selectedAssetCodeGuid + '","currentMonitoringDate":"' + currentMonitoringDate + '","assetTypeGuid":"' + selectedAssetTypeGuid + '","performanceMonitoringGuid":"' + performanceMonitoringGuid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    itemPerformanceData = response.d;
                    editPerformanceChecklistData(response.d);
                },
                failure: function (result) {
                    alert("Error");
                }
            });
        }
    });

    $('#updatePerformanceDetailsBtn').on('click', function () {
        //e.preventDefault();

        //if ($("#editPerformanceMonitoringDetailsForm").valid()) {
        //    $("#preloader").show();
        //    $("#status").show();

        //    var updatePerformanceData = updatePerformanceMontioringDetails();
        //    //var assetSaved = saveAsset(codeArray[2]);

        //    if (updatePerformanceData === 1) {
        //        $('#status').delay(300).fadeOut();
        //        $('#preloader').delay(350).fadeOut('slow');

        //        bootbox.alert("Data is updated successfully.", function () {
        //            window.location = 'DBAHome.aspx';
        //        });
        //    } else {
        //        $('#status').delay(300).fadeOut();
        //        $('#preloader').delay(350).fadeOut('slow');

        //        bootbox.alert("Data cannot be Saved.");
        //    }

        //}
        if ($("#editPerformanceMonitoringDetailsForm").valid()) {

            updatePerformanceMontioringDetails();
        }

        
    });

    $('#overallStatusSelect').multiselect({
        maxHeight: 150,
        numberDisplayed: 1,
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',

        onChange: function (element, checked) {

            $(element).closest('.multiselect').valid();

            if (checked === true) {

                var selectedOverallStatusGuid = $(element).val();
                var selectedOverallStatus = $(element).text();

                $(document).click();
            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:9%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#inspectorPositionSelect').multiselect({
        maxHeight: 150,
        numberDisplayed: 1,
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',

        onChange: function (element, checked) {

            $(element).closest('.multiselect').valid();

            if (checked === true) {

                var selectedInspectorPositionGuid = $(element).val();
                var selectedInspectorPosition = $(element).text();

                $(document).click();
            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:9%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#editPerformanceMonitoringDetailsForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            assetCodeList: "required",
            overallStatusSelect: "required",
            inspectorPositionSelect: "required",
            inspectorName: "required",
            generalComments: "required",
            inspectionDate: {
                required: true,
                dateITA: true
            }
        }
    });

    $('#cancelPerformanceDetailsBtn').on('click', function (e) {
        e.preventDefault();

        window.location = 'DBAHome.aspx';
    });

});

editPerformanceChecklistData = function (monitoringItemPerformanceArr) {

    if (monitoringItemPerformanceArr.length === 0) {
        $('fieldset#performanceCheckList').addClass('hidden');
    }
    else {
        $('fieldset#performanceCheckList').removeClass('hidden');

        for (var i = 0; i < monitoringItemPerformanceArr.length; i++) {
            var fieldsetId = 'fieldset' + monitoringItemPerformanceArr[i].MonitoringItemManager.MonitoringItemTypeManager.MonitoringItemTypeName.replace(/\s/g, '');

            if (!$('fieldset#' + fieldsetId).length) {

                var checklistHtml = '<fieldset class="scheduler-border" id="' + fieldsetId + '">';
                checklistHtml += "<legend class=\"scheduler-border a1\">" + monitoringItemPerformanceArr[i].MonitoringItemManager.MonitoringItemTypeManager.MonitoringItemTypeName + "</legend>";
                // checklistHtml += "<div class=\"contents\">";

                checklistHtml += '<div id="table-container" style="padding:1%;">';
                checklistHtml += '<div class="table-responsive">';

                checklistHtml += '<table id="' + fieldsetId + 'Table" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';
                checklistHtml += '<thead>';
                checklistHtml += '<tr>';
                checklistHtml += '<th>' + '#' + '</th>';
                checklistHtml += '<th>' + 'Item Performance Guid' + '</th>';
                checklistHtml += '<th>' + 'Item Guid' + '</th>';
                checklistHtml += '<th>' + 'Item Name' + '</th>';
                checklistHtml += '<th>' + 'Item Performance Status' + '</th>';
                checklistHtml += '<th>' + 'Item Description' + '</th>';
                checklistHtml += '</tr>';
                checklistHtml += '</thead>';
                checklistHtml += '<tbody>';
                checklistHtml += '</tbody>';
                checklistHtml += '</table>';

                //checklistHtml += '</div>';
                checklistHtml += '</div>';
                checklistHtml += '</div>';
                checklistHtml += "</fieldset>";

                $("#performanceChecklistPanel").append(checklistHtml);

                eval('var ' + fieldsetId + 'Table' + ' = $("#' + fieldsetId + 'Table").DataTable({paging:false,info:false,ordering:false,searching:false});');

                editPerformanceChecklistDataTable(monitoringItemPerformanceArr, eval(fieldsetId + 'Table'), fieldsetId);

                monitoringItemTableDataArr.push(eval(fieldsetId + 'Table'));

                var table = eval(fieldsetId + 'Table');
                table.column(1).visible(false);
                table.column(2).visible(false);

                monitoringItemTableDataArr1.push(fieldsetId);
            }

        }

    }

}

editPerformanceChecklistDataTable = function (monitoringItemPerformanceArr, tableVar, currentFieldsetId) {
    var newArr = [];

    for (var i = 0; i < monitoringItemPerformanceArr.length; i++) {
        var fieldsetId = 'fieldset' + monitoringItemPerformanceArr[i].MonitoringItemManager.MonitoringItemTypeManager.MonitoringItemTypeName.replace(/\s/g, '');

        if (fieldsetId === currentFieldsetId) {
            newArr.push(monitoringItemPerformanceArr[i]);
        }
    }

    for (var fieldsetTableRow = 0; fieldsetTableRow < newArr.length; fieldsetTableRow++) {
        getOverallStatus();
        var selectPerformanceStatus = '<select id="' + currentFieldsetId + fieldsetTableRow + '" name="itemPerformanceSelect" class="form-control">';

        for (var selectPerformanceOptions = 0; selectPerformanceOptions < overallStatusArr.length;) {
            if (overallStatusArr[selectPerformanceOptions] == newArr[fieldsetTableRow].OverallStatusManager.OverallStatusGuid) {
                selectPerformanceStatus += '<option value="' + overallStatusArr[selectPerformanceOptions] + '" selected="selected" >';
                selectPerformanceStatus += overallStatusArr[selectPerformanceOptions + 1];
               
            } else {
                selectPerformanceStatus += '<option value="' + overallStatusArr[selectPerformanceOptions] + '" >';
                selectPerformanceStatus += overallStatusArr[selectPerformanceOptions + 1];
            }
          
            selectPerformanceStatus += '</option>';
            selectPerformanceOptions = selectPerformanceOptions + 2;
        }

        selectPerformanceStatus += '</select>';

        var inputText = '<input id="' + currentFieldsetId + fieldsetTableRow + '" name="itemPerformanceDescription" class="form-control itemPerformanceDescription" type="text" value="' + newArr[fieldsetTableRow].MonitoringItemPerformanceDescription + '" />';

        tableVar.row.add([
            fieldsetTableRow + 1,
            newArr[fieldsetTableRow].MonitoringItemPerformanceGuid,
            newArr[fieldsetTableRow].MonitoringItemManager.MonitoringItemGuid,
            newArr[fieldsetTableRow].MonitoringItemManager.MonitoringItemName,
            selectPerformanceStatus,
            inputText
        ]).draw(false);
    }
}

getOverallStatus = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "EditPerformanceMonitoringDetails.aspx/DisplayOverallStatusList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            overallStatusArr = response.d;            
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getInspectorPosition = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "EditPerformanceMonitoringDetails.aspx/DisplayDesignationList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            inspectorPositionArr = response.d;            
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

updatePerformanceMontioringDetails = function () {
    var data;

    var monitoringItemGuid;
    var monitoringItemPerformanceGuid;
    var monitoringItemPerformanceDescription;
    var itemStatusGuid;
    var performanceMonitoringGuid = $('#performanceMonitoringGuid').val();

    var overallStatusManager = {
        overallStatusGuid: $("#overallStatusSelect").val()
    }

    var designationManager = {
        designationGuid: $("#inspectorPositionSelect").val()
    }

    var inspectionDate = $('#inspectionDate').val();
    inspectionDate = Date.parse(inspectionDate).toString('yyyy/MM/dd');

    var monitoringItemPerformanceManager = [];

    var performanceMonitoringManager = {

        performanceMonitoringGuid: $('#performanceMonitoringGuid').val(),
        inspectorName: $('#inspectorName').val(),
        generalComments: $('#generalComments').val(),
        inspectionDate: inspectionDate,
        overallStatusManager: overallStatusManager,
        designationManager: designationManager,
        monitoringItemPerformanceManager: monitoringItemPerformanceManager
    }

    if (monitoringItemTableDataArr.length != null) {
        for (var i = 0; i < monitoringItemTableDataArr.length; i++) {

            monitoringItemTableDataArr[i].rows().every(function (rowIdx, tableLoop, rowLoop) {
                data = this.data();
                monitoringItemPerformanceGuid = data[1];
                monitoringItemGuid = data[2];
                itemStatusGuid = $('#' + monitoringItemTableDataArr1[i] + 'Table select#' + monitoringItemTableDataArr1[i] + rowIdx).val();
                monitoringItemPerformanceDescription = $('#' + monitoringItemTableDataArr1[i] + 'Table input#' + monitoringItemTableDataArr1[i] + rowIdx).val();

                var monitoringItemManager = {
                    monitoringItemGuid: monitoringItemGuid
                }

                var itemStatusManager = {
                    overallStatusGuid: itemStatusGuid
                }
                monitoringItemPerformanceManager.push({                  
                    monitoringItemPerformanceGuid: monitoringItemPerformanceGuid,
                    monitoringItemManager: monitoringItemManager,
                    monitoringItemPerformanceDescription: monitoringItemPerformanceDescription,
                    overallStatusManager: itemStatusManager

                })
            });
        }
    }
    performanceMonitoringManager.monitoringItemPerformanceManager = monitoringItemPerformanceManager;

    updateItemPerformance(performanceMonitoringManager);
}

updateItemPerformance = function (performanceMonitoringManager) {

    var performanceMonitoringGuid= $('#performanceMonitoringGuid').val();

    var performanceMonitoringManagerJsonString = JSON.stringify(performanceMonitoringManager);

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "EditPerformanceMonitoringDetails.aspx/UpdatePerformanceMontoringData",
        data: '{"performanceMonitoringGuid":"' + performanceMonitoringGuid + '","performanceMonitoringManager":' + performanceMonitoringManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;

    
}
