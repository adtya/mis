﻿$(document).ready(function () {
    var cfgCulture = 'en-IN';
    $.preferCulture(cfgCulture);
    //$.preferCulture("en-IN");

    $('#assetCost').maskMoney();

    var b;
    var selectedDivision;
    var selectedDivisionText;
    var selectedSchemeText;
    var selectedAssetTypeText;
    var selectedAssetNumberText;   
    //$('#assetTypePanel').hide();

    var select = '';
    select += '<option val="">' + "Select Asset Number" + '</option>';
    for (i = 1; i <= 100; i++)
    {
        select += '<option val=' + i + '>' + i + '</option>';
    }

    $('#assetCodeNumberSelect').html(select);
    
    $('#divisionList').on('change', function () {
        $('#schemeSelect').empty();
        selectedDivision = $(this).val();        
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "CreateAsset.aspx/DisplaySchemeNameForDivisions",
            data: '{"divisionGuid":"' + selectedDivision + '"}',
            dataType: "json",
            success: function (data) {
                b = data.d;
                $('#schemeSelect').append("<option value='" + 0 + "'>" + "Select Scheme" + "</option>");
                for (var i = 0; i < b.length; i++) {
                    $('#schemeSelect').append('<option value="' + b[i].SchemeGuid + '">' + b[i].SchemeCode + "-" + b[i].SchemeName + '</option>');
                    
                }

            },
            error: function (result) {
                alert("Error");
            }
        });
    });

    $('#assetTypeList').on('change', function () {
        selectedAssetTypeText = $('#assetTypeList option:selected').text().split("-");

        //if ($('#assetTypeList option:selected').text() == "EMB") {
        //    var html = emb();
        //    $('#assetTypePanel').empty();
        //    $('#assetTypePanel').append(html);
        //    getEmbCrestType();
        //    getEmbFillType();
        //}
        if (selectedAssetTypeText[0] == "EMB") {
                var html = emb();
                $('#assetTypePanel').empty();
                $('#assetTypePanel').append(html);
                getEmbCrestType();
                getEmbFillType();
            }
        else if (selectedAssetTypeText[0] == "BRG") {
            var html = brg();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);
            getBridgeType();
        }

        else if (selectedAssetTypeText[0] == "CAN") {
            var html = can();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);
            getCanalLiningType();
        }

        else if (selectedAssetTypeText[0] == "POR") {
            var html = por();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);
            getPorcupineType();
            getPorMaterialType();
        }

        else if (selectedAssetTypeText[0] == "REV") {
            var html = rev();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);
            getRevetmentType();
            getRevRiverprotType();
            getRevWaveprotType();
        }

        else if (selectedAssetTypeText[0] == "CUL") {
            var html = cul();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);
            getCulvertType();
        }
        else if (selectedAssetTypeText[0] == "DRN") {
            var html = drn();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);
        }
        else if (selectedAssetTypeText[0] == "GAU") {
            var html = gau();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);

            $('#dpStartDate').datepicker({
                format: "dd/mm/yyyy",
            }).on('changeDate', function (ev)
            {
                var startDate = new Date(ev.date.valueOf());
                startDate = startDate.add(1).days();
                $('#dpEndDate').datepicker('setStartDate', startDate);
                $(this).blur();
                $(this).datepicker('hide');
            }).on('clearDate', function (selected)
            {
                $('#dpEndDate').datepicker('setStartDate', null);
            });

            $('#dpEndDate').datepicker({
                format: "dd/mm/yyyy",

            }).on('changeDate', function (ev)
            {
                var endDate = new Date(ev.date.valueOf());
                endDate = endDate.add(-1).days();
                $('#dpStartDate').datepicker('setEndDate', endDate);
                $(this).blur();
                $(this).datepicker('hide');
            }).on('clearDate', function (selected)
            {
                $('#dpStartDate').datepicker('setEndDate', null);
            });

            getGaugeType();
            getGauSeasonType();
            getGauFrequencyType();
        }
        else if (selectedAssetTypeText[0] == "SLU") {
            var html = slu();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);
            getSluiceType();
        }

        else if (selectedAssetTypeText[0] == "DRP") {
            var html = drp();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);
        }

        else if (selectedAssetTypeText[0] == "REG") {
            var html = reg();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);
            getRegulatorType();
            getRegGateType();
        }

        else if (selectedAssetTypeText[0] == "TRN") {
            var html = trn();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);
            getTurnoutType();
            getTrnGateType();
        }

        else if (selectedAssetTypeText[0] == "WEI") {
            var html = wei();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);
            getWeirType();
        }

        else if (selectedAssetTypeText[0] == "SPU") {
            var html = spu();
            $('#assetTypePanel').empty();
            $('#assetTypePanel').append(html);            
            getSpurConstructionType();
            getSpurRevetType();
            getSpurShapeType();
        }
    });

    $('#assetCodeNumberSelect').on('change', function () {
        selectedDivisionText = $('#divisionList option:selected').text().split("-");
        selectedSchemeText = $('#schemeSelect option:selected').text().split("-");
        //selectedAssetTypeText = $('#assetTypeList option:selected').text().split("-");
        selectedAssetNumberText = $('#assetCodeNumberSelect option:selected').text();

    $('#assetCode').val(function (index, val) {           
            return selectedDivisionText[0] + "-" + selectedSchemeText[0] + "-" + selectedAssetTypeText[0] + "-" + selectedAssetNumberText;
        });
    });   

    $('#saveAssetBtn').on('click', function () {
        if ($('#addAssetForm').valid()) {
            saveAsset();
        }         
    });

    $('#cancelAssetBtn').click(function (e) {
        e.preventDefault();

        window.location = 'DBAAssets.aspx';
    });

    $('#dpDateOfCompletionOfAsset').datepicker({
        format: "dd/mm/yyyy",
    }); 

   $('#addAssetForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            divisionList: "required",
            schemeSelect: "required",
            assetTypeList: "required",
            assetCodeNumberSelect: "required",
            bridgeTypeSelect: "required",
            liningTypeSelect: "required",
            culvertTypeSelect: "required",
            crestTypeSelect: "required",
            fillTypeSelect: "required",
            gaugeTypeSelect: "required",
            seasonTypeSelect: "required",
            frequencyTypeSelect: "required",
            porcupineTypeSelect: "required",
            materialTypeSelect: "required",
            regulatorTypeSelect: "required",
            regulatorGateTypeSelect: "required",
            revetmentTypeSelect: "required",
            riverProtectionTypeSelect: "required",
            waveProtectionTypeSelect: "required",
            sluiceTypeSelect: "required",
            shapeTypeSelect: "required",
            constructionTypeSelect: "required",
            spurRevetTypeSelect: "required",
            turnoutTypeSelect: "required",
            turnoutGateTypeSelect: "required",
            weirTypeSelect: "required",
            assetName: {
                required: true,
                noSpace: true
            },
            chainageStart: {
                number: true
            },
            chainageEnd: {
                number: true
            },
            eastingUS: {
                number: true
            },
            eastingDS: {
                number: true
            },
            northingUS: {
                number: true
            },
            northingDS: {
                number: true
            },
            dateOfCompletionOfAsset: {
                required: true,
                dateITA: true
            },            
            lengthBridge: {
                number: true
            },
            numberPeirs: {
                number: true
            },
            roadWidth: {
                number: true
            },
            uSCrestElevationCanal: {
                number: true
            },
            dSCrestElevationCanal: {
                number: true
            },
            lengthCanal: {
                number: true
            },
            bedWidthCanal: {
                number: true
            },
            slipeSlope1Canal: {
                number: true
            },
            slipeSlope2Canal: {
                number: true
            },
            averageDepthCanal: {
                number: true
            },            
            numberOfVents: {
                number: true
            },
            ventHeight: {
                number: true
            },
            ventWidth: {
                number: true
            },
            lengthCulvert: {
                number: true
            },
            usCrestElevationDrainage: {
                number: true
            },
            dsCrestElevationDrainage: {
                number: true
            },
            lengthDrainage: {
                number: true
            },
            BedWidthDrainage: {
                number: true
            },
            slope1Drainage: {
                number: true
            },
            slope2Drainage: {
                number: true
            },
            averageDepthDrainage: {
                number: true
            },
            uSCrestElevation: {
                number: true
            },
            dSCrestElevation: {
                number: true
            },
            length: {
                number: true
            },
            crestWidth: {
                number: true
            },
            csSlope1: {
                number: true
            },
            csSlope2: {
                number: true
            },
            rsSlope1: {
                number: true
            },
            rsSlope2: {
                number: true
            },
            bermSlope1: {
                number: true
            },
            bermSlope2: {
                number: true
            },
            platformWidth: {
                number: true
            },            
            averageHeight: {
                number: true
            },            
            zeroDatum: {
                number: true
            },
            startDate: {
                required: true,
                dateITA: true
            },
            endDate: {
                required: true,
                dateITA: true
            },
            usInvertLevel: {
                number: true
            },
            dsInvertLevel: {
                number: true
            },
            stillingBasinLength: {
                number: true
            },
            stillingBasinWidth: {
                number: true
            },            
            screenLength: {
                number: true
            },
            spacingAlongScreen: {
                number: true
            },
            spacingAlongScreen: {
                number: true
            },
            screenRows: {
                number: true
            },
            noOfLayers: {
                number: true
            },
            mamberLength: {
                number: true
            },
            lengthPorcupine: {
                number: true
            },
            numberOfVentsRegulator: {
                number: true
            },
            ventHeightRegulator: {
                number: true
            },
            ventWidthRegulator: {
                number: true
            },
            slopeRevetment: {
                number: true
            },
            lengthRevetment: {
                number: true
            },
            plainWidth: {
                number: true
            },
            NoOfVentsSluice: {
                number: true
            },
            ventDiameterSluice: {
                number: true
            },
            ventHeightSluice: {
                number: true
            },
            ventWidthSluice: {
                number: true
            },
            orientation: {
                number: true
            },
            lengthSpur: {
                number: true
            },
            widthSpur: {
                number: true
            },
            inletBoxWidth: {
                number: true
            },
            inletBoxLength: {
                number: true
            },
            outletNo: {
                number: true
            },
            outletWidth: {
                number: true
            },
            outletHeight: {
                number: true
            },
            invertLevel: {
                number: true
            },
            crestTopLevel: {
                number: true
            },
            crestTopWidth: {
                number: true
            }
        }
    });

});

function brg() {
    var brgHtml = "<fieldset class=\"scheduler-border\">";
    brgHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Bridge</u></legend>";
    brgHtml += "<div class=\"row\">";
    brgHtml += "<div class=\"col-sm-12 col-md-12\">";

    
    brgHtml += "<div class=\"col-sm-6 col-md-6\">";
    brgHtml += "<div class=\"form-group\">";

    brgHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    brgHtml += "<label for=\"bridgeType\" id=\"bridgeTypeLabel\" class=\"control-label\">Bridge Type</label>";
    brgHtml += "</div>";
    brgHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    brgHtml += "<select id=\"bridgeTypeSelect\" name=\"bridgeTypeSelect\" class=\"form-control\">";
    brgHtml += "</select>";
    brgHtml += "</div>";

    brgHtml += "</div>";
    brgHtml += "</div>";

    brgHtml += "<div class=\"col-sm-6 col-md-6\">";
    brgHtml += "<div class=\"form-group\">";

    brgHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    brgHtml += "<label for=\"lengthBridge\" id=\"lengthBridgeLabel\" class=\"control-label\">Length(m)</label>";
    brgHtml += "</div>";
    brgHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    brgHtml += "<input id=\"lengthBridge\" name=\"lengthBridge\" placeholder=\"Length(m)\" class=\"form-control\" />";
    brgHtml += "</div>";

    brgHtml += "</div>";
    brgHtml += "</div>";

    brgHtml += "</div>";
    brgHtml += "</div>";   
    brgHtml += "<div class=\"row\">";
    brgHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    brgHtml += "<div class=\"col-sm-6 col-md-6\">";
    brgHtml += "<div class=\"form-group\">";

    brgHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    brgHtml += "<label for=\"numberPeirs\" id=\"numberPeirsLabel\" class=\"control-label\">Number Of Peirs</label>";
    brgHtml += "</div>";
    brgHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    brgHtml += "<input id=\"numberPeirs\" name=\"numberPeirs\" placeholder=\"Number Of Peirs\" class=\"form-control\" />";
    brgHtml += "</div>";

    brgHtml += "</div>";
    brgHtml += "</div>";
    brgHtml += "<div class=\"col-sm-6 col-md-6\">";
    brgHtml += "<div class=\"form-group\">";

    brgHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    brgHtml += "<label for=\"roadWidth\" id=\"roadWidthLabel\" class=\"control-label\">Road Width (m)</label>";
    brgHtml += "</div>";
    brgHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    brgHtml += "<input id=\"roadWidth\" name=\"roadWidth\" placeholder=\"Road Width (m)\" class=\"form-control\" />";
    brgHtml += "</div>";

    brgHtml += "</div>";
    brgHtml += "</div>";

    brgHtml += "</div>";
    brgHtml += "</div>";
    brgHtml += "</fieldset>";
    return brgHtml;
}

function can() {
    var canHtml = "<fieldset class=\"scheduler-border\">";;
    canHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Canal</u></legend>";

    canHtml += "<div class=\"row\">";
    canHtml += "<div class=\"col-sm-12 col-md-12\">";

    
    canHtml += "<div class=\"col-sm-6 col-md-6\">";
    canHtml += "<div class=\"form-group\">";

    canHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    canHtml += "<label for=\"uSCrestElevationCanal\" id=\"uSCrestElevationCanalLabel\" class=\"control-label\">U/S Crest Elevation(m)</label>";
    canHtml += "</div>";

    canHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    canHtml += "<input id=\"uSCrestElevationCanal\" name=\"uSCrestElevationCanal\" placeholder=\"U/S Crest Elevation(m)\" class=\"form-control\" />";
    canHtml += "</div>";

    canHtml += "</div>";
    canHtml += "</div>";

    canHtml += "<div class=\"col-sm-6 col-md-6\">";
    canHtml += "<div class=\"form-group\">";

    canHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    canHtml += "<label for=\"dSCrestElevationCanal\" id=\"dSCrestElevationCanalLabel\" class=\"control-label\">D/S Crest Elevation(m)</label>";
    canHtml += "</div>";

    canHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    canHtml += "<input id=\"dSCrestElevationCanal\" name=\"dSCrestElevationCanal\" placeholder=\"D/S Crest Elevation(m)\" class=\"form-control\" />";
    canHtml += "</div>";

    canHtml += "</div>";
    canHtml += "</div>";

    canHtml += "</div>";
    canHtml += "</div>";

    canHtml += "<div class=\"row\">";
    canHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    canHtml += "<div class=\"col-sm-6 col-md-6\">";
    canHtml += "<div class=\"form-group\">";
    canHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    canHtml += "<label for=\"lengthCanal\" id=\"LengthCanalLabel\" class=\"control-label\">Length (m)</label>";
    canHtml += "</div>";
    canHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    canHtml += "<input id=\"lengthCanal\" name=\"lengthCanal\" placeholder=\"Length (m)\" class=\"form-control\" />";
    canHtml += "</div>";
    canHtml += "</div>";
    canHtml += "</div>";

    canHtml += "<div class=\"col-sm-6 col-md-6\">";
    canHtml += "<div class=\"form-group\">";    
    canHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    canHtml += "<label for=\"bedWidthCanal\" id=\"bedWidthCanalLabel\" class=\"control-label\">Bed Width(m)</label>";
    canHtml += "</div>";
    canHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    canHtml += "<input id=\"bedWidthCanal\" name=\"bedWidthCanal\" placeholder=\"Bed Width(m)\" class=\"form-control\" />";
    canHtml += "</div>";
    canHtml += "</div>";
    canHtml += "</div>";

    canHtml += "</div>";
    canHtml += "</div>";
    

    canHtml += "<div class=\"row\">";
    canHtml += "<div class=\"col-sm-12 col-md-12\">";

    canHtml += "<div class=\"col-sm-6 col-md-6\">";
    canHtml += "<div class=\"form-group\">";
    canHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    canHtml += "<label for=\"slipeSlopeCanal\" id=\"slipeSlopeCanalLabel\" class=\"control-label\">Slipe Slope</label>";
    canHtml += "</div>";
    canHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    canHtml += '<div class="input-group">';
    canHtml += '<input id="slipeSlope1Canal" name="slipeSlope1Canal" placeholder="Slope" class="form-control" />';
    canHtml += '<span class="input-group-addon" value=\":\">:</span>';
    canHtml += '<input id="slipeSlope2Canal" name="slipeSlope2Canal" placeholder="Slope" class="form-control" />';
    canHtml += '</div>';
    canHtml += '</div>';

    canHtml += "</div>";
    canHtml += "</div>";

    canHtml += "<div class=\"col-sm-6 col-md-6\">";
    canHtml += "<div class=\"form-group\">";
    
    canHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    canHtml += "<label for=\"averageDepthCanal\" id=\"averageDepthCanalLabel\" class=\"control-label\">Average Depth</label>";
    canHtml += "</div>";
    canHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    canHtml += "<input id=\"averageDepthCanal\" name=\"averageDepthCanal\" placeholder=\"Average Depth\" class=\"form-control\" />";
    canHtml += "</div>";
    canHtml += "</div>";
    canHtml += "</div>";

    canHtml += "</div>";
    canHtml += "</div>";

    canHtml += "<div class=\"row\">";
    canHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    canHtml += "<div class=\"col-sm-6 col-md-6\">";
    canHtml += "<div class=\"form-group\">";

    canHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    canHtml += "<label for=\"liningType\" id=\"liningTypeLabel\" class=\"control-label\">Lining Type</label>";
    canHtml += "</div>";

    canHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    canHtml += "<select id=\"liningTypeSelect\" name=\"liningTypeSelect\" class=\"form-control\">";
    canHtml += "</select>";

    canHtml += "</div>";
    canHtml += "</div>";
    canHtml += "</div>";
    canHtml += "</div>";
    
    canHtml += "<div class=\"row\">";
    canHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    canHtml += "<div class=\"col-sm-6 col-md-6\">";    
    canHtml += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    canHtml += "<div class=\"form-group\">";
    canHtml += "<label for=\"appurtenantAssetsCanal\" id=\"appurtenantAssetsCanalLabel\" class=\"control-label\">Appurtenant Assets</label>";
    canHtml += "</div>";
    canHtml += "</div>";
    canHtml += "</div>";

    canHtml += "</div>";
    canHtml += "</div>";

    canHtml += "<div class=\"row\">";
    canHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    canHtml += "<div class=\"col-sm-6 col-md-6\">";
    canHtml += "<div class=\"form-group\">";
    canHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    //embHtml +="<GridView id="GridView1" runat="server"></asp:GridView>
    canHtml += "</div>";
    canHtml += "</div>";
    canHtml += "</div>";
    canHtml += "</div>";
    canHtml += "</div>";
    canHtml += "</fieldset>";
    return canHtml;
}

function cul() {
    var culHtml = "<fieldset class=\"scheduler-border\">";
    culHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Culvert</u></legend>";
    culHtml += "<div class=\"row\">";
    culHtml += "<div class=\"col-sm-12 col-md-12\">";

    culHtml += "<div class=\"col-sm-6 col-md-6\">";
    culHtml += "<div class=\"form-group\">";

    culHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    culHtml += "<label for=\"culvertType\" id=\"culvertTypeLabel\" class=\"control-label\">Culvert Type</label>";
    culHtml += "</div>";
    culHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    culHtml += "<select id=\"culvertTypeSelect\" name=\"culvertTypeSelect\" class=\"form-control\">";
    culHtml += "</select>";
    culHtml += "</div>";
    culHtml += "</div>";
    culHtml += "</div>";
    culHtml += "<div class=\"col-sm-6 col-md-6\">";
    culHtml += "<div class=\"form-group\">";
    culHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    culHtml += "<label for=\"numberOfVents\" id=\"NumberOfVentsLabel\" class=\"control-label\">Number Of Vents</label>";
    culHtml += "</div>";
    culHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    culHtml += "<input id=\"numberOfVents\" name=\"numberOfVents\" placeholder=\"Number Of Vents\" class=\"form-control\" />";
    culHtml += "</div>";

    culHtml += "</div>";
    culHtml += "</div>";
    culHtml += "</div>";
    culHtml += "</div>";
    culHtml += "<div class=\"row\">";
    culHtml += "<div class=\"col-sm-12 col-md-12\">";

    culHtml += "<div class=\"col-sm-6 col-md-6\">";
    culHtml += "<div class=\"form-group\">";
    culHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    culHtml += "<label for=\"ventHeight\" id=\"ventHeightLabel\" class=\"control-label\">Vent Height</label>";
    culHtml += "</div>";
    culHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    culHtml += "<input id=\"ventHeight\" name=\"ventHeight\" placeholder=\"Vent Height\" class=\"form-control\" />";
    culHtml += "</div>";
    culHtml += "</div>";
    culHtml += "</div>";
    culHtml += "<div class=\"col-sm-6 col-md-6\">";
    culHtml += "<div class=\"form-group\">";
    culHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    culHtml += "<label for=\"ventWidth\" id=\"ventWidthLabel\" class=\"control-label\">Vent Width</label>";
    culHtml += "</div>";
    culHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    culHtml += "<input id=\"ventWidth\" name=\"ventWidth\" placeholder=\"Vent Width\" class=\"form-control\" />";
    culHtml += "</div>";

    culHtml += "</div>";
    culHtml += "</div>";
    culHtml += "</div>";
    culHtml += "</div>";
    culHtml += "<div class=\"row\">";
    culHtml += "<div class=\"col-sm-12 col-md-12\">";

    culHtml += "<div class=\"col-sm-6 col-md-6\">";
    culHtml += "<div class=\"form-group\">";
    culHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    culHtml += "<label for=\"lengthCulvert\" id=\"lengthCulvertLabel\" class=\"control-label\">Length (m)</label>";
    culHtml += "</div>";
    culHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    culHtml += "<input id=\"lengthCulvert\" name=\"lengthCulvert\" placeholder=\"Length (m)\" class=\"form-control\" />";
    culHtml += "</div>";

    culHtml += "</div>";
    culHtml += "</div>";
    culHtml += "</div>";
    culHtml += "</div>";

    culHtml += "</fieldset>";
    return culHtml;
}

function drn() {
    var drnHtml = "<fieldset class=\"scheduler-border\">";
    drnHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Drainage</u></legend>";

    drnHtml += "<div class=\"row\">";
    drnHtml += "<div class=\"col-sm-12 col-md-12\">";
   
    drnHtml += "<div class=\"col-sm-6 col-md-6\">";
    drnHtml += "<div class=\"form-group\">";
    drnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    drnHtml += "<label for=\"usCrestElevationDrainage\" id=\"usCrestElevationDrainageLabel\" class=\"control-label\">U/S Crest Elevation(m)</label>";
    drnHtml += "</div>";
    drnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    drnHtml += "<input id=\"usCrestElevationDrainage\" name=\"usCrestElevationDrainage\" placeholder=\"U/S Crest Elevation(m)\" class=\"form-control\" />";
    drnHtml += "</div>";

    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "<div class=\"col-sm-6 col-md-6\">";
    drnHtml += "<div class=\"form-group\">";
    drnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    drnHtml += "<label for=\"dsCrestElevationDrainage\" id=\"dsCrestElevationDrainageLabel\" class=\"control-label\">D/S Crest Elevation(m)</label>";
    drnHtml += "</div>";
    drnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    drnHtml += "<input id=\"dsCrestElevationDrainage\" name=\"dsCrestElevationDrainage\" placeholder=\"D/S Crest Elevation(m)\" class=\"form-control\" />";
    drnHtml += "</div>";

    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "</div>";

    drnHtml += "<div class=\"row\">";
    drnHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    drnHtml += "<div class=\"col-sm-6 col-md-6\">";
    drnHtml += "<div class=\"form-group\">";
    drnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    drnHtml += "<label for=\"lengthDrainage\" id=\"lengthDrainageLabel\" class=\"control-label\">Length</label>";
    drnHtml += "</div>";
    drnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    drnHtml += "<input id=\"lengthDrainage\" name=\"lengthDrainage\" placeholder=\"Length\" class=\"form-control\" />";
    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "<div class=\"col-sm-6 col-md-6\">";
    drnHtml += "<div class=\"form-group\">";
    drnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    drnHtml += "<label for=\"BedWidthDrainage\" id=\"BedWidthDrainageLabel\" class=\"control-label\">Bed Width(m)</label>";
    drnHtml += "</div>";
    drnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    drnHtml += "<input id=\"BedWidthDrainage\" name=\"BedWidthDrainage\" placeholder=\"Bed Width(m)\" class=\"form-control\" />";
    drnHtml += "</div>";

    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "</div>";

    drnHtml += "<div class=\"row\">";
    drnHtml += "<div class=\"col-sm-12 col-md-12\">";

    
    drnHtml += "<div class=\"col-sm-6 col-md-6\">";
    drnHtml += "<div class=\"form-group\">";

    drnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    drnHtml += "<label for=\"slopeDrainage\" id=\"slopeDrainageLabel\" class=\"control-label\">Slope</label>";
    drnHtml += "</div>";

    drnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    drnHtml += '<div class="input-group">';
    drnHtml += '<input id="slope1Drainage" name="slope1Drainage" placeholder="Slope" class="form-control" />';
    drnHtml += '<span class="input-group-addon" value=\":\">:</span>';
    drnHtml += '<input id="slope2Drainage" name="slope2Drainage" placeholder="Slope" class="form-control" />';
    drnHtml += '</div>';
    drnHtml += '</div>';

    drnHtml += "</div>";
    drnHtml += "</div>";

    drnHtml += "<div class=\"col-sm-6 col-md-6\">";
    drnHtml += "<div class=\"form-group\">";

    drnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    drnHtml += "<label for=\"averageDepthDrainage\" id=\"averageDepthDrainageLabel\" class=\"control-label\">Average Depth(m)</label>";
    drnHtml += "</div>";

    drnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    drnHtml += "<input id=\"averageDepthDrainage\" name=\"averageDepthDrainage\" placeholder=\"Average Depth(m)\" class=\"form-control\" />";
    drnHtml += '</div>';

    drnHtml += '</div>';
    drnHtml += "</div>";

    drnHtml += "</div>";
    drnHtml += "</div>";
    

    drnHtml += "<div class=\"row\">";
    drnHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    drnHtml += "<div class=\"col-sm-6 col-md-6\">";
    drnHtml += "<div class=\"form-group\">";
    drnHtml += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    drnHtml += "<label for=\"appurtenantAssets\" id=\"appurtenantAssetsLabel\" class=\"control-label\">Appurtenant Assets</label>";
    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "</div>";

    drnHtml += "<div class=\"row\">";
    drnHtml += "<div class=\"col-sm-12 col-md-12\">";    
    drnHtml += "<div class=\"col-sm-6 col-md-6\">";
    drnHtml += "<div class=\"form-group\">";
    drnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    //drnHtml +="<GridView id="GridView1" runat="server"></asp:GridView>
    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "</div>";
    drnHtml += "</fieldset>";
    return drnHtml;
}

function emb() {
    var embHtml = "<fieldset class=\"scheduler-border\">";
    embHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Embankment</u></legend>";
    embHtml += "<div class=\"row\">";
    embHtml += "<div class=\"col-sm-12 col-md-12\">";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";
    embHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    embHtml += "<label for=\"uSCrestElevation\" id=\"uSCrestElevationLabel\" class=\"control-label\">U/S Crest Elevation(m)</label>";
    embHtml += "</div>";
    embHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    embHtml += "<input id=\"uSCrestElevation\" name=\"uSCrestElevation\" placeholder=\"U/S Crest Elevation\" class=\"form-control\" />";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";
    embHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    embHtml += "<label for=\"dSCrestElevation\" id=\"dSCrestElevationLabel\" class=\"control-label\">D/S Crest Elevation(m)</label>";
    embHtml += "</div>";
    embHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    embHtml += "<input id=\"dSCrestElevation\" name=\"dSCrestElevation\" placeholder=\"D/S Crest Elevation\" class=\"form-control\" />";
    embHtml += "</div>";

    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "<div class=\"row\">";
    embHtml += "<div class=\"col-sm-12 col-md-12\">";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";
    embHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    embHtml += "<label for=\"length\" id=\"LengthLabel\" class=\"control-label\">Length (m)</label>";
    embHtml += "</div>";
    embHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    embHtml += "<input id=\"length\" name=\"length\" placeholder=\"Length (m)\" class=\"form-control\" />";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";
    embHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    embHtml += "<label for=\"crestWidth\" id=\"crestWidthLabel\" class=\"control-label\">Crest Width (m)</label>";
    embHtml += "</div>";
    embHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    embHtml += "<input id=\"crestWidth\" name=\"crestWidth\" placeholder=\"Crest Width (m)\" class=\"form-control\" />";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "<div class=\"row\">";
    embHtml += "<div class=\"col-sm-12 col-md-12\">";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";
    embHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    embHtml += "<label for=\"csSlope\" id=\"csSlopeLabel\" class=\"control-label\">CS Slope</label>";
    embHtml += "</div>";
    embHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    embHtml += '<div class="input-group">';
    embHtml += "<input id=\"csSlope1\" name=\"csSlope1\" placeholder=\"CS Slope\" class=\"form-control\" />";
    embHtml += '<span class="input-group-addon" value=\":\">:</span>';
    embHtml += "<input id=\"csSlope2\" name=\"csSlope2\" placeholder=\"CS Slope\" class=\"form-control\" />";
    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";

    embHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    embHtml += "<label for=\"rsSlope\" id=\"rsSlopeLabel\" class=\"control-label\">RS Slope</label>";
    embHtml += "</div>";
    embHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    embHtml += '<div class="input-group">';
    embHtml += "<input id=\"rsSlope1\" name=\"rsSlope1\" placeholder=\"RS Slope\" class=\"form-control\" />";
    embHtml += '<span class="input-group-addon" value=\":\">:</span>';
    embHtml += "<input id=\"rsSlope2\" name=\"rsSlope2\" placeholder=\"RS Slope\" class=\"form-control\" />";
    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "</div>";
    embHtml += "</div>";


    embHtml += "<div class=\"row\">";
    embHtml += "<div class=\"col-sm-12 col-md-12\">";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";
    embHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    embHtml += "<label for=\"bermSlope\" id=\"bermSlopeLabel\" class=\"control-label\">Berm Slope</label>";
    embHtml += "</div>";

    embHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    embHtml += '<div class="input-group">';
    embHtml += "<input id=\"bermSlope1\" name=\"bermSlope1\" placeholder=\"Berm Slope\" class=\"form-control\" />";
    embHtml += '<span class="input-group-addon" value=\":\">:</span>';
    embHtml += "<input id=\"bermSlope2\" name=\"bermSlope2\" placeholder=\"Berm Slope\" class=\"form-control\" />";
    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";
    embHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    embHtml += "<label for=\"platformWidth\" id=\"PlatformWidthLabel\" class=\"control-label\">Platform Width (m)</label>";
    embHtml += "</div>";
    embHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    embHtml += "<input id=\"platformWidth\" name=\"platformWidth\" placeholder=\"Platform Width (m)\" class=\"form-control\" />";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "<div class=\"row\">";
    embHtml += "<div class=\"col-sm-12 col-md-12\">";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";
    embHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    embHtml += "<label for=\"crestType\" id=\"crestTypeLabel\" class=\"control-label\">Crest Type</label>";
    embHtml += "</div>";
    embHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    embHtml += "<select id=\"crestTypeSelect\" name=\"crestTypeSelect\" class=\"form-control\">";
    embHtml += "</select>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";
    embHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    embHtml += "<label for=\"fillType\" id=\"fillTypeLabel\" class=\"control-label\">Fill Type</label>";
    embHtml += "</div>";
    embHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    embHtml += "<select id=\"fillTypeSelect\" name=\"fillTypeSelect\" class=\"form-control\">";
    embHtml += "</select>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "<div class=\"row\">";
    embHtml += "<div class=\"col-sm-12 col-md-12\">";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";
    embHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    embHtml += "<label for=\"averageHeight\" id=\"averageHeightLabel\" class=\"control-label\">Average Height (m)</label>";
    embHtml += "</div>";
    embHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    embHtml += "<input id=\"averageHeight\" name=\"averageHeight\" placeholder=\"Average Height (m)\" class=\"form-control\" />";
    embHtml += "</div>";

    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "<div class=\"row\">";
    embHtml += "<div class=\"col-sm-12 col-md-12\">";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";
    embHtml += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    embHtml += "<label for=\"appurtenantAssets\" id=\"appurtenantAssetsLabel\" class=\"control-label\">Appurtenant Assets</label>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "</div>";
    embHtml += "</div>";

    embHtml += "<div class=\"row\">";
    embHtml += "<div class=\"col-sm-12 col-md-12\">";

    embHtml += "<div class=\"col-sm-6 col-md-6\">";
    embHtml += "<div class=\"form-group\">";
    embHtml += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    //embHtml +="<GridView id="GridView1" runat="server"></asp:GridView>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</div>";
    embHtml += "</fieldset>";
    return embHtml;
}

function gau() {
    var gauHtml = "<fieldset class=\"scheduler-border\">";
    gauHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Gauge</u></legend>";
    gauHtml += "<div class=\"row\">";
    gauHtml += "<div class=\"col-sm-12 col-md-12\">";

    gauHtml += "<div class=\"col-sm-6 col-md-6\">";
    gauHtml += "<div class=\"form-group\">";
    gauHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    gauHtml += "<label for=\"gaugeType\" id=\"GaugeTypeLabel\" class=\"control-label\">Gauge Type</label>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    gauHtml += "<select id=\"gaugeTypeSelect\" name=\"gaugeTypeSelect\" class=\"form-control\">";
    gauHtml += "</select>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-sm-6 col-md-6\">";
    gauHtml += "<div class=\"form-group\">";
    gauHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    gauHtml += "<label for=\"seasonType\" id=\"SeasonTypeLabel\" class=\"control-label\">Season Type</label>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    gauHtml += "<select id=\"seasonTypeSelect\" name=\"seasonTypeSelect\" class=\"form-control\">";
    gauHtml += "</select>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";

    gauHtml += "<div class=\"row\">";
    gauHtml += "<div class=\"col-sm-12 col-md-12\">";

    gauHtml += "<div class=\"col-sm-6 col-md-6\">";
    gauHtml += "<div class=\"form-group\">";
    gauHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    gauHtml += "<label for=\"frequencyType\" id=\"frequencyTypeLabel\" class=\"control-label\" class=\"form-control\">Frequency Type</label>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    gauHtml += "<select id=\"frequencyTypeSelect\" name=\"frequencyTypeSelect\" class=\"form-control\">";
    gauHtml += "</select>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-sm-6 col-md-6\">";
    gauHtml += "<div class=\"form-group\">";
    gauHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    gauHtml += "<label for=\"zeroDatum\" id=\"zeroDatumLabel\" class=\"control-label\">Zero Datum(m)</label>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    gauHtml += "<input id=\"zeroDatum\" name=\"zeroDatum\" placeholder=\"Zero Datum(m)\" class=\"form-control\" />";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";

    gauHtml += "<div class=\"row\">";
    gauHtml += "<div class=\"col-sm-12 col-md-12\">";

    gauHtml += "<div class=\"col-sm-6 col-md-6\">";
    gauHtml += "<div class=\"form-group\">";
    gauHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    gauHtml += "<label for=\"startDate\" id=\"StartDateLabel\" class=\"control-label\">Start Date</label>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    gauHtml += "<div id=\"dpStartDate\" class=\"input-group date\">";
    gauHtml += "<input type=\"text\" name=\"startDate\" class=\"form-control\" id=\"startDate\" placeholder=\"Start Date (dd/mm/yyyy)\" />";
    gauHtml += "<span class=\"input-group-addon form-control-static\">";
    gauHtml += "<i class=\"fa fa-calendar fa-fw\">";
    gauHtml += "</i>";
    gauHtml += "</span>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-sm-6 col-md-6\">";
    gauHtml += "<div class=\"form-group\">";
    gauHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    gauHtml += "<label for=\"endDate\" id=\"endDateLabel\" class=\"control-label\">End Date</label>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    gauHtml +="<div id=\"dpEndDate\" class=\"input-group date\">";
    gauHtml +="<input type=\"text\" name=\"endDate\" class=\"form-control\" id=\"endDate\" placeholder=\"End Date (dd/mm/yyyy)\" />";
    gauHtml +="<span class=\"input-group-addon form-control-static\">";
    gauHtml += "<i class=\"fa fa-calendar fa-fw\">";
    gauHtml += "</i>";
    gauHtml +="</span>";
    gauHtml +="</div>";
    //gauHtml += "<input id=\"endDate\" name=\"endDate\" placeholder=\"End Date\" class=\"form-control\" />";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"row\">";
    gauHtml += "<div class=\"col-sm-12 col-md-12\">";

    gauHtml += "<div class=\"col-sm-3 col-md-3\">";
    gauHtml += "<div class=\"form-group\">";
    gauHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    gauHtml += "<label for=\"active\" id=\"activeLabel\" class=\"control-label\">Active</label>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    gauHtml += "<input id=\"active\" type=\"checkbox\" name=\"active\" class=\"form-control\" />";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";

    gauHtml += "<div class=\"col-sm-3 col-md-3\">";
    gauHtml += "<div class=\"form-group\">";
    gauHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    gauHtml += "<label for=\"lwlPresent\" id=\"lwlPresentLabel\" class=\"control-label\">LWL Present</label>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    gauHtml += "<input id=\"lwlPresent\" type=\"checkbox\" name=\"lwlPresent\" class=\"form-control\" />";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";

    gauHtml += "<div class=\"col-sm-3 col-md-3\">";
    gauHtml += "<div class=\"form-group\">";
    gauHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    gauHtml += "<label for=\"hwlPresent\" id=\"hwlPresentLabel\" class=\"control-label\">HWL Present</label>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    gauHtml += "<input id=\"hwlPresent\" type=\"checkbox\" name=\"hwlPresent\" class=\"form-control\" />";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";

    gauHtml += "<div class=\"col-sm-3 col-md-3\">";
    gauHtml += "<div class=\"form-group\">";
    gauHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    gauHtml += "<label for=\"levelGeo\" id=\"levelGeoLabel\" class=\"control-label\">Level Geo</label>";
    gauHtml += "</div>";
    gauHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    gauHtml += "<input id=\"levelGeo\" type=\"checkbox\" name=\"levelGeo\" class=\"form-control\" />";
    gauHtml += "</div>";

    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</div>";
    gauHtml += "</fieldset>";
    return gauHtml;
}

function drp() {
    var drpHtml = "<fieldset class=\"scheduler-border\">";
    drpHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Drop Structure</u></legend>";
    drpHtml += "<div class=\"row\">";
    drpHtml += "<div class=\"col-sm-12 col-md-12\">";


    drpHtml += "<div class=\"col-sm-6 col-md-6\">";
    drpHtml += "<div class=\"form-group\">";
    drpHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    drpHtml += "<label for=\"usInvertLevel\" id=\"usInvertLevelLabel\" class=\"control-label\">U/S Invert Level</label>";
    drpHtml += "</div>";
    drpHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    drpHtml += "<input id=\"usInvertLevel\" name=\"usInvertLevel\" placeholder=\"U/S Invert Level\" class=\"form-control\" />";
    drpHtml += "</div>";
    drpHtml += "</div>";
    drpHtml += "</div>";
    drpHtml += "<div class=\"col-sm-6 col-md-6\">";
    drpHtml += "<div class=\"form-group\">";
    drpHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    drpHtml += "<label for=\"dsInvertLevel\" id=\"dsInvertLevelLabel\" class=\"control-label\">D/S Invert Level</label>";
    drpHtml += "</div>";
    drpHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    drpHtml += "<input id=\"dsInvertLevel\" name=\"dsInvertLevel\" placeholder=\"D/S Invert Level\" class=\"form-control\" />";
    drpHtml += "</div>";

    drpHtml += "</div>";
    drpHtml += "</div>";
    drpHtml += "</div>";
    drpHtml += "</div>";
    drpHtml += "<div class=\"row\">";
    drpHtml += "<div class=\"col-sm-12 col-md-12\">";

    drpHtml += "<div class=\"col-sm-6 col-md-6\">";
    drpHtml += "<div class=\"form-group\">";
    drpHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    drpHtml += "<label for=\"stillingBasinLength\" id=\"stillingBasinLengthLabel\" class=\"control-label\">Stilling Basin Length(m)</label>";
    drpHtml += "</div>";
    drpHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    drpHtml += "<input id=\"stillingBasinLength\" name=\"stillingBasinLength\" placeholder=\"Stilling Basin Length(m)\" class=\"form-control\" />";
    drpHtml += "</div>";
    drpHtml += "</div>";
    drpHtml += "</div>";

    drpHtml += "<div class=\"col-sm-6 col-md-6\">";
    drpHtml += "<div class=\"form-group\">";
    drpHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    drpHtml += "<label for=\"stillingBasinWidth\" id=\"stillingBasinWidthLabel\" class=\"control-label\">Stilling Basin Width (m)</label>";
    drpHtml += "</div>";
    drpHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    drpHtml += "<input id=\"stillingBasinWidth\" name=\"stillingBasinWidth\" placeholder=\"Stilling Basin Width (m)\" class=\"form-control\" />";
    drpHtml += "</div>";

    drpHtml += "</div>";
    drpHtml += "</div>";
    drpHtml += "</div>";
    drpHtml += "</div>";
    drpHtml += "</fieldset>";
    return drpHtml;
}

function por() {
    var porHtml = "<fieldset class=\"scheduler-border\">";
    porHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Porcupine</u></legend>";
    porHtml += "<div class=\"row\">";
    porHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    porHtml += "<div class=\"col-sm-6 col-md-6\">";
    porHtml += "<div class=\"form-group\">";
    porHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    porHtml += "<label for=\"porcupineType\" id=\"porcupineTypeLabel\" class=\"control-label\">Porcupine Type</label>";
    porHtml += "</div>";
    porHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    porHtml += "<select id=\"porcupineTypeSelect\" name=\"porcupineTypeSelect\" class=\"form-control\">";    
    porHtml += "</select>";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "<div class=\"col-sm-6 col-md-6\">";
    porHtml += "<div class=\"form-group\">";
    porHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    porHtml += "<label for=\"materialType\" id=\"materialTypeLabel\" class=\"control-label\">Material Type</label>";
    porHtml += "</div>";
    porHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    porHtml += "<select id=\"materialTypeSelect\" name=\"materialTypeSelect\" class=\"form-control\">";    
    porHtml += "</select>";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</div>";

    porHtml += "<div class=\"row\">";
    porHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    porHtml += "<div class=\"col-sm-6 col-md-6\">";
    porHtml += "<div class=\"form-group\">";
    porHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    porHtml += "<label for=\"screenLength\" id=\"screenLengthLabel\" class=\"control-label\">Screen Length</label>";
    porHtml += "</div>";
    porHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    porHtml += "<input id=\"screenLength\" name=\"screenLength\" placeholder=\"Screen Length\" class=\"form-control\" />";
    porHtml += "</div>";

    porHtml += "</div>";
    porHtml += "</div>";

    porHtml += "<div class=\"col-sm-6 col-md-6\">";
    porHtml += "<div class=\"form-group\">";

    porHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    porHtml += "<label for=\"spacingAlongScreen\" id=\"spacingAlongScreenLabel\" class=\"control-label\">Spacing Along Screen</label>";
    porHtml += "</div>";
    porHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    porHtml += "<input id=\"spacingAlongScreen\" name=\"spacingAlongScreen\" placeholder=\"Spacing Along Screen\" class=\"form-control\" />";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</div>";

    porHtml += "</div>";
    porHtml += "</div>";

    porHtml += "<div class=\"row\">";
    porHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    porHtml += "<div class=\"col-sm-6 col-md-6\">";
    porHtml += "<div class=\"form-group\">";
    porHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    porHtml += "<label for=\"screenRows\" id=\"screenRowsLabel\" class=\"control-label\">Screen Rows</label>";
    porHtml += "</div>";
    porHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    porHtml += "<input id=\"screenRows\" name=\"screenRows\" placeholder=\"Screen Rows\" class=\"form-control\" />";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "<div class=\"col-sm-6 col-md-6\">";
    porHtml += "<div class=\"form-group\">";
    porHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    porHtml += "<label for=\"noOfLayers\" id=\"noOfLayersLabel\" class=\"control-label\">No Of Layers</label>";
    porHtml += "</div>";
    porHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    porHtml += "<input id=\"noOfLayers\" name=\"noOfLayers\" placeholder=\"No Of Layers\" class=\"form-control\" />";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</div>";

    porHtml += "<div class=\"row\">";
    porHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    porHtml += "<div class=\"col-sm-6 col-md-6\">";
    porHtml += "<div class=\"form-group\">";
    porHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    porHtml += "<label for=\"mamberLength\" id=\"MamberLengthLabel\" class=\"control-label\">Mamber Length</label>";
    porHtml += "</div>";
    porHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    porHtml += "<input id=\"mamberLength\" name=\"mamberLength\" placeholder=\"Mamber Length\" class=\"form-control\" />";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "<div class=\"col-sm-6 col-md-6\">";
    porHtml += "<div class=\"form-group\">";

    porHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    porHtml += "<label for=\"lengthPorcupine\" id=\"lengthPorcupineLabel\" class=\"control-label\">Length</label>";
    porHtml += "</div>";
    porHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    porHtml += "<input id=\"lengthPorcupine\" name=\"lengthPorcupine\" placeholder=\"Length\" class=\"form-control\" />";
    porHtml += "</div>";

    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</div>";
    porHtml += "</fieldset>";
    return porHtml;
}

function reg() {
    var regHtml = "<fieldset class=\"scheduler-border\">";
    regHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Regulator</u></legend>";
    regHtml += "<div class=\"row\">";
    regHtml += "<div class=\"col-sm-12 col-md-12\">";
       
    regHtml += "<div class=\"col-sm-6 col-md-6\">";
    regHtml += "<div class=\"form-group\">";
    regHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    regHtml += "<label for=\"numberOfVentsRegulator\" id=\"numberOfVentsRegulatorLabel\" class=\"control-label\">Number Of Vents</label>";
    regHtml += "</div>";
    regHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    regHtml += "<input id=\"numberOfVentsRegulator\" name=\"numberOfVentsRegulator\" placeholder=\"Number Of Vents\" class=\"form-control\" />";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "<div class=\"col-sm-6 col-md-6\">";
    regHtml += "<div class=\"form-group\">";
    regHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    regHtml += "<label for=\"ventHeightRegulator\" id=\"ventHeightRegulatorLabel\" class=\"control-label\">Vent Height</label>";
    regHtml += "</div>";
    regHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    regHtml += "<input id=\"ventHeightRegulator\" name=\"ventHeightRegulator\" placeholder=\"Vent Height\" class=\"form-control\" />";    
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "</div>";

    regHtml += "<div class=\"row\">";
    regHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    regHtml += "<div class=\"col-sm-6 col-md-6\">";
    regHtml += "<div class=\"form-group\">";
    regHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    regHtml += "<label for=\"ventWidthRegulator\" id=\"ventWidthLabelRegulator\" class=\"control-label\">Vent Width</label>";
    regHtml += "</div>";
    regHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    regHtml += "<input id=\"ventWidthRegulator\" name=\"ventWidthRegulator\" placeholder=\"Vent Width\" class=\"form-control\" />";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "<div class=\"col-sm-6 col-md-6\">";
    regHtml += "<div class=\"form-group\">";
    regHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    regHtml += "<label for=\"regulatorType\" id=\"regulatorTypeLabel\" class=\"control-label\">Regulator Type</label>";
    regHtml += "</div>";
    regHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    regHtml += "<select id=\"regulatorTypeSelect\" name=\"regulatorTypeSelect\" class=\"form-control\">";
    regHtml += "</select>";    
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "</div>";

    regHtml += "<div class=\"row\">";
    regHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    regHtml += "<div class=\"col-sm-6 col-md-6\">";
    regHtml += "<div class=\"form-group\">";
    regHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    regHtml += "<label for=\"regulatorGateType\" id=\"regulatorGateTypeLabel\" class=\"control-label\">Gate Type</label>";
    regHtml += "</div>";
    regHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    regHtml += "<select id=\"regulatorGateTypeSelect\" name=\"regulatorGateTypeSelect\" class=\"form-control\">";
    regHtml += "</select>";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "<div class=\"col-sm-6 col-md-6\">";
    regHtml += "<div class=\"form-group\">";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "</div>";
    regHtml += "</fieldset>";
    return regHtml;
}

function rev() {
    var revHtml = "<fieldset class=\"scheduler-border\">";
    revHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Revetment</u></legend>";
    revHtml += "<div class=\"row\">";
    revHtml += "<div class=\"col-sm-12 col-md-12\">";

    revHtml += "<div class=\"col-sm-6 col-md-6\">";
    revHtml += "<div class=\"form-group\">";
    revHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    revHtml += "<label for=\"revetmentType\" id=\"revetmentTypeLabel\" class=\"control-label\">Revetment Type</label>";
    revHtml += "</div>";
    revHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    revHtml += "<select id=\"revetmentTypeSelect\" name=\"revetmentTypeSelect\" class=\"form-control\">";
    revHtml += "</select>";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "<div class=\"col-sm-6 col-md-6\">";
    revHtml += "<div class=\"form-group\">";
    revHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    revHtml += "<label for=\"riverProtectionType\" id=\"riverProtectionTypeLabel\" class=\"control-label\">River Protection Type</label>";

    revHtml += "</div>";
    revHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    revHtml += "<select id=\"riverProtectionTypeSelect\" name=\"riverProtectionTypeSelect\" class=\"form-control\">";
    revHtml += "</select>";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</div>";

    revHtml += "<div class=\"row\">";
    revHtml += "<div class=\"col-sm-12 col-md-12\">";

    revHtml += "<div class=\"col-sm-6 col-md-6\">";
    revHtml += "<div class=\"form-group\">";
    revHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    revHtml += "<label for=\"waveProtectionType\" id=\"waveProtectionTypeLabel\" class=\"control-label\">Wave Protection Type</label>";
    revHtml += "</div>";
    revHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    revHtml += "<select id=\"waveProtectionTypeSelect\" name=\"waveProtectionTypeSelect\" class=\"form-control\">";
    revHtml += "</select>";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</div>";

    revHtml += "<div class=\"col-sm-6 col-md-6\">";
    revHtml += "<div class=\"form-group\">";
    revHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    revHtml += "<label for=\"slopeRevetment\" id=\"slopeRevetmentLabel\" class=\"control-label\">Slope(m)</label>";
    revHtml += "</div>";
    revHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    revHtml += "<input id=\"slopeRevetment\" name=\"slopeRevetment\" placeholder=\"Slope(m)\" class=\"form-control\" />";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</div>";

    revHtml += "<div class=\"row\">";
    revHtml += "<div class=\"col-sm-12 col-md-12\">";

    revHtml += "<div class=\"col-sm-6 col-md-6\">";
    revHtml += "<div class=\"form-group\">";
    revHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    revHtml += "<label for=\"lengthRevetment\" id=\"lengthRevetmentLabel\" class=\"control-label\">Length(m)</label>"
    revHtml += "</div>";
    revHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    revHtml += "<input id=\"lengthRevetment\" name=\"lengthRevetment\" placeholder=\"Length(m)\" class=\"form-control\" />";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</div>";

    revHtml += "<div class=\"col-sm-6 col-md-6\">";
    revHtml += "<div class=\"form-group\">";
    revHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    revHtml += "<label for=\"plainWidth\" id=\"plainWidthLabel\" class=\"control-label\">Plain Width(m)</label>";
    revHtml += "</div>";
    revHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    revHtml += "<input id=\"plainWidth\" name=\"plainWidth\" placeholder=\"Plain Width(m)\" class=\"form-control\" />";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</div>";
    revHtml += "</fieldset>";
    return revHtml;
}

function slu() {
    var sluHtml = "<fieldset class=\"scheduler-border\">";
    sluHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Sluice</u></legend>";
    sluHtml += "<div class=\"row\">";
    sluHtml += "<div class=\"col-sm-12 col-md-12\">";
    sluHtml += "<div class=\"col-sm-6 col-md-6\">";
    sluHtml += "<div class=\"form-group\">";

    sluHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    sluHtml += "<label for=\"sluiceType\" id=\"sluiceTypeLabel\" class=\"control-label\">Sluice Type</label>";
    sluHtml += "</div>";
    sluHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    sluHtml += "<select id=\"sluiceTypeSelect\" name=\"sluiceTypeSelect\" class=\"form-control\">";
    sluHtml += "</select>";
    sluHtml += "</div>";

    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";

    sluHtml += "<div class=\"row\">";
    sluHtml += "<div class=\"col-sm-12 col-md-12\">";

    sluHtml += "<div class=\"col-sm-6 col-md-6\">";
    sluHtml += "<div class=\"form-group\">";
    sluHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    sluHtml += "<label for=\"NoOfVentsSluice\" id=\"NoOfVentsSluiceLabel\" class=\"control-label\">No Of Vents</label>";
    sluHtml += "</div>";
    sluHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    sluHtml += "<input id=\"NoOfVentsSluice\" name=\"NoOfVentsSluice\" placeholder=\"No Of Vents\" class=\"form-control\" />";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "<div class=\"col-sm-6 col-md-6\">";
    sluHtml += "<div class=\"form-group\">";
    sluHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    sluHtml += "<label for=\"ventDiameterSluice\" id=\"ventDiameterSluiceLabel\" class=\"control-label\">Vent Diameter(m)</label>";
    sluHtml += "</div>";
    sluHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    sluHtml += "<input id=\"ventDiameterSluice\" name=\"ventDiameterSluice\" placeholder=\"Vent Diameter(m)\" class=\"form-control\" />";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";

    sluHtml += "<div class=\"row\">";
    sluHtml += "<div class=\"col-sm-12 col-md-12\">";

    sluHtml += "<div class=\"col-sm-6 col-md-6\">";
    sluHtml += "<div class=\"form-group\">";
    sluHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    sluHtml += "<label for=\"ventWidthSluice\" id=\"ventWidthSluiceLabel\" class=\"control-label\">Vent Width(m)</label>";
    sluHtml += "</div>";
    sluHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    sluHtml += "<input id=\"ventWidthSluice\" name=\"ventWidthSluice\" placeholder=\"Vent Width(m)\" class=\"form-control\" />";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";

    sluHtml += "<div class=\"col-sm-6 col-md-6\">";
    sluHtml += "<div class=\"form-group\">";
    sluHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    sluHtml += "<label for=\"ventHeightSluice\" id=\"ventHeightSluiceLabel\" class=\"control-label\">Vent Height(m)</label>";
    sluHtml += "</div>";
    sluHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    sluHtml += "<input id=\"ventHeightSluice\" name=\"ventHeightSluice\" placeholder=\"Vent Height(m)\" class=\"form-control\" />";
    sluHtml += "</div>";

    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";

    sluHtml += "<div class=\"row\">";
    sluHtml += "<div class=\"col-sm-12 col-md-12\">";

    sluHtml += "<div class=\"col-sm-6 col-md-6\">";
    sluHtml += "<div class=\"form-group\">";

    sluHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    sluHtml += "<button class=\"btn\" aria-hidden=\"true\" id=\"btnAdd\">Add</button>";
    sluHtml += "</div>";

    sluHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">"
    sluHtml += "<button class=\"btn\" aria-hidden=\"true\" id=\"btnDelete\">Delete</button>";
    sluHtml += "</div>";

    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";

    sluHtml += "<div class=\"row\">";
    sluHtml += "<div class=\"col-sm-12 col-md-12\">";

    sluHtml += "<div class=\"col-sm-6 col-md-6\">";
    sluHtml += "<div class=\"form-group\">";
    sluHtml += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
    sluHtml += "<label for=\"appurtenantAssets\" id=\"appurtenantAssetsLabel\" class=\"control-label\">Appurtenant Assets</label>";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";

    sluHtml += "<div class=\"row\">";
    sluHtml += "<div class=\"col-sm-12 col-md-12\">";

    sluHtml += "<div class=\"col-sm-6 col-md-6\">";
    sluHtml += "<div class=\"form-group\">";
    sluHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    //embHtml +="<GridView id="GridView1" runat="server"></asp:GridView>
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</div>";
    sluHtml += "</fieldset>";
    return sluHtml;
}

function spu() {
    var spuHtml = "<fieldset class=\"scheduler-border\">";
    spuHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Spur</u></legend>";
    spuHtml += "<div class=\"row\">";
    spuHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    spuHtml += "<div class=\"col-sm-6 col-md-6\">";
    spuHtml += "<div class=\"form-group\">";
    spuHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    spuHtml += "<label for=\"orientation\" id=\"orientationLabel\" class=\"control-label\">Orientation</label>";
    spuHtml += "</div>";
    spuHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    spuHtml += "<input id=\"orientation\" name=\"orientation\" placeholder=\"Orientation\" class=\"form-control\" />";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "<div class=\"col-sm-6 col-md-6\">";
    spuHtml += "<div class=\"form-group\">";
    spuHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    spuHtml += "<label for=\"lengthSpur\" id=\"lengthSpurLabel\" class=\"control-label\">Length (m)</label>";
    spuHtml += "</div>";
    spuHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    spuHtml += "<input id=\"lengthSpur\" name=\"lengthSpur\" placeholder=\"Length (m)\" class=\"form-control\" />";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</div>";

    spuHtml += "<div class=\"row\">";
    spuHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    spuHtml += "<div class=\"col-sm-6 col-md-6\">";
    spuHtml += "<div class=\"form-group\">";
    spuHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    spuHtml += "<label for=\"widthSpur\" id=\"widthSpurLabel\" class=\"control-label\">Width (m)</label>";
    spuHtml += "</div>";
    spuHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    spuHtml += "<input id=\"widthSpur\" name=\"widthSpur\" placeholder=\"Width (m)\" class=\"form-control\" />";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "<div class=\"col-sm-6 col-md-6\">";
    spuHtml += "<div class=\"form-group\">";
    spuHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    spuHtml += "<label for=\"shapeType\" id=\"shapeTypeLabel\" class=\"control-label\">Shape Type</label>";
    spuHtml += "</div>";
    spuHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    spuHtml += "<select id=\"shapeTypeSelect\" name=\"shapeTypeSelect\" class=\"form-control\">";
    spuHtml += "</select>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</div>";

    spuHtml += "<div class=\"row\">";
    spuHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    spuHtml += "<div class=\"col-sm-6 col-md-6\">";
    spuHtml += "<div class=\"form-group\">";
    spuHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    spuHtml += "<label for=\"constructionType\" id=\"constructionTypeLabel\" class=\"control-label\">Construction Type</label>";
    spuHtml += "</div>";
    spuHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    spuHtml += "<select id=\"constructionTypeSelect\" name=\"constructionTypeSelect\" class=\"form-control\">";
    spuHtml += "</select>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "<div class=\"col-sm-6 col-md-6\">";
    spuHtml += "<div class=\"form-group\">";
    spuHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    spuHtml += "<label for=\"spurRevetType\" id=\"spurRevetTypeLabel\" class=\"control-label\">Revet Type</label>";
    spuHtml += "</div>";
    spuHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    spuHtml += "<select id=\"spurRevetTypeSelect\" name=\"spurRevetTypeSelect\" class=\"form-control\">";
    spuHtml += "</select>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</div>";
    spuHtml += "</fieldset>";
    return spuHtml;
}

function trn() {
    var trnHtml = "<fieldset class=\"scheduler-border\">";
    trnHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Turnout</u></legend>";
    trnHtml += "<div class=\"row\">";
    trnHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    trnHtml += "<div class=\"col-sm-6 col-md-6\">";
    trnHtml += "<div class=\"form-group\">";
    trnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    trnHtml += "<label for=\"inletBoxWidth\" id=\"inletBoxWidthLabel\" class=\"control-label\">Inlet Box Width (m)</label>";
    trnHtml += "</div>";
    trnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    trnHtml += "<input id=\"inletBoxWidth\" name=\"inletBoxWidth\" placeholder=\"Inlet Box Width (m)\" class=\"form-control\" />";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "<div class=\"col-sm-6 col-md-6\">";
    trnHtml += "<div class=\"form-group\">";
    trnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    trnHtml += "<label for=\"inletBoxLength\" id=\"inletBoxLengthLabel\" class=\"control-label\">Inlet Box Length (m)</label>";
    trnHtml += "</div>";
    trnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    trnHtml += "<input id=\"inletBoxLength\" name=\"inletBoxLength\" placeholder=\"Inlet Box Length (m)\" class=\"form-control\" />";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";

    trnHtml += "<div class=\"row\">";
    trnHtml += "<div class=\"col-sm-12 col-md-12\">";
   
    trnHtml += "<div class=\"col-sm-6 col-md-6\">";
    trnHtml += "<div class=\"form-group\">";
    trnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    trnHtml += "<label for=\"outletNo\" id=\"outletNoLabel\" class=\"control-label\">Outlet Number</label>";
    trnHtml += "</div>";
    trnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    trnHtml += "<input id=\"outletNo\" name=\"outletNo\" placeholder=\"Outlet Number\" class=\"form-control\" />";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "<div class=\"col-sm-6 col-md-6\">";
    trnHtml += "<div class=\"form-group\">";
    trnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    trnHtml += "<label for=\"outletWidth\" id=\"outletWidthLabel\" class=\"control-label\">Outlet Width (m)</label>";
    trnHtml += "</div>";
    trnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    trnHtml += "<input id=\"outletWidth\" name=\"outletWidth\" placeholder=\"Outlet Width (m)\" class=\"form-control\" />";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";

    trnHtml += "<div class=\"row\">";
    trnHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    trnHtml += "<div class=\"col-sm-6 col-md-6\">";
    trnHtml += "<div class=\"form-group\">";
    trnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    trnHtml += "<label for=\"outletHeight\" id=\"outletHeightLabel\" class=\"control-label\">Outlet Height (m)</label>";
    trnHtml += "</div>";
    trnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    trnHtml += "<input id=\"outletHeight\" name=\"outletHeight\" placeholder=\"Outlet Height (m)\" class=\"form-control\" />";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "<div class=\"col-sm-6 col-md-6\">";
    trnHtml += "<div class=\"form-group\">";
    trnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    trnHtml += "<label for=\"turnoutType\" id=\"turnoutTypeLabel\" class=\"control-label\">Turnout Type</label>";
    trnHtml += "</div>";
    trnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    trnHtml += "<select id=\"turnoutTypeSelect\" name=\"turnoutTypeSelect\" class=\"form-control\">";
    trnHtml += "</select>";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";

    trnHtml += "<div class=\"row\">";
    trnHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    trnHtml += "<div class=\"col-sm-6 col-md-6\">";
    trnHtml += "<div class=\"form-group\">";
    trnHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    trnHtml += "<label for=\"turnoutGateType\" id=\"turnoutGateTypeLabel\" class=\"control-label\">GateType</label>";
    trnHtml += "</div>";
    trnHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    trnHtml += "<select id=\"turnoutGateTypeSelect\" name=\"turnoutGateTypeSelect\" class=\"form-control\">";
    trnHtml += "</select>";
    trnHtml += "</div>";

    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</div>";
    trnHtml += "</fieldset>";
    return trnHtml;
}

function wei() {
    var weiHtml = "<fieldset class=\"scheduler-border\">";
    weiHtml += "<legend class=\"scheduler-border\"><u>Technical Specification: Weir</u></legend>";
    weiHtml += "<div class=\"row\">";
    weiHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    weiHtml += "<div class=\"col-sm-6 col-md-6\">";
    weiHtml += "<div class=\"form-group\">";
    weiHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    weiHtml += "<label for=\"invertLevel\" id=\"invertLevelLabel\" class=\"control-label\">Invert Level</label>";
    weiHtml += "</div>";
    weiHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    weiHtml += "<input id=\"invertLevel\" name=\"invertLevel\" placeholder=\"Invert Level\" class=\"form-control\" />";
    weiHtml += "</div>";
    weiHtml += "</div>";
    weiHtml += "</div>";

    weiHtml += "<div class=\"col-sm-6 col-md-6\">";
    weiHtml += "<div class=\"form-group\">";
    weiHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    weiHtml += "<label for=\"crestTopLevel\" id=\"crestTopLevelLabel\" class=\"control-label\">Crest Top Level</label>";
    weiHtml += "</div>";
    weiHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    weiHtml += "<input id=\"crestTopLevel\" name=\"crestTopLevel\" placeholder=\"Crest Top Level\" class=\"form-control\" />";
    weiHtml += "</div>";
    weiHtml += "</div>";
    weiHtml += "</div>";
    weiHtml += "</div>";
    weiHtml += "</div>";

    weiHtml += "<div class=\"row\">";
    weiHtml += "<div class=\"col-sm-12 col-md-12\">";
    
    weiHtml += "<div class=\"col-sm-6 col-md-6\">";
    weiHtml += "<div class=\"form-group\">";
    weiHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    weiHtml += "<label for=\"crestTopWidth\" id=\"crestTopWidthLabel\" class=\"control-label\">Crest Top Width (m)</label>";
    weiHtml += "</div>";
    weiHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    weiHtml += "<input id=\"crestTopWidth\" name=\"crestTopWidth\" placeholder=\"Crest Top Width (m)\" class=\"form-control\" />";
    weiHtml += "</div>";
    weiHtml += "</div>";
    weiHtml += "</div>";

    weiHtml += "<div class=\"col-sm-6 col-md-6\">";
    weiHtml += "<div class=\"form-group\">";
    weiHtml += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    weiHtml += "<label for=\"weirType\" id=\"weirTypeLabel\" class=\"control-label\">Weir Type</label>";
    weiHtml += "</div>";
    weiHtml += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
    weiHtml += "<select id=\"weirTypeSelect\" name=\"weirTypeSelect\" class=\"form-control\">";
    weiHtml += "</select>";
    weiHtml += "</div>";

    weiHtml += "</div>";
    weiHtml += "</div>";
    weiHtml += "</div>";
    weiHtml += "</div>";
    weiHtml += "</fieldset>";
    return weiHtml;
}

getBridgeType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetBridgeType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            bridgeTypeArr = response.d;
            showBridgeType('bridgeTypeSelect', bridgeTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getCanalLiningType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetCanalLiningType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            canalLiningTypeArr = response.d;
            showCanalLiningType('liningTypeSelect', canalLiningTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getCulvertType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetCulvertType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            culvertTypeArr = response.d;
            showCulvertType('culvertTypeSelect', culvertTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getEmbCrestType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetEmbCrestType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            embCrestTypeArr = response.d;
            showEmbCrestType('crestTypeSelect', embCrestTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getEmbFillType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetEmbFillType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            embFillTypeArr = response.d;
            showEmbFillType('fillTypeSelect', embFillTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getGaugeType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetGaugeType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            gaugeTypeArr = response.d;
            showGaugeType('gaugeTypeSelect', gaugeTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getGauFrequencyType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetGauFrequencyType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            gauFrequencyTypeArr = response.d;
            showGauFrequencyType('frequencyTypeSelect', gauFrequencyTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getGauSeasonType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetGauSeasonType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            gauSeasonTypeArr = response.d;
            showGauSeasonType('seasonTypeSelect', gauSeasonTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getPorcupineType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetPorcupineType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            porcupineTypeArr = response.d;
            showPorcupineType('porcupineTypeSelect', porcupineTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getPorMaterialType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetPorMaterialType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            porMaterialTypeArr = response.d;
            showPorMaterialType('materialTypeSelect', porMaterialTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getRegulatorType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetRegulatorType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            regulatorTypeArr = response.d;
            showRegulatorType('regulatorTypeSelect', regulatorTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getRegGateType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetRegGateType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            regGateTypeArr = response.d;
            showRegGateType('regulatorGateTypeSelect', regGateTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getRevetmentType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetRevetmentType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            revetmentTypeArr = response.d;
            showRevetmentType('revetmentTypeSelect', revetmentTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getRevRiverprotType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetRevRiverprotType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            revRiverprotTypeArr = response.d;
            showRevRiverprotType('riverProtectionTypeSelect', revRiverprotTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getRevWaveprotType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetRevWaveprotType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            revWaveprotTypeArr = response.d;
            showRevWaveprotType('waveProtectionTypeSelect', revWaveprotTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getSluiceType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetSluiceType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            embSluiceTypeArr = response.d;
            showSluiceType('sluiceTypeSelect', embSluiceTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getSpurConstructionType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetSpurConstructionType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            spurConstructionTypeArr = response.d;
            showSpurConstructionType('constructionTypeSelect', spurConstructionTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getSpurRevetType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetSpurRevetType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            spurRevetTypeArr = response.d;
            showSpurRevetType('spurRevetTypeSelect', spurRevetTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getSpurShapeType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetSpurShapeType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            spurShapeTypeArr = response.d;
            showSpurShapeType('shapeTypeSelect', spurShapeTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getTurnoutType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetTurnoutType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            turnoutTypeArr = response.d;
            showTurnoutType('turnoutTypeSelect', turnoutTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getTrnGateType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetTrnGateType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            trnGateTypeArr = response.d;
            showTrnGateType('turnoutGateTypeSelect', trnGateTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

getWeirType = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "CreateAsset.aspx/GetWeirType",
        //data: '{"circleGuid":"' + selectedCircleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            embWeirTypeArr = response.d;
            showWeirType('weirTypeSelect', embWeirTypeArr);
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

showBridgeType = function (selectId, arr) {
    var option_str_bridge = document.getElementById(selectId);
    option_str_bridge.length = 0;
    option_str_bridge.options[0] = new Option('--Select Bridge Type--', '');
    option_str_bridge.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_bridge.options[option_str_bridge.length] = new Option(arr[i].BridgeTypeName, arr[i].BridgeTypeGuid);
        //i += 3;
    }
}

showCanalLiningType = function (selectId, arr) {
    var option_str_canal_lining_type = document.getElementById(selectId);
    option_str_canal_lining_type.length = 0;
    option_str_canal_lining_type.options[0] = new Option('--Select Lining Type--', '');
    option_str_canal_lining_type.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_canal_lining_type.options[option_str_canal_lining_type.length] = new Option(arr[i].LiningTypeName, arr[i].LiningTypeGuid);
        //i += 3;
    }
}

showCulvertType = function (selectId, arr) {
    var option_str_culvert = document.getElementById(selectId);
    option_str_culvert.length = 0;
    option_str_culvert.options[0] = new Option('--Select Culvert Type--', '');
    option_str_culvert.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_culvert.options[option_str_culvert.length] = new Option(arr[i].CulvertTypeName, arr[i].CulvertTypeGuid);
        //i += 3;
    }
}

showEmbCrestType = function (selectId, arr) {
    var option_str_crest = document.getElementById(selectId);
    option_str_crest.length = 0;
    option_str_crest.options[0] = new Option('--Select Crest Type--', '');
    option_str_crest.selectedIndex = 0;
    for (var i = 0; i < arr.length;i++) {
        option_str_crest.options[option_str_crest.length] = new Option(arr[i].CrestTypeName, arr[i].CrestTypeGuid);
        //i += 3;
    }
}

showEmbFillType = function (selectId, arr) {
    var option_str_fill = document.getElementById(selectId);
    option_str_fill.length = 0;
    option_str_fill.options[0] = new Option('--Select Fill Type--', '');
    option_str_fill.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_fill.options[option_str_fill.length] = new Option(arr[i].FillTypeName, arr[i].FillTypeGuid);
        //i += 3;
    }
}

showGaugeType = function (selectId, arr) {
    var option_str_gauge = document.getElementById(selectId);
    option_str_gauge.length = 0;
    option_str_gauge.options[0] = new Option('--Select Gauge Type--', '');
    option_str_gauge.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_gauge.options[option_str_gauge.length] = new Option(arr[i].GaugeTypeName, arr[i].GaugeTypeGuid);
        //i += 3;
    }
}

showGauFrequencyType = function (selectId, arr) {
    var option_str_gauFrequency = document.getElementById(selectId);
    option_str_gauFrequency.length = 0;
    option_str_gauFrequency.options[0] = new Option('--Select Frequency Type--', '');
    option_str_gauFrequency.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_gauFrequency.options[option_str_gauFrequency.length] = new Option(arr[i].FrequencyTypeName, arr[i].FrequencyTypeGuid);
        //i += 3;
    }
}

showGauSeasonType = function (selectId, arr) {
    var option_str_gauSeason = document.getElementById(selectId);
    option_str_gauSeason.length = 0;
    option_str_gauSeason.options[0] = new Option('--Select Season Type--', '');
    option_str_gauSeason.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_gauSeason.options[option_str_gauSeason.length] = new Option(arr[i].SeasonTypeName, arr[i].SeasonTypeGuid);
        //i += 3;
    }
}

showPorcupineType = function (selectId, arr) {
    var option_str_porcupine = document.getElementById(selectId);
    option_str_porcupine.length = 0;
    option_str_porcupine.options[0] = new Option('--Select Porcupine Type--', '');
    option_str_porcupine.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_porcupine.options[option_str_porcupine.length] = new Option(arr[i].PorcupineTypeName, arr[i].PorcupineTypeGuid);
        //i += 3;
    }
}

showPorMaterialType = function (selectId, arr) {
    var option_str_por_material = document.getElementById(selectId);
    option_str_por_material.length = 0;
    option_str_por_material.options[0] = new Option('--Select Material Type--', '');
    option_str_por_material.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_por_material.options[option_str_por_material.length] = new Option(arr[i].PorcupineMaterialTypeName, arr[i].PorcupineMaterialTypeGuid);
        //i += 3;
    }
}

showRegulatorType = function (selectId, arr) {
    var option_str_regulator = document.getElementById(selectId);
    option_str_regulator.length = 0;
    option_str_regulator.options[0] = new Option('--Select Regulator Type--', '');
    option_str_regulator.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_regulator.options[option_str_regulator.length] = new Option(arr[i].RegulatorTypeName, arr[i].RegulatorTypeGuid);
        //i += 3;
    }
}

showRegGateType = function (selectId, arr) {
    var option_str_regGate = document.getElementById(selectId);
    option_str_regGate.length = 0;
    option_str_regGate.options[0] = new Option('--Select Gate Type--', '');
    option_str_regGate.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_regGate.options[option_str_regGate.length] = new Option(arr[i].RegulatorGateTypeName, arr[i].RegulatorGateTypeGuid);
        //i += 3;
    }
}

showRevetmentType = function (selectId, arr) {
    var option_str_revetment = document.getElementById(selectId);
    option_str_revetment.length = 0;
    option_str_revetment.options[0] = new Option('--Select Revetment Type--', '');
    option_str_revetment.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_revetment.options[option_str_revetment.length] = new Option(arr[i].RevetTypeName, arr[i].RevetTypeGuid);
        //i += 3;
    }
}

showRevRiverprotType = function (selectId, arr) {
    var option_str_revRiverprot = document.getElementById(selectId);
    option_str_revRiverprot.length = 0;
    option_str_revRiverprot.options[0] = new Option('--Select River Protection Type--', '');
    option_str_revRiverprot.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_revRiverprot.options[option_str_revRiverprot.length] = new Option(arr[i].RiverprotTypeName, arr[i].RiverprotTypeGuid);
        //i += 3;
    }
}

showRevWaveprotType = function (selectId, arr) {
    var option_str_revWaveprot = document.getElementById(selectId);
    option_str_revWaveprot.length = 0;
    option_str_revWaveprot.options[0] = new Option('--Select Wave Protection Type--', '');
    option_str_revWaveprot.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_revWaveprot.options[option_str_revWaveprot.length] = new Option(arr[i].WaveprotTypeName, arr[i].WaveprotTypeGuid);
        //i += 3;
    }
}

showSluiceType = function (selectId, arr) {
    var option_str_sluice = document.getElementById(selectId);
    option_str_sluice.length = 0;
    option_str_sluice.options[0] = new Option('--Select Sluice Type--', '');
    option_str_sluice.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_sluice.options[option_str_sluice.length] = new Option(arr[i].SluiceTypeName, arr[i].SluiceTypeGuid);
        //i += 3;
    }
}

showSpurConstructionType = function (selectId, arr) {
    var option_str_spurConstruction = document.getElementById(selectId);
    option_str_spurConstruction.length = 0;
    option_str_spurConstruction.options[0] = new Option('--Select Construction Type--', '');
    option_str_spurConstruction.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_spurConstruction.options[option_str_spurConstruction.length] = new Option(arr[i].ConstructionTypeName, arr[i].ConstructionTypeGuid);
        //i += 3;
    }
}

showSpurRevetType = function (selectId, arr) {
    var option_str_spurRevet = document.getElementById(selectId);
    option_str_spurRevet.length = 0;
    option_str_spurRevet.options[0] = new Option('--Select Revet Type--', '');
    option_str_spurRevet.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_spurRevet.options[option_str_spurRevet.length] = new Option(arr[i].SpurRevetTypeName, arr[i].SpurRevetTypeGuid);
        //i += 3;
    }
}

showSpurShapeType = function (selectId, arr) {
    var option_str_spurShape = document.getElementById(selectId);
    option_str_spurShape.length = 0;
    option_str_spurShape.options[0] = new Option('--Select Shape Type--', '');
    option_str_spurShape.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_spurShape.options[option_str_spurShape.length] = new Option(arr[i].ShapeTypeName, arr[i].ShapeTypeGuid);
        //i += 3;
    }
}

showTurnoutType = function (selectId, arr) {
    var option_str_turnout = document.getElementById(selectId);
    option_str_turnout.length = 0;
    option_str_turnout.options[0] = new Option('--Select Turnout Type--', '');
    option_str_turnout.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_turnout.options[option_str_turnout.length] = new Option(arr[i].TurnoutTypeName, arr[i].TurnoutTypeGuid);
        //i += 3;
    }
}

showTrnGateType = function (selectId, arr) {
    var option_str_trnGate = document.getElementById(selectId);
    option_str_trnGate.length = 0;
    option_str_trnGate.options[0] = new Option('--Select Gate Type--', '');
    option_str_trnGate.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_trnGate.options[option_str_trnGate.length] = new Option(arr[i].TurnoutGateTypeName, arr[i].TurnoutGateTypeGuid);
        //i += 3;
    }
}

showWeirType = function (selectId, arr) {
    var option_str_weir = document.getElementById(selectId);
    option_str_weir.length = 0;
    option_str_weir.options[0] = new Option('--Select Weir Type--', '');
    option_str_weir.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str_weir.options[option_str_weir.length] = new Option(arr[i].WeirTypeName, arr[i].WeirTypeGuid);
        //i += 3;
    }
}

saveAsset = function () {

    var divisionManager = {
        divisionGuid: $("#divisionList").val()
    }

    var schemeManager = {
        schemeGuid: $("#schemeSelect").val()
    }

    var assetTypeManager = {
        assetTypeGuid: $("#assetTypeList").val()
    }

    var completionDate = $('#dateOfCompletionOfAsset').val();
    completionDate = Date.parse(completionDate).toString('yyyy/MM/dd');

    var assetCost = $("#assetCost").val();
    var newchar = '';
    assetCost = assetCost.split(',').join(newchar);
    assetCost = stringIsNullOrEmpty($.trim(assetCost)) ? parseFloat(0) : parseFloat(assetCost);
    //assetCost = parseFloat(assetCost);

    var assetManager = {
        assetCode: $('#assetCode').val(),
        drawingCode: $('#drawingCode').val(),
        amtd: $('#amtd').val(),
        assetName: $('#assetName').val(),
        startChainage: stringIsNullOrEmpty($.trim($('#chainageStart').val())) ? 0 : parseInt($.trim($('#chainageStart').val())),
        endChainage: stringIsNullOrEmpty($.trim($('#chainageEnd').val())) ? 0 : parseInt($.trim($('#chainageEnd').val())),
        utmEast1: stringIsNullOrEmpty($.trim($('#eastingUS').val())) ? 0 : parseInt($.trim($('#eastingUS').val())),
        utmEast2: stringIsNullOrEmpty($.trim($('#eastingDS').val())) ? 0 : parseInt($.trim($('#eastingDS').val())),
        utmNorth1: stringIsNullOrEmpty($.trim($('#northingUS').val())) ? 0 : parseInt($.trim($('#northingUS').val())),
        utmNorth2: stringIsNullOrEmpty($.trim($('#northingDS').val())) ? 0 : parseInt($.trim($('#northingDS').val())),
        assetNumber: $('#assetCodeNumberSelect').val(),
        completionDate: completionDate,
        assetCostInput: assetCost,//stringIsNullOrEmpty($.trim($('#assetCost').val())) ? 0.0 : parseFloat($.trim($('#assetCost').val())),
        divisionManager: divisionManager,
        schemeManager: schemeManager,
        assetTypeManager: assetTypeManager,
        canalManager: canalManager,
        culvertManager: culvertManager,
        drainageManager: drainageManager,
        embankmentManager: embankmentManager,
        gaugeManager: gaugeManager,
        porcupineManager: porcupineManager,
        regulatorManager: regulatorManager,
        revetmentManager: revetmentManager,
        sluiceManager: sluiceManager,
        spurManager: spurManager,
        turnoutManager: turnoutManager,
        weirManager: weirManager
    }

    var bridgeTypeManager;
    var bridgeManager;

    var canalLiningTypeManager;
    var canalManager;

    var culvertTypeManager;
    var culvertManager;

    var drainageManager;

    var dropStructureManager;

    var embankmentCrestTypeManager;
    var embankmentFillTypeManager;
    var embankmentManager;

    var gaugeTypeManager;
    var gaugeSeasonTypeManager;
    var gaugeFrequencyTypeManager;
    var gaugeManager;

    var porcupineTypeManager;
    var porcupineMaterialTypeManager;
    var porcupineManager;    

    var regulatorTypeManager;
    var regulatorGateTypeManager;
    var regulatorManager;

    var revetmentTypeManager;
    var revetmentRiverprotTypeManager;
    var revetmentWaveprotTypeManager;
    var revetmentManager;

    var sluiceTypeManager;
    var sluiceManager;

    var spurConstructionTypeManager;
    var spurRevetTypeManager;
    var spurShapeTypeManager;
    var spurManager;

    var turnoutTypeManager;
    var turnoutGateTypeManager;
    var turnoutManager;

    var weirTypeManager;
    var weirManager;

    var assetTypeCode = $("#assetTypeList option:selected").text().split("-");

    switch (assetTypeCode[0]) {
        case 'BRG':
            bridgeTypeManager = {
                bridgeTypeGuid: $('#bridgeTypeSelect').val(),
            }

            bridgeManager = {
                length: $('#lengthBridge').val(),
                pierNumber: $('#numberPeirs').val(),
                roadWidth: $('#roadWidth').val(),
                bridgeTypeManager: bridgeTypeManager
            }

            assetManager.bridgeManager = bridgeManager;

            saveAssetWithBridge(assetManager);
            break;

        case 'CAN':
            canalLiningTypeManager = {
                liningTypeGuid: $('#liningTypeSelect').val(),
            }

            canalManager = {
                usElevation: $('#uSCrestElevationCanal').val(),
                dsElevation: $('#dSCrestElevationCanal').val(),
                length: $('#lengthCanal').val(),
                bedWidth: $('#bedWidthCanal').val(),
                slope1: $('#slipeSlope1Canal').val(),
                slope2: $('#slipeSlope2Canal').val(),
                averageDepth: $('#averageDepthCanal').val(),
                canalLiningTypeManager: canalLiningTypeManager
            }

            assetManager.canalManager = canalManager;

            saveAssetWithCanal(assetManager);
            break;

        case 'CUL':
            culvertTypeManager = {
                culvertTypeGuid: $('#culvertTypeSelect').val(),
            }

            culvertManager = {                
                ventNumber: $('#numberOfVents').val(),
                ventHeight: $('#ventHeight').val(),
                ventWidth: $('#ventWidth').val(),
                lengthCulvert: $('#lengthCulvert').val(),
                culvertTypeManager: culvertTypeManager
            }

            assetManager.culvertManager = culvertManager;
            saveAssetWithCulvert(assetManager);
            break;

        case 'DRN':
            drainageManager = {
                usElevationDrainage: $('#usCrestElevationDrainage').val(),
                dsElevationDrainage: $('#dsCrestElevationDrainage').val(),
                lengthDrainage: $('#lengthDrainage').val(),
                bedWidthDrainage: $('#BedWidthDrainage').val(),
                slope1Drainage: $('#slope1Drainage').val(),
                slope2Drainage: $('#slope2Drainage').val(),
                averageDepthDrainage: $('#averageDepthDrainage').val()
            }

            assetManager.drainageManager = drainageManager;
            saveAssetWithDrainage(assetManager);
            break;

        case 'DRP':
            dropStructureManager = {
                usInvertLevel: $('#usInvertLevel').val(),
                dsInvertLevel: $('#dsInvertLevel').val(),
                stillingBasinLength: $('#stillingBasinLength').val(),
                stillingBasinWidth: $('#stillingBasinWidth').val()
           }
            assetManager.dropStructureManager = dropStructureManager;
            saveAssetWithDropStructure(assetManager);
            break;

        case 'EMB':
            embankmentCrestTypeManager = {
                crestTypeGuid: $('#crestTypeSelect').val(),
            }
            embankmentFillTypeManager = {
                fillTypeGuid: $('#fillTypeSelect').val(),
            }
           
            embankmentManager = {
                usElevation: $('#uSCrestElevation').val(),
                dsElevation: $('#dSCrestElevation').val(),
                length: $('#length').val(),
                crestWidth: $('#crestWidth').val(),
                csSlope1: $('#csSlope1').val(),
                csSlope2: $('#csSlope2').val(),
                rsSlope1: $('#rsSlope1').val(),
                rsSlope2: $('#rsSlope2').val(),
                bermSlope1: $('#bermSlope1').val(),
                bermSlope2: $('#bermSlope2').val(),
                platformWidth: $('#platformWidth').val(),
                averageHeight: $('#averageHeight').val(),
                embankmentCrestTypeManager: embankmentCrestTypeManager,
                embankmentFillTypeManager: embankmentFillTypeManager                
            }
            assetManager.embankmentManager = embankmentManager;
            saveAssetWithEmbankment(assetManager);
            break;
            
        case 'GAU':
            gaugeTypeManager = {
                gaugeTypeGuid: $('#gaugeTypeSelect').val(),
            }
            gaugeSeasonTypeManager = {
                seasonTypeGuid: $('#seasonTypeSelect').val(),
            }
            gaugeFrequencyTypeManager = {
                frequencyTypeGuid: $('#frequencyTypeSelect').val(),
            }

            var startDate = $('#startDate').val();
            startDate = Date.parse(startDate).toString('yyyy/MM/dd');

            var endDate = $('#endDate').val();
            endDate = Date.parse(endDate).toString('yyyy/MM/dd');

            gaugeManager = {                
                startDate: startDate,
                endDate: endDate,
                active: $('#active').is(':checked'),
                lwl: $('#lwlPresent').is(':checked'),
                hwl: $('#hwlPresent').is(':checked'),
                levelGeo: $('#levelGeo').is(':checked'),
                zeroDatum: $('#zeroDatum').val(),
                gaugeTypeManager: gaugeTypeManager,
                gaugeFrequencyTypeManager: gaugeFrequencyTypeManager,
                gaugeSeasonTypeManager: gaugeSeasonTypeManager
            }
            assetManager.gaugeManager = gaugeManager;
            saveAssetWithGauge(assetManager);
            break;

        case 'POR':
            porcupineTypeManager = {
                porcupineTypeGuid: $('#porcupineTypeSelect').val(),
            }
            porcupineMaterialTypeManager = {
                porcupineMaterialTypeGuid: $('#materialTypeSelect').val(),
            }

            porcupineManager = {
                screenLength: $('#screenLength').val(),
                spacingAlongScreen: $('#spacingAlongScreen').val(),
                screenRows: $('#screenRows').val(),
                numberOfLayers: $('#noOfLayers').val(),
                memberLength: $('#mamberLength').val(),
                lengthPorcupine: $('#lengthPorcupine').val(),
                porcupineTypeManager: porcupineTypeManager,
                porcupineMaterialTypeManager: porcupineMaterialTypeManager,
            }
            assetManager.porcupineManager = porcupineManager;
            saveAssetWithPorcupine(assetManager);
            break;
            
        case 'REG':
            regulatorTypeManager = {
                regulatorTypeGuid: $('#regulatorTypeSelect').val(),
            }
            regulatorGateTypeManager = {
                regulatorGateTypeGuid: $('#regulatorGateTypeSelect').val(),
            }

            regulatorManager = {
                ventNumberRegulator: $('#numberOfVentsRegulator').val(),
                ventHeightRegulator: $('#ventHeightRegulator').val(),
                ventWidthRegulator: $('#ventWidthRegulator').val(),
                regulatorTypeManager: regulatorTypeManager,
                regulatorGateTypeManager: regulatorGateTypeManager,
            }
            assetManager.regulatorManager = regulatorManager;
            saveAssetWithRegulator(assetManager);
            break;

        case 'REV':
            revetmentTypeManager = {
                revetTypeGuid: $('#revetmentTypeSelect').val(),
            }
            revetmentRiverprotTypeManager = {
                riverprotTypeGuid: $('#riverProtectionTypeSelect').val(),
            }
            revetmentWaveprotTypeManager = {
                waveprotTypeGuid: $('#waveProtectionTypeSelect').val(),
            }

            revetmentManager = {                
                lengthRevetment: $('#lengthRevetment').val(),
                plainWidth: $('#plainWidth').val(),
                slopeRevetment: $('#slopeRevetment').val(),
                revetmentTypeManager: revetmentTypeManager,
                revetmentRiverprotTypeManager: revetmentRiverprotTypeManager,
                revetmentWaveprotTypeManager:revetmentWaveprotTypeManager
            }
            assetManager.revetmentManager = revetmentManager;
            saveAssetWithRevetment(assetManager);
            break;

        case 'SLU':
            sluiceTypeManager = {
                sluiceTypeGuid: $('#sluiceTypeSelect').val(),
            }

            sluiceManager = {
                ventNumberSluice: $('#NoOfVentsSluice').val(),
                ventDiameterSluice: $('#ventDiameterSluice').val(),
                ventHeightSluice: $('#ventHeightSluice').val(),
                ventWidthSluice: $('#ventWidthSluice').val(),
                sluiceTypeManager: sluiceTypeManager
            }

            assetManager.sluiceManager = sluiceManager;
            saveAssetWithSluice(assetManager);
            break;

        case 'SPU':
            spurConstructionTypeManager = {
                constructionTypeGuid: $('#constructionTypeSelect').val(),
            }
            spurRevetTypeManager = {
                spurRevetTypeGuid: $('#spurRevetTypeSelect').val(),
            }
            spurShapeTypeManager = {
                shapeTypeGuid: $('#shapeTypeSelect').val(),
            }

            spurManager = {               
                orientation: $('#orientation').val(),
                lengthSpur: $('#lengthSpur').val(),
                widthSpur: $('#widthSpur').val(),
                spurConstructionTypeManager: spurConstructionTypeManager,
                spurRevetTypeManager: spurRevetTypeManager,
                spurShapeTypeManager: spurShapeTypeManager
            }
            assetManager.spurManager = spurManager;
            saveAssetWithSpur(assetManager);
            break;
            
        case 'TRN':
            turnoutTypeManager = {
                turnoutTypeGuid: $('#turnoutTypeSelect').val(),
            }
            turnoutGateTypeManager = {
                turnoutGateTypeGuid: $('#turnoutGateTypeSelect').val(),
            }
            
            turnoutManager = {
                inletboxWidth: $('#inletBoxWidth').val(),
                inletboxLength: $('#inletBoxLength').val(),
                outletNumber: $('#outletNo').val(),
                outletWidth: $('#outletWidth').val(),
                outletHeight: $('#outletHeight').val(),
                turnoutTypeManager: turnoutTypeManager,
                turnoutGateTypeManager: turnoutGateTypeManager
            }
            assetManager.turnoutManager = turnoutManager;
            saveAssetWithTurnout(assetManager);
            break;
            
        case 'WEI':
            weirTypeManager = {
                weirTypeGuid: $('#weirTypeSelect').val(),
            }

            weirManager = {
                invertLevel: $('#invertLevel').val(),
                crestTopLevel: $('#crestTopLevel').val(),
                crestTopWidth: $('#crestTopWidth').val(),
                weirTypeManager: weirTypeManager
            }

            assetManager.weirManager = weirManager;
            saveAssetWithWeir(assetManager);
            break;
            
            
        default: document.write("Unknown grade<br />")
    }
}

stringIsNullOrEmpty = function (value) {
    return (value == null || value === "");
}

saveAssetWithBridge = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithBridge",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithCanal = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithCanal",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithCulvert = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithCulvert",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithDrainage = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithDrainage",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithDropStructure = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithDropStructure",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithEmbankment = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithEmbankment",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithGauge = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithGauge",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithPorcupine = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithPorcupine",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithRegulator = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithRegulator",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithRevetment = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithRevetment",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithSluice = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithSluice",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithSpur = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithSpur",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithTurnout = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithTurnout",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

saveAssetWithWeir = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateAsset.aspx/SaveAssetWithWeir",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        //data: '{"assetCode":"' + assetCode + '","drawingCode":"' + drawingCode + '","amtd":"' + amtd + '","assetName":"' + assetName + '","chainageStart":"' + chainageStart + '","chainageEnd":"' + chainageEnd + '","utmEast1":"' + utmEast1 + '","utmEast2":"' + utmEast2 + '","utmNorth1":"' + utmNorth1 + '","utmNorth2":"' + utmNorth2 + '","completionDate":"' + completionDate + '","assetCost":"' + assetCost + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid + '","assetTypeGuid":"' + assetTypeGuid + '","assetNumber":"' + assetNumber + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}

