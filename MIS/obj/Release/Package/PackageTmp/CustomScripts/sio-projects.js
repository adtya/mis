﻿$(document).ready(function () {

    var btnClicked;

    var table = $('#subChainageList').dataTable({
        "responsive": true,
        "pagingType": "full_numbers",
        //"searchHighlight": true,
        "columnDefs": [
            //{
            //    className: 'never',//className:'hidden',
            //    "targets": [5],
            //    "visible": false,
            //    "searchable": false
            //},
            //{
            //    className: 'never',//className:'hidden',
            //    "targets": [6],
            //    "visible": false,
            //    "searchable": false
            //},
            //{
            //    className: 'never',//className:'hidden',
            //    "targets": [7],
            //    "visible": false,
            //    "searchable": false
            //},
            //{
            //    className: 'never',//className:'hidden',
            //    "targets": [8],
            //    "visible": false,
            //    "searchable": false
            //},
            //{
            //    className: 'never',//className:'hidden',
            //    "targets": [9],
            //    "visible": false,
            //    "searchable": false
            //},
            {
                className: 'never',//className:'hidden',
                "targets": [0],
                "visible": false,
                "searchable": false
            }
        ],
        "language": {
            //"lengthMenu": "Display _MENU_ records per page",
            "lengthMenu": 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
    });

    //table1 = $('#trainingList').DataTable({
    //    retrieve: true,
    //    paging: false,
    //    searchHighlight: true
    //});

    //$('.enterProgressBtn').on('click', function () {
    //    var val = 1;
    //    btnClicked = val;
    //});

    //$('#subChainageList tbody').on('click', 'tr', function () {
    //    if ($(this).hasClass('selected')) {
    //        //$(this).removeClass('selected');
    //        //alert(this.rowIndex);
    //        //alert(table1.row(this).index());
    //    }
    //    else {
    //        table.$('tr.selected').removeClass('selected');
    //        $(this).addClass('selected');
    //    }

    //    //rowIndex = table1.row(this).index();
    //    //var fid3 = $('#priorityList' + rowIndex + ' option:selected').val();//Priority

    //    //var rowIndex = table1.row(this).index();
    //    //var a = table.row(this).index();
    //    var fid0 = table.fnGetData(this, 0);//SubChainage Guid
    //    //var fid1 = table.fnGetData(this, 1);//Training Date(from to when)
    //    //var fid2 = table.fnGetData(this, 2);//Training Location
    //    ////var fid3 = table.fnGetData(this, 3);//Priority
    //    //var fid5 = table.fnGetData(this, 5);//Course Id
    //    //var fid6 = table.fnGetData(this, 6);//Course Description
    //    //var fid7 = table.fnGetData(this, 7);//Course Status
    //    //var fid8 = table.fnGetData(this, 8);//Start Date
    //    //var fid9 = table.fnGetData(this, 9);//Location Type
    //    //var fid10 = table.fnGetData(this, 10);//Target User

    //    //var accessFlag = (fid11.toLowerCase() === "true");

    //    //var fid9Text;

    //    //if (fid9 == 1) {
    //    //    fid9Text = "Local";
    //    //}
    //    //else if (fid9 == 2) {
    //    //    fid9Text = "National";
    //    //}
    //    //else {
    //    //    fid9Text = "International";
    //    //}

    //    //if (btnColumnIndex != 4 && btnColumnIndex != 3) {
    //    //    $("#popupModal .modal-title").empty();
    //    //    var title = "Complete details about the clicked training";
    //    //    $("#popupModal .modal-title").append(title);

    //    //    $("#popupModal .modal-body").empty();
    //    //    var html = "<div class=\"desc\">";
    //    //    html += "<table style=\"width:100%\">";
    //    //    html += "<tr>";
    //    //    html += "<td>Training Name:</td>";
    //    //    html += "<td>" + fid0 + "</td>";
    //    //    html += "</tr>";
    //    //    html += "<tr>";
    //    //    html += "<td>Training Date:</td>";
    //    //    html += "<td>" + fid1 + "</td>";
    //    //    html += "</tr>";
    //    //    html += "<tr>";
    //    //    html += "<td>Training Location:</td>";
    //    //    html += "<td>" + fid2 + "</td>";
    //    //    html += "</tr>";
    //    //    html += "<tr>";
    //    //    html += "<td>Location Type:</td>";
    //    //    html += "<td>" + fid9Text + "</td>";
    //    //    html += "</tr>";
    //    //    html += "<tr>";
    //    //    html += "<td>Description:</td>";
    //    //    html += "<td>" + fid6 + "</td>";
    //    //    html += "</tr>";
    //    //    html += "<tr>";
    //    //    html += "<td>Target User:</td>";
    //    //    html += "<td>" + fid10 + "</td>";
    //    //    html += "</tr>";
    //    //    html += "</table>";
    //    //    html += "</div>";
    //    //    $("#popupModal .modal-body").append(html);//.html('<div class=\"descp\"></div>');
    //    //    $('#popupModal').modal('show');
    //    //}

    //    if (btnClicked == 1 && btnColumnIndex == 4) {
    //        if (userOfficialDetailsChanged == "true") {
    //            bootbox.alert("Your Official Details Changes are not approved by the DBA.<br>So, you cannot apply for the training.");
    //        }
    //        else {
    //            if (fid3 == "") {
    //                bootbox.alert("Please select priority.");
    //            }
    //            else {
    //                if (userDisabled == "false") {
    //                    bootbox.confirm("Are you sure you want to apply for this training?<br>You have only <b>3 chances</b> for each type of training.", function (result) {
    //                        if (result == true) {
    //                            checkAccessibility(fid5, fid8, fid9, fid3);
    //                            $('#priorityList' + rowIndex).val("");
    //                        }
    //                        else {
    //                            $('#priorityList' + rowIndex).val("");
    //                        }
    //                    });
    //                }
    //                else {
    //                    bootbox.alert("You have either been blocked by your Super User or completed the training quota.<br />So you cannot apply for any of the trainings.<br />Please contact your Super User for any further assistance.", function () {
    //                        //var nextUrl = location.protocol + "//" + location.host + "/UserHome.aspx";
    //                        //window.location.href = nextUrl;
    //                        window.location = 'UserHome.aspx';
    //                    });
    //                }
    //            }
    //        }
    //    }
    //});

    $('.enterProgressBtn').on('click', function (e) {
        myRowIndex = $(this).parent().parent().index();
        //myColIndex = $(this).index();
        //myColIndex = $(this).parent().index()

        var fid0 = table.fnGetData(myRowIndex, 0);//SubChainage Guid

        subProjectClicked(fid0);

        e.stopPropagation();
    });

});

subProjectClicked = function (subChainageGuid) {
    $.ajax({
        type: "Post",
        async: false,
        url: "SIOProjects.aspx/SetSessionParameter",
        data: '{"subChainageGuid":"' + subChainageGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //str1 = response.d;
            //$('#status').delay(300).fadeOut(); // will first fade out the loading animation
            //$('#preloader').delay(350).fadeOut('slow');
            //str = str1;
            //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
            //window.location.href = nextUrl;
            window.location = 'EnterProgress.aspx';
        },

        failure: function (msg) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            alert("Please contact your administrator");
        }
    });
}