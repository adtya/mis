﻿$(document).ready(function () {

    $('#addDivisionMenuBtn').on('click', function () {
        
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetCircleNames",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {                
                showCircleListOptionsToAddDivision('circleListToAddDivision', response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

    $('#addDivisionPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            divisionCode: {
                required: true,
                noSpace: true,
                maxlength: 5,
                minlength: 3
            },
            divisionName: {
                required: true,
                noSpace: true
            },
            circleListToAddDivision: "required"
        },
    });

    $('#addDivisionBtn').on('click', function () {

        if ($("#addDivisionPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            var check = saveDivision();

            if (check == 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data is saved successfully.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'DBAHome.aspx';
                });
            }
            else if (check == 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data cannot be saved.\nPlease contact your database administrator.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'DBAHome.aspx';
                });
            }
        }

    });

    $('#addDivisionPopupModal').on('click', '.modal-footer button#addDivisionPopupModalCloseBtn', function (e) {
        e.preventDefault();

        location.reload(true);

        e.stopPropagation();
    });

});

showCircleListOptionsToAddDivision = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Circle--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 3;
    }
}

saveDivision = function () {

    var divisionCode = $.trim($('#divisionCode').val());
    var divisionName = $.trim($('#divisionName').val());
    var circleGuid = $("#circleListToAddDivision").val();

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveDivision",
        data: '{"divisionCode":"' + divisionCode + '","divisionName":"' + divisionName + '","circleGuid":"' + circleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });

    return retValue;
}