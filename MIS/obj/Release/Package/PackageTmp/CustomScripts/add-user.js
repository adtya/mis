﻿$(document).ready(function () {

    $("#addUserMenuBtn").on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetRoleNames",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createAddUserForm(response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

});

createAddUserForm = function (userRoleArr) {
    $("#addUserPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/createuser.jpg" class="login" height="70" />';
    html += '<h2 class="text-center">Create User</h2>';

    html += '<div class="panel-body">';
    html += '<!--start form--><!--add form action as needed-->';
    html += '<form id="addUserPopupForm" name="addUserPopupForm" role="form" class="form form-horizontal" method="post">';
    html += '<fieldset>';
    
    html += '<div class="form-group">';
    html += '<div class="input-group">';
    html += '<!--User Name-->';
    html += '<span class="input-group-addon"><i class="glyphicon glyphicon-user color-blue"></i></span>';
    html += '<input id="userFirstName" name="userFirstName" placeholder="First Name" class="form-control" />';
    html += '<span class="input-group-btn" style="width:0px;"></span>';
    html += '<input id="userMiddleName" name="userMiddleName" placeholder="Middle Name" class="form-control" />';
    html += '<span class="input-group-btn" style="width:0px;"></span>';
    html += '<input id="userLastName" name="userLastName" placeholder="Last Name" class="form-control" />';
    html += '</div>';
    html += '</div>';
    
    html += '<div class="form-group">';
    html += '<div class="input-group">';
    html += '<!--EMAIL ADDRESS-->';
    html += '<span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>';
    html += '<input id="userEmailId" name="userEmailId" placeholder="Email Address" class="form-control" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="input-group">';
    html += '<span class="input-group-addon"><i class="fa fa-users color-blue"></i></span>';
    html += '<select id="roleList" name="roleList" class="form-control">';
    html += '</select>';
    html += '</div>';
    html += '</div>';
    
    html += '<div class="form-group">';
    html += '<input id="addUserBtn" class="btn btn-lg btn-primary btn-block" value="Add User" type="submit" />';
    html += '</div>';
    
    html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';
    
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#addUserPopupModal .modal-body").append(html);

    showRoleListOptions('roleList', userRoleArr);

    addUserValidationRules();

    $('#addUserPopupModal').modal('show');

    $('#addUserBtn').on('click', function (e) {
        e.preventDefault();
        if ($("#addUserPopupForm").valid()) {
            $("#preloader").show();
            $("#status").show();

            var check = saveUserData();

            if (check == 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.confirm({
                    title: '',
                    message: 'Your data is saved successfully.<br>Do you want to create more users?',
                    buttons: {
                        'cancel': {
                            label: 'No',
                            className: 'btn-default pull-left'
                        },
                        'confirm': {
                            label: 'Yes',
                            className: 'btn-default pull-right'
                        }
                    },
                    callback: function (result) {
                        if (result) {

                        } else {
                            $('#addUserPopupModal').modal('hide');
                        }
                    }
                });
            } else if (check == 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data cannot be saved.\nPlease contact your database administrator.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'DBAHome.aspx';
                });
            }
        }

    });
}

showRoleListOptions = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Role--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 2;
    }
}

addUserValidationRules = function () {
    $('#addUserPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            userFirstName: {
                required: true,
                noSpace: true
            },
            userLastName: {
                required: true,
                noSpace: true
            },
            userEmailId: {
                required: true,
                //email: true
                email1: true
            },
            roleList: "required",
        },
        messages: {
            userEmailId: {
                required: "Please Enter your Email Id.",
                email1: "Please check the Email Id entered."
            }
        }
    });
}

saveUserData = function () {

    var userFirstName = $('#userFirstName').val();
    var userMiddleName = $('#userMiddleName').val();
    var userLastName = $('#userLastName').val();

    var userEmailId = $('#userEmailId').val();

    var userRoleGuid = $("#roleList").val();

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveUserData",
        data: '{"userFirstName":"' + userFirstName + '","userMiddleName":"' + userMiddleName + '","userLastName":"' + userLastName + '","userEmailId":"' + userEmailId + '","userRoleGuid":"' + userRoleGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });

    return retValue;
}