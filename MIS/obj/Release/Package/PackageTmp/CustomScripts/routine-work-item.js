﻿var workItemClassArr = [];
var assetQuantityParameterArr = [];
var Arr = [];
var assetQuanityParameterData = [];
var routineWorkItemSavedGuid;

$(document).ready(function () {

    $('#addRoutineWorkItemBtn').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetWorkItemClass",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                workItemClassArr = response.d;
                createRoutineWorkItemPopupForm(getRoutineWorkItemData());
            },
            failure: function (result) {
                alert("Error");
            }
        });        
        e.stopPropagation();
    });

    $('#routineWorkItemPopupModal').on('click', '.modal-footer button#routineWorkItemPopupModalCloseBtn', function (e) {
        e.preventDefault();

        location.reload(true);

        e.stopPropagation();
    });

    $('#addRoutineWorkItemPopupModal').on('click', '.modal-footer button#backFromAddRoutineWorkItemPopupModalBtn', function (e) {
        e.preventDefault();

        $('#addRoutineWorkItemPopupModal').modal('hide');

        $('#routineWorkItemPopupModal').modal('show');

        e.stopPropagation();
    });

    $('#viewRoutineWorkItemPopupModal').on('click', '.modal-footer button#backToRoutineWorkItemPopupModalBtn', function (e) {
        e.preventDefault();

        $('#viewRoutineWorkItemPopupModal').modal('hide');

        $('#routineWorkItemPopupModal').modal('show');

        e.stopPropagation();
    });

    $('#editRoutineWorkItemPopupModal').on('click', '.modal-footer button#backEditToRoutineWorkItemPopupModalBtn', function (e) {
        e.preventDefault();

        $('#editRoutineWorkItemPopupModal').modal('hide');

        $('#routineWorkItemPopupModal').modal('show');

        e.stopPropagation();
    });
    
});


createRoutineWorkItemPopupForm = function (routineWorkItemDataTable) {
    $("#routineWorkItemPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/view-data.png" class="login" height="70" />';
    html += '<h2 class="text-center">Routine WorkItems</h2>';

    html += '<div class="panel-body">';
    html += '<form id="routineWorkItemPopupForm" name="routineWorkItemPopupForm" role="form" class="form form-horizontal" method="post">';
    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    html += '<div class="table-responsive">';
    html += '<table id="routineWorkItemsList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'WorkItem Guid' + '</th>';
    html += '<th>' + 'WorkItem Class' + '</th>';
    html += '<th>' + 'WorkItem Code' + '</th>';
    html += '<th>' + 'Asset Quantity Paramters' + '</th>';
    html += '<th>' + 'Units' + '</th>';
    html += '<th>' + 'Short Description' + '</th>';
    html += '<th>' + 'Full Description' + '</th>';
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    for (var i = 0; i < routineWorkItemDataTable.length; i++) {
        html += '<tr>';

        html += '<td>' + '' + '</td>';
        html += '<td>' + routineWorkItemDataTable[i].RoutineWorkItemGuid + '</td>';
        html += '<td>' + routineWorkItemDataTable[i].RoutineWorkItemClassManager.RoutineWorkItemClassName + '</td>';
        html += '<td>' + routineWorkItemDataTable[i].RoutineWorkItemTypeCode + '</td>';
        html += '<td>' + routineWorkItemDataTable[i].AssetQuantityParameterManager.AssetQuantityParameterName + '</td>';
        html += '<td>' + routineWorkItemDataTable[i].AssetQuantityParameterManager.UnitManager.UnitName + '</td>';
        html += '<td>' + routineWorkItemDataTable[i].ShortDescription + '</td>';
        html += '<td>' + routineWorkItemDataTable[i].LongDescription + '</td>';
        html += '<td>' + '<a href="#" class="viewRoutineWorkItemBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editRoutineWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteRoutineWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';

        html += '</tr>';
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';    
    html += '</div>';
    html += '</div>';

    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#routineWorkItemPopupModal .modal-body").append(html);

    $('#routineWorkItemPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    routineWorkItemTableRelatedFunctions();
}

routineWorkItemTableRelatedFunctions = function () {
    var routineWorkItemTable = $('#routineWorkItemPopupModal table#routineWorkItemsList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        //destroy: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false
            },
            {
                targets: [4],
                orderable: false
            },
            {
                targets: [5],
                orderable: false
            },
            {
                targets: [6],
                orderable: false,
                searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewWorkItemBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>'
            }
        ],
        order: [[2, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Create New Routine WorkItem',
                className: 'btn-success addNewRoutineWorkItemBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                   addNewRoutineWorkItem();

                    e.stopPropagation();
                }
            }
        ]

    });

    routineWorkItemTable.on('order.dt search.dt', function () {
        routineWorkItemTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    addNewRoutineWorkItem = function () {        
        createAddRoutineWorkItemPopupForm(routineWorkItemTable);
    };



    $('#routineWorkItemPopupModal').on('click', '#routineWorkItemsList a.viewRoutineWorkItemBtn', function (e) {
        e.preventDefault();       
     
        var selectedRow = $(this).parents('tr')[0];

        var routineWorkItemDataTable = getRoutineWorkItemModalDetails(routineWorkItemTable.row(selectedRow).data()[1]);

        $("#viewRoutineWorkItemPopupModal .modal-body").append(routineWorkItemDataTable);

        $('#routineWorkItemPopupModal').modal('hide');

        $('#viewRoutineWorkItemPopupModal').modal('show');
    });

    $('#routineWorkItemPopupModal').on('click', '#routineWorkItemsList a.editRoutineWorkItemBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

       // var routineWorkItemDataTable = getRoutineWorkItemModalDetails(routineWorkItemTable.row(selectedRow).data()[1]);
       
        createEditRoutineWorkItemPopupForm(routineWorkItemTable, selectedRow);

       
    });

    $('#routineWorkItemPopupModal').on('click', '#routineWorkItemsList a.deleteRoutineWorkItemBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];      

        var routineWorkItemDeleted = deleteRoutineWorkItem(routineWorkItemTable.row(selectedRow).data()[1]);

        if (routineWorkItemDeleted == 1) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Your WorkItem is deleted successfully.", function () {
                routineWorkItemTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (routineWorkItemDeleted == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
        }

    });

}

createAddRoutineWorkItemPopupForm = function (routineWorkItemTable) {

    var assetQuantityParametersData = getAssetTypeParameters();
    
    $("#addRoutineWorkItemPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Add Routine WorkItem</h2>';

    html += '<div class="panel-body">';
    html += '<form id="addRoutineWorkItemPopupForm" name="addRoutineWorkItemPopupForm" class="form form-horizontal" role="form" method="post">';
    html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemClass" id="workItemClassLabel" class="control-label pull-left">WorkItem Class</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control" name="workItemClassList" id="workItemClassList">';    
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemTypeList" id="workItemTypeListLabel" class="control-label pull-left">WorkItem Type</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';   
    html += '<select class="form-control" name="workItemTypeList" id="workItemTypeList">';
    html += '<option>--Select Work Item Type--</option>';
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="codeNumber" id="codeNumberLabel" class="control-label pull-left">Code Number</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="codeNumber" name="codeNumber" placeholder="Code Number" class="form-control text-capitalize" style="width:100%;" readonly="readonly" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="assetQuantityParameterList" id="assetQuantityParameterListLabel" class="control-label pull-left">Asset Quantity Parameter</label>';
    html += '</div>';
    html += '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">';
    html += '<select class="form-control" name="assetQuantityParameterList" id="assetQuantityParameterList">';
    html += '<option value="" selected="selected">' + '--Select Asset Quantity Parameter--' + '</option>';
    for (var i = 0; i < assetQuantityParametersData.length; i++) {
        html += '<option value="' + assetQuantityParametersData[i].AssetQuantityParameterGuid + '">';
        html += assetQuantityParametersData[i].AssetQuantityParameterName;
        html += '</option>';
    }
    html += '</select>';    
    html += '</div>';
    html += '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">';
    html += '<input type="text" id="assetQuantityParameterUnit" name="assetQuantityParameterUnit" placeholder="Unit" class="form-control text-capitalize" style="width:100%;" readonly="readonly" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="shortDescription" id="ashortDescriptionLabel" class="control-label pull-left">Short Description</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="shortDescription" name="shortDescription" placeholder="Short Description" class="form-control text-capitalize" style="width:100%;" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="longDescription" id="longDescriptionLabel" class="control-label pull-left">Full Description</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="longDescription" name="longDescription" placeholder="Full Description" class="form-control text-capitalize" style="width:100%;" />';
    html += '</div>';
    html += '</div>';

    html += '</fieldset>';

    html += '<div id="routineWorkItemListDisplay" class="panel panel-default panel-body col-xs-12 col-sm-12 col-md-12 col-lg-12 well well-sm hidden"></div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    //html += '<button class="btn icon-btn-save btn-success submit" type="button">';
    //html += '<span class="btn-save-label">';
    //html += '<i class="glyphicon glyphicon-floppy-disk"></i>';
    //html += '</span>';
    //html += ' Save';
    //html += '</button>';

    html += '<a href="#" id="saveNewRoutineWorkItemBtn" name="saveNewRoutineWorkItemBtn" type="button" class="btn btn-lg btn-success btn-block">';
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i>';
    html += '</span>';
    html += '  Save';
    html += '</a>';

    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    html += '<a href="#" id="cancelNewRoutineWorkItemBtn" name="cancelNewRoutineWorkItemBtn" type="button" class="btn btn-lg btn-danger btn-block">';//btn-primary
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i>';
    html += '</span>';
    html += ' Cancel';
    html += '</a>';

    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#addRoutineWorkItemPopupModal .modal-body").append(html);

    $('#routineWorkItemPopupModal').modal('hide');

    showRoutineWorkItemClassListOptions('workItemClassList', workItemClassArr);

   // getAssetTypeParamters();

    $('#workItemClassList').on('change', function () {
        $('#workItemTypeList').empty();
        var selectedWorkItemClassGuid = $('#workItemClassList').val();
        $.ajax({
            type: "POST",
            async: false,
            url: "DBAMethods.asmx/GetWorkItemType",
            data: '{"routineWorkItemClassGuid":"' + selectedWorkItemClassGuid + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
               showRoutineWorkItemTypeListOptions('workItemTypeList', response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

    $('#workItemTypeList').on('change', function () {        
        var selectedWorkItemTypeGuid = $('#workItemTypeList').val();
        $.ajax({
            type: "POST",
            async: false,
            url: "DBAMethods.asmx/GetWorkItemNumber",
            data: '{"routineWorkItemTypeGuid":"' + selectedWorkItemTypeGuid + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                Arr = response.d;
                $('#codeNumber').val(Arr.RoutineWorkItemTypeNumber);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

    $('#assetQuantityParameterList').on('change', function () {      
        var selectedAssetQuantityParameterGuid = $('#assetQuantityParameterList').val();
        $.ajax({
            type: "POST",
            async: false,
            url: "DBAMethods.asmx/GetUnitForAssetQuantityParameter",
            data: '{"assetQuantityParameterGuid":"' + selectedAssetQuantityParameterGuid + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                Arr = response.d;              
                $('#assetQuantityParameterUnit').val(Arr.UnitManager.UnitName);             
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });   

    $('#addRoutineWorkItemPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    $('#addRoutineWorkItemPopupModal input#workItemName').focus();

    $('#addRoutineWorkItemPopupModal').on('click', 'a#saveNewRoutineWorkItemBtn', function (e) {
        e.preventDefault();
        if ($('#addRoutineWorkItemPopupForm').valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];
            routineWorkItemSavedGuid = saveRoutineWorkItemData(routineWorkItemTable, selectedRow);

            if (jQuery.Guid.IsValid(routineWorkItemSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(routineWorkItemSavedGuid.toUpperCase())) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                var newRoutineWorkItemRow = routineWorkItemTable.row.add([
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                ]).draw(false);

                var newRoutineWorkItemRowNode = newRoutineWorkItemRow.node();

                $(newRoutineWorkItemRowNode).css('color', 'red').animate({ color: 'black' });

                saveNewRoutineWorkItemRow(routineWorkItemTable, newRoutineWorkItemRow, routineWorkItemSavedGuid);

                bootbox.alert("Your WorkItem is created successfully.", function () {
                    $('#addRoutineWorkItemPopupModal').modal('hide');

                    $('#routineWorkItemPopupModal').modal('show');
                });

            } else {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
            }        

            
        }        

    });

    $('#addRoutineWorkItemPopupModal').on('click', 'a#cancelNewRoutineWorkItemBtn', function (e) {
        e.preventDefault();

        $('#addRoutineWorkItemPopupModal').modal('hide');

        $('#routineWorkItemPopupModal').modal('show');   
              
        e.stopPropagation();
    });

    $('#addRoutineWorkItemPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            workItemClassList: "required",
            workItemTypeList: "required",
            assetQuantityParameterList: "required"
        }          
    });

}

createEditRoutineWorkItemPopupForm = function (routineWorkItemTable, selectedRow) {

    var selectedRoutineWorkItemGuid = routineWorkItemTable.row(selectedRow).data()[1];

    var selectedRoutineWorkItemData = getEditRoutineWorkItemModalDetails(selectedRoutineWorkItemGuid);

    var assetQuantityParametersData = getAssetTypeParameters();

    $("#editRoutineWorkItemPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Edit WorkItem</h2>';

    html += '<div class="panel-body">';
    html += '<form id="editRoutineWorkItemPopupForm" name="editRoutineWorkItemPopupForm" class="form form-horizontal" role="form" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="edittedWorkItemClass" id="edittedWorkItemClassLabel" class="control-label pull-left">Work Item Class</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="edittedWorkItemGuid" name="edittedWorkItemGuid" placeholder="Work Item Class" class="form-control text-capitalize" style="display:none" value="' + selectedRoutineWorkItemGuid + '" style="width:100%;" />';
    html += '<input type="text" id="edittedWorkItemClass" name="edittedWorkItemClass" placeholder="Work Item Class" class="form-control text-capitalize" readonly="readonly" value="' + selectedRoutineWorkItemData.RoutineWorkItemClassManager.RoutineWorkItemClassName + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="edittedWorkItemTypeCode" id="edittedWorkItemTypeCodeLabel" class="control-label pull-left">Work Item Type Code</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="edittedWorkItemTypeCode" name="edittedWorkItemTypeCode" placeholder="Work Item Type Code" class="form-control text-capitalize" readonly="readonly" value="' + selectedRoutineWorkItemData.RoutineWorkItemTypeCode + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="edittedWorkItemShortDesc" id="edittedWorkItemShortDescLabel" class="control-label pull-left">Short Description</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="edittedWorkItemShortDesc" name="edittedWorkItemShortDesc" placeholder="Short Description" class="form-control text-capitalize" value="' + selectedRoutineWorkItemData.ShortDescription + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="edittedWorkItemLongDesc" id="edittedWorkItemLongDescLabel" class="control-label pull-left">Long Description</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="edittedWorkItemLongDesc" name="edittedWorkItemLongDesc" placeholder="Long Description" class="form-control text-capitalize" value="' + selectedRoutineWorkItemData.LongDescription + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="edittedAssetQuantityParameterList" id="edittedAssetQuantityParameterListLabel" class="control-label pull-left">Asset Quantity Parameter</label>';
    html += '</div>';
    html += '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">';
    html += '<select class="form-control" name="edittedAssetQuantityParameterList" id="edittedAssetQuantityParameterList">';

    html += '<option value="" selected="selected">' + '--Select Asset Quantity Parameter--' + '</option>';
    for (var i = 0; i < assetQuantityParametersData.length; i++) {
        if (assetQuantityParametersData[i].AssetQuantityParameterGuid == selectedRoutineWorkItemData.AssetQuantityParameterManager.AssetQuantityParameterGuid) {
            html += '<option value="' + assetQuantityParametersData[i].AssetQuantityParameterGuid + '" selected="selected">';
            html += assetQuantityParametersData[i].AssetQuantityParameterName;
            html += '</option>';
        } else {
            html += '<option value="' + assetQuantityParametersData[i].AssetQuantityParameterGuid + '">';
            html += assetQuantityParametersData[i].AssetQuantityParameterName;
            html += '</option>';
        }
    }

    html += '</select>';
    html += '</div>';
    html += '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">';
    html += '<input type="text" id="edittedAssetQuantityParameterUnit" name="edittedAssetQuantityParameterUnit" placeholder="Unit" readonly="readonly" class="form-control text-capitalize" value="' + selectedRoutineWorkItemData.AssetQuantityParameterManager.UnitManager.UnitName + '" style="width:100%;" />';
    html += '</div>';
    html += '</div>';  

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    html += '<a href="#" id="updateRoutineWorkItemBtn" name="updateRoutineWorkItemBtn" type="button" class="btn btn-lg btn-success btn-block">';
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Update" title="Update"></i>';
    html += '</span>';
    html += '  Update';
    html += '</a>';

    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';

    html += '<a href="#" id="cancelRoutineWorkItemEditBtn" name="cancelRoutineWorkItemEditBtn" type="button" class="btn btn-lg btn-danger btn-block">';//btn-primary
    html += '<span>';
    html += '<i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i>';
    html += '</span>';
    html += ' Cancel';
    html += '</a>';

    html += '</div>';
    html += '</div>';


    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#editRoutineWorkItemPopupModal .modal-body").append(html);

    $('#routineWorkItemPopupModal').modal('hide');

    $('#edittedAssetQuantityParameterList').on('change', function () {
        var selectedEditAssetQuantityParameterGuid = $('#edittedAssetQuantityParameterList').val();
        if (selectedEditAssetQuantityParameterGuid == "")
        {
            $('#edittedAssetQuantityParameterUnit').val("Unit");
        }
        else
        {
            $.ajax({
                type: "POST",
                async: false,
                url: "DBAMethods.asmx/GetUnitForAssetQuantityParameter",
                data: '{"assetQuantityParameterGuid":"' + selectedEditAssetQuantityParameterGuid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    Arr = response.d;
                    $('#edittedAssetQuantityParameterUnit').val(Arr.UnitManager.UnitName);
                },
                failure: function (result) {
                    alert("Error");
                }
            });
        }      
    });

    $('#editRoutineWorkItemPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });
    

   $('#editRoutineWorkItemPopupForm').validate({ // initialize plugin        
        rules: {
            edittedAssetQuantityParameterList: "required"
        }
    });

    $('#editRoutineWorkItemPopupModal').off().on('click', 'a#updateRoutineWorkItemBtn', function (e) {
            e.preventDefault();        

            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            var routineWorkItemGuid = $('#edittedWorkItemGuid').val();
            var routineWorkItemRowUpdated = updateRoutineWorkItem();

            if (routineWorkItemRowUpdated == 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                updateRoutineWorkItemRow(routineWorkItemTable, selectedRow);

                bootbox.alert("Your WorkItem is updated successfully.", function () {

                $('#editRoutineWorkItemPopupModal').modal('hide');
                $('#routineWorkItemPopupModal').modal('show');
            });            
        }
        else {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
        }

    });

    $('#editRoutineWorkItemPopupModal').on('click', 'a#cancelRoutineWorkItemEditBtn', function (e) {
        e.preventDefault();

        $('#editRoutineWorkItemPopupModal').modal('hide');

        $('#routineWorkItemPopupModal').modal('show');

        e.stopPropagation();
    });   

}

getRoutineWorkItemData = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetRoutineWorkItemsData",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

getRoutineWorkItemModalDetails = function (routineWorkItemGuid) {

    //$("#preloader").show();
    //$("#status").show();
    var str;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/ViewRoutineWorkItemData",
        data: '{"routineWorkItemGuid":"' + routineWorkItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //str = str1;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return str;
}

getEditRoutineWorkItemModalDetails = function (routineWorkItemGuid) {

    //$("#preloader").show();
    //$("#status").show();
    var str;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/EditRoutineWorkItemData",
        data: '{"routineWorkItemGuid":"' + routineWorkItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //str = str1;
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return str;
}

saveRoutineWorkItemData = function () {

    var selectedWorkItemTypeTextSplit = $("#workItemTypeList :selected").text();
    selectedWorkItemTypeTextSplit = multiSplit(selectedWorkItemTypeTextSplit, [' (', ')']);

     selectedWorkItemTypeCode = selectedWorkItemTypeTextSplit[1];

    var routineWorkItemClassManager = {
        routineWorkItemClassGuid: $("#workItemClassList").val()
    }

    var routineWorkItemTypeManager = {
        routineWorkItemTypeGuid: $("#workItemTypeList").val(),
        routineWorkItemTypeName: selectedWorkItemTypeCode,
        routineWorkItemTypeNumber: $('#codeNumber').val()
    }

    var assetQuantityParameterManager = {
        assetQuantityParameterGuid: $("#assetQuantityParameterList").val()
    }

    var routineWorkItemManager = {
        shortDescription: $('#shortDescription').val(),
        longDescription: $('#longDescription').val(),            
        routineWorkItemClassManager: routineWorkItemClassManager,
        routineWorkItemTypeManager: routineWorkItemTypeManager,
        assetQuantityParameterManager: assetQuantityParameterManager
      
    }

    routineWorkItemSavedGuid = updateRoutineWorkItemNumber(routineWorkItemManager);

    return routineWorkItemSavedGuid;
}

updateRoutineWorkItemNumber = function (routineWorkItemManager) {
    var routineWorkItemManagerJsonString = JSON.stringify(routineWorkItemManager);

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateRoutineWorkItemTypeNumber",
        data: '{"routineWorkItemManager":' + routineWorkItemManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });

    return retValue;
}

getAssetTypeParameters = function () {
    var retValue;
    $.ajax({
        type: "POST",
        async: false,
        url: "DBAMethods.asmx/GetAssetQuantityParameter",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
            assetQuanityParameterData = response.d;
        },
        failure: function (result) {
            alert("Error");
        }        
    });
    return retValue;
}

showRoutineWorkItemClassListOptions = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Work Item Class--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].RoutineWorkItemClassName, arr[i].RoutineWorkItemClassGuid);
    }
}

showRoutineWorkItemTypeListOptions = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Work Item Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;i++) {
        option_str.options[option_str.length] = new Option(arr[i].RoutineWorkItemTypeFullName + ' (' + arr[i].RoutineWorkItemTypeName + ')', arr[i].RoutineWorkItemTypeGuid);
     }
}

saveNewRoutineWorkItemRow = function (routineWorkItemTable, selectedRow, routineWorkItemSavedGuid) {
    var selectedRowData = routineWorkItemTable.row(selectedRow).data();

    var selectedWorkItemTypeTextSplit = $("#workItemTypeList :selected").text();
    selectedWorkItemTypeTextSplit = multiSplit(selectedWorkItemTypeTextSplit, [' (', ')']);

    selectedWorkItemTypeCode = selectedWorkItemTypeTextSplit[1];

    var assetQuantityParameterGuid = $.trim($('#addRoutineWorkItemPopupModal select#assetQuantityParameterList').val());

    var assetQuantityParameter;

    for (var i = 0; i < assetQuanityParameterData.length; i++) {
        if (assetQuanityParameterData[i].AssetQuantityParameterGuid == assetQuantityParameterGuid) {

            assetQuantityParameter = assetQuanityParameterData[i].AssetQuantityParameterName;
        }
    }
    selectedRowData[1] = routineWorkItemSavedGuid;
    selectedRowData[2] = $("#workItemClassList :selected").text();
    selectedRowData[3] = selectedWorkItemTypeCode + $('#codeNumber').val();
    selectedRowData[4] = assetQuantityParameter;
    selectedRowData[5] = $('#assetQuantityParameterUnit').val();
    selectedRowData[6] = $('#shortDescription').val();
    selectedRowData[7] = $('#longDescription').val();
    selectedRowData[8] = '<a href="#" class="viewRoutineWorkItemBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editRoutineWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteRoutineWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';
    routineWorkItemTable.row(selectedRow).data(selectedRowData);

    routineWorkItemTable.on('order.dt search.dt', function () {
        routineWorkItemTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

updateRoutineWorkItemRow = function (routineWorkItemTable, selectedRow) {
    var selectedRowData = routineWorkItemTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    var assetQuantityParameterGuid = $.trim($('#editRoutineWorkItemPopupModal select#edittedAssetQuantityParameterList').val());

    var assetQuantityParameter;

    for (var i = 0; i < assetQuanityParameterData.length; i++) {
        if (assetQuanityParameterData[i].AssetQuantityParameterGuid == assetQuantityParameterGuid) {

            assetQuantityParameter = assetQuanityParameterData[i].AssetQuantityParameterName;
        }
    }

    selectedRowData[4] = assetQuantityParameter;
    selectedRowData[5] = $('#edittedAssetQuantityParameterUnit').val();
    selectedRowData[6] = $('#edittedWorkItemShortDesc').val();
    selectedRowData[7] = $('#edittedWorkItemLongDesc').val();
    

    routineWorkItemTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

    //unitTable.draw();
}

updateRoutineWorkItem = function (routineWorkItemManager) {
    var assetQuantityParameterManager = {
        assetQuantityParameterGuid: $.trim($('#editRoutineWorkItemPopupModal select#edittedAssetQuantityParameterList').val()),
    };

   var routineWorkItemManager = {
       routineWorkItemGuid: $('#edittedWorkItemGuid').val(),
       shortDescription: $('#edittedWorkItemShortDesc').val(),
       longDescription: $('#edittedWorkItemLongDesc').val(),
       routineWorkItemTypeCode:$('#edittedWorkItemTypeCode').val(),
       assetQuantityParameterManager: assetQuantityParameterManager       
    }

   var routineWorkItemManagerJsonString = JSON.stringify(routineWorkItemManager);

    var retValue = 0;  

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateRoutineWorkItemData",
        data: '{"routineWorkItemManager":' + routineWorkItemManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {          
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteRoutineWorkItem = function (routineWorkItemGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/DeleteRoutineWorkItemData",
        data: '{"routineWorkItemGuid":"' + routineWorkItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

