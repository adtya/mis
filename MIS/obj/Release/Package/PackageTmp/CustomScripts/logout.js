﻿$(document).ready(function () {

    btnlogOff = $("#logOffBtn");

    btnlogOff.click(function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/UserLogout",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#status').delay(200).fadeOut();
                $('#preloader').delay(250).fadeOut('slow');

                window.location = 'Login.aspx';
            },
            failure: function (msg) {
                $('#status').delay(200).fadeOut();
                $('#preloader').delay(250).fadeOut('slow');

                bootbox.alert("Please contact your administrator");
            }
        });

    });

});