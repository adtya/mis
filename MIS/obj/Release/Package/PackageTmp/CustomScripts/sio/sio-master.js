﻿$(window).load(function () {

    $('.marquee').marquee({
        //speed in milliseconds of the marquee
        duration: 15000,
        //gap in pixels between the tickers
        gap: 50,
        //time in milliseconds before the marquee will start animating
        delayBeforeStart: 0,
        //'left' or 'right'
        direction: 'left',
        pauseOnHover: true
    });
});

//FOCUS FUNCTION
sioFocusFunction = function (id) {
    $('#' + id).css('background-color', 'yellow');
}

//BLUR FUNCTION
sioBlurFunction = function (id) {
    $('#' + id).css('background-color', '');
}