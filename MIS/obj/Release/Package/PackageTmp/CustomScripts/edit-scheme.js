﻿$(document).ready(function () {

    $("#editSchemeMenuBtn").on('click', function () {
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetCircleNames",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createEditSchemeForm(response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

    $('#updateSchemeBtn').on('click', function () {
        if ($("#editSchemePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            var circleGuid = $("#circleListToEditScheme").val();
            var divisionGuid = $("#divisionListToEditSchemeBasedOnCircleSelected").val();
            var schemeGuid = $("#schemeListToEditSchemeBasedOnDivisionSelected").val();
            var edittedSchemeCode = $("#edittedSchemeCode").val();
            var edittedSchemeName = $("#edittedSchemeName").val();
            var edittedSchemeType = $("#edittedSchemeType").val();
            var edittedUtmEast1 = $("#edittedUtmEast1").val();
            var edittedUtmEast2 = $("#edittedUtmEast2").val();
            var edittedUtmNorth1 = $("#edittedUtmNorth1").val();
            var edittedUtmNorth2 = $("#edittedUtmNorth2").val();
            var edittedArea = $("#edittedArea").val();
            var edittedDrawingId = $("#edittedDrawingId").val();

            $('#editSchemePopupForm').modal('hide');

            $.ajax({
                type: "Post",
                async: false,
                url: "DBAMethods.asmx/UpdateScheme",
                data: '{"circleGuid":"' + circleGuid + '","divisionGuid":"' + divisionGuid + '","schemeGuid":"' + schemeGuid +
                    '","edittedSchemeCode":"' + edittedSchemeCode + '","edittedSchemeName":"' + edittedSchemeName + '","edittedSchemeType":"' + edittedSchemeType +
                    '","edittedUtmEast1":"' + edittedUtmEast1 + '","edittedUtmEast2":"' + edittedUtmEast2 +
                    '","edittedUtmNorth1":"' + edittedUtmNorth1 + '","edittedUtmNorth2":"' + edittedUtmNorth2 +
                    '","edittedArea":"' + edittedArea + '","edittedDrawingId":"' + edittedDrawingId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    var str = response.d;
                    if (str == true) {
                        bootbox.alert("Scheme updated successfully");
                        $('#editSchemePopupModal').modal('hide');
                    }
                    else {
                        bootbox.alert("Scheme cannot be updated.");
                    }
                },

                failure: function (msg) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    alert(msg);
                }
            });

        }

    });

});

createEditSchemeForm = function (circleArr) {
    $("#editSchemePopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Edit Scheme</h2>';

    html += '<div class="panel-body">';
    html += '<form id="editSchemePopupForm" class="form form-horizontal" method="post">';
    html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="circleListToEditScheme" id="circleListToEditSchemeLabel" class="control-label">Circle Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<select id="circleListToEditScheme" name="circleListToEditScheme" class="form-control">';
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="divisionListToEditSchemeBasedOnCircleSelected" id="divisionListToEditSchemeBasedOnCircleSelectedLabel" class="control-label">Division Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<select id="divisionListToEditSchemeBasedOnCircleSelected" name="divisionListToEditSchemeBasedOnCircleSelected" class="form-control">';
    html += '<option value="">--Select Division--</option>';
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="schemeListToEditSchemeBasedOnDivisionSelected" id="schemeListToEditSchemeBasedOnDivisionSelectedLabel" class="control-label">Scheme Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<select id="schemeListToEditSchemeBasedOnDivisionSelected" name="schemeListToEditSchemeBasedOnDivisionSelected" class="form-control">';
    html += '<option value="">--Select Scheme--</option>';
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedSchemeName" id="edittedSchemeNameLabel" class="control-label">Scheme Name</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedSchemeName" name="edittedSchemeName" placeholder="Scheme Name" class="form-control text-capitalize" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedSchemeCode" id="edittedSchemeCodeLabel" class="control-label">Scheme Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedSchemeCode" name="edittedSchemeCode" placeholder="Scheme Code" class="form-control text-uppercase" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedSchemeType" id="edittedSchemeTypeLabel" class="control-label">Scheme Type</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedSchemeType" name="edittedSchemeType" placeholder="Scheme Type" class="form-control text-capitalize" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedUtmEast1" id="edittedUtmEast1Label" class="control-label">UTM East1</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedUtmEast1" name="edittedUtmEast1" placeholder="UTM East1" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedUtmEast2" id="edittedUtmEast2Label" class="control-label">UTM East2</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedUtmEast2" name="edittedUtmEast2" placeholder="UTM East2" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedUtmNorth1" id="edittedUtmNorth1Label" class="control-label">UTM North1</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedUtmNorth1" name="edittedUtmNorth1" placeholder="UTM North1" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedUtmNorth2" id="edittedUtmNorth2Label" class="control-label">UTM North2</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedUtmNorth2" name="edittedUtmNorth2" placeholder="UTM North2" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '<div class="row">';
    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedDrawingId" id="edittedDrawingIdLabel" class="control-label">Drawing ID</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedDrawingId" name="edittedDrawingId" placeholder="Drawing ID" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedArea" id="edittedAreaLabel" class="control-label">Area</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedArea" name="edittedArea" placeholder="Area" class="form-control" />';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';

    html += '</fieldset>';
    html += '</form>';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#editSchemePopupModal .modal-body").append(html);

    showCircleListOptionsToEditScheme('circleListToEditScheme', circleArr);

    $('#editSchemePopupModal').modal('show');

    editSchemeValidationRules();

    $('#circleListToEditScheme').on('change', function () {
        $('#divisionListToEditSchemeBasedOnCircleSelected').empty();
        var selectedCircleGuid = $(this).val();
        $.ajax({
            type: "POST",
            async: false,
            url: "DBAMethods.asmx/GetDivisionsBasedOnCircleGuid",
            data: '{"circleGuid":"' + selectedCircleGuid + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                divisionArr = response.d;
                showDivisionListOptionsToEditScheme('divisionListToEditSchemeBasedOnCircleSelected', divisionArr);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

    $('#divisionListToEditSchemeBasedOnCircleSelected').on('change', function () {
        $('#schemeListToEditSchemeBasedOnDivisionSelected').empty();
        var selectedCircleGuid = $('#circleListToEditScheme').val();
        var selectedDivisionGuid = $(this).val();
        $.ajax({
            type: "POST",
            async: false,
            url: "DBAMethods.asmx/GetSchemeNames",
            data: '{"circleGuid":"' + selectedCircleGuid + '","divisionGuid":"' + selectedDivisionGuid + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                schemeNameArr = response.d;
                showSchemeListOptionsToEditScheme('schemeListToEditSchemeBasedOnDivisionSelected', schemeNameArr);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

    $('#schemeListToEditSchemeBasedOnDivisionSelected').on('change', function () {
        var selectedCircleGuid = $('#circleListToEditScheme').val();
        var selectedDivisionGuid = $('#divisionListToEditSchemeBasedOnCircleSelected').val();
        var selectedSchemeGuid = $(this).val();

        if (selectedSchemeGuid == "") {
            $('#updateSchemeBtn').addClass('hidden');
            $('#edittedSchemeCode').val('');
            $('#edittedSchemeName').val('');
            $('#edittedSchemeType').val('');
            $('#edittedUtmEast1').val('');
            $('#edittedUtmEast2').val('');
            $('#edittedUtmNorth1').val('');
            $('#edittedUtmNorth2').val('');
            $('#edittedArea').val('');
            $('#edittedDrawingId').val('');
        } else {
            $.ajax({
                type: "POST",
                async: false,
                url: "DBAMethods.asmx/GetSchemeData",
                data: '{"circleGuid":"' + selectedCircleGuid + '","divisionGuid":"' + selectedDivisionGuid + '","schemeGuid":"' + selectedSchemeGuid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    schemeArr = response.d;
                    for (var i = 0; i < schemeArr.length;) {
                        if (schemeArr[i] == selectedSchemeGuid) {
                            $('#edittedSchemeCode').val(schemeArr[i + 1]);
                            $('#edittedSchemeName').val(schemeArr[i + 2]);
                            $('#edittedSchemeType').val(schemeArr[i + 3]);
                            $('#edittedUtmEast1').val(schemeArr[i + 4]);
                            $('#edittedUtmEast2').val(schemeArr[i + 5]);
                            $('#edittedUtmNorth1').val(schemeArr[i + 6]);
                            $('#edittedUtmNorth2').val(schemeArr[i + 7]);
                            $('#edittedArea').val(schemeArr[i + 8]);
                            $('#edittedDrawingId').val(schemeArr[i + 9]);
                        }
                        i = i + 10;
                    }
                },
                failure: function (result) {
                    alert("Error");
                }
            });

            $('#updateSchemeBtn').removeClass('hidden');
        }
    });

}

showCircleListOptionsToEditScheme = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Circle--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 3;
    }
}

showDivisionListOptionsToEditScheme = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Division--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 3;
    }
}

showSchemeListOptionsToEditScheme = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Scheme--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 2;
    }
}

editSchemeValidationRules = function () {
    $('#editSchemePopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            circleListToEditScheme: "required",
            divisionListToEditSchemeBasedOnCircleSelected: "required",
            edittedSchemeCode: {
                required: true,
                noSpace: true,
                minlength: 3,
                maxlength: 5
            },
            edittedSchemeName: {
                required: true,
                noSpace: true
            },
            edittedSchemeType: {
                required: true,
                noSpace: true
            },
            edittedUtmEast1: {
                number: true,
            },
            edittedUtmEast2: {
                number: true,
            },
            edittedUtmNorth1: {
                number: true,
            },
            edittedUtmNorth2: {
                number: true,
            },
            edittedDrawingId: {
                number: true,
            },
            edittedArea: {
                number: true,
            }       
       
        }
    });
}