﻿$(document).ready(function () {

    $("#deletUserMenuBtn").on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetUsers",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createDeleteUserForm(response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

});

createDeleteUserForm = function (userData) {
    $("#deleteUserPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/createuser.jpg" class="login" height="70" />';
    html += '<h2 class="text-center">Delete User</h2>';

    html += '<div class="panel-body">';
    html += '<form id="deleteUserPopupForm" name="deleteUserPopupForm" role="form" class="form form-horizontal" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    if (userData.length > 0) {
        html += '<div class="table-responsive">';
        html += '<table id="registeredUserList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

        html += '<thead>';
        html += '<tr>';
        html += '<th>' + '#' + '</th>';
        html += '<th>' + '' + '</th>';
        html += '<th>' + 'User Guid' + '</th>';
        html += '<th>' + 'Name' + '</th>';
        html += '<th>' + 'EmailId' + '</th>';
        html += '<th>' + 'User Role' + '</th>';
        html += '</tr>';
        html += '</thead>';

        html += '<tbody id="deleteUserTableBody">';
        
        for(var i = 0; i < userData.length; i++) {
            html += '<tr>';

            html += '<td>' + '' + '</td>';
            html += '<td>' + '' + '</td>';
            html += '<td id="userGuid">' + userData[i].UserGuid + '</td>';
            //html += '<td>' + '<input type="checkbox" id="deleteUserCheck' + i + '" name="deleteUserCheck" class="deleteUserCheck" value="true">' + '</td>';
            html += '<td>' + userData[i].FirstName + '</td>';
            html += '<td>' + userData[i].Email + '</td>';
            html += '<td>' + userData[i].RoleManager.RoleName + '</td>';
            
            html += '</tr>';
        }

        html += '</tbody>';

        html += '</table>';
        html += '</div>';
    } else {
        html += '<p>There is no data to be displayed.</p>';
    }
    
    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    
    $("#deleteUserPopupModal .modal-body").append(html);

    $('#deleteUserPopupModal').modal('show');

    tableRelatedFunctions();

    //$('#deleteUserBtn').on('click', function (e) {
    //    e.preventDefault();

    //    if ($(this).hasClass('disabled')) {
            
    //    } else {
    //        deleteUserConfirmation();
    //    }
    //});
}

tableRelatedFunctions = function () {
    var table = $('#registeredUserList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'select-checkbox',
                targets: [1],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [2],
                visible: false,
                searchable: false
            }
        ],
        order: [[3, 'asc']],
        select: {
            style: 'multi',
            selector: 'td:nth-child(2)',
            blurable: true
        }
    });

    //table1 = $('#registeredUserList').DataTable({
    //    retrieve: true,
    //    searchHighlight: true,
    //    paging: false
    //});

    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    table.on('select', function (e, dt, type, indexes) {
        var rows = table.rows({ selected: true }).count();
        if (rows > 0) {
            $('#deleteUserBtn').removeClass('disabled');
        }
        //var rowData = table.rows(indexes).data().toArray();
        //userGuidToBeDeleted.push(rowData[0][2]);
        //if (userGuidToBeDeleted.length > 0) {
           // $('#deleteUserBtn').removeClass('disabled');
        //}
    });

    table.on('deselect', function (e, dt, type, indexes) {
        var rows = table.rows({ selected: true }).count();
        if (rows > 0) {
            $('#deleteUserBtn').removeClass('disabled');
        } else {
            $('#deleteUserBtn').addClass('disabled');
        }
        //var rowData = table.rows(indexes).data().toArray();
        //var index = userGuidToBeDeleted.indexOf(rowData[0][2]);
        //if (index > -1) {
        //    userGuidToBeDeleted.splice(index, 1);
        //}

        //if (userGuidToBeDeleted.length > 0) {
        //    $('#deleteUserBtn').removeClass('disabled');
        //} else {
        //    $('#deleteUserBtn').addClass('disabled');
        //}
    });

    //$('#registeredUserList tbody').on('click', 'tr', function () {
    //    if ($(this).hasClass('selected')) {
    //        //$(this).removeClass('selected');
    //    }
    //    else {
    //        table.$('tr.selected').removeClass('selected');
    //        $(this).addClass('selected');
    //    }

    //    rowIndex = table1.row(this).index();

    //    fid0 = table.fnGetData(this, 0);//for user_id
    //    var fid1 = table.fnGetData(this, 1);//user name
    //    userName = fid1;
    //    var fid2 = table.fnGetData(this, 2);//designation
    //    var fid3 = table.fnGetData(this, 3);//divison
    //    var fid4 = table.fnGetData(this, 4);//department
    //    var fid5 = table.fnGetData(this, 5);//emailId
    //    var fid6 = table.fnGetData(this, 6);//mobile
    //    var fid7 = table.fnGetData(this, 7);//superUser
    //    var fid8 = table.fnGetData(this, 8);//admin
    //    var fid9 = table.fnGetData(this, 9);//superAdmin
    //    var fid10 = table.fnGetData(this, 10);//selfApproval

    //    $("#popupModal .modal-title").empty();
    //    var title = fid1;
    //    $("#popupModal .modal-title").append(title);

    //    $("#popupModal .modal-body").empty();
    //    var html = "<div class=\"desc\">";
    //    html += "<table class=\"table table-striped\" style=\"width:100%\">";
    //    html += "<tr>";
    //    html += "<td>Designation:</td>";
    //    html += "<td>" + fid2 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Divison:</td>";
    //    html += "<td>" + fid3 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Department:</td>";
    //    html += "<td>" + fid4 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>EmailId:</td>";
    //    html += "<td>" + fid5 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Mobile:</td>";
    //    html += "<td>" + fid6 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Self Approval:</td>";
    //    html += "<td>" + fid10 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>SuperUser:</td>";
    //    html += "<td>" + fid7 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>Admin:</td>";
    //    html += "<td>" + fid8 + "</td>";
    //    html += "</tr>";
    //    html += "<tr>";
    //    html += "<td>SuperAdmin:</td>";
    //    html += "<td>" + fid9 + "</td>";
    //    html += "</tr>";
    //    html += "</table>";
    //    html += "</div>";
    //    $("#popupModal .modal-body").append(html);//.html('<div class=\"descp\"></div>');
    //    $('#popupModal').modal('show');

    //    $('.selfApproval').on('click', function () {
    //        if ($("#selfApproval" + rowIndex).is(":checked")) {
    //            $("#superUserList" + rowIndex).prop('disabled', 'disabled');
    //            $("#adminList" + rowIndex).prop('disabled', 'disabled');
    //            $("#superAdminList" + rowIndex).prop('disabled', 'disabled');
    //        }
    //        else {
    //            $("#superUserList" + rowIndex).prop('disabled', false);
    //            $("#adminList" + rowIndex).prop('disabled', false);
    //            $("#superAdminList" + rowIndex).prop('disabled', false);
    //        }
    //    });
    //});

    

    //$('.deleteUserCheck').on('click', function (e) {
    //    myRowIndex = $(this).parent().parent().index();
    //    //myColIndex = $(this).index();
    //    //myColIndex = $(this).parent().index()

    //    //var fid2 = table.fnGetData(myRowIndex, 2);//SubChainage Guid

    //    //alert(myRowIndex);

    //    //var count = table.rows({ selected: true });

    //    //alert(count);

    //    e.stopPropagation();
    //});

    $('#deleteUserBtn').on('click', function (e) {
        e.preventDefault();

        var rowsData = table.rows({ selected: true }).data();
        var userGuidToBeDeleted = [];

        for (var i = 0; i < rowsData.length; i++) {
            userGuidToBeDeleted.push(rowsData[i][2]);
        }

        if ($(this).hasClass('disabled')) {

        } else {
            deleteUserConfirmation(userGuidToBeDeleted);
        }
    });


}

deleteUserConfirmation = function (userGuidToBeDeleted) {
    bootbox.confirm({
        title: '!!Alert!!',
        message: 'Are you sure you want to delete the selected user(s).',
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default pull-left'
            },
            'confirm': {
                label: 'Delete',
                className: 'btn-danger pull-right'
            }
        },
        callback: function (result) {
            if (result) {
                //window.location = $("a[data-bb='confirm']").attr('href');
                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                //window.location.href = nextUrl;
                deleteUser(userGuidToBeDeleted);
            }
        }
    });
}

deleteUser = function (userGuidToBeDeleted) {
    //var y = document.getElementById("deleteUserTableBody");
    //var x = y.getElementsByTagName("tr");
    //var id = [];
    //for (var i = 0; i < x.length; i++) {
    //    //var txt = "The index of Row " + (i + 1) + " is: " + x[i].rowIndex + "<br>";
    //    var rowIndex1 = x[i].rowIndex;
    //    var lfckv = document.getElementById("deleteUserCheck" + i).checked;
    //    if (lfckv == true) {
    //        var idToDelete = document.getElementById("registeredUserList").rows[rowIndex1].cells[0].innerHTML;
    //        id.push(idToDelete);
    //    }
    //}

    var userGuidListToDelete = JSON.stringify(userGuidToBeDeleted);

    $("#preloader").show();
    $("#status").show();
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/DeleteUserData",
        data: '{"userGuidListToDelete":' + userGuidListToDelete + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$("table input[type='checkbox']:checked").parent().parent().remove();
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            var str = response.d;
            if (str == true) {
                bootbox.alert("User(s) deleted successfully.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/DeleteTraining.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'DBAHome.aspx';
                });
            }
            else {
                bootbox.alert("User cannot be deleted.");
            }
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            alert(msg);
        }
    });
}