﻿$(document).ready(function () {

    $('#addFinancialSubHeadMenuBtn').on('click', function (e) {
        e.preventDefault();
        $('#addFinancialSubHeadPopupModal').modal('show');
    });

    $('#addFinancialSubHeadPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            financialSubHeadName: {
                required: true,
                noSpace: true
            }
        }
    });

    $('#addFinancialSubHeadBtn').on('click', function (e) {
        e.preventDefault();

        if ($("#addFinancialSubHeadPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            var checkExistence = checkFinancialSubHeadExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a Financial SubHead with this name.");
            } else if (checkExistence == false) {
                var check = saveFinancialSubHead();

                if (check == 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    $('#financialSubHeadName').val('');
                    bootbox.alert("Your Sub-Head is created successfully.");
                } else if (check == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }
            }

        }

    });

});

checkFinancialSubHeadExistence = function () {
    var subHeadName = $.trim($('#financialSubHeadName').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckFinancialSubHeadExistence",
        data: '{"subHeadName":"' + subHeadName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveFinancialSubHead = function () {
    var subHeadName = $.trim($('#financialSubHeadName').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveFinancialSubHead",
        data: '{"subHeadName":"' + subHeadName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}