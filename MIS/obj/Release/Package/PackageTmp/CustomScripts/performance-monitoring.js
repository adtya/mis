﻿var itemPerformanceArr;
var monitoringItemTableDataArr = [];
var monitoringItemTableDataArr1 = [];


$(document).ready(function () {
    
    $.ajax({
        type: "POST",
        async: false,
        url: "PerformanceMonitoringDetails.aspx/DisplayOverallStatusList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //performanceMonitoringDetailsData(response.d);
            itemPerformanceArr = response.d;
            showOverallStatusOptions('overallStatusSelect', itemPerformanceArr);           
        },

        failure: function (result) {
            alert("Error");
        }
    });

    $.ajax({
        type: "POST",
        async: false,
        url: "PerformanceMonitoringDetails.aspx/DisplayDesignationList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            showInspectorPositionOptions('inspectorPositionSelect', response.d);
        },
        failure: function (result) {
            alert("Error");
        }
    });

   $('#dpInspectionDate').datepicker({
        format: "dd/mm/yyyy",
    });
    
    //$(function () {
    //    $('.a1').click(function () {
    //        $(this).parent().find('.content').slideToggle("slow");
    //    });
    //});

   //$("fieldset legend").click(function () {

   //    if ($(this).closest('fieldset').find('.contents').length == 0) return;
   //    if ($(this).parent().children().length == 2) $(this).siblings().slideToggle("slow");
   //    else {
   //        $(this).parent().wrapInner("<div>");
   //        $(this).appendTo($(this).parent().parent());
   //        $(this).parent().find("div").toggle();
   //    }
    //});

   $('#assetCodeList').on('change', function () {       
       monitoringItemTableDataArr1 = [];      
       var selectedAssetCodeGuid = $(this).val();

        if (selectedAssetCodeGuid === '') {
            $('#assetTypeCode').val('');
            $('#assetCode').val('');
            $('#assetName').val('');
            $('#assetTypeGuid').val('');
        } else {
            $.ajax({
                type: "POST",
                async: false,
                url: "PerformanceMonitoringDetails.aspx/GetAssetCodeData",
                data: '{"assetCodeGuid":"' + selectedAssetCodeGuid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    assetCodeArr = response.d;
                    for (var i = 0; i < assetCodeArr.length;i++) {
                        if (assetCodeArr[i] == selectedAssetCodeGuid) {
                            $('#assetCode').val(assetCodeArr[i + 1]);
                            $('#assetName').val(assetCodeArr[i + 2]);
                            $('#assetTypeGuid').val(assetCodeArr[i + 3]);
                            $('#assetTypeCode').val(assetCodeArr[i + 4]);                           
                        }                      
                    }
                },
                failure: function (result) {
                    alert("Error");
                }
            });

            var selectedAssetTypeGuid = $('#assetTypeGuid').val();

            $.ajax({
                type: "POST",
                async: false,
                url: "PerformanceMonitoringDetails.aspx/GetMonitoringItemData",
                data: '{"assetTypeGuid":"' + selectedAssetTypeGuid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    performanceChecklistData(response.d);                      
                },
                failure: function (result) {
                    alert("Error");
                }
            });           
       }
    });    

    $('#savePerformanceDetailsBtn').on('click', function () {

        if ($("#addPerformanceMonitoringDetailsForm").valid()) {
           
            savePerformanceMontioringDetails();           
        }        
    });

    $('#addPerformanceMonitoringDetailsForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            assetCodeList: "required",
            overallStatusSelect: "required",
            inspectorPositionSelect: "required",
            inspectorName: "required",
            generalComments: "required",
            inspectionDate: {
                required: true,
                dateITA: true
            }
        }
    });

    $('#cancelPerformanceDetailsBtn').on('click', function (e) {
        e.preventDefault();

        window.location = 'DBAHome.aspx';
    });
    
});

performanceChecklistData = function (monitoringItemArr) {

    if (monitoringItemArr.length === 0)
    {
        $('fieldset#performanceCheckList').addClass('hidden');
    }
    else
    {
        $('fieldset#performanceCheckList').removeClass('hidden');

        for (var i = 0; i < monitoringItemArr.length; i++) {
            var fieldsetId = 'fieldset' + monitoringItemArr[i].MonitoringItemTypeManager.MonitoringItemTypeName.replace(/\s/g, '');

            if (!$('fieldset#' + fieldsetId).length) {                

                var checklistHtml = '<fieldset class="scheduler-border" id="' + fieldsetId + '">';
                checklistHtml += "<legend class=\"scheduler-border a1\">" + monitoringItemArr[i].MonitoringItemTypeManager.MonitoringItemTypeName + "</legend>";
                // checklistHtml += "<div class=\"contents\">";

                checklistHtml += '<div id="table-container" style="padding:1%;">';
                checklistHtml += '<div class="table-responsive">';

                checklistHtml += '<table id="' + fieldsetId + 'Table" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';
                checklistHtml += '<thead>';
                checklistHtml += '<tr>';
                checklistHtml += '<th>' + '#' + '</th>';
                checklistHtml += '<th>' + 'Item Guid' + '</th>';
                checklistHtml += '<th>' + 'Item Name' + '</th>';
                checklistHtml += '<th>' + 'Item Performance Status' + '</th>';
                checklistHtml += '<th>' + 'Item Description' + '</th>';
                checklistHtml += '</tr>';
                checklistHtml += '</thead>';
                checklistHtml += '<tbody>';
                checklistHtml += '</tbody>';
                checklistHtml += '</table>';

                //checklistHtml += '</div>';
                checklistHtml += '</div>';
                checklistHtml += '</div>';
                checklistHtml += "</fieldset>";

                $("#performanceChecklistPanel").append(checklistHtml);

                eval('var ' + fieldsetId + 'Table' + ' = $("#' + fieldsetId + 'Table").DataTable({paging:false,info:false,ordering:false,searching:false});');

                performanceChecklistDataTable(monitoringItemArr, eval(fieldsetId + 'Table'), fieldsetId);

                monitoringItemTableDataArr.push(eval(fieldsetId + 'Table'));

                var table = eval(fieldsetId + 'Table');
                table.column(1).visible(false);

                monitoringItemTableDataArr1.push(fieldsetId);
            }

        }

    }   
    
}

performanceChecklistDataTable = function (monitoringItemArr, tableVar, currentFieldsetId) {
    var newArr = [];

    for (var i = 0; i < monitoringItemArr.length; i++) {
        var fieldsetId = 'fieldset' + monitoringItemArr[i].MonitoringItemTypeManager.MonitoringItemTypeName.replace(/\s/g, '');

        if (fieldsetId === currentFieldsetId) {
            newArr.push(monitoringItemArr[i]);
        }
    }

    for (var i = 0; i < newArr.length; i++) {
        var selectPerformanceStatus = '<select id="' + currentFieldsetId + i + '" name="itemPerformanceSelect" class="form-control">';

        for (var selectPerformanceOptions = 0; selectPerformanceOptions < itemPerformanceArr.length;) {
            selectPerformanceStatus += '<option value="' + itemPerformanceArr[selectPerformanceOptions] + '" >';
            selectPerformanceStatus += itemPerformanceArr[selectPerformanceOptions + 1];
            selectPerformanceStatus += '</option>';
            selectPerformanceOptions = selectPerformanceOptions + 2;
        }

        selectPerformanceStatus += '</select>';

        var inputText = '<input id="' + currentFieldsetId + i + '" name="itemPerformanceDescription" class="form-control itemPerformanceDescription" type="text"/>';

        tableVar.row.add([
            i + 1,
            newArr[i].MonitoringItemGuid,
            newArr[i].MonitoringItemName,
            selectPerformanceStatus,
            inputText
        ]).draw(false);
    }
}

showOverallStatusOptions = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Overall Status--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 2;
    }
}

showInspectorPositionOptions = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Inspector Position--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 2;
    }
}

savePerformanceMontioringDetails = function () {    
    var data;

    var monitoringItemGuid;
    var monitoringItemPerformanceGuid;
    var monitoringItemPerformanceDescription;
    var assetManager = {
        assetGuid: $("#assetCodeList").val()
    }

    var overallStatusManager = {
        overallStatusGuid: $("#overallStatusSelect").val()
    }

    var designationManager = {
        designationGuid: $("#inspectorPositionSelect").val()
    }

    var systemVariablesManager = {
        currentMonitoringDate: Date.parse($('#monitoringDate').val()).toString('yyyy/MM/dd')
     
    }
    var inspectionDate = $('#inspectionDate').val();
    inspectionDate = Date.parse(inspectionDate).toString('yyyy/MM/dd');

    var monitoringItemPerformanceManager=[];

    var performanceMonitoringManager = {

        assetManager: assetManager,        
        overallStatusManager: overallStatusManager,
        systemVariablesManager: systemVariablesManager,
        inspectorName: $('#inspectorName').val(),        
        designationManager: designationManager,
        inspectionDate: inspectionDate,
        generalComments: $('#generalComments').val(),       
        monitoringItemPerformanceManager:monitoringItemPerformanceManager       
    }

    if (monitoringItemTableDataArr.length != null) {
        for (var i = 0; i < monitoringItemTableDataArr.length; i++) {

                monitoringItemTableDataArr[i].rows().every(function (rowIdx, tableLoop, rowLoop) {
                data = this.data();

                monitoringItemGuid = data[1];
                monitoringItemPerformanceGuid = $('#' + monitoringItemTableDataArr1[i] + 'Table select#' + monitoringItemTableDataArr1[i] + rowIdx).val();
                monitoringItemPerformanceDescription = $('#' + monitoringItemTableDataArr1[i] + 'Table input#' + monitoringItemTableDataArr1[i] + rowIdx).val();

                var monitoringItemManager = {
                    monitoringItemGuid: monitoringItemGuid
                }

                var itemStatusManager = {
                    overallStatusGuid: monitoringItemPerformanceGuid
                }
                monitoringItemPerformanceManager.push({
                    assetManager: assetManager,
                    systemVariablesManager: systemVariablesManager,
                    monitoringItemManager: monitoringItemManager,
                    monitoringItemPerformanceDescription: monitoringItemPerformanceDescription,
                    overallStatusManager: itemStatusManager

                })
            });            
        }
    }   
    performanceMonitoringManager.monitoringItemPerformanceManager = monitoringItemPerformanceManager;

    saveItemPerformance(performanceMonitoringManager);
}

saveItemPerformance = function (performanceMonitoringManager) {
    var performanceMonitoringManagerJsonString = JSON.stringify(performanceMonitoringManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "PerformanceMonitoringDetails.aspx/SaveItemPerformance",
        data: '{"performanceMonitoringManager":' + performanceMonitoringManagerJsonString + '}',       
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            ret = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
}