﻿var unitTypeTable;

$(document).ready(function () {

    $('#unitTypeListMenuBtn').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetUnitTypes",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createUnitTypePopupForm(response.d);
            },
            failure: function (result) {
                bootbox.alert("Error");
            }
        });
        //e.stopPropagation();
    });

    $('#unitTypePopupModal').on('click', '.modal-footer button#unitTypeModalCloseBtn', function (e) {
        e.preventDefault();

        //unitTypeTable.button(['.addNewUnitTypeBtn']).destroy()

        unitTypeTable.buttons().destroy();

        unitTypeTable.destroy();

        $('#unitTypePopupModal table#unitTypesList').empty();

        //e.stopPropagation();
    });

    
    
});

createUnitTypePopupForm = function (UnitTypeData) {
    $('#unitTypePopupModal .modal-body').empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Units</h2>';

    html += '<div class="panel-body">';
    html += '<form id="unitTypePopupForm" name="unitTypePopupForm" role="form" class="form form-horizontal" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div id="table-container" style="padding:1%;">';

    //if (UnitTypeData.length > 0) {
    html += '<div class="table-responsive">';
    html += '<table id="unitTypesList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'UnitType Guid' + '</th>';
    html += '<th>' + 'UnitType Name' + '</th>';
    html += '<th>' + 'Unit List' + '</th>';
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    for (var i = 0; i < UnitTypeData.length; i++) {
        html += '<tr>';

        html += '<td>' + '' + '</td>';
        html += '<td>' + UnitTypeData[i].UnitTypeGuid + '</td>';
        html += '<td>' + UnitTypeData[i].UnitTypeName + '</td>';
        html += '<td class="unitListMenuBtnTd" id="unitListMenuBtnTd' + i + '">' + '' + '</td>';
        html += '<td class="unitListOperationTd" id="unitListOperationTd' + i + '">' + '' + '</td>';
        //html += '<td class="unitListMenuBtnTd">' + '<a href="#" class="unitListMenuBtn">Unit</a>' + '</td>';
        //html += '<td class="unitListOperationTd">' + '<a href="#" class="viewUnitTypeBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editUnitTypeBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteUnitTypeBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>';

        html += '</tr>';
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';
    //} else {
    // html += '<p>There is no Unit Type available to be displayed.</p>';
    // }

    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $('#unitTypePopupModal .modal-body').append(html);

    $('#unitTypePopupModal').modal('show');

    $('#unitTypePopupModal').on('show.bs.modal', function () {
        //$('#unitTypePopupModal a.btn').on('click', function (e) {
        //    //$('#unitTypePopupModal').modal('hide');     // dismiss the dialog
        //});
        //unitTypeTableRelatedFunctions();
    });

    $('#unitTypePopupModal').on('shown.bs.modal', function () {
        //unitTypeTableRelatedFunctions();
    });

    $('#unitTypePopupModal').on('hide.bs.modal', function () {    // remove the event listeners when the dialog is dismissed
        //$('#unitTypePopupModal a.btn').off('click');
    });

    $('#unitTypePopupModal').on('hidden.bs.modal', function () {  // remove the actual elements from the DOM when fully hidden
        //$('#unitTypePopupModal').remove();
        //$('#unitTypePopupModal .modal-body').empty();
        //$(this).data('bs.modal', null);
        alert("aditya");
    });

    unitTypeTableRelatedFunctions();

    $('#unitTypePopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            unitTypeName: {
                required: true,
                noSpace: true
            }
        },
        messages: {
            unitTypeName: {
                required: "Please Enter the UnitType Name.",
                noSpace: "UnitType Name cannot be empty."
            }
        },
        //errorPlacement: function (error, element) {

        //    switch (element.attr("name")) {
        //        case "dpProgressMonth":
        //            error.insertAfter($("#dpProgressMonth"));
        //            break;

        //        default:
        //            //nothing
        //    }
        //}
    });

}

unitTypeTableRelatedFunctions = function () {
    var unitTypeEditing = null;

    unitTypeTable = $('#unitTypePopupModal table#unitTypesList').DataTable({
        //var unitTypeTable = $("#unitTypePopupModal .modal-body unitTypeTable#unitTypesList").DataTable({
        //var unitTypeTable = $('#unitTypesList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        destroy: true,
        //orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false,
                searchable: false
            },
            {
                targets: [4],
                //data: null,
                orderable: false,
                searchable: false,
                //defaultContent: '<a href="#" class="viewUnitTypeBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editUnitTypeBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteUnitTypeBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>'
            }
            //{
            //    targets: [4],
            //    orderable: false,
            //    searchable: false,
            //    //defaultContent: '<a class="viewWorkItemSubHeadBtn" onclick="editBtnClicked(this);"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="editWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-pencil" style="font-size: 22px;" data-original-title="Edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="deleteWorkItemSubHeadBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete"></i></a>'
            //}
        ],
        order: [[2, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        //select: {
        //    style: 'os',
        //    blurable: true
        //},
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        //initComplete: function(settings, json) {
        //    alert( 'DataTables has finished its initialisation.' );
        //},
        buttons: [
            {
                text: '+ Create New UnitType',
                className: 'btn-success addNewUnitTypeBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewUnitType();

                    e.stopPropagation();
                }
            }
        ]

    });

    unitTypeTable.on('order.dt search.dt', function () {
        unitTypeTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    unitTypeTable.page.len(-1).draw();

    new $.fn.dataTable.Buttons(unitTypeTable, {
        buttons: [
            {
                text: 'Unit',
                className: 'btn-link unitListMenuBtn',
                titleAttr: 'Unit',
                action: function (e, dt, node, conf) {
                    //alert('Button View clicked on');
                }
            }
        ]
    });

    unitTypeTable.buttons(1, null).container().appendTo(
        //unitTypeTable.table().container()
        $('.unitListMenuBtnTd')
    );

    new $.fn.dataTable.Buttons(unitTypeTable, {
        buttons: [
            {
                text: '<i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i>',
                className: 'btn-link viewUnitTypeBtn',
                titleAttr: 'View',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    //var selectedRow = dt.row().node();

                    var selectedRow = $(e.currentTarget).parents('tr')[0];

                    viewUnitType(selectedRow);

                    e.stopPropagation();
                }
            },
            {
                text: '<i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i>',
                className: 'btn-link editUnitTypeBtn',
                titleAttr: 'Edit',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    //var selectedRow = dt.row().node();

                    var selectedRow = $(e.currentTarget).parents('tr')[0];

                    editUnitType(selectedRow);

                    e.stopPropagation();
                }
            },
            {
                text: '<i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i>',
                className: 'btn-link deleteUnitTypeBtn',
                titleAttr: 'Delete',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    //var selectedRow = dt.row().node();

                    var selectedRow = $(e.currentTarget).parents('tr')[0];

                    deleteUnitType(selectedRow);

                    e.stopPropagation();
                }
            }
        ]
    });

    unitTypeTable.buttons(2, null).container().appendTo(
        //unitTypeTable.table().container()
        $('.unitListOperationTd')
    );

    new $.fn.dataTable.Buttons(unitTypeTable, {
        buttons: [
            {
                text: '<i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i>',
                className: 'btn-link saveNewUnitTypeBtn',
                titleAttr: 'Save',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    //var selectedRow = dt.row().node();

                    var selectedRow = $(e.currentTarget).parents('tr')[0];

                    saveNewUnitType(selectedRow);

                    e.stopPropagation();
                }
            },
            {
                text: '<i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i>',
                className: 'btn-link cancelNewUnitTypeEditBtn',
                titleAttr: 'Cancel',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    //var selectedRow = dt.row().node();

                    var selectedRow = $(e.currentTarget).parents('tr')[0];

                    cancelNewUnitType(selectedRow);

                    e.stopPropagation();
                }
            }
        ]
    });

    //unitTypeTable.buttons(3, null).container().appendTo(
    //    //unitTypeTable.table().container()
    //    $('.unitListOperationTd')
    //);

    new $.fn.dataTable.Buttons(unitTypeTable, {
        buttons: [
            {
                text: '<i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i>',
                className: 'btn-link updateUnitTypeBtn',
                titleAttr: 'Save',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    //var selectedRow = dt.row().node();

                    var selectedRow = $(e.currentTarget).parents('tr')[0];

                    updateUnitType(selectedRow);

                    e.stopPropagation();
                }
            },
            {
                text: '<i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i>',
                className: 'btn-link cancelUnitTypeEditBtn',
                titleAttr: 'Cancel',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    //var selectedRow = dt.row().node();

                    var selectedRow = $(e.currentTarget).parents('tr')[0];

                    cancelUnitTypeEdit(selectedRow);

                    e.stopPropagation();
                }
            }
        ]
    });

    //unitTypeTable.buttons(4, null).container().appendTo(
    //    //unitTypeTable.table().container()
    //    $('.unitListOperationTd')
    //);

    unitTypeTable.page.len(5).draw();

    var addUnitTypeBtn = unitTypeTable.button(['.addNewUnitTypeBtn']);

    addNewUnitType = function () {

        if (unitTypeEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreUnitTypeRowData(unitTypeTable, unitTypeEditing);
            unitTypeEditing = null;
            addUnitTypeBtn.enable();
        }

        var newUnitTypeRow = unitTypeTable.row.add([
            '',
            '',
            '',
            '',
            ''
        ]).draw(false);

        var newUnitTypeRowNode = newUnitTypeRow.node();

        var rowIndex = unitTypeTable.row(newUnitTypeRowNode).index();

        //unitTypeTable.column(4).nodes().each(function (cell, i) {
        //    cell.innerHTML = i + 1;
        //});

        unitTypeTable.cell(rowIndex, 3).nodes().to$().addClass('unitListMenuBtnTd');

        unitTypeTable.cell(rowIndex, 3).nodes().to$().attr('id', 'unitListMenuBtnTd' + rowIndex);



        unitTypeTable.cell(rowIndex, 4).nodes().to$().addClass('unitListOperationTd');

        unitTypeTable.cell(rowIndex, 4).nodes().to$().attr('id', 'unitListOperationTd' + rowIndex);

        //unitTypeTable
        //    .column(4)
        //    .nodes()
        //    .to$()
        //    .addClass('unitListOperationTd');

        //var h = unitTypeTable
        //    .column(4)
        //    .nodes()
        //    .to$();

        //unitTypeTable
        //    .column(4)
        //    .nodes()
        //    .to$()
        //    .addClass('unitListMenuBtnTd');

        

        addNewUnitTypeRow(unitTypeTable, newUnitTypeRowNode);

        unitTypeEditing = newUnitTypeRowNode;

        addUnitTypeBtn.disable();

        $('#unitTypePopupModal input#unitTypeName').focus();

        $(newUnitTypeRowNode).css('color', 'red').animate({ color: 'black' });
    };

    saveNewUnitType = function (selectedRow) {

        if ($("#unitTypePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            //var selectedRow = $(this).parents('tr')[0];

            var checkExistence = checkUnitTypeExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a UnitType with this name.");
            } else if (checkExistence == false) {
                var unitTypeSavedGuid = saveUnitType();

                if (jQuery.Guid.IsValid(unitTypeSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(unitTypeSavedGuid.toUpperCase())) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your UnitType is created successfully.", function () {
                        if (unitTypeEditing !== null && unitTypeEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            saveNewUnitTypeRow(unitTypeTable, unitTypeEditing, unitTypeSavedGuid);
                            unitTypeEditing = null;
                            addUnitTypeBtn.enable();
                        }
                    });
                } else {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }
            }

        }

    };

    cancelNewUnitType = function (selectedRow) {

        /* Get the row as a parent of the link that was clicked on */
        //var selectedRow = $(this).parents('tr')[0];

        if (unitTypeEditing !== null && unitTypeEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            removeNewUnitTypeRowCreatedOnCancel(unitTypeTable, selectedRow);
            unitTypeEditing = null;
            addUnitTypeBtn.enable();
        }

    };

    $('#unitTypePopupModal').on('click', '#unitTypesList a.unitListMenuBtn', function (e) {
        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];
        var unitTypeGuid = unitTypeTable.row(selectedRow).data()[1];
        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetUnits",
            data: '{"unitTypeGuid":"' + unitTypeGuid + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#unitTypePopupModal').modal('hide');
                createUnitPopupForm(response.d, unitTypeGuid);
            },
            failure: function (result) {
                bootbox.alert("Error");
            }
        });

        //e.stopPropagation();

    });

    viewUnitType = function (selectedRow) {
        
        ///* Get the row as a parent of the link that was clicked on */
        //var selectedRow = $(this).parents('tr')[0];

        ////var selectedRowIndex = unitTypeTable.row($(this).parents('tr')).index();

        //if (workItemEditing !== null && workItemEditing != selectedRow) {
        //    /* A different row is being edited - the edit should be cancelled and this row edited */
        //    restoreWorkItemRowData(unitTypeTable, workItemEditing);
        //    editWorkItemRow(unitTypeTable, selectedRow);
        //    workItemEditing = selectedRow;
        //    addWorkItemBtn.enable();
        //}
        //    //else if (nEditing == selectedRow && this.innerHTML == "Save") {
        //    //    /* This row is being edited and should be saved */
        //    //    saveRow(oTable, nEditing);
        //    //    nEditing = null;
        //    //    createBtnClicked = 0;
        //    //}
        //else {
        //    /* No row currently being edited */
        //    editWorkItemRow(unitTypeTable, selectedRow);
        //    workItemEditing = selectedRow;
        //    addWorkItemBtn.enable();
        //}

        
    };

    editUnitType = function (selectedRow) {

        /* Get the row as a parent of the link that was clicked on */
        //var selectedRow = $(this).parents('tr')[0];

        //var selectedRowIndex = unitTypeTable.row(selectedRow).index();

        if (unitTypeEditing !== null && unitTypeEditing != selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreUnitTypeRowData(unitTypeTable, unitTypeEditing);
            editUnitTypeRow(unitTypeTable, selectedRow);
            unitTypeEditing = selectedRow;
            addUnitTypeBtn.enable();
            $('#unitTypePopupModal input#unitTypeName').focus();
        } else {
            /* No row currently being edited */
            editUnitTypeRow(unitTypeTable, selectedRow);
            unitTypeEditing = selectedRow;
            addUnitTypeBtn.enable();
            $('#unitTypePopupModal input#unitTypeName').focus();
        }

    };

    updateUnitType = function (selectedRow) {
        
        if ($("#unitTypePopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            //var selectedRow = $(this).parents('tr')[0];

            var checkExistence = checkUnitTypeExistence();

            if (checkExistence == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');
                bootbox.alert("There is already a UnitType with this name.");
            } else if (checkExistence == false) {
                var unitTypeSaved = updateUnitTypeInDB(unitTypeTable.row(selectedRow).data()[1]);

                if (unitTypeSaved == 1) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    //$('#workItemSubHeadName').val('');
                    bootbox.alert("Your UnitType is updated successfully.", function () {
                        if (unitTypeEditing !== null && unitTypeEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            updateUnitTypeRow(unitTypeTable, unitTypeEditing);
                            unitTypeEditing = null;
                            addUnitTypeBtn.enable();
                        }
                    });
                } else if (unitTypeSaved == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
                }
            }

        }

    };

    cancelUnitTypeEdit = function (selectedRow) {

        /* Get the row as a parent of the link that was clicked on */
        //var selectedRow = $(this).parents('tr')[0];

        if (unitTypeEditing !== null && unitTypeEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreUnitTypeRowData(unitTypeTable, unitTypeEditing);
            unitTypeEditing = null;
            addUnitTypeBtn.enable();
        }

    };

    deleteUnitType = function (selectedRow) {

        $("#preloader").show();
        $("#status").show();

        /* Get the row as a parent of the link that was clicked on */
        //var selectedRow = $(this).parents('tr')[0];

        var unitTypeDeleted = deleteUnitTypeFromDB(unitTypeTable.row(selectedRow).data()[1]);

        if (unitTypeDeleted == 1) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Your UnitType is deleted successfully.", function () {
                unitTypeTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (unitTypeDeleted == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
        }

        //e.stopPropagation();
    };

}

addNewUnitTypeRow = function (unitTypeTable, selectedRow) {
    var selectedRowData = unitTypeTable.row(selectedRow).data();

    var rowIndex = unitTypeTable.row(selectedRow).index();

    var availableTds = $('>td', selectedRow);

    //unitTypeTable.order([2, 'asc']).draw();

    unitTypeTable.column('1:visible').order('asc').draw();

    unitTypeTable.on('order.dt search.dt', function () {

        var x = unitTypeTable.order();

        if (x[0][1] == "asc") {
            var a = unitTypeTable.column(0, { search: 'applied', order: 'applied' }).nodes();
            for (i = 1; i < a.length; i++) {
                a[i].innerHTML = i;
            }
        } else {
            unitTypeTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }
        availableTds[0].innerHTML = '*';
    }).draw(false);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[0].innerHTML = '*';
    availableTds[1].innerHTML = '<input type="text" id="unitTypeName" name="unitTypeName" placeholder="UnitType Name" class="form-control text-capitalize" autofocus="autofocus" value="' + selectedRowData[2] + '">';//UnitType
    availableTds[2].innerHTML = '';//UnitType
    //availableTds[3].innerHTML = '<a href="#" class="saveNewUnitTypeBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelNewUnitTypeEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';

    //$('.unitListOperationTd').empty();

    unitTypeTable.buttons(3, null).container().appendTo(
        //unitTypeTable.table().container()
        $('#unitListOperationTd' + rowIndex)
    );
}

saveNewUnitTypeRow = function (unitTypeTable, selectedRow, unitTypeSavedGuid) {
    var lengthOfPage = unitTypeTable.page.info().length;

    unitTypeTable.page.len(-1).draw();

    var selectedRowData = unitTypeTable.row(selectedRow).data();

    var rowIndex = unitTypeTable.row(selectedRow).index();

    var availableInputs = $('input', selectedRow);

    selectedRowData[1] = unitTypeSavedGuid;
    selectedRowData[2] = capitalizeFirstAllWords($.trim(availableInputs[0].value));
    //selectedRowData[3] = '<a href="#" class="unitListMenuBtn">Unit</a>';
    //selectedRowData[4] = '<a href="#" class="viewUnitTypeBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editUnitTypeBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteUnitTypeBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    unitTypeTable.row(selectedRow).data(selectedRowData);

    unitTypeTable.on('order.dt search.dt', function () {
        unitTypeTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $('.unitListMenuBtnTd').empty();

    unitTypeTable.buttons(1, null).container().appendTo(
        //unitTypeTable.table().container()
        //$('#unitListMenuBtnTd' + rowIndex)
        $('.unitListMenuBtnTd')
    );

    $('.unitListOperationTd').empty();

    unitTypeTable.buttons(2, null).container().appendTo(
        //unitTypeTable.table().container()
        //$('#unitListOperationTd' + rowIndex)
        $('.unitListOperationTd')
    );

    unitTypeTable.page.len(lengthOfPage).draw();
}

removeNewUnitTypeRowCreatedOnCancel = function (unitTypeTable, selectedRow) {

    unitTypeTable
        .row(selectedRow)
        .remove()
        .draw();

    unitTypeTable.on('order.dt search.dt', function () {
        unitTypeTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

editUnitTypeRow = function (unitTypeTable, selectedRow) {
    var selectedRowData = unitTypeTable.row(selectedRow).data();

    var rowIndex = unitTypeTable.row(selectedRow).index();

    var availableTds = $('>td', selectedRow);

    availableTds[1].innerHTML = '<input type="text" id="unitTypeName" name="unitTypeName" placeholder="UnitType Name" class="form-control text-capitalize" value="' + selectedRowData[2] + '">';//UnitType
    availableTds[2].innerHTML = '';
    //availableTds[3].innerHTML = '<a href="#" class="updateUnitTypeBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelUnitTypeEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>'

    //$('.unitListOperationTd').empty();

    $('#unitListOperationTd' + rowIndex).empty();

    unitTypeTable.buttons(4, null).container().appendTo(
        //unitTypeTable.table().container()
        $('#unitListOperationTd' + rowIndex)
    );
}

updateUnitTypeRow = function (unitTypeTable, selectedRow) {
    var lengthOfPage = unitTypeTable.page.info().length;

    unitTypeTable.page.len(-1).draw();

    var selectedRowData = unitTypeTable.row(selectedRow).data();

    var rowIndex = unitTypeTable.row(selectedRow).index();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[2] = capitalizeFirstAllWords($.trim(availableInputs[0].value));

    unitTypeTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

    //unitTypeTable.draw();

    //$('.unitListMenuBtnTd').empty();

    unitTypeTable.buttons(1, null).container().appendTo(
        //unitTypeTable.table().container()
        $('#unitListMenuBtnTd' + rowIndex)
        //$('.unitListMenuBtnTd')
    );

    //$('.unitListOperationTd').empty();

    unitTypeTable.buttons(2, null).container().appendTo(
        //unitTypeTable.table().container()
        $('#unitListOperationTd' + rowIndex)
        //$('.unitListOperationTd')
    );

    unitTypeTable.page.len(lengthOfPage).draw();
}

restoreUnitTypeRowData = function (unitTypeTable, previousRow) {
    var previousRowData = unitTypeTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewUnitTypeRowCreatedOnCancel(unitTypeTable, previousRow);
    } else {
        var lengthOfPage = unitTypeTable.page.info().length;

        unitTypeTable.page.len(-1).draw();

        var rowIndex = unitTypeTable.row(previousRow).index();

        unitTypeTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;

        //unitTypeTable.cell(previousRowIndex, 0).innerHTML = previousRowSerialNum;

        //unitTypeTable.draw();

        $('.unitListMenuBtnTd').empty();

        unitTypeTable.buttons(1, null).container().appendTo(
            //unitTypeTable.table().container()
            //$('#unitListMenuBtnTd' + rowIndex)
            $('.unitListMenuBtnTd')
        );

        $('.unitListOperationTd').empty();

        unitTypeTable.buttons(2, null).container().appendTo(
            //unitTypeTable.table().container()
            //$('#unitListOperationTd' + rowIndex)
            $('.unitListOperationTd')
        );

        unitTypeTable.page.len(lengthOfPage).draw();
    }

}





capitalizeFirstAllWords = function (str) {
    var pieces = str.split(" ");
    for (var i = 0; i < pieces.length; i++) {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
}

checkUnitTypeExistence = function () {
    var unitTypeName = $.trim($('#unitTypePopupModal input#unitTypeName').val());

    var retValue = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckUnitTypeExistence",
        data: '{"unitTypeName":"' + unitTypeName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveUnitType = function () {
    var unitTypeName = $.trim($('#unitTypePopupModal input#unitTypeName').val());

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveUnitType",
        data: '{"unitTypeName":"' + unitTypeName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updateUnitTypeInDB = function (unitTypeGuid) {
    var unitTypeName = $.trim($('#unitTypePopupModal input#unitTypeName').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateUnitType",
        data: '{"unitTypeName":"' + unitTypeName + '","unitTypeGuid":"' + unitTypeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteUnitTypeFromDB = function (unitTypeGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/DeleteUnitType",
        data: '{"unitTypeGuid":"' + unitTypeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}