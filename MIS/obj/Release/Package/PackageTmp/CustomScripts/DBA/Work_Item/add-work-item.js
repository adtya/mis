﻿$(document).ready(function () {

    $('#addWorkItemPopupModal').on('click', '.modal-footer button#backFromAddWorkItemPopupModalBtn', function (e) {
        e.preventDefault();

        $('#addWorkItemPopupModal').modal('hide');

        $('#workItemPopupModal').modal('show');

        e.stopPropagation();
    });

});

createAddWorkItemPopupForm = function () {
    var arrWorkItemSubHeadsSelectedToAddWorkItem = [];

    var workItemSubHeadDataToAddWorkItem = getWorkItemSubHeadsToAddWorkItem();

    var unitTypeDataToAddWorkItem = getUnitTypesToAddWorkItem();

    //getUnitTypesToAddWorkItem();

    $("#addWorkItemPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Add WorkItem</h2>';

    html += '<div class="panel-body">';
    html += '<form id="addWorkItemPopupForm" name="addWorkItemPopupForm" class="form form-horizontal" role="form" method="post">';
    //html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemName" id="workItemNameLabel" class="control-label pull-left">WorkItem Name</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="workItemName" name="workItemName" placeholder="WorkItem Name" class="form-control text-capitalize" value="" onfocus="workItemNameFocusFunction()" onblur="workItemNameBlurFunction()" style="width:100%;" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemSubHeadList" id="workItemSubHeadListLabel" class="control-label pull-left">WorkItem SubHead</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    //html += '<div class="btn-group-justified">';
    html += '<select class="form-control multiselect" name="workItemSubHeadList" id="workItemSubHeadList" multiple="multiple" size="5">';

    for (var i = 0; i < workItemSubHeadDataToAddWorkItem.length; i++) {
        html += '<option value="' + workItemSubHeadDataToAddWorkItem[i].WorkItemSubHeadGuid + '">';
        html += workItemSubHeadDataToAddWorkItem[i].WorkItemSubHeadName;
        html += '</option>';
    }

    html += '</select>';
    //html += '</div>';
    html += '</div>';
    html += '</div>';

    //html += '<fieldset class="scheduler-border">';
    //html += '<legend class="scheduler-border">WorkItem Unit</legend>';
    
    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemUnitTypeList" id="workItemUnitTypeListLabel" class="control-label pull-left">Unit Type</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control multiselect" name="workItemUnitTypeList" id="workItemUnitTypeList" size="5">';// onchange="getUnitsToAddWorkItem(this.value);">';

    //html += '<option value="" selected="selected">' + '--Select UnitType--' + '</option>';
    for (var i = 0; i < unitTypeDataToAddWorkItem.length; i++) {
        html += '<option value="' + unitTypeDataToAddWorkItem[i].UnitTypeGuid + '">';
        html += unitTypeDataToAddWorkItem[i].UnitTypeName;
        html += '</option>';
    }

    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div id="workItemUnitDiv" class="hidden">';
    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="workItemUnitList" id="workItemUnitListLabel" class="control-label pull-left">Unit</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control multiselect" name="workItemUnitList" id="workItemUnitList" size="5">';
    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    //html += '</fieldset>';

    html += '<div id="workItemSubHeadListDisplay" class="panel panel-default panel-body col-xs-12 col-sm-12 col-md-12 col-lg-12 well well-sm hidden"></div>';

    html += '<div class="form-group">';
    html += '<input id="addWorkItemBtn" class="btn btn-lg btn-primary btn-block" value="Add WorkItem" type="button" />';
    html += '</div>';

    //html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#addWorkItemPopupModal .modal-body").append(html);

    //getUnitTypesToAddWorkItem();

    $('#workItemPopupModal').modal('hide');

    //$('#addWorkItemPopupModal').modal('show');

    $('#addWorkItemPopupModal').modal({
        backdrop: 'static',
        keyboard: false//,
        //show: true
    });

    $('#workItemSubHeadList').multiselect({
        maxHeight: 150,
        enableCaseInsensitiveFiltering: true,
        numberDisplayed: 0,
        //includeSelectAllOption: true,
        nonSelectedText: '--Select WorkItem SubHead--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$('.multiselect').valid();
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                $('#workItemSubHeadListDisplay').removeClass('hidden');

                arrWorkItemSubHeadsSelectedToAddWorkItem.push({
                    workItemSubHeadGuid: element.val(),
                    workItemSubHeadName: element.text()
                });

                displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelectedToAddWorkItem);
            }
            else if (checked === false) {
                if (confirm('Do you wish to deselect the element?')) {
                    for (var i = arrWorkItemSubHeadsSelectedToAddWorkItem.length - 1; i >= 0; i--) {
                        if (arrWorkItemSubHeadsSelectedToAddWorkItem[i].workItemSubHeadGuid === element.val()) {
                            arrWorkItemSubHeadsSelectedToAddWorkItem.splice(i, 1);
                        }
                    }

                    displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelectedToAddWorkItem);
                }
                else {
                    $("#workItemSubHeadList").multiselect('select', element.val());

                    displayWorkItemSubHeadsList('workItemSubHeadListDisplay', arrWorkItemSubHeadsSelectedToAddWorkItem);
                }
            }
        },
        //onSelectAll: function (checked) {
        //    while (arrWorkItemSubHeadsSelectedToAddWorkItem.length > 0) {
        //        arrWorkItemSubHeadsSelectedToAddWorkItem.pop();
        //    }
        //    alert(checked);
        //}
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#workItemUnitTypeList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select UnitType--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$('.multiselect').valid();
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                getUnitsToAddWorkItem(element.val());
                $(document).click();
            } else if (checked === false) {
                
            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#workItemUnitList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select Unit--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            //$('.multiselect').valid();
            $(element).closest('.multiselect').valid();

            if (checked === true) {
                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#addWorkItemPopupForm').validate({ // initialize plugin
        ignore: ':hidden:not(".multiselect")',
        //ignore: ':hidden:not("#select")',
        //ignore: ":not(:visible)",
        rules: {
            workItemName: {
                required: true,
                noSpace: true
            },
            workItemSubHeadList: "required",
            workItemUnitTypeList: "required",
            workItemUnitList: "required"
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block small',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                element.closest('.form-group').append(error);
            }
        },
        submitHandler: function () {
            alert('valid form');
            return false;
        }
    });

    $('#addWorkItemBtn').on('click', function (e) {
        e.preventDefault();

        if ($("#addWorkItemPopupForm").valid()) {

            if (arrWorkItemSubHeadsSelectedToAddWorkItem.length > 0) {
                $("#preloader").show();
                $("#status").show();

                var checkExistence = checkWorkItemExistenceToAddWorkItem();

                if (checkExistence == true) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("There is already a WorkItem with this name.");
                } else if (checkExistence == false) {

                    for (var index = 0; index < arrWorkItemSubHeadsSelectedToAddWorkItem.length; index++) {
                        arrWorkItemSubHeadsSelectedToAddWorkItem[index].workItemSubHeadSequence = (index + 1);
                    }

                    var workItemSavedGuid = saveWorkItem(arrWorkItemSubHeadsSelectedToAddWorkItem);

                    if (jQuery.Guid.IsValid(workItemSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(workItemSavedGuid.toUpperCase())) {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');
                    
                        bootbox.alert("Your WorkItem is created successfully.", function () {
                            $('#addWorkItemPopupModal').modal('hide');

                            $('#workItemListMenuBtn').click();
                        });
                    } else {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');
                        bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                    }
                }

            } else {
                bootbox.alert("Please Select WorkItem SubHeads.");
            }

        }

    });

}




workItemNameFocusFunction = function () {
    // Focus = Changes the background color of input to yellow
    //document.getElementById("workItemSubHeadName").style.background = "yellow";
    $('#addWorkItemPopupModal input#workItemName').css('background-color', 'yellow');
}

workItemNameBlurFunction = function () {
    // No focus = Changes the background color of input to nothing
    //document.getElementById("workItemSubHeadName").style.background = "";
    $('#addWorkItemPopupModal input#workItemName').css('background-color', '');
}















getWorkItemSubHeadsToAddWorkItem = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetWorkItemSubHeads",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //showWorkItemSubHeadsListOptionsToAddWorkItems('workItemSubHeadList', response.d);
            retValue = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
    return retValue;
}

getUnitTypesToAddWorkItem = function () {
    var retValue;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/GetUnitTypes",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //showUnitTypesListOptionsToAddWorkItem('workItemUnitTypeList', response.d);
            retValue = response.d;
        },
        failure: function (result) {
            bootbox.alert("Error");
        }
    });
    return retValue;
}

getUnitsToAddWorkItem = function (unitTypeGuid) {
    if (jQuery.Guid.IsValid(unitTypeGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(unitTypeGuid.toUpperCase())) {
        $('#workItemUnitDiv').removeClass('hidden');

        var options = [];

        $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetUnits",
            data: '{"unitTypeGuid":"' + unitTypeGuid + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //showUnitsListOptionsBasedOnUnitTypeToAddWorkItems('workItemUnitList', response.d);
                var arr = response.d;

                //options.push({
                //    label: '--Select Unit--',
                //    value: '',
                //    selected: true
                //});

                for (var a = 0; a < arr.length ; a++) {

                    options.push({
                        label: arr[a].UnitSymbol,
                        value: arr[a].UnitGuid,
                    });

                    //var options = [
                    //    { label: 'A', title: '', value: '', selected: true },
                    //    { label: 'Option 6', title: 'Option 6', value: '6', disabled: true }
                    //];

                }

                $('#workItemUnitList').multiselect('dataprovider', options);
            },
            failure: function (result) {
                bootbox.alert("Error");
            }
        });

        $('#workItemUnitList').multiselect('refresh');

    } else if (stringIsNullOrEmpty(unitTypeGuid)) {
        $('#workItemUnitDiv').addClass('hidden');

        //var newArr = [];
        //showUnitsListOptionsBasedOnUnitTypeToAddWorkItems('workItemUnitList', newArr);
    }
}

showWorkItemSubHeadsListOptionsToAddWorkItems = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    //option_str.options[0] = new Option('--Select Sub-Head--', '');
    //option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].WorkItemSubHeadName, arr[i].WorkItemSubHeadGuid);
    }
}

showUnitTypesListOptionsToAddWorkItem = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    //option_str.options[0] = new Option('--Select UnitType--', '');
    //option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].UnitTypeName, arr[i].UnitTypeGuid);
    }
}

showUnitsListOptionsBasedOnUnitTypeToAddWorkItems = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Unit--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].UnitSymbol, arr[i].UnitGuid);
    }
}


displayWorkItemSubHeadsList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    var html = '<ul class="list-group text-left">';
    html += '<li class="nav nav-header disabled">';
    html += 'WorkItem SubHeads Selected';
    html += '</li>';
    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += (index + 1) + '. ' + arr[index].workItemSubHeadName;
        html += '</li>';
    }

    html += '</ul>';

    document.getElementById(divname).innerHTML = html;

    if (arr.length == 0) {
        div.addClass('hidden');
    }
}




checkWorkItemExistenceToAddWorkItem = function () {
    var workItemName = $.trim($('#addWorkItemPopupModal input#workItemName').val());

    var retValue = false;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/CheckWorkItemExistence",
        data: '{"workItemName":"' + workItemName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

saveWorkItem = function (arrWorkItemSubHeadsSelectedToAddWorkItem) {

    workItemSubHeadManager = arrWorkItemSubHeadsSelectedToAddWorkItem;

    var unitTypeManager = {
        unitTypeGuid: $.trim($('#addWorkItemPopupModal select#workItemUnitTypeList').val()),
    };

    var unitManager = {
        unitGuid: $.trim($('#addWorkItemPopupModal select#workItemUnitList').val()),
        //unitSymbol: $.trim($('#addWorkItemPopupModal select#workItemUnitList option:selected').text()),
        unitTypeManager: unitTypeManager
    };

    var workItemManager = {
        workItemName: $.trim($('#addWorkItemPopupModal input#workItemName').val()),
        unitManager: unitManager,
        workItemSubHeadManager: workItemSubHeadManager,
    }

    var workItemManagerJsonString = JSON.stringify(workItemManager);

    var retValue = jQuery.Guid.Empty();

    //arrWorkItemSubHeadsJSONString = JSON.stringify(getSimpleArray(arrWorkItemSubHeadsSelectedToAddWorkItem))

    //var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveWorkItem",
        data: '{"workItemManager":' + workItemManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}