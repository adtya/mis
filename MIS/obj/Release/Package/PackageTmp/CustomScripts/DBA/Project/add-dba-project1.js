﻿var projectGuid = jQuery.Guid.Empty();
var projectValue = 0;
var designationsList = [];

var fremaaOfficerEditing = null;
var fremaaOfficerTable = null;

var pmcSiteEngineerEditing = null;
var pmcSiteEngineerTable = null;










var arrSubChainages = [];
var arrWorkTypeItems = [];
var arrHeadBudgets = [];


//document.onkeydown = function (e) {
//    stopDefaultBackspaceBehaviour(e);
//}

//document.onkeypress = function (e) {
//    stopDefaultBackspaceBehaviour(e);
//}

//function stopDefaultBackspaceBehaviour(event) {
//    var event = event || window.event;
//    if (event.keyCode == 8) {
//        var elements = "HTML, BODY, TABLE, TBODY, TR, TD, DIV";
//        var d = event.srcElement || event.target;
//        var regex = new RegExp(d.tagName.toUpperCase());
//        if (d.contentEditable != 'true') { //it's not REALLY true, checking the boolean value (!== true) always passes, so we can use != 'true' rather than !== true/
//            if (regex.test(elements)) {
//                event.preventDefault ? event.preventDefault() : event.returnValue = false;
//            }
//        }
//    }
//}

$(document).ready(function () {

    var cfgCulture = 'en-IN';
    $.preferCulture(cfgCulture);
    //$.preferCulture("en-IN");

    $('#hdProjectGuid').val(jQuery.Guid.Empty());

    showDistrictOptions('districtList', getDistricts());

    showDivisionOptions('divisionList', getDivisions());

    designationsList = getDesignations();

    //showDesignationOptions('officersEngagedDesignationList', designationsList);

    //showDesignationOptions('pmcSiteEngineersDesignationList', designationsList);

    var current = 1;

    $('#projectValue').maskMoney('projectValueInWords');
    $('#subChainageValue').maskMoney();

    var defaultFormStep1 = $('.step1 input, .step1 select, .step1 textarea').serialize();

    widget = $(".step");

    btnNext = $(".next");
    btnBack = $(".back");
    btnSkip = $(".skip");

    btnSubmit = $(".submit");


    //Init Buttons and UI
    widget.not(':eq(0)').hide();
    hideButtons(current);
    setProgress(current);

    //Next Button Click Action
    btnNext.click(function () {

        if (current < widget.length) {

            //Step 1
            if (current === 1) {

                if ($('select[name="districtList"], select[name="divisionList"], input[name="projectName"], input[name="subProjectName"], input[name="projectChainage"], input[name="projectStartDate"], input[name="projectExpectedCompletionDate"], input[name="projectValue"]').valid()) {

                    if (defaultFormStep1 !== $('.step1 input, .step1 select, .step1 textarea').serialize()) {

                        var districtManager = {
                            districtGuid: $.trim($('#districtList').val())
                        };

                        var divisionManager = {
                            divisionGuid: $.trim($('#divisionList').val())
                        };

                        projectValue = $('#projectValue').val();
                        var newchar = '';
                        projectValue = projectValue.split(',').join(newchar);
                        //projectValue = projectValue.replace(/,/g , newchar);
                        projectValue = Math.abs(parseFloat(projectValue));

                        var projectManager = {
                            districtManager: districtManager,
                            divisionManager: divisionManager,
                            projectGuid: $.trim($('#hdProjectGuid').val()),
                            projectName: $.trim($('#projectName').val()),
                            subProjectName: $.trim($('#subProjectName').val()),
                            projectChainage: $.trim($('#projectChainage').val()),
                            projectStartDate: Date.parse($('#projectStartDate').val()).toString('yyyy/MM/dd'),
                            projectExpectedCompletionDate: Date.parse($('#projectExpectedCompletionDate').val()).toString('yyyy/MM/dd'),
                            projectValueInput: projectValue
                        };

                        if ($.Guid.IsValid($('#hdProjectGuid').val()) && $.Guid.IsEmpty($('#hdProjectGuid').val())) {
                            projectGuid = saveProjectStep1(projectManager);

                            $('#hdProjectGuid').val(projectGuid);

                            $('.projectName').text($.trim($('#projectName').val()));

                            if ($.Guid.IsValid(projectGuid)) {
                                if (!$.Guid.IsEmpty(projectGuid)) {
                                    bootbox.alert("Project Basic Details are saved properly.", function () {
                                        widget.show();
                                        widget.not(':eq(' + (current++) + ')').hide();
                                        hideButtons(current);
                                        setProgress(current);
                                    });
                                }
                                else {
                                    bootbox.alert("Project Basic Details are not saved properly.");
                                }
                            }
                            else {
                                bootbox.alert(projectGuid);
                            }

                        }
                        else if ($.Guid.IsValid($('#hdProjectGuid').val()) && !$.Guid.IsEmpty($('#hdProjectGuid').val())) {
                            var projectUpdated = updateProjectStep1(projectManager);

                            if (projectUpdated === 0) {
                                bootbox.alert("Project Basic Details could not be updated successfully.", function () {
                                    widget.show();
                                    widget.not(':eq(' + (current++) + ')').hide();
                                    hideButtons(current);
                                    setProgress(current);
                                });
                            }
                            else if (projectUpdated === 1) {
                                bootbox.alert("Project Basic Details are updated successfully.", function () {
                                    widget.show();
                                    widget.not(':eq(' + (current++) + ')').hide();
                                    hideButtons(current);
                                    setProgress(current);
                                });
                            }
                            else {
                                bootbox.alert(projectUpdated);
                            }

                        }

                    }
                    else {

                        if ($.Guid.IsValid($('#hdProjectGuid').val()) && !$.Guid.IsEmpty($('#hdProjectGuid').val())) {
                            widget.show();
                            widget.not(':eq(' + (current++) + ')').hide();
                            hideButtons(current);
                            setProgress(current);
                        }

                    }
                }

            }

            //Step 2
            else if (current === 2) {

                var addedFremaOfficerGuids = fremaaOfficerTable
                    .column(1)
                    .data();

                if (addedFremaOfficerGuids.length > 0) {

                    if ($.inArray(jQuery.Guid.Empty(), addedFremaOfficerGuids) !== -1) {

                        //fremaaOfficerTable.column('2:visible').order('asc').draw();

                        //var output = fremaaOfficerTable.buttons.exportData({
                        //    columns: [1, '2:visible','3:visible']
                        //});

                        bootbox.confirm({
                            title: '',
                            message: 'Some Fremaa Officers are not saved.<br>To save click Save&Proceed or else Cancel to proceed without saving.',
                            buttons: {
                                'cancel': {
                                    label: 'Cancel',
                                    className: 'btn-danger pull-left'
                                },
                                'confirm': {
                                    label: 'Save&Proceed',
                                    className: 'btn-success pull-right'
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    $('table#fremaaOfficersList a.saveNewFremaaOfficerBtn').click();
                                } else {
                                    if (fremaaOfficerEditing !== null) {
                                        /* A different row is being edited - the edit should be cancelled and this row edited */
                                        restoreFremaaOfficerRowData(fremaaOfficerTable, fremaaOfficerEditing);
                                        fremaaOfficerEditing = null;
                                        addFremaaOfficerBtn.enable();
                                    }
                                }

                                if ($.Guid.IsValid($('#hdProjectGuid').val()) && !$.Guid.IsEmpty($('#hdProjectGuid').val())) {
                                    widget.show();
                                    widget.not(':eq(' + (current++) + ')').hide();
                                    hideButtons(current);
                                    setProgress(current);
                                }

                            }
                        });

                    } else {

                        if ($.Guid.IsValid($('#hdProjectGuid').val()) && !$.Guid.IsEmpty($('#hdProjectGuid').val())) {
                            widget.show();
                            widget.not(':eq(' + (current++) + ')').hide();
                            hideButtons(current);
                            setProgress(current);
                        }

                    }

                }
                else {

                }

            }

            //Step 3
            if (current === 3) {

                var addedPmcSiteEngineerGuids = fremaaOfficerTable
                    .column(1)
                    .data();

                if (addedPmcSiteEngineerGuids.length > 0) {

                    if ($.inArray(jQuery.Guid.Empty(), addedPmcSiteEngineerGuids) !== -1) {

                        //fremaaOfficerTable.column('2:visible').order('asc').draw();

                        //var output = fremaaOfficerTable.buttons.exportData({
                        //    columns: [1, '2:visible','3:visible']
                        //});

                        bootbox.confirm({
                            title: '',
                            message: 'Some Fremaa Officers are not saved.<br>To save click Save&Proceed or else Cancel to proceed without saving.',
                            buttons: {
                                'cancel': {
                                    label: 'Cancel',
                                    className: 'btn-danger pull-left'
                                },
                                'confirm': {
                                    label: 'Save&Proceed',
                                    className: 'btn-success pull-right'
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    $('table#fremaaOfficersList a.saveNewFremaaOfficerBtn').click();
                                } else {
                                    if (fremaaOfficerEditing !== null) {
                                        /* A different row is being edited - the edit should be cancelled and this row edited */
                                        restoreFremaaOfficerRowData(fremaaOfficerTable, fremaaOfficerEditing);
                                        fremaaOfficerEditing = null;
                                        addFremaaOfficerBtn.enable();
                                    }
                                }

                                if ($.Guid.IsValid($('#hdProjectGuid').val()) && !$.Guid.IsEmpty($('#hdProjectGuid').val())) {
                                    widget.show();
                                    widget.not(':eq(' + (current++) + ')').hide();
                                    hideButtons(current);
                                    setProgress(current);
                                }

                            }
                        });

                    } else {

                        if ($.Guid.IsValid($('#hdProjectGuid').val()) && !$.Guid.IsEmpty($('#hdProjectGuid').val())) {
                            widget.show();
                            widget.not(':eq(' + (current++) + ')').hide();
                            hideButtons(current);
                            setProgress(current);
                        }

                    }

                }
                else {

                }














































                //if ($(".form").valid()) {
                //var savedProperly;
                if (arrPmcSiteEngineers.length > 0) {
                    var savedProperly = saveStep3();

                    if (savedProperly === true) {

                        bootbox.alert("Saved Properly.", function () {
                            //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                            //window.location.href = nextUrl;
                            //window.location = 'Default.aspx';

                            if (current < widget.length) {
                                // Check validation
                                //if ($(".form").valid()) {
                                widget.show();
                                widget.not(':eq(' + (current++) + ')').hide();
                                hideButtons(current);
                                setProgress(current);
                                // }
                            }

                        });

                    }
                    else {
                        bootbox.alert("Not Saved Properly.");
                    }
                }
                else {
                    bootbox.confirm({
                        title: '',
                        message: 'Are you sure yount want to leave site engineers list empty?<br>Are you sure?',
                        buttons: {
                            'cancel': {
                                label: 'No',
                                className: 'btn-default pull-left'
                            },
                            'confirm': {
                                label: 'Yes',
                                className: 'btn-danger pull-right'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                //window.location = $("a[data-bb='confirm']").attr('href');
                                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                                //window.location.href = nextUrl;
                                //window.location = 'Default.aspx';
                                if (current < widget.length) {
                                    // Check validation
                                    //if ($(".form").valid()) {
                                    widget.show();
                                    widget.not(':eq(' + (current++) + ')').hide();
                                    setProgress(current);
                                    //}
                                }
                            }
                        }
                    });
                }

            }
            //}

            //Step 4
            if (current === 4) {
                //if ($(".form").valid()) {
                //var savedProperly;
                if (arrSubChainages.length > 0) {
                    var savedProperly = saveStep4();

                    if (savedProperly === true) {

                        bootbox.alert("Saved Properly.", function () {
                            //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                            //window.location.href = nextUrl;
                            //window.location = 'Default.aspx';

                            if (current < widget.length) {
                                // Check validation
                                if ($(".form").valid()) {
                                    widget.show();
                                    widget.not(':eq(' + (current++) + ')').hide();
                                    hideButtons(current);
                                    setProgress(current);
                                }
                            }

                        });

                    }
                    else {
                        bootbox.alert("Not Saved Properly.");
                    }
                }
                else {
                    bootbox.confirm({
                        title: '',
                        message: 'Are you sure yount want to leave site engineers list empty?<br>Are you sure?',
                        buttons: {
                            'cancel': {
                                label: 'No',
                                className: 'btn-default pull-left'
                            },
                            'confirm': {
                                label: 'Yes',
                                className: 'btn-danger pull-right'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                //window.location = $("a[data-bb='confirm']").attr('href');
                                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                                //window.location.href = nextUrl;
                                //window.location = 'Default.aspx';
                                if (current < widget.length) {
                                    // Check validation
                                    //if ($(".form").valid()) {
                                    widget.show();
                                    widget.not(':eq(' + (current++) + ')').hide();
                                    setProgress(current);
                                    //}
                                }
                            }
                        }
                    });
                }

                //}
            }

            //Step 5
            if (current === 5) {
                if ($(".form").valid()) {
                    //var savedProperly;
                    var savedProperly = saveStep5();

                    if (savedProperly === true) {

                        bootbox.alert("Saved Properly.", function () {
                            //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                            //window.location.href = nextUrl;
                            //window.location = 'Default.aspx';

                            if (current < widget.length) {
                                // Check validation
                                if ($(".form").valid()) {
                                    widget.show();
                                    widget.not(':eq(' + (current++) + ')').hide();
                                    hideButtons(current);
                                    setProgress(current);
                                }
                            }

                        });

                    }
                    else {
                        bootbox.alert("Not Saved Properly.");
                    }
                }
            }

            //Step 6
            if (current === 6) {
                if ($(".form").valid()) {
                    var savedProperly = saveStep6();

                    if (savedProperly == true) {

                        bootbox.alert("Saved Properly.", function () {
                            //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                            //window.location.href = nextUrl;
                            //window.location = 'Default.aspx';

                            if (current < widget.length) {
                                // Check validation
                                if ($(".form").valid()) {
                                    widget.show();
                                    widget.not(':eq(' + (current++) + ')').hide();
                                    hideButtons(current);
                                    setProgress(current);
                                }
                            }

                        });

                    }
                    else {
                        bootbox.alert("Not Saved Properly.");
                    }
                }
            }

            //Step 7
            //if (current == 7) {
            //    if ($(".form").valid()) {
            //        var savedProperly = saveStep7();

            //        if (savedProperly == true) {

            //            bootbox.alert("The form is submitted successfully.", function () {
            //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
            //                //window.location.href = nextUrl;
            //                //window.location = 'Default.aspx';

            //                if (current < widget.length) {
            //                    // Check validation
            //                    if ($(".form").valid()) {
            //                        widget.show();
            //                        widget.not(':eq(' + (current++) + ')').hide();
            //                        hideButtons(current);
            //                        setProgress(current);
            //                    }
            //                }

            //            });

            //        }
            //        else {
            //            bootbox.alert("Not Saved Properly.");
            //        }
            //    }
            //}

            //hideButtons(current);
        }

    });

    // Back button click action
    btnBack.click(function () {

        if (current > 1) {

            current = current - 2;

            if (current === 0) {
                defaultFormStep1 = $('.step1 input, .step1 select, .step1 textarea').serialize();
            }

            if (current < widget.length) {
                widget.show();
                widget.not(':eq(' + (current++) + ')').hide();
                setProgress(current);
            }
        }

        hideButtons(current);
    });

    // Skip button click action
    btnSkip.click(function () {
        if (current < widget.length) {

            //Step 2
            if (current === 2) {
                //while (arrOfficersEngaged.length > 0) {
                //    arrOfficersEngaged.pop();
                //}

                //displayEngagedOfficersList('officersListDisplay', arrOfficersEngaged);

                widget.show();
                widget.not(':eq(' + (current++) + ')').hide();
                setProgress(current);
            }

            //Step 3
            else if (current === 3) {
                //while (arrPmcSiteEngineers.length > 0) {
                //    arrPmcSiteEngineers.pop();
                //}

                //displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);

                showSioUserOptionsToAddDbaProjects('sioUsersList', getUsersBasedOnRoleName());

                arrWorkItemsToDisplay = getWorkItems();
                showWorkItemOptionsToAddDbaProjects('workItemList', arrWorkItemsToDisplay);

                widget.show();
                widget.not(':eq(' + (current++) + ')').hide();
                setProgress(current);
            }

            ////Step 4
            //else if (current == 4) {
            //    if (arrSubChainages.length > 0) {
            //        arrSubChainages.pop();
            //    }
            //    widget.show();
            //    widget.not(':eq(' + (current++) + ')').hide();
            //    setProgress(current);
            //}

        }

        hideButtons(current);
    });





    // Submit button click
    btnSubmit.click(function () {
        $("#preloader").show();
        $("#status").show();

        $(function () {
            var check = saveStep7();
            if (check == true) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data is saved successfully.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'Default.aspx';
                });
            }
            else if (check == false) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data cannot be saved.\nPlease contact your administrator.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'Default.aspx';
                });
            }
        });
    });




    $('#districtList').select2({
        theme: 'bootstrap',
        placeholder: '--Select District--',
        //placeholder: {
        //    id: '', // the value of the option
        //    //text: ''
        //},
        allowClear: true
    }).on('change', function (e) {
        $(this).valid();

        //var selectedDistrictGuid = $(this).val();
        //var selectedDistrictText = $('option:selected', this).text();

        //if (selectedDistrictGuid === '') {

        //} else {
        //    var selectedDistrictTextSplit = multiSplit(selectedDistrictText, [' (', ')']);

        //    projectCodeArray[0] = selectedDistrictTextSplit[1];

        //    var projectCodeCreated = createProjectCode(projectCodeArray);

        //    $('#projectCode').val(projectCodeCreated);
        //    $('.projectCode').text(projectCodeCreated);
        //}

    });

    $('#divisionList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Division--',
        //placeholder: {
        //    id: '', // the value of the option
        //    //text: ''
        //},
        allowClear: true
    }).on('change', function (e) {
        $(this).valid();

        //var selectedDivisionGuid = $(this).val();
        //var selectedDivisionText = $('option:selected', this).text();

        //if (selectedDivisionGuid === '') {

        //} else {
        //    var selectedDivisionTextSplit = multiSplit(selectedDivisionText, [' (', ')']);

        //    projectCodeArray[1] = selectedDivisionTextSplit[1];

        //    var projectCodeCreated = createProjectCode(projectCodeArray);

        //    $('#projectCode').val(projectCodeCreated);
        //    $('.projectCode').text(projectCodeCreated);
        //}

    });

    //$('#projectName').on('change', function (e) {
    //    e.preventDefault();

    //    if ($('input[name="projectName"]').valid()) {
    //        projectCodeArray[2] = ($.trim($('#projectName').val())).slice(0, 3).toUpperCase();

    //        var projectCodeCreated = createProjectCode(projectCodeArray);

    //        $('#projectCode').val(projectCodeCreated);
    //        $('.projectCode').text(projectCodeCreated);
    //    }

    //});

    //$('#subProjectName').on('change', function (e) {
    //    e.preventDefault();

    //    if ($('input[name="subProjectName"]').valid()) {
    //        projectCodeArray[3] = ($.trim($('#subProjectName').val())).slice(0, 3).toUpperCase();

    //        var projectCodeCreated = createProjectCode(projectCodeArray);

    //        $('#projectCode').val(projectCodeCreated);
    //        $('.projectCode').text(projectCodeCreated);
    //    }

    //});

    $('#dpProjectStartDate').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        //$('input[name="projectStartDate"], input[name="projectExpectedCompletionDate"]').valid();
        $('input[name="' + $(this)[0].firstElementChild.name + '"]').valid();

        var projectExpectedCompletionDateStartDate = new Date(ev.date.valueOf());
        projectExpectedCompletionDateStartDate = projectExpectedCompletionDateStartDate.add(1).days();
        $('#dpProjectExpectedCompletionDate').datepicker('setStartDate', projectExpectedCompletionDateStartDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpProjectExpectedCompletionDate').datepicker('setStartDate', null);
    });

    $('#dpProjectExpectedCompletionDate').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        //$('input[name="projectStartDate"], input[name="projectExpectedCompletionDate"]').valid();
        $('input[name="' + $(this)[0].firstElementChild.name + '"]').valid();

        var projectStartDateEndDate = new Date(ev.date.valueOf());
        projectStartDateEndDate = projectStartDateEndDate.add(-1).days();
        $('#dpProjectStartDate').datepicker('setEndDate', projectStartDateEndDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpProjectStartDate').datepicker('setEndDate', null);
    });

    fremaaOfficerTableRelatedFunctions();

    pmcSiteEngineersTableRelatedFunctions();


        //Designation List
    //$('#officersEngagedDesignationList, #pmcSiteEngineersDesignationList').multiselect({
    //    maxHeight: 150,
    //    //enableCaseInsensitiveFiltering: true,
    //    numberDisplayed: 1,
    //    nonSelectedText: '--Select Designation--',
    //    disableIfEmpty: true,
    //    buttonClass: 'btn btn-default',
    //    buttonContainer: '<div class="btn-group btn-group-justified" />',
    //    //disabledText: 'There is no WorkItem SubHead...'
    //    onChange: function (element, checked) {
    //        $(element).closest('.multiselect').valid();

    //        if (checked === true) {
    //            $(document).click();
    //        } else if (checked === false) {

    //        }
    //    },
    //    templates: {
    //        button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
    //    }
    //});

    //SIO List
    $('#sioUsersList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select SIO--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            $(element).closest('.multiselect').valid();
            if (checked === true) {
                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    //WorkItem List
    $('#workItemList').multiselect({
        maxHeight: 150,
        //enableCaseInsensitiveFiltering: true,
        numberDisplayed: 1,
        nonSelectedText: '--Select WorkItem--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',
        //disabledText: 'There is no WorkItem SubHead...'
        onChange: function (element, checked) {
            $(element).closest('.multiselect').valid();
            if (checked === true) {
                displayWorkItemValueDiv(element.val());
                $(document).click();
            } else if (checked === false) {

            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    $('#addDbaProjectForm').validate({ // initialize plugin
        //ignore: ":not(:visible)",
        ignore: ':hidden:not(".multiselect")',
        rules: {
            districtList: 'required',
            divisionList: 'required',
            projectName: {
                required: true,
                noSpace: true
            },
            subProjectName: {
                required: true,
                noSpace: true
            },
            projectChainage: {
                required: true,
                noSpace: true,
                number: true
            },
            projectStartDate: {
                required: true,
                dateITA: true
            },
            projectExpectedCompletionDate: {
                required: true,
                dateITA: true
            },
            projectValue: "required",

            officersEngagedName: {
                required: true,
                noSpace: true
            },
            officersEngagedDesignationList: {
                required: true,
            },

            pmcSiteEngineersName: {
                required: true,
                noSpace: true
            },
            pmcSiteEngineersDesignationList: {
                required: true,
            },

            subChainageName: {
                required: true,
                noSpace: true
            },
            sioUsersList: "required",
            subChainageValue: "required",
            administrativeApprovalReference: {
                required: true,
                noSpace: true
            },
            technicalSanctionReference: {
                required: true,
                noSpace: true
            },
            contractorName: {
                required: true,
                noSpace: true
            },
            workOrderReference: {
                required: true,
                noSpace: true
            },
            subChainageStartingDate: {
                required: true,
                dateITA: true
            },
            subChainageExpectedCompletionDate: {
                required: true,
                dateITA: true
            },
            typeOfWorks: {
                required: true,
                noSpace: true
            },
            workItemValue: {
                required: true,
                number: true
            },

            landAcquisitionRadio: "required"
        },
        messages: {
            districtList: {
                required: "Please Enter Project District."
            },
            divisionList: {
                required: "Please Enter Project Division."
            },
            projectName: {
                required: "Please Enter Project Name.",
                noSpace: "Project Name cannot be blank."
            },
            subProjectName: {
                required: "Please Enter Sub-Project Name.",
                noSpace: "Sub-Project Name cannot be blank."
            },
            projectChainage: {
                required: "Please Enter Chainage of the Project.",
                noSpace: "Chainage of the Project cannot be blank.",
                number: 'Please Enter a Valid Project Chainage.'
            },
            projectStartDate: {
                required: "Please Enter the Starting Date of the Project.",
                dateITA: "Please Enter the Starting Date of the Project as: dd/mm/yyyy"
            },
            projectExpectedCompletionDate: {
                required: "Please Enter the Expected Completion Date of the Project.",
                dateITA: "Please Enter the Expected Completion Date of the Project as: dd/mm/yyyy"
            },
            projectValue: {
                required: "Please Enter the Value of the Project."
            },

            officersEngagedName: {
                required: "Please Enter the Officer Name.",
                noSpace: "Officer Name cannot be blank."
            },
            officersEngagedDesignationList: {
                required: "Please Select the Officers Designation.",
            },

            pmcSiteEngineersName: {
                required: "Please Enter the Site Engineer Name.",
                noSpace: "Site Engineer Name cannot be blank."
            },
            pmcSiteEngineersDesignationList: {
                required: "Please Select the Site Engineer Designation.",
            },

            subChainageName: {
                required: "Please Enter the Name of the Sub-Chainage.",
                noSpace: "Sub-Chainage Name cannot be blank."
            },
            sioUsersList: {
                required: "Please Enter the SIO for the Sub-Chainage."
            },
            subChainageValue: {
                required: "Please Enter the Value of the Sub-Chainage."
            },
            administrativeApprovalReference: {
                required: "Please Enter the AA Reference of the Sub-Chainage.",
                noSpace: "AA Reference cannot be blank."
            },
            technicalSanctionReference: {
                required: "Please Enter the TS Reference of the Sub-Chainage.",
                noSpace: "TS Reference cannot be blank."
            },
            contractorName: {
                required: "Please Enter the Name of the Contractor of the Sub-Chainage.",
                noSpace: "Contractor Name cannot be blank."
            },
            workOrderReference: {
                required: "Please Enter the W.O. Reference of the Sub-Chainage.",
                noSpace: "W.O. Reference cannot be blank."
            },
            subChainageStartingDate: {
                required: "Please Enter the Starting Date of the Sub-Chainage.",
                dateITA: "Please Enter the Starting Date of the Sub-Chainage as: dd/mm/yyyy"
            },
            subChainageExpectedCompletionDate: {
                required: "Please Enter the Expected Completion Date of the Sub-Chainage.",
                dateITA: "Please Enter the Expected Completion Date of the Sub-Chainage as: dd/mm/yyyy"
            },
            typeOfWorks: {
                required: "Please Enter the Type of Work.",
                noSpace: "Type of Work cannot be blank."
            },
            workItemValue: {
                required: "Please Enter the WorkItem Value.",
                number: "Please Enter a Valid WorkItem Value."
            },

            landAcquisitionRadio: "Please Select the Land Acquisition Option."
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block small',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('multiselect')) {
                //error.insertAfter($('#' + element.attr('id')));
                //error.insertBefore(element);
                error.insertAfter(element.parent().find('.btn-group'));
                //$(this).parents("fieldset").find("*:not('legend')").toggle();
            } else if (element.hasClass('select2')) {
                //error.insertAfter($('#' + element.attr('id')));
                //error.insertBefore(element);
                error.insertAfter(element.parent().find('.select2-container'));
                //$(this).parents("fieldset").find("*:not('legend')").toggle();
            } else {
                //element.closest('.form-group').append(error);
                error.insertAfter(element);
            }
        },
        submitHandler: function () {
            return false;
        }
    });

    $('#dpSubChainageStartingDate').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var subChainageCompletionDateStartDate = new Date(ev.date.valueOf());
        subChainageCompletionDateStartDate = subChainageCompletionDateStartDate.add(1).days();
        $('#dpSubChainageExpectedCompletionDate').datepicker('setStartDate', subChainageCompletionDateStartDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpSubChainageExpectedCompletionDate').datepicker('setStartDate', null);
    });

    $('#dpSubChainageExpectedCompletionDate').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        var subChainageStartDateEndDate = new Date(ev.date.valueOf());
        subChainageStartDateEndDate = subChainageStartDateEndDate.add(-1).days();
        $('#dpSubChainageStartingDate').datepicker('setEndDate', subChainageStartDateEndDate);
        $(this).blur();
        $(this).datepicker('hide');
    }).on('clearDate', function (selected) {
        $('#dpSubChainageStartingDate').datepicker('setEndDate', null);
    });

    

    addedFinancialSubHeadsTable = $('table#addedFinancialSubHeadsTable').DataTable({
        //var workItemTable = $('#workItemsList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        stateSave: true,
        //destroy: true,
        orderable: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                targets: [1],
                //orderable: false
            },
            {
                className: 'dt-right',
                targets: [2],
                orderable: false
            },
            {
                targets: [3],
                orderable: false,
                searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewWorkItemBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>'
            }
        ],
        order: [[1, 'desc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Assign Value',
                className: 'btn-success assignValueToFinancialSubHeadBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    createAssignValueToFinancialSubHeadPopupForm();

                    e.stopPropagation();
                }
            }
        ]

    });

    addedFinancialSubHeadsTable.on('order.dt search.dt', function () {
        addedFinancialSubHeadsTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    

    $('#addOfficersEngagedBtn').on('click', function (e) {

        e.preventDefault();

        if ($('input[name="officersEngaged"], select[name="officersEngagedDesignationList"]').valid()) {

            //if (arrOfficersEngaged.length === array2.length && arrOfficersEngaged.every(function (element, index) { return ($.inArray(element, array2) != -1) })) {

            //}

            $('#officersListDisplay').removeClass('hidden');

            arrOfficersEngaged.push({
                fremaaOfficerName: $('#officersEngagedName').val() + ' ' + $('#officersEngagedDesignationList option:selected').text()
            });

            $('#officersEngagedName').val('');

            displayEngagedOfficersList('officersListDisplay', arrOfficersEngaged);
        }

    });

    $('#addPmcSiteEngineersBtn').on('click', function (e) {

        e.preventDefault();

        if ($('input[name="pmcSiteEngineers"], select[name="pmcSiteEngineersDesignationList"]').valid()) {

            $('#pmcSiteEngineersListDisplay').removeClass('hidden');

            arrPmcSiteEngineers.push({
                siteEngineerName: $('#pmcSiteEngineersName').val() + ' ' + $('#pmcSiteEngineersDesignationList option:selected').text()
            });

            $('#pmcSiteEngineersName').val('');

            displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);
        }

    });

    $('#addSubChainagesBtn').on('click', function (e) {

        if ($(".form").valid()) {

            $('#subChainagesListDisplay').removeClass('hidden');

            var subChainageName = $('#subChainageName').val();
            var subChainageSioGuid = $('#sioUsersList').val();

            var subChainageValue = $('#subChainageValue').val();
            var newchar = '';
            subChainageValue = subChainageValue.split(',').join(newchar);
            //subChainageValue = subChainageValue.replace(/,/g , newchar);
            subChainageValue = parseFloat(subChainageValue);

            var administrativeApprovalReference = $('#administrativeApprovalReference').val();
            var technicalSanctionReference = $('#technicalSanctionReference').val();
            var contractorName = $('#contractorName').val();
            var workOrderReference = $('#workOrderReference').val();

            var subChainageStartingDate = $('#subChainageStartingDate').val();
            subChainageStartingDate = Date.parse(subChainageStartingDate).toString('yyyy/MM/dd');

            var subChainageExpectedCompletionDate = $('#subChainageExpectedCompletionDate').val();
            subChainageExpectedCompletionDate = Date.parse(subChainageExpectedCompletionDate).toString('yyyy/MM/dd');

            var typeOfWorks = $('#typeOfWorks').val();
            //var workTypeItems = $('#workTypeItems').val();

            arrSubChainages.push({
                subChainageName: subChainageName,
                subChainageSioGuid: subChainageSioGuid,
                subChainageValue: subChainageValue,
                administrativeApprovalReference: administrativeApprovalReference,
                technicalSanctionReference: technicalSanctionReference,
                contractorName: contractorName,
                workOrderReference: workOrderReference,
                subChainageStartingDate: subChainageStartingDate,
                subChainageExpectedCompletionDate: subChainageExpectedCompletionDate,
                typeOfWorks: typeOfWorks,
                arrWorkTypeItems: arrWorkTypeItems
            });

            displaySubChainagesList('subChainagesListDisplay', arrSubChainages);
        }

    });

    $('#addWorkTypeItemsBtn').on('click', function (e) {

        $('#workTypeItemsListDisplay').removeClass('hidden');

        var workTypeItems = $('#workTypeItemList').val();

        arrWorkTypeItems.push(workTypeItems);

        $('option:selected', this).remove();

        displayWorkTypeItemsList('workTypeItemsListDisplay', arrWorkTypeItems);
    });

    $('#createNewHeadBtn').on('click', function (e) {
        $("#popupModal .modal-body").empty();

        var projectValue = $("#projectValue").val();
        var newchar = '';
        projectValue = projectValue.split(',').join(newchar);
        //subChainageValue = subChainageValue.replace(/,/g , newchar);
        projectValue = parseFloat(projectValue);

        var remainingProjectValue = projectValue;

        if (arrHeadBudgets.length > 0) {
            for (var i = 0; i < arrHeadBudgets.length; i++) {
                var a = arrHeadBudgets[i].headBudget;
                remainingProjectValue = remainingProjectValue - a;
            }
        }

        if (remainingProjectValue > 0) {
            var html = "<div class=\"container-fluid col-md-12\">";
            html += "<div class=\"row\">";
            html += "<div class=\"row\">";
            html += "<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">";
            html += "<div class=\"panel panel-default\">";
            html += "<div class=\"panel-body\">";
            html += "<div class=\"text-center\">";
            html += "<img src=\"Images/create-financial.png\" class=\"login\" height=\"70\" />";
            html += "<h2 class=\"text-center\">Financial Head</h2>";

            html += "<div class=\"panel-body\">";
            html += "<form id=\"popupForm\" class=\"popupForm form form-horizontal\" method=\"post\">";
            html += "<fieldset>";

            html += "<div class=\"form-group\">";
            html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
            html += "<label id=\"totalBudgetLblText\" class=\"control-label\">" + "Total Budget: " + "</label>";
            html += "</div>";
            html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
            html += "<label id=\"totalBudgetLbl\" class=\"control-label\">" + accounting.formatMoney(projectValue, { symbol: "Rs.", format: "%s %v" }) + "</label>";
            html += "</div>";

            html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
            html += "<label id=\"remainingBudgetLblText\" class=\"control-label\">" + "Remaining Budget: " + "</label>";
            html += "</div>";
            html += "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">";
            html += "<label id=\"remainingBudgetLbl\" name=\"remainingBudgetLbl\" class=\"control-label\" value=\"" + accounting.formatMoney(remainingProjectValue, { symbol: "Rs.", format: "%s %v" }) + "\">" + accounting.formatMoney(remainingProjectValue, { symbol: "Rs.", format: "%s %v" }) + "</label>";
            html += "</div>";
            html += "</div>";

            //html += "<div class=\"form-group\">";
            //html += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
            //html += "<label id=\"remainingBudgetLblText\" class=\"control-label\">" + "Remaining Budget: " + "</label>";
            //html += "</div>";
            //html += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
            //html += "<label id=\"remainingBudgetLbl\" name=\"remainingBudgetLbl\" class=\"control-label\">" + accounting.formatMoney(remainingProjectValue, { symbol: "Rs.", format: "%s %v" }) + "</label>";
            //html += "</div>";
            //html += "</div>";

            html += "<div class=\"form-group\">";
            html += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
            html += "<label for=\"headName\" id=\"headNameLabel\" class=\"control-label\">Name of Head</label>";
            html += "</div>";
            html += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
            html += "<input id=\"headName\" name=\"headName\" placeholder=\"Head Name\" class=\"form-control\" />";
            html += "</div>";
            html += "</div>";

            html += "<div class=\"form-group\">";
            html += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
            html += "<label for=\"headBudget\" id=\"headBudgetLabel\" class=\"control-label\">Budget</label>";
            html += "</div>";
            html += "<div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">";
            html += "<input id=\"headBudget\" name=\"headBudget\" placeholder=\"Budget\" class=\"form-control\" />";
            html += "</div>";
            html += "</div>";

            html += "<div class=\"form-group\">";
            html += "<button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\" id=\"createHeadBtn\">Create Head</button>";
            html += "</div>";

            html += "</fieldset>";
            html += "</form>";
            html += "</div>";

            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";

            $("#popupModal .modal-body").append(html);
            $('#popupModal').modal('show');

            callOtherFuctions();

            $('#headBudget').maskMoney();

            $('#createHeadBtn').on('click', function (e) {

                if ($(".popupForm").valid()) {
                    var headName = $('#headName').val();

                    var headBudget = $('#headBudget').val();
                    var newchar = '';
                    headBudget = headBudget.split(',').join(newchar);
                    //subChainageValue = subChainageValue.replace(/,/g , newchar);
                    headBudget = parseFloat(headBudget);

                    arrHeadBudgets.push({
                        headName: headName,
                        headBudget: headBudget
                    });

                    createHeadTable();
                }

            });
        }



    });

});

// Hide buttons according to the current step
hideButtons = function (current) {
    var limit = parseInt(widget.length);

    $('.action').hide();

    if (current < limit) {
        btnNext.show();
    }

    if (current > 1) {
        btnBack.show();
    }

    if (current === limit) {
        btnNext.hide();
        btnSubmit.show();
    }

    if (current === 2 || current === 3) {
        btnSkip.show();
    }
}

// Change progress bar action
setProgress = function (currstep) {
    var percent = parseFloat(100 / widget.length) * currstep;
    percent = percent.toFixed();
    $('.progress-bar').css('width', percent + '%').html(percent + '%');
    $('#step-display').html('Step ' + currstep + ' of ' + widget.length);
}





saveProjectStep1 = function (projectManager) {
    var arrProjectManagerJsonString = JSON.stringify(projectManager);

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "AddDBAProject.aspx/SaveProjectStep1",
        data: '{"projectManager":' + arrProjectManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        error: function (msg) {
            retValue = msg;
        }
    });

    return retValue;
}

updateProjectStep1 = function (projectManager) {
    var arrProjectManagerJsonString = JSON.stringify(projectManager);

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "AddDBAProject.aspx/UpdateProjectStep1",
        data: '{"projectManager":' + arrProjectManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        error: function (msg) {
            retValue = msg;
        }
    });

    return retValue;
}





fremaaOfficerTableRelatedFunctions = function () {

    fremaaOfficerTable = $('table#fremaaOfficersList').DataTable({
        //responsive: true,
        pagingType: 'full_numbers',
        searchHighlight: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'dt-left',
                targets: [2],
                orderable: false,
            },
            {
                className: 'dt-left',
                targets: [3],
                orderable: false,
            },
            {
                targets: [4],
                orderable: false,
                searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewWorkItemBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>'
            }
        ],
        order: [[2, 'asc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Create New Fremaa Officer',
                className: 'btn-success addNewFremaaOfficerBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewFremaaOfficer();

                    e.stopPropagation();
                }
            }
        ]

    });

    fremaaOfficerTable.on('order.dt search.dt', function () {
        fremaaOfficerTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addFremaaOfficerBtn = fremaaOfficerTable.button(['.addNewFremaaOfficerBtn']);

    addNewFremaaOfficer = function () {

        if (fremaaOfficerEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreFremaaOfficerRowData(fremaaOfficerTable, fremaaOfficerEditing);
            fremaaOfficerEditing = null;
            addFremaaOfficerBtn.enable();
        }

        var newFremaaOfficerRow = fremaaOfficerTable.row.add([
            '',
            jQuery.Guid.Empty(),
            '',
            '',
            ''
        ]).draw(false);

        var newFremaaOfficerRowNode = newFremaaOfficerRow.node();

        addNewFremaaOfficerRow(fremaaOfficerTable, newFremaaOfficerRowNode);

        fremaaOfficerEditing = newFremaaOfficerRowNode;

        addFremaaOfficerBtn.disable();

        $('table#fremaaOfficersList input#fremaaOfficerName').focus();

        $(newFremaaOfficerRowNode).css('color', 'red').animate({ color: 'black' });
    };

    $(document).on('click', 'table#fremaaOfficersList a.saveNewFremaaOfficerBtn', function (e) {
        e.preventDefault();

        if ($('input[name="officersEngagedName"], select[name="fremaaOfficerDesignationList"]').valid()) {

            //var checkExistence = false;

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            if (fremaaOfficerEditing !== null && fremaaOfficerEditing === selectedRow) {
                /* A different row is being edited - the edit should be cancelled and this row edited */
                var checkExistence = checkProjectStep2FremaaOfficerExistence(fremaaOfficerTable, fremaaOfficerEditing);

                if (checkExistence === true) {
                    bootbox.alert('There is already a Fremaa Officer with this name & designation.');
                } else if (checkExistence === false) {
                    var fremaaOfficerSavedGuid = saveProjectStep2FremaaOfficer();

                    if (jQuery.Guid.IsValid(fremaaOfficerSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(fremaaOfficerSavedGuid.toUpperCase())) {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');

                        bootbox.alert('Fremaa Officer is created successfully.', function () {
                            if (fremaaOfficerEditing !== null && fremaaOfficerEditing == selectedRow) {
                                /* A different row is being edited - the edit should be cancelled and this row edited */
                                saveNewFremaaOfficerRow(fremaaOfficerTable, fremaaOfficerEditing, fremaaOfficerSavedGuid);
                                fremaaOfficerEditing = null;
                                addFremaaOfficerBtn.enable();
                            }
                        });
                    } else {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');
                        bootbox.alert('Your data cannot be saved.<br>Please contact your database administrator.');
                    }
                }
            }

        }

    });

    $(document).on('click', 'table#fremaaOfficersList a.cancelNewFremaaOfficerEditBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (fremaaOfficerEditing !== null && fremaaOfficerEditing === selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            removeNewFremaaOfficerRowCreatedOnCancel(fremaaOfficerTable, selectedRow);
            fremaaOfficerEditing = null;
            addFremaaOfficerBtn.enable();
        }

    });

    $(document).on('click', 'table#fremaaOfficersList a.editFremaaOfficerBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (fremaaOfficerEditing !== null && fremaaOfficerEditing !== selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreFremaaOfficerRowData(fremaaOfficerTable, fremaaOfficerEditing);
            editFremaaOfficerRow(fremaaOfficerTable, selectedRow);
            fremaaOfficerEditing = selectedRow;
            addFremaaOfficerBtn.enable();
            //$('#unitTypePopupModal input#unitTypeName').focus();

            var el = $('table#fremaaOfficersList input#officersEngagedName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);
        } else {
            /* No row currently being edited */
            editFremaaOfficerRow(fremaaOfficerTable, selectedRow);
            fremaaOfficerEditing = selectedRow;
            addFremaaOfficerBtn.enable();
            //$('#unitTypePopupModal input#unitTypeName').focus();

            var el = $('table#fremaaOfficersList input#officersEngagedName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);
        }
    });

    $(document).on('click', 'table#fremaaOfficersList a.updateFremaaOfficerBtn', function (e) {
        e.preventDefault();

        if ($('input[name="officersEngagedName"], select[name="fremaaOfficerDesignationList"]').valid()) {

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            if (fremaaOfficerEditing !== null && fremaaOfficerEditing === selectedRow) {
                /* A different row is being edited - the edit should be cancelled and this row edited */
                var checkExistence = checkProjectStep2FremaaOfficerExistence(fremaaOfficerTable, fremaaOfficerEditing);

                if (checkExistence === true) {
                    bootbox.alert('There is already a Fremaa Officer with this name & designation.');
                } else if (checkExistence === false) {
                    var fremaaOfficerUpdated = updateProjectStep2FremaaOfficer(fremaaOfficerTable.row(selectedRow).data()[1]);

                    if (fremaaOfficerUpdated === 1) {
                        bootbox.alert("Fremaa Officer is updated successfully.", function () {
                            if (fremaaOfficerEditing !== null && fremaaOfficerEditing == selectedRow) {
                                /* A different row is being edited - the edit should be cancelled and this row edited */
                                updateFremaaOfficerRow(fremaaOfficerTable, fremaaOfficerEditing);
                                fremaaOfficerEditing = null;
                                addFremaaOfficerBtn.enable();
                            }
                        });
                    } else if (fremaaOfficerUpdated === 0) {
                        bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
                    }

                }

            }

        }

    });

    $(document).on('click', 'table#fremaaOfficersList a.cancelFremaaOfficerEditBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (fremaaOfficerEditing !== null && fremaaOfficerEditing === selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreFremaaOfficerRowData(fremaaOfficerTable, fremaaOfficerEditing);
            fremaaOfficerEditing = null;
            addFremaaOfficerBtn.enable();
        }

    });

    $(document).on('click', 'table#fremaaOfficersList a.deleteFremaaOfficerBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        var fremaaOfficerDeleted = deleteProjectStep2FremaaOfficer(fremaaOfficerTable.row(selectedRow).data()[1]);

        if (fremaaOfficerDeleted == 1) {
            bootbox.alert("Fremaa Officer is deleted successfully.", function () {
                fremaaOfficerTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (fremaaOfficerDeleted == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
        }

    });

}

restoreFremaaOfficerRowData = function (fremaaOfficerTable, previousRow) {
    var previousRowData = fremaaOfficerTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewFremaaOfficerRowCreatedOnCancel(fremaaOfficerTable, previousRow);
    } else {
        fremaaOfficerTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;

    }

};

removeNewFremaaOfficerRowCreatedOnCancel = function (fremaaOfficerTable, selectedRow) {

    fremaaOfficerTable
        .row(selectedRow)
        .remove()
        .draw();

    fremaaOfficerTable.on('order.dt search.dt', function () {
        fremaaOfficerTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

addNewFremaaOfficerRow = function (fremaaOfficerTable, selectedRow) {
    var selectedRowData = fremaaOfficerTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    fremaaOfficerTable.column('1:visible').order('asc').draw();

    fremaaOfficerTable.on('order.dt search.dt', function () {

        var x = fremaaOfficerTable.order();

        if (x[0][1] === 'asc') {
            var a = fremaaOfficerTable.column(0, { search: 'applied', order: 'applied' }).nodes();
            for (var i = 1; i < a.length; i++) {
                a[i].innerHTML = i;
            }
        } else {
            fremaaOfficerTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }
        availableTds[0].innerHTML = '*';
    }).draw(false);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[0].innerHTML = '*';
    availableTds[1].innerHTML = '<input type="text" id="officersEngagedName" name="officersEngagedName" placeholder="Officer Name" class="form-control" value="' + selectedRowData[2] + '" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)" style="width:100%;">';
    availableTds[2].innerHTML = '<select class="form-control select2" name="fremaaOfficerDesignationList" id="fremaaOfficerDesignationList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)"></select>';//Unit
    availableTds[3].innerHTML = '<a href="#" class="saveNewFremaaOfficerBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelNewFremaaOfficerEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';

    showDesignationOptions('fremaaOfficerDesignationList', designationsList);

    $('#fremaaOfficerDesignationList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Designation--',
        //placeholder: {
        //    id: '', // the value of the option
        //    //text: ''
        //},
        allowClear: true
    }).on('change', function (e) {
        $(this).valid();

        //var selectedDivisionGuid = $(this).val();
        //var selectedDivisionText = $('option:selected', this).text();

        //if (selectedDivisionGuid === '') {

        //} else {
        //    var selectedDivisionTextSplit = multiSplit(selectedDivisionText, [' (', ')']);

        //    projectCodeArray[1] = selectedDivisionTextSplit[1];

        //    var projectCodeCreated = createProjectCode(projectCodeArray);

        //    $('#projectCode').val(projectCodeCreated);
        //    $('.projectCode').text(projectCodeCreated);
        //}

    });

}

checkProjectStep2FremaaOfficerExistence = function (fremaaOfficerTable, selectedRow) {
    var retValue = false;

    var selectedRowData = fremaaOfficerTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableSelects = $('select', selectedRow);

    var addedFremaOfficerNames = fremaaOfficerTable
        .column(2)
        .data();

    var addedFremaOfficerDesignations = fremaaOfficerTable
        .column(3)
        .data();

    if ($.inArray($.trim(availableInputs[0].value), addedFremaOfficerNames) !== -1 && $.inArray($.trim(availableSelects[0].selectedOptions[0].text), addedFremaOfficerDesignations) !== -1) {
        retValue = true;
    }

    return retValue;
}

saveNewFremaaOfficerRow = function (fremaaOfficerTable, selectedRow, fremaaOfficerSavedGuid) {

    var selectedRowData = fremaaOfficerTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableSelects = $('select', selectedRow);

    selectedRowData[1] = fremaaOfficerSavedGuid;
    selectedRowData[2] = $.trim(availableInputs[0].value);
    selectedRowData[3] = $.trim(availableSelects[0].selectedOptions[0].text);
    selectedRowData[4] = '<a href="#" class="editFremaaOfficerBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteFremaaOfficerBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    fremaaOfficerTable.row(selectedRow).data(selectedRowData);

    fremaaOfficerTable.on('order.dt search.dt', function () {
        fremaaOfficerTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

editFremaaOfficerRow = function (fremaaOfficerTable, selectedRow) {
    var selectedRowData = fremaaOfficerTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    availableTds[1].innerHTML = '<input type="text" id="officersEngagedName" name="officersEngagedName" placeholder="Officer Name" class="form-control" value="' + selectedRowData[2] + '" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)" style="width:100%;">';
    availableTds[2].innerHTML = '<select class="form-control select2" name="fremaaOfficerDesignationList" id="fremaaOfficerDesignationList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)"></select>';
    availableTds[3].innerHTML = '<a href="#" class="updateFremaaOfficerBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelFremaaOfficerEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';

    showDesignationOptions('fremaaOfficerDesignationList', designationsList);

    //$('#fremaaOfficerDesignationList option:contains("' + selectedRowData[3] + '")');

    $('#fremaaOfficerDesignationList option:contains("' + selectedRowData[3] + '")').prop('selected', true);

    //$('#fremaaOfficerDesignationList option')
    //    .filter(function (i, e) { return $(e).text() == selectedRowData[3] })

    $('#fremaaOfficerDesignationList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Designation--',
        //placeholder: {
        //    id: '', // the value of the option
        //    //text: ''
        //},
        allowClear: true
    }).on('change', function (e) {
        $(this).valid();
    });
}

updateFremaaOfficerRow = function (fremaaOfficerTable, selectedRow) {

    var selectedRowData = fremaaOfficerTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableSelects = $('select', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[2] = $.trim(availableInputs[0].value);
    selectedRowData[3] = $.trim(availableSelects[0].selectedOptions[0].text);
    selectedRowData[4] = '<a href="#" class="editFremaaOfficerBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteFremaaOfficerBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    fremaaOfficerTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

}

saveProjectStep2FremaaOfficer = function () {

    var retValue = jQuery.Guid.Empty();

    var projectAndFremaaOfficersRelationManager = {
        projectManager: {
            projectGuid: projectGuid
        },
        fremaaOfficerName: $.trim($('table#fremaaOfficersList input#officersEngagedName').val()) + ', ' + $.trim($('table#fremaaOfficersList select#fremaaOfficerDesignationList option:selected').text())
    };

    var projectAndFremaaOfficersRelationManagerJsonString = JSON.stringify(projectAndFremaaOfficersRelationManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "AddDBAProject.aspx/SaveProjectStep2FremaaOfficer",
        data: '{"projectAndFremaaOfficersRelationManager":' + projectAndFremaaOfficersRelationManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updateProjectStep2FremaaOfficer = function (fremaaOfficerProjectRelationshipGuid) {

    var retValue = 0;

    var projectAndFremaaOfficersRelationManager = {
        fremaaOfficerProjectRelationshipGuid: fremaaOfficerProjectRelationshipGuid,
        projectManager: {
            projectGuid: projectGuid
        },
        fremaaOfficerName: $.trim($('table#fremaaOfficersList input#officersEngagedName').val()) + ', ' + $.trim($('table#fremaaOfficersList select#fremaaOfficerDesignationList option:selected').text())
    };

    var projectAndFremaaOfficersRelationManagerJsonString = JSON.stringify(projectAndFremaaOfficersRelationManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "AddDBAProject.aspx/UpdateProjectStep2FremaaOfficer",
        data: '{"projectAndFremaaOfficersRelationManager":' + projectAndFremaaOfficersRelationManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteProjectStep2FremaaOfficer = function (fremaaOfficerProjectRelationshipGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "AddDBAProject.aspx/DeleteProjectStep2FremaaOfficer",
        data: '{"projectGuid":"' + projectGuid + '","fremaaOfficerProjectRelationshipGuid":"' + fremaaOfficerProjectRelationshipGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}





pmcSiteEngineersTableRelatedFunctions = function () {

    pmcSiteEngineerTable = $('table#pmcSiteEngineersList').DataTable({
        //responsive: true,
        pagingType: 'full_numbers',
        searchHighlight: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                orderable: false,
                searchable: false
            },
            {
                className: 'dt-left',
                targets: [2],
                orderable: false,
            },
            {
                className: 'dt-left',
                targets: [3],
                orderable: false,
            },
            {
                targets: [4],
                orderable: false,
                searchable: false,
                //data: null,
                //defaultContent: '<a href="#" class="viewWorkItemBtn"><i class="ui-tooltip fa fa-file-text-o" style="font-size: 22px;" data-original-title="View" title="View"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="editWorkItemBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteWorkItemBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>' + '</td>'
            }
        ],
        order: [[2, 'asc']],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                            '<option value="5">5</option>' +
                            '<option value="10">10</option>' +
                            '<option value="15">15</option>' +
                            '<option value="20">20</option>' +
                            '<option value="25">25</option>' +
                            '<option value="30">30</option>' +
                            '<option value="35">35</option>' +
                            '<option value="40">40</option>' +
                            '<option value="45">45</option>' +
                            '<option value="50">50</option>' +
                            '<option value="100">100</option>' +
                            '<option value="-1">All</option>' +
                            '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5,
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        //dom: '<lf<t>ip>',
        //dom: '<"wrapper"flipt>',
        //dom: '<"top"i>rt<"bottom"flp><"clear">',
        buttons: [
            {
                text: '+ Create New Site Engineer',
                className: 'btn-success addNewPmcSiteEngineerBtn',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    addNewPmcSiteEngineer();

                    e.stopPropagation();
                }
            }
        ]

    });

    pmcSiteEngineerTable.on('order.dt search.dt', function () {
        pmcSiteEngineerTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var addPmcSiteEngineerBtn = pmcSiteEngineerTable.button(['.addNewFremaaOfficerBtn']);

    addNewPmcSiteEngineer = function () {

        if (pmcSiteEngineerEditing !== null) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restorePmcSiteEngineerRowData(pmcSiteEngineerTable, pmcSiteEngineerEditing);
            pmcSiteEngineerEditing = null;
            addPmcSiteEngineerBtn.enable();
        }

        var newPmcSiteEngineerRow = pmcSiteEngineerTable.row.add([
            '',
            jQuery.Guid.Empty(),
            '',
            '',
            ''
        ]).draw(false);

        var newPmcSiteEngineerRowNode = newPmcSiteEngineerRow.node();

        addNewPmcSiteEngineerRow(pmcSiteEngineerTable, newPmcSiteEngineerRowNode);

        pmcSiteEngineerEditing = newPmcSiteEngineerRowNode;

        addPmcSiteEngineerBtn.disable();

        $('table#pmcSiteEngineersList input#pmcSiteEngineersName').focus();

        $(newPmcSiteEngineerRowNode).css('color', 'red').animate({ color: 'black' });
    };

    $(document).on('click', 'table#pmcSiteEngineersList a.saveNewPmcSiteEngineerBtn', function (e) {
        e.preventDefault();

        if ($('input[name="pmcSiteEngineersName"], select[name="pmcSiteEngineersDesignationList"]').valid()) {

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            if (pmcSiteEngineerEditing !== null && pmcSiteEngineerEditing === selectedRow) {
                /* A different row is being edited - the edit should be cancelled and this row edited */
                var checkExistence = checkProjectStep3PmcSiteEngineerExistence(pmcSiteEngineerTable, pmcSiteEngineerEditing);

                if (checkExistence === true) {
                    bootbox.alert('There is already a Pmc Site Engineer with this name & designation.');
                } else if (checkExistence === false) {
                    var pmcSiteEngineerSavedGuid = saveProjectStep3PmcSiteEngineer();

                    if (jQuery.Guid.IsValid(pmcSiteEngineerSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(pmcSiteEngineerSavedGuid.toUpperCase())) {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');

                        bootbox.alert('Pmc Site Engineer is created successfully.', function () {
                            if (pmcSiteEngineerEditing !== null && pmcSiteEngineerEditing == selectedRow) {
                                /* A different row is being edited - the edit should be cancelled and this row edited */
                                saveNewPmcSiteEngineerRow(pmcSiteEngineerTable, pmcSiteEngineerEditing, pmcSiteEngineerSavedGuid);
                                pmcSiteEngineerEditing = null;
                                addPmcSiteEngineerBtn.enable();
                            }
                        });
                    } else {
                        $('#status').delay(300).fadeOut();
                        $('#preloader').delay(350).fadeOut('slow');
                        bootbox.alert('Your data cannot be saved.<br>Please contact your database administrator.');
                    }
                }
            }

        }

    });

    $(document).on('click', 'table#pmcSiteEngineersList a.cancelNewPmcSiteEngineerEditBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (pmcSiteEngineerEditing !== null && pmcSiteEngineerEditing === selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            removeNewPmcSiteEngineerRowCreatedOnCancel(pmcSiteEngineerTable, selectedRow);
            pmcSiteEngineerEditing = null;
            addPmcSiteEngineerBtn.enable();
        }

    });

    $(document).on('click', 'table#pmcSiteEngineersList a.editPmcSiteEngineerBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (pmcSiteEngineerEditing !== null && pmcSiteEngineerEditing !== selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restorePmcSiteEngineerRowData(pmcSiteEngineerTable, pmcSiteEngineerEditing);
            editPmcSiteEngineerRow(pmcSiteEngineerTable, selectedRow);
            pmcSiteEngineerEditing = selectedRow;
            addPmcSiteEngineerBtn.enable();
            //$('#unitTypePopupModal input#unitTypeName').focus();

            var el = $('table#pmcSiteEngineersList input#pmcSiteEngineersName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);
        } else {
            /* No row currently being edited */
            editPmcSiteEngineerRow(pmcSiteEngineerTable, selectedRow);
            pmcSiteEngineerEditing = selectedRow;
            addPmcSiteEngineerBtn.enable();
            //$('#unitTypePopupModal input#unitTypeName').focus();

            var el = $('table#pmcSiteEngineersList input#pmcSiteEngineersName');
            var elemLen = el.val().length;
            el.focus();

            //el.selectRange(0, elemLen);
            el.setCursorPosition(elemLen);
        }
    });

    $(document).on('click', 'table#pmcSiteEngineersList a.updatePmcSiteEngineerBtn', function (e) {
        e.preventDefault();

        if ($('input[name="pmcSiteEngineersName"], select[name="pmcSiteEngineersDesignationList"]').valid()) {

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            if (pmcSiteEngineerEditing !== null && pmcSiteEngineerEditing === selectedRow) {
                /* A different row is being edited - the edit should be cancelled and this row edited */
                var checkExistence = checkProjectStep3PmcSiteEngineerExistence(pmcSiteEngineerTable, pmcSiteEngineerEditing);

                if (checkExistence === true) {
                    bootbox.alert('There is already a Pmc Site Engineer with this name & designation.');
                } else if (checkExistence === false) {
                    var pmcSiteEngineerUpdated = updateProjectStep3PmcSiteEngineer(pmcSiteEngineerTable.row(selectedRow).data()[1]);

                    if (pmcSiteEngineerUpdated === 1) {
                        bootbox.alert("Pmc Site Engineer is updated successfully.", function () {
                            if (pmcSiteEngineerEditing !== null && pmcSiteEngineerEditing == selectedRow) {
                                /* A different row is being edited - the edit should be cancelled and this row edited */
                                updatePmcSiteEngineerRow(pmcSiteEngineerTable, pmcSiteEngineerEditing);
                                pmcSiteEngineerEditing = null;
                                addPmcSiteEngineerBtn.enable();
                            }
                        });
                    } else if (pmcSiteEngineerUpdated === 0) {
                        bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
                    }

                }

            }

        }

    });

    $(document).on('click', 'table#pmcSiteEngineersList a.cancelPmcSiteEngineerEditBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (pmcSiteEngineerEditing !== null && pmcSiteEngineerEditing === selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restorePmcSiteEngineerRowData(pmcSiteEngineerTable, pmcSiteEngineerEditing);
            pmcSiteEngineerEditing = null;
            addPmcSiteEngineerBtn.enable();
        }

    });

    $(document).on('click', 'table#pmcSiteEngineersList a.deletePmcSiteEngineerBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        var pmcSiteEngineerDeleted = deleteProjectStep3PmcSiteEngineer(pmcSiteEngineerTable.row(selectedRow).data()[1]);

        if (pmcSiteEngineerDeleted === 1) {
            bootbox.alert("Pmc Site Engineer is deleted successfully.", function () {
                pmcSiteEngineerTable
                    .row(selectedRow)
                    .remove()
                    .draw();
            });
        } else if (pmcSiteEngineerDeleted === 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be deleted.<br>Please contact your database administrator.");
        }

    });

}

restorePmcSiteEngineerRowData = function (pmcSiteEngineerTable, previousRow) {
    var previousRowData = pmcSiteEngineerTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        removeNewPmcSiteEngineerRowCreatedOnCancel(pmcSiteEngineerTable, previousRow);
    } else {
        pmcSiteEngineerTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;

    }

};

removeNewPmcSiteEngineerRowCreatedOnCancel = function (pmcSiteEngineerTable, selectedRow) {

    pmcSiteEngineerTable
        .row(selectedRow)
        .remove()
        .draw();

    pmcSiteEngineerTable.on('order.dt search.dt', function () {
        pmcSiteEngineerTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

addNewPmcSiteEngineerRow = function (pmcSiteEngineerTable, selectedRow) {
    var selectedRowData = pmcSiteEngineerTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    pmcSiteEngineerTable.column('1:visible').order('asc').draw();

    pmcSiteEngineerTable.on('order.dt search.dt', function () {

        var x = pmcSiteEngineerTable.order();

        if (x[0][1] === 'asc') {
            var a = pmcSiteEngineerTable.column(0, { search: 'applied', order: 'applied' }).nodes();
            for (var i = 1; i < a.length; i++) {
                a[i].innerHTML = i;
            }
        } else {
            pmcSiteEngineerTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }
        availableTds[0].innerHTML = '*';
    }).draw(false);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.
    availableTds[0].innerHTML = '*';
    availableTds[1].innerHTML = '<input type="text" id="pmcSiteEngineersName" name="pmcSiteEngineersName" placeholder="Pmc Site Engineer Name" class="form-control" value="' + selectedRowData[2] + '" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)" style="width:100%;">';
    availableTds[2].innerHTML = '<select class="form-control select2" name="pmcSiteEngineersDesignationList" id="pmcSiteEngineersDesignationList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)"></select>';
    availableTds[3].innerHTML = '<a href="#" class="saveNewPmcSiteEngineerBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelNewPmcSiteEngineerEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';

    showDesignationOptions('pmcSiteEngineersDesignationList', designationsList);

    $('#pmcSiteEngineersDesignationList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Designation--',
        //placeholder: {
        //    id: '', // the value of the option
        //    //text: ''
        //},
        allowClear: true
    }).on('change', function (e) {
        $(this).valid();
    });

}

checkProjectStep3PmcSiteEngineerExistence = function (pmcSiteEngineerTable, selectedRow) {
    var retValue = false;

    var selectedRowData = pmcSiteEngineerTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableSelects = $('select', selectedRow);

    var addedPmcSiteEngineerNames = pmcSiteEngineerTable
        .column(2)
        .data();

    var addedPmcSiteEngineerDesignations = pmcSiteEngineerTable
        .column(3)
        .data();

    if ($.inArray($.trim(availableInputs[0].value), addedPmcSiteEngineerNames) !== -1 && $.inArray($.trim(availableSelects[0].selectedOptions[0].text), addedPmcSiteEngineerDesignations) !== -1) {
        retValue = true;
    }

    return retValue;
}

saveNewPmcSiteEngineerRow = function (pmcSiteEngineerTable, selectedRow, pmcSiteEngineerSavedGuid) {

    var selectedRowData = pmcSiteEngineerTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableSelects = $('select', selectedRow);

    selectedRowData[1] = pmcSiteEngineerSavedGuid;
    selectedRowData[2] = $.trim(availableInputs[0].value);
    selectedRowData[3] = $.trim(availableSelects[0].selectedOptions[0].text);
    selectedRowData[4] = '<a href="#" class="editPmcSiteEngineerBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deletePmcSiteEngineerBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    pmcSiteEngineerTable.row(selectedRow).data(selectedRowData);

    pmcSiteEngineerTable.on('order.dt search.dt', function () {
        pmcSiteEngineerTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

editPmcSiteEngineerRow = function (pmcSiteEngineerTable, selectedRow) {
    var selectedRowData = pmcSiteEngineerTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    availableTds[1].innerHTML = '<input type="text" id="pmcSiteEngineersName" name="pmcSiteEngineersName" placeholder="Pmc Site Engineer Name" class="form-control" value="' + selectedRowData[2] + '" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)" style="width:100%;">';
    availableTds[2].innerHTML = '<select class="form-control select2" name="pmcSiteEngineersDesignationList" id="pmcSiteEngineersDesignationList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)"></select>';
    availableTds[3].innerHTML = '<a href="#" class="updatePmcSiteEngineerBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelPmcSiteEngineerEditBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';

    showDesignationOptions('pmcSiteEngineersDesignationList', designationsList);

    $('#pmcSiteEngineersDesignationList option:contains("' + selectedRowData[3] + '")').prop('selected', true);

    $('#pmcSiteEngineersDesignationList').select2({
        theme: 'bootstrap',
        placeholder: '--Select Designation--',
        //placeholder: {
        //    id: '', // the value of the option
        //    //text: ''
        //},
        allowClear: true
    }).on('change', function (e) {
        $(this).valid();
    });
}

updatePmcSiteEngineerRow = function (pmcSiteEngineerTable, selectedRow) {

    var selectedRowData = pmcSiteEngineerTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableSelects = $('select', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[2] = $.trim(availableInputs[0].value);
    selectedRowData[3] = $.trim(availableSelects[0].selectedOptions[0].text);
    selectedRowData[4] = '<a href="#" class="editPmcSiteEngineerBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deletePmcSiteEngineerBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    pmcSiteEngineerTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

}

saveProjectStep3PmcSiteEngineer = function () {

    var retValue = jQuery.Guid.Empty();

    var projectAndPmcSiteEngineersRelationManager = {
        projectManager: {
            projectGuid: projectGuid
        },
        siteEngineerName: $.trim($('table#pmcSiteEngineersList input#pmcSiteEngineersName').val()) + ', ' + $.trim($('table#pmcSiteEngineersList select#pmcSiteEngineersDesignationList option:selected').text())
    };

    var projectAndPmcSiteEngineersRelationManagerJsonString = JSON.stringify(projectAndPmcSiteEngineersRelationManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "AddDBAProject.aspx/SaveProjectStep3PmcSiteEngineer",
        data: '{"projectAndPmcSiteEngineersRelationManager":' + projectAndPmcSiteEngineersRelationManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updateProjectStep3PmcSiteEngineer = function (pmcSiteEngineerProjectRelationshipGuid) {

    var retValue = 0;

    var projectAndPmcSiteEngineersRelationManager = {
        pmcSiteEngineerProjectRelationshipGuid: pmcSiteEngineerProjectRelationshipGuid,
        projectManager: {
            projectGuid: projectGuid
        },
        siteEngineerName: $.trim($('table#pmcSiteEngineersList input#pmcSiteEngineersName').val()) + ', ' + $.trim($('table#pmcSiteEngineersList select#pmcSiteEngineersDesignationList option:selected').text())
    };

    var projectAndPmcSiteEngineersRelationManagerJsonString = JSON.stringify(projectAndPmcSiteEngineersRelationManager);

    $.ajax({
        type: "Post",
        async: false,
        url: "AddDBAProject.aspx/UpdateProjectStep3PmcSiteEngineer",
        data: '{"projectAndPmcSiteEngineersRelationManager":' + projectAndPmcSiteEngineersRelationManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteProjectStep3PmcSiteEngineer = function (pmcSiteEngineerProjectRelationshipGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "AddDBAProject.aspx/DeleteProjectStep3PmcSiteEngineer",
        data: '{"projectGuid":"' + projectGuid + '","pmcSiteEngineerProjectRelationshipGuid":"' + pmcSiteEngineerProjectRelationshipGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}





















displayEngagedOfficersList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    var html = '<ul class="list-group">';

    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += (index + 1) + '.  ';
        html += arr[index].fremaaOfficerName + '  ';
        html += '<button type="button" class="btn btn-primary" onclick="removeOfficersEngaged(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    div.append(html);

    if (arr.length == 0) {
        div.addClass('hidden');
    }

}

removeOfficersEngaged = function (index) {

    arrOfficersEngaged.splice(index, 1);

    displayEngagedOfficersList('officersListDisplay', arrOfficersEngaged);

}

displayPmcSiteEngineersList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    var html = '<ul class="list-group">';

    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += (index + 1) + '.  ';
        html += arr[index].siteEngineerName + '  ';
        html += '<button type="button" class="btn btn-primary" onclick="removePmcSiteEngineers(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    div.append(html);

    if (arr.length == 0) {
        div.addClass('hidden');
    }

}

removePmcSiteEngineers = function (index) {

    arrPmcSiteEngineers.splice(index, 1);

    displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);

}








callOtherFuctions = function () {
    $('.popupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            headName: "required",
            headBudget: {
                required: true,
                currencySmallerThan: [name = 'remainingBudgetLbl']
            }
        },
    });

}

createHeadTable = function () {
    $('#headTableDiv').empty();

    var projectValue = $("#projectValue").val();
    var newchar = '';
    projectValue = projectValue.split(',').join(newchar);
    //subChainageValue = subChainageValue.replace(/,/g , newchar);
    projectValue = parseFloat(projectValue);

    var totalAssignedValue = 0;

    if (arrHeadBudgets.length > 0) {
        for (var i = 0; i < arrHeadBudgets.length; i++) {
            var a = arrHeadBudgets[i].headBudget;
            totalAssignedValue = totalAssignedValue + a;
        }
    }

    if (arrHeadBudgets.length > 0) {
        var html1 = "<table class=\"table table-striped\">";

        html1 += "<thead>";
        html1 += "<tr>";
        html1 += "<th>";
        html1 += "Head Name";
        html1 += "</th>";
        html1 += "<th>";
        html1 += "Head Budget";
        html1 += "</th>";
        html1 += "<th>";
        html1 += "";
        html1 += "</th>";
        html1 += "</tr>";
        html1 += "</thead>";

        html1 += "<tfoot>";
        html1 += "<tr>";
        html1 += "<th>";
        html1 += "Total Value Assigned to Heads";
        html1 += "</th>";
        html1 += "<th>";
        html1 += accounting.formatMoney(totalAssignedValue, { symbol: "Rs.", format: "%s %v" });
        html1 += "</th>";
        html1 += "<th>";
        html1 += "";
        html1 += "</th>";
        html1 += "</tr>";
        html1 += "</tfoot>";

        html1 += "<tbody>";

        for (var i = 0; i < arrHeadBudgets.length; i++) {
            html1 += "<tr>";

            html1 += "<th>";
            html1 += arrHeadBudgets[i].headName;
            html1 += "</th>";

            html1 += "<th>";
            html1 += accounting.formatMoney(arrHeadBudgets[i].headBudget, { symbol: "Rs.", format: "%s %v" });
            html1 += "</th>";

            html1 += "<th>";
            html1 += '<button type="button" class="btn btn-primary" onclick="removeHeadTable(' + i + ');">Remove</button>';;
            html1 += "</th>";

            html1 += "</tr>";
        }


        html1 += "</tbody>";

        html1 += "</table>";

        $('#headTableDiv').append(html1);
    }
}

removeHeadTable = function (index) {
    //alert("aditya");
    //alert(index);
    arrHeadBudgets.splice(index, 1);

    createHeadTable();

}











//Save Step 3 Details
saveStep3 = function () {
    var arrPmcSiteEngineersJsonString = JSON.stringify(arrPmcSiteEngineers);

    //$("#preloader").show();
    //$("#status").show();
    var str;
    $.ajax({
        type: "Post",
        async: false,
        url: "CreateNewProject.aspx/SaveProjectStep3",
        data: '{"projectGuid":"' + projectGuid + '","arrPmcSiteEngineers":' + arrPmcSiteEngineersJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            str = response.d;
            //if (str == true) {
            //    bootbox.alert("Step 1 Saved.");
            //}
            //else {
            //    bootbox.alert("Data cannot be saved.");
            //}
            //str = str1;
        },
        failure: function (msg) {
            alert(msg);
            //$('#status').delay(300).fadeOut(); // will first fade out the loading animation
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
    return str;
}

saveStep4 = function () {
    var arrSubChainagesJsonString = JSON.stringify(arrSubChainages);
    var retValue = false;
    $.ajax({
        type: "Post",
        async: false,
        url: "CreateNewProject.aspx/SaveProjectStep4",
        data: '{"projectGuid":"' + projectGuid + '","arrSubChainages":' + arrSubChainagesJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');

            //alert("Success");
            retValue = response.d;
            //var str = response.d;
            //if (str == true) {
            //    bootbox.confirm({
            //        title: '!!Attention!!',
            //        message: 'There is already a person with this designation in this division in our database. To change click Change otherwise click Exit.',
            //        buttons: {
            //            'cancel': {
            //                label: 'Change',
            //                className: 'btn-default pull-left'
            //            },
            //            'confirm': {
            //                label: 'Exit',
            //                className: 'btn-danger pull-right'
            //            }
            //        },
            //        callback: function (result) {
            //            if (result) {
            //                //window.location = $("a[data-bb='confirm']").attr('href');
            //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
            //                //window.location.href = nextUrl;
            //                window.location = 'Default.aspx';
            //            }
            //        }
            //    });
            //}
            //ret = str;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your administrator.");
        }
    });

    return retValue;
}

saveStep5 = function () {
    var includeLandAcquisition = $('input[name=landAcquisitionRadio]:checked', '#createNewProjectForm').val();
    var retValue = false;

    if (includeLandAcquisition == "yes") {
        //includeLandAcquisition = "true";

        $.ajax({
            type: "Post",
            async: false,
            url: "CreateNewProject.aspx/SaveProjectStep5",
            data: '{"projectGuid":"' + projectGuid + '","includeLandAcquisition":"true"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //$('#status').delay(300).fadeOut();
                //$('#preloader').delay(350).fadeOut('slow');

                //alert("Success");
                retValue = response.d;
                //var str = response.d;
                //if (str == true) {
                //    bootbox.confirm({
                //        title: '!!Attention!!',
                //        message: 'There is already a person with this designation in this division in our database. To change click Change otherwise click Exit.',
                //        buttons: {
                //            'cancel': {
                //                label: 'Change',
                //                className: 'btn-default pull-left'
                //            },
                //            'confirm': {
                //                label: 'Exit',
                //                className: 'btn-danger pull-right'
                //            }
                //        },
                //        callback: function (result) {
                //            if (result) {
                //                //window.location = $("a[data-bb='confirm']").attr('href');
                //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                //                //window.location.href = nextUrl;
                //                window.location = 'Default.aspx';
                //            }
                //        }
                //    });
                //}
                //ret = str;
            },
            failure: function (msg) {
                //$('#status').delay(300).fadeOut();
                //$('#preloader').delay(350).fadeOut('slow');
                //bootbox.alert("Please contact your administrator.");
            }
        });


    } else {
        retValue = true;
    }

    return retValue;
}

saveStep6 = function () {
    var arrHeadBudgetsJsonString = JSON.stringify(arrHeadBudgets);
    var retValue = false;
    $.ajax({
        type: "Post",
        async: false,
        url: "CreateNewProject.aspx/SaveProjectStep6",
        data: '{"projectGuid":"' + projectGuid + '","arrHeadBudgets":' + arrHeadBudgetsJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');

            //alert("Success");
            retValue = response.d;
            //var str = response.d;
            //if (str == true) {
            //    bootbox.confirm({
            //        title: '!!Attention!!',
            //        message: 'There is already a person with this designation in this division in our database. To change click Change otherwise click Exit.',
            //        buttons: {
            //            'cancel': {
            //                label: 'Change',
            //                className: 'btn-default pull-left'
            //            },
            //            'confirm': {
            //                label: 'Exit',
            //                className: 'btn-danger pull-right'
            //            }
            //        },
            //        callback: function (result) {
            //            if (result) {
            //                //window.location = $("a[data-bb='confirm']").attr('href');
            //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
            //                //window.location.href = nextUrl;
            //                window.location = 'Default.aspx';
            //            }
            //        }
            //    });
            //}
            //ret = str;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your administrator.");
        }
    });

    return retValue;
}

saveStep7 = function () {
    var retValue = false;
    $.ajax({
        type: "Post",
        async: false,
        url: "CreateNewProject.aspx/SaveProjectStep7",
        data: '{"projectGuid":"' + projectGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');

            //alert("Success");
            retValue = response.d;
            //var str = response.d;
            //if (str == true) {
            //    bootbox.confirm({
            //        title: '!!Attention!!',
            //        message: 'There is already a person with this designation in this division in our database. To change click Change otherwise click Exit.',
            //        buttons: {
            //            'cancel': {
            //                label: 'Change',
            //                className: 'btn-default pull-left'
            //            },
            //            'confirm': {
            //                label: 'Exit',
            //                className: 'btn-danger pull-right'
            //            }
            //        },
            //        callback: function (result) {
            //            if (result) {
            //                //window.location = $("a[data-bb='confirm']").attr('href');
            //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
            //                //window.location.href = nextUrl;
            //                window.location = 'Default.aspx';
            //            }
            //        }
            //    });
            //}
            //ret = str;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            //bootbox.alert("Please contact your administrator.");
        }
    });

    return retValue;
}






removeOfficersEngaged = function (index) {
    //alert("aditya");
    //alert(index);
    arrOfficersEngaged.splice(index, 1);

    displayEngagedOfficersList('officersListDisplay', arrOfficersEngaged);

}

displayPmcSiteEngineersList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    //var newDiv = document.createElement('div');
    //newDiv.className = "panel panel-default";

    //var html = '<div class="panel-heading">Panel heading</div>';
    //html += '<div class="panel-body">';

    var html = '<ul class = "list-group">';
    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += arr[index];
        html += '<button type="button" class="btn btn-primary" onclick="removePmcSiteEngineers(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    //html += '</div>';

    //newDiv.innerHTML = html;

    //document.getElementById(divname).appendChild(newDiv);

    document.getElementById(divname).innerHTML = html;

    if (arr.length == 0) {
        div.addClass('hidden');
    }
}

removePmcSiteEngineers = function (index) {
    //alert("aditya");
    //alert(index);
    arrPmcSiteEngineers.splice(index, 1);

    displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);

}

displaySubChainagesList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    //var newDiv = document.createElement('div');
    //newDiv.className = "panel panel-default";

    //var html = '<div class="panel-heading">Panel heading</div>';
    //html += '<div class="panel-body">';

    var html = '<ul class = "list-group">';
    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += "Sub Chainage: " + arr[index].subChainageName + " ";
        //alert(arr[index].subChainageName + " " + arr[index].subChainageSioName + " " + arr[index].subChainageProjectValue + " " + arr[index].aaRef + " " + arr[index].tsRef + " " + arr[index].contractorName + " " + arr[index].workOrderRef + " " + arr[index].subChainageStartingDate + " " + arr[index].subChainageCompletionDate + " " + arr[index].typeOfWorks);
        html += '<button type="button" class="btn btn-primary" onclick="removeSubChainages(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    //html += '</div>';

    //newDiv.innerHTML = html;

    //document.getElementById(divname).appendChild(newDiv);

    document.getElementById(divname).innerHTML = html;

    if (arr.length == 0) {
        div.addClass('hidden');
    }
}

removeSubChainages = function (index) {
    //alert("aditya");
    //alert(index);
    arrSubChainages.splice(index, 1);
    displaySubChainagesList('subChainagesListDisplay', arrSubChainages);
}

displayWorkTypeItemsList = function (divname, arr) {

    var div = $('#' + divname);
    div.empty();

    div.removeClass('hidden');

    //var newDiv = document.createElement('div');
    //newDiv.className = "panel panel-default";

    //var html = '<div class="panel-heading">Panel heading</div>';
    //html += '<div class="panel-body">';

    var html = '<ul class = "list-group">';
    for (var index = 0; index < arr.length; index++) {
        html += '<li class="list-group-item">';
        html += arr[index] + " ";
        html += '<button type="button" class="btn btn-primary" onclick="removeWorkTypeItems(' + index + ');">Remove</button>';
        html += '</li>';
    }

    html += '</ul>';

    //html += '</div>';

    //newDiv.innerHTML = html;

    //document.getElementById(divname).appendChild(newDiv);

    document.getElementById(divname).innerHTML = html;

    if (arr.length == 0) {
        div.addClass('hidden');
    }
}

removeWorkTypeItems = function (index) {
    //alert("aditya");
    //alert(index);
    arrWorkTypeItems.splice(index, 1);

    displayWorkTypeItemsList('workTypeItemsListDisplay', arrWorkTypeItems);

}