﻿var subChainagePeriod = []

document.onkeydown = function (e) {
    stopDefaultBackspaceBehaviour(e);
}

document.onkeypress = function (e) {
    stopDefaultBackspaceBehaviour(e);
}

function stopDefaultBackspaceBehaviour(event) {
    var event = event || window.event;
    if (event.keyCode == 8) {
        var elements = "HTML, BODY, TABLE, TBODY, TR, TD, DIV";
        var d = event.srcElement || event.target;
        var regex = new RegExp(d.tagName.toUpperCase());
        if (d.contentEditable != 'true') { //it's not REALLY true, checking the boolean value (!== true) always passes, so we can use != 'true' rather than !== true/
            if (regex.test(elements)) {
                event.preventDefault ? event.preventDefault() : event.returnValue = false;
            }
        }
    }
}

$(document).ready(function () {

    getSubChainagePeriod();

    //var fullDate = new Date();

    //var twoDigitMonth = fullDate.getMonth() + 1 + "";
    //if (twoDigitMonth.length == 1) {
    //    twoDigitMonth = "0" + twoDigitMonth;
    //}

    //var twoDigitDate = fullDate.getDate() + "";
    //if (twoDigitDate.length == 1) {
    //    twoDigitDate = "0" + twoDigitDate;
    //}

    //var currentDate = twoDigitDate + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

    var startDateFull = Date.parse(subChainagePeriod[0]);
    var endDateFull = Date.parse(subChainagePeriod[1]);

    $('#dpProgressMonth').datepicker({
        //format: "MM, yyyy",
        format: {
            toDisplay: function (date, format, language) {
                var d = new Date(date);
                return d.toString('MMMM, yyyy');
            },
            toValue: function (date, format, language) {
                var d = new Date(date);
                return new Date(d);
            }
        },
        startView: 1,
        minViewMode: "months",
        startDate: startDateFull,//startDateFull.toString('MMMM') + ", " + startDateFull.toString('yyyy'),
        endDate: endDateFull//endDateFull.toString('MMMM') + ", " + endDateFull.toString('yyyy')
    }).on('changeMonth', function (e) {
        var dp = $(e.currentTarget).data('datepicker');
        dp.date = e.date;
        dp.setValue();
        dp.hide();
    });

    $('#aaaaa').on('click', function (e) {
        e.preventDefault();

        alert($('#dpProgressMonth').datepicker('getDate'));
    });

    //$('#dpProgressYear').datepicker({
    //    format: "yyyy",
    //    startView: 2,//2-years
    //    minViewMode: "years",
    //    orientation: "auto",
    //    startDate: startDateFull.toString('yyyy'),
    //    endDate: endDateFull.toString('yyyy')
    //}).on('changeYear', function (e) {
    //    var dp = $(e.currentTarget).data('datepicker');
    //    dp.date = e.date;
    //    dp.setValue();
    //    dp.hide();

    //    var startMonthDate;
    //    var endMonthDate;


        //if (e.date.toString('yyyy') == startDateFull.toString('yyyy')) {
        //    startMonthDate = Date.parse(startDateFull.toString('dd') + "/" + startDateFull.toString('MMM') + "/" + e.date.toString('yyyy'));
        //} else if (e.date.toString('yyyy') > startDateFull.toString('yyyy')) {
        //    startMonthDate = Date.parse('01' + "/" + '01' + "/" + e.date.toString('yyyy'));
        //} else {

        //}

        
    //    if (e.date.toString('yyyy') == endDateFull.toString('yyyy')) {
    //        endMonthDate = Date.parse(endDateFull.toString('dd') + "/" + endDateFull.toString('MMM') + "/" + e.date.toString('yyyy'));
    //    } else if (e.date.toString('yyyy') < endDateFull.toString('yyyy')) {
    //        endMonthDate = Date.parse('31' + "/" + '12' + "/" + e.date.toString('yyyy'));
    //    } else {

    //    }

        
    //    $('#dpProgressMonth').datepicker('setStartDate', startMonthDate);

    //    $('#dpProgressMonth').datepicker('setEndDate', endMonthDate);

    //    //$('#dpProgressMonth').datepicker('setDate', startMonthDate);

    //    $('#dpProgressMonth').datepicker('update', startMonthDate);

    //    //$('#dpProgressMonth').val('');
    //});

    //$('#dpProgressMonth').datepicker({
    //    format: {
    //        toDisplay: function (date, format, language) {
    //            var d = new Date(date);
    //            return d.toString('MMMM');
    //        },
    //        toValue: function (date, format, language) {
    //            var d = new Date(date);
    //            return new Date(d);
    //        }
    //    },
    //    startView: 1,//1-months
    //    minViewMode: "months",
    //    orientation: "auto"
    //    //endDate: '31/12/2018'
    //}).on('changeMonth', function (e) {
    //    var dp = $(e.currentTarget).data('datepicker');
    //    dp.date = e.date;
    //    dp.setValue();
    //    dp.hide();
    //});

    //$('#dpProgressYear').datepicker({
    //    format: "yyyy",
    //    viewMode: "years",
    //    minViewMode: "years",
    //    startDate: currentDate.toString(),
    //    clearBtn: true,
    //    autoclose: true,
    //    todayHighlight: true
    //}).on('changeDate', function (ev) {
    //    var a = new Date(ev.date.valueOf());
    //    alert(a);
    //    a = a.addMonths(-1);
    //    //a = a.add(-1).months();
    //    alert(a);

    //    var b = a.addMonths(11);
    //    alert(b);
    //    //b = b.add(1).months();
    //    //alert(b);

    //    //alert(a.addMonths(11));
    //    $('#dpProgressMonth').datepicker('setStartDate', a);
    //    $('#dpProgressMonth').datepicker('setEndDate', b);
    //    $(this).blur();
    //    $(this).datepicker('hide');
    ////}).on('clearDate', function (selected) {
    ////    $('#dpDateOfStartOfProject').datepicker('setEndDate', null);
    //});



    //$('#dpProgressMonth').datepicker({
    //    format: "MM",
    //    //format: {
    //    //    toDisplay: function (date, format, language) {
    //    //        var d = new Date(date);
    //    //        return d.toString('MMMM');
    //    //    },
    //    //    toValue: function (date, format, language) {
    //    //        var d = new Date(date);
    //    //        return d.toString('MM'); //new Date(d);
    //    //    }
    //    //},
    //    viewMode: "months",
    //    minViewMode: "months",
    //    //startDate: currentDate.toString(),
    //    clearBtn: true,
    //    autoclose: true,
    //    //todayHighlight: true
    //    //}).on('changeDate', function (ev) {
    //    //    var projectStartDateEndDate = new Date(ev.date.valueOf());
    //    //    projectStartDateEndDate = projectStartDateEndDate.add(-1).days();
    //    //    $('#dpDateOfStartOfProject').datepicker('setEndDate', projectStartDateEndDate);
    //    //    $(this).blur();
    //    //    $(this).datepicker('hide');
    //    //}).on('clearDate', function (selected) {
    //    //    $('#dpDateOfStartOfProject').datepicker('setEndDate', null);
    //});


    $('.form').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            progressMonth: {
                required: true,
                dateInSpecifiedFormat: "MMMM, yyyy"//[name = 'remainingBudgetLbl']
            }
        },
        messages: {
            progressMonth: {
                required: "Please Enter the Month For Progress.",
                dateITA: "Please Enter the Month For Progress in the specified format."
            }
        },
        errorPlacement: function (error, element) {

            switch (element.attr("name")) {
                case "dpProgressMonth":
                    error.insertAfter($("#dpDateOfStartOfProject"));
                    break;

                default:
                    //nothing
            }
        }
    });

    var y = document.getElementsByClassName("fremaaOfficers");

    for (var a = 0; a < y.length; a++) {
        var x = y[a].getElementsByTagName("input")[0].getAttribute("name");

        $('input[name="' + x + '"]').rules('add', {
            required: true
        });
    }

    //Submit button click action
    $('#btnSubmitProgress').click(function () {
        //$("#preloader").show();
        //$("#status").show();

        if ($(".form").valid()) {
            //widget.show();
            //widget.not(':eq(' + (current++) + ')').hide();
            //setProgress(current);

            $(function () {
                var check = saveProjectPhysicalProgress();
                if (check == true) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your data is saved successfully.", function () {
                        //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                        //window.location.href = nextUrl;
                        window.location = 'DBAProjects.aspx';
                    });
                }
                else if (check == false) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your data cannot be saved.\nPlease contact your administrator.", function () {
                        //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                        //window.location.href = nextUrl;
                        window.location = 'DBAProjects.aspx';
                    });
                }
            });
        }
        else {
            alert("Not Validated");
        }

        
    });

   
    






















    

    //$('#projectValue').maskMoney();
    //$('#subChainageValue').maskMoney();

    //widget = $(".step");

    //btnNext = $(".next");
    //btnBack = $(".back");
    //btnSkip = $(".skip");

    //btnSubmit = $(".submit");

    ////Init Buttons and UI
    //widget.not(':eq(0)').hide();
    //hideButtons(current);
    //setProgress(current);

    // Next button click action
    //btnNext.click(function () {
    //    if (current < widget.length) {

    //        //Step 1
    //        if (current == 1) {
    //            // Check validation
                //if ($(".form").valid()) {
                //    widget.show();
                //    widget.not(':eq(' + (current++) + ')').hide();
                //    setProgress(current);
                //}
    //        }

    //            //Step 2
    //        else if (current == 2) {
    //            if (arrOfficersEngaged.length > 0) {
    //                // Check validation
    //                //if ($(".form").valid()) {
    //                widget.show();
    //                widget.not(':eq(' + (current++) + ')').hide();
    //                setProgress(current);
    //                //}
    //            } else {
    //                bootbox.alert("Officers List cannot be Empty.");
    //            }
    //        }

    //            //Step 3
    //        else if (current == 3) {
    //            if (arrPmcSiteEngineers.length > 0) {
    //                getWorkItems();
    //                // Check validation
    //                //if ($(".form").valid()) {
    //                widget.show();
    //                widget.not(':eq(' + (current++) + ')').hide();
    //                setProgress(current);
    //                //}
    //            } else {
    //                bootbox.alert("Site Engineers List cannot be Empty.");
    //            }
    //        }

    //            //Step 4
    //        else if (current == 4) {
    //            if (arrSubChainages.length > 0) {
    //                getFinancialSubHeads();
    //                // Check validation
    //                //if ($(".form").valid()) {
    //                widget.show();
    //                widget.not(':eq(' + (current++) + ')').hide();
    //                setProgress(current);
    //                //}
    //            } else {
    //                bootbox.alert("Sub Chainages List cannot be Empty.");
    //            }
    //        }

    //            //Step 5
    //        else if (current == 5) {
    //            // Check validation
    //            if ($(".form").valid()) {
    //                widget.show();
    //                widget.not(':eq(' + (current++) + ')').hide();
    //                setProgress(current);
    //            }
    //        }

    //            //Step 6
    //        else if (current == 6) {
    //            if (arrFinancialHeadBudgets.length > 0) {
    //                // Check validation
    //                //if ($(".form").valid()) {
    //                widget.show();
    //                widget.not(':eq(' + (current++) + ')').hide();
    //                setProgress(current);
    //                //}
    //            } else {
    //                bootbox.alert("Financial Head Budgets List cannot be Empty.");
    //            }
    //        }

    //    }

    //    hideButtons(current);
    //});

    // Back button click action
    //btnBack.click(function () {
    //    if (current > 1) {
    //        current = current - 2;
    //        if (current < widget.length) {
    //            widget.show();
    //            widget.not(':eq(' + (current++) + ')').hide();
    //            setProgress(current);
    //        }
    //    }
    //    hideButtons(current);
    //});

    // Next button click action
    //btnSkip.click(function () {
    //    if (current < widget.length) {

    //        //Step 2
    //        if (current == 2) {
    //            while (arrOfficersEngaged.length > 0) {
    //                arrOfficersEngaged.pop();
    //            }

    //            displayEngagedOfficersList('officersListDisplay', arrOfficersEngaged);

    //            widget.show();
    //            widget.not(':eq(' + (current++) + ')').hide();
    //            setProgress(current);
    //        }

    //            //Step 3
    //        else if (current == 3) {
    //            if (arrPmcSiteEngineers.length > 0) {
    //                arrPmcSiteEngineers.pop();
    //            }

    //            displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);

    //            getWorkItems();

    //            widget.show();
    //            widget.not(':eq(' + (current++) + ')').hide();
    //            setProgress(current);
    //        }

    //            //Step 4
    //        else if (current == 4) {
    //            if (arrSubChainages.length > 0) {
    //                arrSubChainages.pop();
    //            }

    //            widget.show();
    //            widget.not(':eq(' + (current++) + ')').hide();
    //            setProgress(current);
    //        }

    //    }

    //    hideButtons(current);
    //});

    // Submit button click action
    //btnSubmit.click(function () {
    //    $("#preloader").show();
    //    $("#status").show();

    //    $(function () {
    //        var check = saveProject();
    //        if (check == true) {
    //            $('#status').delay(300).fadeOut();
    //            $('#preloader').delay(350).fadeOut('slow');

    //            bootbox.alert("Your data is saved successfully.", function () {
    //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
    //                //window.location.href = nextUrl;
    //                window.location = 'DBAProjects.aspx';
    //            });
    //        }
    //        else if (check == false) {
    //            $('#status').delay(300).fadeOut();
    //            $('#preloader').delay(350).fadeOut('slow');

    //            bootbox.alert("Your data cannot be saved.\nPlease contact your administrator.", function () {
    //                //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
    //                //window.location.href = nextUrl;
    //                window.location = 'DBAProjects.aspx';
    //            });
    //        }
    //    });
    //});

    //$('.form').validate({ // initialize plugin
    //    ignore: ":not(:visible)",
    //    rules: {
    //        districtList: "required",
    //        divisionList: "required",
    //        projectName: {
    //            required: true,
    //            noSpace: true
    //        },
    //        subProjectName: {
    //            required: true,
    //            noSpace: true
    //        },
    //        chainageOfProject: {
    //            required: true,
    //            noSpace: true
    //        },
    //        dateOfStartOfProject: {
    //            required: true,
    //            dateITA: true
    //        },
    //        expectedDateOfCompletionOfProject: {
    //            required: true,
    //            dateITA: true
    //        },
    //        projectValue: "required",

    //        officersEngaged: {
    //            required: true,
    //            noSpace: true
    //        },

    //        pmcSiteEngineers: {
    //            required: true,
    //            noSpace: true
    //        },

    //        subChainageName: {
    //            required: true,
    //            noSpace: true
    //        },
    //        sioUsersList: "required",
    //        subChainageValue: "required",
    //        administrativeApprovalReference: {
    //            required: true,
    //            noSpace: true
    //        },
    //        technicalSanctionReference: {
    //            required: true,
    //            noSpace: true
    //        },
    //        contractorName: {
    //            required: true,
    //            noSpace: true
    //        },
    //        workOrderReference: {
    //            required: true,
    //            noSpace: true
    //        },
    //        subChainageStartingDate: {
    //            required: true,
    //            dateITA: true
    //        },
    //        subChainageExpectedCompletionDate: {
    //            required: true,
    //            dateITA: true
    //        },
    //        typeOfWorks: {
    //            required: true,
    //            noSpace: true
    //        },
    //        //workTypeItemList: "required",

    //        landAcquisitionRadio: "required"
    //    },
    //    messages: {
    //        districtList: {
    //            required: "Please Enter Project District."
    //        },
    //        divisionList: {
    //            required: "Please Enter Project Division."
    //        },
    //        projectName: {
    //            required: "Please Enter Project Name.",
    //            noSpace: "Project Name cannot be blank."
    //        },
    //        subProjectName: {
    //            required: "Please Enter Sub-Project Name.",
    //            noSpace: "Sub-Project Name cannot be blank."
    //        },
    //        chainageOfProject: {
    //            required: "Please Enter Chainage of the Project.",
    //            noSpace: "Chainage of the Project cannot be blank."
    //        },
    //        dateOfStartOfProject: {
    //            required: "Please Enter the Starting Date of the Project.",
    //            dateITA: "Please Enter the Starting Date of the Project as: dd/mm/yyyy"
    //        },
    //        expectedDateOfCompletionOfProject: {
    //            required: "Please Enter the Expected Completion Date of the Project.",
    //            dateITA: "Please Enter the Expected Completion Date of the Project as: dd/mm/yyyy"
    //        },
    //        projectValue: {
    //            required: "Please Enter the Value of the Project."
    //        },

    //        officersEngaged: {
    //            required: "Please Enter the Officer Name.",
    //            noSpace: "Officer Name cannot be blank."
    //        },

    //        pmcSiteEngineers: {
    //            required: "Please Enter the Site Engineer Name.",
    //            noSpace: "Site Engineer Name cannot be blank."
    //        },

    //        subChainageName: {
    //            required: "Please Enter the Name of the Sub-Chainage.",
    //            noSpace: "Sub-Chainage Name cannot be blank."
    //        },
    //        sioUsersList: {
    //            required: "Please Enter the SIO for the Sub-Chainage."
    //        },
    //        subChainageValue: {
    //            required: "Please Enter the Value of the Sub-Chainage."
    //        },
    //        administrativeApprovalReference: {
    //            required: "Please Enter the AA Reference of the Sub-Chainage.",
    //            noSpace: "AA Reference cannot be blank."
    //        },
    //        technicalSanctionReference: {
    //            required: "Please Enter the TS Reference of the Sub-Chainage.",
    //            noSpace: "TS Reference cannot be blank."
    //        },
    //        contractorName: {
    //            required: "Please Enter the Name of the Contractor of the Sub-Chainage.",
    //            noSpace: "Contractor Name cannot be blank."
    //        },
    //        workOrderReference: {
    //            required: "Please Enter the W.O. Reference of the Sub-Chainage.",
    //            noSpace: "W.O. Reference cannot be blank."
    //        },
    //        subChainageStartingDate: {
    //            required: "Please Enter the Starting Date of the Sub-Chainage.",
    //            dateITA: "Please Enter the Starting Date of the Sub-Chainage as: dd/mm/yyyy"
    //        },
    //        subChainageExpectedCompletionDate: {
    //            required: "Please Enter the Expected Completion Date of the Sub-Chainage.",
    //            dateITA: "Please Enter the Expected Completion Date of the Sub-Chainage as: dd/mm/yyyy"
    //        },
    //        typeOfWorks: {
    //            required: "Please Enter the Type of Work.",
    //            noSpace: "Type of Work cannot be blank."
    //        },
    //        //workTypeItemList: {
    //        //    required: "Please Enter Work Type Item."
    //        //},

    //        landAcquisitionRadio: "Please Select the Land Acquisition Option."
    //    },
    //    errorPlacement: function (error, element) {

    //        switch (element.attr("name")) {
    //            case "districtList":
    //                error.insertAfter($("#districtList"));
    //                break;
    //            case "divisionList":
    //                error.insertAfter($("#divisionList"));
    //                break;
    //            case "projectName":
    //                error.insertAfter($("#projectName"));
    //                break;
    //            case "subProjectName":
    //                error.insertAfter($("#subProjectName"));
    //                break;
    //            case "chainageOfProject":
    //                error.insertAfter($("#chainageOfProject"));
    //                break;
    //            case "dateOfStartOfProject":
    //                error.insertAfter($("#dpDateOfStartOfProject"));
    //                break;
    //            case "expectedDateOfCompletionOfProject":
    //                error.insertAfter($("#dpExpectedDateOfCompletionOfProject"));
    //                break;
    //            case "projectValue":
    //                error.insertAfter($("#projectValueInputGroup"));
    //                break;

    //            case "officersEngaged":
    //                error.insertAfter($("#officersEngaged"));
    //                break;

    //            case "pmcSiteEngineers":
    //                error.insertAfter($("#pmcSiteEngineers"));
    //                break;

    //            case "subChainageName":
    //                error.insertAfter($("#subChainageName"));
    //                break;
    //            case "sioUsersList":
    //                error.insertAfter($("#sioUsersList"));
    //                break;
    //            case "subChainageValue":
    //                error.insertAfter($("#subChainageValueInputGroup"));
    //                break;
    //            case "administrativeApprovalReference":
    //                error.insertAfter($("#administrativeApprovalReference"));
    //                break;
    //            case "technicalSanctionReference":
    //                error.insertAfter($("#technicalSanctionReference"));
    //                break;
    //            case "contractorName":
    //                error.insertAfter($("#contractorName"));
    //                break;
    //            case "workOrderReference":
    //                error.insertAfter($("#workOrderReference"));
    //                break;
    //            case "subChainageStartingDate":
    //                error.insertAfter($("#dpSubChainageStartingDate"));
    //                break;
    //            case "subChainageExpectedCompletionDate":
    //                error.insertAfter($("#dpSubChainageExpectedCompletionDate"));
    //                break;
    //            case "typeOfWorks":
    //                error.insertAfter($("#typeOfWorks"));
    //                break;
    //                //case "workTypeItemList":
    //                //    error.insertAfter($("#workTypeItemList"));
    //                //    break;

    //            case "landAcquisitionRadio":
    //                error.insertAfter($("landAcquisitionRadio"));
    //                break;

    //            default:
    //                //nothing
    //        }
    //    }
    //});

    //var fullDate = new Date();

    //var twoDigitMonth = fullDate.getMonth() + 1 + "";
    //if (twoDigitMonth.length == 1) {
    //    twoDigitMonth = "0" + twoDigitMonth;
    //}

    //var twoDigitDate = fullDate.getDate() + "";
    //if (twoDigitDate.length == 1) {
    //    twoDigitDate = "0" + twoDigitDate;
    //}

    //var currentDate = twoDigitDate + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

    //$('#dpDateOfStartOfProject').datepicker({
    //    format: "dd/mm/yyyy",
    //    startDate: currentDate.toString(),
    //    clearBtn: true,
    //    autoclose: true,
    //    todayHighlight: true
    //}).on('changeDate', function (ev) {
    //    var projectExpectedCompletionDateStartDate = new Date(ev.date.valueOf());
    //    projectExpectedCompletionDateStartDate = projectExpectedCompletionDateStartDate.add(1).days();
    //    $('#dpExpectedDateOfCompletionOfProject').datepicker('setStartDate', projectExpectedCompletionDateStartDate);
    //    $(this).blur();
    //    $(this).datepicker('hide');
    //}).on('clearDate', function (selected) {
    //    $('#dpExpectedDateOfCompletionOfProject').datepicker('setStartDate', null);
    //});

    //$('#dpExpectedDateOfCompletionOfProject').datepicker({
    //    format: "dd/mm/yyyy",
    //    clearBtn: true,
    //    autoclose: true,
    //    todayHighlight: true
    //}).on('changeDate', function (ev) {
    //    var projectStartDateEndDate = new Date(ev.date.valueOf());
    //    projectStartDateEndDate = projectStartDateEndDate.add(-1).days();
    //    $('#dpDateOfStartOfProject').datepicker('setEndDate', projectStartDateEndDate);
    //    $(this).blur();
    //    $(this).datepicker('hide');
    //}).on('clearDate', function (selected) {
    //    $('#dpDateOfStartOfProject').datepicker('setEndDate', null);
    //});




    //$('#dpSubChainageStartingDate').datepicker({
    //    format: "dd/mm/yyyy",
    //    clearBtn: true,
    //    autoclose: true,
    //    todayHighlight: true
    //}).on('changeDate', function (ev) {
    //    var subChainageCompletionDateStartDate = new Date(ev.date.valueOf());
    //    subChainageCompletionDateStartDate = subChainageCompletionDateStartDate.add(1).days();
    //    $('#dpSubChainageExpectedCompletionDate').datepicker('setStartDate', subChainageCompletionDateStartDate);
    //    $(this).blur();
    //    $(this).datepicker('hide');
    //}).on('clearDate', function (selected) {
    //    $('#dpSubChainageExpectedCompletionDate').datepicker('setStartDate', null);
    //});

    //$('#dpSubChainageExpectedCompletionDate').datepicker({
    //    format: "dd/mm/yyyy",
    //    clearBtn: true,
    //    autoclose: true,
    //    todayHighlight: true
    //}).on('changeDate', function (ev) {
    //    var subChainageStartDateEndDate = new Date(ev.date.valueOf());
    //    subChainageStartDateEndDate = subChainageStartDateEndDate.add(-1).days();
    //    $('#dpSubChainageStartingDate').datepicker('setEndDate', subChainageStartDateEndDate);
    //    $(this).blur();
    //    $(this).datepicker('hide');
    //}).on('clearDate', function (selected) {
    //    $('#dpSubChainageStartingDate').datepicker('setEndDate', null);
    //});



    //$('#addOfficersEngagedBtn').on('click', function (e) {

    //    if ($(".form").valid()) {

    //        $('#officersListDisplay').removeClass('hidden');

    //        var data = $('#officersEngaged').val();
    //        arrOfficersEngaged.push(data);
    //        $('#officersEngaged').val("");

    //        displayEngagedOfficersList('officersListDisplay', arrOfficersEngaged);
    //    }

    //});

    //$('#addPmcSiteEngineersBtn').on('click', function (e) {

    //    if ($(".form").valid()) {

    //        $('#pmcSiteEngineersListDisplay').removeClass('hidden');

    //        var data = $('#pmcSiteEngineers').val();
    //        arrPmcSiteEngineers.push(data);
    //        $('#pmcSiteEngineers').val("");

    //        displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);
    //    }

    //});

    //$('#addSubChainagesBtn').on('click', function (e) {

    //    if ($(".form").valid()) {

    //        $('#subChainagesListDisplay').removeClass('hidden');

    //        var subChainageName = $('#subChainageName').val();
    //        var subChainageSioGuid = $('#sioUsersList').val();

    //        var subChainageValue = $('#subChainageValue').val();
    //        var newchar = '';
    //        subChainageValue = subChainageValue.split(',').join(newchar);
    //        //subChainageValue = subChainageValue.replace(/,/g , newchar);
    //        subChainageValue = parseFloat(subChainageValue);
    //        //subChainageValue = new Decimal(subChainageValue);
    //        //alert(typeof subChainageValue);

    //        var administrativeApprovalReference = $('#administrativeApprovalReference').val();
    //        var technicalSanctionReference = $('#technicalSanctionReference').val();
    //        var contractorName = $('#contractorName').val();
    //        var workOrderReference = $('#workOrderReference').val();

    //        var subChainageStartingDate = $('#subChainageStartingDate').val();
    //        subChainageStartingDate = Date.parse(subChainageStartingDate).toString('yyyy/MM/dd');

    //        var subChainageExpectedCompletionDate = $('#subChainageExpectedCompletionDate').val();
    //        subChainageExpectedCompletionDate = Date.parse(subChainageExpectedCompletionDate).toString('yyyy/MM/dd');

    //        var typeOfWorks = $('#typeOfWorks').val();
    //        //var workTypeItems = $('#workTypeItems').val();

    //        if (arrWorkItems.length > 0) {
    //            arrSubChainages.push({
    //                subChainageName: subChainageName,
    //                subChainageSioGuid: subChainageSioGuid,
    //                subChainageValueInput: subChainageValue,
    //                administrativeApprovalReference: administrativeApprovalReference,
    //                technicalSanctionReference: technicalSanctionReference,
    //                contractorName: contractorName,
    //                workOrderReference: workOrderReference,
    //                subChainageStartingDate: subChainageStartingDate,
    //                subChainageExpectedCompletionDate: subChainageExpectedCompletionDate,
    //                typeOfWorks: typeOfWorks,
    //                workItemManager: arrWorkItems
    //            });

    //            displaySubChainagesList('subChainagesListDisplay', arrSubChainages);
    //        } else {
    //            bootbox.alert("Work Items List cannot be empty.");
    //        }

    //    }

    //});

    //$('#addWorkItemsBtn').on('click', function (e) {

    //    var workItemGuid = $('#workItemList').val();

    //    if (workItemGuid != "") {

    //        $('#workItemsListDisplay').removeClass('hidden');

    //        var workItemName = $('#workItemList option:selected').text();

    //        arrWorkItems.push({
    //            workItemGuid: workItemGuid,
    //            workItemName: workItemName
    //        });

    //        //$('option:selected', this).remove();

    //        displayWorkItemsList('workItemsListDisplay', arrWorkItems);

    //        removeWorkItemOptions('workItemList');

    //        showWorkItemOptions('workItemList', arrWorkItemsToDisplay);
    //    }


    //});

    //$('#assignValueToFinancialSubHeadBtn').on('click', function (e) {
    //    $("#assignValueToFinancialSubHeadPopupModal .modal-body").empty();

    //    var projectValue = $("#projectValue").val();
    //    var newchar = '';
    //    projectValue = projectValue.split(',').join(newchar);
    //    //subChainageValue = subChainageValue.replace(/,/g , newchar);
    //    projectValue = parseFloat(projectValue);

    //    var remainingProjectValue = projectValue;

    //    if (arrFinancialHeadBudgets.length > 0) {
    //        for (var i = 0; i < arrFinancialHeadBudgets.length; i++) {
    //            var a = arrFinancialHeadBudgets[i].financialSubHeadBudgetInput;
    //            remainingProjectValue = remainingProjectValue - a;
    //        }
    //    }

    //    if (remainingProjectValue > 0) {
    //        var html = '<div class="container-fluid col-md-12">';
    //        html += '<div class="row">';
    //        html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    //        html += '<div class="panel panel-default">';
    //        html += '<div class="panel-body">';
    //        html += '<div class="text-center">';
    //        html += '<img src="Images/create-financial.png" class="login" height="70" />';
    //        html += '<h2 class="text-center">Financial Head</h2>';

    //        html += '<div class="panel-body">';
    //        html += '<form id="assignValueToFinancialSubHeadPopupForm" class="popupForm form form-horizontal" method="post">';
    //        html += '<fieldset>';

    //        html += '<div class="form-group">';
    //        html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    //        html += '<label id="totalBudgetLblText" class="control-label">' + 'Total Budget: ' + '</label>';
    //        html += '</div>';
    //        html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    //        html += '<label id="totalBudgetLbl" class="control-label">' + accounting.formatMoney(projectValue, { symbol: "Rs.", format: "%s %v" }) + '</label>';
    //        html += '</div>';

    //        html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    //        html += '<label id="remainingBudgetLblText" class="control-label">' + 'Remaining Budget: ' + '</label>';
    //        html += '</div>';
    //        html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    //        html += '<label id="remainingBudgetLbl" name="remainingBudgetLbl" class="control-label" value="' + accounting.formatMoney(remainingProjectValue, { symbol: "Rs.", format: "%s %v" }) + '">' + accounting.formatMoney(remainingProjectValue, { symbol: "Rs.", format: "%s %v" }) + '</label>';
    //        html += '</div>';
    //        html += '</div>';

    //        //html += "<div class=\"form-group\">";
    //        //html += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    //        //html += "<label id=\"remainingBudgetLblText\" class=\"control-label\">" + "Remaining Budget: " + "</label>";
    //        //html += "</div>";
    //        //html += "<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\">";
    //        //html += "<label id=\"remainingBudgetLbl\" name=\"remainingBudgetLbl\" class=\"control-label\">" + accounting.formatMoney(remainingProjectValue, { symbol: "Rs.", format: "%s %v" }) + "</label>";
    //        //html += "</div>";
    //        //html += "</div>";

    //        html += '<div class="form-group">';
    //        html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    //        html += '<label for="financialSubHeadList" id="financialSubHeadListLabel" class="control-label">Name of Head</label>';
    //        html += '</div>';
    //        html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    //        html += '<select class="form-control" name="financialSubHeadList" id="financialSubHeadList"></select>';
    //        html += '</div>';
    //        html += '</div>';

    //        html += '<div class="form-group">';
    //        html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    //        html += '<label for="financialSubHeadBudget" id="financialSubHeadBudgetLabel" class="control-label">Budget</label>';
    //        html += '</div>';
    //        html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    //        html += '<input id="financialSubHeadBudget" name="financialSubHeadBudget" placeholder="Budget" class="form-control" />';
    //        html += '</div>';
    //        html += '</div>';

    //        html += '<div class="form-group">';
    //        html += '<button class="btn btn-primary" aria-hidden="true" id="createFinancialHeadBtn" name="createFinancialHeadBtn">Create Head</button>';
    //        html += '</div>';

    //        html += '</fieldset>';
    //        html += '</form>';
    //        html += '</div>';

    //        html += '</div>';
    //        html += '</div>';
    //        html += '</div>';
    //        html += '</div>';
    //        html += '</div>';
    //        html += '</div>';
    //        html += '</div>';

    //        $("#assignValueToFinancialSubHeadPopupModal .modal-body").append(html);

    //        showFinancialSubHeadOptions('financialSubHeadList', arrFinancialSubHeadsToDisplay);

    //        $('#assignValueToFinancialSubHeadPopupModal').modal('show');

    //        callOtherFuctions();

    //        $('#financialSubHeadBudget').maskMoney();

    //        $('#createFinancialHeadBtn').on('click', function (e) {
    //            e.preventDefault();
    //            if ($("#assignValueToFinancialSubHeadPopupForm").valid()) {

    //                var financialSubHeadNameGuid = $('#financialSubHeadList').val();
    //                var financialSubHeadName = $('#financialSubHeadList option:selected').text();

    //                var arrFinancialSubHeads = {
    //                    financialSubHeadGuid: financialSubHeadNameGuid,
    //                    financialSubHeadName: financialSubHeadName
    //                };

    //                var financialSubHeadBudget = $('#financialSubHeadBudget').val();
    //                var newchar = '';
    //                financialSubHeadBudget = financialSubHeadBudget.split(',').join(newchar);
    //                //subChainageValue = subChainageValue.replace(/,/g , newchar);
    //                financialSubHeadBudget = parseFloat(financialSubHeadBudget);

    //                arrFinancialHeadBudgets.push({
    //                    financialSubHeadManager: arrFinancialSubHeads,
    //                    financialSubHeadBudgetInput: financialSubHeadBudget
    //                });

    //                createFinancialSubHeadTable();

    //                $('#assignValueToFinancialSubHeadPopupModal').modal('hide');
    //            }

    //        });
    //    } else {
    //        bootbox.alert("You have assigned the whole Project Value.");
    //    }

    //});

});

getSubChainagePeriod = function () {
    $.ajax({
        type: "Post",
        async: false,
        url: "EnterProgress.aspx/GetSubChainagePeriod",
        //data: '{"emailId":"' + emailId + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            subChainagePeriod = response.d.split('|');
        },
        failure: function (msg) {
            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });
}

// Hide buttons according to the current step
//hideButtons = function (current) {
//    var limit = parseInt(widget.length);

//    $(".action").hide();

//    if (current < limit) {
//        btnNext.show();
//    }

//    if (current > 1) {
//        btnBack.show();
//    }

//    if (current == limit) {
//        btnNext.hide();
//        btnSubmit.show();
//    }

//    if (current == 2) {
//        btnSkip.show();
//    }

//    if (current == 3) {
//        btnSkip.show();
//    }

//    if (current == 6) {
//        btnNext.hide();
//    }
//}

//// Change progress bar action
//setProgress = function (currstep) {
//    var percent = parseFloat(100 / widget.length) * currstep;
//    percent = percent.toFixed();
//    $(".progress-bar").css("width", percent + "%").html(percent + "%");
//    $("#step-display").html("Step " + currstep + " of " + widget.length);
//}

//displayEngagedOfficersList = function (divname, arr) {

//    var div = $('#' + divname);
//    div.empty();

//    div.removeClass('hidden');

//    //var newDiv = document.createElement('div');
//    // newDiv.className = "panel panel-default";

//    //var html = '<div class="panel-heading">Panel heading</div>';
//    //html += '<div class="panel-body">';

//    var html = '<ul class = "list-group">';
//    for (var index = 0; index < arr.length; index++) {
//        html += '<li class="list-group-item">';
//        html += (index + 1) + '.  ';
//        html += arr[index] + '  ';
//        html += '<button type="button" class="btn btn-primary" onclick="removeOfficersEngaged(' + index + ');">Remove</button>';
//        html += '</li>';
//    }

//    html += '</ul>';

//    //html += '</div>';

//    //newDiv.innerHTML = html;

//    //document.getElementById(divname).appendChild(newDiv);

//    document.getElementById(divname).innerHTML = html;

//    if (arr.length == 0) {
//        div.addClass('hidden');
//    }
//}

//removeOfficersEngaged = function (index) {
//    //alert("aditya");
//    //alert(index);
//    arrOfficersEngaged.splice(index, 1);

//    displayEngagedOfficersList('officersListDisplay', arrOfficersEngaged);

//}

//displayPmcSiteEngineersList = function (divname, arr) {

//    var div = $('#' + divname);
//    div.empty();

//    div.removeClass('hidden');

//    //var newDiv = document.createElement('div');
//    //newDiv.className = "panel panel-default";

//    //var html = '<div class="panel-heading">Panel heading</div>';
//    //html += '<div class="panel-body">';

//    var html = '<ul class = "list-group">';
//    for (var index = 0; index < arr.length; index++) {
//        html += '<li class="list-group-item">';
//        html += (index + 1) + '.  ';
//        html += arr[index] + '  ';
//        html += '<button type="button" class="btn btn-primary" onclick="removePmcSiteEngineers(' + index + ');">Remove</button>';
//        html += '</li>';
//    }

//    html += '</ul>';

//    //html += '</div>';

//    //newDiv.innerHTML = html;

//    //document.getElementById(divname).appendChild(newDiv);

//    document.getElementById(divname).innerHTML = html;

//    if (arr.length == 0) {
//        div.addClass('hidden');
//    }
//}

//removePmcSiteEngineers = function (index) {
//    //alert("aditya");
//    //alert(index);
//    arrPmcSiteEngineers.splice(index, 1);

//    displayPmcSiteEngineersList('pmcSiteEngineersListDisplay', arrPmcSiteEngineers);

//}

//getWorkItems = function () {
//    $.ajax({
//        type: "Post",
//        async: false,
//        url: "DBAMethods.asmx/GetWorkItems",
//        //data: '{"emailId":"' + emailId + '"}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            arrWorkItemsToDisplay = response.d;
//            showWorkItemOptions('workItemList', arrWorkItemsToDisplay);
//        },
//        failure: function (msg) {
//            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
//            $('#preloader').delay(350).fadeOut('slow');
//            bootbox.alert(msg);
//        }
//    });
//}

//showWorkItemOptions = function (id, workItemArray) {
//    var option_str = document.getElementById(id);
//    option_str.length = 0;
//    option_str.options[0] = new Option('--Select Work Item--', '');
//    option_str.selectedIndex = 0;
//    for (var i = 0; i < workItemArray.length;) {
//        option_str.options[option_str.length] = new Option(workItemArray[i + 1], workItemArray[i]);
//        if (arrWorkItems.length > 0) {
//            for (var a = 0; a < arrWorkItems.length; a++) {
//                if (workItemArray[i] == arrWorkItems[a].workItemGuid) {
//                    option_str.options[i + 1].classList.add("hidden");
//                    //option_str.classList.toggle("hidden");
//                }
//            }
//        }
//        i += 2;
//    }
//}

//displayWorkItemsList = function (divname, arr) {

//    var div = $('#' + divname);
//    div.empty();

//    div.removeClass('hidden');

//    //var newDiv = document.createElement('div');
//    //newDiv.className = "panel panel-default";

//    //var html = '<div class="panel-heading">Panel heading</div>';
//    //html += '<div class="panel-body">';

//    var html = '<ul class = "list-group">';
//    for (var index = 0; index < arr.length; index++) {
//        html += '<li class="list-group-item">';
//        html += (index + 1) + '.  ';
//        html += arr[index].workItemName + '  ';
//        html += '<button type="button" class="btn btn-primary" onclick="removeWorkItems(' + index + ');">Remove</button>';
//        html += '</li>';
//    }

//    html += '</ul>';

//    //html += '</div>';

//    //newDiv.innerHTML = html;

//    //document.getElementById(divname).appendChild(newDiv);

//    document.getElementById(divname).innerHTML = html;

//    if (arr.length == 0) {
//        div.addClass('hidden');
//    }
//}

//removeWorkItems = function (index) {
//    //alert("aditya");
//    //alert(index);
//    arrWorkItems.splice(index, 1);

//    displayWorkItemsList('workItemsListDisplay', arrWorkItems);

//    removeWorkItemOptions('workItemList');

//    showWorkItemOptions('workItemList', arrWorkItemsToDisplay);

//}

//removeWorkItemOptions = function (id) {
//    var option_str = document.getElementById(id);

//    //for (var i = option_str.length; i > 0; i--) {
//    //    option_str.remove(option_str.length - 1);
//    //}

//    for (var i = 0; i < option_str.options.length; i++) {
//        option_str.options[i] = null;
//    }
//}

//displaySubChainagesList = function (divname, arr) {

//    var div = $('#' + divname);
//    div.empty();

//    div.removeClass('hidden');

//    //var newDiv = document.createElement('div');
//    //newDiv.className = "panel panel-default";

//    //var html = '<div class="panel-heading">Panel heading</div>';
//    //html += '<div class="panel-body">';

//    var html = '<ul class = "list-group">';
//    for (var index = 0; index < arr.length; index++) {
//        html += '<li class="list-group-item">';
//        html += (index + 1) + '.  ';
//        html += arr[index].subChainageName + '  ';
//        //alert(arr[index].subChainageName + " " + arr[index].subChainageSioName + " " + arr[index].subChainageProjectValue + " " + arr[index].aaRef + " " + arr[index].tsRef + " " + arr[index].contractorName + " " + arr[index].workOrderRef + " " + arr[index].subChainageStartingDate + " " + arr[index].subChainageCompletionDate + " " + arr[index].typeOfWorks);
//        html += '<button type="button" class="btn btn-primary" onclick="removeSubChainages(' + index + ');">Remove</button>';
//        html += '</li>';
//    }

//    html += '</ul>';

//    //html += '</div>';

//    //newDiv.innerHTML = html;

//    //document.getElementById(divname).appendChild(newDiv);

//    document.getElementById(divname).innerHTML = html;

//    if (arr.length == 0) {
//        div.addClass('hidden');
//    }
//}

//removeSubChainages = function (index) {
//    //alert("aditya");
//    //alert(index);
//    arrSubChainages.splice(index, 1);
//    displaySubChainagesList('subChainagesListDisplay', arrSubChainages);
//}

//getFinancialSubHeads = function () {
//    $.ajax({
//        type: "Post",
//        async: false,
//        url: "DBAMethods.asmx/GetFinancialSubHeads",
//        //data: '{"emailId":"' + emailId + '"}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            arrFinancialSubHeadsToDisplay = response.d;
//            //showFinancialSubHeadOptions('workTypeItemList', arrFinancialSubHeadsToDisplay);
//        },
//        failure: function (msg) {
//            $('#status').delay(300).fadeOut(); // will first fade out the loading animation
//            $('#preloader').delay(350).fadeOut('slow');
//            bootbox.alert(msg);
//        }
//    });
//}

//removeFinancialSubHeadOptions = function (id) {
//    var option_str = document.getElementById(id);

//    //for (var i = option_str.length; i > 0; i--) {
//    //    option_str.remove(option_str.length - 1);
//    //}

//    for (var i = 0; i < option_str.options.length; i++) {
//        option_str.options[i] = null;
//    }
//}

//showFinancialSubHeadOptions = function (id, financialSubHeadArray) {
//    var option_str = document.getElementById(id);
//    option_str.length = 0;
//    option_str.options[0] = new Option('--Select Sub-Head--', '');
//    option_str.selectedIndex = 0;
//    for (var i = 0; i < financialSubHeadArray.length;) {
//        option_str.options[option_str.length] = new Option(financialSubHeadArray[i + 1], financialSubHeadArray[i]);
//        if (arrFinancialHeadBudgets.length > 0) {
//            for (var a = 0; a < arrFinancialHeadBudgets.length; a++) {
//                if (financialSubHeadArray[i] == arrFinancialHeadBudgets[a].financialSubHeadNameGuid) {
//                    option_str.options[i + 1].classList.add("hidden");
//                    //option_str.classList.toggle("hidden");
//                }
//            }
//        }
//        i += 2;
//    }
//}

//callOtherFuctions = function () {
    //$('#assignValueToFinancialSubHeadPopupForm').validate({ // initialize plugin
    //    ignore: ":not(:visible)",
    //    rules: {
    //        financialSubHeadList: "required",
    //        financialSubHeadBudget: {
    //            required: true,
    //            currencySmallerThan: [name = 'remainingBudgetLbl']
    //        }
    //    },
    //});

//}

//createFinancialSubHeadTable = function () {
//    $('#financialSubHeadTableDiv').empty();

//    var projectValue = $("#projectValue").val();
//    var newchar = '';
//    projectValue = projectValue.split(',').join(newchar);
//    //subChainageValue = subChainageValue.replace(/,/g , newchar);
//    projectValue = parseFloat(projectValue);

//    var totalAssignedValue = 0;

//    if (arrFinancialHeadBudgets.length > 0) {
//        for (var i = 0; i < arrFinancialHeadBudgets.length; i++) {
//            var a = arrFinancialHeadBudgets[i].financialSubHeadBudgetInput;
//            totalAssignedValue = totalAssignedValue + a;
//        }
//    }

//    var remainingProjectValue = projectValue;

//    if (arrFinancialHeadBudgets.length > 0) {
//        for (var i = 0; i < arrFinancialHeadBudgets.length; i++) {
//            var a = arrFinancialHeadBudgets[i].financialSubHeadBudgetInput;
//            remainingProjectValue = remainingProjectValue - a;
//        }
//    }

//    if (arrFinancialHeadBudgets.length > 0) {
//        var html1 = "<table class=\"table table-striped\">";

//        html1 += "<thead>";
//        html1 += "<tr>";
//        html1 += "<th>";
//        html1 += "Head Name";
//        html1 += "</th>";
//        html1 += "<th style=\"text-align:right\">";
//        html1 += "Head Budget (in Rs.)";
//        html1 += "</th>";
//        html1 += "<th>";
//        html1 += "";
//        html1 += "</th>";
//        html1 += "</tr>";
//        html1 += "</thead>";

//        html1 += "<tfoot>";
//        html1 += "<tr>";
//        html1 += "<th>";
//        html1 += "Total Budget";
//        html1 += "</th>";
//        html1 += "<th class=\"numColumn\" style=\"text-align:right\">";
//        html1 += $.format(parseFloat(projectValue));//projectValue//accounting.formatMoney(projectValue, { symbol: "Rs.", format: "%s %v" });
//        html1 += "</th>";
//        html1 += "<th>";
//        html1 += "";
//        html1 += "</th>";
//        html1 += "</tr>";

//        html1 += "<tr>";
//        html1 += "<th>";
//        html1 += "Total Value Assigned to Heads";
//        html1 += "</th>";
//        html1 += "<th class=\"numColumn\" style=\"text-align:right\">";
//        html1 += $.format(parseFloat(totalAssignedValue)); //totalAssignedValue//accounting.formatMoney(totalAssignedValue, { symbol: "Rs.", format: "%s %v" });
//        html1 += "</th>";
//        html1 += "<th>";
//        html1 += "";
//        html1 += "</th>";
//        html1 += "</tr>";

//        html1 += "<tr>";
//        html1 += "<th>";
//        html1 += "Remaining Value";
//        html1 += "</th>";
//        html1 += "<th class=\"numColumn\" style=\"text-align:right\">";
//        html1 += $.format(parseFloat(remainingProjectValue)); //remainingProjectValue;//accounting.formatMoney(remainingProjectValue, { symbol: "Rs.", format: "%s %v" })
//        html1 += "</th>";
//        html1 += "<th>";
//        html1 += "";
//        html1 += "</th>";
//        html1 += "</tr>";
//        html1 += "</tfoot>";

//        html1 += "<tbody>";

//        for (var i = 0; i < arrFinancialHeadBudgets.length; i++) {
//            html1 += "<tr>";

//            html1 += "<th>";
//            html1 += arrFinancialHeadBudgets[i].financialSubHeadManager.financialSubHeadName;
//            html1 += "</th>";

//            html1 += "<th class=\"numColumn\" style=\"text-align:right\">";
//            html1 += $.format(parseFloat(arrFinancialHeadBudgets[i].financialSubHeadBudgetInput)); //arrHeadBudgets[i].headBudget;//accounting.formatMoney(arrHeadBudgets[i].headBudget, { symbol: "Rs.", format: "%s %v" });
//            html1 += "</th>";

//            html1 += "<th>";
//            html1 += '<button type="button" class="btn btn-primary" onclick="removeFinancialSubHeadTable(' + i + ');">Remove</button>';;
//            html1 += "</th>";

//            html1 += "</tr>";
//        }


//        html1 += "</tbody>";

//        html1 += "</table>";

//        $('#financialSubHeadTableDiv').append(html1);
//    }

//    if (remainingProjectValue > 0) {
//        btnNext.hide();
//    } else if (remainingProjectValue == 0) {
//        btnNext.show();
//    }
//}

//removeFinancialSubHeadTable = function (index) {
//    //alert("aditya");
//    //alert(index);
//    arrFinancialHeadBudgets.splice(index, 1);

//    createFinancialSubHeadTable();
//}

//saveProject = function () {
//    var retValue = false;

//    var districtGuid = $("#districtList").val();
//    var divisionGuid = $("#divisionList").val();
//    var projectName = $("#projectName").val();
//    var subProjectName = $("#subProjectName").val();
//    var chainageOfProject = $("#chainageOfProject").val();
//    var dateOfStartOfProject = $("#dateOfStartOfProject").val();
//    dateOfStartOfProject = Date.parse(dateOfStartOfProject).toString('yyyy/MM/dd');
//    var expectedDateOfCompletionOfProject = $("#expectedDateOfCompletionOfProject").val();
//    expectedDateOfCompletionOfProject = Date.parse(expectedDateOfCompletionOfProject).toString('yyyy/MM/dd');
//    var projectValue = $("#projectValue").val();
//    var newchar = '';
//    projectValue = projectValue.split(',').join(newchar);
//    projectValue = parseFloat(projectValue);

//    var arrOfficersEngagedJsonString = JSON.stringify(arrOfficersEngaged);

//    var arrPmcSiteEngineersJsonString = JSON.stringify(arrPmcSiteEngineers);

//    var arrSubChainagesJsonString = JSON.stringify(arrSubChainages);

//    var includeLandAcquisition = $('input[name=landAcquisitionRadio]:checked', '#createNewProjectForm').val();

//    var arrFinancialHeadBudgetsJsonString = JSON.stringify(arrFinancialHeadBudgets);

//    $.ajax({
//        type: "Post",
//        async: false,
//        url: "CreateNewProject.aspx/SaveProject",
//        data: '{"districtGuid":"' + districtGuid + '","divisionGuid":"' + divisionGuid + '","projectName":"' + projectName + '","subProjectName":"' + subProjectName + '","chainageOfProject":"' + chainageOfProject +
//                '","dateOfStartOfProject":"' + dateOfStartOfProject + '","expectedDateOfCompletionOfProject":"' + expectedDateOfCompletionOfProject + '","projectValue":"' + projectValue +
//                '","arrOfficersEngaged":' + arrOfficersEngagedJsonString + ',"arrPmcSiteEngineers":' + arrPmcSiteEngineersJsonString + ',"arrSubChainages":' + arrSubChainagesJsonString +
//                ',"includeLandAcquisition":"' + includeLandAcquisition + '","arrFinancialHeadBudgets":' + arrFinancialHeadBudgetsJsonString + '}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            //$('#status').delay(300).fadeOut();
//            //$('#preloader').delay(350).fadeOut('slow');
//            retValue = response.d;
//            //if (str == true) {
//            //    bootbox.alert("Step 1 Saved.");
//            //}
//            //else {
//            //    bootbox.alert("Data cannot be saved.");
//            //}
//            //str = str1;
//        },
//        failure: function (msg) {
//            alert(msg);
//            //$('#status').delay(300).fadeOut(); // will first fade out the loading animation
//            //$('#preloader').delay(350).fadeOut('slow');
//            bootbox.alert(msg);
//        }
//    });

//    return retValue;
//}


saveProjectPhysicalProgress = function () {
    var retValue = false;

    var progressYear = new Date($('#dpProgressMonth').datepicker('getDate')).getFullYear();
    var progressMonth = new Date($('#dpProgressMonth').datepicker('getDate')).getMonth();

    var projectGuid = $('#hdProjectGuid').val();
    var subChainageGuid = $('#hdSubChainageGuid').val();

    var fremaaOffiersVisited = [];

    var y = document.getElementsByClassName("fremaaOfficers");

    for (var a = 0; a < y.length; a++) {
        var x = y[a].getElementsByTagName("input")[0].getAttribute("name");

        fremaaOffiersVisited.push($('input[name="' + x + '"]:checked', '#subChainageProgressForm').val());
    }

    var pmcSiteEngineersVisited = [];

    var z = document.getElementsByClassName("pmcSiteEngineers");

    for (var a = 0; a < z.length; a++) {
        var x = z[a].getElementsByTagName("input")[0].getAttribute("name");

        pmcSiteEngineersVisited.push($('input[name="' + x + '"]:checked', '#subChainageProgressForm').val());
    }

    var z1 = document.getElementsByClassName("workItems");

    //var workItemGuid = 

    var districtGuid = $("#districtList").val();
    var divisionGuid = $("#divisionList").val();
    var projectName = $("#projectName").val();
    var subProjectName = $("#subProjectName").val();
    var chainageOfProject = $("#chainageOfProject").val();
    var dateOfStartOfProject = $("#dateOfStartOfProject").val();
    dateOfStartOfProject = Date.parse(dateOfStartOfProject).toString('yyyy/MM/dd');
    var expectedDateOfCompletionOfProject = $("#expectedDateOfCompletionOfProject").val();
    expectedDateOfCompletionOfProject = Date.parse(expectedDateOfCompletionOfProject).toString('yyyy/MM/dd');
    var projectValue = $("#projectValue").val();
    var newchar = '';
    projectValue = projectValue.split(',').join(newchar);
    projectValue = parseFloat(projectValue);

    var arrOfficersEngagedJsonString = JSON.stringify(arrOfficersEngaged);

    var arrPmcSiteEngineersJsonString = JSON.stringify(arrPmcSiteEngineers);

    var arrSubChainagesJsonString = JSON.stringify(arrSubChainages);

    var includeLandAcquisition = $('input[name=landAcquisitionRadio]:checked', '#createNewProjectForm').val();

    var arrFinancialHeadBudgetsJsonString = JSON.stringify(arrFinancialHeadBudgets);

    $.ajax({
        type: "Post",
        async: false,
        url: "CreateNewProject.aspx/SaveProject",
        data: '{"districtGuid":"' + districtGuid + '","divisionGuid":"' + divisionGuid + '","projectName":"' + projectName + '","subProjectName":"' + subProjectName + '","chainageOfProject":"' + chainageOfProject +
                '","dateOfStartOfProject":"' + dateOfStartOfProject + '","expectedDateOfCompletionOfProject":"' + expectedDateOfCompletionOfProject + '","projectValue":"' + projectValue +
                '","arrOfficersEngaged":' + arrOfficersEngagedJsonString + ',"arrPmcSiteEngineers":' + arrPmcSiteEngineersJsonString + ',"arrSubChainages":' + arrSubChainagesJsonString +
                ',"includeLandAcquisition":"' + includeLandAcquisition + '","arrFinancialHeadBudgets":' + arrFinancialHeadBudgetsJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
            //if (str == true) {
            //    bootbox.alert("Step 1 Saved.");
            //}
            //else {
            //    bootbox.alert("Data cannot be saved.");
            //}
            //str = str1;
        },
        failure: function (msg) {
            alert(msg);
            //$('#status').delay(300).fadeOut(); // will first fade out the loading animation
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert(msg);
        }
    });

    return retValue;
}