﻿$(document).ready(function () {

    var cfgCulture = 'en-IN';
    $.preferCulture(cfgCulture);

    $('#assetCost').maskMoney();

    //$(document.body).on('click', 'button#updateAssetBtn', function (e) {
    $('#updateAssetBtn').on('click', function (e) {
        e.preventDefault();

        if ($('#editAssetForm').valid()) {

            var assetUpdated = updateAsset();

            if (assetUpdated === 1) {
                bootbox.alert("Your data is updated successfully.", function () {

                    //window.location.replace("http://www.google.com")

                    //window.location = 'EditAsset.aspx';

                    //window.location = 'DBAAssets.aspx';
                    //setTimeout(function () { window.location = 'DBAAssets.aspx'; }, 4000);
                    location.reload(true);
                });
            } else {
                bootbox.alert("Your data cannot be updated.");
            }
        }
       
    });

    $('#cancelAssetEditBtn').on('click', function (e) {
        e.preventDefault();

        window.location = 'DBAAssets.aspx';
    });

    $('#dpDateOfCompletionOfAsset').datepicker({
        format: "dd/mm/yyyy",
    });

    $('#dpStartDate').datepicker({
        format: "dd/mm/yyyy",
    });

    $('#dpEndDate').datepicker({
        format: "dd/mm/yyyy",
    });

    $('#editAssetForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {            
            bridgeTypeSelect: "required",
            liningTypeSelect: "required",
            culvertTypeSelect: "required",
            crestTypeSelect: "required",
            fillTypeSelect: "required",
            gaugeTypeSelect: "required",
            seasonTypeSelect: "required",
            frequencyTypeSelect: "required",
            porcupineTypeSelect: "required",
            materialTypeSelect: "required",
            regulatorTypeSelect: "required",
            regulatorGateTypeSelect: "required",
            revetmentTypeSelect: "required",
            riverProtectionTypeSelect: "required",
            waveProtectionTypeSelect: "required",
            sluiceTypeSelect: "required",
            shapeTypeSelect: "required",
            constructionTypeSelect: "required",
            spurRevetTypeSelect: "required",
            turnoutTypeSelect: "required",
            turnoutGateTypeSelect: "required",
            weirTypeSelect: "required",
            assetName: {
                required: true,
                noSpace: true
            },
            chainageStart: {
                number: true
            },
            chainageEnd: {
                number: true
            },
            eastingUS: {
                number: true
            },
            eastingDS: {
                number: true
            },
            northingUS: {
                number: true
            },
            northingDS: {
                number: true
            },
            dateOfCompletionOfAsset: {
                required: true,
                dateITA: true
            },
            lengthBridge: {
                number: true
            },
            numberPeirs: {
                number: true
            },
            roadWidth: {
                number: true
            },
            uSCrestElevationCanal: {
                number: true
            },
            dSCrestElevationCanal: {
                number: true
            },
            lengthCanal: {
                number: true
            },
            bedWidthCanal: {
                number: true
            },
            slipeSlope1Canal: {
                number: true
            },
            slipeSlope2Canal: {
                number: true
            },
            averageDepthCanal: {
                number: true
            },
            numberOfVents: {
                number: true
            },
            ventHeight: {
                number: true
            },
            ventWidth: {
                number: true
            },
            lengthCulvert: {
                number: true
            },
            usCrestElevationDrainage: {
                number: true
            },
            dsCrestElevationDrainage: {
                number: true
            },
            lengthDrainage: {
                number: true
            },
            BedWidthDrainage: {
                number: true
            },
            slope1Drainage: {
                number: true
            },
            slope2Drainage: {
                number: true
            },
            averageDepthDrainage: {
                number: true
            },
            uSCrestElevation: {
                number: true
            },
            dSCrestElevation: {
                number: true
            },
            length: {
                number: true
            },
            crestWidth: {
                number: true
            },
            csSlope1: {
                number: true
            },
            csSlope2: {
                number: true
            },
            rsSlope1: {
                number: true
            },
            rsSlope2: {
                number: true
            },
            bermSlope1: {
                number: true
            },
            bermSlope2: {
                number: true
            },
            platformWidth: {
                number: true
            },
            averageHeight: {
                number: true
            },
            zeroDatum: {
                number: true
            },
            startDate: {
                required: true,
                dateITA: true
            },
            endDate: {
                required: true,
                dateITA: true
            },
            usInvertLevel: {
                number: true
            },
            dsInvertLevel: {
                number: true
            },
            stillingBasinLength: {
                number: true
            },
            stillingBasinWidth: {
                number: true
            },
            screenLength: {
                number: true
            },
            spacingAlongScreen: {
                number: true
            },
            spacingAlongScreen: {
                number: true
            },
            screenRows: {
                number: true
            },
            noOfLayers: {
                number: true
            },
            mamberLength: {
                number: true
            },
            lengthPorcupine: {
                number: true
            },
            numberOfVentsRegulator: {
                number: true
            },
            ventHeightRegulator: {
                number: true
            },
            ventWidthRegulator: {
                number: true
            },
            slopeRevetment: {
                number: true
            },
            lengthRevetment: {
                number: true
            },
            plainWidth: {
                number: true
            },
            NoOfVentsSluice: {
                number: true
            },
            ventDiameterSluice: {
                number: true
            },
            ventHeightSluice: {
                number: true
            },
            ventWidthSluice: {
                number: true
            },
            orientation: {
                number: true
            },
            lengthSpur: {
                number: true
            },
            widthSpur: {
                number: true
            },
            inletBoxWidth: {
                number: true
            },
            inletBoxLength: {
                number: true
            },
            outletNo: {
                number: true
            },
            outletWidth: {
                number: true
            },
            outletHeight: {
                number: true
            },
            invertLevel: {
                number: true
            },
            crestTopLevel: {
                number: true
            },
            crestTopWidth: {
                number: true
            }
        }
    });

});

updateAsset = function () {
    $("#preloader").show();
    $("#status").show();

    var ret = 0;

    var assetTypeManager = {
        assetTypeCode: $('#assetTypeCode').val()
    }

    var completionDate = $('#dateOfCompletionOfAsset').val();
    completionDate = Date.parse(completionDate).toString('yyyy/MM/dd');

    var assetCost = $("#assetCost").val();
    var newchar = '';
    assetCost = assetCost.split(',').join(newchar);
    assetCost = stringIsNullOrEmpty($.trim(assetCost)) ? parseFloat(0) : parseFloat(assetCost); 

    var assetManager = {
        assetCode: $('#assetCode').val(),
        drawingCode: $('#drawingCode').val(),
        amtd: $('#amtd').val(),
        assetName: $('#assetName').val(),
        startChainage: stringIsNullOrEmpty($.trim($('#chainageStart').val())) ? 0 : parseInt($.trim($('#chainageStart').val())),
        endChainage: stringIsNullOrEmpty($.trim($('#chainageEnd').val())) ? 0 : parseInt($.trim($('#chainageEnd').val())),
        utmEast1: stringIsNullOrEmpty($.trim($('#eastingUS').val())) ? 0 : parseInt($.trim($('#eastingUS').val())),
        utmEast2: stringIsNullOrEmpty($.trim($('#eastingDS').val())) ? 0 : parseInt($.trim($('#eastingDS').val())),
        utmNorth1: stringIsNullOrEmpty($.trim($('#northingUS').val())) ? 0 : parseInt($.trim($('#northingUS').val())),
        utmNorth2: stringIsNullOrEmpty($.trim($('#northingDS').val())) ? 0 : parseInt($.trim($('#northingDS').val())),        
        completionDate: completionDate,
        assetCostInput: assetCost,
        //divisionManager: divisionManager,
        //schemeManager: schemeManager,
        assetTypeManager: assetTypeManager,
        canalManager: canalManager,
        culvertManager: culvertManager,
        drainageManager: drainageManager,
        embankmentManager: embankmentManager,
        gaugeManager: gaugeManager,
        porcupineManager: porcupineManager,
        regulatorManager: regulatorManager,
        revetmentManager: revetmentManager,
        sluiceManager: sluiceManager,
        spurManager: spurManager,
        turnoutManager: turnoutManager,
        weirManager: weirManager
    }

    var bridgeTypeManager;
    var bridgeManager;

    var canalLiningTypeManager;
    var canalManager;

    var culvertTypeManager;
    var culvertManager;

    var drainageManager;

    var dropStructureManager;

    var embankmentCrestTypeManager;
    var embankmentFillTypeManager;
    var embankmentManager;

    var gaugeTypeManager;
    var gaugeSeasonTypeManager;
    var gaugeFrequencyTypeManager;
    var gaugeManager;

    var porcupineTypeManager;
    var porcupineMaterialTypeManager;
    var porcupineManager;

    var regulatorTypeManager;
    var regulatorGateTypeManager;
    var regulatorManager;

    var revetmentTypeManager;
    var revetmentRiverprotTypeManager;
    var revetmentWaveprotTypeManager;
    var revetmentManager;

    var sluiceTypeManager;
    var sluiceManager;

    var spurConstructionTypeManager;
    var spurRevetTypeManager;
    var spurShapeTypeManager;
    var spurManager;

    var turnoutTypeManager;
    var turnoutGateTypeManager;
    var turnoutManager;

    var weirTypeManager;
    var weirManager;

    //var assetTypeCode = $("#assetTypeList option:selected").text().split("-");

    switch (assetTypeManager.assetTypeCode) {
        case 'BRG':
            bridgeTypeManager = {
                bridgeTypeGuid: $('#bridgeTypeSelect').val(),
                //bridgeTypeName: $('#bridgeTypeSelect').val(),
            }

            bridgeManager = {
                bridgeGuid: $('#bridgeGuidHidden').val(),
                length: $('#lengthBridge').val(),
                pierNumber: $('#numberPeirs').val(),
                roadWidth: $('#roadWidth').val(),
                bridgeTypeManager: bridgeTypeManager
            }
            assetManager.bridgeManager = bridgeManager;
            ret = updateAssetWithBridge(assetManager);
            break;

        case 'CAN':
            canalLiningTypeManager = {
                liningTypeGuid: $('#liningTypeSelect').val(),
            }

            canalManager = {
                canalGuid: $('#canalGuidHidden').val(),
                usElevation: $('#uSCrestElevationCanal').val(),
                dsElevation: $('#dSCrestElevationCanal').val(),
                length: $('#lengthCanal').val(),
                bedWidth: $('#bedWidthCanal').val(),
                slope1: $('#slipeSlope1Canal').val(),
                slope2: $('#slipeSlope2Canal').val(),
                averageDepth: $('#averageDepthCanal').val(),
                canalLiningTypeManager: canalLiningTypeManager
            }

            assetManager.canalManager = canalManager;

            updateAssetWithCanal(assetManager);
            break;

        case 'CUL':
            culvertTypeManager = {
                culvertTypeGuid: $('#culvertTypeSelect').val(),
            }

            culvertManager = {
                culvertGuid: $('#culvertGuidHidden').val(),
                ventNumber: $('#numberOfVents').val(),
                ventHeight: $('#ventHeight').val(),
                ventWidth: $('#ventWidth').val(),
                lengthCulvert: $('#lengthCulvert').val(),
                culvertTypeManager: culvertTypeManager
            }

            assetManager.culvertManager = culvertManager;
            updateAssetWithCulvert(assetManager);
            break;

        case 'DRN':
            drainageManager = {
                drainageGuid: $('#drainageGuidHidden').val(),
                usElevationDrainage: $('#usCrestElevationDrainage').val(),
                dsElevationDrainage: $('#dsCrestElevationDrainage').val(),
                lengthDrainage: $('#lengthDrainage').val(),
                bedWidthDrainage: $('#bedWidthDrainage').val(),
                slope1Drainage: $('#slope1Drainage').val(),
                slope2Drainage: $('#slope2Drainage').val(),
                averageDepthDrainage: $('#averageDepthDrainage').val()
            }

            assetManager.drainageManager = drainageManager;
            updateAssetWithDrainage(assetManager);
            break;

        case 'DRP':
            dropStructureManager = {
                dropStructureGuid: $('#dropStructureGuidHidden').val(),
                usInvertLevel: $('#usInvertLevel').val(),
                dsInvertLevel: $('#dsInvertLevel').val(),
                stillingBasinLength: $('#stillingBasinLength').val(),
                stillingBasinWidth: $('#stillingBasinWidth').val()
            }
            assetManager.dropStructureManager = dropStructureManager;
            updateAssetWithDropStructure(assetManager);
            break;

        case 'EMB':
            embankmentCrestTypeManager = {
                crestTypeGuid: $('#crestTypeSelect').val(),
            }
            embankmentFillTypeManager = {
                fillTypeGuid: $('#fillTypeSelect').val(),
            }

            embankmentManager = {
                embankmentGuid: $('#embankmentGuidHidden').val(),
                usElevation: $('#uSCrestElevation').val(),
                dsElevation: $('#dSCrestElevation').val(),
                length: $('#length').val(),
                crestWidth: $('#crestWidth').val(),
                csSlope1: $('#csSlope1').val(),
                csSlope2: $('#csSlope2').val(),
                rsSlope1: $('#rsSlope1').val(),
                rsSlope2: $('#rsSlope2').val(),
                bermSlope1: $('#bermSlope1').val(),
                bermSlope2: $('#bermSlope2').val(),
                platformWidth: $('#platformWidth').val(),
                averageHeight: $('#averageHeight').val(),
                embankmentCrestTypeManager: embankmentCrestTypeManager,
                embankmentFillTypeManager: embankmentFillTypeManager
            }
            assetManager.embankmentManager = embankmentManager;
            updateAssetWithEmbankment(assetManager);
            break;

        case 'GAU':
            gaugeTypeManager = {
                gaugeTypeGuid: $('#gaugeTypeSelect').val(),
            }
            gaugeSeasonTypeManager = {
                seasonTypeGuid: $('#seasonTypeSelect').val(),
            }
            gaugeFrequencyTypeManager = {
                frequencyTypeGuid: $('#frequencyTypeSelect').val(),
            }

            var startDate = $('#startDate').val();
            startDate = Date.parse(startDate).toString('yyyy/MM/dd');

            var endDate = $('#endDate').val();
            endDate = Date.parse(endDate).toString('yyyy/MM/dd');

            gaugeManager = {
                gaugeGuid: $('#gaugeGuidHidden').val(),
                startDate: startDate,
                endDate: endDate,
                active: $('#active').is(':checked'),
                lwl: $('#lwlPresent').is(':checked'),
                hwl: $('#hwlPresent').is(':checked'),
                levelGeo: $('#levelGeo').is(':checked'),
                zeroDatum: $('#zeroDatum').val(),
                gaugeTypeManager: gaugeTypeManager,
                gaugeFrequencyTypeManager: gaugeFrequencyTypeManager,
                gaugeSeasonTypeManager: gaugeSeasonTypeManager
            }
            assetManager.gaugeManager = gaugeManager;
            updateAssetWithGauge(assetManager);
            break;

        case 'POR':
            porcupineTypeManager = {
                porcupineTypeGuid: $('#porcupineTypeSelect').val(),
            }
            porcupineMaterialTypeManager = {
                porcupineMaterialTypeGuid: $('#materialTypeSelect').val(),
            }

            porcupineManager = {
                porcupineGuid: $('#porcupineGuidHidden').val(),
                screenLength: $('#screenLength').val(),
                spacingAlongScreen: $('#spacingAlongScreen').val(),
                screenRows: $('#screenRows').val(),
                numberOfLayers: $('#noOfLayers').val(),
                memberLength: $('#mamberLength').val(),
                lengthPorcupine: $('#lengthPorcupine').val(),
                porcupineTypeManager: porcupineTypeManager,
                porcupineMaterialTypeManager: porcupineMaterialTypeManager,
            }
            assetManager.porcupineManager = porcupineManager;
            updateAssetWithPorcupine(assetManager);
            break;

        case 'REG':
            regulatorTypeManager = {
                regulatorTypeGuid: $('#regulatorTypeSelect').val(),
            }
            regulatorGateTypeManager = {
                regulatorGateTypeGuid: $('#regulatorGateTypeSelect').val(),
            }

            regulatorManager = {
                regulatorGuid: $('#regulatorGuidHidden').val(),
                ventNumberRegulator: $('#numberOfVentsRegulator').val(),
                ventHeightRegulator: $('#ventHeightRegulator').val(),
                ventWidthRegulator: $('#ventWidthRegulator').val(),
                regulatorTypeManager: regulatorTypeManager,
                regulatorGateTypeManager: regulatorGateTypeManager,
            }
            assetManager.regulatorManager = regulatorManager;
            updateAssetWithRegulator(assetManager);
            break;

        case 'REV':
            revetmentTypeManager = {
                revetTypeGuid: $('#revetmentTypeSelect').val(),
            }
            revetmentRiverprotTypeManager = {
                riverprotTypeGuid: $('#riverProtectionTypeSelect').val(),
            }
            revetmentWaveprotTypeManager = {
                waveprotTypeGuid: $('#waveProtectionTypeSelect').val(),
            }

            revetmentManager = {
                revetmentGuid: $('#revetmentGuidHidden').val(),
                lengthRevetment: $('#lengthRevetment').val(),
                plainWidth: $('#plainWidth').val(),
                slopeRevetment: $('#slopeRevetment').val(),
                revetmentTypeManager: revetmentTypeManager,
                revetmentRiverprotTypeManager: revetmentRiverprotTypeManager,
                revetmentWaveprotTypeManager: revetmentWaveprotTypeManager
            }
            assetManager.revetmentManager = revetmentManager;
            updateAssetWithRevetment(assetManager);
            break;

        case 'SLU':
            sluiceTypeManager = {
                sluiceTypeGuid: $('#sluiceTypeSelect').val(),
            }

            sluiceManager = {
                sluiceGuid: $('#sluiceGuidHidden').val(),
                ventNumberSluice: $('#NoOfVentsSluice').val(),
                ventDiameterSluice: $('#ventDiameterSluice').val(),
                ventHeightSluice: $('#ventHeightSluice').val(),
                ventWidthSluice: $('#ventWidthSluice').val(),
                sluiceTypeManager: sluiceTypeManager
            }

            assetManager.sluiceManager = sluiceManager;
            updateAssetWithSluice(assetManager);
            break;

        case 'SPU':
            spurConstructionTypeManager = {
                constructionTypeGuid: $('#constructionTypeSelect').val(),
            }
            spurRevetTypeManager = {
                spurRevetTypeGuid: $('#spurRevetTypeSelect').val(),
            }
            spurShapeTypeManager = {
                shapeTypeGuid: $('#shapeTypeSelect').val(),
            }

            spurManager = {
                spurGuid: $('#spurGuidHidden').val(),
                orientation: $('#orientation').val(),
                lengthSpur: $('#lengthSpur').val(),
                widthSpur: $('#widthSpur').val(),
                spurConstructionTypeManager: spurConstructionTypeManager,
                spurRevetTypeManager: spurRevetTypeManager,
                spurShapeTypeManager: spurShapeTypeManager
            }
            assetManager.spurManager = spurManager;
            updateAssetWithSpur(assetManager);
            break;

        case 'TRN':
            turnoutTypeManager = {
                turnoutTypeGuid: $('#turnoutTypeSelect').val(),
            }
            turnoutGateTypeManager = {
                turnoutGateTypeGuid: $('#turnoutGateTypeSelect').val(),
            }

            turnoutManager = {
                turnoutGuid: $('#turnoutGuidHidden').val(),
                inletboxWidth: $('#inletBoxWidth').val(),
                inletboxLength: $('#inletBoxLength').val(),
                outletNumber: $('#outletNo').val(),
                outletWidth: $('#outletWidth').val(),
                outletHeight: $('#outletHeight').val(),
                turnoutTypeManager: turnoutTypeManager,
                turnoutGateTypeManager: turnoutGateTypeManager
            }
            assetManager.turnoutManager = turnoutManager;
            updateAssetWithTurnout(assetManager);
            break;

        case 'WEI':
            weirTypeManager = {
                weirTypeGuid: $('#weirTypeSelect').val(),
            }

            weirManager = {
                weirGuid: $('#weirGuidHidden').val(),
                invertLevel: $('#invertLevel').val(),
                crestTopLevel: $('#crestTopLevel').val(),
                crestTopWidth: $('#crestTopWidth').val(),
                weirTypeManager: weirTypeManager
            }

            assetManager.weirManager = weirManager;
            updateAssetWithWeir(assetManager);
            break;


        default: document.write("Unknown grade<br />");
    }

    $('#status').delay(200).fadeOut();
    $('#preloader').delay(250).fadeOut('slow');

    return ret;
}

stringIsNullOrEmpty = function (value) {
    return (value == null || value === "");
}

updateAssetWithBridge = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithBridge",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
            
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithCanal = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithCanal",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithCulvert = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithCulvert",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithDrainage = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithDrainage",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithDropStructure = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithDropStructure",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithEmbankment = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithEmbankment",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithGauge = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithGauge",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithPorcupine = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithPorcupine",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithRegulator = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithRegulator",
        data: '{"assetManager":' + assetManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithRevetment = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithRevetment",
        data: '{"assetManager":' + assetManagerJsonString + '}',       
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithSluice = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithSluice",
        data: '{"assetManager":' + assetManagerJsonString + '}',        
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithSpur = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithSpur",
        data: '{"assetManager":' + assetManagerJsonString + '}',       
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithTurnout = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithTurnout",
        data: '{"assetManager":' + assetManagerJsonString + '}',       
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

updateAssetWithWeir = function (assetManager) {
    var assetManagerJsonString = JSON.stringify(assetManager);

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "EditAsset.aspx/UpdateAssetWithWeir",
        data: '{"assetManager":' + assetManagerJsonString + '}',        
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}