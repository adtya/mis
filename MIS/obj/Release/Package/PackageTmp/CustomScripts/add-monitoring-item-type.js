﻿$(document).ready(function () {

    $("#addMonitoringItemTypeBtn").on('click', function (e) {
        $.ajax({
            type: "Post",
            async: false,
            url: "SystemMaintain.aspx/GetAssetType",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {                
                createAddMonitoringItemTypeForm();
                showAssetTypeListOptionsToAddItemType('assetTypeListToAddItemType', response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });     
    });

    $('#addMonitoringItemTypePopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {            
            monitoringItemTypeName: {
                required: true,
                noSpace: true
            },
            assetTypeListToAddItemType: "required"
        }
    });   

});

createAddMonitoringItemTypeForm = function () {
    $("#addMonitoringItemTypePopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Create Item Type</h2>';

    html += '<div class="panel-body">';
    html += '<!--start form--><!--add form action as needed-->';
    html += '<form id="addMonitoringItemTypePopupForm" name="addMonitoringItemTypePopupForm" role="form" class="form form-horizontal" method="post">';
    html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="assetTypeList" id="assetTypeListLabel" class="control-label pull-left">Select Asset Type</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control" name="assetTypeListToAddItemType" id="assetTypeListToAddItemType">';   
    html +='</select>';
    html += '</div>';
    html += '</div>';                                                                
    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="monitoringItemTypeName" id="monitoringItemTypeNameLabel" class="control-label pull-left">Item Type</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<input type="text" id="monitoringItemTypeName" name="monitoringItemTypeName" placeholder="Item Type" class="form-control text-capitalize" value="" style="width:100%;" />';
    html += '</div>';
    html += '</div>';  
      

    html += '<div class="form-group">';
    html += '<input id="saveMonitoringItemTypeBtn" class="btn btn-lg btn-primary btn-block" value="Add Item Type" type="submit" />';
    html += '</div>';

    html += '</fieldset>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#addMonitoringItemTypePopupModal .modal-body").append(html);    

    $('#addMonitoringItemTypePopupModal').modal('show');

    $('#saveMonitoringItemTypeBtn').on('click', function (e) {
        e.preventDefault();
        if ($("#addMonitoringItemTypePopupForm").valid()) {
            $("#preloader").show();
            $("#status").show();

            var check = saveMonitoringItemTypeData();

            if (check == 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.confirm({
                    title: '',
                    message: 'Your data is saved successfully.<br>Do you want to create more item types?',
                    buttons: {
                        'cancel': {
                            label: 'No',
                            className: 'btn-default pull-left'
                        },
                        'confirm': {
                            label: 'Yes',
                            className: 'btn-default pull-right'
                        }
                    },
                    callback: function (result) {
                        if (result) {

                        } else {
                            $('#addMonitoringItemTypePopupModal').modal('hide');
                        }
                    }
                });
            } else if (check == 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data cannot be saved.\nPlease contact your database administrator.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'DBAHome.aspx';
                });
            }
        }

    });
}

showAssetTypeListOptionsToAddItemType = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Asset Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].AssetTypeCode + "/" + arr[i].AssetTypeName, arr[i].AssetTypeGuid);
    }
}

saveMonitoringItemTypeData = function () {

    var assetTypeGuid = $('#assetTypeListToAddItemType').val();
    var monitoringItemTypeName = $('#monitoringItemTypeName').val();

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "SystemMaintain.aspx/SaveMonitoringItemType",
        data: '{"monitoringItemTypeName":"' + monitoringItemTypeName + '","assetTypeGuid":"' + assetTypeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });

    return retValue;
}