﻿(function( factory ) {
    "use strict";

    if ( typeof define === 'function' && define.amd ) {
        // AMD
        define( ['jquery'], function ( $ ) {
            return factory( $, window, document );
        } );
    }
    else if ( typeof exports === 'object' ) {
        // CommonJS
        module.exports = function (root, $) {
            if ( ! root ) {
                // CommonJS environments without a window global must pass a
                // root. This will give an error otherwise
                root = window;
            }

            if ( ! $ ) {
                $ = typeof window !== 'undefined' ? // jQuery's factory checks for a global window
					require('jquery') :
					require('jquery')( root );
            }

            return factory( $, root, root.document );
        };
    }
    else {
        // Browser
        factory( jQuery, window, document );
    }
}
(function( $, window, document, undefined ) {
    "use strict";

    var criteriaEnum = {
        'Indian': 1,
        'Foreign': 2
    };

    Object.freeze(criteriaEnum);

    var numToWordConverter;

    /**
     * Locally scoped variables.
     */
    var _htPunctuation;
    var _dictStaticSuffix;
    var _dictStaticPrefix;
    var _dictHelpNotation;
    var _native;

    numToWordConverter = function (native) {
        _htPunctuation = [];
        _dictStaticSuffix = [];
        _dictStaticPrefix = [];
        _dictHelpNotation = [];
        _native = native;
        _loadStaticPrefix();
        _loadStaticSuffix();
        _loadHelpofNotation();
    }

    var _isNumeric = function(valueInNumeric){
        var isNumberFine = true;
        for(var char in valueInNumeric){
            if(!(char >= '0' && char <= '9')){
                isNumberFine = false;
            }
        }
        return isNumberFine;
    }

    var _returnHashtableValue = function(){
        var strFinalString = '';
        for(var i = (_htPunctuation.length - 1); i >= 0; i--){
            if(_getWordConversion((_htPunctuation[i]).ToString()) != ''){
                strFinalString = strFinalString + _getWordConversion((_htPunctuation[i]).ToString()) + _staticPrefixFind((i).ToString());
            }
        }
        return strFinalString;
    }

    var _reverseHashTable = function(){
        var htTemp = [];
        for (var item in _htPunctuation){
            htTemp.push({key: item.key, value: _reverse(item.value.toString()) });
        }
        _htPunctuation = [];
        _htPunctuation = htTemp;
    }

    var _loadStaticSuffix = function(){
        _dictStaticSuffix.push({key: 1, value: 'One '});
        _dictStaticSuffix.push({key: 2, value: 'Two '});
        _dictStaticSuffix.push({key: 3, value: 'Three '});
        _dictStaticSuffix.push({key: 4, value: 'Four '});
        _dictStaticSuffix.push({key: 5, value: 'Five '});
        _dictStaticSuffix.push({key: 6, value: 'Six '});
        _dictStaticSuffix.push({key: 7, value: 'Seven '});
        _dictStaticSuffix.push({key: 8, value: 'Eight '});
        _dictStaticSuffix.push({key: 9, value: 'Nine '});
        _dictStaticSuffix.push({key: 10, value: 'Ten '});
        _dictStaticSuffix.push({key: 11, value: 'Eleven '});
        _dictStaticSuffix.push({key: 12, value: 'Twelve '});
        _dictStaticSuffix.push({key: 13, value: 'Thirteen '});
        _dictStaticSuffix.push({key: 14, value: 'Fourteen '});
        _dictStaticSuffix.push({key: 15, value: 'Fifteen '});
        _dictStaticSuffix.push({key: 16, value: 'Sixteen '});
        _dictStaticSuffix.push({key: 17, value: 'Seventeen '});
        _dictStaticSuffix.push({key: 18, value: 'Eighteen '});
        _dictStaticSuffix.push({key: 19, value: 'Nineteen '});
        _dictStaticSuffix.push({key: 20, value: 'Twenty '});
        _dictStaticSuffix.push({key: 30, value: 'Thirty '});
        _dictStaticSuffix.push({key: 40, value: 'Fourty '});
        _dictStaticSuffix.push({key: 50, value: 'Fifty '});
        _dictStaticSuffix.push({key: 60, value: 'Sixty '});
        _dictStaticSuffix.push({key: 70, value: 'Seventy '});
        _dictStaticSuffix.push({key: 80, value: 'Eighty '});
        _dictStaticSuffix.push({key: 90, value: 'Ninty '});

    }

    var _loadStaticPrefix = function(){
        if (_native == criteriaEnum.Indian){
            _dictStaticPrefix.push({key: 2, value: 'Thousand '});
            _dictStaticPrefix.push({key: 3, value: 'Lac '});
            _dictStaticPrefix.push({key: 4, value: 'Crore '});
            _dictStaticPrefix.push({key: 5, value: 'Arab '});
            _dictStaticPrefix.push({key: 6, value: 'Kharab '});
            _dictStaticPrefix.push({key: 7, value: 'Neel '});
            _dictStaticPrefix.push({key: 8, value: 'Padma '});
            _dictStaticPrefix.push({key: 9, value: 'Shankh '});
            _dictStaticPrefix.push({key: 10, value: 'Maha-shankh '});
            _dictStaticPrefix.push({key: 11, value: 'Ank '});
            _dictStaticPrefix.push({key: 12, value: 'Jald '});
            _dictStaticPrefix.push({key: 13, value: 'Madh '});
            _dictStaticPrefix.push({key: 14, value: 'Paraardha '});
            _dictStaticPrefix.push({key: 15, value: 'Ant '});
            _dictStaticPrefix.push({key: 16, value: 'Maha-ant '});
            _dictStaticPrefix.push({key: 17, value: 'Shisht '});
            _dictStaticPrefix.push({key: 18, value: 'Singhar '});
            _dictStaticPrefix.push({key: 19, value: 'Maha-singhar '});
            _dictStaticPrefix.push({key: 20, value: 'Adant-singhar '});
        }
        if (_native == criteriaEnum.Foreign)
        {
            _dictStaticPrefix.push({key: 1, value: 'Thousand '});
            _dictStaticPrefix.push({key: 2, value: 'Million '});
            _dictStaticPrefix.push({key: 3, value: 'Billion '});
            _dictStaticPrefix.push({key: 4, value: 'Trillion '});
            _dictStaticPrefix.push({key: 5, value: 'Quadrillion '});
            _dictStaticPrefix.push({key: 6, value: 'Quintillion '});
            _dictStaticPrefix.push({key: 7, value: 'Sextillion '});
            _dictStaticPrefix.push({key: 8, value: 'Septillion '});
            _dictStaticPrefix.push({key: 9, value: 'Octillion '});
            _dictStaticPrefix.push({key: 10, value: 'Nonillion '});
            _dictStaticPrefix.push({key: 11, value: 'Decillion '});
            _dictStaticPrefix.push({key: 12, value: 'Undecillion '});
            _dictStaticPrefix.push({key: 13, value: 'Duodecillion '});
        }
    }

    var _loadHelpofNotation = function (){
        if (_native == criteriaEnum.Indian) {
            _dictHelpNotation.push({key: 2, value: '=1,000 (3 Trailing Zeros)'});
            _dictHelpNotation.push({key: 3, value: '=1,00,000 (5 Trailing Zeros)'});
            _dictHelpNotation.push({key: 4, value: '=1,00,00,000 (7 Trailing Zeros)'});
            _dictHelpNotation.push({key: 5, value: '=1,00,00,00,000 (9 Trailing Zeros)'});
            _dictHelpNotation.push({key: 6, value: '=1,00,00,00,00,000 (11 Trailing Zeros)'});
            _dictHelpNotation.push({key: 7, value: '=1,00,00,00,00,00,000 (13 Trailing Zeros)'});
            _dictHelpNotation.push({key: 8, value: '=1,00,00,00,00,00,00,000 (15 Trailing Zeros)'});
            _dictHelpNotation.push({key: 9, value: '=1,00,00,00,00,00,00,00,000 (17 Trailing Zeros)'});
            _dictHelpNotation.push({key: 10, value: '=1,00,00,00,00,00,00,00,00,000 (19 Trailing Zeros)'});
            _dictHelpNotation.push({key: 11, value: '=1,00,00,00,00,00,00,00,00,00,000 (21 Trailing Zeros)'});
            _dictHelpNotation.push({key: 12, value: '=1,00,00,00,00,00,00,00,00,00,00,000 (23 Trailing Zeros)'});
            _dictHelpNotation.push({key: 13, value: '=1,00,00,00,00,00,00,00,00,00,00,00,000 (25 Trailing Zeros)'});
            _dictHelpNotation.push({key: 14, value: '=1,00,00,00,00,00,00,00,00,00,00,00,00,000 (27 Trailing Zeros)'});
            _dictHelpNotation.push({key: 15, value: '=1,00,00,00,00,00,00,00,00,00,00,00,00,00,000 (29 Trailing Zeros)'});
            _dictHelpNotation.push({key: 16, value: '=1,00,00,00,00,00,00,00,00,00,00,00,00,00,00,000 (31 Trailing Zeros)'});
            _dictHelpNotation.push({key: 17, value: '=1,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,000 (33 Trailing Zeros)'});
            _dictHelpNotation.push({key: 18, value: '=1,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,000 (35 Trailing Zeros)'});
            _dictHelpNotation.push({key: 19, value: '=1,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,000 (37 Trailing Zeros)'});
            _dictHelpNotation.push({key: 20, value: '=1,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,000 (39 Trailing Zeros)'});
        }
        else{
            _dictHelpNotation.push({key: 1, value: '=1,000 (3 Trailing Zeros)'});
            _dictHelpNotation.push({key: 2, value: '=1,000,000 (6 Trailing Zeros)'});
            _dictHelpNotation.push({key: 3, value: '=1,000,000,000 (9 Trailing Zeros)'});
            _dictHelpNotation.push({key: 4, value: '=1,000,000,000,000 (12 Trailing Zeros)'});
            _dictHelpNotation.push({key: 5, value: '=1,000,000,000,000,000 (15 Trailing Zeros)'});
            _dictHelpNotation.push({key: 6, value: '=1,000,000,000,000,000,000 (18 Trailing Zeros)'});
            _dictHelpNotation.push({key: 7, value: '=1,000,000,000,000,000,000,000 (21 Trailing Zeros)'});
            _dictHelpNotation.push({key: 8, value: '=1,000,000,000,000,000,000,000,000 (24 Trailing Zeros)'});
            _dictHelpNotation.push({key: 9, value: '=1,000,000,000,000,000,000,000,000,000 (27 Trailing Zeros)'});
            _dictHelpNotation.push({key: 10, value: '=1,000,000,000,000,000,000,000,000,000,000 (30 Trailing Zeros)'});
            _dictHelpNotation.push({key: 11, value: '=1,000,000,000,000,000,000,000,000,000,000,000 (33 Trailing Zeros)'});
            _dictHelpNotation.push({key: 12, value: '=1,000,000,000,000,000,000,000,000,000,000,000,000 (36 Trailing Zeros)'});
            _dictHelpNotation.push({key: 13, value: '=1,000,000,000,000,000,000,000,000,000,000,000,000,000 (39 Trailing Zeros)'});
            _dictHelpNotation.push({key: 14, value: '=1,000,000,000,000,000,000,000,000,000,000,000,000,000,000 (42 Trailing Zeros)'});
        }
    }

    var _insertIntoPunctuationTable = function(strValue){
        var j = 0;

        if (_native == criteriaEnum.Indian){
            _htPunctuation.push({key: 1, value: strValue.substr(0, 3).toString() });
            j = 2;
            for (var i = 3; i < strValue.length; i = i + 2){
                if (strValue.substr(i).length > 0){
                    if (strValue.substr(i).length >= 2){
                        _htPunctuation.push({key: j, value: strValue.substr(i, 2).toString() });
                    }
                    else{
                        _htPunctuation.push({key: j, value: strValue.substr(i, 1).toString() });
                    }
                }
                else {
                    break;
                }
                j++;
            }
        }

        if (_native == criteriaEnum.Foreign){
            for (var i = 0; i < strValue.length; i = i + 3){
                if (strValue.substr(i).length > 0){
                    if (strValue.substr(i).length >= 3){
                        _htPunctuation.push({key: j, value: strValue.substr(i, 3).toString() });
                    }
                    else
                    {
                        _htPunctuation.push({key: j, value: strValue.substr(i).toString() });
                    }
                }
                j++;
            }
        }
    }

    var _reverseString = function(strValue){
        var reversedStr = '';

        for (var i = 0; i < strValue.length; i++) {
            reversedStr = strValue[i] + reversedStr;
        }

        return reversedStr;
    }

    var _getWordConversion = function(inputNumber) {
        var toReturnWord = '';
        
        if (inputNumber.length <= 3 && inputNumber.length > 0) {
            if (inputNumber.length === 3) {
                if (parseInt(inputNumber.substr(0, 1)) > 0) {
                    toReturnWord = toReturnWord + _staticSuffixFind(inputNumber.substr(0, 1)) + "Hundread ";
                }

                var tempString = _staticSuffixFind(inputNumber.substr(1, 2));

                if (tempString === '') {
                    toReturnWord = toReturnWord + _staticSuffixFind(inputNumber.substr(1, 1) + "0");
                    toReturnWord = toReturnWord + _staticSuffixFind(inputNumber.substr(2, 1));
                }

                toReturnWord = toReturnWord + tempString;
            }
            
            if (inputNumber.length === 2) {
                var tempString = _staticSuffixFind(inputNumber.substr(0, 2));

                if (tempString === '') {
                    toReturnWord = toReturnWord + _staticSuffixFind(inputNumber.substr(0, 1) + "0");
                    toReturnWord = toReturnWord + _staticSuffixFind(inputNumber.substr(1, 1));
                }

                toReturnWord = toReturnWord + tempString;
            }

            if (inputNumber.length === 1) {
                toReturnWord = toReturnWord + _staticSuffixFind(inputNumber.substr(0, 1));
            }

        }

        return toReturnWord;

    }

    var _staticSuffixFind = function (numberKey) {
        var valueFromNumber = '';

        var keyFound = false;
        for(var i = 0; i < _dictStaticSuffix.length; i++) {
            if (_dictStaticSuffix[i].key === parseInt(numberKey)) {
                keyFound = true;
                valueFromNumber = _dictStaticSuffix[i].value;
                break;
            }
        }

        return valueFromNumber;
    }

    var _staticPrefixFind = function (numberKey) {
        var valueFromNumber = '';

        var keyFound = false;
        for(var i = 0; i < _dictStaticPrefix.length; i++) {
            if (_dictStaticPrefix[i].key === parseInt(numberKey)) {
                keyFound = true;
                valueFromNumber = _dictStaticPrefix[i].value;
                break;
            }
        }

        return valueFromNumber;
    }

    var _staticHelpNotationFind = function (numberKey) {
        var helpText = '';

        var keyFound = false;
        for(var i = 0; i < _dictHelpNotation.length; i++) {
            if (_dictHelpNotation[i].key === parseInt(numberKey)) {
                keyFound = true;
                helpText = _dictHelpNotation[i].value;
                break;
            }
        }

        return helpText;
    }

    var convertToWord = function (value) {
        var convertedString = '';
        
        if (!(value.toString().length > 40))
        {
            if (_isNumeric(value.toString()))
            {
                try
                {
                    var strValue = _reverseString(value);
                    switch (strValue.length)
                    {
                        case 1:
                            if (parseInt(strValue.toString()) > 0)
                                convertedString = _getWordConversion(value);
                            else
                                convertedString = 'Zero ';
                            break;
                        case 2:
                            convertedString = _getWordConversion(value);
                            ; break;
                        default:
                            _insertIntoPunctuationTable(strValue);
                            _reverseHashTable();
                            convertedString = _returnHashtableValue();
                            break;
                    }
                }
                catch (exception)
                {
                    convertedString = 'Unexpected Error Occured <br/>';
                    convertedString += exception.Message;
                }
            }
            else
            {
                convertedString = 'Please Enter Numbers Only, Decimal Values Are not supported';
            }
        }
        else
        {
            convertedString = 'Please Enter Value in Less Then or Equal to 40 Digit';
        }
        return convertedString;
    }

    $.fn.numToWordConverter = numToWordConverter;

    //DataTable.$ = $;

    return $.fn.numToWordConverter;
    
}));