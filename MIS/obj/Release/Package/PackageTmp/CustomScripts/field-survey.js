﻿var assetCodeDataForFieldSurvey;
var routineWorkItems = "";
var fieldSurveyData = "";
var routineWorkItemDataForFieldSurvey;
var routineWorkItemRateDataForFieldSurvey;
var quantity;
var fieldSurveyTable;
var unitRate;


$(document).ready(function () {

    $('#assetCodeList').multiselect({
        maxHeight: 150,
        numberDisplayed: 1,
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',

        onChange: function (element, checked) {

            $(element).closest('.multiselect').valid();

            if (checked === true) {
                var selectedAssetGuid = $(element).val();
                var selectedAssetText = $(element).text();

                if (selectedAssetGuid === '') {
                    $('#assetTypeGuid').val("");
                    $('#assetTypeName').val("");
                    $('#assetCode').val("");
                    $('#assetName').val("");                   
                } else {
                    getAssetCodeDataForFieldSurvey();

                    $('#assetTypeGuid').val(assetCodeDataForFieldSurvey.AssetTypeManager.AssetTypeGuid);
                    $('#assetTypeName').val(assetCodeDataForFieldSurvey.AssetTypeManager.AssetTypeName);
                    $('#assetCode').val(assetCodeDataForFieldSurvey.AssetCode);
                    $('#assetName').val(assetCodeDataForFieldSurvey.AssetName);
                    getRoutineWorkItemsForFieldSurvey();
                    getFieldSurveyData();
                    createFieldSurveyCostTable(routineWorkItems);
                }
                $(document).click();
            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

});

createFieldSurveyCostTable = function (routineWorkItems) {

    $("#fieldSurveyCostFieldsetPanel").html("");

    var html = '<div class="row">';

    html += '<div class="col-sm-12 col-md-12">';

    html += '<div class="col-sm-6 col-md-6">';

    html += '<div class="form-group">';
    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    html += '<label for="periodicWorkItemList" id="periodicWorkItemListLabel" class="control-label pull-left">Work Item</label>';
    html += '</div>';
    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
    html += '<select class="form-control multiselect" name="periodicWorkItemList" id="periodicWorkItemList" onfocus="dbaFocusFunction(this.id)" onblur="dbaBlurFunction(this.id)">';
    html += '<option value="" selected="selected">--Select Work Item--</option>';

    if (fieldSurveyData.length === 0) {
    for (var i = 0; i < routineWorkItems.length; i++) {
        html += '<option value="' + routineWorkItems[i].RoutineWorkItemGuid + '">';
        html += routineWorkItems[i].RoutineWorkItemTypeCode;
        html += '</option>';
        }

    }
    else {
        var a = [];

        for (var i = 0; i < fieldSurveyData.length; i++) {
            a.push(fieldSurveyData[i].RoutineWorkItemManager.RoutineWorkItemGuid);
        }

        for (var i = 0; i < routineWorkItems.length; i++) {

            if ($.inArray(routineWorkItems[i].RoutineWorkItemGuid, a) === -1) {
                html += '<option value="' + routineWorkItems[i].RoutineWorkItemGuid + '">';
                html += routineWorkItems[i].RoutineWorkItemTypeCode;
                html += '</option>';
            }
        }
    }

    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div id="table-container" style="padding:1%;">';

    html += '<div class="table-responsive">';
    html += '<table id="fieldSurveyDataList" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">';

    html += '<thead>';
    html += '<tr>';
    html += '<th>' + '#' + '</th>';
    html += '<th>' + 'Field Survey Guid' + '</th>';
    html += '<th>' + 'Work Item Guid' + '</th>';
    html += '<th>' + 'Work Item' + '</th>';
    html += '<th>' + 'Short Description' + '</th>';
    html += '<th>' + 'Unit Rate(Rs/Cum)' + '</th>';
    html += '<th>' + 'Quantity (Cum)' + '</th>';
    html += '<th>' + 'Cost (Rs)' + '</th>';
    html += '<th>' + 'Operation' + '</th>';
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';

    if (fieldSurveyData.length !== 0) {
        for (var i = 0; i < fieldSurveyData.length; i++) {
            html += '<tr>';
            html += '<td>' + '' + '</td>';
            html += '<td>' + fieldSurveyData[i].FieldSurveyGuid + '</td>';
            html += '<td>' + fieldSurveyData[i].RoutineWorkItemManager.RoutineWorkItemGuid + '</td>';
            html += '<td>' + fieldSurveyData[i].RoutineWorkItemManager.RoutineWorkItemTypeCode + '</td>';
            html += '<td>' + fieldSurveyData[i].RoutineWorkItemManager.ShortDescription + '</td>';
            html += '<td>' + fieldSurveyData[i].RoutineWorkItemManager.RoutineWorkItemRateManagerList[0].RoutineWorkItemRate.Amount + '</td>';
            html += '<td>' + fieldSurveyData[i].EarthVolumeQuantity + '</td>';
            html += '<td>' + fieldSurveyData[i].FieldSurveyWorkItemCost.Amount + '</td>';
            html += '<td>' + '<a href="#" class="editFieldSurveyBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteFieldSurveyBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>'; + '</td>';
            html += '</tr>';
          
        }
    }

    html += '</tbody>';

    html += '</table>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</form><!--/end form-->';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#fieldSurveyCostFieldsetPanel").append(html);

    $('#fieldSurveyCostFieldset').show();

    $('#periodicWorkItemList').multiselect({
        maxHeight: 150,
        numberDisplayed: 1,
        nonSelectedText: '--Select Work Item--',
        disableIfEmpty: true,
        buttonClass: 'btn btn-default',
        buttonContainer: '<div class="btn-group btn-group-justified" />',

        onChange: function (element, checked) {

            $(element).closest('.multiselect').valid();

            if (checked === true) {                      

                $(document).click();
            }
        },
        templates: {
            button: '<a type="button" class="multiselect dropdown-toggle multiselect-title-text" style="width:95%;" data-toggle="dropdown"><span class="multiselect-selected-text"></span></a><a type="button" class="multiselect dropdown-toggle" style="width:5%;" data-toggle="dropdown"><span class="caret"></span></a>'
        }
    });

    fieldSurveyTableRelatedFunctions();    
}

fieldSurveyTableRelatedFunctions = function () {
    var fieldSurveyRowEditing = null;

    var fieldSurveyTable = $('#fieldSurveyDataList').DataTable({
        responsive: true,
        paging: false,
        info: false,
        searching: false,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [2],
                visible: false,
                searchable: false
            }
        ],
        dom: '<"top"<"pull-left"B><f>>rt<"bottom"i<"pull-left"l><p>><"clear">',
        buttons: [
            {
                text: '+ Append Work Item',
                className: 'btn-success addWorkItemBtn ',
                action: function (e, dt, node, config) {
                    e.preventDefault();

                    var routineWorkItemSelectGuid = $('#periodicWorkItemList').val();
                    var routineWorkItemSelect = $('#periodicWorkItemList option:selected').text();                    
                    if (routineWorkItemSelectGuid == '') {

                    }
                    else {
                        //fieldSurveyTable.button(['.addWorkItemBtn']).enable();
                        getRoutineWorkItemDataForFieldSurvey(routineWorkItemSelectGuid);
                        getRoutineWorkItemRateDataForFieldSurvey(routineWorkItemSelectGuid);
                        if (routineWorkItemRateDataForFieldSurvey.RoutineWorkItemRate.Amount === 0)
                        {
                            unitRate = 0;
                        }
                        else{
                            unitRate = routineWorkItemRateDataForFieldSurvey.RoutineWorkItemRate.Amount;
                        }                            
                        var newFieldSurveyRow = fieldSurveyTable.row.add([
                          '',
                          '',
                          routineWorkItemSelectGuid,
                          routineWorkItemSelect,
                          routineWorkItemDataForFieldSurvey.ShortDescription,
                          unitRate,
                          '<input id="quantity" name="quantity" class="form-control quantity" type="text"/>',                         
                          '<input id="fieldSurveyCost" name="fieldSurveyCost" class="form-control fieldSurveyCost" type="text" readonly="readonly" />',
                          '<a href="#" class="saveFieldSurveyBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelFieldSurveyBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>'
                        ]).draw();

                        var newFieldSurveyRowNode = newFieldSurveyRow.node();

                        fieldSurveyRowEditing = newFieldSurveyRowNode;

                        fieldSurveyTable.button(['.addWorkItemBtn']).disable();

                        $('option[value="' + routineWorkItemSelectGuid + '"]', $('#periodicWorkItemList')).remove();

                        $('#periodicWorkItemList').multiselect('rebuild');             

                    }
                }
            }
        ]   

    });

   

    fieldSurveyTable.on('order.dt search.dt', function () {
        fieldSurveyTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $('#fieldSurveyCostFieldsetPanel table#fieldSurveyDataList').on('click', 'a.saveFieldSurveyBtn', function (e) {

        e.preventDefault();

        var selectedRow = $(this).parents('tr')[0];

        routineWorkItemSelectGuid = fieldSurveyTable.row(selectedRow).data()[2];

        routineWorkItemSelect = fieldSurveyTable.row(selectedRow).data()[3];

        var availableInputs = $('input', selectedRow);

        quantity = availableInputs[0].value;   

        $('#periodicWorkItemList').multiselect('rebuild');

        var fieldSurveySavedGuid = saveFieldSurveyData(fieldSurveyTable, selectedRow);

        if (jQuery.Guid.IsValid(fieldSurveySavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(fieldSurveySavedGuid.toUpperCase())) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Data is saved successfully.", function () {
                if (fieldSurveyRowEditing !== null && fieldSurveyRowEditing == selectedRow) {
                    /* A different row is being edited - the edit should be cancelled and this row edited */
                    saveNewFieldSurveyRow(fieldSurveyTable, fieldSurveyRowEditing, fieldSurveySavedGuid);
                    fieldSurveyRowEditing = null;
                    fieldSurveyTable.button(['.addWorkItemBtn']).enable();
                }
            });
        } else {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Data cannot be saved.<br>Please contact your database administrator.");
        }

    });

    $('#fieldSurveyCostFieldsetPanel table#fieldSurveyDataList').on('click', 'a.editFieldSurveyBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (fieldSurveyRowEditing !== null && fieldSurveyRowEditing != selectedRow) {

            /* A different row is being edited - the edit should be cancelled and this row edited */

            restoreFieldSurveyRowData(fieldSurveyTable, fieldSurveyRowEditing);
            editFieldSurveyRow(fieldSurveyTable, selectedRow);
            fieldSurveyRowEditing = selectedRow;

            var el = $('#fieldSurveyCostFieldsetPanel input#quantityCell');
            var elemLen = el.val().length;
            el.focus();


            el.setCursorPosition(elemLen);
        }
        else {
            /* No row currently being edited */
            editFieldSurveyRow(fieldSurveyTable, selectedRow);
            fieldSurveyRowEditing = selectedRow;

            var el = $('#fieldSurveyCostFieldsetPanel input#quantityCell');
            var elemLen = el.val().length;
            el.focus();

            el.setCursorPosition(elemLen);
        }
    });

    $('#fieldSurveyCostFieldsetPanel table#fieldSurveyDataList').on('click', 'a.updateFieldSurveyRowBtn', function (e) {
        e.preventDefault();

        $("#preloader").show();
        $("#status").show();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        var fieldSurveyUpdated = updateFieldSurveyData(fieldSurveyTable.row(selectedRow).data()[1]);

        if (fieldSurveyUpdated == 1) {

            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');

            bootbox.alert("Your Data is updated successfully.", function () {
                if (fieldSurveyRowEditing !== null && fieldSurveyRowEditing == selectedRow) {

                    /* A different row is being edited - the edit should be cancelled and this row edited */
                    updateFieldSurveyRow(fieldSurveyTable, fieldSurveyRowEditing);
                    fieldSurveyRowEditing = null;
                    $('#quantityCell').val(fieldSurveyTable.row(selectedRow).data()[5]);

                    //var omRoutineCostValue = $('#serviceLevelForRoutineAnalysis').val() * $('#assetQuantityForRoutineAnalysis').val() * $('#intensityFactorForRoutineAnalysis').val() * $('#unitRateForRoutineAnalysis').val();
                    //$('#omRoutineCost').val(omRoutineCostValue);

                }
            });
        } else if (fieldSurveyUpdated == 0) {
            $('#status').delay(300).fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
        }


    });

    $('#fieldSurveyCostFieldsetPanel table#fieldSurveyDataList').on('click', 'a.deleteFieldSurveyBtn', function (e) {
        e.preventDefault();       

        var selectedRow = $(this).parents('tr')[0];

        var fieldSurveyGuid = fieldSurveyTable.row(selectedRow).data()[1];

        var workItemGuid = fieldSurveyTable.row(selectedRow).data()[2];

        var workItemTyeCode = fieldSurveyTable.row(selectedRow).data()[3];
        
        deleteFieldSurveyData(fieldSurveyGuid);

        fieldSurveyTable.row(selectedRow).remove().draw();

        $('#periodicWorkItemList')
            .append('<option value="' + workItemGuid + '">' + workItemTyeCode + '</option>');

        $('#periodicWorkItemList').multiselect('rebuild');

    });

    $('#fieldSurveyCostFieldsetPanel table#fieldSurveyDataList').on('click', 'a.cancelFieldSurveyBtn', function (e) {
        e.preventDefault();
  
        var selectedRow = $(this).parents('tr')[0];

        var routineWorkItemSelectGuid = fieldSurveyTable.row(selectedRow).data()[2];
        var routineWorkItemSelect = fieldSurveyTable.row(selectedRow).data()[3];

        fieldSurveyTable.row(selectedRow).remove().draw();
    
        $('#periodicWorkItemList')
            .append('<option value="' + routineWorkItemSelectGuid + '">' + routineWorkItemSelect + '</option>')
        ;

        $('#periodicWorkItemList').multiselect('rebuild');

        fieldSurveyTable.button(['.addWorkItemBtn']).enable();

    });

    $('#fieldSurveyCostFieldsetPanel table#fieldSurveyDataList').on('click', 'a.cancelFieldSurveyRowBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (fieldSurveyRowEditing !== null && fieldSurveyRowEditing == selectedRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */

            restoreFieldSurveyRowData(fieldSurveyTable, fieldSurveyRowEditing);
            fieldSurveyRowEditing = null;
        }

    });

    $('#fieldSurveyForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            quantity: {
                number: true
            },
            quantityCell: {
                number: true
            }
        },
    });

    $('#fieldSurveyCostFieldsetPanel table#fieldSurveyDataList').on('click', 'input.fieldSurveyCost', function (e) {

        if ($('input[name = "quantity"]').valid()) {

            var selectedRow = $(this).parents('tr')[0];

            var availableInputs = $('input', selectedRow);

            var unitRate = fieldSurveyTable.row(selectedRow).data()[5]

            var quantityData = availableInputs[0].value;

            fieldSurveyWorkItemCost = unitRate * quantityData;

            $('#fieldSurveyCost').val(fieldSurveyWorkItemCost);
        }        
    });

    $('#fieldSurveyCostFieldsetPanel table#fieldSurveyDataList').on('click', 'input.costCell', function (e) {
        if ($('#fieldSurveyForm').valid()) {

            var selectedRow = $(this).parents('tr')[0];

            var availableInputs = $('input', selectedRow);

            var unitRate = fieldSurveyTable.row(selectedRow).data()[5]

            var quantityData = availableInputs[0].value;

            var fieldSurveyWorkItemCost = unitRate * quantityData;

            $('#costCell').val(fieldSurveyWorkItemCost);
        }       

    });

}

editFieldSurveyRow = function (fieldSurveyTable, selectedRow) {
    var selectedRowData = fieldSurveyTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.

    availableTds[4].innerHTML = '<input type="text" id="quantityCell" name="quantityCell" placeholder=Quantity" class="form-control text-capitalize" value="' + selectedRowData[6] + '">';
    availableTds[5].innerHTML = '<input type="text" id="costCell" name="costCell" placeholder=Cost" class="form-control text-capitalize costCell" value="' + selectedRowData[7] + '" readonly="readonly">';
    availableTds[6].innerHTML = '<a href="#" class="updateFieldSurveyRowBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Update" title="Update"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelFieldSurveyRowBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

restoreFieldSurveyRowData = function (fieldSurveyTable, previousRow) {
    var previousRowData = fieldSurveyTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
        //removeNewWorkItemRateRowCreatedOnCancel(routineWorkItemRateTable, previousRow);
    } else {
        fieldSurveyTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;
    }

}

saveNewFieldSurveyRow = function (fieldSurveyTable, selectedRow, fieldSurveySavedGuid) {

    var selectedRowData = fieldSurveyTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    selectedRowData[1] = fieldSurveySavedGuid;

    selectedRowData[6] = availableInputs[0].value;
    selectedRowData[7] = availableInputs[1].value;
    selectedRowData[8] = '<a href="#" class="editFieldSurveyBtn"><i class="ui-tooltip fa fa-pencil-square-o" style="font-size: 22px;" data-original-title="Edit" title="Edit"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="deleteFieldSurveyBtn"><i class="ui-tooltip fa fa-trash-o" style="font-size: 22px;" data-original-title="Delete" title="Delete"></i></a>';

    fieldSurveyTable.row(selectedRow).data(selectedRowData);

    fieldSurveyTable.on('order.dt search.dt', function () {
        fieldSurveyTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

updateFieldSurveyRow = function (fieldSurveyTable, selectedRow) {
    var selectedRowData = fieldSurveyTable.row(selectedRow).data();

    var availableInputs = $('input', selectedRow);

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    selectedRowData[6] = availableInputs[0].value;
    selectedRowData[7] = availableInputs[1].value;

    fieldSurveyTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

}

getAssetCodeDataForFieldSurvey = function () {
    var selectedAssetGuid = $('#assetCodeList').val();  

    $.ajax({
        type: "POST",
        async: false,
        url: "FieldSurvey.aspx/GetAssetCodeDataForFieldSurvey",
        data: '{"assetGuid":"' + selectedAssetGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            assetCodeDataForFieldSurvey = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });

}

getRoutineWorkItemsForFieldSurvey = function () {    
    $.ajax({
        type: "POST",
        async: false,
        url: "FieldSurvey.aspx/GetRoutineWorkItemsForFieldSurvey",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            routineWorkItems = response.d;           
        },
        failure: function (result) {
            alert("Error");
        }
    });

}

getFieldSurveyData = function () {

    var selectedAssetGuid = $('#assetCodeList').val();
    var circleGuid =$('#circleGuid').val();
    var fiscalYear = $('#fiscalYear').val();
    $.ajax({
        type: "POST",
        async: false,
        url: "FieldSurvey.aspx/GetFieldSurveyData",
        data: '{"assetGuid":"' + selectedAssetGuid + '","circleGuid":"' + circleGuid + '","fiscalYear":"' + fiscalYear + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            fieldSurveyData = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });

}

getRoutineWorkItemDataForFieldSurvey = function (selectedRoutineWorkItemGuid) { 

    $.ajax({
        type: "POST",
        async: false,
        url: "FieldSurvey.aspx/GetRoutineWorkItemDataForFieldSurvey",
        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            routineWorkItemDataForFieldSurvey = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });

}

getRoutineWorkItemRateDataForFieldSurvey = function (selectedRoutineWorkItemGuid) {

    $.ajax({
        type: "POST",
        async: false,
        url: "FieldSurvey.aspx/GetRoutineWorkRateDataForFieldSurvey",
        data: '{"routineWorkItemGuid":"' + selectedRoutineWorkItemGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            routineWorkItemRateDataForFieldSurvey = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });

}

saveFieldSurveyData = function (fieldSurveyTable, selectedRow) {

    var assetManager = {
        assetGuid : $('#assetCodeList').val()
    }
   
    var routineWorkItemManager = {
        routineWorkItemGuid: fieldSurveyTable.row(selectedRow).data()[2]
    }

    var circleManager = {
        circleGuid: $('#circleGuid').val()
    }

    var systemVariablesManager = {
        fiscalYear: $('#fiscalYear').val(),
        circleManager: circleManager

    }

    var unitRate = fieldSurveyTable.row(selectedRow).data()[5]

    var availableInputs = $('input', selectedRow);

    var fieldSurveyWorkItemCost = availableInputs[1].value;

    var fieldSurveyManager = {
        routineWorkItemManager: routineWorkItemManager,
        assetManager: assetManager,
        earthVolumeQuantity: quantity,
        fieldSurveyWorkItemCostInput: fieldSurveyWorkItemCost,
        systemVariablesManager: systemVariablesManager
    }

    var fieldSurveyManagerJsonString = JSON.stringify(fieldSurveyManager);

    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "FieldSurvey.aspx/SaveFieldSurveyData",
        data: '{"fieldSurveyManager":' + fieldSurveyManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });

    return retValue;
}

updateFieldSurveyData = function (fieldSurveyGuid) {
     
    var earthVolumeQuantity = $.trim($('#fieldSurveyCostFieldsetPanel table#fieldSurveyDataList input#quantityCell').val());
    var fieldSurveyWorkItemCost = $.trim($('#fieldSurveyCostFieldsetPanel table#fieldSurveyDataList input#costCell').val());

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "FieldSurvey.aspx/UpdateFieldSurveyData",
        data: '{"fieldSurveyGuid":"' + fieldSurveyGuid + '","earthVolumeQuantity":"' + earthVolumeQuantity + '","fieldSurveyWorkItemCost":"' + fieldSurveyWorkItemCost + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

deleteFieldSurveyData = function (fieldSurveyGuid) {
    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "FieldSurvey.aspx/DeleteFieldSurveyData",
        data: '{"fieldSurveyGuid":"' + fieldSurveyGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {          
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}


