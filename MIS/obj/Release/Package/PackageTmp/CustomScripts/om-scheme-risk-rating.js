﻿var schemeRiskRatingRelationGuid;
var schemeCodeGuid;
var schemeRiskRatingGuid;
var schemeRiskData = [];

$(document).ready(function () {

    $('#schemeRiskRatingBtn').on('click', function (e) {
        e.preventDefault();
        
       $.ajax({
            type: "Post",
            async: false,
            url: "DBAMethods.asmx/GetSchemeCodeDataWithSchemeRisk",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {                

                getSchemeRiskRating();
                $('#schemeRiskRatingPopupModal .modal-body').append(response.d);
             
                $('#schemeRiskRatingPopupModal').modal('show');
                schemeRiskRatingTableRelatedFunctions();
            },
            failure: function (result) {
                alert("Error");
            }
       });

        e.stopPropagation();
    });

    $('#schemeRiskRatingPopupModal').on('click', '.modal-footer button#backFromSchemeRiskRatingPopUpModalToDbaBtn', function (e) {
        e.preventDefault();

        window.location = 'DBAHome.aspx';

        e.stopPropagation();
    });

});

schemeRiskRatingTableRelatedFunctions = function () {
    var schemeRiskRatingRowEditing = null;

    var schemeRiskRatingTable = $('#schemeRiskRatingDataList').DataTable({
        responsive: true,
        pagingType: "full_numbers",
        searchHighlight: true,
        columnDefs: [
            {
                targets: [0],
                orderable: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [1],
                visible: false,
                searchable: false
            },
            {
                targets: [2],
                orderable: true
            },
            {
                targets: [3],
                orderable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [4],
                visible: false,
                searchable: false
            },
            {
                className: 'never',//className:'hidden',
                targets: [5],
                visible: false,
                searchable: false
            }
        ],
        language: {
            //"lengthMenu": "Display _MENU_ records per page",
            lengthMenu: 'Display <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="15">15</option>' +
                        '<option value="20">20</option>' +
                        '<option value="25">25</option>' +
                        '<option value="30">30</option>' +
                        '<option value="35">35</option>' +
                        '<option value="40">40</option>' +
                        '<option value="45">45</option>' +
                        '<option value="50">50</option>' +
                        '<option value="100">100</option>' +
                        '<option value="-1">All</option>' +
                        '</select> records per page',
            zeroRecords: "Nothing found - sorry",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "No records available",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        pageLength: 5

    });

    schemeRiskRatingTable.on('order.dt search.dt', function () {
        schemeRiskRatingTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $('#schemeRiskRatingPopupModal').on('click', '#schemeRiskRatingDataList a.editSchemeRiskRatingRowBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        if (schemeRiskRatingRowEditing !== null && schemeRiskRatingRowEditing != selectedRow) {

            /* A different row is being edited - the edit should be cancelled and this row edited */

            restoreSchemeCodeWithSchemeRiskRatingRelationData(schemeRiskRatingTable, schemeRiskRatingRowEditing);
            editSchemeCodeWithSchemeRiskRatingRelationRow(schemeRiskRatingTable, selectedRow);
            schemeRiskRatingRowEditing = selectedRow;

            var el = $('#schemeRiskRatingPopupModal select#schemeRiskRatingSelect');
            var elemLen = el.val().length;
            el.focus();


            el.setCursorPosition(elemLen);
        }
        else {
            /* No row currently being edited */
            editSchemeCodeWithSchemeRiskRatingRelationRow(schemeRiskRatingTable, selectedRow);
            schemeRiskRatingRowEditing = selectedRow;

            var el = $('#schemeRiskRatingPopupModal select#schemeRiskRatingSelect');
            var elemLen = el.val().length;
            el.focus();

            el.setCursorPosition(elemLen);
        }
    });

    $('#schemeRiskRatingPopupModal').on('click', '#schemeRiskRatingDataList a.updateSchemeCodeWithSchemeRiskRatingRowBtn', function (e) {
        e.preventDefault();

        if ($("#schemeRiskRatingPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            /* Get the row as a parent of the link that was clicked on */
            var selectedRow = $(this).parents('tr')[0];

            schemeRiskRatingRowEditing = selectedRow;

            var guid = schemeRiskRatingTable.row(selectedRow).data()[4];
            if (guid == "00000000-0000-0000-0000-000000000000") {

                schemeCodeGuid = schemeRiskRatingTable.row(selectedRow).data()[1];

                var schemeRiskRatingRelationSavedGuid = saveSchemeCodeWithSchemeRiskRatingRelation(schemeRiskRatingTable, selectedRow);

                if (jQuery.Guid.IsValid(schemeRiskRatingRelationSavedGuid.toUpperCase()) && !jQuery.Guid.IsEmpty(schemeRiskRatingRelationSavedGuid.toUpperCase())) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your Data is saved successfully.", function () {
                        if (schemeRiskRatingRowEditing !== null && schemeRiskRatingRowEditing == selectedRow) {
                            /* A different row is being edited - the edit should be cancelled and this row edited */
                        
                           saveNewSchemeCodeWithSchemeRiskRatingRelationRow(schemeRiskRatingTable, schemeRiskRatingRowEditing, schemeRiskRatingRelationSavedGuid);
                           schemeRiskRatingRowEditing = null;
                            
                        }
                    });
                }
                else {
               
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    
                    bootbox.alert("Your data cannot be saved.<br>Please contact your database administrator.");
                }
            }
            else {
                schemeRiskRatingRelationGuid = schemeRiskRatingTable.row(selectedRow).data()[4];
                var schemeRiskRatingRelationUpdated = updatSchemeCodeAndSchemeRiskRatingRelationData(schemeRiskRatingTable, selectedRow);

                if (schemeRiskRatingRelationUpdated == 1) {

                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');

                    bootbox.alert("Your Data is updated successfully.", function () {
                        if (schemeRiskRatingRowEditing !== null && schemeRiskRatingRowEditing == selectedRow) {

                            /* A different row is being edited - the edit should be cancelled and this row edited */
                            updateSchemeCodeWithSchemeRiskRatingRelationRow(schemeRiskRatingTable, schemeRiskRatingRowEditing);
                           
                            schemeRiskRatingRowEditing = null;
                                                      
                        }
                    });
                }
                else if (schemeRiskRatingRelationUpdated == 0) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    bootbox.alert("Your data cannot be updated.<br>Please contact your database administrator.");
                }
            }
        }

    });

    $('#schemeRiskRatingPopupModal').on('click', '#schemeRiskRatingDataList a.cancelSchemeCodeWithSchemeRiskRatingRowBtn', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var selectedRow = $(this).parents('tr')[0];

        var selectedRowData = schemeRiskRatingTable.row(selectedRow).data();

        var availableTds = $('>td', selectedRow);

        var selectedRowSerialNum = availableTds[0].innerHTML;

        var selectedSchemeRiskRatingGuidRow = schemeRiskRatingTable.row(selectedRow).data()[5];

        var selectedSchemeRiskRating = schemeRiskRatingTable.row(selectedRow).data()[6];

        selectedRowData[5] = selectedSchemeRiskRatingGuidRow;
        selectedRowData[6] = selectedSchemeRiskRating;

        schemeRiskRatingTable.row(selectedRow).data(selectedRowData);

        availableTds[0].innerHTML = selectedRowSerialNum;

    });

}

restoreSchemeCodeWithSchemeRiskRatingRelationData = function (schemeRiskRatingTable, previousRow) {
    var previousRowData = schemeRiskRatingTable.row(previousRow).data();

    var availableTds = $('>td', previousRow);

    var previousRowSerialNum = availableTds[0].innerHTML;

    if (previousRowSerialNum === '*') {
       
    } else {
        schemeRiskRatingTable.row(previousRow).data(previousRowData);

        availableTds[0].innerHTML = previousRowSerialNum;
    }
}

editSchemeCodeWithSchemeRiskRatingRelationRow = function (schemeRiskRatingTable, selectedRow) {
    var selectedRowData = schemeRiskRatingTable.row(selectedRow).data();

    var availableTds = $('>td', selectedRow);

    var schemeRiskRatingGuid = schemeRiskRatingTable.row(selectedRow).data()[5];

    //Only 3 tds are there, but there will be 4 data. The invisible td will not be counted in tds.

    var html = '<select name=schemeRiskRatingSelect class=form-control schemeRiskRatingSelect>';

    for (var i = 0; i < schemeRiskData.length; i++) {
        if (schemeRiskData[i].SchemeRiskRatingGuid == schemeRiskRatingGuid) {
            html += '<option selected="selected" value="' + schemeRiskData[i].SchemeRiskRatingGuid + '">' + schemeRiskData[i].SchemeRiskRating + '</option>';
        }
        else {
            html += '<option value="' + schemeRiskData[i].SchemeRiskRatingGuid + '">' + schemeRiskData[i].SchemeRiskRating + '</option>';
        }

    }

    html += '</select>';

    availableTds[3].innerHTML = html;
    availableTds[4].innerHTML = '<a href="#" class="updateSchemeCodeWithSchemeRiskRatingRowBtn"><i class="ui-tooltip fa fa-floppy-o" style="font-size: 22px;" data-original-title="Save" title="Save"></i></a>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '<a href="#" class="cancelSchemeCodeWithSchemeRiskRatingRowBtn"><i class="ui-tooltip fa fa-times-circle-o" style="font-size: 22px;" data-original-title="Cancel" title="Cancel"></i></a>';
}

getSchemeRiskRating = function () {
    $.ajax({
        type: "POST",
        async: false,
        url: "DBAMethods.asmx/GetSchemeRiskRating",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            schemeRiskData = response.d;
        },
        failure: function (result) {
            alert("Error");
        }
    });
}

saveNewSchemeCodeWithSchemeRiskRatingRelationRow = function (schemeRiskRatingTable, selectedRow, schemeRiskRatingRelationSavedGuid) {

    var selectedRowData = schemeRiskRatingTable.row(selectedRow).data();

    var selectedSchemeRiskRatingGuid;

    var selectedSchemeRiskRating;

    var availableSelects = $('select', selectedRow)[0].value;    

    for (var i = 0; i < schemeRiskData.length; i++) {
        if (schemeRiskData[i].SchemeRiskRatingGuid == availableSelects) {

            selectedSchemeRiskRatingGuid = schemeRiskData[i].SchemeRiskRatingGuid;
            selectedSchemeRiskRating = schemeRiskData[i].SchemeRiskRating;
        }
    }

    selectedRowData[4] = schemeRiskRatingRelationSavedGuid;
    selectedRowData[5] = selectedSchemeRiskRatingGuid;
    selectedRowData[6] = selectedSchemeRiskRating;

    schemeRiskRatingTable.row(selectedRow).data(selectedRowData);

    schemeRiskRatingTable.on('order.dt search.dt', function () {
        schemeRiskRatingTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

updateSchemeCodeWithSchemeRiskRatingRelationRow = function (schemeRiskRatingTable, selectedRow) {
    var selectedRowData = schemeRiskRatingTable.row(selectedRow).data();

    var selectedSchemeRiskRatingGuid;

    var selectedSchemeRiskRating;

    var availableTds = $('>td', selectedRow);

    var selectedRowSerialNum = availableTds[0].innerHTML;

    var availableSelects = $('select', selectedRow)[0].value;

    for (var i = 0; i < schemeRiskData.length; i++) {
        if (schemeRiskData[i].SchemeRiskRatingGuid == availableSelects) {

            selectedSchemeRiskRatingGuid = schemeRiskData[i].SchemeRiskRatingGuid;
            selectedSchemeRiskRating = schemeRiskData[i].SchemeRiskRating;
        }
    }

    selectedRowData[5] = selectedSchemeRiskRatingGuid;
    selectedRowData[6] = selectedSchemeRiskRating;

    schemeRiskRatingTable.row(selectedRow).data(selectedRowData);

    availableTds[0].innerHTML = selectedRowSerialNum;

}

saveSchemeCodeWithSchemeRiskRatingRelation = function (table, selectedRow) {
    var availableSelects = $('select', selectedRow);

    var selectValue = $.trim(availableSelects[0].value);

    var schemeRiskRatingManager = {
        schemeRiskRatingGuid: selectValue
    }

    var schemeManager =
       {
           schemeGuid: schemeCodeGuid
       }

    var schemeCodeAndSchemeRiskRatingRelationManager = {
        schemeRiskRatingManager: schemeRiskRatingManager,
        schemeManager: schemeManager
    }

    var schemeCodeAndSchemeRiskRatingRelationManagerJsonString = JSON.stringify(schemeCodeAndSchemeRiskRatingRelationManager);


    var retValue = jQuery.Guid.Empty();

    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/SaveSchemeCodeAndSchemeRiskRatingRelation",
        data: '{"schemeCodeAndSchemeRiskRatingRelationManager":' + schemeCodeAndSchemeRiskRatingRelationManagerJsonString + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            //$('#status').delay(300).fadeOut();
            //$('#preloader').delay(350).fadeOut('slow');
            bootbox.alert("Please contact your database administrator.");
        }
    });

    return retValue;
}

updatSchemeCodeAndSchemeRiskRatingRelationData = function (table, selectedRow) {
    var availableSelects = $('select', selectedRow);

    var selectValue = $.trim(availableSelects[0].value);

    var schemeRiskRatingGuid = selectValue;

    var retValue = 0;
    $.ajax({
        type: "Post",
        async: false,
        url: "DBAMethods.asmx/UpdateSchemeCodeAndSchemeRiskRatingRelationData",
        data: '{"schemeCodeSchemeRiskRatingRelationGuid":"' + schemeRiskRatingRelationGuid + '","schemeRiskRatingGuid":"' + schemeRiskRatingGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        failure: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });
    return retValue;
}

