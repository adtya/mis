﻿$(document).ready(function () {

    $("#editDivisionMenuBtn").on('click', function () {
        $.ajax({
            type: "POST",
            async: false,
            url: "DBAMethods.asmx/GetCircleNames",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                createEditDivisionForm(response.d);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

    $('#updateDivisionBtn').on('click', function () {
        if ($("#editDivisionPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            var circleGuid = $("#circleListToEditDivision").val();
            var divisionGuid = $("#divisionListToEditDivisionBasedOnCircleSelected").val();
            var edittedDivisionCode = $("#edittedDivisionCode").val();
            var edittedDivisionName = $("#edittedDivisionName").val();

            $('#editDivisionPopupModal').modal('hide');

            $.ajax({
                type: "Post",
                async: false,
                url: "DBAMethods.asmx/UpdateDivision",
                data: '{"circleGuid":"' + circleGuid + '","divisionGuid":"' + divisionGuid + '","edittedDivisionCode":"' + edittedDivisionCode + '","edittedDivisionName":"' + edittedDivisionName + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    var str = response.d;
                    if (str == true) {
                        bootbox.alert("Division updated successfully");
                    }
                    else {
                        bootbox.alert("Division cannot be updated.");
                    }
                },

                failure: function (msg) {
                    $('#status').delay(300).fadeOut();
                    $('#preloader').delay(350).fadeOut('slow');
                    alert(msg);
                }
            });
        }
    });

});

createEditDivisionForm = function (circleArr) {
    var divisionArr = [];

    $("#editDivisionPopupModal .modal-body").empty();

    var html = '<div class="container-fluid col-md-12">';
    html += '<div class="row">';
    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
    html += '<div class="panel panel-default">';
    html += '<div class="panel-body">';
    html += '<div class="text-center">';
    html += '<img src="Images/add-circle.png" class="login" height="70" />';
    html += '<h2 class="text-center">Edit Division</h2>';

    html += '<div class="panel-body">';
    html += '<form id="editDivisionPopupForm" class="form form-horizontal" method="post">';
    html += '<fieldset>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="circleListToEditDivision" id="circleListToEditDivisionLabel" class="control-label">Circle Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<select id="circleListToEditDivision" name="circleListToEditDivision" class="form-control">';
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="divisionListToEditDivisionBasedOnCircleSelected" id="divisionListToEditDivisionBasedOnCircleSelectedLabel" class="control-label">Division Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<select id="divisionListToEditDivisionBasedOnCircleSelected" name="divisionListToEditDivisionBasedOnCircleSelected" class="form-control">';
    html += '<option value="">--Select Division--</option>';
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedDivisionName" id="edittedDivisionNameLabel" class="control-label">Division Name</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<input id="edittedDivisionName" name="edittedDivisionName" placeholder="Division Name" class="form-control text-capitalize" />';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
    html += '<label for="edittedDivisionCode" id="edittedDivisionCodeLabel" class="control-label">Division Code</label>';
    html += '</div>';
    html += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6\">';
    html += '<input id="edittedDivisionCode" name="edittedDivisionCode" placeholder="Division Code" class="form-control text-uppercase" />';
    html += '</div>';

    html += '</fieldset>';
    html += '</form>';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#editDivisionPopupModal .modal-body").append(html);

    showCircleListOptionsToEditDivision('circleListToEditDivision', circleArr);

    editDivisionValidationRules();

    $('#editDivisionPopupModal').modal('show');

    $('#circleListToEditDivision').on('change', function () {
        $('#divisionListToEditDivisionBasedOnCircleSelected').empty();
        var selectedCircleGuid = $(this).val();        
        $.ajax({
            type: "POST",
            async: false,
            url: "DBAMethods.asmx/GetDivisionsBasedOnCircleGuid",
            data: '{"circleGuid":"' + selectedCircleGuid + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                divisionArr = response.d;
                showDivisionListOptionsToEditDivision('divisionListToEditDivisionBasedOnCircleSelected', divisionArr);
            },
            failure: function (result) {
                alert("Error");
            }
        });
    });

    $('#divisionListToEditDivisionBasedOnCircleSelected').on('change', function () {
        var selectedDivision = $(this).val();
        if (selectedDivision == "") {
            $('#updateDivisionBtn').addClass('hidden');
            $('#edittedDivisionCode').val('');
            $('#edittedDivisionName').val('');
        } else {
            for (var i = 0; i < divisionArr.length;) {
                if (divisionArr[i] == selectedDivision) {
                    $('#edittedDivisionCode').val(divisionArr[i + 1]);
                    $('#edittedDivisionName').val(divisionArr[i + 2]);
                }
                i = i + 3;
            }
            $('#updateDivisionBtn').removeClass('hidden');
        }
    });

}

showCircleListOptionsToEditDivision = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Circle--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 3;
    }
}

showDivisionListOptionsToEditDivision = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Division--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;) {
        option_str.options[option_str.length] = new Option(arr[i + 1], arr[i]);
        i += 3;
    }
}

editDivisionValidationRules = function () {
    $('#editDivisionPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {
            edittedDivisionCode: {
                required: true,
                noSpace: true,
                minlength: 3,
                maxlength: 5
            },
            edittedDivisionName: {
                required: true,
                noSpace: true
            }
        }
    });
}