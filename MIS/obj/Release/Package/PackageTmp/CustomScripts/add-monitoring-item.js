﻿$(document).ready(function () {

    $('#addMonitoringItemBtn').on('click', function () {
        $.ajax({
            type: "Post",
            async: false,
            url: "SystemMaintain.aspx/GetAssetType",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#addMonitoringItemPopupModal').modal('show');
                showAssetTypeListOptionsToSelectItemType('assetTypeToGetMonitoringItemTypeList', response.d);                
            },
            failure: function (result) {
                alert("Error");
            }
        });

        $('#assetTypeToGetMonitoringItemTypeList').on('change', function () {
            $('#monitoringItemTypeListToAddItem').empty();
            var selectedAssetTypeGuid = $('#assetTypeToGetMonitoringItemTypeList').val();
            $.ajax({
                type: "POST",
                async: false,
                url: "SystemMaintain.aspx/GetMonitoringItemTypes",
                data: '{"assetTypeGuid":"' + selectedAssetTypeGuid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    monitoringItemTypeArr = response.d;
                    showMonitoringItemTypeListOptionsToAddItem('monitoringItemTypeListToAddItem', monitoringItemTypeArr);
                },
                failure: function (result) {
                    alert("Error");
                }
            });
        });
    });

    $('#addMonitoringItemPopupForm').validate({ // initialize plugin
        ignore: ":not(:visible)",
        rules: {            
            monitoringItemName: {
                required: true,
                noSpace: true
            },
            monitoringItemTypeListToAddItem: "required"
        },
    });

    $('#addMonitoringItemDataBtn').on('click', function () {

        if ($("#addMonitoringItemPopupForm").valid()) {

            $("#preloader").show();
            $("#status").show();

            var check = saveMonitoringItem();

            if (check == 1) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data is saved successfully.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'DBAHome.aspx';
                });
            }
            else if (check == 0) {
                $('#status').delay(300).fadeOut();
                $('#preloader').delay(350).fadeOut('slow');

                bootbox.alert("Your data cannot be saved.\nPlease contact your database administrator.", function () {
                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
                    //window.location.href = nextUrl;
                    window.location = 'DBAHome.aspx';
                });
            }
        }

    });

});

showAssetTypeListOptionsToSelectItemType = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Asset Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length; i++) {
        option_str.options[option_str.length] = new Option(arr[i].AssetTypeCode + "/" + arr[i].AssetTypeName, arr[i].AssetTypeGuid);
    }
}


showMonitoringItemTypeListOptionsToAddItem = function (selectId, arr) {
    var option_str = document.getElementById(selectId);
    option_str.length = 0;
    option_str.options[0] = new Option('--Select Item Type--', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < arr.length;i++) {
        option_str.options[option_str.length] = new Option(arr[i].MonitoringItemTypeName, arr[i].MonitoringItemTypeGuid);
    }
}

saveMonitoringItem = function () {
    
    var monitoringItemName = $.trim($('#monitoringItemName').val());
    var monitoringItemTypeGuid = $('#monitoringItemTypeListToAddItem').val();

    var retValue = 0;

    $.ajax({
        type: "Post",
        async: false,
        url: "SystemMaintain.aspx/SaveMonitoringItem",
        data: '{"monitoringItemName":"' + monitoringItemName + '","monitoringItemTypeGuid":"' + monitoringItemTypeGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            retValue = response.d;
        },
        error: function (msg) {
            alert(msg);
            bootbox.alert(msg);
        }
    });

    return retValue;
}












//createAddMonitoringItemForm = function () {
//    $("#addMonitoringItemPopupModal .modal-body").empty();

//    var html = '<div class="container-fluid col-md-12">';
//    html += '<div class="row">';
//    html += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">';
//    html += '<div class="panel panel-default">';
//    html += '<div class="panel-body">';
//    html += '<div class="text-center">';
//    html += '<img src="Images/add-circle.png" class="login" height="70" />';
//    html += '<h2 class="text-center">Create Item</h2>';

//    html += '<div class="panel-body">';
//    html += '<!--start form--><!--add form action as needed-->';
//    html += '<form id="addMonitoringItemPopupForm" name="addMonitoringItemPopupForm" role="form" class="form form-horizontal" method="post">';
//    html += '<fieldset>';

//    html += '<div class="form-group">';
//    html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
//    html += '<label for="monitoirngItemTypeName" id="monitoirngItemTypeNameLabel" class="control-label pull-left">Item Type</label>';
//    html += '</div>';
//    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
//    html += '<input type="text" id="monitoirngItemTypeName" name="monitoirngItemTypeName" placeholder="Item Type" class="form-control text-capitalize" value="" style="width:100%;" />';
//    html += '</div>';
//    html += '</div>';


//    html += '<div class="form-group">';
//    html += '<input id="addMonitoringItemBtn" class="btn btn-lg btn-primary btn-block" value="Add Item Type" type="submit" />';
//    html += '</div>';

//    html += '</fieldset>';
//    html += '</form><!--/end form-->';
//    html += '</div>';

//    html += '</div>';
//    html += '</div>';
//    html += '</div>';
//    html += '</div>';
//    html += '</div>';
//    html += '</div>';

//    $("#addMonitoringItemPopupModal .modal-body").append(html);

//    $('#addMonitoringItemPopupModal').modal('show');

//    $('#addMonitoringItemBtn').on('click', function (e) {
//        e.preventDefault();
//        if ($("#addMonitoringItemPopupForm").valid()) {
//            $("#preloader").show();
//            $("#status").show();

//            var check = saveMonitoringItemData();

//            if (check == 1) {
//                $('#status').delay(300).fadeOut();
//                $('#preloader').delay(350).fadeOut('slow');

//                bootbox.confirm({
//                    title: '',
//                    message: 'Your data is saved successfully.<br>Do you want to create more item types?',
//                    buttons: {
//                        'cancel': {
//                            label: 'No',
//                            className: 'btn-default pull-left'
//                        },
//                        'confirm': {
//                            label: 'Yes',
//                            className: 'btn-default pull-right'
//                        }
//                    },
//                    callback: function (result) {
//                        if (result) {

//                        } else {
//                            $('#addMonitoringItemPopupModal').modal('hide');
//                        }
//                    }
//                });
//            } else if (check == 0) {
//                $('#status').delay(300).fadeOut();
//                $('#preloader').delay(350).fadeOut('slow');

//                bootbox.alert("Your data cannot be saved.\nPlease contact your database administrator.", function () {
//                    //var nextUrl = location.protocol + "//" + location.host + "/Default.aspx";
//                    //window.location.href = nextUrl;
//                    window.location = 'DBAHome.aspx';
//                });
//            }
//        }

//    });
//}

