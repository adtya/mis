﻿<%@ Page Title="Create Project" Language="C#" MasterPageFile="~/DBA.Master" AutoEventWireup="true" CodeBehind="CreateNewProject.aspx.cs" Inherits="MIS.CreateNewProject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    <link href="Content/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    
    <link href="CustomStyles/create-new-project.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
    <script src="Scripts/jquery.glob.js" type="text/javascript"></script>
    <script src="Scripts/globinfo/jQuery.glob.en-IN.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.GlobalMoneyInput2.js" type="text/javascript"></script>
    <script src="Scripts/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="Scripts/Datejs/build/date.js" type="text/javascript"></script>
    <script src="Scripts/date.format.js" type="text/javascript"></script>
    
    <script src="Scripts/accounting.js" type="text/javascript"></script>
    <%--<script src="Scripts/decimal.min.js" type="text/javascript"></script>--%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div class="container"><%-- style="margin-top:5%;">--%>

        <div class="row">
            <div class="col-xs-6 col-xs-offset-3 col-sm-12 col-sm-offset-3 col-md-12 col-md-offset-3 col-lg-12 col-lg-offset-3">
                <a id="btnBack" class="btn btn-primary" href="DBAProjects.aspx">Project List</a>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>New Project</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" name="createNewProjectForm" id="createNewProjectForm" class="form form-horizontal" action="#" method="post" runat="server">
                            <fieldset>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="box">

                                            <!--Step 1-->
                                            <div class="step well">
                                                <div class="panel panel-default">

                                                    <div class="panel-heading">
                                                        <h3 align="center">
                                                            <strong>Project General Information</strong>
                                                        </h3>
                                                    </div>

                                                    <div class="panel-body">

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="districtList" id="districtListLabel" class="control-label">Name of District</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <asp:Literal ID="ltDistrictList" runat="server"></asp:Literal>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="divisionList" id="divisionListLabel" class="control-label">Name of Division</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <asp:Literal ID="ltDivisionList" runat="server"></asp:Literal>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="projectName" id="projectNameLabel" class="control-label">Name of Project</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="projectName" class="form-control" id="projectName" placeholder="Project Name" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="subProjectName" id="subProjectNameLabel" class="control-label">Name of Sub-Project</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="subProjectName" class="form-control" id="subProjectName" placeholder="Name of Sub-Project" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="chainageOfProject" id="chainageOfProjectLabel" class="control-label">Chainage of Project</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="chainageOfProject" class="form-control" id="chainageOfProject" placeholder="Chainage of Project" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="dateOfStartOfProject" id="dateOfStartOfProjectLabel" class="control-label">Date Of Start</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div id="dpDateOfStartOfProject" class="input-group date">
                                                                    <input type="text" name="dateOfStartOfProject" class="form-control" id="dateOfStartOfProject" placeholder="Date Of Start (dd/mm/yyyy)" />
                                                                    <span class="input-group-addon form-control-static">
                                                                        <i class="fa fa-calendar fa-fw"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="expectedDateOfCompletionOfProject" id="expectedDateOfCompletionOfProjectLabel" class="control-label">Date Of Completion (Expected)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div id="dpExpectedDateOfCompletionOfProject" class="input-group date">
                                                                    <input type="text" name="expectedDateOfCompletionOfProject" class="form-control" id="expectedDateOfCompletionOfProject" placeholder="Date Of Completion (dd/mm/yyyy)" />
                                                                    <span class="input-group-addon form-control-static">
                                                                        <i class="fa fa-calendar fa-fw"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <%--<div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="actualDateOfCompletionOfProject" id="actualDateOfCompletionOfProjectLabel" class="control-label">Date Of Completion (Actual)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div id="dpActualDateOfCompletionOfProject" class="input-group date">
                                                                    <input type="text" name="actualDateOfCompletionOfProject" class="form-control" id="actualDateOfCompletionOfProject" placeholder="Date Of Completion (Actual)" />
                                                                    <span class="input-group-addon form-control-static">
                                                                        <i class="fa fa-calendar fa-fw"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>--%>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="projectValue" id="projectValueLabel" class="control-label">Project Value</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div id="projectValueInputGroup" class="input-group">
                                                                    <span class="input-group-addon form-control-static">
                                                                        <i class="fa fa-inr fa-fw"></i>
                                                                    </span>
                                                                    <input type="text" name="projectValue" class="form-control" id="projectValue" placeholder="Project Value" />
                                                                </div>
                                                                <div id="moneyContainer"></div>
                                                            </div>
                                                            
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <!--Step 1 Ended-->

                                            <!--Step 2-->
                                            <div class="step well">
                                                <div class="panel panel-default">

                                                    <div class="panel-heading">
                                                        <h3 align="center">
                                                            <strong>FREMAA Officers Engaged</strong>
                                                        </h3>
                                                    </div>

                                                    <div class="panel-body">

                                                        <div id="officersListDisplay" class="panel panel-default panel-body col-xs-12 col-sm-12 col-md-12 col-lg-12 well well-sm hidden"></div>

                                                        <div class="form-group">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <label for="officersEngaged" id="officersEngagedLabel" class="control-label">Name and designation of the other officer engaged for FREMAA works</label>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                <input type="text" name="officersEngaged" class="form-control" id="officersEngaged" placeholder="Name and designation of the other officer engaged for FREMAA works" />
                                                            </div>
                                                            <div class="col-xs-4 col-xs-offset-1 col-sm-4 col-sm-offset-1 col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1">
                                                                <button type="button" name="addOfficersEngagedBtn" class="btn btn-block btn-primary" id="addOfficersEngagedBtn">Add</button>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <!--Step 2 Ended-->

                                            <!--Step 3-->
                                            <div class="step well">
                                                <div class="panel panel-default">

                                                    <div class="panel-heading">
                                                        <h3 align="center">
                                                            <strong>PMC Site Engineers</strong>
                                                        </h3>
                                                    </div>

                                                    <div class="panel-body">

                                                        <div id="pmcSiteEngineersListDisplay" class="panel panel-default panel-body col-xs-12 col-sm-12 col-md-12 col-lg-12 well well-sm hidden"></div>

                                                        <div class="form-group">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <label for="pmcSiteEngineers" id="pmcSiteEngineersLabel" class="control-label">Name and designation of the PMC Site Engineer</label>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                <input type="text" name="pmcSiteEngineers" class="form-control" id="pmcSiteEngineers" placeholder="Name and designation of the PMC Site Engineer" />
                                                            </div>
                                                            <div class="col-xs-4 col-xs-offset-1 col-sm-4 col-sm-offset-1 col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1">
                                                                <button type="button" name="addPmcSiteEngineersBtn" class="btn btn-block btn-primary" id="addPmcSiteEngineersBtn">Add</button>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <!--Step 3 Ended-->

                                            <!--Step 4-->
                                            <div class="step well">
                                                <div class="panel panel-default">

                                                    <div class="panel-heading">
                                                        <h3 align="center">
                                                            <strong>Sub Chainages Information</strong>
                                                        </h3>
                                                    </div>

                                                    <div class="panel-body">

                                                        <div id="subChainagesListDisplay" class="panel panel-default panel-body col-xs-12 col-sm-12 col-md-12 col-lg-12 well well-sm hidden"></div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="subChainageName" id="subChainageNameLabel" class="control-label">Name of Sub Chainage</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="subChainageName" class="form-control" id="subChainageName" placeholder="Name of Sub Chainage" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="sioUsersList" id="sioUsersListLabel" class="control-label">SIO for the Sub Chainage</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <asp:Literal ID="ltSioUserList" runat="server"></asp:Literal>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="subChainageValue" id="subChainageValueLabel" class="control-label">Sub-Chainage Value</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div id="subChainageValueInputGroup" class="input-group">
                                                                    <span class="input-group-addon form-control-static">
                                                                        <i class="fa fa-inr fa-fw"></i>
                                                                    </span>
                                                                    <input type="text" name="subChainageValue" class="form-control" id="subChainageValue" placeholder="Sub-Chainage Value" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="administrativeApprovalReference" id="administrativeApprovalReferenceLabel" class="control-label">Ref of AA</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="administrativeApprovalReference" class="form-control" id="administrativeApprovalReference" placeholder="Ref of AA" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="technicalSanctionReference" id="technicalSanctionReferenceLabel" class="control-label">Ref of TS</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="technicalSanctionReference" class="form-control" id="technicalSanctionReference" placeholder="Ref of TS" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="contractorName" id="contractorNameLabel" class="control-label">Name of Contractor</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="contractorName" class="form-control" id="contractorName" placeholder="Name of Contractor" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="workOrderReference" id="workOrderReferenceLabel" class="control-label">Ref of Work Order</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="workOrderReference" class="form-control" id="workOrderReference" placeholder="Ref of Work Order" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="subChainageStartingDate" id="subChainageStartingDateLabel" class="control-label">Date of Starting</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div id="dpSubChainageStartingDate" class="input-group date">
                                                                    <input type="text" name="subChainageStartingDate" class="form-control" id="subChainageStartingDate" placeholder="Date of Starting(dd/mm/yyyy)" />
                                                                    <span class="input-group-addon form-control-static">
                                                                        <i class="fa fa-calendar fa-fw"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="subChainageExpectedCompletionDate" id="subChainageExpectedCompletionDateLabel" class="control-label">Completion Date (Expected)</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <div id="dpSubChainageExpectedCompletionDate" class="input-group date">
                                                                    <input type="text" name="subChainageExpectedCompletionDate" class="form-control" id="subChainageExpectedCompletionDate" placeholder="Expected Completion Date(dd/mm/yyyy)" />
                                                                    <span class="input-group-addon form-control-static">
                                                                        <i class="fa fa-calendar fa-fw"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="typeOfWorks" id="typeOfWorksLabel" class="control-label">Type of Works</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="typeOfWorks" class="form-control" id="typeOfWorks" placeholder="Type of Works" />
                                                            </div>
                                                        </div>

                                                        <div id="workItemsListDisplay" class="panel panel-default panel-body col-xs-12 col-sm-12 col-md-12 col-lg-12 well well-sm hidden"></div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="workItemList" id="workItemListLabel" class="control-label">Works Items</label>
                                                            </div>
                                                            <div id="workItemsListDiv" class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <select class="form-control" name="workItemList" id="workItemList" onchange="displayWorkItemValueDiv();"></select>
                                                            </div>
                                                        </div>

                                                        <div id="workItemValueDiv" class="form-group hidden">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="workItemValue" id="workItemValueLabel" class="control-label">Work Item Value</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="workItemValue" class="form-control" id="workItemValue" placeholder="Work Item Value" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
                                                                <button type="button" name="addWorkItemsBtn" class="btn btn-primary responsive-width" id="addWorkItemsBtn">Add Work Item</button>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-xs-offset-4 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
                                                                <button type="button" name="addSubChainagesBtn" class="btn btn-block btn-primary responsive-width" id="addSubChainagesBtn">Add Sub Chainage</button>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <!--Step 4 Ended-->

                                            <!--Step 5-->
                                            <div class="step well">
                                                <div class="panel panel-default">

                                                    <div class="panel-heading">
                                                        <h3 align="center">
                                                            <strong>Land Acquisition Information</strong>
                                                        </h3>
                                                    </div>

                                                    <div class="panel-body">

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="landAcquisitionRadio" id="landAcquisitionRadioLabel" class="control-label">Include Land Acquisition Details</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <%--<div class="radio radio-inline"></div>--%>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="landAcquisitionRadio" value="yes" />Yes
                                                                </label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="landAcquisitionRadio" value="no" checked="checked" />No
                                                                </label>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <!--Step 5 Ended-->

                                            <!--Step 6-->
                                            <div class="step well">
                                                <div class="panel panel-default">

                                                    <div class="panel-heading">
                                                        <h3 align="center">
                                                            <strong>Financial Information</strong>
                                                        </h3>
                                                    </div>

                                                    <div class="panel-body">

                                                        <div class="form-group">                               
                                                            <div class="col-xs-4 col-xs-offset-4 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
                                                                   <%--<a id="btnCreatehead" class="btn btn-primary btn-block" href="CreateNewProject.aspx">Create Head</a>--%>
                                                                   <button id="assignValueToFinancialSubHeadBtn" name="assignValueToFinancialSubHeadBtn" type="button" class="btn btn-primary">Assign Value</button>                                                                   
                                                               </div>
                                                            <br />
                                                               <div id="financialSubHeadTableDiv" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">                                       
                                                                   <%--<asp:Literal runat="server" ID="headList" />--%>
                                                               </div>
                                                        </div>                                                                                                             

                                                    </div>

                                                </div>
                                            </div>                           
                                            <!--Step 6 Ended-->

                                            <!--Step 7-->
                                            <div class="step well">
                                                <div class="panel panel-heading">
                                                    <h3 align="center">
                                                        <strong></strong>
                                                    </h3>
                                                </div>
                                                <div class="panel panel-body panel-default">
                                                    <div class="form-group">
                                                        <div class="col-sm-12 col-md-12 well">
                                                            <p>
                                                                All the details of the form are saved successfully. Submit the form so that this project can be worked upon.
                                                            </p>
                                                        </div>
                                                        <div class="col-sm-10 col-md-6 col-md-offset-3">
                                                            <button type="button" class="action submit btn btn-success" id="btnCreateProject">Submit</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Step 7 ended-->

                                            <!--Buttons-->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="pull-right">
                                                        <button type="button" class="action back btn btn-info">Back</button>
                                                        <button type="button" class="action next btn btn-info">Next</button>
                                                        <button type="button" class="action btn-sky text-capitalize skip btn">Skip</button>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Buttons ended-->

                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <div id="step-display" class="panel-footer"></div>
                </div>
            </div>
        </div>

        
    </div>





    <div id="assignValueToFinancialSubHeadPopupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header" style="display:none"></div>
                <%--<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h1 class="text-center">What's My Password?</h1>
                </div>--%>

                <div class="modal-body">
                    <%--<div class="col-md-12">--%>
                    <div class="container-fluid col-md-12">
                        <div class="row">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="text-center">
                                                <img src="Images/create-financial.png" class="login" height="70" />
                                                <h2 class="text-center">Financial Head</h2>

                                                <div class="panel-body">
                                                    <%--<form id="popupForm" class="form form-horizontal" method="post"><!--start form--><!--add form action as needed-->
                                                        <fieldset>
                                                            <div class="form-group">
                                                                <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                                                                    <label id="totalBudgetLbl" class="abc control-label"></label>
                                                                </div>
                                                                <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                                                                    <label id="remainingBudgetLbl" class="control-label">dxcvxvxc</label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                       <label for="headName" id="headNameLabel" class="control-label">Name of Head</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                       <input id="headName" name="headName" placeholder="Head Name" class="form-control" />
                                                                </div>                                                                
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                         <label for="budget" id="budgetLabel" class="control-label">Budget</label>
                                                                </div>
                                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                       <input id="budget" name="budget" placeholder="Budget" class="form-control" />
                                                                </div>
                                                                                                                              
                                                            </div>
                                                                                                                     
                                                            <div class="form-group">
                                                                <button class="btn" data-dismiss="modal" aria-hidden="true" id="createHeadButton">Create</button>
                                                            </div>
                                                        </fieldset>
                                                    </form><!--/end form-->--%>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <%--</div>--%>
            </div>

            <div class="modal-footer">
                <div class="col-md-12">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>

        </div>
    </div>
</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
    <script src="CustomScripts/create-new-project.js" type="text/javascript"></script>
</asp:Content>