﻿<%@ Page Title="Login" Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="MIS.Login" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title></title>
    <!-- Latest compiled and minified CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <link href="CustomStyles/login.css" rel="stylesheet" type="text/css" />

    <script src="Scripts/jquery-2.2.2.js" type="text/javascript"></script>
    <script src="Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="Scripts/bootbox.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.validate.min.js" type="text/javascript"></script>
    <script src="CustomScripts/my-validation-methods.js" type="text/javascript"></script>
    <script src="Scripts/jquery-validate.bootstrap-tooltip.flickerfix.min.js" type="text/javascript"></script>
    <script src="CustomScripts/login.js" type="text/javascript"></script>
  
</head>
<body class="wrapper">
    <div class="container">
        <div class="row">
            <div id="page-wrap">
                <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong> Sign in to continue</strong>
                        </div>
                        <div class="panel-body">
                            <form role="form" action="#" method="post" runat="server" id="popupForm">
                                <fieldset>
                                    <div class="row">
                                        <div class="center-block">
                                            <img class="profile-img" src="Images/LoginImages/loginphoto.jpg" alt="Login Photo"/>
                                        </div>
								    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-10  col-md-offset-1 ">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="glyphicon glyphicon-user"></i>
                                                    </span> 
                                                    <input class="form-control" placeholder="Username" name="userName" type="text" id="userName" autofocus="autofocus" />
											    </div>
										    </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="glyphicon glyphicon-lock"></i>
												    </span>
                                                    <input class="form-control" placeholder="Password" name="userPassword" type="password" id="userPassword" />
											    </div>
										    </div>
                                            <div class="form-group">
                                                <input type="button" class="btn btn-lg btn-primary btn-block" value="Login" id="btnLogin" />
                                                <%--<input type="button" class="btn" value="Login" id="btnLogin" runat="server" onserverclick="btnLogin_Click" hidden="hidden" />--%>
										    </div>
                                            <div class="form-group">
                                                <a href="#" id="btnGuestLogin" name="btnGuestLogin">Login As Guest</a>
                                                <%--<input type="button" class="btn btn-lg btn-primary btn-block" value="Login" id="btnLogin" />--%>
                                                <%--<input type="button" class="btn" value="Login" id="btnLogin" runat="server" onserverclick="btnLogin_Click" hidden="hidden" />--%>
										    </div>
                                        </div>
								    </div>
							    </fieldset>
                            </form>
					    </div>					
                    </div>
			    </div>
            </div>
        </div>
    </div>
    
</body>
</html>