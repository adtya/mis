﻿<%@ Page Title="Create SubChainage" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CreateSubChainage.aspx.cs" Inherits="MIS.CreateSubChainage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="CustomStyles/create-sub-chainage.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container" style="margin-top:5%;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>Create Sub Chainage</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" action="#" method="post" runat="server">
                            <fieldset>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="box box-centerside">

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="panel-body">
                                                        <asp:Literal ID="ltProjectList" runat="server"></asp:Literal>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="projectData" class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="panel panel-body well well-sm">

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="subChainageName" id="subChainageNameLabel" class="control-label" style="margin-bottom:1%;">Name of Sub Chainage</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="subChainageName" class="form-control" id="subChainageName" placeholder="Name of Sub Chainage" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="subChainageSio" id="subChainageSioLabel" class="control-label" style="margin-bottom:1%;">SIO for the Sub Chainage</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                <input type="text" name="subChainageSio" class="form-control" id="subChainageSio" placeholder="SIO for the Sub Chainage" />
                                                            </div>
                                                        </div>

                                                         <div class="form-group">
                                                             <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                 <label for="projectValue" id="projectValueLabel" class="control-label" style="margin-bottom:1%;">Project Value</label>
                                                             </div>
                                                             <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                 <input type="text" name="projectValue" class="form-control" id="projectValue" placeholder="Project Value" readonly="readonly" />
                                                             </div>
                                                        </div>

                                                        <div class="form-group">
                                                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="RefAA" id="RefAALabel" class="control-label" style="margin-bottom:1%;">Ref of AA</label>
                                                           </div>
                                                           <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                               <input type="text" name="RefAA" class="form-control" id="RefAA" placeholder="Ref of AA" readonly="readonly" />
                                                           </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                             <label for="RefTS" id="RefTSLabel" class="control-label" style="margin-bottom:1%;">Ref of TS</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                             <input type="text" name="RefTS" class="form-control" id="RefTS" placeholder="Ref of TS" readonly="readonly"/>
                                                        </div>
                                                    </div>

                                                     <div class="form-group">
                                                         <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="contractorName" id="contractorNameLabel" class="control-label" style="margin-bottom:1%;">Name of Contractor</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                           <input type="text" name="contractorName" class="form-control" id="contractorName" placeholder="Name of Contractor" readonly="readonly" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="RefWorkOrder" id="RefWorkOrderLabel" class="control-label" style="margin-bottom:1%;">Ref of Work Order</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                             <input type="text" name="RefWorkOrder" class="form-control" id="RefWorkOrder" placeholder="Ref of Work Order" readonly="readonly" />
                                                        </div>
                                                    </div> 
                                                                                                       
                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                             <label for="startingDate" id="startingDateLabel" class="control-label" style="margin-bottom:1%;">Date of Starting</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <div class="input-group date" id="dpStartingDate">
                                                                <input type="text" name="startingDate" class="form-control" id="startingDate" placeholder="Date of Starting(dd/mm/yyyy)" readonly="readonly"/>
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="completionDate" id="completionDateLabel" class="control-label" style="margin-bottom:1%;">Completion Date</label>
                                                          </div>
                                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                               <div class="input-group date" id="dpcompletionDate">
                                                                    <input type="text" name="completionDate" class="form-control" id="completionDate" placeholder="Completion Date(dd/mm/yyyy)" readonly="readonly" />
                                                                        <span class="input-group-addon">
                                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                                        </span>
                                                               </div>
                                                         </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="workType" id="workTypeLabel" class="control-label" style="margin-bottom:1%;">Type of Works</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                             <input type="text" name="workType" class="form-control" id="workType" placeholder="Type of Works" readonly="readonly" />
                                                        </div>
                                                    </div>

                                                        <div class="form-group">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <label for="workTypeItems" id="workTypeItemsLabel" class="control-label" style="margin-bottom:1%;">Type of Works Items</label>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <asp:Literal ID="ltWorkTypeItems" runat="server"></asp:Literal>
                                                             <%--<input type="text" name="workTypeItems" class="form-control" id="workTypeItems" placeholder="Type of Works" readonly="readonly" />--%>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 pull-right">
                                                                <button type="button" name="addSubChainageBtn" class="btn btn-block btn-primary" id="addSubChainageBtn">Add</button>
                                                            </div>
                                                    </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>

                    <%--<div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="pull-left">
                                    <a class="btn btn-primary" href="DBAHome.aspx" onclick="">Back to Home</a>
                                </div>
                            </div>
                        </div>
                    </div>--%>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
    <script src="CustomScripts/create-sub-chainage.js" type="text/javascript"></script>
</asp:Content>