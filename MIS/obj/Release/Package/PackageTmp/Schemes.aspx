﻿<%@ Page Title="Projects" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Schemes.aspx.cs" Inherits="MIS.Schemes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <h4><strong>List of Schemes</strong></h4>
                <asp:Literal ID="ltSchemeTable" runat="server"></asp:Literal>
            </div>
        </div>
         
    </div>
    </asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
</asp:Content>
