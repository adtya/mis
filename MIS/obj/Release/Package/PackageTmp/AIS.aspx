﻿<%@ Page Title="AMS" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AIS.aspx.cs" Inherits="MIS.AMS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="CustomStyles/ams.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
    <script src="Scripts/bootbox.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.validate.min.js" type="text/javascript"></script>
    <script src="CustomScripts/my-validation-methods.js" type="text/javascript"></script>
    <script src="Scripts/jquery-validate.bootstrap-tooltip.flickerfix.min.js" type="text/javascript"></script>  
     
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Asset Core</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <asp:Label ID="RecordListLabel" CssClass="control-label col-sm-2" AssociatedControlID="RecordList" runat="server" Text="Go to Record"></asp:Label>
                                            <div class="col-sm-10">
                                                <asp:DropDownList ID="RecordList" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="GUWE-P-G-E-EMB01 B/dyke from PGP"></asp:ListItem>
                                                    <asp:ListItem Text="GUWE-P-G-E-EMB02"></asp:ListItem>
                                                    <asp:ListItem Text="GUWE-P-G-E-CUL01"></asp:ListItem>
                                                    <asp:ListItem Text="GUWE-PGW2-CUL01"></asp:ListItem>
                                                    <asp:ListItem Text="GUWE-PGW2-DRN02"></asp:ListItem>
                                                    <asp:ListItem Text="JOR-KAZ-DRN08"></asp:ListItem>
                                                    <asp:ListItem Text="JOR-KAZ-DRN09"></asp:ListItem>
                                                    <asp:ListItem Text="JOR-KAZ-EMB01"></asp:ListItem>
                                                    <asp:ListItem Text="JOR-KAZ-GAU01"></asp:ListItem>
                                                    <asp:ListItem Text="PGP-P-PGP-BRG01"></asp:ListItem>
                                                    <asp:ListItem Text="PGP-P-PGP-BRG02"></asp:ListItem>
                                                    <asp:ListItem Text="PGP-P-PGP-BRG03"></asp:ListItem>
                                                    <asp:ListItem Text="PGP-P-PGP-CAN01"></asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="WRDDivisionLabel" CssClass="control-label col-sm-4" AssociatedControlID="WRDDivision" runat="server" Text="WRD Division"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="WRDDivision" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="KeyAssetLabel" CssClass="control-label col-sm-4" AssociatedControlID="KeyAsset" runat="server" Text="Key Asset"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="KeyAsset" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="SchemeLabel" CssClass="control-label col-sm-4" AssociatedControlID="Scheme" runat="server" Text="Scheme"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="Scheme" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="GoToKeyAssetLabel" CssClass="control-label col-sm-4" AssociatedControlID="GoToKeyAsset" runat="server" Text="Go To Key Asset"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="GoToKeyAsset" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="AssetTypeLabel" CssClass="control-label col-sm-4" AssociatedControlID="AssetType" runat="server" Text="Asset Type"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="AssetType" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5"></div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="AssetCodeLabel" CssClass="control-label col-sm-4" AssociatedControlID="AssetCode" runat="server" Text="Asset Code"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="AssetCode" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5"></div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="DrawingCodeLabel" CssClass="control-label col-sm-4" AssociatedControlID="DrawingCode" runat="server" Text="Drawing Code"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="DrawingCode" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="AMTDLabel" CssClass="control-label col-sm-4" AssociatedControlID="AMTD" runat="server" Text="AMTD"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="AMTD" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="AssetNameLabel" CssClass="control-label col-sm-4" AssociatedControlID="AssetName" runat="server" Text="Asset Name"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="AssetName" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="ChainageStartLabel" CssClass="control-label col-sm-4" AssociatedControlID="ChainageStart" runat="server" Text="Chainage Start"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="ChainageStart" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="EastingUSLabel" CssClass="control-label col-sm-4" AssociatedControlID="EastingUS" runat="server" Text="Easting U/S"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="EastingUS" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="ChainageEndLabel" CssClass="control-label col-sm-4" AssociatedControlID="ChainageEnd" runat="server" Text="Chainage End"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="ChainageEnd" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="NorthingUSLabel" CssClass="control-label col-sm-4" AssociatedControlID="NorthingUS" runat="server" Text="Northing U/S"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="NorthingUS" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="AssetCostLabel" CssClass="control-label col-sm-4" AssociatedControlID="AssetCost" runat="server" Text="Asset Cost (Rs.)"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="AssetCost" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="EastingDSLabel" CssClass="control-label col-sm-4" AssociatedControlID="EastingDS" runat="server" Text="Easting D/S"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="EastingDS" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="CompletionDateLabel" CssClass="control-label col-sm-4" AssociatedControlID="CompletionDate" runat="server" Text="Completion Date"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="CompletionDate" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <div class="form-group">
                                            <asp:Label ID="NorthingDSLabel" CssClass="control-label col-sm-4" AssociatedControlID="NorthingDS" runat="server" Text="Northing D/S"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="NorthingDS" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2"></div>
                                    <div class="col-sm-5 col-md-5"></div>
                                </div>

                                <div>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border"><u>Technical Specification: Embankment<%--<asp:Literal runat="server" ID="FieldSetHeading" />--%></u></legend>

                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <asp:Label ID="USCrestElevationLabel" CssClass="control-label col-sm-6" AssociatedControlID="USCrestElevation" runat="server" Text="U/S Crest Elevation(m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="USCrestElevation" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="DSCrestElevationLabel" CssClass="control-label col-sm-6" AssociatedControlID="DSCrestElevation" runat="server" Text="D/S Crest Elevation(m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="DSCrestElevation" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="LengthLabel" CssClass="control-label col-sm-6" AssociatedControlID="Length" runat="server" Text="Length (m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="Length" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="CrestWidthLabel" CssClass="control-label col-sm-6" AssociatedControlID="CrestWidth" runat="server" Text="Crest Width (m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="CrestWidth" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="CSSlopeLabel" CssClass="control-label col-sm-6" AssociatedControlID="CSSlope" runat="server" Text="CS Slope"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="CSSlope" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="RSSlopeLabel" CssClass="control-label col-sm-6" AssociatedControlID="RSSlope" runat="server" Text="RS Slope"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="RSSlope" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="BermSlopeLabel" CssClass="control-label col-sm-6" AssociatedControlID="BermSlope" runat="server" Text="Berm Slope"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="BermSlope" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="PlatformWidthLabel" CssClass="control-label col-sm-6" AssociatedControlID="PlatformWidth" runat="server" Text="Platform Width (m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="PlatformWidth" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="CrestTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="CrestType" runat="server" Text="Crest Type"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="CrestType" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="None"></asp:ListItem>
                                                        <asp:ListItem Text="Black_Topped"></asp:ListItem>
                                                        <asp:ListItem Text="Brick_Layered"></asp:ListItem>
                                                        <asp:ListItem Text="Concrete" Selected="True"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="FillTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="FillType" runat="server" Text="Fill Type"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="FillType" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Clayey_Loam" Selected="True"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="AverageHeightLabel" CssClass="control-label col-sm-6" AssociatedControlID="AverageHeight" runat="server" Text="Average Height (m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="AverageHeight" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-md-6">
                                            <asp:Label ID="AppurtenantAssetsLabel" CssClass="control-label col-sm-12 pull-left" AssociatedControlID="GridView1" runat="server" Text="Appurtenant Assets"></asp:Label>
                                            <div class="col-sm-12">
                                                <asp:GridView ID="GridView1" runat="server"></asp:GridView>
                                                <%--<asp:Table ID="AppurtenantAssets" runat="server">
                                                    <asp:TableHeaderRow ID="AppurtenantAssetsHeader">
                                                        <asp:TableHeaderCell ID="Code" Text="Code"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell ID="AssetNameInTableHeader" Text="Asset Name"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell ID="Cost" Text="Cost (in Lakh Rs)"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell ID="CompletionDateInTableHeader" Text="Completion Date"></asp:TableHeaderCell>
                                                    </asp:TableHeaderRow>
                                                    <asp:TableRow ID="AppurtenantAssetsRow1"></asp:TableRow>
                                                </asp:Table>--%>
                                            </div>
                                        </div>
                                       
                                    </fieldset>
                                </div>

                                <div>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border"><u>Technical Specification: Bridge<%--<asp:Literal runat="server" ID="FieldSetHeading" />--%></u></legend>

                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <asp:Label ID="BridgeTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="BridgeType" runat="server" Text="Bridge Type"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="BridgeType" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="LengthBridgeLabel" CssClass="control-label col-sm-6" AssociatedControlID="Length" runat="server" Text="Length(m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="LengthBridge" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="NumberPeirsLabel" CssClass="control-label col-sm-6" AssociatedControlID="NumberOfPeirs" runat="server" Text="Number Of Peirs"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="NumberOfPeirs" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="RoadWidthLabel" CssClass="control-label col-sm-6" AssociatedControlID="RoadWidth" runat="server" Text="Road Width(m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="RoadWidth" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>                                           
                                        </div>                                       
                                    </fieldset>
                                </div>

                                <div>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border"><u>Technical Specification: Canal<%--<asp:Literal runat="server" ID="FieldSetHeading" />--%></u></legend>

                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <asp:Label ID="USCrestElevationCanalLabel" CssClass="control-label col-sm-6" AssociatedControlID="USCrestElevationCanal" runat="server" Text="U/S Crest Elevation(m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="USCrestElevationCanal" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="DSCrestElevationLabelCanal" CssClass="control-label col-sm-6" AssociatedControlID="DSCrestElevationCanal" runat="server" Text="D/S Crest Elevation(m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="DSCrestElevationCanal" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="LengthCanalLabel" CssClass="control-label col-sm-6" AssociatedControlID="LengthCanal" runat="server" Text="Length (m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="LengthCanal" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="BedWidthCanalLabel" CssClass="control-label col-sm-6" AssociatedControlID="BedWidthCanal" runat="server" Text="Bed Width (m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="BedWidthCanal" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="SlipeSlopeCanalLAbel" CssClass="control-label col-sm-6" AssociatedControlID="SlipeSlopeCanal" runat="server" Text="Slipe Slope"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="SlipeSlopeCanal" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="LiningTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="LiningType" runat="server" Text="Lining Type"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:DropDownList ID="LiningType" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="None"></asp:ListItem>                                                       
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="AverageDepthCanalLabel" CssClass="control-label col-sm-6" AssociatedControlID="AverageDepthCanal" runat="server" Text="Average Depth"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="AverageDepthCanal" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>                                                               
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <asp:Label ID="AppurtenantAssetsCanalLabel" CssClass="control-label col-sm-12 pull-left" AssociatedControlID="GridView2" runat="server" Text="Appurtenant Assets"></asp:Label>
                                            <div class="col-sm-12">
                                                <asp:GridView ID="GridView2" runat="server"></asp:GridView>
                                                <%--<asp:Table ID="AppurtenantAssets" runat="server">
                                                    <asp:TableHeaderRow ID="AppurtenantAssetsHeader">
                                                        <asp:TableHeaderCell ID="Code" Text="Code"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell ID="AssetNameInTableHeader" Text="Asset Name"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell ID="Cost" Text="Cost (in Lakh Rs)"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell ID="CompletionDateInTableHeader" Text="Completion Date"></asp:TableHeaderCell>
                                                    </asp:TableHeaderRow>
                                                    <asp:TableRow ID="AppurtenantAssetsRow1"></asp:TableRow>
                                                </asp:Table>--%>
                                            </div>
                                        </div>
                                       
                                    </fieldset>
                                </div>

                                <div>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border"><u>Technical Specification: Porcupine<%--<asp:Literal runat="server" ID="FieldSetHeading" />--%></u></legend>

                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <asp:Label ID="ScreenLengthLabel" CssClass="control-label col-sm-6" AssociatedControlID="ScreenLength" runat="server" Text="Screen Length"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="ScreenLength" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="SpacingAlongScreenLabel" CssClass="control-label col-sm-6" AssociatedControlID="SpacingAlongScreen" runat="server" Text="Spacing Along Screen"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="SpacingAlongScreen" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="ScreenRowsLabel" CssClass="control-label col-sm-6" AssociatedControlID="ScreenRows" runat="server" Text="Screen Rows"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="ScreenRows" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="NoOfLayersLabel" CssClass="control-label col-sm-6" AssociatedControlID="NoOfLayers" runat="server" Text="No Of Layers"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="NoOfLayers" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="MamberLengthLabel" CssClass="control-label col-sm-6" AssociatedControlID="MamberLength" runat="server" Text="Mamber Length"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="MamberLength" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="PorcupineLengthLabel" CssClass="control-label col-sm-6" AssociatedControlID="PorcupineLengthDropDown" runat="server" Text="Porcupine Length"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:DropDownList ID="PorcupineLengthDropDown" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="None"></asp:ListItem>                                                       
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="MaterialTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="MaterialTypeDropDown" runat="server" Text="Material Type"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:DropDownList ID="MaterialTypeDropDown" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="None"></asp:ListItem>                                                       
                                                    </asp:DropDownList>
                                                </div>
                                            </div>                                                       

                                       </div>
                                    </fieldset>
                                </div>

                                <div>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border"><u>Technical Specification: Revetment<%--<asp:Literal runat="server" ID="FieldSetHeading" />--%></u></legend>

                                        <div class="col-sm-6 col-md-6">
                                           <div class="form-group">
                                                <asp:Label ID="RevetmentTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="RevetmentTypeDropDownList" runat="server" Text="Revetment Type"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:DropDownList ID="RevetmentTypeDropDownList" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="None"></asp:ListItem>                                                       
                                                    </asp:DropDownList>
                                                </div>
                                            </div>                                             

                                             <div class="form-group">
                                                <asp:Label ID="PlainWidthLabel" CssClass="control-label col-sm-6" AssociatedControlID="PlainWidth" runat="server" Text="Plain Width(m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="PlainWidth" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="WaveProtectionTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="WaveProtectionTypeDropDownList" runat="server" Text="Wave Protection Type"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:DropDownList ID="WaveProtectionTypeDropDownList" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="None"></asp:ListItem>                                                       
                                                    </asp:DropDownList>
                                                </div>
                                            </div>                                      

                                              <div class="form-group">
                                                <asp:Label ID="RiverProtectionTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="RiverProtectionTypeDropDownList" runat="server" Text="River Protection Type"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:DropDownList ID="RiverProtectionTypeDropDownList" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="None"></asp:ListItem>                                                       
                                                    </asp:DropDownList>
                                                </div>
                                            </div> 
                                        </div>
                                    </fieldset>
                                </div>

                                <div>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border"><u>Technical Specification: Culvert Type<%--<asp:Literal runat="server" ID="FieldSetHeading" />--%></u></legend>

                                        <div class="col-sm-6 col-md-6">
                                           <div class="form-group">
                                                <asp:Label ID="CulvertTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="CulvertTypeDropDownList" runat="server" Text="Culvert Type"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:DropDownList ID="CulvertTypeDropDownList" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="None"></asp:ListItem>                                                       
                                                    </asp:DropDownList>
                                                </div>
                                            </div>                                             

                                             <div class="form-group">
                                                <asp:Label ID="NumberOfVentsLabel" CssClass="control-label col-sm-6" AssociatedControlID="NumberOfVents" runat="server" Text="Number Of Vents"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="NumberOfVents" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="VentHeightLabel" CssClass="control-label col-sm-6" AssociatedControlID="VentHeight" runat="server" Text="Vent Height"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:TextBox ID="VentHeight" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>                                      

                                              <div class="form-group">
                                                <asp:Label ID="VentWidthLabel" CssClass="control-label col-sm-6" AssociatedControlID="VentWidth" runat="server" Text="Vent Width"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:TextBox ID="VentWidth" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div> 
                                        </div>
                                    </fieldset>
                                </div>

                                <div>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border"><u>Technical Specification: Drainage<%--<asp:Literal runat="server" ID="FieldSetHeading" />--%></u></legend>

                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <asp:Label ID="USBedElevationDrainageLabel" CssClass="control-label col-sm-6" AssociatedControlID="USBedElevationDrainage" runat="server" Text="U/S Bed Elevation(m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="USBedElevationDrainage" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="DSBedElevationDrainageLabel" CssClass="control-label col-sm-6" AssociatedControlID="DSBedElevationDrainage" runat="server" Text="D/S Bed Elevation(m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="DSBedElevationDrainage" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="LengthDrainageLabel" CssClass="control-label col-sm-6" AssociatedControlID="LengthDrainage" runat="server" Text="Length (m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="LengthDrainage" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="BedWidthDrainageLabel" CssClass="control-label col-sm-6" AssociatedControlID="BedWidthDrainage" runat="server" Text="Bed Width (m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="BedWidthDrainage" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="SlopeLabel" CssClass="control-label col-sm-6" AssociatedControlID="Slope" runat="server" Text="Slope"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="Slope" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>                                                                                                       
                                            </div>
                                        <div class="col-sm-6 col-md-6">
                                            <asp:Label ID="Label8" CssClass="control-label col-sm-12 pull-left" AssociatedControlID="GridView3" runat="server" Text="Appurtenant Assets"></asp:Label>
                                            <div class="col-sm-12">
                                                <asp:GridView ID="GridView3" runat="server"></asp:GridView>
                                                <%--<asp:Table ID="AppurtenantAssets" runat="server">
                                                    <asp:TableHeaderRow ID="AppurtenantAssetsHeader">
                                                        <asp:TableHeaderCell ID="Code" Text="Code"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell ID="AssetNameInTableHeader" Text="Asset Name"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell ID="Cost" Text="Cost (in Lakh Rs)"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell ID="CompletionDateInTableHeader" Text="Completion Date"></asp:TableHeaderCell>
                                                    </asp:TableHeaderRow>
                                                    <asp:TableRow ID="AppurtenantAssetsRow1"></asp:TableRow>
                                                </asp:Table>--%>
                                            </div>
                                        
                                       </div>
                                    </fieldset>
                                </div>

                                <div>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border"><u>Technical Specification: Gauge<%--<asp:Literal runat="server" ID="FieldSetHeading" />--%></u></legend>

                                        <div class="col-sm-6 col-md-6">
                                           <div class="form-group">
                                                <asp:Label ID="GaugeTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="GaugeTypeDropDownList" runat="server" Text="Gauge Type"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:DropDownList ID="GaugeTypeDropDownList" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="None"></asp:ListItem>                                                       
                                                    </asp:DropDownList>
                                                </div>
                                            </div>                                             

                                            <div class="form-group">
                                                <asp:Label ID="SeasonTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="SeasonTypeDropDownList" runat="server" Text="Season Type"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:DropDownList ID="SeasonTypeDropDownList" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="None"></asp:ListItem>                                                       
                                                    </asp:DropDownList>
                                                </div>
                                            </div> 

                                            <div class="form-group">
                                                <asp:Label ID="FrequencyTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="FrequencyTypeDropDownList" runat="server" Text="Frequency Type"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:DropDownList ID="FrequencyTypeDropDownList" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="None"></asp:ListItem>                                                       
                                                    </asp:DropDownList>
                                                </div>
                                            </div> 

                                             <div class="form-group">
                                                <asp:Label ID="StartDateLabel" CssClass="control-label col-sm-6" AssociatedControlID="StartDate" runat="server" Text="Start Date"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="StartDate" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="EndDateLabel" CssClass="control-label col-sm-6" AssociatedControlID="EndDate" runat="server" Text="End Date"></asp:Label>
                                                <div class="col-sm-6">
                                                   <asp:TextBox ID="EndDate" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>                                      

                                              <div class="form-group">
                                                <asp:Label ID="ActiveLabel" CssClass="control-label col-sm-6" AssociatedControlID="Active" runat="server" Text="Active"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:CheckBox ID="Active" runat="server"/>                                                
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="LWLPresentLabel" CssClass="control-label col-sm-6" AssociatedControlID="LWLPresent" runat="server" Text="LWL Present?"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:CheckBox ID="LWLPresent" runat="server"/>                                                
                                                </div>
                                            </div> 

                                            <div class="form-group">
                                                <asp:Label ID="HWLPresentLabel" CssClass="control-label col-sm-6" AssociatedControlID="HWLPresent" runat="server" Text="HWL Present?"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:CheckBox ID="HWLPresent" runat="server" />                                                
                                                </div>
                                            </div> 

                                            <div class="form-group">
                                                <asp:Label ID="LevelZeoLabel" CssClass="control-label col-sm-6" AssociatedControlID="LevelZeo" runat="server" Text="Level Zeo(Y/N)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:CheckBox ID="LevelZeo" runat="server" />                                                
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <asp:Label ID="ZeroDatumLabel" CssClass="control-label col-sm-6" AssociatedControlID="ZeroDatum" runat="server" Text="Zero Datum"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="ZeroDatum" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>                                               
                                                </div>
                                            </div>  
                                        </div>
                                    </fieldset>
                                </div>

                                <div>
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border"><u>Technical Specification: Sluice<%--<asp:Literal runat="server" ID="FieldSetHeading" />--%></u></legend>

                                        <div class="col-sm-12 col-md-12">
                                             <div class="col-sm-6 col-md-6">
                                             <div class="form-group">
                                                <asp:Label ID="SluiceTypeLabel" CssClass="control-label col-sm-6" AssociatedControlID="SluiceType" runat="server" Text="Sluice Type"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="SluiceType" runat="server" CssClass="form-control">
                                                         <asp:ListItem Text="None"></asp:ListItem> 
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-sm-6 col-md-6">
                                                <div class="form-group">
                                                <asp:Label ID="VentHeightSluiceLabel" CssClass="control-label col-sm-6" AssociatedControlID="VentHeightSluice" runat="server" Text="Vent Height (m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="VentHeightSluice" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                                </div>
                                             </div>

                                            <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <asp:Label ID="NoOfVentsSluiceLabel" CssClass="control-label col-sm-6" AssociatedControlID="NoOfVentsSluice" runat="server" Text="No Of Vents"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="NoOfVentsSluice" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <asp:Label ID="VentWidthSluiceLabel" CssClass="control-label col-sm-6" AssociatedControlID="VentWidthSluice" runat="server" Text="Vent Width (m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="VentWidthSluice" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <asp:Label ID="VentDiameterLabel" CssClass="control-label col-sm-6" AssociatedControlID="VentDiameter" runat="server" Text="Vent Diameter(m)"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="VentDiameter" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            </div>                                        
                                            
                                            <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <div class="col-sm-3 col-md-3">
                                                      <asp:Button ID="AddButton" runat="server" Text="Add"></asp:Button>
                                                </div>
                                                <div class="col-sm-3 col-md-3">
                                                    <asp:Button ID="DeleteButton" runat="server" Text="Delete"></asp:Button>
                                                </div>
                                            </div>
                                            </div>
                                                                                      
                                       </div>
                                                                                        
                                        <div class="col-sm-12 col-md-12">
                                            <asp:Label ID="Label6" CssClass="control-label col-sm-12 pull-left" AssociatedControlID="GridView4" runat="server" Text="Appurtenant Assets"></asp:Label>
                                            <div class="col-sm-12">
                                                <asp:GridView ID="GridView4" runat="server"></asp:GridView>
                                                <%--<asp:Table ID="AppurtenantAssets" runat="server">
                                                    <asp:TableHeaderRow ID="AppurtenantAssetsHeader">
                                                        <asp:TableHeaderCell ID="Code" Text="Code"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell ID="AssetNameInTableHeader" Text="Asset Name"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell ID="Cost" Text="Cost (in Lakh Rs)"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell ID="CompletionDateInTableHeader" Text="Completion Date"></asp:TableHeaderCell>
                                                    </asp:TableHeaderRow>
                                                    <asp:TableRow ID="AppurtenantAssetsRow1"></asp:TableRow>
                                                </asp:Table>--%>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>

                                <asp:Button ID="Button1" runat="server" Text="Print" Width="62px" OnClick="Button1_Click"/>                                           
                                        
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>        
        
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">

</asp:Content>