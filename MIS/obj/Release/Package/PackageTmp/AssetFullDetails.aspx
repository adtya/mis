﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AssetFullDetails.aspx.cs" Inherits="MIS.AssetFullDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="CustomStyles/onm.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>Asset Full Details</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server">
                        <fieldset>
                           <fieldset class="scheduler-border">
                                        <legend class="scheduler-border">Filter Enable</legend>
                                <div class="row">
                                    <div class="col-sm-4 col-md-4">
                                        <div class="form-group">
                                            <asp:Label ID="WRDCircle" CssClass="control-label col-sm-6" runat="server" Text="WRD Circle"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="WRDCircleDrop" runat="server" CssClass="form-control">
                                                    <asp:ListItem Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-1 col-md-1"></div>
                                    <div class="col-sm-4 col-md-6">
                                        <div class="form-group">
                                            <asp:Label ID="InitYear" CssClass="control-label col-sm-6" AssociatedControlID="InitYearText" runat="server" Text="Initiation Year"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:Textbox ID="InitYearText" runat="server" CssClass="form-control">                                                    
                                                </asp:Textbox>
                                            </div>
                                        </div>
                                    </div>
                                    </div>                                    

                                 <div class="row">
                                    <div class="col-sm-4 col-md-4">
                                        <div class="form-group">
                                            <asp:Label ID="WRDDivision" CssClass="control-label col-sm-6" runat="server" Text="WRD Division"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="WRDDivisionDrop" runat="server" CssClass="form-control">
                                                    <asp:ListItem Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-1 col-md-1"></div>
                                    <div class="col-sm-4 col-md-6">
                                        <div class="form-group">
                                            <asp:Label ID="YearLabel" CssClass="control-label col-sm-6"  AssociatedControlID="YearText" runat="server" Text="Year Of Completion"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:Textbox ID="YearText" runat="server" CssClass="form-control">                                                   
                                                </asp:Textbox>
                                            </div>
                                        </div>
                                    </div>                                                                      
                                </div>

                                <div class="row">
                                     <div class="col-sm-4 col-md-4">
                                        <div class="form-group">
                                            <asp:Label ID="Scheme" CssClass="control-label col-sm-6" runat="server" Text="Scheme"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="SchemeDrop" runat="server" CssClass="form-control">
                                                    <asp:ListItem Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-1 col-md-1"></div>
                                     <div class="col-sm-4 col-md-4">
                                        <div class="form-group">                                        
                                        </div>                                       
                                    </div>
                                </div>

                                <div class="row">
                                     <div class="col-sm-4 col-md-4">
                                        <div class="form-group">
                                            <asp:Label ID="AssetTypeLabel" CssClass="control-label col-sm-6" runat="server" Text="Asset Type"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="AssetTypeDrop" runat="server" CssClass="form-control">
                                                    <asp:ListItem Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-1 col-md-1"></div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="form-group">
                                            
                                            <div class="col-sm-4">
                                                <asp:Button ID="FilterButtton" runat="server" Text="Filter Results">                                                    
                                                </asp:Button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                               </fieldset>
                             
                                <div class="panel-body">                                    
                                        <asp:GridView ID="GridView1" runat="server" Width="870px">
                                            <%--<Columns>
                                                <asp:BoundField HeaderText="No." />
                                                <asp:BoundField HeaderText="Type" />
                                                <asp:BoundField HeaderText="Item" />
                                                <asp:BoundField HeaderText="Item Performance Description" />
                                                <asp:TemplateField HeaderText="Item Performance Status"></asp:TemplateField>
                                            </Columns>--%>
                                        </asp:GridView>
                                    
                                </div>                                    
                               
                               <asp:Button ID="btnPrint" runat="server" Text="Print"/>
                               <asp:Button ID="btnCancel" runat="server" Text="Cancel"/>
                           
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
</asp:Content>
