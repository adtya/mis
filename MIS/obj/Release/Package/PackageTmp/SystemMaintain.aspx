﻿<%@ Page Title="System Variables" Language="C#" MasterPageFile="~/DBA.Master" AutoEventWireup="true" CodeBehind="SystemMaintain.aspx.cs" Inherits="MIS.SystemMaintain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">   
    <link href="CustomStyles/ams.css" rel="stylesheet" type="text/css" />
    <link href="Content/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">    
    <script src="Scripts/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="Scripts/Datejs/build/date.js" type="text/javascript"></script>
    <script src="Scripts/date.format.js" type="text/javascript"></script>
    <script src="CustomScripts/set-system-variables.js" type="text/javascript"></script>
    <script src="CustomScripts/add-monitoring-item-type.js" type="text/javascript"></script>
    <script src="CustomScripts/add-monitoring-item.js" type="text/javascript"></script>
    
    
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>System Variables</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server" id="addSystemVariablesForm" name="addSystemVariablesForm">
                            <fieldset>                              
                                                         
                                <div class="panel panel-default">
                                    <div class="panel-body">                                                                   
                                
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                      
                                                    <div class="col-sm-8 col-md-8">
                                                         <div class="form-group">
                                                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                                                 <label for="fiscalYear" id="fiscalYearLabel" class="control-label">Fiscal Year</label>
                                                            </div>
                                                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                                                <%--<div id="dpFiscalYear" class="input-group date">
                                                                    <input type="text" name="fiscalYear" class="form-control" id="fiscalYear" placeholder="Fiscal Year(dd/mm/yyyy)"/>
                                                                         <span class="input-group-addon form-control-static">
                                                                             <i class="fa fa-calendar fa-fw"></i>
                                                                         </span>
                                                                </div>--%>
                                                                <asp:Literal ID="ltSystemVariableGuid" runat="server"></asp:Literal>
                                                                <asp:Literal ID="ltFiscalYear" runat="server"></asp:Literal>
                                                            </div> 
                                                                
                                                            </div>
                                                    </div>
                                             </div>                                                    
                                         </div>                                                 
                                
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                                
                                                    <div class="col-sm-8 col-md-8">
                                                        <div class="form-group">
                                                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                                                <label for="currentMonitoringDate" id="currentMonitoringDateLabel" class="control-label">Current Monitoring Date</label>
                                                            </div>
                                                           <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">                                                               
                                                               <asp:Literal ID="ltQuarterNumber" runat="server"></asp:Literal>                                                               
                                                               <asp:Literal ID="ltCurrentMonitoringDate" runat="server"></asp:Literal>                                                                                                       
                                                            </div>
                                                         </div>
                                                     </div>                                                    
                                            </div>                          
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                                
                                                    <div class="col-sm-8 col-md-8">
                                                        <div class="form-group">
                                                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                                                <label for="selectCircle" id="selectCircleLabel" class="control-label">Select Circle</label>
                                                            </div>
                                                           <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">  
                                                                <asp:Literal ID="ltSelectCircle" runat="server"></asp:Literal>                                                                                             
                                                            </div>
                                                         </div>
                                                     </div>                                                    
                                            </div>                          
                                        </div>
                                    </div>
                                </div>                                     
                                
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary" aria-hidden="true" id="saveSystemVariablesDateBtn">Save</button>
                                            <button class="btn btn-primary" aria-hidden="true" id="cancelSystemVariablesDateBtn">Cancel</button>                                            
                                        </div>
                                    </div>                     
                                </div>                              
                                        
                            </fieldset>
                            <button id="addMonitoringItemTypeBtn" class="btn btn-lg btn-primary">Add Item Type</button>
                            <button id="addMonitoringItemBtn" class="btn btn-lg btn-primary" type="button">Add Item</button>                          
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
     
    <div id="addMonitoringItemTypePopupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header" style="display:none"></div>                  

                    <div class="modal-body">

                    </div>

                    <div class="modal-footer">
                        <div class="col-md-12">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    <div id="addMonitoringItemPopupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header" style="display:none"></div>
                                       
                    <div class="modal-body">
                        <div class="container-fluid col-md-12">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="text-center">
                                                <img src="Images/add-circle.png" class="login" height="70" />
                                                <h2 class="text-center">Create Item</h2>
                                                <div class="panel-body">
                                                    <form id="addMonitoringItemPopupForm" name="addMonitoringItemPopupForm" class="form form-horizontal" method="post"><!--start form--><!--add form action as needed-->
                                                        <fieldset>
                                                            <div class="form-group">
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                    <label for="assetTypeToGetMonitoringItemTypeList" id="assetTypeToGetMonitoringItemTypeListLabel" class="control-label">Select Asset Type</label>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                    <select class="form-control" name="assetTypeToGetMonitoringItemTypeList" id="assetTypeToGetMonitoringItemTypeList"><option></option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                    <label for="monitoringItemTypeList" id="monitoringItemTypeListLabel" class="control-label">Select Item Type</label>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                    <select class="form-control" name="monitoringItemTypeListToAddItem" id="monitoringItemTypeListToAddItem">
                                                                        <option>-- Select Item Type --</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                    <label for="monitoringItemName" id="monitoringItemNameLabel" class="control-label">Item Name</label>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                    <input id="monitoringItemName" name="monitoringItemName" placeholder="Item Name" class="form-control text-capitalize" />
                                                                </div>
                                                            </div>  
                                                                                                                      
                                                            <div class="form-group">
                                                                <div class="col-xs-4 col-xs-offset-4 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
                                                                    <input id="addMonitoringItemDataBtn" class="btn btn-lg btn-primary btn-block" value="Add Item" type="submit" />
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </form><!--/end form-->
                                                </div>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <div class="col-md-12">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    


    


    <%--<div id="systemVariablesPopupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header" style="display:none"></div>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h1 class="text-center">What's My Password?</h1>
                        </div>

                    <div class="modal-body">
                        <div class="container-fluid col-md-12">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="text-center">
                                                <img src="Images/add-circle.png" class="login" height="70" />
                                                <h2 class="text-center">Set Current System Variables</h2>

                                                <div class="panel-body">
                                                    <form id="systemVariablesPopupForm" class="form form-horizontal" method="post"><!--start form--><!--add form action as needed-->
                                                        <fieldset>
                                                            <div class="form-group">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <label for="fiscalYear" id="fiscalYearLabel" class="control-label"><a href="http://www.google.com/">Fiscal Year</a></label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <label for="currentMonitoringYear" id="currentMonitoringYearLabel" class="control-label"><a href="http://www.google.com/">Current Monitoring Year</a></label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <label for="selectCircle" id="selectCircleLabel" class="control-label"><a href="http://www.google.com/">Select Circle</a></label>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </form><!--/end form-->
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="col-md-12">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
     </div> --%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
</asp:Content>

