﻿<%@ Page Language="C#" Title="Field Survey" MasterPageFile="~/DBA.Master" AutoEventWireup="true" CodeBehind="FieldSurvey.aspx.cs" Inherits="MIS.FieldSurvey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">   
    <link href="CustomStyles/ams.css" rel="stylesheet" type="text/css" />   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server"> 

    <script src="CustomScripts/field-survey.js" type="text/javascript"></script> 
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Field Survey Cost</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" runat="server" id="fieldSurveyForm" name="fieldSurveyForm">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                    <label for="fiscalYear" id="fiscalYearLabel" class="control-label">Fiscal Year</label>
                                                </div>
                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                    <asp:Literal ID="ltFiscalYear" runat="server"></asp:Literal>
                                                    <%--<input name="fiscalYear" class="form-control" id="fiscalYear" readonly="readonly" />--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <label for="circleCode" id="circleCodeLabel" class="control-label">Circle</label>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <asp:Literal ID="ltCircleGuid" runat="server"></asp:Literal>
                                                    <asp:Literal ID="ltCircleCode" runat="server"></asp:Literal>
                                                    <%--<input id="circleGuid" name="circleGuid" class="form-control" readonly="readonly" style="display:none" />                                                            
                                                                 <input id="circleCode" name="circleCode" class="form-control" readonly="readonly" />--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                         
                                 </div>

                                <div class="panel panel-default">
                                      <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                        
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                 <label for="assetCode" id="assetCodeLabel" class="control-label">Asset Code</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                                 <asp:Literal ID="ltAssetCodeForFieldSurvey" runat="server"></asp:Literal>
                                                            </div>
                                                        </div>
                                                    </div>
                                             </div>  
                                        </div>
                                      </div>
                                </div>
                                                         
                                <div class="panel panel-default">
                                    <div class="panel-body">                                                                 
                                
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                      
                                                    <div class="col-sm-6 col-md-6">
                                                         <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                 <label for="assetTypeCode" id="assetTypeCodeLabel" class="control-label">Asset Type</label>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                                                                 <input id="assetTypeName" name="assetTypeName" readonly="readonly" class="form-control" />
                                                                 <input id="assetTypeGuid" name="assetTypeGuid" style="display:none" class="form-control" />
                                                            </div>
                                                         </div>
                                                   </div>

                                                    <div class="col-sm-6 col-md-6">
                                                          <div class="form-group">
                                                              <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                 <label for="assetCodeText" id="assetCodeTextLabel" class="control-label">Asset Code</label>
                                                              </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                                                                <input id="assetCode" name="assetCode" readonly="readonly" class="form-control" />
                                                            </div>                                                
                                                          </div>
                                                    </div>
                                                </div>
                                            </div>        
                                
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">                                                
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                                <label for="assetName" id="assetNameLabel" class="control-label">Asset Name</label>
                                                            </div>
                                                           <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                                                                <input id="assetName" name="assetName" readonly="readonly" class="form-control" />                                                                                                        
                                                            </div>
                                                         </div>
                                                     </div>
                                            </div>                          
                                        </div>

                                      </div>
                                </div>

                                                                                       

                                <fieldset class="scheduler-border" id="fieldSurveyCostFieldset" name="fieldSurveyCostFieldset" hidden="hidden">
                                           <legend class="scheduler-border" >Field Survey Cost </legend>                                 
                                            <div class="form-group" id="fieldSurveyCostFieldsetPanel"></div>                                                                               
                                </fieldset> 
                                       
                                
                                <%--<div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary" aria-hidden="true" id="saveFieldSurveyBtn">Save</button>
                                            <button class="btn btn-primary" aria-hidden="true" id="cancelFieldSurveyBtn">Cancel</button>
                                        </div>
                                    </div>                     
                                </div>    --%>                          
                                        
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>  
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
</asp:Content>