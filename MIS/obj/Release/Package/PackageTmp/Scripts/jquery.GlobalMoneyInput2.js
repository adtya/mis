/*
* @Copyright (c) 2010 Renan Viegas (contact@renanviegas.com.br)
* @Page http://www.renanviegas.com.br/dev/jquery/
*
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without
* restriction, including without limitation the rights to use,
* copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following
* conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
*/

/*
* @Version: 1.0
* @Release: 2010-06-24
*/
(function ($) {
    $.fn.maskMoney = function (container, settings) {
        settings = $.extend({
            symbol: $.culture.numberFormat.currency.symbol,
            decimal: $.culture.numberFormat['.'],
            precision: $.culture.numberFormat.decimals,
            thousands: $.culture.numberFormat[','],
            showSymbol: true
        }, settings);

        settings.symbol = settings.symbol + " ";

        return this.each(function () {
            var input = $(this);
            function money(e) {
                e = e || window.event;
                var k = e.charCode || e.keyCode || e.which;
                if (k == 8) { // tecla backspace
                    preventDefault(e);
                    var x = input.val().substring(0, input.val().length - 1);
                    input.val(maskValue(x));
                    return false;
                } else if (k == 9) { // tecla tab
                    return true;
                }
                if (k < 48 || k > 57) {
                    preventDefault(e);
                    return true;
                }
                var key = String.fromCharCode(k);  // Valor para o código da Chave
                preventDefault(e);
                input.val(maskValue(input.val() + key));
            }

            function preventDefault(e) {
                if (e.preventDefault) { //standart browsers
                    e.preventDefault()
                } else { // internet explorer
                    e.returnValue = false
                }
            }

            function maskValue(v) {
                v = v.replace(settings.symbol, "");
                var a = '';
                var strCheck = '0123456789';
                var len = v.length;
                var t = "";
                if (len == 0) {
                    t = "0.00";
                }
                for (var i = 0; i < len; i++)
                    if ((v.charAt(i) != '0') && (v.charAt(i) != settings.decimal))
                        break;

                for (; i < len; i++) {
                    if (strCheck.indexOf(v.charAt(i)) != -1) a += v.charAt(i);
                }

                var n = parseFloat(a);
                n = isNaN(n) ? 0 : n / Math.pow(10, settings.precision);
                t = n.toFixed(settings.precision);

                d = (t = t.split("."))[1].substr(0, settings.precision);

                var numStr = numToWord(n, parseFloat(t[0]), parseFloat(d));

                t = $.format(parseFloat(t[0]), 'n0');

                $('#' + container).text(numStr);

                return setSymbol(t + settings.decimal + d + Array(
					(settings.precision + 1) - d.length).join(0));
            }

            function focusEvent() {
                if (input.val() == "") {
                    input.val(setSymbol(getDefaultMask()));
                } else {
                    input.val(setSymbol(input.val()));
                }
            }

            function blurEvent() {
                if (input.val() == setSymbol(getDefaultMask())) {
                    input.val("");
                } else {
                    input.val(input.val().replace(settings.symbol, ""))
                }
            }

            function getDefaultMask() {
                var n = parseFloat("0") / Math.pow(10, settings.precision);
                return (n.toFixed(settings.precision)).replace(
					new RegExp("\\.", "g"), settings.decimal);
            }

            function setSymbol(v) {
                if (settings.showSymbol) {
                    return settings.symbol + v;
                }
                return v;
            }

            function numToWord(value, integer, fraction) {
                if (value < 0) {
                    return 'NUMBER CANNOT BE LESS THAN ZERO.';
                }
                else {
                    var i_text = "";
                    var f_text = "";

                    i_text = convert_number(integer) + " RUPEES";

                    //if (integer > 0) {
                    //    i_text = convert_number(integer) + " RUPEES";
                    //} else if (integer === 0) {
                    //    i_text = convert_number(integer) + " RUPEES";
                    //}

                    if (fraction > 0) {
                        if (integer > 0) {
                            f_text = " AND ";
                        } else {
                            i_text = "";
                        }

                        f_text += convert_number(fraction) + " PAISE";
                    }

                    //var fraction = Math.round(frac(value) * 100);
                    //var f_text = "";

                    //if (fraction > 0) {
                    //    f_text = "AND " + convert_number(fraction) + " PAISE";
                    //}

                    return i_text + f_text + " ONLY.";
                }
            }

            function frac(f) {
                return f % 1;
            }

            function convert_number(number) {
                //if ((number < 0) || (number > 999999999)) {
                //    return "NUMBER OUT OF RANGE!";
                //}
                var Gn = Math.floor(number / 10000000);  /* Crore */
                number -= Gn * 10000000;
                var kn = Math.floor(number / 100000);     /* lakhs */
                number -= kn * 100000;
                var Hn = Math.floor(number / 1000);      /* thousand */
                number -= Hn * 1000;
                var Dn = Math.floor(number / 100);       /* Tens (deca) */
                number = number % 100;               /* Ones */
                var tn = Math.floor(number / 10);
                var one = Math.floor(number % 10);
                var res = "";

                if (Gn > 0) {
                    res += (convert_number(Gn) + " CRORE");
                }
                if (kn > 0) {
                    res += (((res == "") ? "" : " ") +
                    convert_number(kn) + " LAKH");
                }
                if (Hn > 0) {
                    res += (((res == "") ? "" : " ") +
                        convert_number(Hn) + " THOUSAND");
                }

                if (Dn) {
                    res += (((res == "") ? "" : " ") +
                        convert_number(Dn) + " HUNDRED");
                }


                var ones = Array("", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN");
                var tens = Array("", "", "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY");

                if (tn > 0 || one > 0) {
                    if (!(res == "")) {
                        res += " AND ";
                    }
                    if (tn < 2) {
                        res += ones[tn * 10 + one];
                    }
                    else {

                        res += tens[tn];
                        if (one > 0) {
                            res += ("-" + ones[one]);
                        }
                    }
                }

                if (res == "") {
                    res = "ZERO";
                }
                return res;
            }

            input.bind("keypress", money);
            input.bind("blur", blurEvent);
            input.bind("focus", focusEvent);

            input.one("unmaskMoney", function () {
                input.unbind("focus", focusEvent);
                input.unbind("blur", blurEvent);
                input.unbind("keypress", money);
                if ($.browser.msie)
                    this.onpaste = null;
                else if ($.browser.mozilla)
                    this.removeEventListener('input', blurEvent, false);
                input.val(input.val().replace(settings.thousands, ''));
                input.val(input.val().replace(settings.decimal, '.'));
            });

        });
    }

    $.fn.unmaskMoney = function () {
        return this.trigger("unmaskMoney");
    };
})(jQuery);