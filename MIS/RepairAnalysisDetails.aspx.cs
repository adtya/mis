﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class RepairAnalysisDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "DBA")
                {
                    if (!Page.IsPostBack)
                    {
                        DisplayAvailableFunds();
                        //DisplayAssetCodeList();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
            }

        }
 
        private void DisplayAvailableFunds()
        {
            RepairFundingManager repairFundingManagerObj = new RepairFundingManager();
            repairFundingManagerObj = new RepairFundingDB().GetRepairFundingDataForRepairAnalyis();
            ltFiscalYear.Text = "<label id=\"fiscalYearValue\" class=\"control-label\" >" + repairFundingManagerObj.FiscalYear + "</label>";
            ltAvailableFunds.Text = "<label id=\"availableFunds\" class=\"control-label\" >" + "Available Funds for FY " + repairFundingManagerObj.FiscalYear + "</label>";
            ltAvailableFundsValue.Text = "<label id=\"availableFundsValue\" class=\"control-label\" >" + repairFundingManagerObj.AvailableFunds + "</label>";
        }

        [WebMethod]
        public static List<AssetManager> DisplayAssetCodeList()
        {
            List<AssetManager> assetCodeManagerList = new List<AssetManager>();
            assetCodeManagerList = new AssetDB().GetAssetCodeList();
            return assetCodeManagerList;
        }

        [WebMethod]
        public static AssetCodeAndAssetRiskRatingRelationManager GetAssetRiskRatingNumber(string assetGuid)
        {
            AssetCodeAndAssetRiskRatingRelationManager assetRiskRatingRelationForAssetCodeManagerList = new AssetCodeAndAssetRiskRatingRelationManager();
            assetRiskRatingRelationForAssetCodeManagerList = new AssetCodeAndAssetRiskRatingRelationDB().GetAssetRiskRatingForAssetCode(new Guid(assetGuid));
            return assetRiskRatingRelationForAssetCodeManagerList;
        }

        [WebMethod]
        public static SchemeCodeAndSchemeRiskRatingRelationManager GetSchemeRiskRatingNumber(string assetGuid)
        {
            SchemeCodeAndSchemeRiskRatingRelationManager schemeRiskRatingRelationForAssetCode = new SchemeCodeAndSchemeRiskRatingRelationManager();
            schemeRiskRatingRelationForAssetCode = new SchemeCodeAndSchemeRiskRatingRelationDB().GetSchemeRiskRatingNumber(new Guid(assetGuid));
            return schemeRiskRatingRelationForAssetCode;
        }

        [WebMethod]
        public static List<ProbabilityFailureManager> GetProbabilityFailure()
        {
            List<ProbabilityFailureManager> probabilityFailureManagerList = new List<ProbabilityFailureManager>();
            probabilityFailureManagerList = new ProbabilityFailureDB().GetProbabilityFailure();
            return probabilityFailureManagerList;
        }

        [WebMethod]
        public static List<RepairAnalysisManager> GetRepairAnalysisData()
        {
            List<RepairAnalysisManager> repairAnalysisManagerList = new List<RepairAnalysisManager>();
            repairAnalysisManagerList = new RepairAnalysisDB().GetRepairAnalysisData();
            return repairAnalysisManagerList;
        }

        [WebMethod]
        public static List<CurrentAssetFunctionalityManager> GetCurrentAssetFunctionality()
        {
            List<CurrentAssetFunctionalityManager> currentAssetFunctionalityManagerList = new List<CurrentAssetFunctionalityManager>();
            currentAssetFunctionalityManagerList = new CurrentAssetFunctionalityDB().GetCurrentAssetFunctionality();
            return currentAssetFunctionalityManagerList;
        }

        [WebMethod]
        public static List<RepairAnalysisStatusManger> GetRepairAnalysisStatus()
        {
            List<RepairAnalysisStatusManger> repairAnalysisStatusManagerList = new List<RepairAnalysisStatusManger>();
            repairAnalysisStatusManagerList = new RepairAnalysisStatusDB().GetRepairAnalysisStatus();
            return repairAnalysisStatusManagerList;
        }

        [WebMethod]
        public static Guid SaveRepairAnalysisData(RepairAnalysisManager repairAnalysisManager)
        {
            repairAnalysisManager.RepairAnalysisGuid = Guid.NewGuid();
            int result = new RepairAnalysisDB().SaveRepairAnalysisData(repairAnalysisManager);
            if (result == 1)
            {
                return repairAnalysisManager.RepairAnalysisGuid;
            }
            else
            {
                return new Guid();
            }
        }

        [WebMethod]
        public static int UpdateRepairAnalysisData(string repairAnalysisGuid, RepairAnalysisManager repairAnalysisManager)
        {
            return new RepairAnalysisDB().UpdateRepairAnalysisData(new Guid(repairAnalysisGuid), repairAnalysisManager);
        }

        [WebMethod]
        public static int DeleteRepairAnalysisData(Guid repairAnalysisGuid)
        {
            return new RepairAnalysisDB().DeleteRepairAnalysisData(repairAnalysisGuid);
        }
    }
}