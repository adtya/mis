﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class ONM : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            

            DataRow dr = null;

            dt.Columns.Add(new DataColumn("No.", typeof(string)));
            dt.Columns.Add(new DataColumn("Type", typeof(string)));
            dt.Columns.Add(new DataColumn("Item", typeof(string)));
            dt.Columns.Add(new DataColumn("Item Performance Description", typeof(string)));
            dt.Columns.Add(new DataColumn("Item Performance Status", typeof(string)));
          
           dr = dt.NewRow();

            dr["No."] = "1.01";
            dr["Type"] = "Structural";
            dr["Item"] = "Crest";
            dr["Item Performance Description"] = "";
            dr["Item Performance Status"] = "";

            dt.Rows.Add(dr);


            dr = dt.NewRow();

            dr["No."] = "1.02";
            dr["Type"] = "Structural";
            dr["Item"] = " Road Drainage";
            dr["Item Performance Description"] = "";
            dr["Item Performance Status"] = "";

            dt.Rows.Add(dr);

            dr = dt.NewRow();

            dr["No."] = "2.01";
            dr["Type"] = "Failure Modes";
            dr["Item"] = "Freeboard";
            dr["Item Performance Description"] = "";
            dr["Item Performance Status"] = "";

            dt.Rows.Add(dr);

            dr = dt.NewRow();

            dr["No."] = "2.02";
            dr["Type"] = "Failure Modes";
            dr["Item"] = "Earth Settlement";
            dr["Item Performance Description"] = "";
            dr["Item Performance Status"] = "";

            dt.Rows.Add(dr);

            ViewState["CurrentTable"] = dt;

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            //This line of code creates a html file for the data export.
            string tempPath = Path.GetTempPath();
            tempPath = tempPath + "\\PerformanceMontioring.html";
            System.IO.StreamWriter file = new System.IO.StreamWriter(tempPath);

            try
            {
                string sLine = "<html><center><h1>O & M Performance Monitoring Details </h1></center><style>table, tr, td { border:0px}</style>";
                file.WriteLine(sLine);
                file.WriteLine("<table>");
                file.WriteLine("<tr>" + "<td width =\"200\">" + "Asset code" + "</td>" + "<td>" + AssetCode.Text + "</td>" + "<td width= \"200\">" + "</td>" + "<td  width =\"200\">" + "Reporting date" + "</td>" + "<td>" + CurrentMonitoringDate.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Asset type" + "</td>" + "<td>" + AssetType.Text + "</td>" + "<td width= \"200\">" + "</td>" + "<td>" + "Actual inspection date" + "</td>" + "<td>" + InspectionDate.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Asset name" + "</td>" + "<td>" + AssetName.Text + "</td>" + "<td width= \"200\">" + "</td>" + "<td>" + "Inspector name" + "</td>" + "<td>" + InspectorName.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "Overall status" + "</td>" + "<td>" + OverallStatus.Text + "</td>" + "<td width= \"200\">" + "</td>" + "<td>" + "Inspector name" + "</td>" + "<td>" + InspectorPosition.Text + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "General comment" + "</td>" + "<td colspan=\"4\">" + GeneralComments.Text + "</td>" + "</tr>");
                file.WriteLine("</table>");
                file.WriteLine("<h4>" + "Performance Checklist" + "</h4>");
                sLine = "<br><center><table cellspacing=\"0\" cellpadding=\"5\" style=" + "\"" + "border:1px solid black" + "\"" + " " + "align=" + "\"" + "center" + "\"" + " bgcolor=" + "\"" + "white" + "\"" + " width= " + "\"" + "100%" + "\"" + "><tr>";
                file.WriteLine(sLine);
                //This for loop places the column headers into the first row of the HTML table.
                file.WriteLine("<tr>" + "<td>" + "No" + "</td>" + "<td width =\"250\">" + "Type" + "</td>" + "<td>" + "Item" + "</td>" + "<td>" + "Item Performance Description" + "</td>" + "<td>" + "Item Performance Status" + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "1.01" + "</td>" + "<td width =\"250\">" + "Structural" + "</td>" + "<td>" + "Crest" + "</td>" + "<td>" + "Not Applicable" + "</td>" + "<td>" + "" + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "1.02" + "</td>" + "<td width =\"250\">" + "Structural" + "</td>" + "<td>" + "Road Drainage" + "</td>" + "<td>" + "Not Applicable" + "</td>" + "<td>" + "" + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "2.01" + "</td>" + "<td width =\"250\">" + "Failure Modes" + "</td>" + "<td>" + "Freeboard" + "</td>" + "<td>" + "Not Applicable" + "</td>" + "<td>" + "" + "</td>" + "</tr>");
                file.WriteLine("<tr>" + "<td>" + "2.02" + "</td>" + "<td width =\"250\">" + "Failure Modes" + "</td>" + "<td>" + "Earth Settlement" + "</td>" + "<td>" + "Not Applicable" + "</td>" + "<td>" + "" + "</td>" + "</tr>");
                
                sLine = "</table></body><br></html>";
                file.WriteLine(sLine);
                file.Close();
                System.Diagnostics.Process.Start(tempPath);

            }
            catch (System.Exception err)
            {
                file.Close();
            }
        }
    }
}