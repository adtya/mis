﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class AdminHome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "Admin")
                {
                    if (!Page.IsPostBack)
                    {
                        DisplayRoleList();
                        DisplayUserData();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                    //Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("DBALogin.aspx");
            }
        }

        private void DisplayRoleList()
        {
            List<RolesManager> roleList = new List<RolesManager>();
            roleList = new RolesDB().GetRoles();

            StringBuilder htmlRoleList = new StringBuilder();
            htmlRoleList.Append("<select class=\"form-control\" name=\"roleList\" id=\"roleList\">");
            htmlRoleList.Append("<option value=\"\">" + "  --Select User Role--  " + "</option>");
            if (roleList.Count > 0)
            {
                foreach (RolesManager roleObj in roleList)
                {
                    htmlRoleList.Append("<option value=\"" + roleObj.RoleGuid.ToString() + "\">" + roleObj.RoleName.ToString() + "</option>");
                }
            }
            htmlRoleList.Append("</select>");

            ltRoleList.Text = htmlRoleList.ToString();
        }

        private void DisplayUserData()
        {

            List<UserManager> userList = new UserDB().GetUserData();

            StringBuilder htmlTable = new StringBuilder();

            htmlTable.Append("<div id=" + "\"table-container\"" + " style=" + "\"padding:1%;\">");

            if (userList.Count > 0)
            {

                htmlTable.Append("<table id=\"userData\" class=\"table table-striped table-bordered table-hover table-responsive\" cellspacing=\"0\" width=\"100%\">");

                htmlTable.Append("<thead>");
                htmlTable.Append("<tr>");
                htmlTable.Append("<th>Name</th>");
                htmlTable.Append("<th>Email ID</th>");
                htmlTable.Append("<th>Role Name</th>");
                htmlTable.Append("<th>Select User</th>");
                htmlTable.Append("<th //style=\"display:none;\">User ID</th>");                
                htmlTable.Append("</tr>");
                htmlTable.Append("</thead>");

                htmlTable.Append("<tbody id=\"deleteUserTableBody\">");
                foreach (var item in userList)
                {
                    int index = userList.IndexOf(item);

                    htmlTable.Append("<tr>");
                    htmlTable.Append("<td>" + item.FirstName + "</td>");
                    htmlTable.Append("<td>" + item.EmailId + "</td>");
                    htmlTable.Append("<td>" + item.RoleManager.RoleName + "</td>");
                    htmlTable.Append("<td>" + "<input type=\"checkbox\" id=\"cb" + index + "\" />" + "</td>");
                    htmlTable.Append("<td class=\"userIDcell hidden\">" + item.UserGuid + "</td>");
                    htmlTable.Append("</tr>");
                }
                htmlTable.Append("<tbody>");
                htmlTable.Append("</table>");
            }
            else
            {
                htmlTable.Append("<p>No data available.</p>");
            }
            htmlTable.Append("</div>");
            ltUserList.Text = htmlTable.ToString();
        }

        [WebMethod]
        public static int SaveUserData(string userFirstName, string userMiddleName, string userLastName, string userEmailId, string userRoleGuid)
        {
            int rowUpdateStatus;
            Guid userGuid = Guid.NewGuid();
            string userPassword = new PasswordGenerator().CreateRandomPassword();
            try
            {
                rowUpdateStatus = new UserDB().CreateUser(userGuid, userFirstName, userMiddleName, userLastName, userEmailId, new Guid(userRoleGuid), userPassword);
                if (rowUpdateStatus == 1)
                {
                    string subject = "Successful Registration at MIS";
                    string body = "Hello" + " " + "\n";
                    body += "You have registered on MIS Successfully.\n";
                    body += "Please wait for the password of the account.\n";
                    body += "Thanks n Regards\n";
                    body += "FREMAA\n";
                    body += "This is a system generated mail. Please do not reply to this.";
                    new MailManager().SendMailToUser(userEmailId, subject, body);
                }
                return rowUpdateStatus;
            }
            catch
            {
                return 0;
            }
        }

        [WebMethod]
        public static bool DeleteUserData(string[] userGuidListToDelete)
        {
            List<Guid> userGuidList = new List<Guid>();

            foreach (string userGuidString in userGuidListToDelete)
            {
                userGuidList.Add(new Guid(userGuidString));
            }

            return new UserDB().DeleteUserData(userGuidList);
        }
    }

}