﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class SystemMaintain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "DBA")
                {
                    if (!Page.IsPostBack)
                    {
                        DisplayFiscalYear();
                        DisplayCurrentMonitoringDate();
                        DisplayCircleNames();
                        GetSystemVariables();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
            }
        }

        private void DisplayFiscalYear()
        {
            DateTime currentSystemDate = Convert.ToDateTime(DateTime.Now.ToString());
            string currentYear = currentSystemDate.Year.ToString();
            int backYear = currentSystemDate.AddYears(-8).Year;
            StringBuilder htmlFiscalYearList = new StringBuilder();

            htmlFiscalYearList.Append("<select class=\"form-control\" name=\"fiscalYearList\" id=\"fiscalYearList\">");
            htmlFiscalYearList.Append("<option value=\"\">" + "--Select Fiscal Year--" + "</option>");
            for (int numberOfFiscalYear = 1; numberOfFiscalYear <= 15; numberOfFiscalYear++)
            {
                int nextYear = backYear + 1;
                htmlFiscalYearList.Append("<option value=\"" + backYear + "/" + nextYear + "\">" + backYear + "/" + nextYear + "</option>");
                backYear = backYear + 1;
            }
            htmlFiscalYearList.Append("</select>");
            ltFiscalYear.Text = htmlFiscalYearList.ToString();

        }

        private void DisplayCurrentMonitoringDate()
        {
            DateTime currentYearDate = Convert.ToDateTime(DateTime.Now.ToString());
            DateTime backYear = currentYearDate.AddYears(-1);
            int quarter = (currentYearDate.Month + 2) / 3;
            int year = backYear.Year;

            StringBuilder htmlCurrentMonitoringDateList = new StringBuilder();
            htmlCurrentMonitoringDateList.Append("<select class=\"form-control\" name=\"currentMonitoringDateList\" id=\"currentMonitoringDateList\">");
            htmlCurrentMonitoringDateList.Append("<option value=\"\">" + "--Select Current Monitoring Date--" + "</option>");
            if (quarter == 1)
            {
                int startMonth = 3;
                for (int i = 1; i <= 9; i++)
                {
                    string currentMonitoringDate = new DateTime(year, startMonth, DateTime.DaysInMonth(year, startMonth)).ToString("dd/MM/yyyy");
                    htmlCurrentMonitoringDateList.Append("<option value=\"" + currentMonitoringDate + "\">" + currentMonitoringDate  + "</option>");
                    if (startMonth == 12)
                    {
                        year = year + 1;
                        startMonth = 3;
                    }
                    else
                    {
                        startMonth = startMonth + 3;
                    }
                }
            }
            else if (quarter == 2)
            {
                int startMonth = 6;
                for (int i = 1; i <= 9; i++)
                {
                    string currentMonitoringDate = new DateTime(year, startMonth, DateTime.DaysInMonth(year, startMonth)).ToString("dd/MM/yyyy");
                    htmlCurrentMonitoringDateList.Append("<option value=\"" + currentMonitoringDate + "\">" + currentMonitoringDate + "</option>");
                    if (startMonth == 12)
                    {
                        year = year + 1;
                        startMonth = 3;
                    }
                    else
                    {
                        startMonth = startMonth + 3;
                    }
                }
            }
            else if (quarter == 3)
            {
                int startMonth = 9;
                for (int i = 1; i <= 9; i++)
                {
                    string currentMonitoringDate = new DateTime(year, startMonth, DateTime.DaysInMonth(year, startMonth)).ToString("dd/MM/yyyy");
                    htmlCurrentMonitoringDateList.Append("<option value=\"" + currentMonitoringDate + "\">" + currentMonitoringDate + "</option>");
                    if (startMonth == 12)
                    {
                        year = year + 1;
                        startMonth = 3;
                    }
                    else
                    {
                        startMonth = startMonth + 3;
                    }
                }
            }
            else
            {
                int startMonth = 12;
                for (int i = 1; i <= 9; i++)
                {
                    string currentMonitoringDate = new DateTime(year, startMonth, DateTime.DaysInMonth(year, startMonth)).ToString("dd/MM/yyyy");
                    htmlCurrentMonitoringDateList.Append("<option value=\"" + currentMonitoringDate + "\">" + currentMonitoringDate + "</option>");
                    if (startMonth == 12)
                    {
                        year = year + 1;
                        startMonth = 3;
                    }
                    else
                    {
                        startMonth = startMonth + 3;
                    }
                }
            }
            htmlCurrentMonitoringDateList.Append("</select>");
            ltQuarterNumber.Text = "<input id=\"quarterNumber\" type=\"hidden\" value=\"" + quarter + "\" />";
            ltCurrentMonitoringDate.Text = htmlCurrentMonitoringDateList.ToString();
        }

        private void DisplayCircleNames()
        {
            List<CircleManager> circleList = new List<CircleManager>();

            circleList = new CircleDB().GetCircles();

            StringBuilder htmlCircleCodeList = new StringBuilder();

            htmlCircleCodeList.Append("<select class=\"form-control\" name=\"circleCodeList\" id=\"circleCodeList\">");
            htmlCircleCodeList.Append("<option value=\"\">" + "--Select Circle Code--" + "</option>");

            if (circleList.Count > 0)
            {
                foreach (CircleManager circleObj in circleList)
                {
                    htmlCircleCodeList.Append("<option value=\"" + circleObj.CircleGuid.ToString() + "\">" + circleObj.CircleCode.ToString() + "</option>");
                }
            }
            htmlCircleCodeList.Append("</select>");

            ltSelectCircle.Text = htmlCircleCodeList.ToString();
        }

        [WebMethod]
        public void GetSystemVariables()
        {
            SystemVariablesManager systemVariablesObj = new SystemVariablesManager();
            systemVariablesObj = new SystemVariablesDB().GetSystemVariablesGuid();
            ltSystemVariableGuid.Text = "<input id=\"systemVariablesGuid\" type=\"hidden\" value=\"" + systemVariablesObj.SystemVariablesGuid.ToString() + "\" />";
            
        }      

        [WebMethod]
        public static List<AssetTypeManager> GetAssetType()
        {
            List<AssetTypeManager> assetTypeManagerList = new List<AssetTypeManager>();
            assetTypeManagerList = new AssetTypeDB().GetAssetTypes();
            return assetTypeManagerList;
        }

        [WebMethod]
        public static List<MonitoringItemTypeManager> GetMonitoringItemTypes(string assetTypeGuid)
        {
            List<MonitoringItemTypeManager> monitoringItemTypeManagerList = new List<MonitoringItemTypeManager>();
            monitoringItemTypeManagerList = new MonitoringItemTypeDB().GetMonitoringItemType(new Guid(assetTypeGuid));
            return monitoringItemTypeManagerList;
        }

        [WebMethod]
        public static bool UpdateSystemVariable(string systemVariablesGuid, string fiscalYear, string currentMonitoringDate, string quarterNumber,string circleGuid)
        {
            DateTime date = Convert.ToDateTime(currentMonitoringDate);
            return new SystemVariablesDB().UpdateSystemVariables(new Guid(systemVariablesGuid), fiscalYear, date, Convert.ToInt16(quarterNumber), new Guid(circleGuid));
        }

        [WebMethod]
        public static int SaveMonitoringItemType(string monitoringItemTypeName, Guid assetTypeGuid)
        {
             return new MonitoringItemTypeDB().CreateMonitoringItemType(Guid.NewGuid(), monitoringItemTypeName,assetTypeGuid);                
        }

        [WebMethod]
        public static int SaveMonitoringItem(string monitoringItemName, string monitoringItemTypeGuid)
        {
            return new MonitoringItemDB().AddMonitoringItem(Guid.NewGuid(), monitoringItemName, new Guid(monitoringItemTypeGuid));
        }

              
      
        //[WebMethod]
        //public static void SaveSystemVariable(string fiscalYear, string currentMonitoringDate, string quarterNumber)
        //{
        //    DateTime date = Convert.ToDateTime(currentMonitoringDate);
        //    new SystemVariablesDB().AddSystemVariables(Guid.NewGuid(),fiscalYear,date,Convert.ToInt16(quarterNumber));
        //}

        //[WebMethod]
        //public static void SaveRelationOfAssetTypeWithMonitoringType(string assetTypeGuid, string monitoringItemTypeGuid)
        //{
        //    performanceMonitoringManager.PerformanceMonitoringGuid = Guid.NewGuid();
        //    int rowsAffected = new PerformanceMonitoringDB().AddPerformanceMontoringData(performanceMonitoringManager);
        //    if (rowsAffected == 1)
        //    {
        //        new RelationOfAssetMonitoringItemTypeDB().AddRelationOfAssetTypeWithMonitoringType(Guid.NewGuid(), new Guid(assetTypeGuid), new Guid(monitoringItemTypeGuid));
        //    }

        //}
    }
}