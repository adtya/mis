﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //TimerManager.Start();

            //System.Threading.Thread autoRestartThread = new System.Threading.Thread(TimerManager.AutoRestartThreadRun);
            //autoRestartThread.Start();
        }

        [WebMethod]
        public static string LogMeIn(string userName, string userPassword)
        {
            string retValue = "";
            try
            {
                UserManager userObj = new UserManager();
                userObj = new UserDB().LoginByEmail(userName, userPassword);
                if (userObj.UserGuid != Guid.Empty)
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = userObj.FirstName + " " + userObj.MiddleName + " " + userObj.LastName;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = userObj.Password;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = userObj.UserGuid;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = userObj.RoleManager.RoleName;

                    if (userObj.RoleManager.RoleName == "DBA")
                    {
                        retValue = "DBAHome.aspx";
                    }
                    else if (userObj.RoleManager.RoleName == "SIO")
                    {
                        System.Web.HttpContext.Current.Session["sessionSubChainageGuid"] = null;

                        retValue = "SIOHome.aspx";
                    }
                    else if (userObj.RoleManager.RoleName == "Super User")
                    {
                        retValue = "";
                    }
                }
                else
                {
                    retValue = "No User";
                }
            }
            catch (Exception ex)
            {
                //string jScript;
                //jScript = "<script>";
                //jScript += "bootbox.alert({";
                //jScript += "title: 'Alert!!',";
                //jScript += "message: 'Some Unknown error has occured.<br>Please contact your Administrator for the same.',";
                //jScript += "buttons: {";
                //jScript += "'ok': {";
                //jScript += "label: 'Ok',";
                //jScript += "className: 'btn-default pull-right'";
                //jScript += "}";
                //jScript += "},";
                //jScript += "callback: function () {";
                //jScript += "window.location='Default.aspx';";
                //jScript += "}";
                //jScript += "});";
                //jScript += "</script>";
                //Page.ClientScript.RegisterClientScriptBlock(GetType(), "exceptionScript", jScript);
            }
            return retValue;
        }

        [WebMethod]
        public static string LogGuestIn()
        {
            string retValue = "";
            try
            {
                System.Web.HttpContext.Current.Session["sessionUserName"] = "Guest";
                System.Web.HttpContext.Current.Session["sessionUserPassword"] = "abc";
                System.Web.HttpContext.Current.Session["sessionUserGuid"] = "00000000-0000-0000-0000-000000000000";
                System.Web.HttpContext.Current.Session["sessionUserRole"] = "Guest";

                retValue = "GuestHome.aspx";
            }
            catch (Exception ex)
            {
                //string jScript;
                //jScript = "<script>";
                //jScript += "bootbox.alert({";
                //jScript += "title: 'Alert!!',";
                //jScript += "message: 'Some Unknown error has occured.<br>Please contact your Administrator for the same.',";
                //jScript += "buttons: {";
                //jScript += "'ok': {";
                //jScript += "label: 'Ok',";
                //jScript += "className: 'btn-default pull-right'";
                //jScript += "}";
                //jScript += "},";
                //jScript += "callback: function () {";
                //jScript += "window.location='Default.aspx';";
                //jScript += "}";
                //jScript += "});";
                //jScript += "</script>";
                //Page.ClientScript.RegisterClientScriptBlock(GetType(), "exceptionScript", jScript);
            }
            return retValue;
        }
    }
}