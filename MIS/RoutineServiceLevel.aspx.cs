﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class RoutineServiceLevel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dtRSL = new DataTable();
            DataRow drRSL = null;

            dtRSL.Columns.Add(new DataColumn("Service Level Code", typeof(string)));
            dtRSL.Columns.Add(new DataColumn("Work Item Code Short Description", typeof(string)));
            dtRSL.Columns.Add(new DataColumn("Units", typeof(string)));
            dtRSL.Columns.Add(new DataColumn("Asset Type", typeof(string)));
            dtRSL.Columns.Add(new DataColumn("Service Level", typeof(string)));

            drRSL = dtRSL.NewRow();

            drRSL["Service Level Code"] = "";
            drRSL["Work Item Code Short Description"] = "";
            drRSL["Units"] = "";
            drRSL["Asset Type"] = "";
            drRSL["Service Level"] = "";

            dtRSL.Rows.Add(drRSL);

            GridView1.DataSource = dtRSL;
            GridView1.DataBind();
        }
    }
}