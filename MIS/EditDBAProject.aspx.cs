﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class EditDBAProject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "DBA")
                {
                    if (!Page.IsPostBack)
                    {
                        //DisplayDistrictList();
                        //DisplayDivisionList();
                        //DisplaySioList();
                        //DisplayWorkItemsList();
                        //DisplayFinancialSubHeads();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                    //Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("DBALogin.aspx");
            }
        }

        private DateTime CalculateDateTime(string date)
        {
            char[] delimiter = new char[] { '/' };

            string[] dateParts = date.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

            string day = dateParts[0];
            string month = dateParts[1];
            string year = dateParts[2];

            DateTime dt = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));

            return dt;
        }

        [WebMethod]
        public static int AddProject(ProjectManager projectManager)
        {
            return new ProjectDB().AddProject(projectManager);
        }

        [WebMethod]
        public static int UpdateProjectStep1(ProjectManager projectManager)
        {
            return new ProjectDB().UpdateProjectBasicDetails(projectManager);
        }

        [WebMethod]
        public static int UpdateProjectStep2FremaaOfficer(ProjectAndFremaaOfficersRelationManager projectAndFremaaOfficersRelationManager)
        {
            return new ProjectAndFremaaOfficersRelationDB().UpdateProjectAndFremaaOfficersRelation1(projectAndFremaaOfficersRelationManager);

        }

    }
}