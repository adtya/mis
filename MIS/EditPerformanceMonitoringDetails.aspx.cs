﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class EditPerformanceMonitoringDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "DBA")
                {
                    if (!Page.IsPostBack)
                    {
                        DisplayAssetCodeList();
                        DisplayCurrentMonitoringDate();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
            }
        }

        private void DisplayAssetCodeList()
        {
            List<AssetManager> assetCodeList = new List<AssetManager>();

            assetCodeList = new AssetDB().GetAssetCodeList();
            StringBuilder htmlAssetCodeList = new StringBuilder();

            htmlAssetCodeList.Append("<select class=\"form-control\" name=\"assetCodeList\" id=\"assetCodeList\">");
            htmlAssetCodeList.Append("<option value=\"\">" + "--Select Asset Code--" + "</option>");
            if (assetCodeList.Count > 0)
            {
                foreach (AssetManager assetCodeObj in assetCodeList)
                {
                    htmlAssetCodeList.Append("<option value=\"" + assetCodeObj.AssetGuid.ToString() + "\">" + assetCodeObj.AssetCode.ToString() + "</option>");
                }
            }
            htmlAssetCodeList.Append("</select>");
            ltAssetCode.Text = htmlAssetCodeList.ToString();
        }

        [WebMethod]
        public void DisplayCurrentMonitoringDate()
        {
            SystemVariablesManager systemVariablesObj = new SystemVariablesManager();
            systemVariablesObj = new SystemVariablesDB().GetCurrentMonitoringDate();
            ltMonitoringDate.Text = "<input id=\"monitoringDate\" class=\"form-control\" readonly=\"readonly\" value=\"" + systemVariablesObj.CurrentMonitoringDate.ToString("dd/MM/yyyy") + "\" />";

        }

        [WebMethod]
        public static string[] DisplayDesignationList()
        {
            List<DesignationManager> designationList = new List<DesignationManager>();

            designationList = new DesignationDB().GetDesignations();

            List<String> designationNameList = new List<string>();

            if (designationList.Count > 0)
            {
                foreach (DesignationManager designationObj in designationList)
                {
                    designationNameList.Add(designationObj.DesignationGuid.ToString());
                    designationNameList.Add(designationObj.DesignationName.ToString());
                }
            }
            return designationNameList.ToArray();
        }

        [WebMethod]
        public static string[] DisplayOverallStatusList()
        {
            List<OverallStatusManager> overallStatusList = new List<OverallStatusManager>();

            overallStatusList = new OverallStatusDB().GetOverallStatus();

            List<String> overallStatusNameList = new List<string>();

            if (overallStatusList.Count > 0)
            {
                foreach (OverallStatusManager overallStatusObj in overallStatusList)
                {
                    overallStatusNameList.Add(overallStatusObj.OverallStatusGuid.ToString());
                    overallStatusNameList.Add(overallStatusObj.OverallStatusName.ToString());
                }
            }
            return overallStatusNameList.ToArray();
        }

        [WebMethod]
        public static PerformanceMonitoringManager GetPerformanceMonitoringData(string assetGuid, DateTime currentMonitoringDate)
        {
            PerformanceMonitoringManager performanceMonitoringDataObj = new PerformanceMonitoringManager();

            performanceMonitoringDataObj = new PerformanceMonitoringDB().GetPerformanceDataByAssetUuid(new Guid(assetGuid), currentMonitoringDate);

            return performanceMonitoringDataObj;
        }

        [WebMethod]
        public static List<MonitoringItemPerformanceManager> GetMonitoringItemPerformanceData(string assetGuid, DateTime currentMonitoringDate, string assetTypeGuid, string performanceMonitoringGuid)
        {
             List<MonitoringItemPerformanceManager> monitoringItemPerformanceDataList = new List<MonitoringItemPerformanceManager>();
             monitoringItemPerformanceDataList = new MonitoringItemPerformanceDB().GetMonitoringItemPerformanceDataByAssetUuid(new Guid(assetGuid), currentMonitoringDate, new Guid(assetTypeGuid), new Guid(performanceMonitoringGuid));

             return monitoringItemPerformanceDataList;
        }

        [WebMethod]
        public static int UpdatePerformanceMontoringData(string performanceMonitoringGuid,PerformanceMonitoringManager performanceMonitoringManager)
        {
            return new PerformanceMonitoringDB().UpdatePerformanceMontoringData(new Guid(performanceMonitoringGuid), performanceMonitoringManager);
        }

    }
}