﻿<%@ Page Language="C#" Title="Create Work Type" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CreateWorkType.aspx.cs" Inherits="MIS.CreateWorkType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptSection1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="container" style="margin-top:5%;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <strong>Create Work Type</strong>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form form-horizontal" action="#" method="post" runat="server">
                            <fieldset>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="workTypeName" id="workTypeNameLabel" class="control-label">Name of Work</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="workTypeName" class="form-control" id="workTypeName" placeholder="Name of Work" /> 
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="workTypeUnitList" id="workTypeUnitListLabel" class="control-label">Work Type Unit</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <asp:Literal ID="ltWorkTypeUnit" runat="server"></asp:Literal>                                                
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <h3>Enter the head titles for the work type</h3>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="estimateQty" id="estimateQtyLabel" class="control-label">1)Estt qty/Target</label>
                                            </div>                                            
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                               <input type="text" name="estimateQtyName" class="form-control" id="estimateQtyName" placeholder="Estimate Quantity/Target" /> 
                                            </div>
                                        </div>                               
                                        
                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="reportingMonth" id="reportingMonthLabel" class="control-label">2)Reporting Month Head</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="reportingMonth" class="form-control" id="reportingMonth" placeholder="Reporting Month Head" />
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="cumulativeValue" id="cumulativeValueLabel" class="control-label">3)Cumulative Value Head</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="cumulativeValue" class="form-control" id="cumulativeValue" placeholder="Cumulative Value" />
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                <label for="balance" id="balanceLabel" class="control-label">4)Balance</label>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                <input type="text" name="balance" class="form-control" id="balance" placeholder="Balance" />
                                            </div>
                                        </div>                                                                          
                                      </div>
                                    </div>

                               <div class="row">
                                    <div class="col-xs-4 col-xs-offset-4 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
                                        <button type="button" name="createWorkTypeBtn" class="btn btn-block btn-primary" id="createWorkTypeBtn">Create WorkType</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="pull-left">
                                    <a class="btn btn-primary" href="DBAHome.aspx" onclick="">Back to Home</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptSection2" runat="server">
    <script src="CustomScripts/create-work-type.js" type="text/javascript"></script>
</asp:Content>
