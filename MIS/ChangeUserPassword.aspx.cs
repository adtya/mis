﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class ChangeUserPassword : System.Web.UI.Page
    {
        private static string currentPassword = string.Empty;
        private static Guid userGuid = Guid.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                currentPassword = Session["sessionUserPassword"].ToString();
                userGuid = new Guid(Session["sessionUserGuid"].ToString());
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("Default.aspx");
            }
        }

        [WebMethod]
        public static bool CheckCurrentPassword(string currentPassword)
        {
            if (ChangeUserPassword.currentPassword == currentPassword)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [WebMethod]
        public static bool UpdateUserPassword(string newPassword)
        {
            return new UserDB().UpdateUserPassword(userGuid, newPassword);
        }

    }
}