﻿<%@ Page Title="Projects List" Language="C#" MasterPageFile="~/DBA.Master" AutoEventWireup="true" CodeBehind="DBAProjects.aspx.cs" Inherits="MIS.DBAProjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadAreaStylePlaceHolder" runat="server">
    <link href="CustomStyles/DBA/Project/dba-projects.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadAreaScriptPlaceHolder" runat="server">
    <%--<script src="Scripts/bootstrap-collapse.js" type="text/javascript"></script>--%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentAreaPlaceHolder" runat="server">

    <asp:Literal runat="server" ID="projectsList" />

    <div id="viewProjectPopupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="overflow-y:initial !important; max-width:calc(100vw - 100px); min-width:calc(100vw - 100px);">
            <div class="modal-content">

                <div class="modal-header" style="display:none"></div>
                <%--<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h1 class="text-center">What's My Password?</h1>
                </div>--%>

                <div class="modal-body" style="max-height:calc(100vh - 200px); overflow-y: auto;">

                </div>

                <div class="modal-footer">
                    <button id="viewProjectPopupModalCloseBtn" name="viewProjectPopupModalCloseBtn" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>

            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptAreaPlaceHolder" runat="server">
    <script src="CustomScripts/DBA/Project/dba-projects.js" type="text/javascript"></script>
</asp:Content>