﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class Schemes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<ProjectManager> projectNameList = new List<ProjectManager>();
            projectNameList = new ProjectDB().GetProjectNameWithStatus();

            StringBuilder htmlTable = new StringBuilder();
            htmlTable.Append("<div id=" + "\"table-container\"" + "style=" + "\"padding:1%;\">");
            htmlTable.Append("<table id=\"trainingList\" class=\"table table-striped table-bordered table-hover table-responsive\" cellspacing=\"0\" width=\"100%\">");

            if (projectNameList.Count > 0)
            {
                htmlTable.Append("<thead>");
                htmlTable.Append("<tr>");
                htmlTable.Append("<th>");
                htmlTable.Append("Sr No");
                htmlTable.Append("</th>");
                htmlTable.Append("<th>");
                htmlTable.Append("Scheme Name");
                htmlTable.Append("</th>");
                htmlTable.Append("</tr>");
                htmlTable.Append("</thead>");

                htmlTable.Append("<tfoot>");
                htmlTable.Append("<tr>");
                htmlTable.Append("<th>");
                htmlTable.Append("Sr No");
                htmlTable.Append("</th>");
                htmlTable.Append("<th>");
                htmlTable.Append("Scheme Name");
                htmlTable.Append("</th>");
                htmlTable.Append("</tr>");
                htmlTable.Append("</tfoot>");

                htmlTable.Append("<tbody>");

                foreach (ProjectManager prjData in projectNameList)
                {
                    int index = projectNameList.IndexOf(prjData);

                    htmlTable.Append("<tr>");

                    htmlTable.Append("<td>" + ++index + "</td>");
                    htmlTable.Append("<td>" + prjData.ProjectName.ToString() + "</td>");
                    
                    htmlTable.Append("</tr>");
                }

                htmlTable.Append("</tbody>");
            }
            htmlTable.Append("</table>");
            htmlTable.Append("</div>");

            ltSchemeTable.Text = htmlTable.ToString();
        }
    }
}