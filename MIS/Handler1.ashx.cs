﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MIS
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class Handler1 : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            string dirFullPath = HttpContext.Current.Server.MapPath("~/Drawing/Assets/");
            string[] files;
            int numFiles;
            files = System.IO.Directory.GetFiles(dirFullPath);
            numFiles = files.Length;
            numFiles = numFiles + 1;

            string str_image = "";
            string pathToSave = "";

            //var a = context.Request.Form[0];
            //var b = context.Request.Form[1];

            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                string fileName = file.FileName;
                string fileExtension = file.ContentType;

                if (!string.IsNullOrEmpty(fileName))
                {
                    fileExtension = Path.GetExtension(fileName);
                    str_image = fileName + fileExtension;
                    //str_image = "MyPHOTO_" + numFiles.ToString() + fileExtension;
                    pathToSave = HttpContext.Current.Server.MapPath("~/Drawing/Assets/") + fileName;
                    file.SaveAs(pathToSave);
                }
            }
            context.Response.Write(pathToSave);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    
    }
}