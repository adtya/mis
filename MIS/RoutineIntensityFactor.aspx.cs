﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class RoutinIntensityFactor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dtRoutineIntensityFactor = new DataTable();
            DataRow drRoutineIntensityFactor = null;

            dtRoutineIntensityFactor.Columns.Add(new DataColumn("Asset Code", typeof(string)));
            dtRoutineIntensityFactor.Columns.Add(new DataColumn("Name", typeof(string)));
            dtRoutineIntensityFactor.Columns.Add(new DataColumn("Work Item Code", typeof(string)));
            dtRoutineIntensityFactor.Columns.Add(new DataColumn("Description", typeof(string)));
            dtRoutineIntensityFactor.Columns.Add(new DataColumn("Units", typeof(string)));
            dtRoutineIntensityFactor.Columns.Add(new DataColumn("Intensity Factor", typeof(string)));
            dtRoutineIntensityFactor.Columns.Add(new DataColumn("Intensity Factor Justification", typeof(string)));

            drRoutineIntensityFactor = dtRoutineIntensityFactor.NewRow();

            drRoutineIntensityFactor["Asset Code"] = "";
            drRoutineIntensityFactor["Name"] = "";
            drRoutineIntensityFactor["Work Item Code"] = "";
            drRoutineIntensityFactor["Description"] = "";
            drRoutineIntensityFactor["Units"] = "";
            drRoutineIntensityFactor["Intensity Factor"] = "";
            drRoutineIntensityFactor["Intensity Factor Justification"] = "";

            dtRoutineIntensityFactor.Rows.Add(drRoutineIntensityFactor);

            GridView1.DataSource = dtRoutineIntensityFactor;
            GridView1.DataBind();
        }
    }
}