﻿using MIS.App_Code;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MIS
{
    public partial class DBAAssets : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            if (Session["sessionUserGuid"] != null)
            {
                if (Convert.ToString(Session["sessionUserRole"]) == "DBA")
                {
                    if (!Page.IsPostBack)
                    {
                        //DisplayDivisionListForAsset();
                        //DisplaySchemeListForAsset();
                        DisplayAssetTypeListForAsset();
                        DisplayAssetCodeList();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["sessionUserName"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserPassword"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserGuid"] = null;
                    System.Web.HttpContext.Current.Session["sessionUserRole"] = null;

                    System.Web.HttpContext.Current.Session.Clear();
                    System.Web.HttpContext.Current.Session.Abandon();

                    Response.Clear();
                    Response.Write("<script language='javascript' type='text/javascript'>");
                    Response.Write("alert('You cannot access the page requested.');");
                    Response.Write("window.location='Login.aspx';");
                    Response.Write("</script>");
                    //Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Clear();
                Response.Write("<script language='javascript' type='text/javascript'>");
                Response.Write("alert('Session Expired. Please login again... ');");
                Response.Write("window.location='Login.aspx';");
                Response.Write("</script>");
                //Response.Redirect("DBALogin.aspx");
            }
        }
        
        private void DisplayDivisionListForAsset()
        {
            List<DivisionManager> divisionList = new List<DivisionManager>();

            divisionList = new DivisionDB().GetDivisions();

            StringBuilder htmlDivisionList = new StringBuilder();

            htmlDivisionList.Append("<select class=\"form-control\" name=\"divisionList\" id=\"divisionList\">");
            htmlDivisionList.Append("<option value=\"\">" + "--Select Division--" + "</option>");
            if (divisionList.Count > 0)
            {
                foreach (DivisionManager divisionObj in divisionList)
                {
                    htmlDivisionList.Append("<option value=\"" + divisionObj.DivisionGuid + "\">" + divisionObj.DivisionCode + "-" + divisionObj.DivisionName + "</option>");
                }
            }
            htmlDivisionList.Append("</select>");
            //ltDivisionList.Text = htmlDivisionList.ToString();
        }

        //private void DisplaySchemeListForAsset()
        //{
        //    List<SchemeManager> schemeList = new List<SchemeManager>();

        //    schemeList = new SchemeDB().GetAllSchemes();

        //    StringBuilder htmlSchemeList = new StringBuilder();

        //    htmlSchemeList.Append("<select class=\"form-control\" name=\"schemeList\" id=\"schemeList\">");
        //    htmlSchemeList.Append("<option value=\"\">" + "--Select Scheme--" + "</option>");
        //    if (schemeList.Count > 0)
        //    {
        //        foreach (SchemeManager schemeObj in schemeList)
        //        {
        //            htmlSchemeList.Append("<option value=\"" + schemeObj.SchemeGuid + "\">" + schemeObj.SchemeCode + "-" + schemeObj.SchemeName + "</option>");
        //        }
        //    }
        //    htmlSchemeList.Append("</select>");
        //}

        private void DisplayAssetTypeListForAsset()
        {
            List<AssetTypeManager> assetTypeList = new List<AssetTypeManager>();

            assetTypeList = new AssetTypeDB().GetAssetTypes();

            StringBuilder htmlAssetTypeList = new StringBuilder();

            htmlAssetTypeList.Append("<select class=\"form-control\" name=\"assetTypeList\" id=\"assetTypeList\">");
            htmlAssetTypeList.Append("<option value=\"\">" + "--Select AssetType--" + "</option>");
            if (assetTypeList.Count > 0)
            {
                foreach (AssetTypeManager assetTypeObj in assetTypeList)
                {
                    htmlAssetTypeList.Append("<option value=\"" + assetTypeObj.AssetTypeGuid + "\">" + assetTypeObj.AssetTypeName + " (" + assetTypeObj.AssetTypeCode + ")" + "</option>");
                }
            }
            htmlAssetTypeList.Append("</select>");
            ltAssetTypeList.Text = htmlAssetTypeList.ToString();
        }

        private void DisplayAssetCodeList()
        {
            List<AssetManager> assetManagerList = new List<AssetManager>();
            assetManagerList = new AssetDB().GetAssets();

            StringBuilder htmlTable = new StringBuilder();

            htmlTable.Append("<div id=" + "\"table-container\"" + " style=" + "\"padding:1%;\">");

            htmlTable.Append("<div class=\"table-responsive\">");
            htmlTable.Append("<table id=\"assetListForDBA\" name=\"assetListForDBA\" class=\"table table-striped table-bordered table-hover table-responsive\" cellspacing=\"0\" width=\"100%\">");

            htmlTable.Append("<thead>");
            htmlTable.Append("<tr>");
            htmlTable.Append("<th>#</th>");
            htmlTable.Append("<th>Asset Guid</th>");
            htmlTable.Append("<th>Asset Name</th>");
            htmlTable.Append("<th>Asset Code</th>");
            htmlTable.Append("<th>Division Code</th>");
            htmlTable.Append("<th>Division Name</th>");
            htmlTable.Append("<th>Scheme Code</th>");
            htmlTable.Append("<th>Scheme Name</th>");
            htmlTable.Append("<th>AssetType Code</th>");
            htmlTable.Append("<th>AssetType Name</th>");
            htmlTable.Append("<th>Initiation Year</th>");
            htmlTable.Append("<th>Completion Year</th>");
            htmlTable.Append("<th>Division Guid</th>");
            htmlTable.Append("<th>Scheme Guid</th>");
            htmlTable.Append("<th>Asset Type Guid</th>");
            htmlTable.Append("<th>Operations</th>");
            htmlTable.Append("</tr>");
            htmlTable.Append("</thead>");

            htmlTable.Append("<tfoot>");
            htmlTable.Append("<tr>");
            htmlTable.Append("<th>#</th>");
            htmlTable.Append("<th>Asset Guid</th>");
            htmlTable.Append("<th>Asset Name</th>");
            htmlTable.Append("<th>Asset Code</th>");
            htmlTable.Append("<th>Division Code</th>");
            htmlTable.Append("<th>Division Name</th>");
            htmlTable.Append("<th>Scheme Code</th>");
            htmlTable.Append("<th>Scheme Name</th>");
            htmlTable.Append("<th>AssetType Code</th>");
            htmlTable.Append("<th>AssetType Name</th>");
            htmlTable.Append("<th>Initiation Year</th>");
            htmlTable.Append("<th>Completion Year</th>");
            htmlTable.Append("<th>Division Guid</th>");
            htmlTable.Append("<th>Scheme Guid</th>");
            htmlTable.Append("<th>Asset Type Guid</th>");
            htmlTable.Append("<th>Operations</th>");
            htmlTable.Append("</tr>");
            htmlTable.Append("</tfoot>");

            htmlTable.Append("<tbody>");

            foreach (AssetManager assetManagerObj in assetManagerList)
            {
                htmlTable.Append("<tr>");
                htmlTable.Append("<td></td>");
                htmlTable.Append("<td>" + assetManagerObj.AssetGuid + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.AssetName + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.AssetCode + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.DivisionManager.DivisionCode + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.DivisionManager.DivisionName + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.SchemeManager.SchemeCode + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.SchemeManager.SchemeName + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.AssetTypeManager.AssetTypeCode + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.AssetTypeManager.AssetTypeName + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.InitiationDate.Year.ToString() + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.CompletionDate.Year.ToString() + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.DivisionManager.DivisionGuid + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.SchemeManager.SchemeGuid + "</td>");
                htmlTable.Append("<td>" + assetManagerObj.AssetTypeManager.AssetTypeGuid + "</td>");
                htmlTable.Append("<td>" + "<a href=\"#\" class=\"viewAssetListBtn\"><i class=\"ui-tooltip fa fa-file-text-o\" style=\"font-size: 22px;\" data-original-title=\"View\" title=\"View\"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"#\" class=\"editAssetBtn\"><i class=\"ui-tooltip fa fa-pencil-square-o\" style=\"font-size: 22px;\" data-original-title=\"Edit\" title=\"Edit\"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"#\" class=\"deleteAssetBtn\"><i class=\"ui-tooltip fa fa-trash-o\" style=\"font-size: 22px;\" data-original-title=\"Delete\" title=\"Delete\"></i></a>" + "</td>");
                //&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"#\" class=\"printAssetBtn\"><i class=\"ui-tooltip fa fa-print\" style=\"font-size: 22px;\" data-original-title=\"Print\" title=\"Print\"></i></a>" + "</td>");
                htmlTable.Append("</tr>");

            }

            htmlTable.Append("</tbody>");
            htmlTable.Append("</table>");
            htmlTable.Append("</div>");

            htmlTable.Append("</div>");

            assetsList.Text = htmlTable.ToString();
          
        }

        [WebMethod]
        public static List<SchemeManager> DisplaySchemeNameForDivisionsForAsset(Guid divisionGuid)
        {
            List<SchemeManager> schemeList = new List<SchemeManager>();
            schemeList = new SchemeDB().GetSchemesBasedOnDivisions(divisionGuid);

            return schemeList;
        }

        [WebMethod]
        public static void SetAssetSessionParameters(Guid assetGuid,Guid assetTypeGuid, Guid divisionGuid, Guid schemeGuid, string assetTypeCode)
        {
            System.Web.HttpContext.Current.Session["sessionAssetGuid"] = assetGuid;            
            System.Web.HttpContext.Current.Session["sessionAssetTypeGuid"] = assetTypeGuid;
            System.Web.HttpContext.Current.Session["sessionDivisionGuid"] = divisionGuid;
            System.Web.HttpContext.Current.Session["sessionSchemeGuid"] = schemeGuid;
            System.Web.HttpContext.Current.Session["sessionAssetTypeCode"] = assetTypeCode;
        }

        //[WebMethod]
        //public static string ViewAssetData(Guid assetGuid, string assetTypeCode)
        //{
        //    AssetManager assetDataObj = new AssetManager();
        //    assetDataObj = new AssetDB().GetAssetDataByAssetUuid(assetGuid, assetTypeCode);

        //    List<GateManager> gateDataManagerList = new List<GateManager>();
        //    gateDataManagerList = new GateDB().GetGateDataListByAssetUuid(assetGuid);

        //    StringBuilder htmlAssetDetailsBody = new StringBuilder();
        //    htmlAssetDetailsBody.Append("<div class=\"container-fluid col-md-12\">");
        //    htmlAssetDetailsBody.Append("<div class=\"row\">");
        //    htmlAssetDetailsBody.Append("<div class=\"col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1\">");
        //    htmlAssetDetailsBody.Append("<div class=\"panel panel-default\">");
        //    htmlAssetDetailsBody.Append("<div class=\"panel-body\">");
        //    htmlAssetDetailsBody.Append("<div class=\"text-center\">");

        //    htmlAssetDetailsBody.Append("<img src=\"Images/view-data.png\" class=\"login\" height=\"70\">");
        //    htmlAssetDetailsBody.Append("<h3 class=\"text-center\">" + "Asset Code" + ":" + assetDataObj.AssetCode + "</h3>");
        //    htmlAssetDetailsBody.Append("<h3 class=\"text-center\">" + "Asset Name" + ":" + assetDataObj.AssetName + "</h3>");

        //    htmlAssetDetailsBody.Append("<div class=\"panel-body\">");
        //    htmlAssetDetailsBody.Append("<form id=\"viewAssetPopupForm\" name=\"viewAssetPopupForm\" role=\"form\" class=\"form form-horizontal\" method=\"post\">");

        //    htmlAssetDetailsBody.Append("<div class=\"form-group\">");
        //    htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

        //    htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
        //    htmlAssetDetailsBody.Append("<table id=\"assetDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

        //    htmlAssetDetailsBody.Append("<thead>");
        //    htmlAssetDetailsBody.Append("</thead>");

        //    htmlAssetDetailsBody.Append("<tbody>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td colspan=\"3\">" + "<h4>" + "Asset Details" + "</h4>" + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "Division Code" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.DivisionManager.DivisionCode + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "Division Name" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.DivisionManager.DivisionName + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "Scheme Code" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.SchemeManager.SchemeCode + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "Scheme Name" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.SchemeManager.SchemeName + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "AssetType Code" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.AssetTypeManager.AssetTypeCode + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "AssetType Name" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.AssetTypeManager.AssetTypeName + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "Drawing Code" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.DrawingCode + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "AMTD" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.Amtd + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "Start Chainage" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.StartChainage + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "End Chainage" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.EndChainage + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "UTM US East" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.UtmEast1 + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "UTM DS East" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.UtmEast2 + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "UTM US North" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.UtmNorth1 + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "UTM DS North" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.UtmNorth2 + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "Asset Cost" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.AssetCost + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td>" + "Completion Date" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //    htmlAssetDetailsBody.Append("<td>" + assetDataObj.CompletionDate.ToString("dd/MM/yyyy") + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    htmlAssetDetailsBody.Append("<tr>");
        //    htmlAssetDetailsBody.Append("<td colspan=\"3\">" + "<h4>" + "Asset Technical Specification : " + assetDataObj.AssetTypeManager.AssetTypeName + "</h4>" + "</td>");
        //    htmlAssetDetailsBody.Append("</tr>");

        //    if (assetDataObj.AssetTypeManager.AssetTypeCode == "BRG")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Bridge Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.BridgeManager.BridgeTypeManager.BridgeTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Length (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.BridgeManager.Length + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Number Of Piers" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.BridgeManager.PierNumber + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Road Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.BridgeManager.RoadWidth + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "CAN")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "U/S Crest Elevation (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.CanalManager.UsElevation + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "D/S Crest Elevation (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.CanalManager.DSElevation + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Length (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.CanalManager.Length + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Bed Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.CanalManager.BedWidth + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Slope" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.CanalManager.Slope1 + ":" + assetDataObj.CanalManager.Slope2 + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Average Depth (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.CanalManager.AverageDepth + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Lining Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.CanalManager.CanalLiningTypeManager.LiningTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "CUL")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Culvert Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.CulvertManager.CulvertTypeManager.CulvertTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Number Of Vents" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.CulvertManager.VentNumber + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Vent Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.CulvertManager.VentWidth + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Vent Height (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.CulvertManager.VentHeight + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Length (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.CulvertManager.LengthCulvert + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "DRN")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "U/S Crest Elevation (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.DrainageManager.UsElevationDrainage + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "D/S Crest Elevation (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.DrainageManager.DsElevationDrainage + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Length (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.DrainageManager.LengthDrainage + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Bed Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.DrainageManager.BedWidthDrainage + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Slope" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.DrainageManager.Slope1Drainage + ":" + assetDataObj.DrainageManager.Slope2Drainage + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Average Depth (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.DrainageManager.AverageDepthDrainage + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "DRP")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "U/S Invert Level" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.DropStructureManager.UsInvertLevel + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "D/S Invert Level" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.DropStructureManager.DsInvertLevel + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Stilling Basin Length (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.DropStructureManager.StillingBasinLength + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Stilling Basin Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.DropStructureManager.StillingBasinWidth + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "EMB")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "U/S Crest Elevation (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.EmbankmentManager.UsElevation + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "D/S Crest Elevation (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.EmbankmentManager.DsElevation + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Length (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.DrainageManager.LengthDrainage + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Crest Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.EmbankmentManager.CrestWidth + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "CS Slope" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.EmbankmentManager.CsSlope1 + ":" + assetDataObj.EmbankmentManager.CsSlope2 + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "RS Slope" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.EmbankmentManager.RsSlope1 + ":" + assetDataObj.EmbankmentManager.RsSlope2 + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Berm Slope" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.EmbankmentManager.BermSlope1 + ":" + assetDataObj.EmbankmentManager.BermSlope2 + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Platform Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.EmbankmentManager.PlatformWidth + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Average Height (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.EmbankmentManager.AverageHeight + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Crest Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.EmbankmentManager.EmbankmentCrestTypeManager.CrestTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Fill Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.EmbankmentManager.EmbankmentFillTypeManager.FillTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "GAU")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Gauge Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.GaugeManager.GaugeTypeManager.GaugeTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Season Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.GaugeManager.GaugeSeasonTypeManager.SeasonTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Frequency Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.GaugeManager.GaugeFrequencyTypeManager.FrequencyTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Zero Datum (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.GaugeManager.ZeroDatum + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Start Date" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.GaugeManager.StartDate.ToString("dd/MM/yyyy") + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "End Date" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.GaugeManager.EndDate.ToString("dd/MM/yyyy") + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Active" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        if (assetDataObj.GaugeManager.Active == true)
        //        {
        //            htmlAssetDetailsBody.Append("<td>" + "True" + "</td>");
        //        }
        //        else
        //        {
        //            htmlAssetDetailsBody.Append("<td>" + "False" + "</td>");
        //        }
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "LWL" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        if (assetDataObj.GaugeManager.Lwl == true)
        //        {
        //            htmlAssetDetailsBody.Append("<td>" + "True" + "</td>");
        //        }
        //        else
        //        {
        //            htmlAssetDetailsBody.Append("<td>" + "False" + "</td>");
        //        }
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "HWL" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        if (assetDataObj.GaugeManager.Hwl == true)
        //        {
        //            htmlAssetDetailsBody.Append("<td>" + "True" + "</td>");
        //        }
        //        else
        //        {
        //            htmlAssetDetailsBody.Append("<td>" + "False" + "</td>");
        //        }
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Level Geo" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        if (assetDataObj.GaugeManager.LevelGeo == true)
        //        {
        //            htmlAssetDetailsBody.Append("<td>" + "True" + "</td>");
        //        }
        //        else
        //        {
        //            htmlAssetDetailsBody.Append("<td>" + "False" + "</td>");
        //        }
        //        htmlAssetDetailsBody.Append("</tr>");
        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "POR")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Porcupine Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.PorcupineManager.PorcupineTypeManager.PorcupineTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Material Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.PorcupineManager.PorcupineMaterialTypeManager.PorcupineMaterialTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Screen Length (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.PorcupineManager.ScreenLength + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Screen Rows" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.PorcupineManager.ScreenRows + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Spacing Along Screen" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.PorcupineManager.SpacingAlongScreen + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Number Of Layes" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.PorcupineManager.NumberOfLayers + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Member Length (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.PorcupineManager.MemberLength + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Length (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.PorcupineManager.LengthPorcupine + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "REG")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Number Of Vents" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.RegulatorManager.VentNumberRegulator + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Vent Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.RegulatorManager.VentWidthRegulator + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Vent Height (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.RegulatorManager.VentHeightRegulator + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Regulator Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.RegulatorManager.RegulatorTypeManager.RegulatorTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Gate Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.RegulatorManager.RegulatorGateTypeManager.RegulatorGateTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

        //        htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
        //        htmlAssetDetailsBody.Append("<table id=\"gateDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

        //        htmlAssetDetailsBody.Append("<thead>");
        //        htmlAssetDetailsBody.Append("<th>" + "Gate Type" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Construction Material" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Number of Gates" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Height" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Width" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Diameter" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Install Date" + "</th>");
        //        htmlAssetDetailsBody.Append("</thead>");

        //        htmlAssetDetailsBody.Append("<tbody>");
        //        for (var i = 0; i < gateDataManagerList.Count; i++)
        //        {
        //            htmlAssetDetailsBody.Append("<tr>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].GateTypeManager.GateTypeName + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].GateConstructionMaterialManager.GateConstructionMaterialName + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].NumberOfGates + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].Height + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].Width + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].Diameter + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].InstallDate.ToString("dd/MM/yyyy") + "</td>");
        //            htmlAssetDetailsBody.Append("</tr>");
        //        }
        //        htmlAssetDetailsBody.Append("</tbody>");

        //        htmlAssetDetailsBody.Append("</table>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "REV")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Revetment Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.RevetmentManager.RevetmentTypeManager.RevetTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //        htmlAssetDetailsBody.Append("<tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "River Protection Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.RevetmentManager.RevetmentRiverprotTypeManager.RiverprotTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Wave Protection Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.RevetmentManager.RevetmentWaveprotTypeManager.WaveprotTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<td>" + "Slope (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.RevetmentManager.SlopeRevetment + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Length (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.RevetmentManager.LengthRevetment + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Plain Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.RevetmentManager.PlainWidth + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "SLU")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Sluice Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.SluiceManager.SluiceTypeManager.SluiceTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //        htmlAssetDetailsBody.Append("<tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Number Of Vents" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.SluiceManager.VentNumberSluice + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Vent Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.SluiceManager.VentWidthSluice + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Vent Height (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.SluiceManager.VentHeightSluice + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Vent Diameter (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.SluiceManager.VentDiameterSluice + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

        //        htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
        //        htmlAssetDetailsBody.Append("<table id=\"gateDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

        //        htmlAssetDetailsBody.Append("<thead>");
        //        htmlAssetDetailsBody.Append("<th>" + "Gate Type" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Construction Material" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Number of Gates" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Height" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Width" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Diameter" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Install Date" + "</th>");
        //        htmlAssetDetailsBody.Append("</thead>");

        //        htmlAssetDetailsBody.Append("<tbody>");
        //        for (var i = 0; i < gateDataManagerList.Count; i++)
        //        {
        //            htmlAssetDetailsBody.Append("<tr>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].GateTypeManager.GateTypeName + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].GateConstructionMaterialManager.GateConstructionMaterialName + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].NumberOfGates + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].Height + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].Width + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].Diameter + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].InstallDate.ToString("dd/MM/yyyy") + "</td>");
        //            htmlAssetDetailsBody.Append("</tr>");
        //        }
        //        htmlAssetDetailsBody.Append("</tbody>");

        //        htmlAssetDetailsBody.Append("</table>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "SPU")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Orientation (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.SpurManager.Orientation + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Length (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.SpurManager.LengthSpur + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.SpurManager.WidthSpur + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Shape Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.SpurManager.SpurShapeTypeManager.ShapeTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //        htmlAssetDetailsBody.Append("<tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Construction Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.SpurManager.SpurConstructionTypeManager.ConstructionTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Revet Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.SpurManager.SpurRevetTypeManager.SpurRevetTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "TRN")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Inlet Box Length (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.TurnoutManager.InletboxLength + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Inlet Box Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.TurnoutManager.InletboxWidth + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Outlet Number" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.TurnoutManager.OutletNumber + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Outlet Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.TurnoutManager.OutletWidth + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Outlet Height (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.TurnoutManager.OutletHeight + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Turnout Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.TurnoutManager.TurnoutTypeManager.TurnoutTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //        htmlAssetDetailsBody.Append("<tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Gate Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.TurnoutManager.TurnoutGateTypeManager.TurnoutGateTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

        //        htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
        //        htmlAssetDetailsBody.Append("<table id=\"gateDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

        //        htmlAssetDetailsBody.Append("<thead>");
        //        htmlAssetDetailsBody.Append("<th>" + "Gate Type" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Construction Material" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Number of Gates" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Height" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Width" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Diameter" + "</th>");
        //        htmlAssetDetailsBody.Append("<th>" + "Install Date" + "</th>");
        //        htmlAssetDetailsBody.Append("</thead>");

        //        htmlAssetDetailsBody.Append("<tbody>");
        //        for (var i = 0; i < gateDataManagerList.Count; i++)
        //        {
        //            htmlAssetDetailsBody.Append("<tr>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].GateTypeManager.GateTypeName + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].GateConstructionMaterialManager.GateConstructionMaterialName + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].NumberOfGates + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].Height + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].Width + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].Diameter + "</td>");
        //            htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].InstallDate.ToString("dd/MM/yyyy") + "</td>");
        //            htmlAssetDetailsBody.Append("</tr>");
        //        }
        //        htmlAssetDetailsBody.Append("</tbody>");

        //        htmlAssetDetailsBody.Append("</table>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //    }
        //    else if (assetDataObj.AssetTypeManager.AssetTypeCode == "WEI")
        //    {
        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Invert Level" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.WeirManager.InvertLevel + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Crest Top Level" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.WeirManager.CrestTopLevel + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Crest Top Width (m)" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.WeirManager.CrestTopWidth + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");

        //        htmlAssetDetailsBody.Append("<tr>");
        //        htmlAssetDetailsBody.Append("<td>" + "Weir Type" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + ":" + "</td>");
        //        htmlAssetDetailsBody.Append("<td>" + assetDataObj.WeirManager.WeirTypeManager.WeirTypeName + "</td>");
        //        htmlAssetDetailsBody.Append("</tr>");
        //        htmlAssetDetailsBody.Append("<tr>");
        //    }
        //    htmlAssetDetailsBody.Append("</tbody>");

        //    htmlAssetDetailsBody.Append("</table>");
        //    htmlAssetDetailsBody.Append("</div>");

        //    htmlAssetDetailsBody.Append("</div>");
        //    htmlAssetDetailsBody.Append("</div>");

        //    htmlAssetDetailsBody.Append("</form>");
        //    htmlAssetDetailsBody.Append("</div>");

        //    htmlAssetDetailsBody.Append("</div>");
        //    htmlAssetDetailsBody.Append("</div>");
        //    htmlAssetDetailsBody.Append("</div>");
        //    htmlAssetDetailsBody.Append("</div>");
        //    htmlAssetDetailsBody.Append("</div>");
        //    htmlAssetDetailsBody.Append("</div>");


        //    return htmlAssetDetailsBody.ToString();

        //}

        [WebMethod]
        public static string ViewAssetData(Guid assetGuid, string assetTypeCode)
        {
            AssetManager assetDataObj = new AssetManager();
            assetDataObj = new AssetDB().GetAssetDataByAssetUuid(assetGuid, assetTypeCode);

            List<GateManager> gateDataManagerList = new List<GateManager>();
            gateDataManagerList = new GateDB().GetGateDataListByAssetUuid(assetGuid);

            List<AssetManager> appurtenantAssetManagerList = new List<AssetManager>();
            appurtenantAssetManagerList = new AssetDB().GetKeyAssetData(assetGuid);

            StringBuilder htmlAssetDetailsBody = new StringBuilder();           

            //htmlAssetDetailsBody.Append("<h3 class=\"text-center\">" + "Asset Core" + "</h3>");        

            htmlAssetDetailsBody.Append("<div class=\"form-group\">");
            htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

            htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
            htmlAssetDetailsBody.Append("<table id=\"assetDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

            htmlAssetDetailsBody.Append("<thead>");
            htmlAssetDetailsBody.Append("<tr>");
            htmlAssetDetailsBody.Append("<td colspan =\"12\" style=\"border: 0px;\">" + "<h3>" + "Asset Code" + ":" + assetDataObj.AssetCode + "</h3>" + "</td>");
            htmlAssetDetailsBody.Append("</tr>");
            htmlAssetDetailsBody.Append("</thead>");

            htmlAssetDetailsBody.Append("<tbody>");           

            htmlAssetDetailsBody.Append("<tr>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;  border: 0px;\">" + "WRD Division" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.DivisionManager.DivisionName + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("<td colspan =\"2\" style=\"font-weight: bold;border: 0px;\">" + "UTM (m)" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Chainage (m)" + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Scheme" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.SchemeManager.SchemeName + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Easting" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Northing" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Key Asset" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" >" + "" + "</td>");            
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Upstream" + "</td>");
            htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;\">" + assetDataObj.UtmEast1 + "</td>");
            htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;\" >" + assetDataObj.UtmNorth1 + "</td>");
            htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;\">" + assetDataObj.StartChainage + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Drawing Code" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.DrawingCode + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Downstream" + "</td>");
            htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;\">" + assetDataObj.UtmEast2 + "</td>");
            htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;\">" + assetDataObj.UtmNorth2 + "</td>");
            htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;\">" + assetDataObj.EndChainage + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Asset Type" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.AssetTypeManager.AssetTypeCode + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "AMTD" + "</td>");
            htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;\" colspan =\"2\" style=\"border: 0px;\">" + assetDataObj.Amtd + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Asset Name" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.AssetName + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Asset Cost" + "</td>");
            htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;\" colspan =\"2\" style=\"border: 0px;\">" + assetDataObj.AssetCost.ToString(new CultureInfo("en-IN")) + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("</tr>");          

            htmlAssetDetailsBody.Append("<tr>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Asset Code" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.AssetCode + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"4\">" + "" + "</td>");  
            htmlAssetDetailsBody.Append("</tr>");

            htmlAssetDetailsBody.Append("<tr>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Initiation Date" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.InitiationDate.ToString("dd/MM/yyyy") + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Completion Date" + "</td>");
            htmlAssetDetailsBody.Append("<td colspan =\"2\" style=\"border: 0px;\">" + assetDataObj.CompletionDate.ToString("dd/MM/yyyy") + "</td>");
            htmlAssetDetailsBody.Append("<td style=\"border: 0px;\">" + "" + "</td>");
            htmlAssetDetailsBody.Append("</tr>");           

            htmlAssetDetailsBody.Append("<tr>");
            htmlAssetDetailsBody.Append("<td colspan =\"12\" style=\"border: 0px;\">" + "<h3>" + assetDataObj.AssetTypeManager.AssetTypeName + "</h3>" + "</td>");
            htmlAssetDetailsBody.Append("</tr>");

            if (assetDataObj.AssetTypeManager.AssetTypeCode == "BRG")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Bridge Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px; width:10px;\" colspan =\"6\">" + assetDataObj.BridgeManager.BridgeTypeManager.BridgeTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");  
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Length (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.BridgeManager.Length + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>"); 
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Number Of Piers" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.BridgeManager.PierNumber + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>"); 
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Road Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.BridgeManager.RoadWidth + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");
            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "CAN")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "U/S Crest Elevation (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.CanalManager.UsElevation + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "D/S Crest Elevation (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.CanalManager.DSElevation + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Length (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.CanalManager.Length + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Bed Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\" style=\"border: 0px;\">" + assetDataObj.CanalManager.BedWidth + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Slope" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" align = \"right\">" + assetDataObj.CanalManager.Slope1 + ":" + assetDataObj.CanalManager.Slope2 + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Average Depth (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.CanalManager.AverageDepth + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Lining Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" colspan =\"6\">" + assetDataObj.CanalManager.CanalLiningTypeManager.LiningTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

                htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
                htmlAssetDetailsBody.Append("<table id=\"appurtenantAssetManagerDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

                htmlAssetDetailsBody.Append("<thead>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Asset Code" + "</th>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Asset Name" + "</th>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Initiation Year" + "</th>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Completion Year" + "</th>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Asset Cost" + "</th>");
                htmlAssetDetailsBody.Append("</thead>");

                htmlAssetDetailsBody.Append("<tbody>");
                for (var i = 0; i < appurtenantAssetManagerList.Count; i++)
                {
                    htmlAssetDetailsBody.Append("<tr>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + i + 1 + "</td>");
                    //htmlAssetTypeDetailsBody.Append("<td>" + appurtenantAssetManagerList[i].AssetGuid + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].AssetCode + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].AssetName + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].InitiationDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].CompletionDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].AssetCost.Amount + "</td>");
                    htmlAssetDetailsBody.Append("</tr>");
                }
                htmlAssetDetailsBody.Append("</tbody>");

                htmlAssetDetailsBody.Append("</table>");                

            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "CUL")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Culvert Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" colspan =\"6\">" + assetDataObj.CulvertManager.CulvertTypeManager.CulvertTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Number Of Vents" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.CulvertManager.VentNumber + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Vent Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.CulvertManager.VentWidth + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Vent Height (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.CulvertManager.VentHeight + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Length (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.CulvertManager.LengthCulvert + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");
            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "DRN")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "U/S Crest Elevation (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.DrainageManager.UsElevationDrainage + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "D/S Crest Elevation (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.DrainageManager.DsElevationDrainage + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Length (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.DrainageManager.LengthDrainage + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Bed Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.DrainageManager.BedWidthDrainage + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Slope" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\"align = \"right\">" + assetDataObj.DrainageManager.Slope1Drainage + ":" + assetDataObj.DrainageManager.Slope2Drainage + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Average Depth (m)" + "</td>");           
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;\">" + assetDataObj.DrainageManager.AverageDepthDrainage + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

                htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
                htmlAssetDetailsBody.Append("<table id=\"appurtenantAssetManagerDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

                htmlAssetDetailsBody.Append("<thead>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Asset Code" + "</th>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Asset Name" + "</th>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Initiation Year" + "</th>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Completion Year" + "</th>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Asset Cost" + "</th>");
                htmlAssetDetailsBody.Append("</thead>");

                htmlAssetDetailsBody.Append("<tbody>");
                for (var i = 0; i < appurtenantAssetManagerList.Count; i++)
                {
                    htmlAssetDetailsBody.Append("<tr>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + i + 1 + "</td>");
                    //htmlAssetTypeDetailsBody.Append("<td>" + appurtenantAssetManagerList[i].AssetGuid + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].AssetCode + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].AssetName + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].InitiationDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].CompletionDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].AssetCost.Amount + "</td>");
                    htmlAssetDetailsBody.Append("</tr>");
                }
                htmlAssetDetailsBody.Append("</tbody>");

                htmlAssetDetailsBody.Append("</table>");
            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "DRP")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "U/S Invert Level" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.DropStructureManager.UsInvertLevel + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "D/S Invert Level" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.DropStructureManager.DsInvertLevel + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Stilling Basin Length (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.DropStructureManager.StillingBasinLength + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Stilling Basin Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.DropStructureManager.StillingBasinWidth + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");
            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "EMB")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "U/S Crest Elevation (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.EmbankmentManager.UsElevation + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "D/S Crest Elevation (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.EmbankmentManager.DsElevation + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Length (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.DrainageManager.LengthDrainage + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Crest Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.EmbankmentManager.CrestWidth + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "CS Slope" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" align = \"right\">" + assetDataObj.EmbankmentManager.CsSlope1 + ":" + assetDataObj.EmbankmentManager.CsSlope2 + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "RS Slope" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" align = \"right\">" + assetDataObj.EmbankmentManager.RsSlope1 + ":" + assetDataObj.EmbankmentManager.RsSlope2 + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Berm Slope" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" align = \"right\">" + assetDataObj.EmbankmentManager.BermSlope1 + ":" + assetDataObj.EmbankmentManager.BermSlope2 + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Platform Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.EmbankmentManager.PlatformWidth + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Average Height (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.EmbankmentManager.AverageHeight + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Crest Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" colspan =\"6\">" + assetDataObj.EmbankmentManager.EmbankmentCrestTypeManager.CrestTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Fill Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" colspan =\"6\">" + assetDataObj.EmbankmentManager.EmbankmentFillTypeManager.FillTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

                htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
                htmlAssetDetailsBody.Append("<table id=\"appurtenantAssetManagerDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

                htmlAssetDetailsBody.Append("<thead>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Asset Code" + "</th>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Asset Name" + "</th>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Initiation Year" + "</th>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Completion Year" + "</th>");
                htmlAssetDetailsBody.Append("<th style=\"text-align: left\">" + "Asset Cost" + "</th>");
                htmlAssetDetailsBody.Append("</thead>");

                htmlAssetDetailsBody.Append("<tbody>");
                for (var i = 0; i < appurtenantAssetManagerList.Count; i++)
                {
                    htmlAssetDetailsBody.Append("<tr>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + i + 1 + "</td>");
                    //htmlAssetTypeDetailsBody.Append("<td>" + appurtenantAssetManagerList[i].AssetGuid + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].AssetCode + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].AssetName + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].InitiationDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].CompletionDate.ToString(@"dd\/MM\/yyyy") + "</td>");
                    htmlAssetDetailsBody.Append("<td style=\"text-align: left\">" + appurtenantAssetManagerList[i].AssetCost.Amount + "</td>");
                    htmlAssetDetailsBody.Append("</tr>");
                }
                htmlAssetDetailsBody.Append("</tbody>");

                htmlAssetDetailsBody.Append("</table>");
            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "GAU")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Gauge Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.GaugeManager.GaugeTypeManager.GaugeTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Season Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" colspan =\"6\">" + assetDataObj.GaugeManager.GaugeSeasonTypeManager.SeasonTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Frequency Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" colspan =\"6\">" + assetDataObj.GaugeManager.GaugeFrequencyTypeManager.FrequencyTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Zero Datum (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.GaugeManager.ZeroDatum + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Start Date" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\">" + assetDataObj.GaugeManager.StartDate.ToString("dd/MM/yyyy") + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "End Date" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\">" + assetDataObj.GaugeManager.EndDate.ToString("dd/MM/yyyy") + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Active" + "</td>");                
                if (assetDataObj.GaugeManager.Active == true)
                {
                    htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\">" + "True" + "</td>");
                }
                else
                {
                    htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\">" + "False" + "</td>");
                }
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "LWL" + "</td>");                
                if (assetDataObj.GaugeManager.Lwl == true)
                {
                    htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\">" + "True" + "</td>");
                }
                else
                {
                    htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\">" + "False" + "</td>");
                }
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "HWL" + "</td>");               
                if (assetDataObj.GaugeManager.Hwl == true)
                {
                    htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\">" + "True" + "</td>");
                }
                else
                {
                    htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\">" + "False" + "</td>");
                }
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Level Geo" + "</td>");                
                if (assetDataObj.GaugeManager.LevelGeo == true)
                {
                    htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\">" + "True" + "</td>");
                }
                else
                {
                    htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\">" + "False" + "</td>");
                }
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "POR")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Porcupine Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.PorcupineManager.PorcupineTypeManager.PorcupineTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Material Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.PorcupineManager.PorcupineMaterialTypeManager.PorcupineMaterialTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Screen Length (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.PorcupineManager.ScreenLength + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Screen Rows" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.PorcupineManager.ScreenRows + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Spacing Along Screen" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.PorcupineManager.SpacingAlongScreen + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Number Of Layes" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.PorcupineManager.NumberOfLayers + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Member Length (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.PorcupineManager.MemberLength + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Length (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.PorcupineManager.LengthPorcupine + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");
            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "REG")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Number Of Vents" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.RegulatorManager.VentNumberRegulator + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Vent Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.RegulatorManager.VentWidthRegulator + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Vent Height (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.RegulatorManager.VentHeightRegulator + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Regulator Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.RegulatorManager.RegulatorTypeManager.RegulatorTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Gate Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.RegulatorManager.RegulatorGateTypeManager.RegulatorGateTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");
          
                htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

                htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
                htmlAssetDetailsBody.Append("<table id=\"gateDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

                htmlAssetDetailsBody.Append("<thead>");
                htmlAssetDetailsBody.Append("<th>" + "Gate Type" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Construction Material" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Number of Gates" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Height" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Width" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Diameter" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Install Date" + "</th>");
                htmlAssetDetailsBody.Append("</thead>");

                htmlAssetDetailsBody.Append("<tbody>");
                for (var i = 0; i < gateDataManagerList.Count; i++)
                {
                    htmlAssetDetailsBody.Append("<tr>");
                    htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].GateTypeManager.GateTypeName + "</td>");
                    htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].GateConstructionMaterialManager.GateConstructionMaterialName + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\">" + gateDataManagerList[i].NumberOfGates + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\" >" + gateDataManagerList[i].Height + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\" >" + gateDataManagerList[i].Width + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\" >" + gateDataManagerList[i].Diameter + "</td>");
                    htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].InstallDate.ToString("dd/MM/yyyy") + "</td>");
                    htmlAssetDetailsBody.Append("</tr>");
                }
                htmlAssetDetailsBody.Append("</tbody>");

                htmlAssetDetailsBody.Append("</table>");               

            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "REV")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Revetment Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.RevetmentManager.RevetmentTypeManager.RevetTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "River Protection Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.RevetmentManager.RevetmentRiverprotTypeManager.RiverprotTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Wave Protection Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.RevetmentManager.RevetmentWaveprotTypeManager.WaveprotTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Slope (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.RevetmentManager.SlopeRevetment + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Length (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.RevetmentManager.LengthRevetment + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Plain Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.RevetmentManager.PlainWidth + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");
            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "SLU")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Sluice Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.SluiceManager.SluiceTypeManager.SluiceTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Number Of Vents" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.SluiceManager.VentNumberSluice + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Vent Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.SluiceManager.VentWidthSluice + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Vent Height (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.SluiceManager.VentHeightSluice + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Vent Diameter (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.SluiceManager.VentDiameterSluice + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");
               
                htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

                htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
                htmlAssetDetailsBody.Append("<table id=\"gateDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

                htmlAssetDetailsBody.Append("<thead>");
                htmlAssetDetailsBody.Append("<th>" + "Gate Type" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Construction Material" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Number of Gates" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Height" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Width" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Diameter" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Install Date" + "</th>");
                htmlAssetDetailsBody.Append("</thead>");

                htmlAssetDetailsBody.Append("<tbody>");
                for (var i = 0; i < gateDataManagerList.Count; i++)
                {
                    htmlAssetDetailsBody.Append("<tr>");
                    htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].GateTypeManager.GateTypeName + "</td>");
                    htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].GateConstructionMaterialManager.GateConstructionMaterialName + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\" >" + gateDataManagerList[i].NumberOfGates + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\" >" + gateDataManagerList[i].Height + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\" >" + gateDataManagerList[i].Width + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\" >" + gateDataManagerList[i].Diameter + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\" >" + gateDataManagerList[i].InstallDate.ToString("dd/MM/yyyy") + "</td>");
                    htmlAssetDetailsBody.Append("</tr>");
                }
                htmlAssetDetailsBody.Append("</tbody>");

                htmlAssetDetailsBody.Append("</table>");                

            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "SPU")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Orientation (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.SpurManager.Orientation + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Length (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.SpurManager.LengthSpur + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.SpurManager.WidthSpur + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>"); ;

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Shape Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" colspan =\"6\">" + assetDataObj.SpurManager.SpurShapeTypeManager.ShapeTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Construction Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" colspan =\"6\">" + assetDataObj.SpurManager.SpurConstructionTypeManager.ConstructionTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border:0px;\">" + "Revet Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;width:10px;\" colspan =\"6\">" + assetDataObj.SpurManager.SpurRevetTypeManager.SpurRevetTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");
            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "TRN")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Inlet Box Length (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.TurnoutManager.InletboxLength + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Inlet Box Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.TurnoutManager.InletboxWidth + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Outlet Number" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.TurnoutManager.OutletNumber + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Outlet Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.TurnoutManager.OutletWidth + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Outlet Height (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.TurnoutManager.OutletHeight + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Turnout Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.TurnoutManager.TurnoutTypeManager.TurnoutTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Gate Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.TurnoutManager.TurnoutGateTypeManager.TurnoutGateTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<div id=\"table-container\" style=\"padding:1%;\">");

                htmlAssetDetailsBody.Append("<div class=\"table-responsive\">");
                htmlAssetDetailsBody.Append("<table id=\"gateDetails\" class=\"table table-striped table-bordered table-hover\" cellspacing=\"0\" width=\"100%\">");

                htmlAssetDetailsBody.Append("<thead>");
                htmlAssetDetailsBody.Append("<th>" + "Gate Type" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Construction Material" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Number of Gates" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Height" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Width" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Diameter" + "</th>");
                htmlAssetDetailsBody.Append("<th>" + "Install Date" + "</th>");
                htmlAssetDetailsBody.Append("</thead>");

                htmlAssetDetailsBody.Append("<tbody>");
                for (var i = 0; i < gateDataManagerList.Count; i++)
                {
                    htmlAssetDetailsBody.Append("<tr>");
                    htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].GateTypeManager.GateTypeName + "</td>");
                    htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].GateConstructionMaterialManager.GateConstructionMaterialName + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\">" + gateDataManagerList[i].NumberOfGates + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\" >" + gateDataManagerList[i].Height + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\" >" + gateDataManagerList[i].Width + "</td>");
                    htmlAssetDetailsBody.Append("<td align = \"right\" >" + gateDataManagerList[i].Diameter + "</td>");
                    htmlAssetDetailsBody.Append("<td>" + gateDataManagerList[i].InstallDate.ToString("dd/MM/yyyy") + "</td>");
                    htmlAssetDetailsBody.Append("</tr>");
                }
                htmlAssetDetailsBody.Append("</tbody>");

                htmlAssetDetailsBody.Append("</table>");
                

            }
            else if (assetDataObj.AssetTypeManager.AssetTypeCode == "WEI")
            {
                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Invert Level" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.WeirManager.InvertLevel + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Crest Top Level" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.WeirManager.CrestTopLevel + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Crest Top Width (m)" + "</td>");
                htmlAssetDetailsBody.Append("<td align = \"right\" style=\"border: 0px;width:10px;\">" + assetDataObj.WeirManager.CrestTopWidth + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"10\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");

                htmlAssetDetailsBody.Append("<tr>");
                htmlAssetDetailsBody.Append("<td style=\"font-weight: bold;border: 0px;\">" + "Weir Type" + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"6\">" + assetDataObj.WeirManager.WeirTypeManager.WeirTypeName + "</td>");
                htmlAssetDetailsBody.Append("<td style=\"border: 0px;\" colspan =\"5\">" + "" + "</td>");
                htmlAssetDetailsBody.Append("</tr>");
             
            }
            htmlAssetDetailsBody.Append("</tbody>");

            htmlAssetDetailsBody.Append("</table>");
            htmlAssetDetailsBody.Append("</div>");

            htmlAssetDetailsBody.Append("</div>");
            htmlAssetDetailsBody.Append("</div>");   

            return htmlAssetDetailsBody.ToString();

        } 
                    
    }
}